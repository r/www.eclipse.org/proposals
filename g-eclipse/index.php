<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("g-Eclipse");
?>

<h1>Proposal for g-Eclipse - an integrated Grid enabled environment
(IGE)</h1>

<h2>Introduction</h2>
<p>The g-Eclipse project is a proposed open source project under the
<a href="http://www.eclipse.org/technology/">Eclipse Technology
Project</a>.</p>
<p>This proposal is in the Proposal Phase (as defined in the <a
	href="http://www.eclipse.org/projects/dev_process/">Eclipse
Development Process document</a>) and is written to declare its intent and
scope. This proposal is written to solicit input from the Eclipse
community and additional participation. You are invited to comment on
and join the project.</p>
<p>Please send all feedback to the <a
	href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.g-eclipse">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.g-eclipse</a>
newsgroup.</p>

<h2>Background</h2>
<p>Over the last few years Grid infrastructures have been becoming
the backbone of those fields of science and research that require to
solve complex computational problems. At the same time the commercial
application of Grid technologies has led to new categories of offerings
(e.g. on-demand offerings) and will likely play an important role in the
&quot;software as a service&quot; landscape. However, the complexity of
Grid infrastructures is often discouraging to new and inexperienced
users and impedes the use of Grid technologies in new application
domains.</p>
<p>By providing an integrated Grid enabled environment based on
Eclipse (called g-Eclipse) this project aims at facilitating the use of
Grid infrastructures. As an integrating tool g-Eclipse will have impact
on the different actors in the Grid domain. Grid users will access the
Grid with g-Eclipse in an easy and uniform way, instead of having to use
application specific portals or specific Grid consoles. Developers can
take advantage of the large variety of languages that are supported by
Eclipse and benefit from a standard mechanism to deploy applications on
Grid infrastructures.</p>
<p>With the flexible and open plug-in approach of g-Eclipse, the
management interface of new Grid applications can be customized and
deployed relatively easily. It shall be possible to reuse already
existing management components.</p>
<p>g-Eclipse will deliver exemplary support for the <a
	href="http://www.eu-egee.org/">EGEE</a> middleware <a
	href="http://glite.web.cern.ch/glite/">gLite/LCG</a>, which is deployed
on the biggest Grid infrastructure currently available. This middleware
provides a solid basis for exemplary tools, as the number of generic
applications will increase in the second phase of EGEE (a European Grid
initiative), and thus the number of potential adopters of the g-Eclipse
platform will increase in the same way. However, as g-Eclipse is
following a frameworks and exemplary tools approach, other middleware
like UNICORE and GT4 can also be integrated by extending g-Eclipse.</p>
<p>With the proposed g-Eclipse project, Eclipse enters the area of
Grid applications and e-Science. The consortium that is submitting this
proposal will receive funding by the European Union under contract
number IST-034327 to implement and establish <a
	href="http://www.g-eclipse.eu/">g-Eclipse</a>.</p>

<h2>Scope</h2>
<p>The objectives of the g-Eclipse project are:</p>
<ul>
	<li><b>Development of a general framework that can be used by
	Grid users, Grid developers and Grid operators</b>
	<p>The software developed in the g-Eclipse project will consist of
	the &quot;core Grid plug-ins&quot; for the Eclipse platform. These will
	enable and standardize the access of Grid infrastructures from within
	Eclipse, independent of the used middleware. The framework will
	integrate functionality for the following use cases:</p>
	<ul>
		<li><b>Grid Virtual Organization management:</b>
		<p>A functionality that allows the dynamic creation and management
		of Virtual Organizations and their resources will be developed. This
		includes authentication and authorization of users on remote Grid
		resources.</p>
		</li>
		<li><b>Grid job management:</b>
		<p>One of the standard actions on the Grid is the submission of
		Grid jobs and their monitoring. A functionality will support Grid
		users and developers to create and manage Grid jobs independently from
		the middleware.</p>
		</li>
		<li><b>Grid file management:</b>
		<p>The management of distributed data in a Grid environment is
		based on file catalogues. The file management functionality will allow
		the management of distributed files using functionalities of the Grid
		file catalogues.</p>
		</li>
		<li><b>Grid infrastructure monitoring:</b>
		<p>Hardware and other resources of the Grid infrastructure might
		change over time. Therefore a good virtualization tool for Grid
		infrastructure resources is needed by Grid users, developers and
		operators. A functionality will be developed to allow for the visual
		representation and monitoring of Grid resources.</p>
		</li>
		<li><b>Grid application monitoring:</b>
		<p>The execution of Grid applications is different from the
		execution of applications on a single cluster. The monitoring of
		applications on remote, not a-priori known resources, needs tools that
		allow the transparent monitoring of such applications. A frontend that
		allows monitoring of Grid applications will be provided, existing
		functionality of <a href="http://www.eclipse.org/tptp/">TPTP</a> will
		be leveraged if possible.</p>
		</li>
		<li><b>Grid benchmarking:</b>
		<p>Grid resources are heterogeneous in terms of their computing
		and communication capabilities. Therefore different Grid actors must
		be able to explore the potential of a Grid system and resources
		through standard benchmarks. A benchmarking functionality that allows
		the execution and presentation of benchmarks on Grid resources will be
		provided.</p>
		</li>
		<li><b>Grid application deployment:</b>
		<p>A functionality that supports Grid application developers and
		Grid application users with the deployment of their applications on
		the Grid will be developed.</p>
		</li>
		<li><b>Grid visualization tools:</b>
		<p>A Grid visualization framework will offer functionalities that
		allows the visualization of scientific and numerical calculations.</p>
		</li>
		<li><b>Grid workflow builder:</b>
		<p>In the future Grid applications will consist of many
		interlinked Grid jobs building a complex workflow. A functionality to
		support the creation of such workflows by a graphical editor will be
		provided based on <a href="http://www.eclipse.org/gef/">GEF</a> (<a
			href="http://www.eclipse.org/gmf/">GMF</a>).</p>
		</li>
		<li><b>Grid command console:</b>
		<p>Many existing Grid systems are using a command line interface
		(CLI) to interact with Grid resources and most Grid users are familiar
		with these CLIs. Therefore functionality will be delivered that can be
		used as Grid command console.</p>
		</li>
	</ul>
	</li>
	<li><b>Exemplary implementation of the integration of the
	widely deployed EGEE middleware gLite</b>
	<p>The g-Eclipse project will demonstrate the usefulness of the
	general framework by the exemplary support for the gLite middleware of
	the EGEE project and its components.</p>
	<ul>
		<li><b>Grid Virtual Organization management:</b>
		<p>Using the <a
			href="https://edms.cern.ch/file/571991/1/voms-guide.pdf">VOMS
		(virtual organization management service)</a> functionality of
		authorization, authentication, and management of virtual organizations
		will be implemented.</p>
		</li>
		<li><b>Grid job and file management, application deployment
		and monitoring:</b>
		<p>These will be implemented as a connection to the g-Lite
		middleware.</p>
		</li>
		<li><b>Grid infrastructure monitoring:</b>
		<p>A virtualization tool for the EGEE infrastructure will be
		provided.</p>
		</li>
		<li><b>Grid benchmarking:</b>
		<p>Already existing functionality of the <a
			href="http://grid.ucy.ac.cy/gridbench/">GridBench</a> will be reused
		if possible.</p>
		</li>
		<li><b>Grid visualization tools:</b>
		<p>The <a href="http://www.gup.uni-linz.ac.at/gvk/">Grid
		visualization kernel GVK</a> will be integrated.</p>
		</li>
	</ul>
	</li>
</ul>

<h2>Organization</h2>

<h3>Initial committers</h3>
<p>The initial committers will focus on the definition of the
general Grid framework and on the exemplary support for the gLite
middleware. The evolving and hardening of the general framework will be
followed as well. Our goal is the delivery of a middleware independent
API needed to develop and integrate middleware dependent tools.</p>

<p>The initial committers are:</p>

<table rules="none">
	<tbody>
		<tr>
			<td>Harald Kornmayer (project lead)</td>
			<td>Forschungszentrum Karlsruhe GmbH</td>
		</tr>
		<tr>
			<td>Markus Knauer (lead architect)</td>
			<td>Innoopract Informationssysteme GmbH</td>
		</tr>
		<tr>
			<td>Pawel Wolniewicz</td>
			<td>Poznan Supercomputing and Networking Center</td>
		</tr>
		<tr>
			<td>Jochen Krause</td>
			<td>Innoopract Informationssysteme GmbH</td>
		</tr>
		<tr>
			<td>Katarzyna Bylec</td>
			<td>Poznan Supercomputing and Networking Center</td>
		</tr>
		<tr>
			<td>Thomas Koeckerbauer</td>
			<td>Institut f&uuml;r Graphische und Parallele Datenverarbeitung
			der Johannes Kepler Universit&auml;t</td>
		</tr>
		<tr>
			<td>Rafa Lichwala</td>
			<td>Poznan Supercomputing and Networking Center</td>
		</tr>
		<tr>
			<td>Mateusz Pabis</td>
			<td>Poznan Supercomputing and Networking Center</td>
		</tr>
		<tr>
			<td>Martin Polak</td>
			<td>Institut f&uuml;r Graphische und Parallele Datenverarbeitung
			der Johannes Kepler Universit&auml;t</td>
		</tr>
		<tr>
			<td>Mathias Stuempert</td>
			<td>Forschungszentrum Karlsruhe GmbH</td>
		</tr>
		<tr>
			<td>Ashish Thandavan</td>
			<td>Advanced Computing and Emerging Technologies Centre</td>
		</tr>
		<tr>
			<td>George Tsouloupas</td>
			<td>University of Cyprus</td>
		</tr>
	</tbody>
</table>

<h3>Code Contribution</h3>
<p>All partners start the Eclipse specific development with this
project. Therefore no initial code contributions will be given by the
partners at the start of the g-Eclipse project.</p>

<h3>Interested parties</h3>
<ul>
	<li><a href="http://www.fzk.de/">Forschungszentrum Karlsruhe
	GmbH</a>, Karlsruhe, Germany</li>
	<li><a href="http://www.man.poznan.pl/">Poznan Supercomputing
	and Networking Center</a>, Poznan, Poland</li>
	<li><a href="http://www.jku.at/">Institut f&uuml;r Graphische
	und Parallele Datenverarbeitung</a> der Johannes Kepler Universit&auml;t
	Linz, Austria</li>
	<li><a href="http://www.ucy.ac.cy/">Dept. of Computer Science</a>,
	University of Cyprus, Nicosia, Cyprus</li>
	<li><a href="http://www.innoopract.com/">Innoopract
	Informationssysteme GmbH</a>, Karlsruhe, Germany</li>
	<li><a href="http://www.reading.ac.uk/">Advanced Computing and
	Emerging Technologies Centre</a>, University of Reading, UK</li>
</ul>

<p>The above mentioned partners have committed to provide at least 8
full time resources for the next two years for the implementation of
g-Eclipse.</p>


<h3>User community</h3>
<p>Grid infrastructures for e-Science have been built over the last
few years and there is a clear tendency for a long-term provision of
such infrastructures for scientists. As the interest in using these
infrastructures in the scientific and the commercial domain is rising,
the g-Eclipse project expects many early adopters of the projects
results. The project plans to advance the project by cooperating with
other Grid infrastructure projects, by participating at dedicated Grid
conferences and events and by using the standard eclipse.org mechanisms
of supporting an open source community of early adaptors.</p>


<h3>Tentative Plan</h3>
<table rules="none">
	<tbody>
		<tr>
			<td valign="baseline">2006-12</td>
			<td valign="baseline">0.1</td>
			<td valign="baseline">Basic implementation of the integrated
			Grid environment (IGE) <br />
			First implementations of exemplary plug-ins for the gLite middleware
			</td>
		</tr>
		<tr>
			<td valign="baseline">2007-06</td>
			<td valign="baseline">0.5</td>
			<td valign="baseline">Provision of the initial API <br />
			Implementation of plug-ins for the gLite middleware</td>
		</tr>
		<tr>
			<td valign="baseline">2007-12</td>
			<td valign="baseline">0.8</td>
			<td valign="baseline">Exemplary support for a second Grid
			middleware validating the frameworks and API</td>
		</tr>
		<tr>
			<td valign="baseline">2008-06</td>
			<td valign="baseline">1.0</td>
			<td valign="baseline">Release 1.0</td>
		</tr>
	</tbody>
</table>
</body>
</html>


</p>
      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
