<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "GenGMF";
$pageKeywords	= "GenGMF GMF EMF DSL";
$pageAuthor		= "Enrico Schnepel";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>GenGMF - a GMF model generator</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("GenGMF");
?>

<h2>Introduction</h2>

<p>
      GenGMF is an open source project proposed as incubator under the Graphical Modeling Project (GMP) project.
    </p>

<p>
      The project is in the Project Proposal Phase (as defined by the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process</a>) and this 
      proposal declares its intent and scope. The proposal is written to solicit additional participation and input from the Eclipse community. You are 
      invited to comment on and/or join the project. Please send all feedback to the <a href="http://www.eclipse.org/forums/eclipse.modeling.gmf">Modeling GMF</a> forum 
      with "[GenGMF]" in the subject.
    </p>

<h2>Background</h2>

<p>
      "Domain Specific Languages" (DSL) are an essential part of the complex of topic "Model Driven Software Development" (MDSD). Tree based language editors as provided by "Eclipse Modelling Framework" (EMF) as well as textual DSLs as provided by the "Textual Modelling Framework" (TMF) have a good tooling but the language user cannot get a quick overview about the complete model with its main objects and their references. A graphical DSL could be used to display objects and their references intuitively e.g. as boxes and connections. The "Graphical Modelling Framework" (GMF) allows to model the concepts of a graphical editor but the tooling is not so smart. There are a lot of pitfalls a DSL developer must avoid before he can use a graphical editor for modelling.
    </p>

<p>
      GMF models often contain a lot of duplication. Figures that share a common layout concepts but differ in some details need a figure description of its own. Even small changes like an other background color make a new figure which results in much larger editor models. Larger models are harder to handle because they contain a lot of element references and the GMF tooling rarely filters the list of elements appropriately while setting a reference in the properties view.
    </p>

<p>
      The integrated GMF wizards can be used to create the GMF models. The three model creation wizards to create the tooling, graph and mapping definitions are heavy-weight because every information needs to be entered in a few wizard steps making the steps themselves very complex. Information entered in one wizard is not shared and has to be entered in the next wizard again. Additionally the wizards hardly handle meta model evolution and can not handle compartments which are often used to display the concept of a parent-child-relation.
    </p>

<h2>Scope</h2>

<p>
     GenGMF will provide an efficient tooling for developing graphical editors using the GMF. This includes
    </p>
<ul>
	<li>interactive wizards, to create a GenGMF specific editor model iteratively,</li>
	<li>an optimized GenGMF model editor for fine-tuning the wizard-created model and</li>
	<li>a model-to-model transformation to generate the GMF models which in turn can be used without modifications to generate the editor using standard GMF techniques.</li>
</ul>

<h2>Description</h2>

<p>
     GenGMF has been originally developed with the ease of complexity in mind. With the introduction of wizards in version 2 the scope has been extended to provide a smart and adaptive tool support for developing graphical DSLs.
    </p>

<h3>Ease of complexity</h3>

<p>The complexity of a model (in the context of MDSD) can be measured by counting the elements and the setting possibilities of each non-containment model reference being set. The complexity grows over-proportionally with the model size.</p>

<p>
      While developing an editor using the GMF, the editor models can grow so large that they are hardly manageable by humans. GenGMF removes duplications by combining similar figure descriptors with their attached node, edge, compartment and label elements in a single template. The differences between the figures and the template are stored in descriptors which are comparable to the elements from the GMF mapping definition. Transformators can be applied to a descriptor to change the visual appearance of a specific figure.
    </p>

<p>
      The model-to-model transformation generates complete GMF models by merging the templates with the information from the descriptors. The otherwise manually created but now generated GMF models contain about 2 to 5 times more model elements and references than the original GenGMF model they are derived from. Compared to a GMF model the complexity of a GenGMF model is a magnitude smaller.
    </p>

<h3>Smart and adaptive tool support</h3>

<p>
      For GenGMF wizards have been introduced in order to lower the entry level for DSL developers and to get first results fast. They can be used to get a basic editor for an existing meta-model within a few minutes. Different wizards exist for template or descriptor creation or modification. The wizards adapt their behaviour to the settings already made. Using the interactive wizards a first version of a complete GenGMF model can be iteratively created. Afterwards the model can be tweaked and tuned manually or using the wizards to fulfil all the needs. It is possible to automatically modify the model during the transformation using a scripting interface.
    </p>

<h2>Relationship with other Eclipse Projects</h2>

    <ul>
        <li>GenGMF will be built on top of EMF and GMF.</li>
        <li>GenGMF will integrate with the GMF projects.</li>
        <li>GenGMF will make use of the Xtend model transformation language as far as possible for the model transformation.</li>
        <li>GenGMF will make use of the Check model constraint language as far as possible for model validation.</li>
        <li>GenGMF will make use of the AspectJ project as far as possible for improvements of the tree based GenGMF model editor.</li>
        <li>GenGMF will make use of the Epsilon wizarding language as far as possible for implementing and improving wizards.</li>
        <li>GenGMF will make use of the Edapt (formerly COPE) project as far as possible for metamodel evolution.</li>
    </ul>

<h2>Organization</h2>
<h3>Mentors</h3>

    <ul>
        <li>Ed Merks (Macro Modeling, Canada)</li>
    </ul>

<h3>Proposed initial committers</h3>

    <ul>
        <li>Enrico Schnepel (Germany, project lead)</li>
        <li>Attila Bak (Germany)</li>
        <li>Martin Fl&uuml;gge (Germany)</li>
    </ul>

<h3>Interested parties</h3>

    <ul>
        <li>b+m Informatik AG (Germany)</li>
		<li>Anthony Hunter (IBM, Canada)</li>
        <li>Ed Merks (Macro Modeling, Canada)</li>
        <li>Frederic Jouault (Univ. Nantes, France)</li>
        <li>Jean Bezivin (Univ. Nantes, France)</li>
        <li>Kenn Hussey (Canada)</li>
        <li>Paul Elder (IBM, Canada)</li>
        <li>Richard Gronback (Borland, USA)</li>
        <li>Sebastien Demathieu (Thalesgroup, France)</li>
        <li>Sven Efftinge(itemis, Germany)</li>
        <li>Tristan Faure (Atos Origin, France)</li>
        <li>Thibault Landr&eacute; (Atos Origin, France)</li>
        <li>Mahmoud Elhamlaoui (ENSIAS, Morocco)</li>
    </ul>

<h3>Code contributions</h3>

<p>
     The initial code contribution will be a set of plug-ins from the existing GenGMF project. Visit the <a href="http://gengmf.randomice.net/">project's website</a> for further information and documentation.
    </p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
