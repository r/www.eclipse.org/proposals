<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>JavaScript Development Tools</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("JavaScript Development Tools");
?>

<h3>Introduction</h3>
<p>The JavaScript Development Tools (JSDT) project is a proposed Open Source project under the top-level Eclipse Web Tools Platform Project (WTP).</p>
<p>This proposal is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the WTP <a href="http://www.eclipse.org/forums/index.php?t=thread&frm_id=88&S=849948437f2dc6c1fc5b792ec2e7705d">forum</a>/<a href="news://news.eclipse.org/eclipse.webtools">newsgroup</a>.</p>
<h3>Description</h3>
<p>As the JavaScript component available within WTP matures and approaches parity with its contemporaries (JDT, PDT, CDT, etc.), it needs to be positioned similarly to have the appropriate exposure within the Eclipse community and to match its increased scope.  The JSDT project aims to evolve and better promote JavaScript-based development technologies within the Eclipse community.  It will do so by providing a rich set of tools for the development of non-trivial software using JavaScript, including facilities for editing, validating, refactoring, and debugging, as well as frameworks for vendors and other projects to enhance and build upon.</p>
<h3>Project Scope</h3>
<p>The JSDT project will include support for the core language as standardized by Ecma International and developed by the Mozilla Foundation, and for client-side JavaScript within web pages as specified by the Mozilla Foundation and the World Wide Web Consortium (W3C).</p>
<h3>Initial Plan</h3>
<p>The project will issue milestone builds coinciding with the Web Tools Platform, while incubating, with the goal of including its first proper release, Javascript Development Tools 1.2, within WTP 3.2 and various Helios packages.</p>
<h3>Project Organization</h3>
<p>The project will initially consist of the JSDT component of WTP's Source Editing project combined with the JavaScript debugging support currently being developed within the e4 Incubator Project.  The proposed initial committers are:</p>
<ul>
<li>Nitin Dahyabhai (IBM): project lead</li>
<li>Christopher Jaun (IBM)</li>
<li>Simon Kaegi (IBM)</li>
<li>Michael Rennie (IBM)</li>
</ul>
<h4>Interested Parties:</h4>
<ul>
<li>Jacek Pospychala (Zend Technologies Ltd.)</li>
</ul>
<h4>Mentors:</h4>
<ul>
<li>David Williams, WTP</li>
<li>John Arthorne, Eclipse Project</li>
</ul>
<p>To express support, concern, or constructive opinions regarding the formation of this proposed JavaScript Tools project, all are encouraged to utilize the aforementioned forum/newsgroup.</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
