<!--
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>


<title>Mylyn Reviews</title>
</head>

<body>

<p>The Mylyn Reviews project is a proposed open source project 
under the <a href="http://www.eclipse.org/projects/project_summary.php?projectid=tools.mylyn"> Mylyn 
project</a>. <a href="http://www.inso.tuwien.ac.at/projects/reviewclipse/">ReviewClipse</a> will be 
open-sourced under the EPL and evolve into the Mylyn Reviews project.
</p>

<p>
This proposal is in the Project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process document</a>) and is written to declare its intent and scope. 
This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project.
Please send all feedback to the <a href="https://dev.eclipse.org/mailman/listinfo/mylyn-dev">https://dev.eclipse.org/mailman/listinfo/mylyn-dev</a> mailing list.
</p>

<h2>Background</h2>
<p>
Code reviews have many benefits, most importantly to find bugs early in the development phase and to enforce coding standards or guidelines.
</p>
<p>ReviewClipse was started as a research project by Mario Bernhart, Christoph Mayerhofer and Prof. Thomas Grechenig in 2008. 
ReviewClipse is a code review plug-in for Eclipse, which helps developers to review the source code efficiently on a per changeset basis - the item under inspection is one changeset in the revision control system, influenced by the changeset definition of SVN. 
The tight integration of the review plug-in into the Eclipse development environment helps the developers to accept the review task as part of their daily workload, like writing code, debugging or refactoring. 
Because of its simple design it is possible to configure the review process with flexible assignments and filters according to the role of each team member. 
The review data is stored in XML files stored directly in the revision control system.
There is no need to setup any further server-side application. 
The reviewer uses the review editor to inspect the filtered changesets, compares different versions of a source code file with the compare editor and leaves his rating and optional comments for the committer of the changeset. 
The review editor aligns with the usability concepts of the Eclipse environment, so the initial training consumes little time.
Currently the development effort is aimed towards various feature requests from the community, especially inline commenting for source code reviews.
</p>

<p>
ReviewClipse has an active user community and was presented at <a href="http://www.eclipsecon.org/2009/sessions?id=654">EclipseCon 2009</a>, <a href="http://www.eclipsecon.org/summiteurope2009/sessions?id=927">Eclipse Summit Europe 2009</a>, <a href="http://2009.subconf.de/fileadmin/PDF_Dateien/CMConf___SubConf/PDF_Vortraege/MR4.pdf">SubConf 2009</a>, <a href="http://it-republik.de/jaxenter/eclipse-magazin-ausgaben/Jetty%40Eclipse-000323.html">Eclipse Magazin 06.09</a> (German), <a href="http://eclipse.dzone.com/articles/reviewclipse-supporting-agile">DZone</a>, <a href="http://www.heise.de/developer/artikel/Kontinuierliche-Code-Reviews-mit-Subversion-und-Eclipse-787460.html">Heise Developer</a> (German) and <a href="http://wiki.eclipse.org/Eclipse_DemoCamps_November_2009/Vienna">Eclipse Demo Camp Vienna 2009</a>.
</p>

<p>
The concept and architecture of ReviewClipse in a nutshell: <a href="http://www.inso.tuwien.ac.at/fileadmin/Files/ReviewClipse/ReviewClipse_EclipseSummit2009.pdf">ESE09 Slides</a>
</p>

<p>
From a Mylyn user's perspective, binding the review to a Mylyn task would be highly beneficial for the following reasons:
</p>

<ul>
	<li>the task is the core element of the developer's work</li>
	<li>a task state may serve as trigger for reviews</li>
	<li>a task relates to corresponding changes (e.g. list of changesets or an attached patch) which may be reviewed together</li>
</ul>

<h2>Scope</h2>
<p>
The scope of the Mylyn Reviews project is to:
</p>

<ul>
	<li>do reviews based on Mylyn tasks</li>
	<li>filter the review scope e.g. all test cases</li>
	<li>provide inline-commenting for source code reviews</li>
	<li>incorporate the review tasks in the Mylyn task list</li>
	<li>NOT change any existing APIs of Mylyn</li>
</ul>

<h2>Description</h2>
<p>
Mylyn Reviews aims to be a convenient review tool for Mylyn.
It does not require any server-side components other than a Mylyn task repository and optionally a SCM server.
</p>
<p>
The review tasks will be rendered as Mylyn tasks and possibly as subtasks of the corresponding task under review. 
Review tasks can be generated the following ways:
</p>
<ul>
	<li>Manually generated from the changesets/revisions of changes related to a Mylyn task.</li>
	<li>Manually generated from a patch attached to a Mylyn task.</li>
	<li>Manually generated from a Mylyn task context.</li>
</ul>

<p>A review can be planned for multiple reviewers, which may
have a different filtered selection of the review scope e.g. reviewing
of all test cases.
A review is represented as one task in the respective task repository
and contains the following information:
</p>

<ul>
	<li>The review definition: scope, reference to the original task, references to the scope, filters, list of reviewers etc. (one task attachment)</li>
	<li>The review data: state and comments (one task attachment per reviewer)</li>
</ul>

<p>While this proposed design does not directly support the ReviewClipse work-flow, a migration path will be provided at a later time.</p>

<h3 >Planned plug-ins</h3>
<p>
<img alt="Overview of reviews components" title="Overview of reviews components" src="reviews-overview.png" width="100%">
</p>

<table border="1">
<tbody>
	<tr><td>.reviews.core</td><td>This plug-in contains the logic for creating the review task. It is also responsible for persisting the review information.</td></tr>
	<tr><td>.reviews.ui</td><td>This plug-in contains the UI components for the task editor page for review tasks</td></tr>
	<tr><td>.reviews.egit</td><td>This plug-in contains the SCM connector for git using EGit.</td></tr>
	<tr><td>.reviews.cvs</td><td>This plug-in contains the SCM connector for CVS.</td></tr>
	<tr><td>.reviews.subclipse</td><td>This plug-in contains the SCM connector for subversion using SubClipse.</td></tr>
	<tr><td>.reviews.subversive</td><td>This plug-in contains the SCM connector for subversion using Subversive.</td></tr>
</tbody>
</table>
<p>
Note:<br>
The UI and non-UI logic are separeted into different plug-ins.<br>
Configuration of the corresponding Team Provider of the projects is used.
</p>
<h3>Initial Contribution</h3>
<p>
The initial contribution will cover the .reviews.core, .reviews.ui, .reviews.subclipse and the .reviews.subversive plug-ins. The EGit and CVS Support is currently under development and will be merged later on.
The existing code depends only on freely available plug-ins (see the MANIFEST.MF of ReviewClipse for further details) and uses the following libraries: lib/commons-logging-1.0.4.jar,
 lib/log4j-1.2.11.jar,
 lib/jsr173_1.0_api.jar,
 lib/resolver.jar,
 lib/schema.jar,
 lib/xbean_xpath.jar,
 lib/xbean.jar,
 lib/xmlbeans-qname.jar,
 lib/xmlpublic.jar.
Further, the SWTCalendar (under the MIT Licence) was modified and used.
</p>

<h2>Organization</h2>

<h3>Initial committers</h3>
<p>Our agile development process will follow eclipse.org's standards
for openness and transparency. Our goal is to provide a tool to conduct
code reviews as part of the day-to-day developer activities. We will
actively encourage contributions to Mylyn Reviews. We also plan to help
improve the Eclipse platform by submitting patches and extension point
suggestions. The initial committers are:
</p>
<ul>
	<li>Mario Bernhart (<a href="http://www.inso.tuwien.ac.at/">INSO</a>): project lead</li>
	<li>Christoph Mayerhofer (<a href="http://www.inso.tuwien.ac.at/">INSO</a>)</li>
	<li>Kilian Matt (<a href="http://www.inso.tuwien.ac.at/">INSO</a>)</li>
	<li>Stefan Strobl (<a href="http://www.inso.tuwien.ac.at/">INSO</a>)</li>
</ul>

<h3>Interested parties</h3>
<p>The following projects have expressed interest extending the model or tools, contributing ideas, guidance and discussion.
Key contacts listed.
</p>
<ul>
	<li>Vienna University of Technology, Research Group for Industrial Software <a href="http://www.inso.tuwien.ac.at/">INSO</a></li>
	<li>ReviewClipse Project</li>
	<li>Tasktop Technologies</li>
</ul>

<h2>Mentors</h2>
<p>The following Architecture Council members will mentor this project:</p>

<ul>
	<li>Mik Kersten</li>
	<li>Chris Aniszczyk</li>
</ul>

<h3>Developer community</h3>
<p>We expect to extend the initial set of committers by actively supporting a developer community. 
We will make it easy for interested parties to take an active role in the sub-project by making our planning and process transparent and remotely accessible. </p>

<h3>User community</h3>
<p>
ReviewClipse was downloaded 6000+ times. We assume to have around 1000 active users. 
With the tight integration into Mylyn, the potential user community will be extended significantly. 
Many ReviewClipse users requested a task-centered review approach. 
Though, to support a direct migration from ReviewClipse to Mylyn Reviews, a non-task based approach may be implemented. </p>

<h2>Tentative Plan</h2>
<p>
2009-12 v0.1: First open source release<br>
2010-01 v0.2: Patch based reviewing <br>
2010-02 v0.3: Task changeset and context based reviewing<br>
2010-04 v0.4: Inline commenting for source files
</p>


<h2>Changes to this Document</h2>

<table>	
<tr>
	<th>Date</th>
	<th>Change</th>
</tr>

<tr>
	<td>2009-12-14</td>
	<td>First public version</td>
</tr>
</table>


</body>
</html>
