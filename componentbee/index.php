<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>


<div id="maincontent">
  <div id="midcolumn">

<h1>Component Behaviour Evaluation Project (ComponentBee) </h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("ComponentBee");
?>

<h2>Introduction</h2>
<p><strong>Component Behaviour Evaluation (ComponentBee)</strong>  is a proposed open source project under the <a href="http://www.eclipse.org/projects/project_summary.php?projectid=technology" target="_blank">Eclipse Technology Project. </a></p>
<p>This proposal is in the Project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/development_process.php" target="_blank"> Eclipse Development Process</a>) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. Please send 
all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.componentbee">eclipse.componentbee newsgroup</a>.
 <p>ComponentBee project provides an extensibile frawework for tools that are used to evaluate the dynamic behaviour of software components in unit and  integration tests. In brief, such tools are able to log the dynamic behaviour of software components, extract behaviour patterns from the collected log, and finally calculate reliability (or other quality) values for the tested components.  ComponentBee also faciliates the creation of visual test model editors.
 
 <h2>Description</h2>
    <h3>Background</h3>
    <p align="left">The construction of reliable component-based SW systems requires methods to facilitate the SW integrators to ensure that the SW architecture, selected components, and finally the constructed SW system meet the desired quality requirements. 
 Component Behaviour Evaluation (ComponentBee) framework supports the reliability testing of software components and integrates these testing features to support architectural-level reliability analysis of software systems. </p>
    <h3 align="left">Scope</h3>
    <p>The ComponentBee project develops an extensible framework and exemplary tools to evaluate the reliability and quality of software components that are executed in multiple threads. The project does not focus on dynamic behaviour monitoring but utilises existing TPTP tools in trace and raw log data recording. </p>
<p>The aim of the project is to develop a framework for tools being capable of evaluating dynamic behaviour of software components from trace and raw log data. The framework provides interfaces for: 1) the trace and raw log data and 2) for processors that will process and compose new presentations for trace and raw log data. The trace and raw log data interfaces provide an access to data structures that present the dynamic behaviour of software components. The processors will utilise these trace and raw log data interfaces and compose different kinds of presentations (data structures) for dynamic behaviour of software components. More precisely, the processors will: 1) compose an abstract tree presentation for dynamic behaviour of software components performing concurrent processing in different threads, then 2) extract behaviour patterns from the abstract tree presentation, and finally 3) evaluate the extracted behaviour patterns and calculate quality and reliability values for the evaluated software components. In addition, the ComponentBee framework provides a domain-specific-language (DSL), which supports the creation of visual test model editors. </p>
<p>ComponentBee project also provides exemplary tools that are currently capable of evaluating dynamic behaviour of multithreaded Java programs. The provided tools are model-based tools that are integrated into Eclipse. They include a visual editor, which can be used to create generic  and reusable test models and, then, to refine these test models to support the testing of specific Java components. </p>
<h3 align="left"><strong>Proposed Features </strong></h3>
    <p align="left"><strong>Core functionality: </strong></p>
    <ul>
      <li><strong>Support evaluation of concurrent behaviours executed in various threads. </strong>ComponentBee makes it possible to takee message sequences executed in different threads as an input, recognize how these different threads interact, compose an overall presentation (a behaviour tree) for the concurrent behaviours, and finally extracs the behaviour patterns from the behaviour tree.
      <br>
      </li>
      <li><strong>Support for reusable and configurable plugins. </strong>The input data sources can be configured and thus the ComponentBee plugins can be adapted to support behaviour testing of different kinds of component-based applications.<br>
      </li>
      <li><strong>Support for both generic and domain and application-specific test model creation. </strong>ComponentBee provides an extensible language and framework for creating visual test model editors. The editors are based on the <a href="http://www.eclipse.org/gmf">Graphical Modeling Framework</a> (GMF). The resulting test models are <a href="http://www.eclipse.org/modeling/emf/%20" target="_blank">Eclipse Modeling Framework</a> (EMF)  models. The ComponentBee framework supports refining more abstract test models with domain or application-specific test models.<br>
      </li>
      <li><strong>Dynamic instrumentation. </strong>The ProbeKit platform of <a href="http://www.eclipse.org/tptp/">TPTP</a> enables integrators to insert Java code fragments called <em>probes </em> into a program to collect information about its dynamic behaviour without adding test code to the actual software components. ComponentBee takes the test model as an input and generates probes to record raw log information about the input attributes and return values of methods and the states of the components to XML-based behaviour logs.<br>
      </li>
      <li><strong>Utilisation of information embedded in source code. </strong>The ComponentBee uses JDT for obtaining information about Java interfaces and classes. <strong></strong></li>
    </ul>
    <p><strong>Usability improvements:</strong></p><ul>
    <li><strong>Support for developing, debugging and running Evaluator plug-ins within one workbench. </strong>Reliability evaluation with ComponentBee requires development of  Evaluator plug-ins (by extending abstract plug-ins provided by the framework). ComponentBee package includes a supplementary component, which aims to make the development of these plug-ins more fluent. By default, Eclipse PDE requires a user to start another (run-time) instance of the Eclipse workbench before a newly implemented plug-in can be tested. However, starting a run-time workbench is a time- and memory-consuming task. Therefore, we developed an additional component which makes it possible to develop, debug and run Evaluator plug-ins within one workbench.  This component considerably increases usability of the ComponentBee framework and tools (but they can also be used without it).</li>
</ul>
    <h3>Relationship with other projects</h3>
    <h4> Dependences on other Eclipse projects</h4>
    <p>The ComponentBee framework is based on components from several Eclipse projects. The most important relationships are the following.</p>
    <ul><li>The dynamic behaviour logging of software components is based on the <a href="http://www.eclipse.org/tptp/%20" target="_blank">Eclipse TPTP</a> probes        <li><a href="http://www.eclipse.org/jdt/" target="_blank">Eclipse Java Development Tools</a> (JDT) enables the ComponentBee to access information embedded in source code of Java software components. 
	  <li>The visual editor of the ComponentBee is based on <a href="http://www.eclipse.org/gmf" target="_blank">EMF</a> and the <a href="http://www.eclipse.org/gef/" target="_blank">GEF</a>.          
    </ul>
     <h4><strong> Dependency on JavaCC</strong></h4>
     <ul>
       <li><a href="https://javacc.dev.java.net" target="_blank">Java Compiler Compiler (JavaCC)</a> is an essential part of ComponentBee. 
       <li> JavaCC is distributed under the <a href="http://www.opensource.org/licenses/bsd-license.php" target="_blank">Berkeley Software Distribution (BSD) License</a> (without the advertising clause).
       <li>ComponentBee uses JavaCC as follows:
           <ul>
             <li> first generates a BNF (Backus-Naur Form) grammar that defines production rules for the behaviours that are described in the test model</li>
               <li>then calls the JavaCC to generate a BehaviourParser for the grammar</li>
               <li>finally calls the generated BehaviourParser to extract behaviour patterns from the pre-processed raw log data</li>
           </ul>
       <li>Java CC is currenlty included in the ComponentBee installation package. This is convenient, but not strickly necessary. Users could alternatively download it from the JavaCC homepage and then configure ComponentBee to use the seperately installed library. This  would obviouly complicate deployment.</li>
     </ul>
     <h3><strong>Software architecture</strong></h3>
     <p>Components</p>
     <p>The current version of the ComponentBee consists of four main packages: </p><ul>
     <li>The <strong><em>test execution package </em></strong> provides components capable of producing information from the dynamic behaviour of components and finally writing a test report of the observed behaviours.</li>
     <li>The <strong><em>behaviour presentation package </em></strong> provides various kinds of presentation components capable of presenting dynamic behaviour of components.</li>
     <li>The <strong><em>test model package </em></strong> provides components enabling software integrators to define various kinds of test models supporting R&amp;A testing of OS components.</li>

	<li>The <strong><em>test creation package </em></strong> provides tool components to help software integrators to create test models for the R&amp;A testing. </li>
         </ul>
	 <p>In order to increase the level of modularity, the components of the packages communicate with each other via predefined interfaces.&nbsp; Each package is divided into core and implementation parts. The core part provides interfaces for the components of the package whereas the implementation part provides the reference implementations for the core interfaces. In addition, predefined extension points enable developers to build on ComponentBee.</p>
     <h4>Extension points </h4>
     <p>The ComponentBee framework provides extension points for plug-ins that can support test creation, utilisation of UML models in testing, or recording and evaluation of raw and trace data about dynamic behaviour of SW components. Two kinds of extension points exist in the ComponentBee:  static (ie. Eclipse) extension points and dynamic extension points. The Plug-in Development Environment (PDE) provides tools to create, develop, test, debug, build and deploy Eclipse plug-ins by using the static extension points. The dynamic extension points provide a mechanism to extend the ComponentBee at run-time with the plug-ins being developed in the workspace of Eclipse. The current set of extension points are listed in the following table. </p>
     <p><img src="extensions.gif"></p>
     <h2>Organization of the development work </h2>
     <h3>Code contributions</h3>
    <p>VTT Technical Research Centre of Finland is offering the current version of the ComponentBee as an initial codebase (see <a href="http://opensource.erve.vtt.fi/componentBEE/main.html">http://opensource.erve.vtt.fi/componentBEE/main.html</a>). The ComponentBee model and tools have been built in accordance with the proposed concept. The current code base consist of 22146 SLOC hand-written code (152 classes/interfaces) and a GMF-generated visual editor with 360 classes. The current code base has been made availabe under the Eclipse Public Lisence (EPL) </p>
    <h3>Proposed initial committers</h3>
    <ul>
      <li>Marko Palviainen, VTT (marko.palviainen@vtt.fi, proposed as a project administrator) </li>
      <li>Antti Evesti, VTT (antti.evesti@vtt.fi) </li>
      <li>Katja Henttonen, VTT (katja.henttonen@vtt.fi)</li>
    </ul>
    <h3>Interested parties</h3>
    <p> The following parties (from outside of VTT) have expressed interest in the ComponentBee project  and allowed using their name in this proposal:
    <ul><li>Hans Petter Dahle, 


 ICT-Norway (Hans.Petter.Dahle@fornebuconsulting.com)</li>
      <li>Mattsson Anders, Combitech (Anders.Mattsson@combitech.se)</li>
      <li>Fransisco Yuste, Telvent (francisco.yuste@telvent.com)</li>
      <li> Frank van der Linden, Philips Healthcare (frank.van.der.linden@philips.com)</li>
      <li>Carlos Acu&ntilde;a, Telefonica (carlosa@tid.es)</li>
      <li>Jos&eacute; Mar&iacute;a, Telefonica (jmpa@tid.es)</li>
      <li>Rauno Saarnio, Meritie (rauno.saarnio@meritie.fi)</li>
    </ul>
    <p>The given parties are our partners in the research project (ITEA-COSI) which has financed the  development of the ComponentBee. We have not yet tried to promote the ComponentBee tool outside of the research team.</p>
    <h3>Tentative plan</h3>
    <p>The further development of the ComponentBee tool is still on-going work. We would greatly appreciate improvement suggestions and feedback from the Eclipse community. The proposed initial contributors are there to develop the tool into the direction desired by the community. In our opinions, following improvements are needed,  for example: (1) more plug-ins (UMLImporters) capable of generating test model templates of UML models (e.g. of sequence diagrams), (2) new Eclipse editors enabling testers to define extension elements refining the test model templates for the actual reliability tests and (3) new plug-ins supporting evaluation of other quality attributes of concurrent Java programs (e.g. performance attributes). </p>
    <h2>Mentors </h2>
    <ul>
      <li>Naci Dai (<a href="mailto:naci.dai@eteration.com">naci.dai@eteration.com</a>)</li>
      <li>Bjorn Freeman-Benson (<a href="mailto:bjorn.freeman-benson@eclipse.org">bjorn.freeman-benson@eclipse.org</a>)</li>
    </ul>
    <p>&nbsp;</p>
    <p>  <br>
    </p>
    <p align="left">&nbsp;</p>
    <script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
<script type="text/javascript">
		var pageTracker = _gat._getTracker("UA-910670-2");
		pageTracker._initData();
		pageTracker._trackPageview();
		</script>
<p>
<p>
<p>
  <?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
</p>

  </div>
</div>
