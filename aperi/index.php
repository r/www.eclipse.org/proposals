<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">


<h1>Aperi Storage Management Project</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Aperi Storage Management Project");
?>

<p>This proposal is in the Project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">
Eclipse Development Process document</a>)
and is written to declare the intent and scope of a proposed
Technology PMC Project called the Aperi Storage Management Project,
or Aperi.  In addition, this proposal is written to solicit
additional participation and inputs from the Eclipse community. You
are invited to comment on and/or join the project. Please send all
feedback to the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.aperi">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.aperi
newsgroup.</a></p>

<h2>Introduction</h2>
<p>The Eclipse Platform has not only proven effective for rich client-side
applications, but it has also captured the attention of the
server-side development community.  Its extensible, layered component
model and clean interfaces are quickly evolving into a framework for
enterprise application development.  The success of Eclipse as an
enterprise application framework, however, is largely tied to the
success of the applications that exploit it.</p>

<p>Leveraging the Eclipse Platform, the Aperi project intends to develop an
extensible storage management application framework and an initial
set of exemplary, exploiting applications.  The framework will
include standards-based services for control, discovery, and
monitoring of storage resources.  Initial candidate exploiters
include file system, fabric, tape, and disk management applications.</p>

<p>Enterprise management applications face unique deployment, scalability, and
serviceability challenges (among others).  The Aperi project seeks to
overcome these challenges and further advance the enterprise
readiness of the Eclipse Platform.</p>

<h2>Background</h2>
<p>The mission of the Aperi project is to create a vendor-neutral,
open, storage management framework and to cultivate both an open-source community and an ecosystem for complementary products, capabilities, and services
around the framework to promote greater consumer choice and foster
competition. One goal of the Aperi project is to promote
interoperability, eliminate complexity and incompatibility, foster
greater opportunity for innovation, and provide a greater choice and
added-value functionality for end-users.</p>

<p>The Aperi project will fulfill its mission by:</p>
<ul>
<li>Developing a set of representative applications that use this storage management framework to enhance the availability of tools and solutions for storage management;</li>
<li>Promoting the vision of an open, standards-based, common storage management framework within the storage industry;</li>
<li>Promoting compliance of this framework to the latest SMI-S standard specification and other appropriate standards;</li>
<li>Providing test suites for compatibility verification of the framework and vendor-developed applications to help ensure multi-vendor application interoperability;</li>
<li>Fostering projects that enhance or extend the framework and promote open standards; </li>
<li>Enhancing the reach of the technology by recruiting companies to join the community.</li>
</ul>

<p>
The scope of Aperi covers the creation of:</p>
<ul>
<li>The Aperi Storage Management Framework architecture including the
	discovery of host, storage, and infrastructure components through
	SMI-S; maintenance of a coordinated database; a set of services
	including configuration, event and performance management; and to
	formalize the interfaces between the applications and the storage
	management framework.</li>
<li>The Aperi software project:
<ul>
<li>An open source implementation of the Storage Management Framework;</li>
<li>Implementations of representative applications that utilize the framework and deliver end-user functionality for enhanced storage management;</li>
<li>Additional functionality in the storage management framework.</li>
</ul></li>
</ul>

<h2>Architecture</h2>
<p>The Aperi runtime environment consists of four process types:</p>
<ul>
<li>Management Server</li>
<li>Management Console (GUI)</li>
<li>Host Agent (Endpoints)</li>
<li>Repository</li>
</ul>

<p>Each process type extends the Eclipse Platform. Standards are leveraged
whenever possible, including storage industry standards like SMI-S. 
Other communication standards like Web Services and SSL are also
included.  Extensibility, via the Eclipse extension registry is
implemented throughout Aperi. Aperi&rsquo;s commitment to open
standards, an extensible component model, and component layering
permits deployment as a standalone application or consumption &amp;
customization by various applications vendors.</p>

<IMG SRC="Aperi_Eclipse_Project_Proposal_v1.7_html_m3ea1116a.gif">

<h3>Management Server</h3>
<p>The management server maintains two functional layers:</p>
<ul>
<li>Aperi Framework Layer</li>
<li>Aperi Applications Layer</li>
</ul>

<p>Between the two layers exists the application-framework contract, a
collection of well-defined interfaces &ndash; a potential standard.</p>

<p>The Framework Layer is based on technologies from the Eclipse Platform
such as the OSGi implementation, the plug-in model, and the Extension
Registry, and extends these technologies to include services that are
fundamental to storage management, such as</p>
<ul>
<li>Discovery</li>
<li>Monitoring</li>
<li>Control</li>
<li>Alert management</li>
<li>Database interface</li>
</ul>

<p>The Applications Layer consists of collections of features that provide
coherent functionality in a specific area to users. The
&lsquo;applications&rsquo; are provided as plug-ins to the Framework
Layer. The basic Aperi project will include applications that provide
basic functionality in key functional areas for storage management:</p>
<ul>
<li>Storage subsystem management</li>
<li>Tape library management</li>
<li>Storage Area Network fabric management</li>
<li>Basic file system capacity reporting</li>
</ul>

<p>These
applications provide a useful set of fundamental storage management
functions, and are a base that can be extended to produce more
advanced storage management applications.</p>

<h3>Host Agent</h3>
<p>Aperi uses agent-less management wherever possible, but can use a variety
of agents to extend its capabilities. Foremost among these are CIMOMs
implementing the SMI standard for storage management
(http://www.snia.org/smi/home), which allow Aperi to manage storage
hardware devices. The Aperi framework does not include SMI agents;
instead, SMI agents are used as discovered in the managed
environment. However, Aperi does include a host-based agent
framework, and two subagents that run in this framework. The 'File
system' subagent is deployed on hosts to provide reporting and
discovery services for file systems visible to each host. The
'Fabric' subagent is deployed on hosts attached to Fibre Channel
Storage Area Networks (SANs) and uses Fibre Channel protocols to
provide additional discovery and management capabilities for SAN
fabrics. The overall host agent framework is extensible via the
Eclipse plug-in model so that additional subagents can be deployed in
a single instance of the framework. The intention is not for Aperi to
force a particular agent infrastructure on its users, but to be
flexible about using what is discovered in the user's environment.</p>

<h3>GUI Console</h3>
<p>A shared, extensible GUI for storage management is an essential
component of the Aperi project.  We intend to evolve the user
interface over time to an Eclipse Rich Client Platform (RCP)
application, to better exploit the Eclipse extensibility model on the
client side and to have a consistent extensibility model between the
client and the server. The Aperi project will be seeded with a
complete GUI covering all functions included in the initial donation
to Aperi. This GUI is developed using Swing and includes secure
login, role-based authorization, reporting and charting (using the
Eclipse Chart Engine), and a graphical topology viewer for displaying
storage area networks. The evolution of the GUI to a full
implementation of the Eclipse RCP will likely center on the Aperi
topology viewer and the BIRT reporting engine. There may also be
interest from the community in developing alternative user
interfaces, for example a CLI or web-based interface.</p>

<h3>Data Repository</h3>
<p>Aperi's database access routines are not specific to a particular RDBMS and
it is expected that software vendors could provide support for
commercial RDBMS used with Aperi.  Aperi defines extension points for
database integration.  An initial implementation of this extension
includes Derby &ndash; a pure Java, open source RDBMS.</p>

<p>The initial Aperi framework contains its own database access layer and
its own extension points for connecting to alternative RDBMS.  We are
investigating the Eclipse Data Tools Project to see how we can
transition to that framework.</p>

<h3>Extensibility</h3>
<p>The Aperi framework is intended to provide a fundamental level of storage
management functionality to its users. These capabilities are useful
in themselves for discovering, monitoring, and basic management
needs, but are even more useful as building blocks for constructing
higher-level management functions. We expect users to extend the
Aperi framework at each layer of the architecture (GUI, server, and
agent).  By using the Eclipse plug-in model, higher-level management
functions can extend the framework vertically. Users can also extend
the framework horizontally.  Examples of vertical add-ons include new
features like performance management.  Examples of horizontal add-ons
include the broadening of existing features (ex: adding a new
discovery protocol to the discovery feature).  We expect that some of
these areas may be filled by open source offerings contributed to
Aperi in the future or by extensions that are developed and sold
commercially. Our intention is to provide a common platform for
storage management that allows many partners to come together and
extend, creating a richer offering for users than any single vendor
could provide on its own.</p>

<h2>Organization</h2>
<p>Equinox, Corona, and Enterprise Component Framework are projects intersecting
&ldquo;Eclipse as an enterprise platform&rdquo; and &ldquo;Eclipse on
the server.&rdquo;  We will communicate with these projects and any
others identified as within the Aperi scope.</p>

<p>The Aperi project will initially be a sub-project of the Technology
Project.</p>

<h3>Interested Parties and Committers</h3>
<p>This section captures the list of organizations that have expressed
interest in the project and/or its components, and as such will be
updated periodically to reflect the growing interest in this
project.</p>
<p>Rather than canvas the Eclipse community at large,
the submitters of this proposal welcome interested parties to post to
the eclipse.technology.aperi newsgroup and ask to be added to the list as interested parties or to suggest changes to this document.</p>

<h3>Initial Set of Committers </h3>
<TABLE WIDTH=299 BORDER=1 BORDERCOLOR="#000000" CELLPADDING=7 CELLSPACING=0><COL WIDTH=283>
<TR>
<TD WIDTH=283 HEIGHT=4 VALIGN=TOP><SPAN LANG="es-ES">
CA
</SPAN><U>
<A HREF="http://ca.com/"><SPAN LANG="es-ES">http://ca.com/</SPAN></A></U>
</TD></TR>

<TR>
<TD WIDTH=283 HEIGHT=4 VALIGN=TOP>
<SPAN LANG="es-ES">
Emulex
</SPAN><U><A HREF="http://www.emulex.com/"><SPAN LANG="es-ES">http://www.emulex.com/</SPAN></A></U>
</TD></TR>

<TR>
<TD WIDTH=283 HEIGHT=4 VALIGN=TOP>
<SPAN LANG="es-ES">
Fujitsu
</SPAN><A HREF="http://www.fujitsu.com/"><U><SPAN LANG="es-ES">http://www.fujitsu.com/</SPAN></U></A>
</TD></TR>

<TR>
<TD WIDTH=283 HEIGHT=4 VALIGN=TOP>
<SPAN LANG="es-ES">
IBM
</SPAN><A HREF="http://www.ibm.com/"><U><SPAN LANG="es-ES">http://www.ibm.com/</SPAN></U></A>
</TD></TR>

<TR>
<TD WIDTH=283 HEIGHT=4 VALIGN=TOP>
<SPAN LANG="it-IT">
McDATA
</SPAN><U><A HREF="http://www.mcdata.com/"><SPAN LANG="it-IT">http://www.mcdata.com/</SPAN></A></U>		
</TD></TR>

<TR>
<TD WIDTH=283 HEIGHT=3 VALIGN=TOP>
NetApp
<U><A HREF="http://www.netapp.com/">http://www.netapp.com/</A></U>
</TD></TR>

</TABLE>

<h3>Interested Parties</h3> 
<TABLE WIDTH=299 BORDER=1 BORDERCOLOR="#000000" CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=283>
<TR>
<TD WIDTH=283 HEIGHT=4 VALIGN=TOP>
<SPAN LANG="es-ES">
Brocade
</SPAN><U><A HREF="http://www.brocade.com/"><SPAN LANG="es-ES">http://www.brocade.com/</SPAN></A></U>
</TD></TR>

<TR>
<TD WIDTH=283 HEIGHT=4 VALIGN=TOP>
<SPAN LANG="es-ES">
Cisco
</SPAN><U><A HREF="http://www.cisco.com/"><SPAN LANG="es-ES">http://www.cisco.com/</SPAN></A></U>
</TD></TR>

<TR>
<TD WIDTH=283 HEIGHT=5 VALIGN=TOP>
LSI Logic (Engenio Storage Group)
<U><A HREF="http://www.engenio.com/"><SPAN LANG="es-ES">http://www.engenio.com/</SPAN></A></U>
</TD></TR>

<TR>
<TD WIDTH=283 HEIGHT=3 VALIGN=TOP>
Novell
<U><A HREF="http://www.novell.com/">http://www.novell.com/</A></U>
</TD></TR>
</TABLE>
<p>
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
