/*******************************************************************************
 * Copyright (c) 2009 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/

The "proposal-template" directory, when changed, 
needs to be zipped up into the proposal-template.zip file.

A handy Ant script, build.xml, will take care of this. After running
the script, you need to refresh the directory and then do the CVS
check-in.

We only ever give out the URL to the ZIP file.