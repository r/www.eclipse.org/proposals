<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Proposal for Eclipse Learning Project</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Learning Project");
?>

<!-- Start of content -->
<h2>Introduction</h2>
<p>
The Eclipse Learning Project is a proposed open source project under the
<a href="http://www.eclipse.org/technology/">Eclipse Technology Project</a>.
This proposal is in the Project Proposal Phase (as defined in the
<a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development 
Process document</a>) and is written to declare its intent and 
scope. This proposal is written to solicit additional participation and input 
from the Eclipse community. You are invited to comment on and/or join the 
project. Please send all feedback to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.learning">
eclipse.learning newsgroup</a>.
</p>

<h2>Background</h2>
<p>
As the Eclipse community has grown quickly there is a need to ensure that
knowledge is easily accessible and credible signals regarding skills exist in
order to ensure and foster further growth. The goal is to solve the skills
requirement while enabling the commercial ecosystem to achieve revenues from
training and skills certification.
</p>
<p>
The <a href="http://www.eclipse.org/examples/">Eclipse Examples Project</a>
contains technical examples. We believe it will be beneficial for the Eclipse
ecosystem to amend the Eclipse Examples Project with accompanying projects to
provide the Eclipse ecosystem with courseware.
</p>
<p>
The <a href="http://www.eclipse-training.net/">Eclipse Training Alliance</a>
(driven by WeigleWilczek) has become an active community which successfully
collaborates on training materials. We believe that this community can be
motivated to collaborate in the more open frame of the Eclipse Learning Project.
</p>

<h2>Scope</h2>
<p>
The Eclipse Learning Project is targeted to provide materials which help to
acquire Eclipse knowledge. This will support learning as well as skills 
certification. Thus, the Eclipse Learning Project will host
</p>
<ul>
<li>introductions (e.g. a web based quick start to X, videos of a course on X)</li>
<li>courseware made for training (e.g. slides, text, programming tasks)</li>
<li>sample solutions which support the courseware (code)</li>
<li>methods and tasks for skills certification for each course</li>
</ul>
<p>
for all technological Eclipse projects.
</p>


<h2>Description</h2>
<p>
The Eclipse Training Alliance has shown that collaboration on courseware is
feasible. We want use this experience to form an Eclipse based community which
collaborates on all forms of learning materials.</p>
<p>
Collaboration on learning materials should follow the same principles as open
source software development. The Eclipse Learning Project will be subdivided
into topics, e.g. OSGi/Equinox, which contain one or more courses, e.g. a
"Basic OSGi/Equinox course" and an "Enterprise OSGi/Equinox course". Each topic
should be directed by one individual to ensure quality, keep content up-to-date
and motivate, guide and grow the community. There should be committers and
contributors to the materials. </p>
<p>
Skills certification lives from credibility. Skill certificates have to be
reliable over time: A "Certified Eclipse RCP Devloper for Eclipse 3.4" from 2008
should imply the same qualification as "Certified Eclipse RCP Developer for
Eclipse 3.x" from 2010, just for different Eclipse versions. We suggest to form
a group of experts to design methods, tools and tasks to certify skills of
developers. This group of experts will define different skills certifications
and their requirements. New releases of the underlying technology will require
an update of the corresponding skills certification. Skills certification should
also correspond to courses. Eclipse Members should be able to use these methods,
tools and tasks to certify developers. Since credible skills certification
requires outstanding expertise in the topic, a model will be developed how
companies or persons who will be able to certify can be selected.
</p>

<h2>Values</h2>
<ol>
<li>Eclipse learning materials will be publicly available as open source</li>
<li>Methods for skills certification will exist</li>
</ol>

<h2>Organisation</h2>

<h3>Mentors</h3>
<ul>
<li>Wayne Beaton</li>
<li>NN</li>
</ul>

<h3>Initial committers</h3>
<p>
The initial committers will focus on providing the aforementioned courseware and
ensuring that the project values have been applied. They will further focus on
identifying other content that is appropriate for inclusion in the project as
well as keeping the existing materials up-to-date. Additionally they will be
responsible to form the aforementioned group of experts.
</p>
<ul>
<li>Dr. J&ouml;rn Weigle (weigle@weiglewilczek.com): Project lead, committer</li>
<li>Naci Dai (naci.dai@eteration.com): Project lead, committer</li>
<li>Heiko Seeberger (seeberger@weiglewilczek.com): Committer</li>
<li>Alexander Thurow (thurow@weiglewilczek.com): Committer</li>
</ul>

<h3>Interested parties</h3>
<ul>
<li><a href="http://www.weiglewilczek.com">WeigleWilczek</a></li>
<li><a href="http://www.eteration.com">Eteration</a></li>
<li><a href="http://www.industrial-tsi.com">Industrial TSI</a></li>
</ul>

<h2>Tentative Plan</h2>
<p>
[Assumption: Project creation 12-2008]
</p>
<ul>
<li>12-2008: Commit of courseware for basic Eclipse RCP training</li>
<li>01-2009: First commit of courseware for basic OSGi/Equinox training</li>
<li>01-2009: Creation of skills certification working group</li>
<li>06-2009: Certification model ready</li>
</ul>
<!-- End of content -->

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
