<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Rich Ajax Platform (RAP) Incubator";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>RAP Incubator</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Rich Ajax Platform (RAP) Incubator");
?>

<p>
<div>
  <h2>
    Introduction
  </h2>
  The RAP Incubator is a proposed open source project under the RAP Project.<br>
  <br>
  This proposal is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the <a href=http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.rap id=d7qt title=http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.rap>http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.rap</a> newsgroup.<br>
  <h2>
    Background
  </h2>
  The goal of the RAP Incubator project is to experiment with alternative ideas and technologies for enhancing and complementing RAP.
  For example, the evaluation of alternative client technologies for RAP and the integration with other innovative technologies, are possible areas of investigation for the RAP Incubator.
  In addition to this, the Incubator is also intended to serve as a forum for interested third parties and a place for the development of additional components and tools for RAP outside of the restrictions of the RAP project itself.
  This includes the development of additional custom widgets and components.
  Incubator components are intended to stay in the Incubator until they are mature enough to be integrated into the main development stream.
  <h2>
    Initial Work Areas
  </h2>
  <h3>
    RAP Theme Editor
  </h3>
  The RAP Theme Editor <a href="http://wiki.eclipse.org/RAP_Theme_Editor">[1]</a> is a multi-page CSS editor to assist with the creation of RAP theme files. It has been developed by Mathias Sch&auml;ffner during 
  the Google Summer of Code 2008 <a href="http://wiki.eclipse.org/Google_Summer_of_Code_2008">[2]</a>.
  <h3>
    GCCanvas Widget
  </h3>
  GCCanvas is a custom RWT widget that allows drawing operations inside a browser. It has been contributed by Mirko Solazzi <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=261355">[3]</a>.
  <h3>
  Google Visualization widgets for RAP
	</h3>
David Donohue developed ten 
new widgets for RAP that use the Google Visualization API. The source code 
is currently hosted 
at <a href="http://code.google.com/p/inqle/source/browse/#svn/trunk/org.inqle.ui.google.widgets">[4]</a>.
  
  <h2>
    Out of Scope
  </h2>
  Work in the RAP Incubator will be constrained to only those efforts that we expect to graduate (i.e. the code will eventually become part of RAP). It is not a playground for arbitrary development efforts. In addition, we should ensure that investment in the incubator never leaves the community with the perception that it is coming at the cost of important work on the RAP project.
  <h2>
    Interested Parties
  </h2>
  <ul>
    <li>
      EclipseSource
    </li>
    <li>
      CAS
    </li>
  </ul>
</div>
<h2>
  Mentors
</h2>
<ul>
  <li>
    R&uuml;diger Herrmann, EclipseSource
  </li>
  <li>
    Ralf Sternberg, EclipseSource
  </li>
  <li>
  Chris Aniszczyk, EclipseSource
  </li>
</ul>
<h2>
  Initial Committers
</h2>
<p>
  The initial committers will be all committers on the current incubator components. Commit rights will be handled on a component-by-component basis.
</p>
<ul>
  <li>
    Frank Appel
  </li>
  <li>
    Istvan Ballok, CAS Software
  </li>
  <li>
    Jordi B&ouml;hme L&oacute;pez, EclipseSource
  </li>
  <li>
    Ivan Furnadjiev, EclipseSource
  </li>
  <li>
    R&uuml;diger Herrmann, EclipseSource
  </li>
  <li>
    Jochen Krause, EclipseSource
  </li>
  <li>
    Benjamin Muskalla, EclipseSource
  </li>
  <li>
    Joel Oliveira, Critical Software
  </li>
  <li>
    Stefan R&ouml;ck, CAS Software
  </li>
  <li>
    Ralf Sternberg, EclipseSource
  </li>
  <li>
    Elias Volanakis, EclipseSource
  </li>
  <li>
    Mathias Sch&auml;ffner
  </li>
  <li>
    Mirko Solazzi
  </li>
  <li>David Donohue <em>Added on June 5, 2009</em></li>
  <li>Holger Staudacher <em>Added on June 8, 2009</em></li>
</ul>
<div>
  <hr size=2><br>
  [1] http://wiki.eclipse.org/RAP_Theme_Editor<br>
  [2] http://wiki.eclipse.org/Google_Summer_of_Code_2008<br>
  [3] https://bugs.eclipse.org/bugs/show_bug.cgi?id=261355<br>
  [4] http://code.google.com/p/inqle/source/browse/#svn/trunk/org.inqle.ui.google.widgets
  <br>
</div>
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
