<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1>Javascript for EMF (JS4EMF)</h1>

</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Javascript for EMF (JS4EMF)");
?>

<h2>Introduction</h2>
<p>Javascript for EMF is a proposed open source component under the
Eclipse Modeling Framework Technology for integrating scripting with
Javascript with EMF. It is in the Project Proposal Phase (as defined in
the Eclipse Development Process document) and is written to declare its
intent and scope. This proposal is written to solicit additional
participation and input from the Eclipse community. You are invited to
comment on and/or join the project. Please send all feedback to the <a
	href="http://eclipse.org/forums/eclipse.technology.emft">EMFT forum</a>
with <b>[JS4EMF]</b> as a prefix of the subject line.</p>

<h2>Background</h2>

<p>Scripting of applications is commonly used to describe the use of a
second programming language (the scripting language) for parts of an
application. The scripting language is usually interpreted and well
integrated with the application objects, both in the business layer and
the user interface. Usually the scripted part is considered layered on a
core, but more interleaved models are possible. A scripting language can
be internal, i.e. used only within the application by its developers, or
external, i.e. exposed to the users and used for extending the
application. In both cases, the use of the scripting language gives
added flexibility, since many changes to the application can be made by
replacing or adding scripts and without redeploying the whole
application. This is also an advantage during the initial development,
since interpreted scripts can be modified and executed without a costly
edit-compile-build-run cycle.</p>

<p>EMF currently has a Java API for both static and reflective access to
Ecore objects, resources, viewers, commands etc, making it possible to
create complex EMF-based applications with Java. Modeling and editing
EMF data can be done within the development environment, by using
so-called dynamic EMF objects. However, adding behavior to EMF objects
and adapting the user interface, still requires "static" Java code.
Hence, to develop and test an EMF application you still need to compile,
build and (re)launch Eclipse. Using a scripting language like Javascript
can provide a more flexible development model, where operations and
commands can be programmed without the edit-compile-build-run cycle.</p>

<h2>Scope</h2>

<p>This project will seek to integrate Javascript with all parts of EMF
where it makes sense. It will initially support manipulating Ecore
objects (static and dynamic) with Javascript, validating by means of
constraints and invariants, implementing operations and executing
Javascript code as editing commands. Other opportunities will then be
explored, like extending the editors, computed databinding, etc. The
long term goal is to enable developing complete EMF applications with
Javascript based on EMF's dynamic capabilities, while allowing the
developer to decide where the split between Java and Javascript should
be.</p>
<h2>Initial contribution</h2>

<p>Code has already been contributed to the e4 project, in the context
of the Toolkit Model project. But since it does not depend on e4, and
should be generally useful as part of EMFT, the code will be
appropriately refactored and moved. See <a
	href="http://wiki.eclipse.org/E4/UI/Toolkit_Model/org.eclipse.e4.emf.javascript">http://wiki.eclipse.org/E4/UI/Toolkit_Model/org.eclipse.e4.emf.javascript</a>
for a description.</p>

<h2>Relationship with Other Eclipse Projects/Components</h2>

<p>The code depends on various parts of the EMF API.</p>

<h2>Third party libraries</h2>

<ul>
	<li>The code uses Mozilla's Rhino Javascript engine, and
	implements/extends various interfaces/classes from its library.</li>
</ul>

<h2>Organization</h2>

<h3>Committers</h3>

<ul>
	<li>Hallvard Tr&aelig;tteberg (Norwegian University of Science and
	Technology).</li>
	<li>Gilles Vanwormhoudt (Telecom Lille 1)</li>
</ul>

<h3>Mentors</h3>

<ul>
	<li>C&eacute;dric Brun</li>
	<li>Chris Aniszczyk</li>
</ul>

<h3>Interested Parties</h3>

The e4 contribution has not been heavily marketed, but after the ESE
2009 presentation of TM, there has been some questions about making it
available outside TM and e4.</div>
</div>
<?php
$html = ob_get_contents();
ob_end_clean();

# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
