<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Proposal for Rich AJAX Platform (RAP)";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
      <div id="midcolumn">
        <h1>
          Proposal for Rich AJAX Platform (RAP)
        </h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Rich AJAX Platform (RAP)");
?>
<html>
<h2>Introduction </h2>
<p>The Rich AJAX Platform RAP Project is a proposed open source project under
the <a href="/technology/">Eclipse Technology Project</a>. </p>
<p>This proposal is in the Project Proposal Phase (as defined in the Eclipse
Development Process document) and is written to declare its intent and scope.
This proposal is written to solicit additional participation and input from the
Eclipse community. You are invited to comment on and/or join the project. Please
send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology</a>
newsgroup. </p>
<h2>Background </h2>
<p>With the recent ubiquity of AJAX technologies, web applications can provide
the user experience end-users have demanded for a long time. Rich Client and web
applications are converging with regard to functionality and usability - web
applications are becoming &quot;Rich&quot; with respect to UI capabilities, Rich
Client applications are offering better deployment and management functionality.</p>
<p>Eclipse has successfully entered the Rich Client world with its Rich Client
Platform (RCP). RCP offers a flexible, extensible platform that is
operating-system-independent while providing a user experience that is
consistent with that of the native operating system. RCP also offers built in
functionality to extend and update client installations.</p>
<p><b>The RAP project aims to enable developers to build rich, AJAX-enabled Web
applications by using the Eclipse development model, plug-ins and a Java-only
API.</b></p>
<p>See <a href="http://rap.innoopract.com/webworkbench" target="_blank">
http://rap.innoopract.com/webworkbench</a> 
for a demonstration of the usability and look and feel of such an application.</p>
<p>RAP will offer the ease of development of Eclipse, Eclipse extensibility and
user experience by reusing Eclipse technology for distributed applications and
by encapsulating AJAX technologies into simple-to-use Java components (as SWT
encapsulates native widgets).</p>
<p>Web applications remain the appropriate approach for many usage scenarios,
e.g. for applications with zero-install access requirements. With centralized
installation and administration, web applications offer low administration and
production costs. Furthermore, they can provide high scalability and good
performance.</p>
<p>But associated with the classic approach of HTML-based web applications is a
higher expenditure of resources during the development and maintenance process
due to the technology mix that is involved. With AJAX this becomes an even
bigger problem.</p>
<p>By encapsulating the AJAX technologies in a component library one major road
block to a more efficient development model can be eliminated. By offering a
plugin model on the server and providing Eclipse workbench UI capabilities
developers can leverage their experience with Rich Client Applications and
profit from the proven Eclipse development model to create Rich Internet
Applications in a streamlined fashion.</p>
<p>With the proposed Rich AJAX Platform Eclipse enters the Area of Rich Internet
Applications.</p>
<h2>Scope </h2>
<p>The objectives of the RAP project are to:</p>
<ul>
  <li><b>Enable the development of Rich Internet Applications that are based on
    the Eclipse Plugin architecture.</b> The Eclipse OSGi framework (Equinox)
    can run inside of a Web application. This has been demonstrated by several
    parties, and a subproject of the Equinox project has already been
    established (see <a href="http://www.eclipse.org/equinox/incubator/server/">http://www.eclipse.org/equinox/incubator/server/</a>)�</li>
  <li><b>Enable AJAX UI development based on a Java component library. </b>For
    enabling UI development based on a Java component library the project will
    receive a contribution from Innoopract (W4Toolkit), which provides a rich
    set of UI components, event-driven program control, a generic mechanism for
    updating arbitrary UI elements of a web UI based on AJAX, and a Lifecycle
    handling comparable to the one used in Java Server Faces. Furthermore the
    component library offers a mechanism for browser detection and can adapt to
    the capabilities of browsers by using rendering kits (NoScript, Script,
    AJAX). The API of the component library is not identical to the SWT API for
    historical reasons, but at the MVC level the API is very close to the JFace
    API. It is a goal to achieve the furthest possible conformance with SWT /
    JFace.<b>Udate:</b>Based on the feedback from committers and interested parties
    we are actively researching to adopt a full SWT API</b></li>
  <li><b>Provide a Web Workbench similar to the Eclipse platform workbench. </b>As
    a third building block for Eclipse-based Web applications, a Web Workbench:
    <ul>
      <li>provides selection service (with session scope),</li>
      <li>provides extension points for action sets, workbench parts,
        perspectives, preference pages, etc.,</li>
      <li>enables plug-ins to contribute to workbench parts provided by other
        plug-ins (e.g. action contributions)</li>
    </ul>
  </li>
  <li><b>Align RAP API with the Eclipse platform API</b>, work with Eclipse
    teams to provide consistency for RCP and RAP.</li>
  <li><b>Work on defining the right split between Plugins with session scope vs.
    application scope. </b>It is not always obvious if a specific plug-in
    belongs to the application or user (session) scope, e.g. preferences might
    be shared between all users of an application (and only administrators could
    change them) or could be defined (and persisted) per user (this will require
    authentication).. </li>
</ul>
<h2>FAQ</h2>
<h3>What is the goal of RAP?</h3>
<p>It is about enabling programmers to develop powerful, AJAX Web applications
&quot;the eclipse way&quot; - using the plug-in mechanism, a java widget toolkit
and workbench services. - without having to actually hand-code AJAX.</p>
<h3>Where does RAP play?</h3>
<p>RAP addresses the same kind of applications like RCP, and complements RCP
technology with respect to serving as a thin client for distributed
applications. RAP is targeted to enterprise applications and can be employed
independently of RCP.</p>
<h3>How does RAP compare to RCP?</h3>
<div align="left">
  <table border="1" cellspacing="0" cellpadding="3">
    <tr>
      <td>&nbsp;</td>
      <td>RCP</td>
      <td>RAP</td>
    </tr>
    <tr>
      <td>Plugin Model</td>
      <td>OSGi</td>
      <td>OSGi on the server (inside a webapp)</td>
    </tr>
    <tr>
      <td>Widget Toolkit</td>
      <td>SWT</td>
      <td>WWW Widget Toolkit (W4T)</td>
    </tr>
    <tr>
      <td>Model View Controller</td>
      <td>JFace</td>
      <td>JFace (adapted, as standard JFace has references to SWT)</td>
    </tr>
    <tr>
      <td>UI Framework</td>
      <td>Workbench</td>
      <td>Web Workbench</td>
    </tr>
  </table>
</div>
<h3>How does RAP compare to the Google Webtoolkit?</h3>
<p>
The development model of both toolkits is very similar. There are two main differences: 
<ol>
<li>GWT is running on an emulated java engine in the browser, RAP is mainly running on the serverside and only smartly refreshing the client by using AJAX. As a result eventhandling in GWT is on the client side (+ possible RPC calls to the server), whereas RAP relays most client-side events to the server for processing (solely ui related events can be processed on the client). </li>
<li>With RAP being mainly server side it can access the full java api and make use of OSGi inside a web container, enabling the full usage of the eclipse plugin model. Applications can contribute to a common workbench and to other plugins. It is not easy to imagine that this can be ported to run in a browser. </li>
</ol>
In essence, RAP enables a RCP-comparable approach for web applications, GWT enables a &quot;standalone SWT&quot; comparable approach. 
</p>
<h3>Does RAP enable my RCP applications to run on the Web without any changes?</h3>
<p>The project aims for enabling as much reuse of RCP application components as
possible. However, for the first release of RAP a part of the UI of existing RCP
applications needs to be rewritten (which means that core plug-ins can be
reused, as long as they are multi-user capable). It shall be possible to reuse
Content and Labelproviders.</p>
<h3>How are RAP and ATF related?</h3>
<p>RAP is a project focusing on an AJAX web runtime, Ajax Toolkit Framework (ATF
http://eclipse.org/proposals/atf) focuses on tools to write applications for
AJAX runtimes. ATP can be used to build components (widgets) for RAP, as RAP is
encapsulating HTML, CSS and JavaScript into java components.</p>
<h3>Why don't you use JSF as component framework?</h3>
<p>Because it is not possible - yet. JSF does currently not offer a generic
support for AJAX, and although it is possible to use JSF with Java components
only, there is no implementation available right now.</p>
<p>We believe that the core achievement of JSF is its Lifecyle handling. W4T
lifecycle handling is very closely aligned with the JSF lifecycle handling, and
it will be possible to move to JSF in the future.</p>
<h3>Can RAP be extended?</h3>
<p>Eclipse is about &quot;extensible platforms and exemplary tools&quot; - and
we take that seriously. The widget library can be extended with more widgets,
much the same way as SWT requires knowledge of the &quot;native&quot; (meaning
AJAX) technologies. As the Web Workbench will be built on the plug-in model, it
is extensible by design. </p>
<h2>Organization </h2>
<h3>Initial committers </h3>
<p>The initial committers will focus on evolving and hardening the RAP model by
delivering support for Java development. Our agile development process will
follow eclipse.org's standards for openness and transparency. Our goal is to
provide the infrastructure and APIs needed to integrate task and
degree-of-interest-centric UIs to any Eclipse plug-ins whose user-experience
information overload. As such we will actively encourage contributions to RAP.
We also plan to help improve the Eclipse platform by submitting patches and
extension point suggestions. The initial committers are:</p>
<ul>
  <li>Jochen Krause (Innoopract): project lead</li>
  <li>Frank Appel (Innoopract): lead architect</li>
  <li>R�diger Herrmann (Innoopract): AJAX enablement </li>
</ul>
<h3>Code Contribution </h3>
<p>Innoopract will make an initial code contribution that will encompass some of
the core functionality for a Rich AJAX Platform including: &quot; a rich set of
UI components with a Java API &quot; event-driven program control &quot; a
generic mechanism for updating arbitrary UI elements of a web UI based on AJAX
&quot; a Lifecycle handling comparable to the one used in Java Server Faces
&quot; a mechanism for browser detection &quot; rendering kits that enable to
adapt to browser capabilities (NoScript, Script, AJAX). Developer community We
expect to extend the initial set of committers by actively supporting a
developer community. Our goal is to have the initial developer community harden
the core RAP functionality. As most of our planned API shall be identical to
existing Eclipse API, we will make it easy for interested parties to take an
active role in the project by making our planning and process transparent and
remotely accessible. </p>

<h3>Interested parties</h3>
The following parties have expressed interest extending the platform, contributing ideas, guidance and discussion. Key contacts listed.
<ul>
<li>iMedic - Stefan Wilczek</li>
<li>Rahul Mehrotra</li>
<li>Gunnar Wagenknecht</li>
</ul>
<h3>User community </h3>
<p>RAP is a new technology that promises to bring the Eclipse user interface and
interaction model to web applications. As such, supporting and soliciting
feedback from a large user community of developers is critical to creating a
scalable platform and usable UIs. We plan on doing this by using the standard
eclipse.org mechanisms of supporting an open project and community of early
adopters. </p>
<h2>Tentative Plan </h2>
<p>2006-06 v0.1: Java component library for UI development <br>
2006-09 v0.2: Basic WebWorkbench implementation, OSGi running exemplary inside
web applications on selected open source servers <br>
2007-02 v0.5: Provide all API for Release 1.0 <br>
2007-06 v1.0: Release 1.0</p>

      </div>
    </div>
   
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
