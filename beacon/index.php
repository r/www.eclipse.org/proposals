<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>The Process Framework (EPF) Project &lt;Beacon&gt;</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Process Framework (Beacon)");
?>
<h2>Introduction</h2>
<p>The PROCESS FRAMEWORK PROJECT is a proposed open source project under the <a href="/technology/">Eclipse 
  Technology Project</a>.</p>
<p> This proposal is in the Project Proposal Phase (as defined in the Eclipse 
  Development Process document) and is written to declare its intent and scope. 
  This proposal is written to solicit additional participation and input from 
  the Eclipse community. You are invited to comment on and/or join the project. 
  Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology</a>
  newsgroup.<br>
</p>
<h2>Background</h2>
Throughout the software industry, there are a lot of great ideas and knowledge 
about how to effectively develop software. This knowledge may be centered around 
1) technologies, such as J2EE, .NET, and various tool environments; 2) various 
specialty domains, such as how to do iterative and agile development, how to build 
secure software, how to best leverage service-oriented architectures, or how to 
do distributed development; and 3) various vertical industry knowledge, such as 
how to deal with straight-through processing in the financial world, or how to 
build embedded systems for the auto industry. 
<p>The sources for all of these great ideas and knowledge include organizations doing software development, open source communities, the agile community, <a href="http://www.sei.cmu.edu/collaborating/spins/">Software Process Improvement Networks</a> (SPIN groups), product and technology companies, academia and software research groups, such as <a href="http://www.sei.cmu.edu/">SEI Carnegie Mellon</a>, <a href="http://www.ubc.ca/">University of British Columbia</a>, and <a href="http://sunset.usc.edu/">USC Center for Software Engineering</a>, a variety of methodologists and companies capturing industry best practices into various knowledge bases, literature, and processes. The problems with these process assets are:</p>
<ul>
  <li> <strong>Integration</strong>: When different media, notations, language, 
    and terminology are used to express process assets, integration of the process 
    assets becomes difficult to achieve. Furthermore, lack of standard interfaces 
    makes it hard to integrate these assets with; project & portfolio management 
    tools, such as <a href="http://www-306.ibm.com/software/awdtools/portfolio/">IBM 
    Rational Portfolio Manager</a>, <a href="http://www.rallydev.com">Rally's 
    Agile Management Application</a> or <a href="http://www.microsoft.com/office/project/prodinfo/default.mspx">Microsoft 
    Project</a>; process management and support tools, such as <a href="http://www.jaczone.com/product/demonstration/">WayPointer</a> 
    or <a href="http://www.osellus.com/products/iris_suite/iris_suite.html">IRIS</a>; 
    or development environments such as the Eclipse platform.</li>
  <li> <strong>Redundant and Isolated Work:</strong> When process assets are developed 
    with limited collaboration between different groups, some groups may take 
    part in redundant work, reinventing the wheel rather than adding value to 
    preexisting work or failing to leverage ideas towards accelerating innovation.</li>
  <li> <strong>Flexibility and Communication</strong>: When process assets are 
    developed without a framework that allows for integration and customization, 
    they may not be effectively communicated for a specific project or organization. 
  </li>
</ul>

<h2>Description</h2>
		
<p><strong><font size="3">Project Goals</font></strong>
<p align="left"> There are two goals of the Process Framework Project: 
<ul>
  <li>To provide an <strong>extensible framework and exemplary tools for software 
    process engineering </strong> - method and process authoring, library management, 
    configuring and publishing a process.</li>
  <li> To provide <strong>exemplary and extensible process content</strong> for 
    a range of software development and management processes supporting iterative, 
    agile, and incremental development, and applicable to a broad set of development 
    platforms and applications </li>
</ul>

<p><strong>Extensible software process engineering framework</strong>

<p>The framework consists of the following two components:
<ul>
  <li> <strong>Meta-model</strong>: Method content and processes will be structured 
    based on a formal meta-model. This meta-model will be documented with a comprehensive 
    meta-model specification using MOF, UML diagrams, as well as an associated 
    XML schema. The initial version of this meta-model has been derived from IBM's 
    Unified Method Architecture (UMA, reference to UMA specification download 
    to be added soon). UMA is an evolution of the current OMG industry standard 
    <a href="http://www.omg.org/docs/formal/05-01-06.pdf">Software Process Engineering 
    Meta-model (SPEM) v1.1</a> integrating concepts from IBM Rational Unified 
    Process, IBM Global Services, and IBM Rational Summit Ascendant. IBM and other 
    OMG partners are working on making UMA, with improvements suggested by partners, 
    to become <a href="http://www.omg.org/cgi-bin/doc?ad/2004-11-4">SPEM 2.0</a>. 
    Two versions of draft specifications have already been submitted to the OMG 
    as of November 2005. The initial exemplary tool implementation for EPF will 
    be based on the <a href="http://www.omg.org/cgi-bin/doc?ad/05-05-06">first 
    draft submission</a>. As SPEM 2.0 stabilizes, it is expected to update the 
    EPF to the final specification. The meta-model will be extensible through 
    the usage of custom attributes and custom process elements as well as normal 
    EMF extensibility mechanisms.</li>
  <li> <strong>Core extensible process tooling framework</strong> - Basic functionality 
    and extension point APIs will be provided to allow 1) method authoring, 2) 
    process authoring, 3) library management, and 4) configuring and publishing. 
    Each of these domains is described in more details below, under exemplary 
    tools.</h4> </li>
</ul>
<p>An initial implementation of the process engineering framework is offered for 
  contribution by IBM. It will require some modification to be made extensible, 
  following the SPEM 2.0 extensibility mechanisms.</p>

<p><strong>Exemplary tools</strong></p>
<p> The following exemplary tools provide the following capabilities:</p>
<ul>
  <li> <strong>Method Authoring</strong> - Best practices can be captured
  as a set of reusable method building blocks as defined in the meta-model; 
  roles, work products, tasks, and guidance, such as templates, guidelines, 
  examples, and check lists. Relationships between method elements can be 
  defined through forms. A rich-text editor allows you to document method 
  elements, and graphical views present diagrams showing relevant 
  relationships. Reuse is facilitated by allowing you to create a method 
  element as a derivative of another method element through various 
  inheritance-type of relationships. This allows you to e.g. specify that 
  a Systems Architect has similar responsibilities to a Software Architect 
  by expressing the differences, reusing everything that is common. </li>
  <li> <strong>Process Authoring</strong> - Reusable process building blocks 
  can be organized into processes along a lifecycle dimension by defining 
  e.g. Work Breakdown Structures (WBSs), and when in the lifecycle to 
  produce what work products in which state. The tool allows you to construct 
  reusable chunks of processes through so called capability patterns. 
  A capability pattern may for example define how to define, design, 
  implement and test a scenario or a user story, and this pattern can now 
  be reused in a variety of processes. The tool also allows you to define 
  delivery processes, which are end-to-end processes. Structural information 
  can often be edited with graphical as well as non-graphical editors.</li>
  <li> <strong>Library Management and Content Extensibility</strong> - An 
  XMI-based library enables persistency and flexible configuration management 
  as well as content interchange for distributed client-server implementations. 
  Method and process content can be packaged into content plug-ins and content 
  packages allowing simple distribution, management and extensibility of content. 
  A plug-in can be produced describing how content in other plug-ins should 
  be extended. As content plug-ins are added to your content library, the tool 
  will resolve dependencies between process elements.</li>
  <li> <strong>Configuring and Publishing</strong> - A process configuration
     can be created by selecting a set of content plug-ins and content packages.
    Optionally, an exemplary process configuration can be used as a starting
    point,
  and content plug-ins and content packages added or removed from this exemplary
    configuration. As an example, you may start with a generic exemplary process
    suitable for small collocated teams, and add content plug-ins containing
    specific
  guidance for each of Eclipse, JUnit, J2EE, and IBM Rational RequisitePro. 
  The delivery processes associated with a configuration can be further customized.
    As the configuration is published, the tool resolves the many relationships
    that
  may exist between process elements in the selected plug-ins and packages, 
  and generates a set of html pages with links representing relationships between
    process elements to make the resulting Web site easy to navigate.
  The resulting Web site is viewable via a  web browser, without 
  the need for a Web server. This will allow users on all platforms to view 
  the published process.</li>
</ul>
<p>An initial implementation of these features is offered for contribution by 
  IBM. See proposal artifacts at the end of this proposal. This initial contribution 
  needs to be matured, and refactored to address the added extensibility of the 
  process engineering framework.</p>

 
<p> <strong>Exemplary and extensible process content</strong></p>
<p> Our intention is to enable the ecosystem to build software best practices. 
  Some of these best practices may be contributed to Eclipse as exemplary uses 
  of EPF, while others may be commercial process offerings. A key value proposition 
  with the Process Framework is the ability and intent to support a wide spectrum 
  of development styles to accommodate both established practices as well as new 
  ones as they are introduced. The scope of the open source content will be kept 
  small initially, to enable us to build a stable set of high-quality base content, 
  focusing on core development activities; requirements, analysis & design, implementation, 
  testing, change management, and project management. It is however crucial, that 
  the framework and the base content can be extended to scale to support content 
  covering the entire IT Lifecycle Management domain, including software, systems 
  and product development, business engineering, operations and systems management, 
  governance and portfolio management.</p>
<ul>
  <li> IBM is offering for contribution the Basic Unified Process (BUP, a very 
    light weight adaptation of RUP with influences from Scrum. This content exists 
    in draft format, but needs to be completed and evolved based on contributions 
    from other project members. The content may also need to be refactored to 
    enable a broad set of processes to be created from it through content extensions. 
    See <a href="Basic Unified Process.pdf">BUP whitepaper</a> and demo 
    of the BUP prototype (BUP.avi). Also see a demo of a variant of Basic Unified Process with Scrum influences (BUP+Scrum.avi). Several other companies such as 
    <a href="http://www.ambysoft.com/"> Ambysoft</a>, <a href="http://www.bearingpoint.com">BearingPoint</a>, 
    <a href="http://www.capgemini.com/">Capgemini</a>, <a href="http://www.covansys.com/">Covansys</a>, 
    <a href="http://www.ivarjacobson.com/">Ivar Jacobson International</a>, <a href="http://www.numbersix.com/">Number 
    Six</a>, and <a href="http://www.ubc.ca/">University of British Columbia</a> 
    will also create other content extensions</em></u></li>
  <li>Other project members, including <a href="http://www.objectmentor.com/">Object 
    Mentor</a>, <a href="http://www.bedarra.com/">Bedarra Research Lab</a>, <a href="http://www.rallydev.com/index.jsp">Rally 
    Software Development</a> and <a href="http://www.ambysoft.com/"> Ambysoft 
    </a>will collaborate to contribute a Basic Agile process which covers other 
    key ideas and processes from the Agile Development community, including Agile 
    Modeling, Extreme Programming and Scrum</li>
</ul>
<p>More exemplary and extensible processes will be created as committers express 
  interest and contributions are proposed.  See demo of process authoring capabilities in the EPF prototype (Authoring.avi).</p>
<p><strong><font size="3">Target Users of the Process Framework</font></strong></p>
<p> For a perspective of how different types of organizations may leverage the 
  Process Framework, see whitepaper titled <a href="Who will benefit from Eclipse Process Framework.pdf">Who 
  will benefit from Process Framework </a></p>

<h2>Organization</h2>

			
<p align="left">We propose this project should be undertaken as a Technology project 
  rather than as part of the Eclipse Platform. Being a Technology project gives 
  it room to experiment without disruption to other Eclipse Platform development 
  work.</p>

Since Technology sub-projects are not meant to be ongoing, we foresee two possible 
evolutionary paths for the subproject: 
<ol type="1">
  <li>The project is moved to a permanent state as a Tools Platform project.</li>
  <li>The technology proves to be insufficient for Eclipse and the work is set 
    aside.</li>
</ol>
These paths are not mutually exclusive. Some combination of some or all of these 
paths may be the result of the work done in this project. <br>


<p><h3>Proposed project lead and initial committers</h3></p>
<ul>
  <li> <a href="http://www.ibm.com/">IBM</a> 
    <ul>
      <li> Project lead - Per Kroll</li>
      <li> Ricardo Balduino</li>
      <li> Peter Haumer</li>
      <li> Bruce MacIsaac</li>
      <li> Charles Yan</li>
      <li>Kelvin Low</li>
      <li>James Thario</li>
      <li>Todd Frederickson (IBM Global Services)</li>
    </ul>
  </li>
  <li> <a href="http://www.2promentor.com/">2-Pro Mentor</a>: 
    <ul>
      <li> Sigurd Hopen</li>
    </ul>
  </li>
  <li> <a href="http://www.adaptive.com/">Adaptive</a>: 
    <ul>
      <li> Nick Dowler</li>
    </ul>
  </li>
  <li> <a href="http://www.ambysoft.com/">Ambysoft</a>: 
    <ul>
      <li> Scott Ambler</li>
    </ul>
  </li>
  <li> <a href="http://www.aprocessgroup.com/">Armstrong Process Group</a> 
    <ul>
      <li>Chris Armstrong</li>
    </ul>
  </li>
  <li> <a href="http://www.bearingpoint.com">BearingPoint</a> 
    <ul>
      <li>o Bobby Soni</li>
    </ul>
  </li>
  <li> <a href="http://www.bedarra.com/">Bedarra Research Labs</a> 
    <ul>
      <li> Dave Thomas</li>
    </ul>
  </li>
  <li> <a href="http://www.capgemini.com/">Capgemini</a> 
    <ul>
      <li> Rene Maes</li>
      <li> Julian Holmes</li>
    </ul>
  </li>
  <li> <a href="http://www.catalysts.cc/">Catalysts</a> 
    <ul>
      <li> Christoph Steindl</li>
    </ul>
  </li>
  <li> <a href="http://www.covansys.com/">Covansys</a> 
    <ul>
      <li> Kirti Vaidya</li>
    </ul>
  </li>
  <li> <a href="http://www.ivarjacobson.com/">Ivar Jacobson International</a> 
    <ul>
      <li> Ian Spence</li>
      <li> Pan-Wei Ng</li>
    </ul>
  </li>
  <li> <a href="http://www.kruchten.com/">Kruchten Engineering Services Ltd (KESL)</a> 
    <ul>
      <li> Nicolas Kruchten</li>
      <li> Michael Trauttmansdorff</li>
    </ul>
  </li>
  <li> <a href="http://www.numbersix.com/">Number Six Software</a> 
    <ul>
      <li> Brian Lyons</li>
    </ul>
  </li>
  <li> <a href="http://www.objectmentor.com/">Object Mentor</a> 
    <ul>
      <li> Robert C. Martin</li>
    </ul>
  </li>
  <li> <a href="http://www.rallydev.com/index.jsp">Rally Software Development</a> 
    <ul>
      <li> Ryan Martens</li>
    </ul>
  <li> <a href="http://www.softeam.com/">SOFTEAM</a> 
    <ul>
      <li> Philippe Desfray</li>
    </ul>
  </li>
  <li> <a href="http://www.ubc.ca/">University of British Columbia</a> 
    <ul>
      <li> Philippe Kruchten</li>
    </ul>
  </li>
</ul>
<p><h3>Interested parties</h3></p>
<p> The following projects have expressed interest in this project. Key contacts 
  listed. </p>
<ul>
  <li> <a href="http://www.jaczone.com/">Jaczone</a>, Agneta Jacobson</li>
  <li> <a href="http://www.omg.org/">Object Management Group (OMG)</a>, Andrew Watson</li>
  <li> <a href="http://www.nttcom.co.jp/english/">NTT Comware</a></li>,Shinichi Doyama 
  <li> <a href="http://www.sogeti.com/">Sogeti</a>, Jean-Marc Gaultier</li>
  <li> <a href="http://www.unisys.com">Unisys</a>, Bobbi Young</li>
  <li> <a href="http://www.windriver.com/">Wind River</a></li>
</ul>
<p>The following process community leaders are supporting the initiative, on top of the ones that are already defined as committers:  Grady Booch and Ivar Jacobson</p>

<p> <h3>Code Contributions</h3></p>
<p> To jumpstart the project, the following contributions are proposed.</p>
<p><strong>Proposed IBM Contributions</strong></p>
<ul>
  <li><strong> Process engineering framework </strong> 
    <ul>
      <li><strong>Core process tooling framework </strong>&#8211; IBM proposes 
        to donate the core process tooling framework, but this does not yet have 
        exposed API extension points. </li>
      <li><strong> Meta-model </strong>- IBM also proposes to contribute a content 
        meta-model, referred to as Unified Method Architecture;see meta-model 
        section above for more details.</li>
    </ul>
  </li>
  <li><strong> Exemplary Tooling Donation</strong> - IBM proposes to contribute 
    a new set of tools built from scratch, and of alpha quality. The tools already 
    have the capabilities described in the Exemplary Tooling section above, and 
    will need to be modified to support added process engineering framework extensibility</li>
  <li> <strong>Exemplary and Extensible Content</strong> - IBM proposes to contribute 
    a light-weight derivative of the IBM Rational Unified Process, incorporating 
    some practices derived from Scrum. </li>
</ul>
<p><strong>Proposed 2-Pro Mentor Contributions</strong></p>
<ul>
  <li> Contributions on process architecture and process enactment / automation.</li>
</ul>
<p><strong>Proposed Adaptive Contributions</strong></p>
<ul>
  <li> Contribution on enhancements to, and integration of, the meta-model and 
    continuous participation on OMG SPEM initiative.</li>
</ul>
<p><strong>Proposed Ambysoft Contributions</strong></p>
<ul>
  <li> Content around Agile Modeling and Agile Database Techniques.</li>
</ul>
<p><strong>Proposed Catalyst Contributions</strong></p>
<ul>
  <li> Content around Agile Project Management.</li>
</ul>
<strong>Proposed Covansys Contributions</strong>
<ul>
  <li> Meta-model improvements to allow increased content extensibility through 
    the concept of Work Components, as well as exemplary content for Basic Unified 
    Process providing a Work Component based management framework.</li>
</ul>
<p><strong>Proposed Ivar Jacobson International contributions</strong></p>
<ul>
  <li> Content around the best practices of use-case driven development, component-based 
    development, model-based development, architecture-centric development, and 
    iterative and incremental development.</li>
  <li> Content around Aspect Oriented Software Development.</li> 
</ul>
<p><strong>Proposed Object Mentor Contributions</strong></p>
<ul>
  <li>Content for Agile/XP exemplary process <em>whereof subset is co-owned 
    with IBM.</em></li>
</ul>
<p><strong>Proposed SOFTEAM Contributions</strong></p>
<ul>
  <li> Application of software process to projects, by providing extensions to 
    the core process tooling framework, in particular by providing bridges to 
    external tools such as MS Project<strong>.</strong></li>
</ul>
<p><strong>Proposed Rally Contributions</strong></p>
<ul>
  <li> Content for Agile/XP exemplary process. </li>
  <li>Content for key concepts and relationships including User Story, Story card, 
    Usc Case scenario and Use Case.</li>
</ul>


<h2>Tentative Plan</h2>
<p>The following outlines a high-level development plan, including developer outreach, 
  with major milestones and what is expected to be achieved.
<ul>
  <li> Project acceptance and provisioning. December 1, 2005. 
    <ul>
      <li> IBM donation. Project environment established. Committers start to 
        work.</li>
    </ul>
  </li>
  <li> Release of Process Framework 1.0. July 2006. 
    <ul>
      <li> Objectives: 
        <ul>
          <li> Process tools matured and usable to produce a broad set of processes.</li>
          <li> Basic set of interfaces / APIs defined, stabilized, and delivered.</li>
		  <li> Support multi-user content development</li>
		  <li> Support process-centric configuration and publishing</li>
          <li> Content matured. At least two exemplary processes delivered; Basic 
            Unified Process and Agile/XP. Some level of commonality across the 
            two processes established, with reusable components extracted.</li>
          <li> Agree on key concepts and their relationship, such as user story, 
            scenario and use case where it is seems feasible.</li>
        </ul>
      </li>
      <li> Iteration 1: 12/1 &#8211; 1/15 
        <ul>
          <li> Train all committers.</li>
          <li> Add text to all Basic Unified Process (BUP) elements</li>
          <li> Outline Agile/XP process</li>
          <li> Understand required changes to donated code, meta-model and content</li>
		  <li> Suggest basic set of APIs</li>
        </ul>
      </li>
      <li> Iteration 2: 1/15 &#8211; 3/1 
        <ul>
          <li> Add text to all Agile/XP elements</li>
   		  <li> Freeze APIs</li>
        </ul>
      </li>
      <li> Iteration 3: 3/1 &#8211; 4/15 
        <ul>
		  <li> Identify an initial set of key promoters presenting at conferences, writing blogs and articles, etc</li>
          <li> Validate extensibility of BUP</li>
          <li> Freeze structure of BUP and Agile/XP</li>
         
        </ul>
      </li>
      <li> Iteration 4: 4/15 &#8211; 6/1 
        <ul>
          <li> Refactor and Polish BUP and Agile XP to increase commonality </li>
          <li> Code freeze</li>
        </ul>
      </li>
      <li> Iteration 5: 6/1 &#8211; 7/15 
        <ul>
          <li> End game</li>
        </ul>
      </li>
    </ul>
  </li>
  <li> Enlist content owners and champions. Sep 2006. 
    <ul>
      <li> Objectives: 
        <ul>
          <li> Identify a broader set of process champions with an interest 
		  to evolve EPF within a very specific domain. This could be to address 
		  MDA or project management, or a specific vertical. These people do not 
		  need to be well-known gurus, but rather specialists with a deep domain 
		  knowledge, with an interest in evolving best practices within a certain 
		  domain over an extended period of time.</li>
          <li> Arrange a number of workshops in conjunction with major conferences, 
		  allowing those with interest in process to learn more about EPF. Use 
		  these venues to recruit candidate champions. Nurture them and help 
		  them build a business case for why their company should invest in their 
		  participation.</li>
        </ul>
      </li>
    </ul>
  </li>
  <li> Release of Process Framework 1.5. 2007, Q2. 
    <ul>
      <li> Objectives: 
        <ul>
          <li> Ensure that EPF is valuable in a variety of contexts, such as within 
            a specific vertical, or can be integrated with various other process 
            tools</li>
          <li> Expand upon APIs with integration for Eclipse-based tools. </li>
          <li> Provide custom attributes and process elements for meta-model extensibility</li>
		  <li> Provide process compare and merge</li>
		  <li>Provide query facilities</li>
          <li> Provide exemplary integration between EPF and Eclipse-based tools</li>
          <li> Expand content into one or two high-priority areas, such as content 
            for IT project management, or product lifecycle management.</li>
          <li> Refactor content to increase commonality, and to facilitate content 
            extensions and customizations by consumers and plug-in providers.</li>
          <li> Have EPF included in many University curriculums. Establish course 
            material that can assist in including EPF in curriculums.</li>
          <li> Have researches to do research around EPF. Seek to have members 
            offer University grants to researchers.</li>
        </ul>
      </li>
    </ul>
  </li>
</ul>
<p></p>
<h2>Additional Information and Proposal Artifacts</h2>
The following artifacts are referenced in this proposal: 
<ul>
  <li> <a href="Who will benefit from Eclipse Process Framework.pdf">Who will 
    benefit from the Process Framework</a></li>
  <li> <a href="Basic Unified Process.pdf">Basic Unified Process</a></li>
  <li> BUP Prototype Demo (BUP.avi)</li>
  <li> BUP+Scrum Prototype Demo (BUP+Scrum.avi)</li>
  <li> Authoring Prototype Demo (Authoring.avi)</li>
</ul>
<p>Note: If you are unable to see the video portion of the demos above, you may 
  need this <a href="http://www.free-codecs.com/TechSmith_Screen_Capture_Codec_download.htm">codec</a> 
  file.<br>
</p>
      </div>
  </div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
