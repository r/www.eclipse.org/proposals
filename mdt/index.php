<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Eclipse Model Development Tools (MDT) Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Model Development Tools");
?>

<h2>Introduction</h2>

<p>The Model Development Tools (MDT) Project is a proposed open source subproject
under the <a href="http://www.eclipse.org/modeling/">Eclipse Modeling Project</a>.</p>

<p>This proposal is in the Project Pre-Proposal Phase (as defined in the <a
href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process</a>
document) and is written to declare its intent and scope. This proposal is
written to solicit additional participation and input from the Eclipse
community. You are invited to comment and/or join the project. Please send all
feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling.mdt">http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling.mdt</a>
newsgroup.</p>

<h2>Background</h2>

<p>As described in the project proposal for the <a
href="http://www.eclipse.org/proposals/modeling/">Eclipse Modeling Project</a>,
support for industry standards is critical to its success, and to the success of Eclipse
in general. The role of the Modeling project in the support of industry
standards is to enable their creation and maintenance within the Eclipse
community. Furthermore, as standards bodies such as the OMG have a strong
modeling focus, the Modeling project needs to facilitate communication and
outreach through its PMC and project contributors to foster a good working
relationship with external organizations.</p>

<p>MDT is a project inspired by the Eclipse community's need
for more end user &quot;tooling&quot; from the Modeling project. We seem to
have an abundance of frameworks and low-level technologies for modeling, but
not much in the way of what most expect to find. Of course, this is fueled by
the fact that &quot;other&quot; free or open source modeling tools exist,
making Eclipse a bit of a laggard in this area. Hopefully, now that we have the
key components in place (e.g. EMF, GMF, UML2), getting the community focused on
providing this won't take much longer.</p>

<h2>Project Description</h2>

<p>The MDT project will focus on big "M" modeling within the Modeling project, and its purpose will be twofold:</p>

<ol>
<li>To provide an implementation of industry standard metamodels.</li>
<li>To provide exemplary tools for developing models based on those
metamodels.</li>
</ol>

<p>The specifications implemented in the MDT project will be limited to those that fall within the scope of the Modeling Project charter.</p>

<h2>Organization</h2>

<p>We propose that this project be undertaken as part of the Eclipse Modeling Project. It will have dependencies on the Eclipse Platform, EMF, and GMF.</p>

<p>The MDT project will initially consist of components which already exist as subprojects (or components thereof) of the Modeling top-level project, namely XSD, UML2, and EODM. The scope of the UML2 component will be expanded to include the remaining specifications under the UML2 umbrella, i.e. Object Constraint Language (OCL) and Diagram Interchange (DI), plus graphical tools to develop UML models. Each of these (sub)components will be downloadable and useable independently of one another.</p>

<p>It is proposed that the following initial structure reside within the /cvsroot/modeling repository location at dev.eclipse.org:</p>

/mdt<br/>

&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;/org.eclipse.eodm<br/>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;/org.eclipse.uml2<br/>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;/org.eclipse.xsd<br/><br/>

<table border=0 cellpadding=0>
 <tr>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p> <b>Project</b></p>
  </td>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p> <b>Name</b></p>
  </td>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p> <b>Description</b></p>
  </td>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p> <b>Impact</b></p>
  </td>
 </tr>
 <tr>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p> <a href="http://www.eclipse.org/uml2">UML2</a></p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>Unified Modeling Language 2.x</p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>The UML2 project is an EMF-based implementation of the
  OMG's Unified Modeling Language 2.x specification.</p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>UML2 is currently its own project and will become a
  component of MDT. Its newsgroup will be eclipse.modeling.mdt.uml2 and its
  various subcomponents will be listed as components under a Modeling/MDT
  product in Bugzilla.</p>
  </td>
 </tr>
 <tr>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p ><a href="http://www.eclipse.org/xsd">XSD</a></p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>XML Schema Infoset Model</p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>XSD is a library that provides an <a
  href="http://download.eclipse.org/tools/emf/xsd/javadoc?org/eclipse/xsd/package-summary.html#details">API</a>
  for manipulating the components of an XML Schema as described by the <a
  href="http://www.w3.org/TR/XMLSchema-0">W3C XML Schema</a> specifications, as
  well as an API for manipulating the DOM-accessible representation of XML.</p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>XSD is currently its own project and will become a
  component of MDT. Its newsgroup will be org.eclipse.modeling.mdt.xsd and its
  various subcomponents will be listed as components under a Modeling/MDT
  product in Bugzilla.</p>
  </td>
 </tr>
 <tr>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p><a href="http://www.eclipse.org/emft/projects/eodm/">EODM</a></p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>EMF Ontology Definition Metamodel</p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>EODM is an implementation of RDF(S)/OWL metamodels of the <a
  href="http://www.omg.org/ontology">Ontology Definition Metamodel (ODM)</a>
  using EMF with additional parsing, inference, model transformation and
  editing functions.</p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>EODM is currently a component of the (former) EMFT project
  and will become a component of MDT. Its newsgroup will be
  org.eclipse.modeling.mdt.eodm and its various subcomponents will be listed as
  components under a Modeling/MDT product in Bugzilla.</p>
  </td>
 </tr>
 <tr>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p><a href="http://www.eclipse.org/emft">OCL</a></p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>Object Constraint Language</p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>OCL is an implementation of the OCL OMG standard for
  EMF-based models.</p>
  </td>
  <td valign=top style='padding:.75pt .75pt .75pt .75pt'>
  <p>OCL is currently a component of the (former) EMFT project
  and will become a subcomponent of the UML2 component in the MDT project. It
  will be covered by the UML2 newsgroup (eclipse.modeling.mdt.uml2) and it will
  be listed as a component (e.g. UML2 OCL) under a Modeling/MDT product in
  Bugzilla.</p>
  </td>
 </tr>
</table>

<p>Addition of new (sub)components (e.g. metamodels, tools) is
expected over the life of the Model Development Tools Project to take into account
the scope proposed above, and its inevitable alterations. It will be the
responsibility of the PMC to actively recruit contributors and alter the
project organization to maintain alignment and coverage of its defined scope.</p>

<h3>Initial committers</h3>

<p>The following companies will
contribute committers to get the project started:</p>

IBM (<a href="http://www.ibm.com/">www.ibm.com</a>) <br/>
 <ul>
  <li>Kenn Hussey (proposed project lead; UML2 component - UML)</li>
  <li>Christian Damus (UML2 component - OCL)</li>
  <li>Ed Merks (XSD component)</li>
  <li>Guo Tong Xie (EODM component)</li></ul>
<br/>
Borland (<a href="http://www.borland.com/">www.borland.com</a>)<br/>
 <ul>
  <li>Michael Golubev (UML2 component - Tools)</li>
</ul> </p>

<h3>Interested parties</h3>

<p>The following organizations
have expressed initial interest in the project:</p>

<ul>
<li>IBM&nbsp;&nbsp;<a href="http://www.ibm.com/">(www.ibm.com)</a> </li>
<br/>
<li>Borland&nbsp;&nbsp;<a href="http://www.borland.com/">(www.borland.com)</a> </li>
</ul>

<h3>Participation</h3>

<p>Critical to the success of this project is participation of developers in
the modeling tools development community. We intend to reach out to this
community and enlist the support of those interested in making a success of the
Model Development Tools project. We ask interested parties to contact the MDT
newsgroup to express interest in contributing to the project.</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
