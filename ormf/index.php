<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?><div id="maincontent">
  <div id="midcolumn">
    <h1>Open Requirements Management Framework</h1>
    </p>
    <?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("ORMF");
?>
    <h1>Introduction</h1>
    <p>The Open Requirements Management Framework (ORMF) Project is a proposed open source project under the Eclipse Technology Project.</p>
    <p>This proposal is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and is written to declare its intent and scope. This proposal is written to solicit additional 
    participation and input from the Eclipse community. You are invited to comment on and/or 
    join the project. Please send all feedback to 
    the<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ormf"> http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ormf</a> newsgroup. </p>
    <h1>Overview</h1>
    <p>The goal of the Open Requirements Management Framework (ORMF) is to extend the Eclipse platform to create an open source framework for building Eclipse based tools which  facilitate working with structured requirements. ORMF is focused on structured requirements, as opposed to free form ones. However, to avoid pedantic repetition, please read &quot;structured requirements&quot; for each occurrence of the term &quot;requirements&quot;.</p>
    <p> ORMF will be a solid multi-tier framework that will enable the Eclipse platform to support creation, development and management of  disparate types of requirements  by utilising a common set of services, patterns and mechanisms. Ultimately, it will enable analysts and requirements specifiers to collaboratively build valid, complete and resilient requirements models for their projects, while allowing the other team members or, more generally, all stakeholders in the projects to be active  participants of both formation of the requirements and  their subsequent maintenance imposed by evolution and change.</p>
    <p>The framework will develop the infrastructure, components and services that will support the development of the specialised tools. It will be comprised both of a client component, which will be based on the Eclipse platform and will consist of Eclipse plug-ins, and of a server component. The server component, built on top of Java 5  Enterprise Edition (initially Glassfish), will contain all the necessary infrastructure as well as the modules that are required for the publication of the requirements documents in a variety of formats.</p>
    <p>With Eclipse's role as a unifying platform for all  development tools that tackle the entire life cycle of a software project, the need for a tool that guides practitioner in the capture and maintenance of requirements, as well as sophisticated reporting and multiple views into the requirements model, appears large and immediate to us. Our proposal for ORMF aims at fulfilling this need, as well as facing issues with the process of requirements gathering and management that we perceive in today's practices. These issues are covered in the <a href="#background">Background</a> section of this document.</p>
    <p>Substantial portions of the framework have already been built by <a href="http://www.etish.org">Etish Limited</a>, along with an exemplary tool for ORMF, Useme, which allows analysts and requirements specifiers to define use case based requirements in a consistent and harmonious fashion. Etish Limited will offer the existing code for ORMF and Useme to this project, if accepted by the Foundation. Whilst our vision includes ORMF features that still require considerable thinking and exploration and will no doubt involve design and implementation challenges, the fundamental framework mechanisms and components are already in place and experience shows us that building tools such as Useme on top of ORMF is a straightforward and approachable endeavour.</p>
<h1><a name="background" id="background"></a>Background</h1>
    <p>During our consulting experience, we at Etish Limited have uncovered a set of needs that we have perceived time and time again whilst helping our clients organise (or, in many cases, reorganise) their requirements in a rational and usable fashion. In a nutshell, we could summarise these needs as the great difficulty of creating and maintaining robust, correct and consistent requirements  without an excessive expenditure in time and commitment. We have observed this irrespective of the specific technology chosen by the clients for this purpose, whether one of the commonly available automated requirements management tools or manual capture and maintenance.</p>
<p>The main problems with requirements capture and management today, as we see them, are the ones outlined below.</p>
    <dl>
      <dt class="dlterm">Free form documents</dt>
      <dd class="dldesc">Most organisations rely on requirements documents that are ultimately free form text documents. This often leads to document incorrectness and inconsistency.</dd>
    </dl>
    <dl>
      <dt class="dlterm">Gap between UML model and textual specification documents</dt>
      <dd class="dldesc"> The model describes requirements as simple elements (for example use cases and actors) and the structural relationships between such elements (for example use case inclusion). The specification documents describe the details of each specific requirement but frequently loose the connectivity of the model due to the difficulty of capturing and subsequently maintaining structural relationship within a textual context. The two types of artefacts  therefore tend towards being mismatched and inconsistent.
        <p></p>
      </dd>
      <dt class="dlterm">Difficult document maintenance</dt>
      <dd class="dldesc">Even in cases where at least the most important of the relationships are captured in the textual documentation, the task of maintaining them by hand through requirements evolution and change is daunting for the amount        of time and effort that is needed.
        <p></p>
      </dd>
      <dt class="dlterm">Single user requirements capture</dt>
      <dd class="dldesc">Except for very expensive, high end requirements management tools, most other automated systems (or indeed manual capture!) are single user, making it difficult for all the project stakeholders in general to share the information expressed in these documents.
        <p></p>
      </dd>
      <dt class="dlterm">Non integrated requirements management</dt>
      <dd class="dldesc">All requirements management tools available today are standalone applications, with their own user interfaces and their own utilisation rules and procedures, with the well known shortcomings of a non integrated               development environment.</dd>
    </dl>
    <p>ORMF is aimed at resolving these issues.</p>
    <h1>Scope and Objectives</h1>
    <h3>ORMF's objectives in a nutshell</h3>
    <p>To understand ORMF it is essential to recognise that its notion of requirements has two facets. There is the &quot;artefact&quot; view which is how most requirements practitioners perceive what is being created and managed; this is the document centric view. Then there is the &quot;model&quot; centric view which is structured, organised and precise. ORMF goes to great lengths to give the practitioners the impression they are working with documents while in fact the system is building highly structured models behind the scenes. This gives ORMF the ability to be quite robust in the way it handles, manipulates and utilises the model, while hiding  most of the uncomfortable details from the user.</p>
<p>So in essence ORMF makes possible tools that  build entire requirements documentation sets for any given software project. The documentation reflects the underlying  requirements model, including the incorporation of all the necessary connectivity elements that are part of the  model itself. All documentation is automatically kept up to date against the evolving and changing requirements model.  One of the primary intentions of ORMF is to  enable a working paradigm that makes it straightforward for tools to provide guidance and validation to the requirements specifiers throughout the requirements management life cycle.</p>
    <p>The framework's aim is to 
      foster collaboration among team members by providing the requisite mechanisms and controls that are necessary to a multi-user effort, including immediate exposure of the documentation to all the project's stakeholders through a completely transparent publication process. </p>
    <p>A key goal of ORMF is to encourage the development of best of breed Eclipse based tools for the requirements management discipline, thereby expanding and  enriching  the Eclipse user community to embrace those involved in the requirements domain.</p>
    <h3>The detailed vision</h3>
    <p>The main components of our vision for ORMF are the ones outlined below.</p>
    <h4><font color="#336699">Structured model based  requirements</font></h4>
    <p>All requirements  are represented as XML documents, the salient features of each being translated into an XML node. This representation drives the way in which requirements are captured (through specialised form editors) and subsequently presented to the user. The structured nature of the requirements enables any tool that is built on top of ORMF to apply strict validation rules when the requirement is being edited and to guide the user through the compilation of the requirement in order for it to be correct and consistent with the remainder of the model.</p>
    <h4><font color="#336699">Standards</font></h4>
    <p>It is the goal of ORMF to support existing standards where they are relevant to the issues we are addressing and when it is practical, from a resource perspective, for the ORMF team to be taking on the effort. The OMG, IEEE and ISO all have  relevant standards in the space ORMF is targeting; these will be explored and entertained for inclusion. Other standards will undoubtedly be suggested as the project matures.</p>
    <h4><font color="#336699">The introduction of smart links</font></h4>
    <p>All requirements models are characterised to a significant extent by  the relationships between the various elements composing the model. ORMF will provide an extensible mechanism  for representing relationships which can be used, for example, to model such things as UML relationships based upon their stereotype. For any defined type of relationship, the framework will automatically support  the creation of smart links, which are a representation of these relationships at the textual level. In this way ORMF will ensure that the connectivity of the model is preserved within the textual specifications, guaranteeing consistency between textual  and model based descriptions. These  links will be maintained by ORMF automatically throughout the life cycle of the project being developed, through changes to either side, i.e. producer and consumer, of the relationship. This is the reason why we refer to them as being &quot;smart&quot;.</p>
    <h4><font color="#336699">The general availability of common requirements related elements</font></h4>
    <p>ORMF will provide the ability to capture &quot;cross cutting concerns&quot; -- to steal an expression from Aspect Oriented Programming -- that are common to all types of requirements, such as Glossary, Notes, Risks and Issues. These will be provided for the usage of all requirements  built on top of ORMF. In this way these elements can be defined by the user once and then utilised  consistently and transparently. Any subsequent modification of one of these elements is automatically reflected in all consumers by ORMF.</p>
    <h4><font color="#336699">The availability of complex views and reports</font></h4>
    <p>ORMF will enable specifiers to build and then manage a requirements model by utilising custom editors that are designed to support each specific requirement type. For example use cases and software requirement specifications have quite different shapes, so ease of use is enhanced by catering to each specific requirements type. The framework will also make available meaningful visualisations, including diagrammatic ones, non-trivial views and flexible search and reporting tools that will facilitate the extraction of required  information from the model. </p>
    <p>We will  be exploring an extension of the BIRT framework to provide flexible reporting on requirement traceablity matrices, test matrices and high level project management reports, such as use case grouping by status, by priority, by degree of implementation. All these reports should be flexible in the sense that they should be customisable in their granularity to reflect the specialised needs of the stakeholders they are created for.</p>
    <p>Another goal of ORMF is to explore how it can provide useful extensions to the Eclipse modelling projects in order to simplify and  add consistency to diagrammatic representations of various requirements views, building on UML [or possibly other pertinent standards] wherever possible. </p>
    <h4><font color="#336699">A rich infrastructure</font></h4>
    <p>ORMF will provide a useful infrastructure of common patterns, services and mechanisms. This refers to both an infrastructural layer of services that leverage the Eclipse platform in the client tier and a set of   facilities that perform business logic and handle persistence in the server tier.</p>
    <h4><font color="#336699">A collaborative environment</font></h4>
    <p>ORMF is geared towards collaborative work at both stages of requirements capture [production] and of artefact creation [consumption].</p>
    <p>In the production stage, collaborative features, managed by the framework, will be inherited at no extra cost by all requirement type specific tools built on top of ORMF. These features include authentication and authorisation, automatic processes for synchronisation and conflict resolution of the client and server views of a project, automatic maintenance of the integrity of both the documents and the overall requirements model.</p>
    <p>In the consumption stage, ORMF will provide a common engine for the publication of requirements documents in a variety of formats; the engine will be equipped with the possibility of customisation of the look and feel of the final published product.</p>
    <h4><font color="#336699">Extensibility</font></h4>
    <p>ORMF will provide straightforward extension mechanisms, both via specialisation of base classes and via extension points, that permit the extension of the framework into requirement specific tools. See the diagram in section <a href="#functional_architecture">Functional architecture</a> below for a schematic indication of the location within ORMF that will accept extensions.</p>
    <h4><font color="#336699">Support for versioning and change control</font></h4>
    <p>As part of the development of ORMF, we will explore the issues of version control, baselining and snapshots creation, in order to identify what is needed and what can be done. This is especially relevant when considering the needs of change control in the development process.</p>
    <h4><font color="#336699">Data export facilities</font></h4>
    <p>ORMF will provide support for data export into other standard tools, such as UML modellers, project management tools and testing frameworks.</p>
    <h4><font color="#336699">Simple administration</font></h4>
<p>ORMF will include a set of administrative plug-ins that will be utilised transparently by all ORMF based tools. These facilities will enable an Administrator to create and configure requirements projects, to define the participants and the roles of the requirements teams and, finally, to customise documentation artefacts for any given project.</p>
    <h1><a name="functional_architecture" id="functional_architecture"></a>Functional architecture</h1>
    <p>The diagram that follows is a high level representation of our vision for the structure of ORMF, with some of the add-on plug-in tools that it will facilitate. As the diagram shows, we plan to architect ORMF in three tiers, namely the client tier, the server tier and the database tier. Each tier will provide APIs and extension mechanisms that will enable the creation of the add-on plug-ins. These are indicated in the diagram by the components that have a red border. The client tier rests on the Eclipse platform, whereas the server tier is based upon the Java 5 Enterprise platform.</p>
    <p align="center"><a href="high level architecture.pdf" target="_blank"><img src="high level architecture.jpg" alt="high level architecture" width="418" height="311" /></a></p>
    <p align="center"><strong>ORFM High Level Architecture</strong><br />
      <em>(click image for high resolution PDF)</em></p>
    <h4><font color="#336699">The client tier</font></h4>
    <p>This tier
    contains all components which deal with the direct production or with the consumption of the requirements.</p>
    <p>The consumption portion of the client tier is of no great interest for this discussion as it will simply consist of a suitable  program on the consumer's machine, e.g. web browser, Acrobat Reader, Microsoft Word etc.</p>
    <p> The production portion of the client tier is a lot more interesting. It is a set of custom Eclipse plug-ins that will be based on a common extension of the Eclipse Platform  that provides services of general applicability. Example of such services are:</p>
    <ul>
      <li>the translation of the XML based model elements into/from visual gadgets;</li>
      <li>a common wizard validation mechanism;</li>
      <li>a single pattern for handling views updates upon model changes based upon the well known observer/observable pattern</li>
      <li>common strategies for synchronisation of the contents of the visual components with changes occurring at the server level as a result of other users modifying the project. </li>
    </ul>
    <p>The base plug-in is also responsible for acting as the communication mechanism between the client and the server tier. </p>
    <p>The client tier will also offer a plug-in containing many common views, wizards and dialogs that will be used by any specialised tool that functions on top of ORMF. </p>
    <p>Finally the client tier will also offer the Administration interface for direct usage by all ORMF based tools.</p>
    <h4><font color="#336699">The server tier</font></h4>
    <p>The server tier will offer all the business logic and persistence mechanisms that are required to handle users requests coming from the client tier. These services will be mediated by agents located in a Web services layer, which will also be utilised by the publication and reporting engine. The latter will be responsible for the production of publishable documentation for consumption. Any tool that is built on top of ORMF will simply need to add its own document specific contributions to both the business logic components and to the publication and reporting engine, as indicated in the diagram above. </p>
    <p>The persistence layer will finally be responsible for any communication with the database tier.</p>
    <h4><font color="#336699">The database tier</font></h4>
    <p>Presently the framework is dependent on the Apache Derby database. It will be considered in the future if there is sufficient demand by the community for the database to be replaceable by any SQL compliant datastore.</p>
    <h2>Exemplary tool</h2>
    <p>The first demonstration of ORMF as an extensible framework will be provided by Useme, the set of Use Case based plug-ins that handle the details of use case requirements specifications. Useme already exists as an alpha grade product and has been used internally be Etish Limited on several projects, including Useme itself. In the architectural diagram  above, the components that have been developed as part of Useme are the ones with a red border.</p>
    <p> More information about Useme can be found in the following documentation:</p>
    <ul>
      <li><a href="https://useme.dev.java.net/nonav/documentation/overview/overview.html" title="An overview of Useme" target="_blank">An overview of Useme</a></li>
      <li><a href="https://useme.dev.java.net/nonav/documentation/introduction/intro.html" target="_blank">An introduction to Useme</a></li>
      <li><a href="https://useme.dev.java.net/nonav/documentation/visual_tour/index.html" target="_blank">A visual tour of Useme</a></li>
      <li><a href="https://useme.dev.java.net/nonav/documentation/introduction/rationale.html" target="_blank">The rationale for Useme</a></li>
      <li><a href="https://useme.dev.java.net/nonav/documentation/short_demo/spinAroundTheBlock/spinaroundtheblock_viewlet_swf.html">A spin around the bloc</a>k (Flash tutorial)</li>
      <li><a href="https://useme.dev.java.net/nonav/documentation/long_demo/closerLook/closerlook_viewlet_swf.html">A closer look at Useme</a> (Flash tutorial)</li>
    </ul>
<h2>Synergies</h2>
    <p>Above and beyond the standard platform, ORMF does or soon will utilise the following Eclipse projects:</p>
    <ul>
      <li><a href="http://www.eclipse.org/projects/project_summary.php?projectid=modeling" target="_blank">Eclipse Modeling Project</a> (EMF, GMF and UML2)</li>
      <li><a href="http://www.eclipse.org/projects/project_summary.php?projectid=tools.gef">Graphical Editor Framework</a></li>
      <li><a href="http://www.eclipse.org/projects/project_summary.php?projectid=birt" target="_blank">Business Intelligence and Reporting Tools</a></li>
    </ul>
    <p>To date we have identified collaborative possibilities with:</p>
    <ul>
      <li><a href="http://www.eclipse.org/projects/project_summary.php?projectid=technology.epf" target="_blank">Eclipse Process Framework Project</a></li>
      <li><a href="http://www.eclipse.org/projects/project_summary.php?projectid=technology.osee" target="_blank">Open System Engineering Environment</a></li>
      <li><a href="http://www.eclipse.org/projects/project_summary.php?projectid=tptp.test" target="_blank"> Testing Tools</a> (functional testing artefacts can be automatically generated from uses cases)</li>
    </ul>
    <h1>Organization</h1>
    <p>We propose this project should be undertaken as an incubator project with the Technology project.</p>
    
    <h2>Mentors</h2>
    <ul>
    <li>Gary Xue, Actuate</li>
    <li>Richard Gronback, Borland</li>
    <li>Harm Sluiman, IBM</li>
    </ul>
    
    <h2>Initial committers</h2>
    <p>The initial committers will focus on delivering the initial production ready release. The initial committers are:</p>
    <p> 
      * <a href="mailto:Barbara.Rosi-Schwartz@Etish.org">Barbara Rosi-Schwartz </a> (project lead)<br/>
      * <a href="mailto:Joel.Rosi-Schwartz@Etish.org">Joel Rosi-Schwartz</a> (project lead)<br/>
      * <a href="mailto:bengarbers@gmail.com">Ben Garbers </a> <br/>
      * <a href="mailto:chereches_vasile@yahoo.com">Vasile Chereches </a> <br/>
      * <a href="mailto:woponikwar@ib-ponikwar.de">Wolfgang Ponikwar </a> <br/>
      * <a href="mailto:achim.loerke@bredex.de">Achim Loerke </a> <br/>
      * <a href="mailto:aleksandra.k.wozniak@gmail.com">Aleksandra Wozniak </a> <br/>
      * <a href="mailto:costabile.santis@students.cefriel.it">Costabile Santis </a> <br/>
      * <a href="mailto:ezzatdemnati@gmail.com">Ezzat Demnati </a></p>
    <h2>Interested parties</h2>
    <p>To date the following organisations have expressed an interest in ORMF:</p>
    <ul>
      <li>Symbian</li>
      <li>Fijitsu (UK)</li>
      <li>Integranova</li>
      <li>greensys</li>
    </ul>
    <p>The ORMF <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ormf" target="_blank">newsgroup</a> has been been gratifyingly active for a project still under proposal. The ORMF team wishes to thank all of the people who have taken time to comment on the proposal and share their ideas. It is premature to incorporate these ideas into proposal, but the ORMF team will be considering them all carefully as we scope the 1.0 release. In particular one comment which we have heard so frequently deserves special note in the proposal.</p>
<p> It has been consistently noted that requirements capture tools need to have a Web browser interface. The project leads have not disguised the fact that they consider Web interfaces semi-crippled for sophisticated tools and that they inevitably lead to difficult to use tools with compromised functionality. But alas the world, including a number of the ORMF team, does not share our [elitist?] views. Therefore it is clear that a web interface may well be in ORMF's future.<br />
    </p>
<h2>Developer community</h2>
    <p>We are primarily considering several avenues towards growing the developer community.</p>
    <ul>
      <li>It is likely that   many  consulting service organisations   can benefit substantially from both ORMF and Useme. We hope that some of them are interested in working with us towards extending the functionality  for themselves and for the community.</li>
      <li>We believe that tools built on ORMF can save a significant portion of the effort it takes to gather and maintain requirements. By exposing Useme to organisations who expend considerable resources on internal development efforts, it should become  evident to these entities that contributing to the functionality, richness and completeness of the framework (and of course contributing editor plug-ins) will have a positive influence on the success of their projects.</li>
      <li>We are hopeful that developers in the community will join us simply because what we are delivering is a great framework that helps make building software easier, faster and better.</li>
    </ul>
    <h2>User community</h2>
    <p>We are in the very early stages of building a community, yet the limited exposure of our exemplary tool, Useme, to date has produced very positive results. The <a href="https://useme.dev.java.net/">Useme website</a> on Java.net has regular traffic, we receive information requests regularly and the Useme <a href="http://www.etish.org/newsletter/signup.html" target="_blank">newsletter</a> has over a hundred subscribers. We believe that once the initial beta version is released this community will grow rapidly.</p>
    <h2>Code contributions</h2>
    <p>Etish offers an initial contribution to this project which includes code corresponding to the described architecture. The contribution contains a  portion of the features listed above and the first  exemplary tool, Useme. We welcome all parties who are interested in contributing further to the project.</p>
    <h1>Tentative Plan</h1>
    <h2>Development plan</h2>
    <ul>
      <li>Creation review -  25 June 2008
        <ul>
          <li>Etish Limited donation of initial code base</li>
          <li>parallel IP process</li>
          <li>provisioning - project environment established. </li>
          <li>committers begin work</li>
        </ul>
      </li>
      <li>Deliver alpha release to community of both ORMF and Useme -  6 September 2008
        <ul>
          <li>gather feedback from community</li>
          <li>scope 1.0 release</li>
          <li>determine what architectural changes are necessary to achieve the 1.0 release</li>
        </ul>
      </li>
      <li>Deliver project plan for the 1.0 release- 17 September 2008</li>
    </ul>
    <h2>Platforms</h2>
    <p>Both the existing client and server code have been successfully tested on:</p>
    <ul>
      <li>Windows XP SP2</li>
      <li>Red Hat Enterprise Linux 5 </li>
      <li>Mac OS X 10.5</li>
    </ul>
    <p>ORMF requires:</p>
    <ul>
      <li>Eclipse Platform 3.3.1 or better</li>
      <li>A Java 5 Enterprise Edition compliant server</li>
    </ul>
    <p>For development and testing Sun's Java EE 5 SDK (based on Glassfish) has been utilised. Standards have been observed, so any compliant server should suffice. In fact it is probable that some servers that are only partially compliant will also work, but these servers will have to be carefully tested individually.</p>
    <h2>High Level Use Cases</h2>
    <p>Please see the <a href="use cases.pdf">attached list</a> of use cases for the portions of the framework and the exemplary tool that are already in existence.</p>
  </div>
</div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
