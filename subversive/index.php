<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>

    <div id="maincontent">
	<div id="midcolumn">

<h1>Subversive</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("The Subversive Project");
?>

<h1>The Subversive Project</h1>
<b>An Eclipse Technology Project Proposal<br/>
June 2006<br/>
Last update: 12 September 2006</b>

<h2>Introduction</h2>
<p>
	The Subversive project is a proposed open source project under the <a href="http://www.eclipse.org/technology/">Eclipse Technology Project</a>.
</p>
<p>
	This proposal is in the Project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process document</a>) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.subversive">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.subversive</a> newsgroup. 
</p>
<p>
	The goal of project is to create an <a href="http://subversion.tigris.org/">Subversion</a> plug-in for Eclipse (Platform SVN Integration) with functionality similar to the Eclipse CVS Team project (Platform CVS Integration). After gathering wide community acceptance and reaching a sufficient maturity level, it is anticipated that Subversive will become part of the core Eclipse platform.
</p>

<h2>Background</h2>
<p>
Team collaborative work, sharing source files and versioning are key concepts of any software development effort. One of the most popular version control system for quite some time is the open-source CVS project. Nonetheless, CVS concepts have become insufficient in recent years. The aim of the CVS successor known as <a href="http://subversion.tigris.org/">Subversion</a> (SVN) is to avoid CVS conceptual problems, provide more quality solution, which is targeted community needs and finally gather CVS user's community.
</p>
<p>
SVN's user community has been growing continually since the release of the first version. SVN is positioned as a replacement for CVS, and it is highly probable that it will be widely accepted by the community in that capacity. SVN has reached enough maturity level to be used in production, and many companies and development groups previously using CVS are replacing it with SVN now. This process of adoption is clearly reflected in open-source communities like <a href="http://www.apache.org">apache.org</a> and <a href="http://www.sourceforge.net">sourceforge.net</a>, which now use SVN repositories along with CVS. 
</p>
<p>
Wide SVN support by development tools is a key concept for SVN evolution and popularity growth. From the other side, including SVN support to development tools can be also valuable for these tools themselves. Unfortunately Eclipse doesn't provide such support in standard distribution, which is an object for discussion in community during a long time. Instead of using third-party plugins for SVN community want to see SVN support in standard Eclipse distribution.
</p>
<p>
The Subversive project was started in second quarter of 2005 as a component of larger development activity where it played role of foundation for a rich client to the Polarion server. Because of this the project scope was wider then standalone SVN client for Eclipse, especially in following areas:
<ul>
    <li>Code separation to different layers with different level of abstraction</li>
    <li>Strong extensibility and reuse of different layers</li>
    <li>Support of behavior customization by external plugins</li>
    <li>Optimization in specific use-cases</li>
    <li>Performance optimization</li>
    <li>Consistent and usable UI</li>
</ul>
</p>
<p>
One of the most important task since the beginning was to find the best way how the user interface components should interact with the SVN client access. This work brought to light necessity to define additional interfaces to standard JavaHL SVN client access interface. Major reasons for the extension were necessity to satisfy needs of higher layers, and to reach defined quality of UI behavior (e.g. progress reporting, ability to cancel an operation, etc.). Such extensions also allow to resolve problems, which were caused by limitations of standard JavaHL interfaces, like refactoring support. As a foundation for such improvements <a href="http://tmate.org/svn/">JavaSVN</a> library was chosen. Together with JavaSVN authors Subversive team worked on implementation and verification of the SVN access interfaces improvements, which are further suggested for inclusion to standard SVN. As the result of such approach Subversive users can benefit from extended SVN functionality earlier then it can be included to standard SVN.
</p>
<p>
In March 2006, as soon as this project was reached enough maturity level, it was separated from larger project and released as the <a href="http://www.polarion.org/index.php?page=overview&project=subversive">Subversive</a> open source project on the <a href="http://www.polarion.org">Polarion Community</a> site. Since the first public release a Subversive community grew to thousands of users, and hundreds of active community members. During a long time project ranked as one of the most popular in the <a href="http://www.eclipseplugincentral.com">Eclipse Plugin Central</a> site.
</p>

<h2>Scope</h2>
<p>
The Subversive project main scope is providing an ability to work with SVN from the Eclipse. The main goal, which declared by this proposal is creation a foundation, which can be used for including the SVN support in the standard Eclipse distribution. In order to achieve this goal the following areas should be targeted.
<ul>
    <li>Ensure a high quality and usability of functionality and UI, which allow including the project in the standard Eclipse distribution</li>
    <li>Keep features and UI consistent to Eclipse concepts and close as possible to Eclipse CVS</li>
    <li>Provide project support by helping users to resolve problems and collecting tips for improvements</li>
    <li>Provide clear and easy to use infrastructure for integration with other plugins</li>
    <li>Ensure possibility to reuse of project code in other projects</li>
    <li>Contribute to Eclipse Team services improvements</li>
    <li>Contribute to SVN improvements</li>
    <li>Provide and update project documentation, including help files and specifications</li>
    <li>Follow Eclipse guidelines and best practices</li>
    <li>Assist to Eclipse Foundation with setup, maintenance and migration to SVN</li>    
</ul>
</p>

<h2>Description</h2>
<p>
The Subversive project is focused on development of an Eclipse plug-in which provides SVN support. From the project organization point of view it is close to Eclipse CVS project and follows its concepts and Eclipse best practices. Namely the project consists of Core and UI plugins and two SVN Client plugins: JavaSVN, which is pure Java implementation and JavaHL, which is native implementation. The project organization is represented in picture below.<br/>
</p>
<img src="modules.GIF"/>
<p>
Using extension points defined in Core and UI plugins Subversive can be easily integrated with other projects. Also it's possible to change used SVN Client library - contributors may implement their own SVN Client Library plug-ins and Subversive will use them automatically when they are installed in the Eclipse.
</p>
<p>
The Subversive project architecture, which is represented on the picture below, follows to several important requirements for both - UI and Core modules:
<ul>
    <li>Precise separating of UI and Core parts</li>
    <li>Unified error handling</li>
    <li>Failure tolerance</li>
    <li>Conceptual integrity of API</li>
    <li>Strong API levels delimitation</li>
    <li>Easy API extensibility</li>
</ul>
</p>
<img src="architecture.GIF"/>
<p>
Subversive infrastructure contains following main benefits, which can be useful also for integration with other projects. More detailed information about the Subversive architecture is provided by the <a href="http://www.polarion.org/projects/subversive/docs/Subversive_Architecture_Overview.pdf">Subversive Architecture Overview</a> document.
<ul>
    <li><b>Command Framework</b> designed correspondingly to "Command Pattern" concept. It allows user to perform any complex interactions with SVN and Eclipse Platform in performance-optimal and easy way. Command Framework already contains command implementations for all frequently used cases of interaction with SVN, checked out projects and Eclipse Platform. All provided commands can be fully reused in external tools without any limitations</li>
    <li><b>SVN Resource Model</b> allows building of local and repository resource hierarchies and provide command framework with all required information in one standard way</li>    
    <li><b>Execution Framework</b> allows running all commands in the similar way. Its background implementation is responsible for automated resource locking rules calculation and error handling</li>    
    <li><b>Execution Engine API</b> is set of classes and interfaces which hide from user how background implementation serves a Commands execution</li>    
    <li><b>Error Handling</b> mechanism provided by Subversive Core allows user to build applications with high failure tolerance: one failed command does not prevent other commands from execution if it is required. Moreover, commands itself can be recovered from errors</li>    
</ul>
</p>
<p>
The Subversive team together with the JavaSVN authors extended standard SVN API (JavaHL) in order to extend SVN functionality and resolve problems, which are caused by limitations of standard JavaHL interface. Such extensions were made for concepts proofing as a first step to propose standard SVN extension. Currently Subversive users can benefit from following features, which are not available in other SVN tools: 
<ul>
    <li><b>Full projection of Eclipse refactoring operations into SVN</b>. Subversive adds the power of refactoring to SVN. Users don't need to commit changes immediately after each refactoring - they can make such number of sequential refactorings as they need and Subversive cares about it</li>
    <li><b>Interactive merge operation, similar to merge in Eclipse CVS plug-in</b>. In Subversive, the user is able to see the result of merge operations and then choose to proceed or to cancel. In this case, merge looks like an ordinal synchronize operation. In other SVN clients, the merge operation requires creation of temporary files, so it's not possible to see the result of a merge without actually performing the merge operation</li>
    <li><b>Automatic conflicts resolving</b>. Any conflicts which can be resolved automatically are resolved automatically. Other conflicts are resolved manually without original file modification or creation of temporary files</li>
    <li><b>Correct cancellation of operations with SVN</b></li>            
</ul>
</p>

<h3><i>Current status</i></h3>
<p>
Subversive reached the 1.0.0 release in July 2006 and now is developed in two streams: 1.1.x for Eclipse 3.2/Callisto and 1.0.x for Eclipse 3.0 and later. Project is available under <a href="http://www.eclipse.org/legal/epl-v10.html">EPL</a> license.
</p>

<h3><i>Project principles</i></h3>
<p>
<ul>
    <li><b>Similarity with the Eclipse CVS Team project</b>. Subversive should use the same concepts as CVS Team project and look similar to it as possible. It should allow CVS users to start using SVN without complications</li>
    <li><b>Extensibility</b>. Subversive should provide a set of API and extension points which allow other projects to extend functionality and interact with SVN</li>
    <li><b>Self-hosting</b>. From the early project stages, source code should be stored in a SVN repository and Subversive used to work with it. This will guarantee usability feedback and project testing inside the development team from the earliest project stages</li>
    <li><b>Follow agile development process</b>. The Subversive development team should follow agile development processes. Core concepts of this process are effective requirements management and iterative development</li>
    <li><b>Feedback gathering</b>. The Subversive project team should be in close communication with community in order to get information about defects, feature requests and integration. This information is used for definition of short term and long term project goals</li>
</ul>
</p>

<h2>Organization</h2>
<p>
The Project is proposed to be created within the <a href="http://www.eclipse.org/technology">Eclipse Technology Project</a>. After reaching enough of a maturity level and a large enough user community, it is anticipated that will become part of the core Eclipse platform.
</p>
<p>
<a href="http://www.polarion.com/">Polarion Software</a>, as a submitter of this proposal, is looking for companies and persons interested in participating in all aspects of this project. If you are interested in actively participating, please take part in the newsgroup discussions or ask to be added to the list of interested parties.
</p>
<p>
Because Subversive's main functionality is providing access to SVN, it makes sense to store Subversive source code in a SVN repository and use Subversive for development.<br>
</p>

<h3><i>Initial committers</i></h3>
<p>
<a href="http://www.polarion.com/">Polarion Software</a> has committed to providing project leadership and developer resources for this project. The initial committers are:
<ul>
    <li>Alexander Gurov [<a href="http://www.polarion.com/">Polarion Software</a>]</li>
    <li>Sergiy Logvin [<a href="http://www.polarion.com/">Polarion Software</a>]</li>    
    <li>Elena Matokhina [<a href="http://www.polarion.com/">Polarion Software</a>]</li>
    <li>Alessandro Nistico</li>
    <li>Antonin Pokorny: Project Lead [<a href="http://www.polarion.com/">Polarion Software</a>]</li>    
    <li>Igor Vinnykov [<a href="http://www.polarion.com/">Polarion Software</a>]</li>
</ul>
</p>

<h3><i>Interested parties</i></h3>
<p>
This proposal can be interested for the following Eclipse projects, which have integration with Subversive:
<ul>
    <li><a href="http://www.eclipse.org/buckminster">Buckminster</a></li>    
    <li><a href="http://www.polarion.org/index.php?page=overview&project=fasttrack">FastTrack</a></li>        
    <li><a href="http://www.eclipse.org/mylar">Mylar</a></li>
</ul>
</p>

<h3><i>Community</i></h3>
<p>
Since the first appearing on public in March 2006 the Subversive project quickly gathered around itself the thousands of people, which take an active interest in SVN. These people are participated in mail lists and forum discussions, send error reports and tips for improvements. This community of people defines a direction of project evolution and such community is one of the most important values of project.
</p>
<p>
Community people also helped a project by code contributions. Currently there are five code contributors and this number is growing.
</p>

<h3><i>Subclipse project</i></h3>
<p>
A <a href="http://subclipse.tigris.org">Subclipse</a> project is another SVN plugin for Eclipse. After a publishing of the Subversive proposal for Eclipse, Subclipse team decided to publish their own <a href="http://www.eclipse.org/proposals/svn">proposal</a>. After reviewing of this proposal we found that both or our proposals are close to each other and have same goals. The main goal for Subversive and Subclipse proposals is inclusion SVN support into standard Eclipse distribution. As so we invited the Subclipse team to join forces in creation of common project.
</p>
<p>
Both projects have a good history and a lot of supporters, so from all this community point of view cancelling of any project in favor of another is not a profitable way. Instead both teams should join forces in order to analyze strong and weak points of each solution and create new project, which benefit from the best solutions. It will not be a quick and easy way, but it's a good chance to union teams and community in order to reach the main goal.
</p>
<p>
In order to make it possible we propose following:
<ol>
    <li>Define and agree the approach for analysis of technical solution</li>
    <li>Identify areas, which should be analyzed and discussed</li>
    <li>Perform analysis, discuss results and reach an agreement for solution, which should be selected for new project</li>        
</ol>
</p>
<p>
The Subversive team made fist steps in this direction and anyone can contribute to this process. See related discussions in the Subversive project <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.subversive">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.subversive</a> newsgroup.
</p>

<h2>Tentative Plan</h2>
<p>
<u>M1 - Q3 2006</u>. Join the Eclipse community as a new Technology Project. 
</p>
<p>
Aim - appearance in the community, gathering users and user support in order to reach a sound maturity level
</p>


<p>
<u>M2 - September/October 2006</u>.
</p>
<p>
Making current codebase compliant to EPL restrictions.
</p>


<p>
<u>M3 - November 2006</u>
</p>
<ul>
	<li>Repository layout recognition (find projects)
		<ul>
			<li>wizard support for checkout and share operations
		</ul>
	<li>Stabilization and finalization of the Interactive Merge feature
</ul>

<p><u>M4 - January 2007</u></p>
<ul>
	<li>Repository layout recognition part II
		<ul>
			<li>Identify Trunks, Branches and Tags and use this information for configuration of UI wizards
		</ul>
	<li>Support for linking of Work Items during commit (/Issues)
	<li>Decorators for SVN Browser
		<ul>
			<li>Identified Trunks, Branches, Tags, Projects will be displayed in a specialized form
		</ul>
</ul>

<p><u>M5 - February/March 2007</u></p>
<ul>
	<li>Revision Journal (list of revision, ability to expand each one and see affected resource, automatic update of this list from server)
	<li>Explore features planned to be implemented within the Platform CVS Integration project and implement those applicable for SVN (See <a href="http://dev.eclipse.org/viewcvs/index.cgi/%7Echeckout%7E/platform-vcm-home/cvsmain.html">http://dev.eclipse.org/viewcvs/index.cgi/%7Echeckout%7E/platform-vcm-home/cvsmain.html</a>)
</ul>

<p><u>M6 - Q2 2007</u>. Be ready to join the Eclipse platform.</p>
<p>
Aim - be ready to join the Eclipse platform standard distribution, preparation for code review, rework code according Eclipse comments, etc.
</p>

      </div>
  </div>
  
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
