<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
	
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Parallel Tools Platform");
?>
    <h1>eclipse parallel tools platform</h1>
  

      <p>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/">Eclipse 
        Development Process document</a>) and is written to declare the intent 
        and scope of a proposed Technology PMC Project called the Eclipse Parallel 
        Tools Platform Project, or PTP. In addition, this proposal is written 
        to solicit additional participation and inputs from the Eclipse community. 
        You are invited to comment on and/or join the project. Please send all 
        feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ptp">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ptp</a> 
        newsgroup. </p>
   
    
  


  <h2>Overview</h2>
  
    
      <p>Designing and developing parallel programs (that is, multi-process, not 
        just multi-threaded programs) is an inherently complex task. Developers 
        must choose from the many parallel architectures and programming paradigms 
        that are available, and face a plethora of tools that are required to 
        execute, debug, and analyze parallel programs in these environments. Few, 
        if any, of these tools provide any degree of integration, or indeed any 
        commonality in their user interfaces at all. This further complicates 
        the parallel developer's task, hampering software engineering practices, 
        and ultimately reducing productivity.</p>
      <p>One consequence of this complexity is that best practice in parallel 
        application development has not advanced to the same degree as more traditional 
        programming methodologies. The result is that there is currently no open-source, 
        industry-strength platform that provides a highly integrated environment 
        specifically designed for parallel application development. In order to 
        address this deficiency, the Eclipse Parallel Tools Platform Project (PTP) 
        aims to extend the Eclipse framework to support a rich set of parallel 
        programming languages and paradigms, and provide a core infrastructure 
        for the integration of a wide variety of parallel tools. </p>
      <p>The PTP project will provide a comprehensive platform that addresses 
        many of these problems facing parallel application developers today, including:</p>
      <ul>
        <li>no standard, portable, parallel IDE</li>
        <li>no existing open-source parallel debugger</li>
        <li>the proliferation of stand-alone parallel tools</li>
        <li>the inability of existing tools to interoperate</li>
        <li>the poor scalability and reliability of existing tools</li>
      </ul>
      <p>The Eclipse PTP will deliver a portable, scalable, standards-based parallel tools platform that will enable the integration of tools specifically suited for parallel computer architectures. These tools will provide functionality such as:</p>
      <ul>
        <li>parallel debugging</li>
        <li>performance analysis and profiling</li>
        <li>message traffic analysis</li>
        <li>data visualization</li>
        <li>problem solving environments</li>
        <li>model coupling</li>
        <li>automated parallelization</li>
        <li>data parallel languages</li>
      </ul>
    <p>By providing a common parallel tools framework, PTP will also encourage tool developers and vendors to adopt a highly integrated approach to their tool design. By decoupling the user interface requirements, tool developers will also be able to focus on their core tool functionality without having to waste resources on maintaining the infrastructure needed to support their tools. The result will benefit parallel application developers by ensuring that a wide range of highly integrated parallel tools will be available to meet their development needs.</p>
  


  <h2>Projects</h2>
  
    <strong>Parallel Execution Environment </strong>
      <p>The Parallel Execution Environment is an extension to the existing launch configuration environment provided by Eclipse. The Parallel Execution Environment provides Eclipse with the ability to launch and control a parallel program. Integrated into this functionality will be support for a wide range of parallel architectures, including shared memory and distributed memory systems. The parallel execution environment will also provide a simple, uniform interface into a range of resource allocation facilities (such as batch queuing systems) that are employed by many parallel systems.</p>
      <p><strong>Parallel Debugger</strong></p>
      <p>The Parallel Debugger is an extension to the existing platform debug framework that will provide functionality to enable the execution debugging of multi-process programs. The goal of the project is to enable launching in debug mode, or attaching a debugger to, an extremely large (thousands of processes) parallel program. The parallel debugger will rely on services provided by the parallel execution environment in order to launch or attach to the parallel program. A range of extensions to the existing debug model and debug UI will be required to support the new functionality.</p>
      <p><strong>Parallel Tools Integration </strong></p>
      <p>The Parallel Tools Integration project will define and implement a set of core services that will enable the integration of tools specifically designed for parallel programming environments. Examples include:</p>
      <ul>
        <li>A model view controller framework for monitoring and viewing information for a large number of data sources</li>
        <li>Remote communication services for managing and controlling a large number of remote objects</li>
        <li>Standardized data structures for language support, including parallel languages</li>
        <li>Parallel data distribution information for analysis and visualization</li>
      </ul>
      <p>The aim of this project will be to provide a comprehensive, extensible and scalable infrastructure that will encourage the development of highly integrated parallel tools.</p>
      <p><strong>Parallel End-User Support</strong></p>
      <p>In addition to the creation of a portable parallel IDE, the PTP will also provide the foundation for a rich environment to support the end-users of parallel applications. This will include a range of services to simplify end-user interaction with existing parallel run-time systems, resource allocators, and job schedulers, provide monitoring and status information, and to facilitate the management, collection and visualization of data for these applications.</p>
      <p><strong>Fortran Development Tools</strong></p>
      <p>Fortran support is specifically required by the scientific computing community to enable high performance parallel application development. The Fortran Development Tools project will add Fortran language support to the Eclipse platform, providing a similar level of functionality to that provided by the C/C++ Development Tools. This will include editing (syntax highlighting, code assist, etc.), debugging (using native debug support), and launching (as an external application). As for the C/C++ Development Tools, the Fortran Development Tools will provide a parser, search engine, content assist provider and makefile generator.</p>
  


  <h2>Organization</h2>
  
    <p>It is expected that this project will have committers from Los Alamos National Laboratory, but we are encouraging others to participate in all aspects of the project. If you are interested in participating, please take part in the newsgroup discussions or ask to be added to the list of interested parties.</p>
      <p><strong>Interim Leads</strong></p>
      <p>Greg Watson, LANL (Project Lead)<br>
      gwatson <strong>at</strong> lanl.gov<br>
      +1-505-665-0726</p>
      <p>Nathan DeBardeleben, LANL (Parallel Execution Environment)<br>
      ndebard <strong>at</strong> lanl.gov<br>
      +1-505-667-3428</p>
      <p>Craig Rasmussen, LANL (Fortran Development Tools)<br>
      crasmussen <strong>at </strong>lanl.gov<br>
      +1-505-665-6021</p>
    
  


  
    <strong>Interested Parties</strong>
    <strong>Components</strong>
  
  
    
    
  
  
    Etnus, LLC
    <em>Parallel Debugger</em>
  
  
    Intel Corp.
    <em>Parallel Debugger, Fortran Development Tools</em>
  
  
    Monash University
    <em>Parallel Debugger, Parallel Execution Environment </em>
  
  
    Open HPC Inc.
    <em>Parallel Tools Platform</em>
  
  
    Open MPI
    <em>Parallel Tools Platform</em>
  
  
    Rice University 
    <em>Parallel Tools Integration, Fortran Development Tools </em>
  
  
    Terra Soft Solutions 
    <em>Parallel Tools Platform</em>
  
  
    Univeristy of Oregon
    <em>Parallel Tools Integration</em>
  
  
    University of Tennessee
    <em>Parallel Execution Environment, Parallel Debugger, Parallel Tools Integration</em>
  
  
    
    <p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </em></p>
  

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
