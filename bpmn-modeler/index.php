<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "BPMN Modeler Sub-Project proposal";
$pageKeywords	= "bpmn,eclipse,stp,soa tools,";
$pageAuthor		= "Antoine Toulme";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>BPMN Modeler Sub-Project proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("BPMN Modeler");
?>

<p>
<h2>Introduction</h2>

<p>
BPMN Modeler is a proposed sub-project under the top level 
project <a href="http://www.eclipse.org/stp/">Eclipse SOA Tools 
Platform (STP)</a>. The proposal is written to solicit additional participation and input from the 
Eclipse community. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.bpmn-modeler">eclipse.bpmn-modeler</a> newsgroup.
</p>

<h2>Background</h2>
<p>
The mission of the <a href="http://www.eclipse.org/stp/">STP project</a> is to build frameworks and extensible tools that enable the design, configuration, assembly, deployment, monitoring, and management of software designed around a Service Oriented Architecture. The project is guided by the values of transparency, extensibility, vendor neutrality, community collaboration, agile development, and standards-based innovation. 
</p>
<p>
Currently, <a href="http://www.eclipse.org/stp/">STP project</a> contains a component named <a href="http://www.eclipse.org/stp/bpmn/index.php">STP/BPMN</a>. This component provides a graphical designer named <a href="http://wiki.eclipse.org/STP/BPMN_Component#Overview">BPMN Modeler</a>, which allows to model BPMN (<a href="http://www.bpmn.org">Business Process Modeling Notation</a>) diagrams. This tool is based on <a href="http://www.eclipse.org/gmf/">GMF</a> and works with an <a href="http://www.eclipse.org/emf">EMF</a> meta model compatible with the <a href="http://www.bpmn.org/Documents/OMG%20Final%20Adopted%20BPMN%201-0%20Spec%2006-02-01.pdf">BPMN 1.0</a> and <a href="http://www.bpmn.org/Documents/BPMN%201-1%20Specification.pdf">BPMN 1.1</a> specifications proposed by the <a href="http://www.osoa.org/display/Main/Home">Object Management Group</a>.

</p>
<p>
The aim of this proposal is to transform the actual <a href="http://www.eclipse.org/stp/bpmn/index.php">STP/BPMN</a> component into a sub-project of <a href="http://www.eclipse.org/stp/">STP project</a>.
</p>

<h2>Description</h2>
<p>
The purpose of the Eclipse BPMN Modeler sub-project is to provide a graphical modeler for modeling practitioners, ie end users with no particular programming skills, to design business process diagrams under the Business Process Modeling Notation.
</p>
<p>

<h2>Project Scope</h2>
The BPMN sub-project will focus on building an usable and extensible graphical modeler by leveraging the tools provided by the Graphical Modeling Framework and the EMF Validation Framework.

<ul>
<li> Provide a first class user experience when it comes to designing processes, using graphical assistants.</li>

<li> Provide a graphical notation according to the BPMN standard.</li>
<li> Provide ways to extend the modeler to integrate it within commercial products.</li>
</ul>

<h2>Out of Scope</h2>
Some things are not in the scope of the BPMN modeler sub-project.

<ul>
<li> Generation of Java or BPEL code: see STP-IM and JWT for coverage of those areas</li>
<li> Transformation towards other models (see STP-IM)</li>
</ul>

<h2>Proposed Components</h2>
<ul>
<li><b>BPMN model</b>: This component is the core of the modeler. It represents the EMF model used to persist the BPMN diagrams.</li>
<li><b>BPMN diagram</b>: This component should be in charge of the diagram editor itself. It would represent the editor and all its tools.</li>
<li><b>BPMN validation</b>: This component represents the BPMN validator, it enforces the BPMN specification by flagging the invalid shapes with warnings and errors. It should also come with quickfixes.</li>

</ul>

<h2>Relationship with Other Eclipse Projects</h2>

The BPMN sub-project will be built on top of the Eclipse Platform and will have relationship with other Eclipse projects. 

<ul>
<li> <a href="http://www.eclipse.org/stp/">STP</a>
	<ul>
	<li> <a href="http://www.eclipse.org/stp/im/index.php">Intermediate Model</a> component planned to work on transformations between the IM and the <a href="http://www.eclipse.org/stp/bpmn/model/index.php">BPMN Domain Model</a>.</li>
	</ul></li>
<li> <a href="http://www.eclipse.org/mdt">MDT</a>
	<ul>
	<li> The <a href="http://www.eclipse.org/modeling/mdt/?project=bpmn2">BPMN 2</a> component works on creating a standard metamodel implementing the BPMN 2.0 specification. The BPMN subproject is interested into collaborating with this component to align its metamodel to BPMN 2.0 by migrating to their metamodel and/or implementing an exporting tool.</li>
	</ul>
</li>
<li> <a href="http://www.eclipse.org/emf"> EMF project</a> is used to generate the <a href=http://www.eclipse.org/stp/bpmn/model/index.php">BPMN Domain Model</a>.</li>

<li> <a href="http://www.eclipse.org/modeling/emf/?project=validation#validation">EMF Validation Framework</a> is used to implement the additional validation rules.</li>
<li> <a href="http://www.eclipse.org/gmf/">GMF project</a> is used to generate the <a href="http://wiki.eclipse.org/STP/BPMN_Component">BPMN Designer</a>.</li>
<li> <a href="http://www.eclipse.org/epp">Eclipse Packaging Project</a> will be used to create a stand-alone Eclipse product to deliver the BPMN modeler.</li>
</ul>

<h2>Organization</h2>
We propose that this sub-project will take place under the top level project <a href="http://www.eclipse.org/stp/">STP</a>.

<h2>Mentors</h2>
<ul>
<li>Oisin Hurley, IONA</li>
<li>Dave Carver, STAR</li>
</ul>

<h2>Proposed Initial Committers</h2>
<ul>
<li> Antoine Toulm&eacute; - <a href="http://www.intalio.com/">Intalio, Inc.</a> (Leader) - committer on STP/BPMN Component.</li>
<li> Hugues Malphettes - <a href="http://www.intalio.com/">Intalio, Inc.</a> - committer on STP/BPMN Component.</li>
</ul>

<h2>Code Contributions</h2>
The <a href="http://www.eclipse.org/stp/bpmn/index.php">Eclipse STP/BPMN</a> component will be the initial code.

<h2>Interested Parties</h2>
At this time, the following people, companies and projects have expressed their interest to see the creation of the BPMN modeler sub-project:

<ul>
<li> Oisin Hurley - <a href="http://www.eclipse.org/stp/">STP project lead</a>, <a href="http://www.iona.com/">IONA</a></li>
<li> Benoit Beaudet - <a href="http://techsolcom.ca">Techsolcom</a></li>
<li> Andrea Zoppello - Engineering Ingegneria Informatica</li>
<li> Marc Dutoo, JWT project lead, OpenWide</li>
<li> Florian Lautenbacher, JWT Project Lead, University of Augsburg</li>
<li> Andrei Shakirin, Swordfish Project, Sopera</li>
<li> Gerald Preissler, Swordfish Project, Sopera</li>
<li> Alexei Markevich, Swordfish Project, Sopera</li>
<li> Adrian Mos, STP Project, INRIA</li>
</ul>

<h2>Tentative Plan</h2>

<li> June 2009
<ul>
<li> Implement quick fixes: we have added validation to our diagrams, now we want people to be able to right-click and fix the problem they encounter.</li>
<li> Add an export tool a la Eclipse Spaces. Be able to deploy a diagram as a zip file, associated with an image and a description.</li>
<li> Package the BPMN modeler as part of a modeling practitioners Eclipse Product (see bug <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=240141">240141</a>)</li>
</ul></li>
</ul>
</p>


      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
