<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>DOC2MODEL</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Doc2Model");
?>


  <h2>Introduction</h2>
  <p>
  The Doc2Model (Document to Model) framework is a proposed open source component under the Eclipse Modeling Framework Technology project for parsing structured documents (e.g., xlsx, docx, odt, odf...) to produce EMF models.
It is in the Project Proposal Phase (<a href="http://wiki.eclipse.org/Development_Resources/HOWTO/Proposal_Phase">as defined in the Eclipse Development Process document</a>) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the Eclipse Modeling Framework Technology (<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">eclipse.technology.emft</a>) newsgroup with [doc2model] as a prefix of the subject line.  
  </p>
  <h2>Background</h2>
  <p>
  The most widely used tools in many organizations continue to be text processors and spreadsheets. 
  Often these documents describe business data that are important to manipulate in other contexts. Examples of data contained in such documents include the following:
  </p> 
<ul>
<li>Requirements</li>
<li>Tests</li>
<li>CRC cards</li>
<li>Structure definitions</li>
<li>Documentation generation</li>
</ul>
<p>
Because these kinds of tools often produce plain text documents, it's typically quite complex and time-consuming to develop a specific parser able to produce output more amenable to further manipulation.
</p>
<p>
Currently some organizations are investing effort to publish specifications of open source file formats, for example Office XML (e.g., docx, xlsx...) and Open Document (e.g., odt, odf...) to facilitate widespread adoption and easier consumption.
</p>
<p>
In fact, most of the business documents are organized with data defined in a common way, (top down for example for text documents) using text style, regular expressions, and column numbering.  As such, it's possible to support a generic solution for parsing those documents and transforming the business data into EMF models, using XML parsing and EMF's reflective capabilities.
</p>
<h2>Scope</h2>
<p>
This project will provide an extensible framework for producing EMF model instances from plain text and structured documents.
</p>
<p>
Transforming a business document into an EMF model will facilitate more opportunities to exploit the business data contained in such a document. In some cases documents represent the specification of a system. Instead of retyping information to produce the corresponding model it will be possible to generate it.
</p>
<p>
Doc2Model can be used to, for example, to import requirements from text files and transforming them into SysML requirements models.
</p>
<p>
The documents file formats which will be managed by Doc2Model include</p>
<ul>
<li>Open source formats as docx, xlsx, odt, odf;</li>
<li>Common formats as csv;</li>
<li>And formats desired by the eclipse community.</li>
</ul>
<p>The Doc2Model API will provide extension mechanism to allow users to add custom parsers for specific tools. These Parsers could be contributed to Doc2model component if the license is compatible with EPL.</p>
<h2>
Description</h2>
<p>The target model type is specified using a configuration model which describes how the data is identified during the parsing. This configuration is a map indicating what the generator does when a matching rule is applied. Matching rules make use of regular expressions, special styles, columns (spreadsheet), and tags. 
Transformation proceeds as follows:</p>
<p>1.	Read the matching configuration.</p>
<p>2.	Analyse (parse) the input document to identify matching data base on the rules.</p>
<p>3.	Produce an output EMF model instance from the data recognized into the input document. Cross references between the data is supported and additional data can be injected.
</p>
<h2>
Sample
</h2>
<ul>
<li>input user document</li>
<br />
<img  src="./input.png" border="2" />
<br />
<br />
<li>doc2model mapping</li>
<br />
<img  src="./mapping.png" border="2" />
<br />
<br />
<li>result after execution</li>
<br />
<img  src="./result.png" border="2" />
<br />
<br />
</ul>
<h2>Relationship with Other Eclipse Projects/Components</h2>
<ul>
<li>Doc2Model will be built on top of EMF.
</li>
<li>Doc2Model will exploit EMF Compare to obtain differences between models.</li>
</ul>
<h2>Third party libraries</h2>
<ul>
<li>no third party librairies, standard java parsing.</li>
</ul>
<h2>Code Contributions</h2>
<ul>
<li>
<a href="http://www.topcased.org">Topcased</a> is offering doc2model as an initial codebase (see <a href="http://gforge.enseeiht.fr/projects/doc2model">http://gforge.enseeiht.fr/projects/doc2model</a>).
</li>
<li>
a flash demo of current version is available <a href="http://www.4shared.com/file/123030374/bd993fe4/doc2model.html ">here</a>.
</li>
</ul> 
<h2>Organization</h2>
<h3>Committers</h3>
<ul>
<li><a href="mailto:tristan.faure@atosorigin.com">Tristan Faure (Atos Origin)</a></li>
<li><a href="mailto:emilien.perico@atosorigin.com">Emilien Perico (Atos Origin)</a></li>
<li><a href="mailto:werner.keil@gmx.net">Werner Keil</a></li>
</ul>
<h3>Mentors</h3>
<ul>
	<li><a href="mailto:ed.merks@gmail.com">Ed Merks</a></li>
	<li><a href="mailto:cedric.brun@obeo.fr">Cedric Brun</a></li>
</ul>
<h3>Interested Parties</h3>
<ul>
<li><a href=http://www.atosorigin.com>Atos Origin</a></li>
<li><a href="http://www.obeo.fr/?&lang=en">Obeo</a></li>
</ul>
  </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
