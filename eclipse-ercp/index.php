<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">&nbsp; 

  <h1>the embedded rich client platform 
      (eRCP) </h1>
      

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Embedded Rich Client Platform");
?>
<p>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/">Eclipse 
  Development Process document</a>) and is written to declare the intent and scope 
  of a proposed project called the Embedded Rich Client Platform, or eRCP. In 
  addition, this proposal is written to solicit additional participation and inputs 
  from the Eclipse community. You are invited to comment on and/or join the project. 
  Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ercp">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ercp</a> 
  newsgroup. </p>

   
    <h2>Introduction</h2>
  
   
     
      <p>The Embedded Rich Client Platform Subproject is a proposed open source 
        project under the Eclipse Technology Project . This document describes 
        the Background, Scope and Goals and Organization of the proposed project. 
        The eRCP project is managed by Project Leads under the Technology PMC. 
        For more information read the <a href="/technology/technology-charter.php">Technology 
        Project Charter</a> and the <a href="ercpcharter.html">eRCP Project Charter</a>. 
        A proposed project plan is also <a href="plan.html">available here</a>. 
      </p>
      
  


   
    Background
  
   
     
      <p>The Eclipse Project has become one of the most successful open-source 
        projects in the world, establishing preeminence in the tooling/IDE arena. 
        In addition, there has been a recent launch of the Rich Client Platform 
        (RCP) that extends the domain of the Eclipse platform into non-IDE arenas.<br>
        <br>
        Up until this point, the Eclipse Platform has been aimed at desktop computers; 
        however, we believe that many of the same principles and design goals 
        of the Platform are applicable to the embedded space. This project proposal 
        outlines the organization, scope, and work items needed to investigate 
        the suitability of RCP for embedded clients.<br>
        <br>
        Since the term &quot;embedded&quot; means many things in the industry, 
        we define the (minimum) characteristics of our targeted embedded devices 
        as follows: <br>
        <br>
        Note: <i>this section is not meant to be normative or prescriptive. It 
        is meant to provide a simple definition of &quot;embedded&quot; for this 
        document.</i>
      </p>
    
<table BORDER=0 CELLSPACING=5 CELLPADDING=2 WIDTH="90%" >
    <tr>
    <td width="19%"> </td>
    <td valign="top" width="17%">Display: </td>
    <td width="64%"> 
      <ul>
        <li>8-bit color</li>
        <li>176x220 </li>
      </ul>
    </td>
  </tr>
  <tr>
    <td width="19%"> </td>
    <td valign="top" width="17%">Input: </td>
    <td width="64%"> 
      <ul>
        <li>T9, touch, or QWERTY</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td width="19%"> </td>
    <td valign="top" width="17%">Processing Power: </td>
    <td width="64%"> 
      <ul>
        <li>ARM9 100Mhz or equivalent </li>
      </ul>
    </td>
  </tr>
  <tr>
    <td width="19%"> </td>
    <td valign="top" width="17%">Memory Constraints: </td>
    <td width="64%"> 
      <ul>
        <li>Total RAM: 8M</li>
        <li>Total ROM/Flash: 16M </li>
        <li>Heap: 2-4M</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td width="19%"> </td>
    <td valign="top" width="17%">Connectivity: </td>
    <td width="64%"> 
      <ul>
        <li>Cellular: GPRS, EDGE, or 3G </li>
        <li>WiFi</li>
      </ul>
    </td>
  </tr>
</table>
  
  


   
    <h2> 
      Scope and Goals</h2>
  
   
     
      <p>Note: <i>in order to gain embedded perspectives and maintain the Platform's 
        integrity, we envision that the eRCP subproject would be staffed with 
        a mixture of existing Eclipse committers (to leverage their expertise 
        in current Eclipse architecture) and new committers from the embedded 
        industry.</i><br>
      </p>
      <p><b>Scope</b></p>
      <p>As currently envisioned, the eRCP project would focus on adapting the 
        following Eclipse components for the embedded space:</p>
      <ul>
        <li>eSWT</li>
        <li>The Extension Point Framework</li>
        <li>eJface</li>
        <li>eWorkbench</li>
        <li>eUpdate</li>
      </ul>
      <p> Much like the desktop RCP, wherever possible, each of these components 
        should be useable independently of the other components. More specifically, 
        eSWT should have no dependencies on the other components of eRCP that 
        would preclude its (eSWT's) usage in non-eRCP use cases.<br>
        <br>
        Each of these components and their associated goals are discussed below.<br>
        <br>
        <b>eSWT Scope </b><br>
        Create a &quot;fit-for-purpose,&quot; embedded version of SWT with the 
        following characteristics:</p>
      <ul>
        <li>Create a set of mobile-phone-centric components. Other vertical industry 
          specific components may also be addressed depending on the interests 
          of the community.</li>
        <li>Evaluate the architecture of SWT and how it might be improved, with 
          an emphasis on runtime efficiencies, re-factoring, and footprint. </li>
        <li> Retain the design philosophy of SWT (i.e. SWT layers a &quot;native&quot; 
          user-interface, where &quot;native&quot; means the device's built-in 
          user interface).</li>
        <li>Create a proper subset of SWT that can be used in the following ways: 
          <ul>
            <li>As the basis of a primary user interface.</li>
            <li> As the basis for more abstract user-interface paradigms (e.g. 
              an XML, or other declarative UI model).</li>
            <li> As a porting layer for other &quot;fit-for-purpose,&quot; embedded, 
              Java user interfaces.<br>
            </li>
          </ul>
        </li>
        <li>Create multiple ports to embedded user-interfaces. Possible candidates 
          include Linux/GTK, Symbian/Series 90, etc.) </li>
      </ul>
      <p>For more details, see the following documents:</p>
      <ul>
        <li><a href="eSWT_Requirements_v0_9_2.pdf">eSWT Requirements and High-Level 
          Architecture</a> (.pdf)</li>
        <li><a href="eSWT_UI_Proposal_v0_9_5.pdf">The eSWT User Interface - High-Level 
          Design</a> (.pdf)</li>
        <li><a href="SWT_Mobile_Extensions_v0_9_3.pdf">Mobile Extensions for SWT 
          - High-Level Design</a> (.pdf)</li>
      </ul>
      <p><b>Extension Point Framework</b><br>
        Utilize the Extension Point Framework as the basis of creating embedded 
        client platforms. Possible work items include the following:<br>
      </p>
      <ul>
        <li>Evaluate the architecture of the Extension Point Framework and how 
          it might be improved, with an emphasis on runtime efficiencies, re-factoring, 
          and footprint</li>
      </ul>
      <p><b>eJFace</b><br>
        Utilize JFace as a means for providing model, view, controller paradigm 
        widgets (i.e. DataGrid, DataTree). Possible work items are as follows</p>
      <ul>
        <li>Evaluate which parts of JFace are desktop only oriented and can be 
          removed for footprint efficiency.</li>
        <li>Evaluate how JFace might be improved to provide better DataGrid functionality.<br>
        </li>
      </ul>
      <p><b>eWorkBench</b><br>
        We believe concept of the &quot;workbench&quot; from the RCP effort is 
        applicable to the embedded market; however, there are some significant 
        differences too-the embedded workbench is likely to be device or industry 
        specific. For example, it is unlikely that a PDA &quot;workbench&quot; 
        would be the same as the workbench of a mobile phone.<br>
        <br>
        The scope of this project will concentrate on the following areas:</p>
      <ul>
        <li>Identify the &quot;core pieces&quot; of the Workbench that are broadly 
          applicable to as many devices as possible.</li>
        <li>Create at least two eWorkbench implementations as reference implementations 
          (i.e. a &quot;quarter-VGA, PDA&quot; workbench and a &quot;smart phone&quot; 
          workbench.</li>
        <li>Identify common extension points applicable to as many workbenches 
          as possible.<br>
        </li>
      </ul>
      <p><b>eUpdate</b><br>
        One of the strengths of the RCP is that is can be updated &quot;in the 
        field.&quot; This subproject would look at using the &quot;Update component&quot; 
        of the RCP and extend its use for embedded devices.</p>
      <p>&nbsp;</p>
      <p>Goals</p>
      <ul>
        <li>Whenever possible, re-factor the existing Platform's code to be efficient 
          in the embedded space while maintaining structure and API signatures.</li>
        <li>Maintain consistency with Eclipse Platform </li>
      </ul>
      <p><b>Non-goals</b></p>
      <ul>
        <li>Substantial change to the code that creates an apparent &quot;fork&quot; 
          or substantial change to the Platform.<br>
        </li>
      </ul>
    
  


   
    Organization
  
   
     
      <p>Given the differences between embedded devices and desktop computers, 
        we propose that this project be undertaken as a Technology Subproject 
        rather than part of the Eclipse Platform Project. Being a Technology subproject 
        gives the project room to experiment with the technology without disruption 
        to the mainline Eclipse developers and users. In addition, we feel that 
        it is vitally important to involve developers from the embedded market 
        so we create a truly &quot;fit-for-purpose&quot; embedded client platform.<br>
        <br>
        With that said, it is also important to retain as much continuity as possible 
        with the existing Eclipse Platform; therefore, we propose that existing 
        committers from the Eclipse Platform also be part of this project.<br>
        <br>
        Finally, since Technology Sub-projects are not meant to be ongoing, there 
        are three possible evolutionary paths for the eRCP subproject:<br>
      </p>
      <ol>
        <li>The project is merged back into the mainline Eclipse RCP project. 
          In other words, the eRCP work identifies a clean, proper subset of existing 
          Eclipse code, and the existing Eclipse code is &quot;fit-for-purpose&quot; 
          for embedded or refactored to be &quot;fit-for-purpose.&quot;</li>
        <li>The investigative work down by eRCP determines that RCP can not be 
          made &quot;fit-for-purpose&quot; for embedded.</li>
        <li>Sufficient interest and uptake of the Eclipse community drive creation 
          of a new top-level project for embedded. </li>
      </ol>
      <p>These three paths are not mutually exclusive. Some combination of some 
        or all of these paths may be the result of the work done in this sub-project. 
      </p>
      <p><b>Suggested Project Leads and Committers</b><br>
        This section captures the list of companies that have expressed interest 
        in the project and/or its components, and as such will be updated periodically 
        to reflect the growing interest in this project.<br>
        <br>
        Rather than canvas the Eclipse community at large, the submitters of this 
        proposal welcome interested parties to post to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ercp">eclipse.technology.ercp 
        newsgroup</a> and ask to be added to the list as interested parties or 
        to suggest changes to this document.<br>
      </p>
    
  
  
 <table BORDER=0 CELLSPACING=5 CELLPADDING=2 WIDTH="90%" >
  <tr>
    <td valign="top" width="22%"><b>Interim Leads</b></td>
    <td valign="top" width="78%"> </td>
  </tr>
  <tr>
    <td valign="top" width="22%">Jim Robbins, IBM<br>
      robbinsj <b>at</b> us.ibm.com<br>
      +1 512.838.9495 </td>
    <td valign="top" width="78%">Mark VandenBrink, IBM<br>
      vmark <b>at</b> us.ibm.com<br>
      +1 512.838.9262 <br>
      <br>
    </td>
  </tr>
</table>

   
     
      <div align="left"><b>Component</b> </div>
    
    <b>Interested Parties</b>
  
   
     
      <div align="left">eSWT </div>
    
    <i>Nokia, Motorola, IBM</i>
  
   
     
      <div align="left">eJFace </div>
    
    &nbsp;
  
   
     
      <div align="left">Extension Point Framework</div>
    
    &nbsp;
  
   
     
      <div align="left">eWorkbench </div>
    
    &nbsp;
  
   
     
      <div align="left">eUpdate</div>
    
    &nbsp;
  

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

