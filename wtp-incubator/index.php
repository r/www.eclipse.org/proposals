<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle      = "Proposal for WTP Incubator Project";
$pageKeywords   = "Eclipse Web Tools WTP Project Proposals";
$pageAuthor     = "David Williams";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1>WTP Incubator Project</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("WTP Incubator Project");
?>


<H2>Purpose</H2>
<P>This proposal is to create a generic, general purpose WTP Incubator
Project. It is similar to the <A
	HREF="http://www.eclipse.org/proposals/platform-incubator/">Proposal
for Eclipse Project Incubator</A> which was reviewed and approved in
March, 2007.</P>
<P>The WTP Incubator Project would be the home for any incubating
component work from any project in the <A
	HREF="http://www.eclipse.org/webtools/">WTP Top Level Project</A>. See
<A
	HREF="http://www.eclipse.org/webtools/devProcess/wtpProjects/WTP%20Projects.pdf">Eclipse
Web Tools Platform Projects</A> for the current description of the 11
WTP Projects. In the table of 8 primary projects below, I've omitted
ATF, since it itself is incubating and RDB, since it is quiescent, and
Release Engineering since, well, since it's release engineering.</P>

<p>Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools.platform">http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools.platform</a> newsgroup. 

	<P ALIGN=CENTER
		STYLE="margin-top: 0.24in; margin-bottom: 0.24in; background: transparent; font-style: normal; page-break-inside: auto">
	<B>Primary Projects and Project Leads</B></P>


<CENTER>
<TABLE WIDTH=80% BORDER=1 BORDERCOLOR="#000000" CELLPADDING=2
	CELLSPACING=0 STYLE="page-break-inside: avoid">
	<COL WIDTH=154*>
	<COL WIDTH=102*>
	<TR VALIGN=TOP>
		<TH WIDTH=60%>
		<P>Project</P>
		</TH>
		<TH WIDTH=40%>
		<P>Project Lead</P>
		</TH>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=60%>
		<P>Common: tools and infrastructure not directly related to web tools,
		but required by web tools</P>
		</TD>
		<TD WIDTH=40%>
		<P>Konstantin Komissarchik, BEA</P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=60%>
		<P>Server Tools: tools and infrastructure to define and interact with
		servers.</P>
		</TD>
		<TD WIDTH=40%>
		<P>Tim Deboer, IBM</P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=60%>
		<P>Source Editors: xml, dtd, xsd (and sse infrastructure) html, css,
		javascript, jsp</P>
		</TD>
		<TD WIDTH=40%>
		<P>Nitin Dahyabhai, IBM</P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=60%>
		<P>Web Services: wsdl, axis1, axis2, web services framework, web
		services explorer</P>
		</TD>
		<TD WIDTH=40%>
		<P>Kathy Chan, IBM</P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=60%>
		<P>Java EE Tools: Common Project Infrastructure, jee models,
		preferences, classpath model, publish api, refactoring</P>
		</TD>
		<TD WIDTH=40%>
		<P>Chuck Bridgham, IBM</P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=60%>
		<P>EJB Tools: EJB creation wizards, preferences, future annotation
		tools</P>
		</TD>
		<TD WIDTH=40%>
		<P>Naci Dai, eteration</P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=60%>
		<P>JSF Tools: infrastructure and tools for JSF</P>
		</TD>
		<TD WIDTH=40%>
		<P>Raghu Srinivasan, Oracle</P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=60%>
		<P>Dali (JPA Tools): infrastructure and tools for JPA applications</P>
		</TD>
		<TD WIDTH=40%>
		<P>Neil Hauge, Oracle</P>
		</TD>
	</TR>
</TABLE>
</CENTER>
<H2></H2>
<H2>Expected Advantages</H2>
<P>The following are some of the primary motivations for formalizing a
WTP Incubator Project.</P>
<H3>Agile IP Policy</H3>
<P>The new (2007) Eclipse Foundation IP guidelines for projects under
incubation have taken into account the special needs of projects in
incubation. See section 1V of the <A
	HREF="http://www.eclipse.org/org/documents/Eclipse_IP_Policy.pdf">Intellectual
Property Policy</A> (DUE DILIGENCE AND RECORD KEEPING) given the project
well identifies itself as incubating, see <A
	HREF="http://www.eclipse.org/projects/dev_process/parallel-ip-process.php">Guidelines
for Using the Parallel IP Process.</A></P>
<H3>Lower barrier for new contributors</H3>
<P>Potential for improved community involvement as it can make it easier
to get started with contributions.</P>
<H3>Improved Cross-Project work</H3>
<P>Can potentially provide an area for experimental work to be conducted
and reviewed for improved cross-project operation, before firm
commitments made to main line code.</P>
<H2>General Examples</H2>
<UL>
	<LI>
	<P>Dependencies on external IP that has not yet cleared the full
	Eclipse Foundation IP process</P>
	
	
	<LI>
	<P>Experimental work that needs many iteration and much investigation</P>
	
	
	<LI>
	<P>Potentially destabilizing work that requires extended community
	review</P>
	
	
	<LI>
	<P>Long term work that can not necessarily be contained within the next
	planned release</P>
	
	
	<LI>
	<P>Work from potential new committers which have a start with some new
	code, but not necessarily yet a long track record of sustained high
	quality Eclipse development</P>

</UL>
<H2>Specific examples</H2>
<P>Even though this is seen as a long term, ongoing need, there are a
few current cases where this WTP Incubating Project could be helpful:</P>
<UL>
	<LI VALUE=1>
	<P>XSL Tools (bugs 89469, 201954, 201951)</P>
	
	
	<LI>
	<P>TLD Editor (bug 196418)</P>

</UL>
<H2>Scope</H2>
<P>Focus on new development in areas that are relevant to and within
scope of any other WTP Project sub-project, which because of their
nature, would not be appropriate for immediate inclusion in the
sub-project, but which, by their nature, do still require good
visibility or wide availability.</P>
<P>Note, this is unlike, and no substitute for, the temporary branching
efforts, which most projects use from time to time, when they have some
short life changes to explore, usually just existing for a few weeks,
and usually not needing wide spread review or availability.</P>
<H2>Not in Scope</H2>
<P>A sandbox, but not a playground: only efforts that can reasonably be
expected to graduate at some point are appropriate.</P>
<P>It is also not intended to substitute for large, completely new
efforts which would deserve to be a new incubating project in its own
right.</P>
<H2>Mentors</H2>
<ul>
<li>Tim DeBoer, IBM</li>
<li>David Williams, IBM</li>
</ul>
<H2>Organization and Initial Committers</H2>
<P>The WTP Incubator Project Lead will be David Williams, IBM</P>
<P>The Initial committers, listed below, will consist of all the current
primary Project Leads in WTP plus Craig Salter (since one of the first
uses may be for XSL, for which Craig is the component Lead).</P>
<P>This distinguished group will form the seed committers to oversee the
success of this project, even if most have no initial contributions nor
immediate need for incubating code. While all current and future
projects in WTP can have incubating components here, all future
committers will need to be decided by this group, using the usual
Eclipse Project rules: either voted in by current committers, based on
the individual's public history of appropriate Eclipse contributions, or
a large component contributed, which itself would be approved by current
WTP Incubator Project committers, which would have to come with its own
list of initial committers.</P>
<UL>
	<LI VALUE=1>
	<P>Konstantin Komissarchik, BEA</P>
	
	
	<LI>
	<P>Tim Deboer, IBM</P>
	
	
	<LI>
	<P>Nitin Dahyabhai, IBM</P>
	
	
	<LI>
	<P>Kathy Chan, IBM</P>
	
	
	<LI>
	<P>Chuck Bridgham, IBM</P>
	
	
	<LI>
	<P>Naci Dai, eteration</P>
	
	
	<LI>
	<P>Raghu Srinivasan, Oracle</P>
	
	
	<LI>
	<P>Neil Hauge, Oracle</P>
	
	
	<LI>
	<P>David Williams, IBM</P>

</UL>
<H2></H2>
<H2>References</H2>
<P>There are several background documents that describe Eclipse Projects
in general.</P>
<P><A
  HREF="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse
Development Process</A> especially see the <A
  HREF="http://www.eclipse.org/projects/dev_process/development_process.php#6_2_3_Incubation">section
on incubation</A></P>
<P><A HREF="http://www.eclipse.org/projects/official-documents.php">Official
Documents Pertaining to Projects</A></P>
<P><A HREF="http://www.eclipse.org/org/documents/">Governance
Documents</A></P>


</div>
</div>
<?php
$html = ob_get_contents();
ob_end_clean();

# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

