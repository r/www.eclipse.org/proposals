<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Language Development Toolkit");
?>

                                 
                                    <h1>

            Language

            Development Toolkit</h1>                        

            


<br>

<P>This project proposal is in the <A HREF="/org/documents/Eclipse Development Process 2003_11_09 FINAL.pdf">Proposal

Phase</A> and is posted here to solicit community feedback,

additional project participation, and ways the project can be

leveraged from the Eclipse membership-at-large. You are invited to

comment on and/or join the project. Please send all feedback to the

<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ldt">news://news.eclipse.org</B>/eclipse.technology.ldt</B></a> newsgroup.</P>



            

            

            

                        <h2>Project

                                    Organization</h2>

            

            

                        

                                    <P>The Language Development Toolkit Project (LDT</B>) is a proposed open-source  Project

            under the <a href="/technology/">Technology PMC</a> of eclipse.org</B>. The Charter will describe the organization of the project, roles and

                                    responsibilities of the participants, and  development

                                    process for the project.</P>

                        

                        

            





            

                        <h2>Overview</h2>

            

            

                        

Eclipse provides a flexible and extensible framework for third parties

to implement solutions for authoring, editing, and building files in

a particular language. Increasingly, however, modern software artifacts

are concerned with multiple languages: sometimes within a single source

file (JSP, BPEL, Java annotation values), sometimes within a project

(schema and IDL bindings), and always in terms of the sheer number

of languages needed to create and deploy a production strength

enterprise application.</p>

 

The LDT</B> project proposes several areas of language development related

to the Java Development Toolkit (JDT), Structured Source Editor (SSE),

XML Schema Project (XSD), and other language-centric technologies

within the Eclipse family today. Components of the LDT</B> are designed to

interoperate with existing Eclipse language technologies,

where appropriate, and development of common language infrastructure

(versus copy-and-paste) should be considered a guiding principle.

            <ul>

 

              <li><b>XML-based Languages (XDSLs). </b>

Defining extensible semantic checking infrastructure that enables XML-based DSLs (domain specific languages) to be

easily defined and rapidly turned into a high fidelity editing experience. This includes exploring declarative

extensions of XML Schema as well as programmatic frameworks that extend basic XML analysis with domain-specific

semantics and customized editing operations.</li>

 

              <li><b>Enhanced support for XML Schema-to-Java bindings and other �translation to Java� use cases. </b>

IDL, Schema, grammars, and other languages often have Java code generation, but these �generated Java� cases are not

integrated into the JDT services or user experience. Work in this area includes generalizing the JDT�s project model

to multiple languages, providing scalable techniques for handling large numbers of generated Java source files, and

supporting the use of JSR 175 annotations to track the provenance of generated material, so that user type navigation

can cross language boundaries as seamlessly as it crosses file boundaries today.</li>

 

              <li><b>Shared Editing Infrastructure and Reified Language Modeling. </b>

Exploring potential reuse among the JDT, CDT, and SSE editing frameworks and the creation of a reified language

model that can inform a generic (but language aware) editing service. In addition to refactoring commonality,

the goal is to provide appropriate leverage to new languages that avoids a "copy and paste" approach to their

editing needs. This component will also target reusable language-neutral infrastructure, such as frameworks

for dependency and work queue management.</li>

 

              <li><b>Multi-language Debugging. </b>

Creating infrastructure for support debugging of scripting languages and for defining cross-language

breakpoint and stepping operations in the context of a multilingual stack frame.</li>

 

            </ul>



            





            

            

                        <h2>

            Project Organization</h2>

            

            

                        

                                    

              

                We propose that this project be

                undertaken as a Technology Subproject rather than part of the

                Eclipse Platform Project. Being a Technology subproject gives

                the project room to experiment with the technology without

                disruption to the mainline Eclipse developers and users. In

                addition, we feel that it is vitally important to involve

                developers from the embedded market so we create a truly

                &quot;fit-for-purpose&quot; embedded client platform.<br>

                <br>

                With that said, it is also important to retain as much

                continuity as possible with the existing Eclipse Platform;

                therefore, we propose that existing committers from the Eclipse

                Platform also be part of this project.<br>

                <br>

                Finally, since Technology Sub-projects are not meant to be

                ongoing, there are four possible evolutionary paths for the LDT</B> subproject:<ol>

                  <li>The project is merged back into the mainline Eclipse

                  Platform top-level project.</li>

                  <li>The project is moved to a permanent state as a Tools

                  Platform project.</li>

                  <li>The technology proves to be insufficient for Eclipse and

                  the work is set aside.</li>

                  <li>Sufficient interest and uptake of the Eclipse community

                  drive creation of a new top-level project for this technology. </li>

                </ol>

                <p>These  paths are not mutually exclusive. Some

                combination of some or all of these paths may be the result of

                the work done in this  project. </p>

                

              

            

        

            

            

                        

            





            

                        <h2>

                                    <p ALIGN="LEFT">

            Interested Parties</h2>

            

            

                        

                <p><b>Suggested Project Leads and Committers</b><br>

                This section captures the list of companies that have expressed

                interest in the project and/or its components, and as such will

                be updated periodically to reflect the growing interest in this

                project.<br>

                <br>

                Rather than canvas the Eclipse community at large, the

                submitters of this proposal welcome interested parties to post

                to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ldt">eclipse.technology.ldt</B> newsgroup</a> and ask to be added to the

                list as interested parties or to suggest changes to this

                document.</p>

                <p><b>Interim Leads</b><ul>

                  <li>Tim Wagner, BEA (twagner at bea.com)</li>

                  </ul>

                <b>Interested Companies</b><ul>

              <li><a href="http://www.bea.com/">BEA</a></li>

            </ul>

                            <p><b>Potential Committers</b></p>

            <ul>

              <li>Tim Hanson, BEA</li>

              <li>Jess Garms, BEA</li>

              <li>Theodora Yeung, BEA</li>

              <li>Mike Kaufman, BEA</li>

              <li>Walter Harley, BEA</li>

              <li>Ted Carroll, BEA</li>

              <li>Gary Horen, BEA</li>

            </ul>

                        

            



<P><BR><BR>

</P>

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
