<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "Accessibility, Framework, Alternative Interface";
$pageAuthor		= "Kentarou Fukuda";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Accessibility Tools Framework (ACTF)</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("ACTF");
?>

<h2>Introduction</h2><p>The Accessibility Tools Framework (ACTF) is a proposed open source project to be incubated within the Eclipse Technology Project. This document describes the content and the scope of the proposed project.<p>This proposal is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and to declare its intent and scope. The proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.actf">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.actf</a> newsgroup.<h2>Background</h2><p>Assistive technologies help make applications and content accessible to people with disabilities. To accomplish this goal, accessibility standardization activities have been actively performed in various areas, and many institutions and organizations are introducing new criteria for assessing accessibility. <p>For example, to make Rich Internet Applications (RIAs) accessible, the World Wide Web Consortium (W3C) Web Accessibility Initiative (WAI)<a href="#ref1">[1]</a> is developing an Accessible Rich Internet Application (ARIA) suite<a href="#ref2">[2]</a> to help developers make Rich Internet Applications (RIAs) accessible through assistive technologies within Web browsers. To make office documents accessible, the Organization for the Advancement of Structured Information Standards (OASIS)<a href="#ref3">[3]</a> has recently completed accessibility enhancements in version 1.1 of the Open Document Format (ODF). IAccessible2, the new accessibility API contributed by IBM, is currently being standardized by the Free Standards Group Accessibility workgroup<a href="#ref4">[4]</a> to make screen readers fully capable of rendering content based upon accessibility standards for documents and applications. <p>One of the new problems in adopting these standards is the lack of tool support. In order to create documents and applications that are compliant with existing and emerging standards or sets of guidelines, tools for accessibility check and evaluation are needed to reduce the burdens on document authors and application/content developers. For the traditional HTML-based Web content, various accessibility checking tools have been developed resulting in a rich accessibility tools market. <p>Currently, sufficient tools do not exist for new standards and for non-Web rich-client applications and content. This problem may cause delays in the global adoption and use of these new standards. <h2>Project Goal</h2><p>The ACTF will provide an extensible framework and exemplary accessibility tools. The framework will allow users/developers to build and use various types of accessibility tools, such as compliance validation tools, assistive technology simulation tools, usability visualization tools, unit-testing tools, alternative interfaces for people with disabilities, and so on. These tools will be integrated into a single tooling environment on top of the Eclipse framework, and collaborate with each other to provide a comprehensive development environment. The framework will also include various types of middleware components for building accessibility-related tools as plugins.<p>The features included in this proposed project include those currently available through the alphaWorks<a href="#ref5">[5]</a> <span style="font-style: italic">aDesigner<a href="#ref6">[6]</a></span> and <span style="font-style: italic">Rules-based Accessibility Validation Environment (RAVEN)<a href="#ref7">[7]</a></span> technologies with significant additions. This Eclipse project would replace these technologies with an open-source solution partially based on components from these technologies.
<h2>Project Description</h2>
<div align="center">
<img src="ACTFcomponents.JPG" alt="" border="1"><br>
<span style="font-style: italic">Accessibility Tools Framework (ACTF) overview</span>
</div>
<p>
The ACTF will include the following components and exemplary
implementations:
<h3>Validation part</h3>
<h4>Validation manager</h4>
  <ul>
    <li>Manage validation rules and configure the validation engine
    based on target content or application and validation purpose.</li>

  </ul>
<h4>Extensible validation engine and validation rules</h4>
  <ul>
    <li>Validate accessibility of HTML, OpenDocument Format (ODF), Flash, Microsoft Active Accessibility (MSAA), IAccessible2, and Java applications</li>
    <li>Developers or users can create or customize validation rules by using sources such as XML documents and similar configuration files.</li>
    <li>Developers can also extend the validation logic by using Java APIs.</li>
  </ul>

<h3>Presentation part</h3>
<h4>Visualization engine</h4>
<h5>Blind usability visualization engine</h5>
  <ul>
    <li>Provide a visual representation of the blind users' usability of content by using reaching time estimation, accessibility error highlighting, and similar techniques. These methods help authors to quickly grasp accessibility and usability problems with their webpages.</li>
  </ul>
<h5>Image simulation engine</h5>
  <ul>

    <li>Simulate the vision of people with weak eyesight, color vision deficiencies, or cataracts.</li>
    <li>Simulate how presentations will appear to audiences under
    various conditions, such as in small meeting rooms, in
    auditoriums, etc. Authors can confirm whether or not an audience
    can read the text and/or see the figures of their
    presentations.</li>
  </ul>
<h4>Report generator</h4>
<ul>
 <li>Provide various kinds of reports based on validation results, visualization results, data models, and events.</li>
</ul>
<h4>View</h4>

<ul>
 <li>Offer reusable components that will be useful to create accessibility tools, e.g. report viewers, properties viewers, DOM inspector viewers, and so on.</li>
</ul>
<h3>Alternative interface part</h3>
<h4>Alternative UI transformer</h4>
  <ul>
    <li>Support improving the navigating and operating environments by using external metadata without changing the existing applications or content.</li>
  </ul>
<h4>Audio description &amp; caption service</h4>

  <ul>
    <li>Provide audio descriptions and captions to multimedia content
    by using text metadata.</li>
  </ul>
<h4>Multimedia controller</h4>
  <ul>
    <li>Make multimedia content controllable with unified shortcut keys
    even if the content does not support keyboard operations.</li>
    <li>Allow independent adjustment of each sound source (with
    separate sound levels for screen reading software and for
    streaming video).</li>

  </ul>
<h4>Text-to-Speech service</h4>
  <ul>
    <li>Provide interface to use TTS from the framework. (Currently, we support SAPI.)</li>
  </ul>

<h3>Infrastructure part</h3>
<h4>Model service</h4>
<ul>

  <li>Provide access to the runtime properties of the GUI components
  and document elements as well as to the accessibility information
  from applications running outside of the Eclipse platform. This will
  be done through the accessibility APIs, such as MSAA, IAccessible2
  and the Java Accessibility API.</li>
  <li>Provide access to the object models of multimedia content, such as Flash.</li>
  <li>By using this bridging service, application developers can easily test their application's accessibility (i.e. the implementations of their accessibility APIs) in the framework.</li>
</ul>
<h4>Model service (Editor Extension)</h4>
<ul>
  <li>Provide typical application plugins as Editor Extensions for targets such as a Firefox plugin, an Internet Explorer plugin for Web content, and an OpenOffice.org plugin for ODF.</li>
  <li>Provide a uniform interface to access these applications from the framework. This will include several UI control methods for validation and simulation.</li>

</ul>
<h4>Repository service</h4>
  <ul>
    <li>Permit reports to persist on media such as JDBC-driven databases and in file systems.</li>
  </ul>

<h3>Example implementation of tools</h3>
<h4>Accessibility check and visualization tool for HTML/ODF</h4>
  <ul>

    <li>This tool allows users to ensure that the content they create is accessible to individuals who are blind or visually impaired. </li>
    <li>By using the visualization and simulation functions of ACTF,
    authors will understand how low vision or blind users experience
    the content.</li>
  </ul>
<h4>MSAA/IAccessible2 tool</h4>
  <ul>
    <li>This tool allows users to confirm and check the accessibility
    information of applications via accessibility APIs such as MSAA or
    IAccessible2.</li>
    <li>This will make the accessibility information visible by using
    an object hierarchy view, a property view, and an event view. This
    tool will also provide quick validation, error visualization, and screen reader simulation functions.</li>

  </ul>
<h3>Example implementation of alternative interface runtimes</h3>
<h4>Alternative interface implementation for multimedia content</h4>
  <ul>
    <li>This will include a metadata-based accessible browser for Web content. Users can customize the behavior of existing Web content by using metadata.</li>
    <li>This will allow users to control multimedia content embedded
    in the Web. Users can play, pause, resume, and stop the embedded video by using
    unified shortcut keys. Users can also adjust the volume of an
    individual source to identify and listen to different sound
    sources without losing track of the screen-reading software because of
    the soundtrack of a video.</li>
  </ul>
<h4>Alternative interface implementation for ODF</h4>

  <ul>
    <li>Alternative interface for ODF documents. This will analyze the visual structure of presentation documents and create an optimized simple interface for screen reader users.</li>
  </ul>

<h2>Dependencies</h2>

<h3>Overall</h3>
  <ul>
    <li>Eclipse Platform 3.2+</li>

    <li>J2SE 5.0+</li>
  </ul>
<h3>Validation part:</h3>
  <ul>
   <li>AspectJ Weaver/Runtime v1.5.2: Eclipse Public License</li>
   <li>Commons-Configuration, Commons-BeanUtils: Apache Software License v2.0 </li>
   <li>Bean Scripting Framework (BSF) v2.3.0: Apache Software License v2.0</li>

   <li>Rhino: Mozilla JavaScript for Java, v.1.6.2, Mozilla Public License 1.1</li>
  </ul>
<h3>Infrastructure part:</h3>
  <ul>
    <li>Eclipse ATF Mozilla Browser widget v0.2.0: Eclipse Public License</li>
    <li>Java XPCOM v1.8.2: Mozilla Public License</li>
  </ul>

<h2>Design</h2>
<div align="center">
<img src="ACTFoverview.JPG" alt="" border="1"><br>
<span style="font-style: italic">Accessibility Tools Framework (ACTF) System Architecture</span>
</div>
<p>Main components and key interfaces:</p>
<ul>
 <li><span style="font-weight: bold">Mediator:</span> Provide useful functions to manage components and dataflows in the framework by collaborating with the Eclipse framework.</li>
 <li><span style="font-weight: bold">Model service:</span> Support access to runtime properties of GUI components or document elements as well as to accessibility information from standard applications running within or outside of the Eclipse platform. The framework will include typical application plugins as editor extensions, such as Internet Explorer and/or Firefox plugins for Web content and an OpenOffice.org plugin for ODF. Each component of this service will implement a common interface IModelService. Other components in the framework can access model services by using this interface.</li>

 <li><span style="font-weight: bold">Validation engine:</span> The framework will provide an extensible accessibility validator. Developers/users can customize the validation rules by using XML configuration files or through APIs. Developers also can extend the validation engine by using the interfaces and APIs provided in the framework. For example, Developers could add additional bridges or sources services. Other components in the framework could access validation engines through mediators by using the IValidator interface.</li>
 <li><span style="font-weight: bold">Visualization engine:</span> Provide the basic functions to visualize and simulate the accessibility/usability for people with disabilities. Visualization engines will implement a common interface, IVisualizer.</li>
 <li><span style="font-weight: bold">Report generator:</span> Provide
 various kinds of reports based on validation results, visualization
 results, data models, events, and so on.</li>
<li><span style="font-weight: bold">View:</span> Offer reusable UI components that are useful to create accessibility tools, e.g. report viewers, DOM inspectors, and so on.</li>

 <li><span style="font-weight: bold">Text-to-Speech service:</span>
 Allow developers to use TTS engines by using unified interface
 IVoice from the framework.</li>
</ul>

<h2>Code contributions</h2>
<p>IBM will make an initial code contribution that will encompass the core functionality for the ACTF project including: </p>
<ul>
<li><span style="font-weight: bold">Extensible accessibility validation engine</span><br>
An extensible accessibility validation engine with support for HTML, OpenDocument Format (ODF), Flash, Java Swing, Eclipse SWT, and accessibility APIs such as Microsoft Active Accessibility (MSAA) and IAccessible2.
Developers/users can customize validation rules by using XML configuration files or through APIs.</li>

<li><span style="font-weight: bold">Blind visualization engine</span><br>
Visualizes the blind users' usability for the content by using reaching time estimation, accessibility error highlighting, etc. It helps authors to quickly understand the accessibility and usability of their pages.</li>
<li><span style="font-weight: bold">Image simulation engine</span><br>
Simulates the views of people with weak eyesight, color vision
deficiencies, or cataracts. It also simulates how presentations will appear to audiences in various situations, such as in a small meeting room, in an auditorium, etc.</li>

<li><span style="font-weight: bold">Infrastructure</span><br>
The framework will also include various types of middleware for
building tool plugins, such as a mediator, a model service and
a repository service.</li>

<li><span style="font-weight: bold">Alternative interface implementation for ODF documents</span><br>
Analyzes the visual structure of presentation documents and creates an optimized simple interface for screen reader users.</li>

<li><span style="font-weight: bold">Alternative interface implementation for multimedia content</span><br>
Allows screen reader users to control embedded multimedia content,
with commands to play, stop, or control the volume for streaming video, by simply using predefined shortcut keys. Users can customize the behavior of existing Web content by using metadata. This also adds audio descriptions for Internet movies based on a simple text script.</li>
</ul>

<h2>Development Plan</h2>
<p><span style="font-style: italic">Note: This is a tentative plan. We need more discussion.</span></p>

<ul>
 <li>3Q 2007:
  <ul>

   <li>Process input from community to enhance functionality and documentation.</li>
   <li>Enhancements currently under consideration include: 
    <ul>
     <li>Refinement of APIs for embedded runtimes.</li>
     <li>Support for additional runtimes, such as IBM Productivity Tools. </li>

     <li>Support for W3C WCAG 2.0.</li>
    </ul>

   </li>
   <li>Initial prototype version will be available.
    <ul>
     <li>Exemplary implementation (MSAA/IAccessible2 tool) on top of ACTF.</li>
    </ul>
   </li>

  </ul></li>
 <li>4Q 2007: 
  <ul>

   <li>Projected first release, which will include some of the components and exemplary tools/runtimes mentioned in previous sections.</li>
  </ul>
 </li>
 <li>Future plans
  <ul>
   <li>Enhancing W3C WAI-ARIA-related functions.</li>
   <li>Linux support</li>
   <li>Expand supported runtimes</li>

   <li>Web crawler support</li>
   <li>JDBC support for repositories</li>
  </ul>
 </li>
</ul>

<h2>Organization</h2>
<h3>Mentors</h3>
<ul>
<li>Ed Merks, IBM (merks@ca.ibm.com)</li>
<li>Naci Dai, Eteration (naci.dai@eteration.com)</li>
</ul>
<h3>Proposed project leaders</h3>
<p>The project leaders will initially be the following people: 

<ul>
 <li>Chieko Asakawa, IBM (chie@jp.ibm.com), Co-Lead</li>
 <li>Mike Paciello, The Paciello Group, (mpaciello@paciellogroup.com), Co-Lead</li>
</ul>

<h3>Interested Parties</h3>

<p>The following parties have expressed interest in extending the platform and contributing ideas, guidance, and discussion. Key contacts listed.</p>

<ul>

  <li>Actuate Corporation, US (Dan Melcher)</li>

  <li>Adobe Systems Incorporated, US (Andrew Kirkpatrick)</li>

  <li>BIRT Project, Eclipse Foundation (Paul Clenahan)</li>

  <li>BrailleNet, France (Dominique Burger)</li>

  <li>Center for Mathematics and Computer Science, Netherlands (Zeljko Obrenovic)</li>

  <li>IBM Corporation, US (Chieko Asakawa)</li>

  <li>International Webmasters Association/HTML Writers Guild (IWA/HWG), US (Roberto Scano)</li>

  <li>Japan Braille Library, Japan (Tetsuji Tanaka)</li>

  <li>Mozilla foundation, US (Frank Hecker)</li>

  <li>Royal National Institute of Blind People (RNIB), UK (Henny Swan)</li>

  <li>SAP AG, Germany (Michael Bernhardt)</li>

  <li>SAS Institute Inc., US (Lisa Pappas)</li>

  <li>SIG-Universal Access to the Internet (UAI), Internet Technology Research Committee (ITRC), Japan (Takayuki Watanabe)</li>

  <li>State University of New York at Stony Brook, US (I.V. Ramakrishnan)</li>

  <li>The Carroll Center for the Blind, US (Brian Charlson)</li>

  <li>The Paciello Group, US (Mike Paciello)</li>

  <li>Technosite (ONCE Foundation), Spain (Alan Chuter)</li>

  <li>Tokyo Institute of Technology, Japan (Ken Wakita)</li>

  <li>University of Manchester, UK (Simon Harper)</li>

  <li>University of Toronto, Canada (Jutta Treviranus)</li>

  <li>University of Washington, US (Jeffrey Bigham)</li>

  <li>Vision Australia, Australia (Andrew Arch)</li>

  <li>Web Accessibility Tools Consortium (WAT-C), (Makoto Ueki)</li>

</ul>


<h3>Potential Committers</h3>

<ul>

  <li>Kentarou Fukuda, IBM (kentarou@jp.ibm.com)</li>

  <li>Mike Squillace, IBM (masquill@us.ibm.com)</li>

  <li>Hironobu Takagi, IBM (takagih@jp.ibm.com)</li>

  <li>Hisashi Miyashita, IBM (himi@jp.ibm.com)</li>

  <li>Takashi Itoh, IBM (JL03313@jp.ibm.com)</li>

  <li>Bill Carter, IBM (wscarter@us.ibm.com)</li>

  <li>Steve Faulkner, The Paciello Group (sfaulkner@paciellogroup.com)</li>

  <li>Will Pearson, The Paciello Group (wpearson@paciellogroup.com)</li>
  
  <li>Zeljko Obrenovic, Center for Mathematics and Computer Science (Zeljko.Obrenovic@cwi.nl)</li>

  <li>Roberto Scano, IWA/HWG (rscano@iwa-italy.org)</li>

  <li>I.V. Ramakrishnan, State University of New York at Stony Brook (ram@cs.sunysb.edu)</li>

  <li>Simon Harper, University of Manchester (Simon.Harper@manchester.ac.uk)</li>

  <li>David Bolter, University of Toronto (david.bolter@utoronto.ca)</li>

  <li>Simon Bates, University of Toronto (simon.bates@utoronto.ca)</li>

  <li>Jeffrey Bigham, University of Washington (jbigham@cs.washington.edu)</li>

  <li>Makoto Ueki, WAT-C (ueki@infoaxia.co.jp)</li>

</ul>

<h2>References</h2>
<ul>
<li><a name="ref1"></a>[1]: World Wide Web Consortium (W3C) Web Accessibility Initiative (WAI), <a href="http://www.w3.org/WAI/">http://www.w3.org/WAI/</a>
<li><a name="ref2"></a>[2]: Accessible Rich Internet Application (ARIA) Roadmap, <a href="http://www.w3.org/TR/aria-roadmap/">http://www.w3.org/TR/aria-roadmap/</a>
<li><a name="ref3"></a>[3]: Organization for the Advancement of Structured Information Standards (OASIS), <a href="http://www.oasis-open.org/">http://www.oasis-open.org/</a>
<li><a name="ref4"></a>[4]: Free Standards Group Accessibility workgroup, <a href="http://www.linux-foundation.org/en/Accessibility">http://www.linux-foundation.org/en/Accessibility</a>
<li><a name="ref5"></a>[5]: IBM alphaWorks, <a href="http://www.alphaworks.ibm.com/">http://www.alphaworks.ibm.com/</a>

<li><a name="ref6"></a>[6]: aDesigner, <a href="http://www.alphaworks.ibm.com/tech/adesigner">http://www.alphaworks.ibm.com/tech/adesigner</a>
<li><a name="ref7"></a>[7]: IBM Rules-based Accessibility Validation Environment (RAVEN), <a href="http://www.alphaworks.ibm.com/tech/raven">http://www.alphaworks.ibm.com/tech/raven</a>
</ul>

      </div>
  </div>

<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
