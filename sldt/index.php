<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Eclipse Tools for Microsoft Silverlight - Component Proposal";
$pageKeywords	= "sldt";
$pageAuthor		= "Yves YANG";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Silverlight Development Toolkit - SLDT</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Silverlight Development Toolkit");
?>

<h2>Introduction</h2>

<p><span lang="EN-US" xml:lang="EN-US"><a href="http://silverlight.net/">Microsoft Silverlight</a> is a cross-browser, cross-platform, and cross-device plug-in for   delivering the next generation of media experiences and rich interactive   applications for the Web. </span></p>
<p><span lang="EN-US" xml:lang="EN-US">Eclipse is an   open source community, whose projects are focused on building an open   development platform comprised of extensible frameworks, tools and runtimes for   building, deploying and managing software across the   lifecycle.&nbsp;</span></p>
<p><span lang="EN-US" xml:lang="EN-US">Both Microsoft   Silverlight and Eclipse technologies focus on delivering a rich, differentiating   and interoperable environment to their respective audiences: Web Users and   Developers respectively.</span></p>
<p><span lang="EN-US" xml:lang="EN-US">By integrating   the Microsoft Silverlight technology with the Eclipse platform, this project   proposes to extend the Silverlight Development Experience to all Web Application   Developers should they be running Windows, Linux or Mac OS.</span></p>
<p>
The Silverlight Development Toolkit (SLDT) is a proposed open-source project under the  
<a href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse Development Process</a>, and will incubate
under the top-level Technology project. This proposal is posted here to solicit community 
feedback and additional project participation. You are invited to comment on and/or 
join the project. Please send all feedback to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.sldt"> eclipse.sldt newsgroup</a>.
</p>
<h2>Scope</h2>
<p>The   purpose of this project is to provide a cross-platform development environment to build the RIA Web application on Microsoft Silverlight. It consists in a set of Eclipse plug-ins that address two main issues:
Silverlight applications development, and easing the integration of Java-based web sites and services into Silverlight applications.
<h3>Silverlight   applications development</h3>
<p>This environment provides the all necessary tools to simplfy the RIA Web development on Microsoft Silverlight. It is composed of the following  groups of tools:</p>
<ul>
  <li><span lang="EN-US" xml:lang="EN-US">Silverlight   project creation and resource management </span> </li>
  <li><span lang="EN-US" xml:lang="EN-US">XAML   visual editor with instant preview </span> </li>
  <li>Controller  class editors for most used programming languages such as C#, Java and JavaScript </li>
  <li>Build, run and debug</li>
</ul>
<h3>Integration between Silverlight and Java-based web sites and services</h3>
<p>In today&rsquo;s heterogeneous computing environments and with the  emergence of Web services and service-oriented architectures, interoperability  has become a critical business and IT need. The interoperability between  Silverlight and Java middle wares is the main focus of this project. Based on  the standard technologies, this project will propose the common patterns with  necessary tools, best practices, or out-of-the box solutions to help developers  to connect quickly the RIA Web application on Microsoft Silverlight with  Java-based enterprise services.</p>
<h2>Relationship with Other Eclipse Projects/Components</h2>
<p><span lang="EN-US" xml:lang="EN-US">SLDT     leverages the Eclipse platform blocks to provide features such as resource   management, source edition, code generation, build and launch of applications.   WTP components are the most heavily used. </span></p>
<p><span lang="EN-US" xml:lang="EN-US">SLDT   also extends the following components:</span></p>
<ul>
  <li>XML editor</li>
  <li>Java Script editor</li>
  <li>HTML editor </li>
</ul>
<h2>Organization</h2>

<h3>Mentors</h3>
<ul>
<li>Ed Merks</li>
<li>Wayne Beaton</li>
</ul>

<h3>Initial Committers</h3>
The initial committers for this component would be:
<ul>
  <li>Yves Yang (<a href="http://www.soyatec.com/">Soyatec</a>), proposed project lead</li>
  <li>Thomas Guiu (<a href="http://www.soyatec.com/">Soyatec</a>), Committer</li>
  <li>Bo Zhou (<a href="http://www.soyatec.com/">Soyatec</a>), Committer</li>
  <li>Bing  Qian (<a href="http://www.soyatec.com/">Soyatec</a>), Committer</li>
  <li>Jin Liu (<a href="http://www.soyatec.com/">Soyatec</a>), Committer</li>
  <li>Remy Chi Jian Suen (<a href="mailto:remy.suen@gmail.com">independent</a>), Committer</li>
</ul>
<h3>Code Contributions</h3>
<span lang="EN-US" xml:lang="EN-US">Soyatec offers an initial code donation, which </span><span lang="EN-US" xml:lang="EN-US">   is hosted on sourceforge: <a title="blocked::http://eclipse4sl.sourceforge.net/" href="http://eclipse4sl.sourceforge.net">http://eclipse4sl.sourceforge.net</a>. </span><span lang="EN-US" xml:lang="EN-US">The    SLDT implementation consists in Java packages : the org.eclipse.wtp.sldt.*   namespace. </span>
<h3><strong><span lang="EN-US" xml:lang="EN-US">Interested   Parties</span></strong></h3>
<p>Interest in this project has been expressed by</p>
<ul>
<li><a href="http://www.microsoft.com">Microsoft</a></li>
<li><a href="http://www.soyatec.com">Soyatec</a></li>
<li><a href="http://www.cas.de">CAS</a></li>
<li>Reinhold Bihler <a href="mailto:reinhold.bihler@gmx.de">reinhold.bihler@gmx.de</a></li>
<li><a href="http://www.effisoft.com">effisoft-group</a></li>
</ul>

<h3><strong><span lang="EN-US" xml:lang="EN-US">Developer   Community</span></strong></h3>
<p><span lang="EN-US" xml:lang="EN-US">The   team of initial committers will explore statements of interest from additional   developers experienced in Web and UI development or willing to gain such   experience. </span></p>
<h3><strong><span lang="EN-US" xml:lang="EN-US">User   Community</span></strong></h3>
<p><span lang="EN-US" xml:lang="EN-US">It   is expected that the user community for this component will consist primarily of   Web developers, given that it is essentially tools for building RIA Web   applications for the Microsoft Silverlight technology, connecting to Java   Business Logic.</span></p>
<h3><strong><span lang="EN-US" xml:lang="EN-US">Project    Plan</span></strong></h3>
<p>The first official release of the project will be released arround Janurary 2009. The goal of this release is to <span lang="EN-US" xml:lang="EN-US">meet the key requirements for Silverlight development in   Eclipse by providing a toolkit covering the entire development cycle   :</span></p>
<ul>
  <li><span lang="EN-US" xml:lang="EN-US">Silverlight   project creation and resource management </span> </li>
  <li><span lang="EN-US" xml:lang="EN-US">XAML   editor with immediate preview </span> </li>
  <li>C#   and JavaScript editor </li>
  <li>Build   and run </li>
</ul>
<p><span lang="EN-US" xml:lang="EN-US">More advanced features will be released in Spring 2009. Summer major release is planned for June 2009 aligned with annual release of Eclipse.</span></p>
<p>Afterwards, we will focus on interoperability between Silverlight and Java, but   also on bringing the development story to other platforms (Apple,   Linux&hellip;)</p>
<h3><?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
</h3>

</div>
</div>
