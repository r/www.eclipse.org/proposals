<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Java 2 CSharp translator</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Java 2 CSharp Translator");
?>

<H2>
  <SPAN>Introduction</SPAN>
</H2>
<P>
  <SPAN>The Java 2 CSharp translator is a proposed open source project under the </SPAN><A href=http://www.eclipse.org/technology/><SPAN>Eclipse Technology
  Project</SPAN></A><SPAN>.</SPAN>
</P>
<P>
  <SPAN>This proposal is in the Project Proposal Phase (as defined in the </SPAN><A href=http://www.eclipse.org/projects/dev_process/ id=es-317><SPAN>Eclipse
  Development Process document</SPAN></A><SPAN>)
  and is written to declare its intent and scope. This proposal is written to
  solicit additional participation and input from the Eclipse community. You are
  invited to comment on and/or join the project. Please send all feedback to the
  </SPAN><A href=http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.c-sharp>
  <SPAN>http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.c-sharp</SPAN></A><SPAN>
  newsgroup.&nbsp; </SPAN>
</P>
<H2>
  <SPAN>Background</SPAN>
</H2>
<P>
  <SPAN>With the .NET market growing, many
  Java developers and vendors want to offer a true native .NET version of their
  software. Since there is not a viable solution to have a single source policy
  for both platforms, and since the Java and CSharp languages are very similar,
  there is a growing need for conversion tool.</SPAN>
</P>
<P>
  <SPAN>Some solutions already exist, but most
  of them are too limited (i.e, to JDK1.3), have a "one shot" behavior, or are
  not well integrated in the development environment. </SPAN>
</P>
<P>
  <SPAN>Eclipse's highly flexible JDT
  framework, in particular its refactoring capability, has been an excellent
  start point for the Java 2 CSharp project.</SPAN>
</P>
<H2>
  <SPAN>Scope</SPAN>
</H2>
<P>
  <SPAN>The
  The Java 2 CSharp translator is an extensible framework for translating Java code 
  to any similar language and an exemplary tool to translate Java to CSharp. It is built 
  on top of the JDT; it uses and extends the powerful refactoring capability and it is 
  integrated in the Eclipse user interface. </SPAN>
</P>
<H2>
  <SPAN>Decription</SPAN>
</H2>
<P>
  <SPAN>The Java 2 CSharp translator goal is to allow automatic translation 
  of Java source files into CSharp source files.</SPAN>
</P>
<P>
  <SPAN>By design it's
  extensible (you can add your own "transformations"), customizable (you can add
  or modify the way a Java construct is mapped on a CSharp construct) and robust
  (already used in industrial environment at ILOG to produce the Rules for .NET
  business rules engine). </SPAN>
</P>
<P>
  <SPAN>As
  the user experience is important, translating code must be simple for a
  developer. The tool is integrated in the Eclipse UI via wizards and popup
  menus. The typical development phase workflow is as follows: write the Java
  code normally in Eclipse as normal, execute tests, and when needed, use a
  wizard to produce the C# version, compile it, and run tests.&nbsp; If needed,
  the UI integration helps the developer with customization, e.g., adding
  translator-aware Javadoc comments). </SPAN>
</P>
<P>
  <SPAN>For
  real-life projects, with many thousands of lines of code, a command line
  version is available to allow fully automatic translation using standard tools
  such as Maven.</SPAN>
</P>
<P>
  <SPAN>Major features:</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN><FONT>JDK6/Generics support</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>UI
  Integration</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Tests
  translation (from junit/testng to NUnit)</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Project
  translation</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Command
  line version</SPAN>
</P>
<H2>
  <SPAN>Organization</SPAN>
</H2>
<P>
  <B><SPAN>Initial
  committers</SPAN></B></FONT>
</P>
<P>
  <SPAN>The
  initial committers are:</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Alexandre
  FAU (ILOG): project lead</SPAN>
</P>
<P>
  <B><SPAN>Interested parties</SPAN></B>
</P>
<P>
  <SPAN>The following companies have expressed interest in this project. Key contacts
  listed.</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></SPAN></SPAN><SPAN>EMF4.NET : Reinhold Bihler</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></SPAN></SPAN><SPAN>ILOG : Alexandre FAU</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></SPAN></SPAN><SPAN>SCORT : Thomas Colin de Verdiere</SPAN>
</P>
<H2>
  <SPAN>Tentative Plan</SPAN>
</H2>
<P>
  <SPAN>The project is already available at Sourceforge
  (</SPAN><A href=http://sourceforge.net/projects/j2cstranslator/><SPAN>http://sourceforge.net/projects/j2cstranslator/</SPAN></A><SPAN>)
  with a developers/users community.&nbsp;</SPAN>
</P>
<P>
  <B><SPAN>Q4 2008:</SPAN></B>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Modify the tool
  to allow translating Java to other Java-like languages</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Eclipse 3.4 support</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Re-work the
  Maven integration</SPAN>
</P>
<P>
  <B><SPAN>Q2 2009:</SPAN></B>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Improve the
  mapping language to match AST subtrees instead of strings</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Look at the extension point mechanism for the extensibility</SPAN>
</P>
<P>
  <B><SPAN>Future directions:</SPAN></B>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Keep up with
  Java and CSharp evolution&nbsp;</SPAN>
</P>
<P>
  <B><SPAN>Mentors</SPAN></B>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Ed Merks</SPAN>
</P>
<P>
  <SPAN><SPAN>*<SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </SPAN></SPAN></SPAN><SPAN>Chris Aniszcyzk</SPAN>
</P>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
