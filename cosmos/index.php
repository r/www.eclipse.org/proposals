<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1><img src="CosmosVeryHighLevelOverview.gif" alt="Very high overview of COSMOS" title="COSMOS" align="left"/>
<u>CO</u>mmunity <u>S</u>ystems <u>M</u>anagement <u>O</u>pen <u>S</u>ource
(COSMOS) Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("COSMOS");
?>
<body>
<br/>
<h2>Introduction</h2>
<p>The COSMOS Project is a project proposed incubate under the supervision of the Technology PMC
 at Eclipse.org and once reaching full project status will become a top level project. 
This proposal is in the project Proposal Phase (as defined in
the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse
Development Process</a> document) and is written to declare its
intent and scope. This proposal is written to solicit additional
participation and input from the Eclipse community. You are invited
to comment and/or join the project. Once announced, please send all feedback to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.cosmos">eclipse.technology.cosmos</a> newsgroup.
The project will follow the structure and guidelines as outlined by the
Technology PMC. 
</p>

<h2>Background</h2>
<p>In the IT industry there is a long history of tools being developed
for specific roles or tasks. It has often been the case that these tools
did not integrate with each other, even though they are all working with
the same end resource. Systems management is no different. In the past,
there have been efforts to declare common API or data
formats to facilitate integration and open communications. These efforts
generally focused on a particular subset of resource types, execution
environments, or management domain. However, the activities involved in
managing the entire life cycle of an IT resource span a wide set of  domains.
Viewed holistically, systems management encompasses many disciplines and user roles that
can be divided into several high level categories, such as:</p>
<ol>
    <li><p style="margin-bottom: 0in">Health monitoring</p></li>
    <li><p style="margin-bottom: 0in">Inventory tracking</p></li>
    <li><p style="margin-bottom: 0in">Resource modeling</p></li>
    <li><p style="margin-bottom: 0in">Deployment modeling</p></li>
    <li><p style="margin-bottom: 0in">Deployment</p></li>
    <li><p style="margin-bottom: 0in">Dynamic configuration</p></li>
    <li><p style="margin-bottom: 0in">Configuration change tracking</p></li>
</ol>
<p>
COSMOS is being proposed to facilitate the first steps in the next evolutionary move in the
integration in the area of systems management. 
</p>

<h2>Project Overview</h2>
<p>The mission of the Eclipse COSMOS Project is to build a generic, extensible, standards-based
components for a tools platform upon which software developers can create specialized,
differentiated, and inter-operable offerings of tools for system management.</p>
<p>
It is important to focus the initial project efforts on specific problems. The domain of system management as shown 
above is very large and there is plenty of work to be done. Initially the key problem areas COSMOS 
intends to target can be described as follows.
</p>
<h3>Resource Monitoring</h3>
<p>
In the area of resource monitoring there are already many ways to observe information about common resources. In fact
many resource providers support several of the current standard APIs in an effort to publish informationto the widest audience 
possible. However there
is a constant trade off of performance and other factors against providing all the data to anyone requesting it. 
This has lead to support for purpose driven APIs have been standardized in order to try to focus the amount of 
data being requested. However although often APIs have been agreed and implemented, the data made available via these 
interfaces is not always consistent. The units of measure as well as the meaning of the terms are critical. For example is 
"available memory" of 2000 a measure of megabytes of RAM or gigabytes of disk space.
</p>
<p>
Historically it has been the case that each data consumer leveraged some mechanism, often a local agent, to manage it's access
to the resource through some published api, and these agents would be in conflict with each other, causing customers to 
pick specific vendor solutions which implicitly excluded others. Because the use of the data is often greatly varied, 
and vendors typically do not cover every single user use case, this is a significant problem for end users. Users were also 
left with the cost of ownership and administration of these agents as they had to mix and match agents with run times 
depending on the tools they wanted to use. End users are now demanding agentless environments when environments 
provide native collection systems.
</p>
<p>
In order to address this set of problems, COSMOS intends to provide a data staging server that will exploit existing 
infrastructure and APIs to access data and normalize it into a single agreed form. In cases where local agents are 
required existing infrastructure will be used to access the data. COSMOS will promote standards based 
data collection systems and work with the community to select the optimal standards to follow.
</p>
<p>
In order to provide a complete out of the box user experience, and to demonstrate the value of the data that can be collected,
COSMOS will also provide a BIRT based web interface as well as an RCP client. These user interfaces are being provided as
reusable examples for browser or thick client tools. These will provide an exemplary out of the box experience to the COSMOS user.
</p>
<h3>Transaction monitoring and correlation</h3>
<p>
There are times when data from multiple resources need to be correlated in order analyze one resource in the context of
another. A typical example is transaction decomposition of a distributed application. Standards such as ARM from Open Group
are intended to address this need, however this is an example of the previously mentioned purpose driven APIs. As a run time
component it provides a way to define correlation tokens and present them in logged events, however each implementation is
unique and the run time needed to pass the correlation data is redundant with each provider. 
</p>
<p>
COSMOS will leverage the work done by various run times and the TPTP project to advocate a standard correlation mechanism that
can be used in distributed decomposition scenarios. COSMOS will consume this correlation data and support the consumption and
provisioning of ARM events using this data. This will be done in the data collection component. 
</p>
<p>
In order to allow data providers, be they existing resources or new applications, to more easily develop any new code needed to
expose data through the standard interfaces COSMOS will provide developer targeted tools, if they do not exist, to simplify the task.
COSMOS will manage a dependancy on tools and services in other open source projects
when they exist rather than develop new ones. When COSMOS does need to develop technology in the area of enablement it will be
done in the build to manage component.
</p>
<h3>Resource Modeling</h3>
<p>
In most tools that monitor resources there is an awareness of some of the more static features of a resource as well. For example
the physical location and the IP address, in addition to the actual data being extracted from the resource is often known. Yet each 
tool has it's own representation of this information. With the growing presence of distributed and componentized applications this
information also includes the overall topology of the application. This is redundant information between the tool used to debug a 
performance bottleneck and the tool that is used to deploy components of the application, or the tool that is doing load testing
and monitoring of the application. Just as with the data collection scenarios discussed earlier there are also purpose driven formats
for this kind of information. An example is the SCA composition model which is intended to describe the composition and relationship
of the components making up the application, but at some point it is not intended to model the asset tags used in the inventory of a
data center or abstract design views of the application.
The resolution of this redundancy can once again be addressed by having a canonical representation of the resources. To that end
SML is proposed as a constrained extension of XML to be used as the language to interchange this kind of information. COSMOS will
provide a reference implementation of a Service Modeling Language Interchange Format (SML-IF) storage and validation system as 
well as basic tools to manipulate such data.
</p>

<h2>Organization</h2>
<p>
The project will contain several components;
monitoring user interface, data collection and server, build to manage, and resource modeling.
</p>
<ul>
	<li><b>Monitoring user interface</b>
	<br/>This component will develop a web based and stand alone RCP based user
	experience for monitoring data.</li>
	<li><b>Data collection and server</b>
	<br/>This component will exploit
	instrumentation and wrappers developed in the Build to Manage component and other existing infrastructure.
	This component has the primary purpose of normalizing and storing data that is
	received from various data collection and management systems. In other words a client
	of those systems that normalizes and stores the data.</li>
	<li><b>Build to Manage</b>
	<br/>This component will be dependant on other open source 
	instrumentation, and wrappers of applications and run times to enable data collection.
	The focus is enablement tooling, and advocating capabilites that already exist by being dependant on them.
	</li>
	<li><b>Resource Modeling</b>
	<br/>This component will be the home for a reference
	implementation of the emerging SML specification in the form of validators as well as common model
	instances and manipulation tools.</li>
</ul>


<h2> Scope</h2>
<p>
The  goal of this proposal is to generate interest among companies involved in systems management
so that the project can be defined by the community with an agreed upon scope. However,
it is key to practically set the limits of the project at the outset.
This project will deliver support for each of the categories described earlier, and do this via
the components previously outlined. This is a significantly large domain and the COSMOS end user function will
be limited to monitoring and data collection components needed to support application health monitoring and debugging as well as
initial support for validating and manipulating Service Modeling Language (SML) based models.
</p>
<p>
In building an ecosystem and fostering community growth, it is important to provide a fundamental level of management function
that offers immediate "out of the box" value to consumers and 
development tooling for  vendors, resource providers, and system administrators. This is the objective of the first release of Cosmos.
Following releases will add value for subsequent categories of use at the framework and end user function level.
</p>

<p>
Each component is complimented by set of development tools that facilitate the creation of
extensions or authoring of key artifacts, e.g. resource models, agent configurations, etc.
For example, the BIRT tooling can be used to create &quot;canned&quot; reports that can be
deployed in the Web UI or re-used within the RCP client.  The
components of COSMOS will expose extension points to allow for both run time extension as well as tooling value add.
</p>
<!-- 
<p>
A high level overview and brief description of some of the components contained in the Monitoring
User Interface component and the Data Collection and Server component are illustrated in the figure below.
</p>
<img src="CosmosOverview-v21.gif"/>
<br/>
<b>RCP and Web UI</b>
<p>An extensible GUI for COSMOS is an essential component of the project, as this will permit better
exploitation of the Eclipse plug-in/extension model. The project will enable the user interfaces 
currently available in the TPTP project as well as BIRT reports.
</p>
<p>
BIRT based reports will be combined with a static web interface for browser oriented users. The
reference implementation for this web support will be based on Tomcat and extract data from the
data server directly. SWT based UI implementations, such as what is in TPTP today will be provided
in a stand alone RCP configuration as well as integration workbench plugins. This will provide support
for fat/rich client use cases that may or may not need full integration with the other Eclipse IDE based tools.
</p>
<br/><b>Data Server</b>
<p>
The data server provides the ability to store data out of process for use by the RCP and Web UI, e.g. as a basis for the static reports.
This is a scaling up of the support currently provided in TPTP and is necessary for unattended collection of information over an extended duration.
</p>
<p>
There is no plan to provide typical management server capabilities in the reference
implementation. At this point in time
capabilities such as complex correlation, alerts and dynamic analysis are expected to be value add functions that can be provided
on top of the infrastructure developed in this project.
</p>
<p>
The reference implementation will be built to be deployable on several open relational database
technologies such as Derby and MySql.
</p>
<br/><b>Interchange Normalization</b>
<p>
Existing data that is available via various instrumentation and api will be transformed into a normalized form that will 
be stored and made available for consumption by various client and visualization tools.
</p>
 -->
 <h3>COSMOS as a Tool Platform</h3>COSMOS is intended to provide a fundamental level of management functionality to its users.
<p>
It will provide immediate "out-of-the-box" value with workable solutions for resource monitoring and modeling upon open standards.
These capabilities are useful in themselves for monitoring, and basic management needs,
but are even more useful as building blocks for constructing higher-level management functions.
The combination of tooling and run time offer a distinct advantage for COSMOS as no other open source offering
effectively covers  multiple aspects of the management life cycle.Integration with other Eclipse projects adds to the
application life cycle already supported by those projects.
This unique advantage over other open source projects directly benefit users who will extend the framework and tooling.
</p>
<h3>Initial Release Plan</h3>
<p>
COSMOS will follow a 7 week iteration cycle leading to a 1.0 release in March and a 1.5 release in June 2007 aligned 
with the Eclipse platform.
The initial commitment will be to deliver the systems monitoring support and SML validation described earlier. As more committers are
confirmed, the PMC will determine if more function can be provided on the same schedule.
The next priority after systems monitoring support will be resource modeling, beginning with support for the emerging SML standard.
The first iteration, due in Oct 06, will be focused on establishing the project infrastructure. Transferring function from other
eclipse projects such as TPTP, will be discussed as possible deliverable in 1.5.
The following are the current iteration complete dates.
</p>
<table>
<tr><td>Iteration</td><td></td><td>Comments</td></tr>
<tr><td>I0</td>          <td>11/09/06 - 27/10/06  </td><td> Establish infrastructure</td></tr>
<tr><td>I1</td>          <td>30/10/06 - 15/12/06  </td><td></td></tr>
<tr><td>1.0 Shutdown</td><td>15/01/07 - 02/03/07  </td><td> 1.0 GA</td></tr>
<tr><td>I2</td>          <td>15/01/07 - 02/03/07  </td><td> 1.5 Feature/API freeze</td></tr>
<tr><td>I3</td>          <td>05/03/07 - 20/04/07  </td><td> Documentation freeze</td></tr>
<tr><td>I4</td>          <td>23/04/07 - 01/06/07  </td><td> Candidate GA</td></tr>
<tr><td>1.5 Shutdown</td><td>02/06/07 - 29/06/07  </td><td> 1.5 GA</td></tr>
</table>

<h3>Initial Community</h3>
<p style="margin-bottom: 0in">The following companies are helping to shape the project and may contribute
committers to get the project started:</p>
<ul>
	<li>IBM (<a href="http://www.ibm.com/">www.ibm.com</a>)<br/>
	Toni Drapkin / Harm Sluiman / Mark Weitzel (acting PMC/AG co-lead)</li>
    <li>OC Systems (<a href="http://www.ocsystems.com/">www.ocsystems.com</a>)</li>
    <li>GroundWork (<a href="http://www.groundworkopensource.com/">www.groundworkopensource.com</a>)</li>
	<li>Cisco (<a href="http://www.cisco.com/">www.cisco.com</a>)</li>
	<li>Intel (<a href="http://www.intel.com/">www.intel.com</a>)</li>
	<li>Compuware (<a href="http://www.compuware.com/">www.compuware.com</a>)</li>	
</ul>
<h3>Interested parties</h3>
<p style="margin-bottom: 0in">The following companies have expressed
interest in the project:</p>
<ul>
	<li><p style="margin-bottom: 0in">HP (<a href="http://www.hp.com/">www.hp.com</a>)</p></li>
    <li><p style="margin-bottom: 0in">Sybase (<a href="http://www.sybase.com/">www.sybase.com</a>)</p></li>
    <li><p style="margin-bottom: 0in">your company name here </p></li>
</ul>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
