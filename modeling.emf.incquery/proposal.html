<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>EMF-IncQuery</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<p>The EMF-IncQuery project is a proposed open source project under the <a href="http://www.eclipse.org/projects/project_summary.php?projectid=modeling.emft">Eclipse Modeling Framework Technology (EMFT) Container Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<h2>Background</h2>

<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->

<p>
The EMF-IncQuery project supports (1) the declarative and reusable specification of  queries over EMF models and (2) the efficient search and retrieval of EMF model elements by maintaining an incremental cache of query results. The motivation for EMF-IncQuery is simple: while the EMF API already provides basic functionality, it lacks several features that are crucial both from the functionality as well as the performance viewpoints. As a result, such features are frequently implemented on an ad-hoc, case-by-case basis in today's EMF-based applications.
</p>

<p>
From a technical viewpoint, EMF-IncQuery addresses three key challenges:
<ul>
  <li><b>Simple backward getters are missing</b> from the EMF API, which are essential in many modeling applications.</li>
  <li>There is <b>no standard way to define complex queries</b> that concern multi-object configurations.</li>
  <li><b>Existing EMF query technologies</b> are not specifically optimized for in-memory scalability, and thus <b>have performance issues</b> when used with complex queries and large instance models.</li>
</ul>

<p>As queries are frequently called in many applications (such as UI back-ends, model transformations), an incremental, online evaluation approach is desirable to provide fast response times even for large models.</p>

<p>
EMF-IncQuery provides a light-weight framework built on an adaptation of the RETE algorithm that is well-known from expert systems. The key contributions of EMF-IncQuery are the following:
<ul>
 <li>It provides <b>efficient, incremental support for evaluating backward getters</b> over EMF models, aligned with existing types and conventions of EMF's APIs. With the IncQuery API, the enumeration of instances of a specific type or reverse navigation along references are instant, O(1) operations with minimal model modification overhead.</li>
 <li>It features a <b>high performance incremental query engine for complex queries</b>. As a key advantage, the evaluation times are practically independent of the complexity of the query and the size of the model. As a result, EMF-IncQuery can scale up to handling models containing millions of model elements with fast response times, even for very complex queries.</li>
 <li>It provides a <b>user friendly, declarative graph pattern-based query language</b> and tools for easy definition of queries. The query language is built on Xtext2 and can be integrated into a hierarchy of such DSLs.</li>
</ul>
</p>

<p>
EMF-IncQuery conceptually builds upon the research and development on incremental graph pattern matching carried out within the scope of the <a href="http://eclipse.org/gmt/VIATRA2/">VIATRA2 Eclipse GMT subproject</a>, which project started back in 2004 and has been continuously maintained since then.
</p>


<h2>Scope</h2>

<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->

<p>
EMF-IncQuery provides a new API for querying EMF models, an implementation of the API that leverages an incremental cache for fast retrieval, and a DSL and tools for specifying and testing queries. The emphasis is on scalability, which means fast (re-)execution even for complex queries and large models. EMF-IncQuery is intended to be a technological enabler, to provide a robust and scalable infrastructure on which higher level features such as well-formedness validation, view maintenance, or model synchronization can be built.
</p>




<!-- old stuff that I only left in for reference :
<p>The specific objectives of the EMF-IncQuery project are the following:

<ul>
 <li>Develop queries for EMF models
  <ul>
   <li>Expressive language (reusable queries, derived features)</li>
   <li>Advanced tools for query development</li>
  </ul>
 </li>
 <li>Efficiently execute queries over EMF models
  <ul>
   <li>Low-level incremental query API and high-level graph pattern query API</li>
   <li>Complexity and model size-agnostic performance characteristics thanks to the RETE engine</li>
  </ul>
 </li>
 <li>Seemlessly integrate with EMF applications
  <ul>
   <li>Deep integration with Ecore tools</li>
   <li>Easy-to-use APIs and extension points</li>
  </ul>
 </li>
</ul>

<p>EMF-IncQuery does not cover these issues:
<ul>
  <li>Dealing with models not based on EMF</li>
  <li>Querying not actively loaded (in-memory) EMF models</li>
  <li>Supporting OCL or any other query language</li>
</ul>
</p>
-->


<h2>Description</h2>

<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->
<p>The architecture of the <a href="http://viatra.inf.mit.bme.hu/incquery/">EMF-IncQuery framework</a> and tools is shown in Figure 1.</p>


<div>
<img src="incquery_arch.png" width="800"/>
<div><b>Figure 1: The EMF-IncQuery architecture</b></div>
</div>

<h3>Runtime and API</h3>

<p>The complex query evaluator of the EMF-IncQuery framework is built on a graph pattern matching engine that uses the <a href="http://dl.acm.org/citation.cfm?doid=1402947.1402953">RETE algorithm</a>, adapted from expert systems to facilitate the efficient storage and retrieval of partial views of graph-like models. EMF-IncQuery also features the <a href="http://viatra.inf.mit.bme.hu/incquery/base">Base</a> component that provides incremental support for backward getters as well as transitive closures for the efficient computation of e.g. reachability regions. EMF-IncQuery relies on the EMF Notification facility to incrementally update its internal cache to guarantee the consistency of the result set with respect to the actual contents of the model.</p>

<p>As an interface over the core algorithms, the IncQuery runtime consists of:
 <ul>
   <li>a query / pattern matching API that provides a typesafe wrapper for instantiating and executing queries over EMF Notifiers (Resources, ResourceSets and EObject hierarchies)</li>
   <li>add-ons such as the <a href="http://viatra.inf.mit.bme.hu/incquery/examples/umlvalidation">Validation Framework</a> (for the specification and live checking of well-formedness rules that correspond to Eclipse Resource Markers) or <a href="http://viatra.inf.mit.bme.hu/incquery/examples/derivedfeatures">Derived feature support</a> (for the efficient evaluation of derived EAttributes and EReferences with IncQuery queries as back-ends)</li>
 </ul>
</p>

<p>The runtime is designed to be compatible with standard Eclipse-EMF configurations as well as a headless, <a href="http://viatra.inf.mit.bme.hu/incquery/examples/standalone">standalone</a> execution mode. The efficiency of the execution engine has been <a href="http://viatra.inf.mit.bme.hu/publications/models10">demonstrated</a> in an industrial setting as well as in several academic research case studies.</p>

<h3>Query language and tools</h3>

<p>The IncQuery tools consist of an Xtext-based pattern language editor, and a development UI that can be used to execute, test and debug queries within the Eclipse IDE. The tools can make use of the interpretative runtime API for on-the-fly query execution and testing, while it can also generate type-safe wrappers that ease the integration of queries into application code. The tools also interfacee with IncQuery's add-ons to generate feature-specific adapter code.</p>

<p>IncQuery's declarative queries can be evaluated over EMF models without manual (programmed) traversal. The <a href="https://viatra.inf.mit.bme.hu/incquery/language">query language</a> is built upon the concepts of graph patterns (which is a key concept in many transformation tools) to provide a concise, reusable and easy way to specify complex structural model queries. The key features are:
 <ul>
   <li><b>complex interconnections</b> of EMF objects can be easily formulated as a graph pattern,</li>
   <li>the language is <b>highly expressive</b> and provides powerful features such as negation or counting,</li>
   <li>queries can be evaluated with great freedom, i.e. <b>input and output parameters can be bound freely</b> at evaluation time,</li>
   <li>graph patterns are <b>composable</b>, making it possible to <b>build reusable query libraries</b>.</li>
 </ul>
</p>

<p>Available documentation already discusses numerous demonstrative <a href="http://viatra.inf.mit.bme.hu/incquery/examples">examples</a> as well as <a href="https://viatra.inf.mit.bme.hu/incquery/advanced">advanced issues</a>.</p>

<h2>Why Eclipse?</h2> 

<!-- 
	Answer these two questions:	What value does this project bring to the Eclipse
	community? What value do you expect to obtain from hosting your project at Eclipse?
	
	What value do you get by having your project at Eclipse over and above the value
	of hosting at Eclipse Labs?
 -->


<p>With EMF-IncQuery, our aim is to gain access to a much wider audience by designing the technology around the EMF ecosystem from the ground up. Based on OptXware's experience gained in a number of industrial tool development projects, we believe that IncQuery can add significant value to the foundation layer, that is, EMF itself. Additionally, many other EMF technologies can consume IncQuery technology, including:
 <ul>
   <li><a href="http://wiki.eclipse.org/EMF_Search">EMF Search</a>, <a href="http://wiki.eclipse.org/EMF/Query2">EMF Query 2</a> and <a href="http://wiki.eclipse.org/MDT-OCL">Eclipse OCL</a>: using IncQuery as an efficient back-end and index provider;</li>
   <li><a href="http://wiki.eclipse.org/EMF/Validation">EMF Validation</a>: interfacing with IncQuery's validation engine to provide more expressive and faster well-formedness rules;</li>
   <li><a href="http://wiki.eclipse.org/EEF">EEF</a>: using IncQuery as a back-end for UI components such as views, forms and editors.</li>
 </ul>
</p>

<h2>Relation to Other Eclipse Projects</h2>

<p>EMF-IncQuery builds on the foundations developed in another Eclipse.org project, the <a href="http://eclipse.org/gmt/VIATRA2/">VIATRA2 model transformation framework</a>. Our experience with VIATRA2, both in BUTE's academic research as well as OptXware's industrial projects, has already proven the values and benefits of open sourcing a versatile, enabling framework, under the EPL with Eclipse.org's guarantee of being IP risk free.</p>

<p>EMF-IncQuery's key distinguishing features compared to existing EMF model query technologies are:
 <ul>
  <li>EMF-IncQuery focuses on high-performance queries of <b>in-memory models</b>, whereas EMF Query and Query2 are concerned with querying workspace files or models persisted in databases;</li>
  <li>EMF-IncQuery features a unique, <b>graph pattern-based, fully declarative query language</b> that is more expressive than the languages of Query and Query2; its expressive power is comparable to OCL, but IncQuery supports query reuse and dynamic parameterization, making it better suited for general purpose queries;</li>
  <li>In contrast with both Query/Query2 and OCL, EMF-IncQuery is an <b>incremental query evaluation engine</b>, which means that once the results of a query have been computed, these are stored in a cache and continuously updated as the model changes, so that the consecutive result retrievals are instant operations.</li>
 </ul>
</p>

<h2>Initial Contribution</h2>

<!-- 
	Projects are expected to arrive at Eclipse with existing code.
	
	Describe the existing code that will be contributed to the project. Please provide
	a couple of paragraphs describing the code with modest detail, including important
	information like code ownership (who holds the copyright?), and some consideration
	of community that exists around the code. Include a listing of third-party libraries 
	and associated licenses.
 -->
 
<p>The initial contribution will be a set of plug-ins already under the EPL, consisting of the components and features described in the Description section (no external dependencies).</p>

<p>These are currently available at two sites and described in detail <a href="http://viatra.inf.mit.bme.hu/incquery/new">here</a>:
 <ul>
  <li>the official EMF-IncQuery website hosted by the Budapest University of Technolgy and Economics, <a href="http://viatra.inf.mit.bme.hu/incquery">http://viatra.inf.mit.bme.hu/incquery</a> and its corresponding SVN repository <a href="https://viatra.inf.mit.bme.hu/svn/incquery">https://viatra.inf.mit.bme.hu/svn/incquery</a> (archived for technical purposes at <a href="https://github.com/ujhelyiz/EMF-IncQuery-Old/">https://github.com/ujhelyiz/EMF-IncQuery-Old/</a>)</li>
  <li>The Git repository at GitHub, currently used for development: <a href="https://github.com/ujhelyiz/EMF-IncQuery/">https://github.com/ujhelyiz/EMF-IncQuery/</a></li> 
 </ul>
</p>

<h2>Legal Issues</h2>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->

<p>There are no outstanding legal issues concerning EMF-IncQuery.
 <ul>
  <li>The core IP (that is, the RETE implementation) stems directly from VIATRA2, which is already part of Eclipse and thus has been IP checked thoroughly.</li>
  <li>Other components have been developed from scratch, and licensed under the EPL from the start. We have a proven track record (<a href="http://viatra.inf.mit.bme.hu/incquery/publications">peer reviewed academic publications</a>) of the entire research and development process.</li>
  <li>All previous and current contributors have obtained Eclipse-compatible employer permissions.</li>
 </ul>
</p>
 
<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

<p>The following individuals are proposed as initial committers to the project:</p>

<dl>
  <dt>Dr. Istv&aacute;n R&aacute;th (rath@mit.bme.hu), Budapest University of Technology and Economics</dt><dd>He has provided significant contributions both the GMT/VIATRA2 project and the existing code base of EMF-IncQuery. He will be the development and technical lead in this new project.</dd>
  <dt>Dr. D&aacute;niel Varr&oacute; (varro@mit.bme.hu), Budapest University of Technology and Economics</dt><dd>He is already a committer on the GMT/VIATRA2 project where he has made significant contributions over many years. He will be the research lead in this new project.</dd>
  <dt>&Aacute;kos Horv&aacute;th (ahorvath@mit.bme.hu), Budapest University of Technology and Economics.</dt><dd>He has provided significant contributions both to VIATRA2 and the existing codebase. He will contribute as an architect and researcher.</dd>
  <dt>G&aacute;bor Bergmann (bergmann@mit.bme.hu), Budapest University of Technology and Economics.</dt><dd>He has provided significant contributions both to VIATRA2 and to the existing codebase, developed the RETE implementation of both VIATRA2 and IncQuery, and is going to work on further core query evaluation engine improvements.</dd>
  <dt>Zolt&aacute;n Ujhelyi (ujhelyiz@mit.bme.hu), Budapest University of Technology and Economics.</dt><dd>He has provided significant contributions to the existing codebase of EMF-IncQuery, and is already a committer in the GEF/Zest project. He will be primarily responsible for developing the query language and the Xtext-based tools.</dd>
  <dt>&Aacute;bel Heged&uuml;s (hegedusa@mit.bme.hu), Budapest University of Technology and Economics.</dt><dd>He has provided significant contributions to the existing codebase of EMF-IncQuery, and he will primarily work on IncQuery add-ons such as the Validation Engine and derived feature integration.</dd>
  <dt>Zolt&aacute;n Balogh (zoltan.balogh@optxware.com), OptXWare Research and Development Ltd.</dt><dd>He has provided significant contributions to the existing codebase, and he will primarily work on tools, core APIs and testing.</dd>
  <dt>Andr&aacute;s &Ouml;kr&ouml;s (andras.okros@optxware.com), OptXWare Research and Development Ltd.</dt><dd>He has provided significant contributions to the existing codebase, and he will primarily work performance-oriented optimization and testing.</dd>
</dl>

<p>We welcome additional committers and contributions.</p>

<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Ed Merks</li>
	<li>C&eacute;dric Brun</li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have expressed interest in this project:</p>

<ul>
  <li>Dr. Andr&aacute;s Pataricza, professor, Budapest University of Technology and Economics, Department of Measurement and Information Systems, Fault Tolerant Systems Research Group (Hungary)</li>
  <li>Dr. Gy&ouml;rgy Csert&aacute;n, CEO. OptXware Research and Development Ltd. (Hungary)</li>
  <li>Jose Ricardo Parizi Negr&atilde;o, Technology Development Engineer, Embraer (Brazil)</li>
  <li>Dr. Zsolt Szepessy and Zolt&aacute;n Theisz, EvoPro Informatics and Automation Ltd. (Hungary)</li>
</ul>

<h2>Project Scheduling</h2>

<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->
<ul>
 <li>June 2012: initial contribution, core components</li>
 <li>August 2012: first build</li>
 <li>June 2013: 1.0</li>
</ul>

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>30-May-2012</td>
		<td>Updated schedule.</td>
	</tr>
	<tr>
		<td>08-February-2012</td>
		<td>Istv&aacute;n R&aacute;th: Document created</td>
	</tr>
</table>
</body>
</html>
