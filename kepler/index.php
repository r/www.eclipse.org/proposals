<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Kepler Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Kepler");
?>

<p>The Kepler Project is a new project proposal under the Technology
    Project at the Eclipse Foundation. For more information, or to express
    interest or feedback on this project proposal, please use the 
    <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.kepler">Kepler Newsgroup</a>.</p>

    <p>This proposal is in the Project Proposal Phase (as defined in the 
    <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development
    Process</a>

    document) and is written to declare its intent and scope. This proposal is
    written to solicit additional participation and input from the Eclipse
    community. You are invited to comment on and/or join the project. Please
    send all feedback to the 
    <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.kepler">Kepler Newsgroup</a>.</p>

<h2>Introduction</h2>
<p>Community-oriented software development is widely seen as one of the most efficient, effective ways to promote both quality and reuse in the development process. Today's open source software communities provide a means to logically break down and organize otherwise monolithic collections of libraries into functionally-focused modules that are self-describing, discrete, and reusable. This community-oriented development model enables a collaborative approach to providing the common subsystems that many applications need. Harnessing the efforts of a diverse development community allows a shared project to accumulate skill, experience, and testing from all of the applications using it. By building an ecosystem of such software communities, each focused on delivering a single, well-articulated feature set, developers are free to seek the best implementation possible for each subsystem, then reuse it. Focusing each project on a single, well-defined problem promotes clearer architecture through a narrowed project scope. Such projects also tend to render a more reliable solution, since they draw on a larger set of use cases to produce a more diverse set of project tests. Additionally, as bugs surface and are fixed within these projects, the improvements benefit the entire community. Finally, the shared infrastructure code provided by these projects leads to greater consistency across grouped or related applications , and makes it easier for developers to integrate these projects with one another. Organizations can reap great rewards in terms of improved application quality and enhanced developer effectiveness by fostering rich, community-oriented development environments.
</p><p>Unfortunately, today's development processes often impose a high price in the form of complex infrastructure and provisioning requirements, making the efficiency benefits of a community-oriented development model nearly impossible to realize. Infrastructure-related complexity is manifested in the many, fragmentary systems that currently inhibit information flow throughout the development life cycle. Further complicating matters is the considerable overhead imposed on engineering through the the micro-management of tasks and information specified by most traditional development methodologies. Every software project has the same essential requirements, such as supporting its users (architects, developers, project managers, etc.), tracking software issues and their respective resolution activities, managing requirements, and generating system designs. Whenever a new project is formed, the complexity or inefficiency involved in the existing development processes is propagated, further increasing the volume of noise that pollutes the engineering environment, inhibiting effective collaboration. In the end, the information overload suffered by community participants makes the cost of implementing a highly modular software ecosystem prohibitive. With today's project-infrastructure requirements, the ideal concept of community-oriented software development simply doesn't scale.
</p>

<h2>Objective</h2>

<p>The purpose of Kepler is to address the complexities involved with provisioning, managing, and using a shared infrastructure in order to support a community-oriented development model. Our strategy will consist of the following:
</p>
<ol><li> Derive an abstract set of tasks required to support software communities, based on tools commonly used today.
</li><li> Develop a framework to orchestrate the execution of related tasks and task groups, with the goal of enabling discovery, access, and control of any third-party, community-infrastructure system from within Kepler.
</li><li> Provide access to each set of framework features through a rich, Eclipse-based environment, thereby enabling users to participate in community activities without leaving their development environment.
</li></ol>

<h2>Principles</h2>
<p>Kepler will provide extensible frameworks and exemplary tools for participation in and management of a large, diverse software community environment. In pursuit of this mission, Kepler will be guided by the following principles:

</p>


<h3>Implementation Neutrality</h3>
<p>We intend to create project-metadata management frameworks that provide equal access to all tools. Our goal is to integrate and simplify the development process in existing environments, and the best way to achieve this is to design a framework which allows existing products to integrate easily.
</p>

<h3>Open, Standards-based Orientation</h3>

<p>This project will strive to use open standards for all information storage and communications APIs, in order to facilitate tighter integration with third-party tools. Where existing standards exist, we will attempt to use them. Where they do not exist, or are deemed unworkable, data formats used will be published and will reflect industry best practices for such designs.
</p>


<h3>Eclipse Community Involvement</h3>
<p>We believe the success of Kepler is contingent upon successful integration with the rest of the Eclipse community. In order to achieve the level of usability required to serve the user community properly, it is critical to engage the broader Eclipse development community and reuse whatever components, technologies, and techniques that make sense.
</p>


<h3>User Community Involvement</h3>

<p>The purpose of Kepler is to make community-oriented software development simpler. Moreover, we believe it is essential that Kepler integrate well with existing Community Infrastructure Tools tool-sets. In order to support this, it is critical to maintain tight bonds with our user community, to be characterized by constant feedback and discussion between project developers and project users.
</p>


<h3>Usability Focus</h3>
<p>Perfect integration of third-party tools is of very limited value if the integrated environment is itself unusable. Therefore, Kepler will maintain a strong focus on the experience of the target audience when designing both user-interface components and the integration framework itself.
</p>

<h3>Transparency</h3>

<p>In the tradition of the Eclipse community, key discussions and documentation will be easily accessible to the foundation membership at large.
</p>


<h3>Incremental Delivery</h3>
<p>Due to the many potential aspects of community-oriented development which this project may improve, we believe that it is critical to provide concrete, usable features as they become available, without waiting for a complete feature set. In this way, incremental roll-out of successively larger feature sets will give the developer and user communities an earlier opportunity to shape Kepler, and make it the best it can be.
<b>MUST</b>, <b>SHOULD</b>, <b>MAY</b> Specification Semantics

</p><p>In order to support the most advanced features of the most advanced tools applicable to each framework, each feature will strive to mark each requirement as one of MUST, SHOULD, or MAY to indicate the features that are required for minimal participation via the framework, and which additional features are supported by the framework for tools that can provide them. These feature annotations are provided in an attempt to avoid the least-common denominator effect, where a framework is limited by the feature-set of the most rudimentary tool to be integrated.
See  <a href="http://www.faqs.org/rfcs/rfc2119.html" class="external text" title="http://www.faqs.org/rfcs/rfc2119.html" rel="nofollow">RFC 2119</a>
</p>


<h2>Scope</h2>
<p>Kepler intends to start with small, high-impact steps to make concrete progress toward its goals as soon as possible. As new opportunities to support software communities are identified, the implementation of the overall Kepler project will gradually expand within each of its main feature categories to provide a richer toolset to its users. However, regardless of the scope of implementation, the project will remain true to one goal: minimizing the complexity involved with managing and participating in an ecosystem of distinct, but interdependent, software communities. The following are an initial list of feature categories to address some areas of community-oriented development in which we believe Kepler can make significant early progress:
</p>


<h3>Community Project Model</h3>

<p>In order to provide frameworks for working with community-oriented project infrastructure, and in order unite the frameworks of Kepler behind the common goal of lowering the barriers to a community-oriented development model, it is essential that Kepler frameworks operate on a common model representing community-oriented projects. To this end, Kepler will introduce a community-project model, to define and maintain the specification for projects which are candidates for community-infrastructure support. While this model may implement a concrete API reading and writing to some "native" format, it will concern itself primarily with providing a means of building a project model in memory, and adapting other systems to provide project information that can be consumed by other participants in the Kepler framework.
</p><p>The community project model will implement the following requirements:
</p>
<ul><li> <b>MUST</b> define appropriate extension points to enable third-party tool configuration to be added to the model.
</li><li> <b>MUST</b> provide mechanisms for extensibility, versioning, and backward compatibility in the project model.
</li><li> <b>MUST</b> provide dependency information
<ul><li> <b>SHOULD</b> support dependency searching

</li><li> <b>MAY</b> support optional (user-configured) substitution of workspace projects for stated binary dependencies
</li></ul>
</li><li> <b>MUST</b> provide project-identification information for integration of the project into the larger software ecosystem
</li><li> <b>MUST</b> track the origins of project-model metadata
</li><li> <b>SHOULD</b> provide mechanisms for modifying project-model metadata files

</li><li> <b>MAY</b> provide a mechanism for writing changed metadata back to the appropriate file using existing metadata-model APIs
</li><li> <b>MAY</b> provide a compound navigator or editor (embedding or linking to existing metadata editors) which allows users single-point access to all project-model data elements
</li><li> <b>SHOULD</b> provide integration metadata for build servers, where applicable
<ul><li> <b>SHOULD</b> include any applicable notification configurations, to enable build servers to provide feedback to the community
</li></ul>

</li><li> <b>SHOULD</b> provide all applicable provisioning information for a project, including:
<ul><li> mailing list addresses (subscribe, unsubscribe, archive; one set per mailing list)
</li><li> SCM location (developer access, anonymous access, and browsing locations, where applicable)
</li><li> issue tracker information (system identifier and location)
</li><li> web-site locations
</li><li> forum location
</li><li> committer information (including authorization or role where applicable)
</li></ul>

</li></ul>
<p><i>Sources of inspiration for the community project model will include Apache Maven's POM (<a href="http://maven.apache.org/ref/current/maven-model/maven.html" class="external free" title="http://maven.apache.org/ref/current/maven-model/maven.html" rel="nofollow">http://maven.apache.org/ref/current/maven-model/maven.html</a>), Buckminster's Component Specification model (CSPEC), DOAP (<a href="http://usefulinc.com/doap" class="external free" title="http://usefulinc.com/doap" rel="nofollow">http://usefulinc.com/doap</a>) and Eclipse's own plugin.xml, feature.xml, and .project/.classpath/etc. project binding files, among others.</i>
</p>


<h3>Kepler Preferences</h3>
<ul><li> <b>SHOULD</b> support configuration of dependency-search sources (globally, or per-project)

<ul><li> <i>(see <a href="#Community_Project_Model" title="">Community Project Model</a> section above for more on dependencies)</i>
</li></ul>
</li><li> <b>MUST</b> support tool-specific preferences, if they are provided by the third-party integration plugin
<ul><li> <i>(tool configuration refers to things settings required to run the build tool, such as ANT_HOME)</i>
</li><li> <b>SHOULD</b> manage tool configuration per-project, or globally (per-workspace?)

</li></ul>
</li></ul>


<h3>Build Management</h3>
<p>One key feature commonly found in successful community-oriented software initiatives is the ability to build the project from source code with a minimum of required specialized knowledge or configuration effort. Aside from supporting users who choose to build from source, this feature is critical for all developers in the community, to ensure that when new code is committed, it does not introduce major problems. The tighter the integration between the project's build system and the developer's environment, the more likely developers are to run project builds, and the less likely that an integration build will fail as a result of code changes. To this end, one of the features of Kepler will be concerned with integrating with build tools, through Eclipse technologies like Buckminster, to extract any interesting community information.
</p><p>The following are some initial build-management feature requirements for the Kepler framework:
</p>
<ul><li> <b>MUST</b> extract information from different build systems
</li><li> <b>MUST</b> integrate with Buckminster for build tool support

</li><li> <b>MUST</b> integrate with Problems and Errors views
</li><li> <b>MUST</b> provide bidirectional synchronization of community information changes to and from the build metadata
</li><li> <b>MUST</b> detect supported build files with community metadata, and add appropriate tool-configuration sections to the project model
</li><li> <b>MUST</b> support editing of community metadata in build metadata using graphical tools
</li></ul>

<h3>Build Server</h3>
<p>Another common feature of successful community-oriented software projects is the implementation of continuous integration (CI) and other server-side build infrastructure. Performing ongoing, consistent build-and-test cycles for a project - rather than just building the software when it's time to release a new version - reduces the risks associated with a project release, and helps to ensure that the community always has up-to-date information about the health and progress of the project. Additionally, since CI builds happen frequently, developers get much quicker feedback when something breaks. With the timely feedback provided by a CI environment, developers can resolve problems as they surface in the build, while they're still relatively easy to diagnose and fix. Finally, server-side build infrastructure enables several advanced development practices, such as proactive detection of errors due to dependency upgrades and binary promotion of project build results. Another feature set of Kepler will be concerned with interacting with server-side build environments.
</p><p>The following are some initial build-server feature requirements for the Kepler framework:
</p>
<ul><li> <b>MUST</b> Store information of the build servers type
</li><li> <b>MUST</b> Store information of the location of the build servers

</li><li> <b>SHOULD</b> Add a graphical view of the build servers
</li></ul>
<h3>Project Community Provisioning and Maintenance</h3>
<p>The ability to provision and manage infrastructure-related configuration for multiple projects in a consistent manner is one of the most intensive overhead activities for a community-oriented software ecosystem. When the ecosystem allows the promotion of community members from user to developer, such as in the open-source realm, maintaining these community-support systems becomes all the more difficult. Therefore, to lessen the burden of adopting a community-oriented development model on large scales, Kepler will introduce several project provisioning features.
</p><p>The following are some initial project-provisioning feature requirements for the Kepler framework:
</p>
<h4>General Features</h4>

<ul><li> <b>MUST</b> support existing community-infrastructure tools
</li><li> <b>MUST</b> provision new services for existing projects as needed
</li></ul>

<h4>Project Provisioning</h4>
<p>Initializes the following, using existing infrastructure (shared by multiple projects):

</p>
<ul><li> <b>MUST:</b> continuous integration (single-instance, clustered, and farmed scenarios)
</li><li> <b>MUST:</b> mailing lists and associated rules
</li><li> <b>MUST:</b> forum
</li><li> <b>MUST:</b> integrate with Mylyn for issue tracking

</li><li> <b>SHOULD:</b> wiki
</li><li> <b>MUST:</b> integrate with Eclipse Team Support for version control (CVS, Subversion, etc.)
</li><li> <b>SHOULD:</b> static web sites
</li><li> <b>MUST:</b> Eclipse IDE configuration for the project
</li></ul>



<h4>Project Maintenance</h4>
<ul><li> <b>MUST</b> support addition and removal of developers (committers)

</li><li> <b>SHOULD</b> support modification of developer authorization/roles
</li></ul>
<p><i>NOTE: Our initial goal for this feature of Kepler will be to support the community infrastructure of the Eclipse Foundation itself. In order to achieve this, Kepler development team will need to work closely with the Eclipse IT team. Since the IT team is responsible for maintaining and provisioning infrastructure for the Eclipse Foundation communities, they represent a rich source of knowledge about the workflows and current tools used to manage these systems. Additionally, any implementation of the project provisioning feature which aims to serve the Eclipse Foundation will have to have access and buy-in from the IT team.</i>
</p>



<h3>Multi-Community Visibility</h3>
<p>One of the great costs associated with subdividing a monolithic code base into modules is the complexity involved with participating in the resulting menagerie of projects. Instead of interacting with a single system for each life-cycle task, developers must manage information in dozens. For example, where a monolithic project might house all of its open issues in a single, application-wide tracking interface, issues logged against an ecosystem of community-oriented projects are housed in multiple separate issue-tracking repositories; one per project. Needless to say, modularization under these circumstances severely complicates the development and project management tasks of any application based on such an ecosystem. Similarly, community participation may take place largely on project-specific mailing lists, making it more difficult to maintain a single conversation thread that addresses multiple projects. Even the process of searching for documentation can become quite complex, as each project may host separate forums, FAQs, and project-documentation web sites.
</p><p>In order to overcome the scalability problem and realize the benefits of a community-oriented development process, Kepler will reduce the burden incurred through a multi-project infrastructure by introducing a multi-community visibility feature. This project will deliver a framework and exemplary tooling that provide integrated, read-write views of information flow among and through multiple software communities.

</p><p>The following are some initial multi-community feature requirements for the Kepler framework:
</p>
<ul><li> <b>MUST</b> provide issue-tracker reporting (integration with Mylyn)
</li><li> <b>MUST</b> aggregate and organize mailing list traffic (including easy cross-posting)
</li><li> <b>MUST</b> support forum searches (including aggregation of results)
</li><li> <b>SHOULD</b> support web-site documentation browsing (and search?)

</li><li> <b>SHOULD</b> support aggregation of structured documents, like FAQs
</li><li> <b>MUST</b> support aggregation of CI results
</li><li> <b>MUST</b> support aggregated management of and reporting on project binaries
</li></ul>


<h2>Interaction with existing projects</h2>

<ul>
<li>Buckminster: interaction with build systems (Maven, Ant, PDE,...)</li>
<ul>
  <li>Shared libraries for interaction with Maven used by Kepler and Buckminster are going through the IP verification process</li> 
</ul>
<li>Mylyn: interaction with issue tracking systems</li>
<li>Eclipse Team Support: interaction with source control management systems</li> 
</ul>

<h2>Organization</h2>

<p>This project proposes to work closely with individuals and organizations, both inside and outside the Eclipse Foundation, in order to arrive at an architecture that is relevant to as wide a community as is practically possible. To begin, the Buckminster project at Eclipse has expressed interest in exploring opportunities to integrate with and help shape the features detailed above.
Proposed Project Lead and Initial Committers
</p>
<ul><li> Carlos Sanchez, DevZuz - Project Lead
</li><li> Philip Dodds, DevZuz
</li><li> Brett Porter, DevZuz
</li><li> Mike Lim, Exist
</li><li> Glen Gonzales, Exist
</li><li> Maria Odea Ching, Exist
</li><li> Jonas Lim, Exist
</li><li> Cata Pineda, Exist

</li><li> Erle Mantos, Exist
</li><li> Genesis Deiparine, Exist
</li><li> Thomas Hallgren, Cloudsmith
</li><li> Henrik Lindberg, Cloudsmith
</li></ul>

<h3>Project Mentors</h3>
<ul>
<li>Thomas Hallgren, Cloudsmith</li>
<li>Oisin Hurley, IONA</li>
</ul>

<h2>Interested Parties</h2>
<p>The following organizations and individuals have expressed interest in this project:

</p>
<ul><li> Europa Build Workshop Members ( <a href="http://wiki.eclipse.org/index.php/Europa_Build_Workshop_Report" class="external free" title="http://wiki.eclipse.org/index.php/Europa_Build_Workshop_Report" rel="nofollow">http://wiki.eclipse.org/index.php/Europa_Build_Workshop_Report</a> )
</li><li> Apache Maven ( <a href="http://maven.apache.org" class="external free" title="http://maven.apache.org" rel="nofollow">http://maven.apache.org</a> )
</li><li> Eclipse Buckminster ( <a href="http://www.eclipse.org/buckminster" class="external free" title="http://www.eclipse.org/buckminster" rel="nofollow">http://www.eclipse.org/buckminster</a> )
</li><li> DevZuz ( <a href="http://www.devzuz.com" class="external free" title="http://www.devzuz.com" rel="nofollow">http://www.devzuz.com</a> )

</li><li> Intervoice ( <a href="http://www.intervoice.com" class="external free" title="http://www.intervoice.com" rel="nofollow">http://www.intervoice.com</a> )
</li><li> Exist ( <a href="http://www.exist.com" class="external free" title="http://www.exist.com" rel="nofollow">http://www.exist.com</a> )
</li><li> Simone Bordet simone _dot_ bordet *at* gmail _dot_ com
</li></ul>

<h2>Initial contributions</h2>
<p>
DevZuz is contributing the initial implementation mainly consisting on the community model and EMF representations and Maven metadata adaptor.
</p>


      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
