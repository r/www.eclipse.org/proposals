<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">


<h1>Dash, Tools for Committer Community</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Dash, Tools for Committers");
?>

<h2>Introduction</h2>
<p>The Tools for Committer Community (Dash) is a proposed open source
project under the <a href="/technology/">Technology Project</a>.</p>
<p>This proposal is in the Project Proposal Phase (as defined in the
<a href="/projects/dev_process/">
Eclipse Development Process document</a>) and is written to declare its intent
and scope. This proposal is written to solicit additional participation and
input from the Eclipse community. You are invited to comment on and/or join the
project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology</a> newsgroup.&nbsp;

<h2>Description</h2>
<p align="left">The <a href="/projects/dashboard/">Eclipse
project dashboard</a> is a good first prototype of tools that will help the
committer community. Project Dash is a place where the community itself can
collaborate on tools for community awareness and collaboration in support of our
ultimate objective of committer quality and cooperation.</p>
<p align="left">This is very much a research project proposal as we are not
entirely sure what these tools will be. Obviously extensions and enhancements of
the dashboard are a possibility. Or perhaps there are add-ons to Bugzilla
that could help with searching for duplicates? Perhaps there are mash-ups of our
wiki and the ECF project that could help with committer awareness and
collaboration? We will prototype these tools in this project and if they stick,
we'll roll them into production on the eclipse.org or dev.eclipse.org website.</p>

<p align="left">Our first tool is going to be Eclipse Monkey - a dynamic
scripting tool for the automation of routine programming tasks. Monkey scripts
are little Javascript programs using either the Eclipse APIs or custom Monkey
DOMs. The prototype help pages (<a href="html/maintopic.html">page 1</a> and <a href="html/domtopic.html">page
2</a>) explaining this further.</p>

<h2 align="left">Tentative Roadmap</h2>

<ul>
  <li>
    <p align="left">M0 (Dec 18, 2005) Project approval and initial code drop of
    Eclipse Monkey</li>
  <li>
    <p align="left">M1 (Feb 6, 2006) Second DOM; dependency meta-data; update
    site compatible DOMs</li>
  <li>
    <p align="left">M2 (Mar 20, 2006) script gallery website</li>
  <li>
    <p align="left">EclipseCon (Mar 22) DOMFest at EclipseCon (BOF for building
    scripts and DOMs)</li>
  <li>
    <p align="left">M3 (May 1, 2006) resource change triggered scripts;
    meta-data parsing extension</li>
  <li>
    <p align="left">M4 (Jun 12, 2006) script self testing; scripting 3.2 Command
    sequences</li>
  <li>
    <p align="left">R1.0 (Jun 30, 2006) release coincident with Callisto</li>
</ul>

<h2>Organization</h2>

			<p align="left">We propose this project should be undertaken as a sub-project within the Eclipse
            Technology Project.&nbsp;</p>


		<h2 align="left">Proposed project lead and initial committers</h3>
</h2>
		<ul>

			<li>
            Bjorn Freeman-Benson (Eclipse Foundation)</li>

			<li>
            Ward Cunningham (Eclipse Foundation)</li>

			<li>
            Ed Warnicke (Cisco)</li>
		</ul>

			<h2 align="left">Code Contributions</h3>
</h2>
<p align="left">Initial code contributions (the Eclipse Monkey scripting system)
from Ward Cunningham and Bjorn Freeman-Benson.

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
