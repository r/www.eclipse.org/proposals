<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "MST Project Proposal";
$pageKeywords	= "MST";
$pageAuthor		= "Kenn Hussey";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>MDT MST</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("MST");
?>

<h2>Introduction</h2>

<p>Metamodel Specification Tools (MST) is a proposed open source subproject of the <a href="http://www.eclipse.org/modeling/mdt/">Model Development Tools (MDT)</a> project to provide tooling for the development of MOF compliant metamodels, and specifications based on them.</p>

<p>This project is in the Pre-Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse Development Process</a>) and this document is written to declare its intent and scope. This 
proposal is written to solicit additional participation and input from the Eclipse community. You are invited to 
comment on and/or join in the development of the project. Please send all feedback to 
the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.mst">eclipse.mst</a> newsgroup.</p>

<h2>Background</h2>

<p>The importance of supporting industry standards is critical to the success of the <a href="http://www.eclipse.org/modeling/">Modeling</a> project, and to Eclipse in general. The role of the Modeling project in the support of industry standards is to enable their creation and maintenance within the Eclipse community. Furthermore, as standards bodies such as the OMG have a strong modeling focus, the Modeling project needs to facilitate communication and outreach through its PMC and project contributors to foster a good working relationship with external organizations.</p>
<p>Years of experience with creating and using metamodels based on MOF and similar technologies (e.g. EMF) have shown that the object-oriented structure of MOF is quite effective at providing an abstract syntax for a model representing a single view and notation but has proven limited in a broader context involving multiple viewpoints, related models, or different viewpoints of the same thing, e.g., a set of process, information, service, and rules models about the same enterprise. Multiple "stovepipe" metamodels and profiles have thus been created and are causing problems for users who use multiple tools. In addition, many MOF-based metamodels lack a "semantic grounding" because MOF does not currently facilitate semantic precision. The OMG has issued a Request for Proposal (RFP) to make incremental changes to several existing specifications in order to make MOF better capable of supporting richer and evolving models; the proposed MST project would provide an implementation of the specification that emerges in response to this RFP.</p>
<p>One of the challenges of developing an OMG specification is the lack of tooling support for CMOF (Complete Meta Object Facility) models. This was one of the many topics that were discussed during the �Mega Modeling Mania� BoF at EclipseCon 2008 and again at the Eclipse/OMG Symposia in March and June. Based on those discussions, there appears to be demand for tooling that will make it easy to create, serialize (in CMOF-compliant XMI), and document metamodels that form the basis for open specifications. This a proposal for a new sub-project of the MDT project, dubbed �Metamodel Specification Tools�, or MST, to provide this kind of tooling at Eclipse.</p>

<h2>Scope</h2>

<p>The MST project would customize and/or extend the existing (or forthcoming) UML editors (primarily for class and package/profile diagrams) to expose CMOF concepts which are missing in UML (like identifiers, namespace URIs, and XMI tags), leverage the CMOF (de)serialization support that was introduced in the UML2 project as part of the Ganymede release, and provide a mechanism for generating a specification (or at least a decent boiler plate for one) directly from the metamodel using BIRT. 
The project would also aim to automate the mapping between a metamodel and its profile representation 
(if there is one) and to make use of the Eclipse Process Framework (EPF) to document and coordinate 
the specification development process. Finally, it will provide a proof of concept for changes and/or extensions to EMF and/or UML2 to better support richer and evolving metamodels, as 
per the <a href="http://www.omg.org/cgi-bin/doc?ad/2006-06-08">MOF Support for Semantic Structures RFP</a>.</p>

<h2>Relationship with Other Eclipse Projects</h2>

<p>The Papyrus (recently created), UML2 Tools, and UML2 subprojects of the <a href="http://www.eclipse.org/mdt/">Model Developement Tools (MDT)</a> project already (or will soon) provide much of the functionality that will be used to create and serialize MOF-compliant metamodels. It is our intent to work closely with these project teams to ensure that the challenges discovered while developing this new project can be addressed.</p>

<p>Obvious integrations with other Modeling subprojects will be explored as the project evolves.</p>

<h2>Organization</h2>

<h3>Mentors</h3>

<ul>

<li>Ed Merks</li>

<li>Richard Gronback</li>

</ul>

<h3>Initial Committers</h3>

The initial committers for this project would be:

<ul>

<li>Kenn Hussey (<a href="http://www.embarcadero.com/">Embarcadero Technologies</a>), proposed project lead</li>

<li>Nicolas Rouquette (<a href="http://www.jpl.nasa.gov/">JPL</a>)</li>

<li>Nick Dowler (<a href="http://www.adaptive.com/">Adaptive</a>)</li>

<li>Chris Armstrong (<a href="http://www.aprocessgroup.com/">Armstrong Process Group, Inc.</a>)</li>

<li>Tom Digre (<a href="http://portal.modeldriven.org/">ModelDriven.org</a>)</li>

<li>TBD</li>

</ul>

<h3>Code Contributions</h3>

<p>This project will not include an initial code contribution. Extensions to 
existing framworks and tooling consisting of Java packages within the 
org.eclipse.mst.* namespace will be developed as required to provide the 
capbilities mentioned above.</p>

<h3>Interested Parties</h3>

<p>Thus far, interest in this component has been expressed 
by <a href="http://www.embarcadero.com/">Embarcadero Technologies</a>, 
<a href="http://www.ibm.com/">IBM</a>, <a href="http://www.jpl.nasa.gov/">JPL</a>, 
<a href="http://www.adaptive.com/">Adaptive</a>, 
<a href="http://www.nomagic.com/">No Magic, Inc.</a>, 
<a href="http://www.visumpoint.com/">visumpoint</a>, 
<a href="http://www.aprocessgroup.com/">Armstrong Process Group, Inc.</a>, 
<a href="http://www.unisys.com/">Unisys</a>, 
<a href="http://www.zeligsoft.com/">Zeligsoft, Inc.</a>, and <a href="http://www.modeldriven.org/">ModelDriven.org</a>.</p>

<h3>Developer Community</h3>

<p>The team of initial committers will explore statements of interest from additional developers experienced with metamodel and/or specification development or willing to gain such experience.</p>

<h3>User Community</h3>

<p>It is expected that the user community for this component will consist primarily of specification authors, given that it is essentially a tool aimed at building and documentation metamodels.</p>

<h2>Tentative Plan</h2>

<p>The first release of this project, focused on metamodel creation and serialization, would be tentatively scheduled for June 2009, as part of the Galileo simultaneous release.</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
