<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("JavaServer Faces");
?>

<h1>JavaServer Faces Tooling Project</h1><br>

 </p>


<h2>Introduction</h2>





<p>The JavaServer Faces Tooling Project is a proposed open source project to be incubated
under the <a href="/webtools">Eclipse Web Tools Project</a>. </p>


<p>This project proposal is in the Proposal Phase (as defined in the <a href="/org/documents/Eclipse%2520Development%2520Process%25202003_11_09%2520FINAL.pdf">Eclipse
Development Process</a> document) and is posted here to solicit community feedback,
additional project participation, and ways the project can be leveraged from the Eclipse
membership-at-large. You are invited to comment on and/or join the project. Please send
all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools.jsf">eclipse.webtools.jsf</a>
newsgroup.</p>



<h2>Background</h2>


<p>JavaServer Faces (JSF) is a server-side user interface component framework for
Java-based web applications. JSF has become a popular technology that is increasingly used
in building Web applications. JSF artifacts include JavaServer Pages (JSP) that make use
of the JSF tag libraries and the JSF configuration file, &ldquo;faces-config.xml&rdquo;. The
configuration file configures both page-flow (control) aspects and managed bean (model)
aspects of the JSF application, amongst other things. A JSF application will also include
Java classes. </p>


<p>The objective of the JSF Tooling project is to build extensible
frameworks and exemplary tools that support&nbsp;the full life cycle of
building
a JSF-based web application. </p>


<h2>Description</h2>


<h3>Project Goal </h3>


<p>The goal of this project is to add comprehensive support to the Eclipse Web Tools
Project to simplify development and deployment of <a href="http://java.sun.com/j2ee/javaserverfaces/index.jsp">JavaServer Faces</a> (JSF)
applications.&nbsp;The project will provide an extensible tooling infrastructure and
exemplary tools for building JSF-based, web-enabled applications. The tooling
infrastructure will be designed for extensibility so software vendors can provide
additional functionality.&nbsp; </p>


<p>This project will be layered over the services provided by the <a href="/webtools/jst/main.html">JST</a> and <a href="/webtools/wst/main.html">WST</a> subprojects. JSF artifacts
will be first class citizens with respect to the capabilities that Eclipse users expect.
This includes support to create, edit, and refactor JSF pages and JSF configuration files.
A model will be created (or an existing model may be enhanced) for such resources and
these resources will participate in the build process. </p>


<h3>Project Scope </h3>


<p>The project will provide&nbsp;extensible frameworks and exemplary tools that support&nbsp;the full life cycle of building
a JSF-based web application. This includes tooling that will aid in developing, deploying, testing and
debugging a JSF-based web application. Some of the functional requirements of this
project will be met by extending/inheriting the services provided by the JST and WST
subprojects. These include support for deploying, testing and debugging. </p>


<p>The project will build editors and validators for all the artifacts of a JSF project.
Initially, these will be source/textual editors that are JSF aware. However, within scope
is to build an editor for the JSF configuration file, &ldquo;faces-config.xml&rdquo; that
would allow a user to create a Page flow at a higher level of abstraction than the plain
XML syntax. Similarly, the project scope includes building a visual editor for the JSP
constructs. The visual editors will be implemented in later phases of the project.</p>


<p>The project will be architected as a set of plug-ins, each of which provides the
ability for other groups or vendors to further extend and customize the functionality in a
well-defined manner. </p>


<p><strong>Tools for J2EE Web Module </strong>

</p>
<ol>

  <li>Add JSF Project Feature to any Web module. This will do the following: 
    <ol type="a">

      <li>Configure the project 
        <ol type="i">

          <li>Add required JSF tag libraries and jars </li>

          <li>Create a default faces-config.xml </li>

          <li>Update web.xml with Faces Servlet class </li>

        
        </ol>

      </li>

      <li>Participate in the Build process 
        <ol type="i">

          <li>Add validation semantics for the content in faces-config.xml, e.g., Navigation rules </li>

        
        </ol>

      </li>

    
    </ol>

  </li>

  <li>Wizard for creating a new JSF Project 
    <ol type="a">

      <li>This will provide support for multiple versions of JSF including MyFaces </li>

      <li>This will also provide some support for migration to a higher-version </li>

    
    </ol>

  </li>

  <li>Wizard for creating new JSF Page 
    <ol type="a">

      <li>Create a JSF page from a template </li>

      <li>Create and manage templates </li>

    
    </ol>

  </li>

  <li>Wizard for creating managed beans, other constructs. </li>

</ol>


<p>The wizards will all be extensible such that vendors can augment the UI and provide value-added services. </p>


<p><strong>Editors for JSF </strong></p>


<p>There would be two forms of editor: one to edit JSP pages containing JSF tags, and one
to edit faces-config.xml. These editors will be multi-page editors that support both
textual/source editing and a graphical/visual editing. </p>


<p>The visual editor for the constructs in the faces-config.xml will
have the capability
to create the Page flow and also manage other constructs such as
managed beans. The faces-config.xml will have an associated model and
corresponding API's to manipulate it programmatically. The
editors will have an associated Outline view that will display the
content in a structured
format.
</p>
<ol>

  <li>Source Editor for JSF 
    <ol type="a">

      <li>Inherit JSP Editor from JST. Some of the inherited features include: 
        <ol type="i">

          <li>Standard editor capabilities (coloring, code assist, refactoring, formatting) </li>

          <li>JSP Expression Language Support (Content Assist, Syntax Coloring) </li>

          <li>Content based selection </li>

          <li>Hybrid editing </li>

        
        </ol>

      </li>

      <li>Add context sensitivity for JSF artifacts such as managed beans </li>

      <li>Participation in the build process </li>

    
    </ol>

  </li>

  <li>Source Editor for faces-config.xml 
    <ol type="a">

      <li>Inherit XML Editor from WST </li>

      <li>Add context sensitivity for JSF tags </li>

      <li>Navigate, pull-down support for associated Java class </li>

    
    </ol>

  </li>

  <li>Graphical/Visual Editor for JSF Pages 
    <ol type="a">

      <li>Visual editing of JSF components </li>

      <li>Support drag and drop, wizards for standard and core JSF tags </li>

      <li>Support for backing beans </li>

    
    </ol>

  </li>

  <li>Graphical/Visual Editor for faces-config.xml (Page-flow Editor) 
    <ol type="a">

      <li>Create and remove pages (Navigation Cases) from the diagram </li>

      <li>Rename navigation rule outcomes and actions and navigation cases on the diagram </li>

      <li>Automatically diagram navigation data from any JSF configuration file </li>

    
    </ol>

  </li>

  <li>JSF Preview 
    <ol type="a">

      <li>The JSF Editors will have a preview capability </li>

    
    </ol>

  </li>

  <li>JSF Debugger 
    <ol type="a">

      <li>JSR-45 Debugging </li>

    
    </ol>

  </li>

</ol>


<p><strong>Automation/Generation Functionality </strong></p>


<p>Wizards will be provided to assist in development

</p>
<ol>

  <li>Wizard for creating a new JSF Project 
    <ol type="a">

      <li>This will provide support for multiple versions of JSF including MyFaces </li>

      <li>This will also provide some support for migration to a higher-version </li>

    
    </ol>

  </li>

  <li>Wizard for creating new JSF Page 
    <ol type="a">

      <li>Create a JSF page from a template </li>

      <li>Create and manage templates </li>

    
    </ol>

  </li>

  <li>Wizard for creating managed beans, other constructs </li>

</ol>


<p><strong>Support for 3<sup>rd</sup> party JSF Components </strong></p>


<p>The JSF tools in Eclipse will be designed as a vendor-neutral solution with a set of
JSF components provided as a sample. No vendor-specific runtime implementations will be
built into Eclipse as part of this project. Any 3<sup>rd</sup> party JSF-compliant
component, such as Sun's JSF Reference Implementation components, Apache's MyFaces
components, and Oracle's ADF Faces components, will enjoy easy drop-in support right out
of the box. </p>


<p><strong>Extension Points </strong></p>


<p>The project will define appropriate extension points that would enable other software
vendors to provide value-added functionality. These include ability for a 3<sup>rd</sup>
party vendor to add/enhance the visual Page flow editor, add/enhance the validators, add
custom property editors for JSF components, etc. </p>


<p><strong>Integration Points with Other Eclipse Projects </strong></p>


<p>Given the scope of the project, the JSF tools will interact with and leverage the
following existing Eclipse projects. This is not an exhaustive list and will be enhanced
during the course of this project.

</p>
<ul>

  <li><a href="/jdt/index.html">JDT (Java Development Tools) </a></li>

  
  <ul>

    <li>Core, UI, and LTK (Language ToolKit) </li>

  
  </ul>

  <li><a href="/webtools/wst/main.html">WST (Web Standard Tools) </a></li>

  
  <ul>

    <li>Structured Source Editor (SSE) </li>

  
  </ul>

  <li><a href="/webtools/jst/main.html">JST (J2EE Standard Tools) </a></li>

  <li><a href="/gef/main.html">Graphical Editing Framework (GEF) </a></li>

</ul>


<p>This project will also integrate with a potential visual HTML editor if and when such a
contribution is made. </p>


<p>This project will also integrate with a potential data-binding project if and when such
a project comes into existence. </p>


<h2>Organization</h2>


<p align="left">We propose this project should be undertaken as an incubator under the <a href="/webtools">Eclipse Web Tools Project</a>. </p>


<h2 align="left">Proposed project lead and initial committers</h3>


<p>Oracle has committed to providing project leadership and developer resources for this
project. During the proposal phase the project leadership will be pro-actively contacting
interested parties to invite their contribution of code and development resources.

</p>
<ul>

  <li>Project Lead: Raghunathan Srinivasan, Oracle</li>

</ul>


<h2 align="left">Interested parties</h3>


<p>Initially, JSF Expert Group members and other individuals and organizations that have
serious interest will be solicited to participate.&nbsp;This list would cover a broad base
of organizations interested in JSF, including J2EE vendors. </p>


<p>The submitters of this proposal welcome interested parties to post to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools.jsf">eclipse.webtools.jsf</a> newsgroup and
ask to be added to the list as interested parties or to suggest changes to this document. </p>

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
