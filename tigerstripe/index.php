<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Tigerstripe Sub-Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Tigerstripe");
?>

<p> 
	
	<h2>Introduction</h2> 
		<p><strong>Note:</strong>The name of the project may change at or prior to the Creation Review due to trademark issues.
		</p>
		<p>
			The Tigerstripe project is a proposed open source project under the <a href="http://www.eclipse.org/technology">Eclipse Technology Project</a>.
		</p>
		<p>
			This proposal is in the project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process</a> document)
			 and is written to declare its intent and scope. This proposal is written to solicit additional participation 
			 and input from the Eclipse community. You are invited to comment and/or join the project. 
Please send all feedback to the 
			 <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.tigerstripe">eclipse.technology.tigerstripe</a> newsgroup. 
			 The project will follow the structure and guidelines as outlined by the Technology PMC.
		</p> 
	
	<h2>Background</h2> 
		<p>Model-Driven Development (MDD) is an approach in software and systems development to create application code, application 
	programming interfaces (API) and specification based on Models. These models 
	are processed through a series of transformations to generate the final 
	application, code or systems to be deployed.
		</p> 
		<p>Using MDD has multiple benefits over traditional software and systems development, in that it leads 
	to better code quality, better maintainability and has the potential to improve 
	the overall efficiency of a development organization significantly.
		</p> 
		<p>However "mastering" of MDD can only be achieved with the appropriate tooling support, from modeling support,
		to code generation, but also for automated testing, continuous integration, etc. While the industry at large 
		provides all of these parts, having a single environment to cover the process from A to Z is key to harvesting
		the benefits of MDD. 
		Moreover, generic-purpose UML modeling tools offer too much flexibility to non-experienced UML modelers. When working
		with large distributed teams, it becomes clear that the modeling tool needs to guide and constrain the users
		according to the target domain. This typically goes beyond UML profiles and require tight naming conventions
		and validation rules accross a model to ensure the quality of the generated code or specification, in particular
		in the Telecommunications Industry.
		</p>
		<p>Tigerstripe was born two years ago to address this need of a single, integrated environment dedicated to MDD
			in the Telecommunications Industry: in particular, Tigerstripe was first adopted by Telecommunications standard 
			bodies for Operational and Business Support Systems management interfaces. The product is built using the standard
			Eclipse Modeling Components (namely EMF/GMF) and provides an extensible	MDD framework within Eclipse.</br>
			
		In the last two years, the Tigerstripe user-community has been well established within the Telecom Industry, 
		where in particular, Standard Bodies such as the TeleManagement Forum has been delivering standard specification and
		APIs for Operational and Business Support Systems integration (OSS/BSS) with Tigerstripe.
		</p>

	 <h2>Scope</h2> 
	 	<p>The objectives of the Tigerstripe sub-project are:
	 		<ul>
				<li>contribute an extensible toolset for Model-Driven Development targeting the Telecommunications Vertical,
					with extension points supporting the generation of application code, APIs, specifications or documentation.
					The initial version was built using a combination of existing Eclipse components and proprietary technology
					to address the specific needs of this industry.</li>
				<li>integrate additional components of Eclipse projects into the existing environment to enhance the existing
					MDD environment or replace proprietary technologies as appropriate</li>
				<li>provide an extensible framework, and solicit contributions so that industry specific models can be developed,
					maintained and exchanged. Tigerstripe's initial user community is expected to be in the Telecoms industry</li>
				<li>provide additional tools, samples and ideas that allows integration of Tigerstripe with other existing 
					environments.</li>
		</ul>
		
			We intend to migrate existing parts of the software to the latest frameworks 
			supported by the Modeling top-level project. In particular, we intend to use 
			more of the EMF and UML2 frameworks where applicable to complement the 
			current technology in use, and allow easier model sharing between users. As 
			we proceed we will feedback requirements as experienced in our Telecom 
			industry vertical.

	 <h2>Description</h2> 
		<p>Tigerstripe in its current state is commercial grade Eclipse feature that provides:
			<ul>
				<li>A subset of UML2 features with Class Diagrams and Instance Diagrams</li>
				<li>Model scoping capabilities aimed at support for large models and multiple generation paths</li>
				<li>Strong, flexible generation engine allowing users to develop "Tigerstripe Plugins" to plug-in the
				necessary code and templates needed for code/specs/documentation generation.</li>
				<li>Headless model-driven generation, to enable integration with continuous build environments.</li>
			</ul>
			Tigerstripe is currently used in production on large models at Cisco Systems, and actively used by multiple
			member companies of the TeleManagement Forum.
		</p>
	 	<p>
	 		It shall be noted that while the existing initial user-community (see below) is mainly coming from the Telecom Industry, the platform
	 	is true agnostic as what is modeled and what is generated from it. </br>
	 	We believe Tigerstripe has the potential to foster an eco-system for MDD across industries.
	 	</p>
	 	
	 <h2>Organization</h2>
	 
	 <h3>Mentors</h3>
	 <ul>
	 	<li>Doug Schaefer, QNX</li>
	 	<li>Harm Sluiman, IBM</li>
	 </ul>
	  
	 <h3>Initial Committers</h3>
		<p>
		Initial committers will be issued of the existing developers' pool. They will initially focus on the transition from
		a commercial product to an open-source, community-based platform. They will work with the existing user-community
	to encourage contributions to the platform.</br>
			The initial committers are from Cisco Systems:
			<ul>
				<li>Eric Dillon (project lead)</li>
				<li>Duncan Keysell</li>
				<li>Richard Craddock</li>
				<li>Jim Strawn</li>
				<li>Steve Jerman</li>
			</ul>
		</p>
		
		<h3>Interested Parties</h3>
		<p>
			The following parties have expressed interest in this project: 
			<ul>
				<li>Harris Stratex - Zhdankin, Alex [azhdanki@harris.com]</li>
				<li>Telcordia - Francesco Caruso [caruso@telcordia.com]</li>
				<li>Progress Software - John Petrie [petrie@progress.com]</li>
				<li>Vodafone D2 - Joerg Frankenberger [joerg.frankenberger@vodafone.com]</li>
				<li>Nokia Siemens Networks - Stefan Vaillant [stefan.vaillant@nsn.com]</li>
				<li>Amdocs - Richard Mishra [richard.mishra@amdocs.com]</li>
				<li>Frox - Roman Schlegel  [roman.schlegel@frox.com]</li>
			</ul>
		</p>
		<h3>Initial User Community</h3>
		<p>
			The initial user community will consist of Cisco Systems and multiple technical teams of the TeleManagement Forum 
			that have been actively using Tigerstripe for standards making process over the last two years. 
		</p>
		<p>
			A series of Tutorials, samples together with the corresponding documentation is already available about Tigerstripe.
			This will be made available through a Tigerstripe wiki for example in an effort to further bootstrap the growth
			of the existing user-community.
		</p>
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
