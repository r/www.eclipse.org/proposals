<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>EGL Development Tools Project</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
</style>

<body>
<p>The EGL Development Tools project is a proposed open source project under the <a
	href="http://www.eclipse.org/projects/project_summary.php?projectid=tools">Tools Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is a forum
	that is available from http://www.eclipse.org/forums with a URL using the
	project's short name; it will be something along the lines of
	of http://www.eclipse.org/forums/eclipse.woolsey. EMO can (and will)
	provide assistance if you're not sure what to put here.
 -->

<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the <a href="http://www.eclipse.org/forums/eclipse.egl">EGL Development Tools</a>
Forum.</p>

<h2>Background</h2>

<b>EGL Language Concepts</b>
<p>
<a href="http://en.wikipedia.org/wiki/EGL_%28programming_language%29" target="_blank">EGL</a> is a programming language conceptually similar to many common languages that have come before it. The language borrows concepts familiar to anyone using <a href="http://en.wikipedia.org/wiki/Type_system#Static_typing" target="_blank">statically typed languages</a> like Java, COBOL, C, etc.  However, it borrows a concept from UML (Universal Modeling Language) that is not typically found in statically typed programming language � the concept of Stereotype. In UML, stereotypes are used to tag UML elements with metadata (in this case, metadata refers to information about the UML element, for example, information about a UML element called a "class"). Constraints can be defined by stereotype definitions such that elements stereotyped by the given stereotype must adhere to the defined constraints of that stereotype. Stereotypes in UML are used as a lightweight mechanism to extend the standard modeling concepts. Stereotypes in EGL are essentially the same idea and are used to extend the basic EGL core language concepts. 
<p>
With most programming languages, source code is either compiled into a standard set of byte codes readable by a VM (Virtual Machine) or the code is compiled into the actual machine instructions for a specific platform. The compilation process usually has at least two major phases:
<ol>
<li>Conversion of the source code into some intermediate model representation
<li>Generation from that intermediate model to the appropriate byte code (object code) or platform-specific instructions
</ol>
Typically the set of constructs and concepts a compiler must deal with is a closed set so both steps can be handled by a single processor called the compiler. However, in EGL the set of concepts to be compiled and transformed is extensible. Therefore, in EGL these two steps have been explicitly separated into a compile step (which converts EGL source code to a <a href="http://en.wikipedia.org/wiki/Serialization" target="_blank">serializable</a> intermediate model) and a generation step (which takes the intermediate model and generates some other type of source code).
<p>
The EGL compiler is responsible for validating and binding the basic core language as well as enforcing the constraints defined by various stereotypes. If validation is successful, the intermediate representation of the source code will be serialized. The EGL generator reads this model and transforms it into runtime source code.  The EGL compiler is closed in the same sense as any compiler as it is dealing with a predefined set of concepts.  However, the EGL generator is left with the task of transforming the concepts embodied in various stereotypes to actual runtime source code.  This is why the generator is separated from the compiler; so that extensible generator frameworks can be built without having to be linked with any particular compiler implementation.  New stereotypes can be added and used by EGL programs. The compiler will validate the usage of those stereotypes without any extension to the compiler.  However, the generator must be extended to understand these new stereotypes and do the appropriate transformations.  
<p>
<b>EGL Implementation and Tools</b>
<p>
The EGL technology, in its current form, exists today within the Eclipse-based <a href="http://www.ibm.com/software/awdtools/developer/business/" target="_blank">IBM Rational Business Developer</a> (RBD) development tool and provides a programming environment capable of deploying multi-tier application components across different platforms and middleware. EGL code is not executed directly as byte or machine code, but is instead compiled and generated into either Java (e.g. Java-based Web services that access information in databases), JavaScript (e.g. <a href="http://en.wikipedia.org/wiki/Rich_Internet_Applications" target="_blank">RIAs</a> that utilize <a href="http://www-949.ibm.com/software/rational/cafe/docs/DOC-2999" target="_blank">Dojo</a> and other JavaScript libraries), or COBOL source code depending on the desired target runtime environment (the generator is sensitive to both resulting source code language and resulting runtime environment in which that code will run). Being able to deploy the same EGL code to multiple environments enables ultimate platform flexibility, and breaks the barrier that today forces use of specific languages when developing for specific platforms. 
<p>
Portability is especially useful for businesses that currently deploy applications across a wide array of environments ranging from web browsers on client machines, to JEE on mid-tier application servers, to mainframe servers such as CICS or IMS. These environments are constantly changing. New platforms, such as WebSphere XD Compute Grid and others being developed in the <a href="http://eclipse.org/rt/" target="_blank">Eclipse Runtime Project</a> are emerging. A technology such as EGL allows the creation of applications that are able to be re-hosted across changing runtime environments without changing the source code. Instead changes to the model generators can either apply existing concepts to the new infrastructure or to allow the programmers to more easily apply new concepts on the new platform.

<h2>Scope</h2>

The scope of the EGL Development Tools project is restricted to:

<ul>
<li>First-class development tools for the EGL language, comparable in scope to the tools provided for the Java language by the <a href="http://www.eclipse.org/jdt/overview.php" target="_blank">Java Development Tools</a> project
<li>EGL language parser, compiler and generators, which can be invoked via the tools (e.g. by project builders) or command line (for headless compilation and generation)
</ul>
<p>
Because the EGL language itself (including keywords and syntax) will be managed within the scope of the EGL Development Tools project, we solicit feedback from the community on how best to manage its evolution. A formal community-driven specification process may need to be established to support this function.

<h2>Description</h2>

Initially, application developers will be able to develop, run, and debug EGL-generated Java and JavaScript applications and deploy to a Dynamic Web Project. These capabilities exist today in the <a href="https://www.ibm.com/software/rational/products/eglce/" target="_blank">EGL Community Edition</a> tool (<a href="http://www.youtube.com/watch?v=FxKTNgoT0cU" target="_blank">watch a simple Hello World video</a>).
<p>
The generators provided as part of the initial contribution are not designed for extensibility. We propose to work with the community to reimplement the existing Java and JavaScript generators on top of an extensible framework. Once completed, these generators can then be extended to support new runtime environments (e.g. OSGi-based runtimes), or new generators could be created using the extensible framework. 
<p>
<img src="eglgen.png" width=800>
<h2>Initial Contribution</h2>
Our goal is to provide capabilities that will make the initial contribution immediately useful. We intend to actively develop, refactor, extend, and grow the capabilities (either organically or through integration with other projects) over time.
<p>
The initial contribution to the project will come from an existing IBM product, Rational Business Developer. The initial contribution will be functionally comparable to the <a href="https://www.ibm.com/software/rational/products/eglce/" target="_blank">EGL Community Edition</a> tool. 
<p>
The initial contribution will include:

<ul>
<li><b>Tools</b>
<ul>
<li>EGL source editing
<ul>
<li>Editor with syntax highlighting, content assist, validation
<li>EGL build and deployment descriptor editors (editors to control how EGL source code is compiled and generated)
<li>EGL builders (full and incremental)
<li>Visual editor for WYSIWYG editing of Rich UI components
</ul>
<li>Project Explorer view integration
<ul>
<li>Integration to show EGL packages and source for EGL projects in Eclipse software development environment Project Explorer view
</ul>
<li>Supporting views and perspectives
<ul>
<li>Outline view showing hierarchy of EGL source
<li>Palette view with EGL and user-contributed widgets
</ul>
<li>EGL application debugging
<ul>
<li>Integrates with Eclipse Debugger
</ul>
<li>Deployment capabilities
<ul>
<li>Deployment to Tomcat
<li>Deployment of EGL services as REST and/or SOAP Web services
</ul>
<li>Eclipse Data Tools integration
<ul>
<li>Tools to generate EGL from database table or view
<li>Tools to validate and view EGL/SQL source
</ul>
</ul>
<li><b>EGL code compiler and generators</b>
<ul>
<li>EGL source compiler
<li>Java generator targeting JEE environments
<li>JavaScript generator
</ul>
<li><b>Documentation</b>
<ul>
<li>EGL langauge reference and developer guides
</ul>
</ul>

<h2>Relationship to other Eclipse projects</h2>

The basic approach used with EGL is that of model to code transformation.  This approach has a lot in common with Model Driven Development (MDD).  With EGL, a programmer is using a Domain Specific Language (DSL), in this case EGL, instead of UML directly to represent the �model� of their application. EGL is the DSL. The Eclipse Modeling Project has many elements that could be in support of EGL and vice-versa.  In particular EGL has its own implementation of a modeling infrastructure similar in concept to EMF but implemented to be able to scale to the needs of a programming language infrastructure.  It is the intent of this project to merge the appropriate elements of existing Eclipse technology into the EGL tool infrastructure but the opposite may also be true.

<h2>Legal Issues</h2>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->

The EGL trademark is owned by IBM. Discussions are underway to determine the appropriate process to accomodate using this trademark in the Eclipse project name.

<h2>Committers</h2>

The following individuals are proposed as initial committers to this project:

<ul>
<li>Tim Wilson, IBM (project lead)
<li>Tony Chen, IBM
<li>Paul Harmon, IBM
<li>Matt Heitz, IBM
<li>Brian Svihovec, IBM
<li>Joe Vincens, IBM
<li>Paul Pilotto, ASIST
<li>Bernd van Oostrum, ASIST
<li>Wim Delen, ASIST
<li>David Van Herzele, ASIST
<li>Aaron Allsbrook, ClearBlade
<li>John Trevisan, ClearBlade
<li>Rhys Priddle, FBDA
<li>Roland Zurawka, PKS
<li>Oleg Arsky, Synchrony Systems
<li>Corey Grimes, Synchrony Systems
<li>Richard Doust, Synchrony Systems
</ul>

<!-- <p>The following individuals are proposed as initial committers to the project:</p>

<dl>
	<dt>Committer 1, Affiliation</dt>
	<dd>Committer 1 is a committer on the *** project where he has made significant contributions over
	many years. S/he will be contribute to the *** functionality in this new project.</dd>
	<dt>Committer 2, Affiliation</dt>
	<dd>Committer 2 has provided significant contributions to the existing code base. S/he will contribute
	to the *** functionality in this new project.
	</dd>
</dl>
-->

<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>David Williams, IBM</li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li>To be disclosed</li>
</ul>

<h2>Project Scheduling</h2>

<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->

<ul>
<li>June 2010 &ndash; initial proposal on Eclipse.org
<li>August 2010 &ndash; creation of project
<li>December 2010 &ndash; final delivery of initial contribution
</ul>

<!-- 

<h2>Changes to this Document</h2>

	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>DD-Month-YYYY</td>
		<td>Document created</td>
	</tr>
</table>

 -->

<h2>Additional information</h2>

To better understand EGL:

<ul>
<li><a href="http://www.youtube.com/watch?v=FxKTNgoT0cU" target="_blank">Introduction to EGL video</a>
<li><a href="http://www.ibm.com/software/rational/cafe/docs/DOC-3216">Rational EGL Community Edition tool</a> (functionally comparable to proposed initial contribution)
</ul>

</body>
</html>