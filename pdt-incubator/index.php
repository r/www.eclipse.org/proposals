<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "PDT Incubator Proposal";
$pageKeywords	= "eclipse pdt incubator proposal";
$pageAuthor		= "Roy Ganor, Eclipse PDT Community";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("PHP Development Tools Incubator");
?>

<h1>PDT Incubator Project</h1>
<p>
The PDT Incubator is a proposed open source project under the PHP Development Tools (PDT) Project. This 
proposal is in the Project Proposal Phase (as defined in the Eclipse 
Development Process document) and is written to declare its intent and 
scope. This proposal is written to solicit additional participation and 
input from the Eclipse community. You are invited to comment on and/or 
join the project. Please send all feedback to 
the http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.pdt newsgroup.

<h2>Background</h2>
The goal of the PDT Incubator project is to experiment with alternative ideas and technologies for enhancing and complementing PDT. For example, the addition of Smarty source editing, Aspect oriented programming PDT, are possible areas of investigation for the PDT Incubator. In addition to this, the Incubator is also intended to serve as a forum for interested third parties and a place for the development of additional components and tools for PDT outside of the restrictions of the PDT project itself. This includes the development of additional source editing improvements and other components improvements. Incubator components are intended to stay in the Incubator until they are mature enough to be integrated into the main development stream. We intend to use the Athena build system for this project.

<h2>Initial Work Areas</h2>

<h3>APDT � Aspect PDT</h3>
The Aspect PHP Development Tools (APDT) [1] project provides Eclipse platform based tool support for AOSD with PHP. It has been developed by William Candillon during the Google Summer of Code 2008 [2] and mentored by Michael Spector.

<h3>SmartyPDT</h3>
The SmartyPDT [3] Editor adds a Smarty support for PDT. Smarty is a template engine for PHP, this project enables users to develop Smarty projects utilizing the power of PDT development tool. It has been developed by William Candillon and Roy Ganor.

<h3>AST View</h3>
A view to visualize the AST (abstract syntax tree) of a PHP file opened in the editor. Navigate from text selection to AST nodes and from nodes to selections, also to show binding information provided.

<h2>Out of Scope</h2>
Work in the PDT Incubator will be constrained to only those efforts that we expect to graduate (i.e. the code will eventually become part of PDT). It is not a playground for arbitrary development efforts. In addition, we should ensure that investment in the incubator never leaves the community with the perception that it is coming at the cost of important work on the PDT project.

<h2>Mentors</h2>
TBD

<h2>Initial Committers</h2>
<ul>
  <li> William Candillon - Engineering student at Telecom Lille 1, is the creator the Aspect PHP Development Tools (APDT) project that provides Eclipse platform based tool support for AOSD with PHP. </li>
  <li> Zhongwei Zhao � has an experience with J2EE development, as well as Eclipse plugin development with EMF (for custom emf editor), GEF and GMF (custom the code generated by GMF) , Encapsulate JDT debug with the GMF presentation.  Has contributed to Eclipse PDT in two main areas 0 editor semantic highlighting and PHAR support </li>
  <li> Roy Ganor - Current Eclipse PDT developer</li>
  <li> Michael Spector - Current Eclipse PDT developer</li></li>
  <li> Nick Boldt - Current Eclipse PDT developer</li>
  <li> Qiangsheng Wang - Current Eclipse PDT developer</li>
  <li> Shalom Gibly - Current Eclipse PDT developer</li>
</ul>
<p>
The initial committers will be all committers on the current incubator components. Commit rights will be handled on a component-by-component basis.
</p>
[1] http://code.google.com/p/apdt/<br>
[2] http://wiki.eclipse.org/Google_Summer_of_Code_2008<br>
[3] http://code.google.com/p/smartypdt/<br>

</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
