<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>eTrice</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
</style>

<body>
<p>The eTrice project is a proposed open source project under the <a
	href="http://www.eclipse.org/projects/project_summary.php?projectid=modeling.mdt">MDT Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is a forum
	that is available from http://www.eclipse.org/forums with a URL using the
	project's short name; it will be something along the lines of
	of http://www.eclipse.org/forums/eclipse.woolsey. EMO can (and will)
	provide assistance if you're not sure what to put here.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the <a href="http://www.eclipse.org/forums/eclipse.etrice">eTrice</a>
Forum.</p>

<h2>Background</h2>

<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->

<p>Domain Specific Languages (DSLs), whether they use a textual or graphical notation, are widely recognized
as a convenient way to formalize abstractions of a given domain. They allow the engineer
working on a problem in this domain to express his solution on exactly the level
of abstraction which is best suited for the domain and best adapted to the way
he states the problem and thinks about a solution.</p>

<p>For event driven real-time embedded systems engineers traditionally tend to use
general purpose (and sometimes low level) programming languages like
assembler, C and (usually a restricted subset of) C++. This way they have maximum
control over the memory and performance impact of every single detail of their
system. But the exponentially growing complexity of current software systems in the domains
of telecommunication, automotive and industrial automation calls for advanced
engineering methods. The challenge is to master the new complexity of highly distributed,
concurrent and large software systems while maintaining quality and reliablity.</p>

<p>In recent years UML has been adopted for modeling in many areas including
embedded systems. But while ULM1 lacked concepts for abstraction, e.g. the notion of ports and components,
which are agreed to be essential for distributed systems, UML2 suffers from
its mere complexity which is owed to its general and domain independent
applicability. Additionally there is a gap between the UML model and the semantics of the code produced
from it which is not addressed by the standard. State machines and ports
leave many degrees of freedom. So vendors of UML2 modeling tools independently
filled this gap while narrowing down the abundance of modeling elements
by stereotypes.</p>

<p>In the early 1990s a DSL called <a href="#room-book">[ROOM]</a>
(Real-Time Object Oriented Modeling) was proposed
by Bran Selic et al. This language was designed for the, at this time, most complex
real time systems developed for the upcoming mobile
telephony. In a way, ROOM can be thought of a specific implementation, including
the semantics of the model, of a subset of UML2 for the very specific needs
of those kinds of systems - even though ROOM was proposed before the advent of UML.
In its relation to UML2 ROOM is in a similar position as EMF compared to MOF
where the design was guided by the principles of simplicity and performance.
ROOM was tailored to suit the needs of a very special class of problems and
uses concepts that address them in a highly formalized way. Maybe it is worth
to stress that ROOM is not limited at all to the domain of telecommunication.
It is general enough to be applied to any distributed event driven real time system.</p>

<p>The proposed eTrice project aims at an implementation of the ROOM language
together with code generators and tooling for model editing.
For the embedded target a portable runtime library will be supplied as part of
the project which implements a platform abstraction, basic services like messaging, thread and
process creation and a layer with the invariant parts of the modeling elements .</p>

<h2>Scope</h2>

<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->

<p>The objectives of the eTrice project are to:</p>

<ul>
<li>provide an implementation of the real time modeling standard <a href="#room-book">[ROOM]</a></li>
<li>build exemplary but ready to use editors for ROOM models (textual and graphical)</li>
<li>create code generators and portable target runtime libraries for Java and C++, later also for ANSI-C</li>
<li>provide built-in support for modeling level debugging of the running target software:
state machine animation, data inspection and manipulation and message injection</li>
<li>provide built-in possibilities for sequence diagram creation from the running software</li>
<li>support distributed heterogenous systems out of the box</li>
</ul>

<h2>Description</h2>

<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->

<p>The eTrice project will strive to deliver state of the art tooling and target middleware for
the model driven development of industry quality event driven real-time software.
Emphasis will be laid on the usablity of tooling, produced code and middleware in real
industry projects.</p>

<p>The design will be guided by the following principles</p>
<ul>
<li><em>Simplicity</em>: eTrice will be kept as simple as possible to fulfill its purpose</li>
<li><em>Conceptual Integrity</em>:
<ul>
<li>eTrice will offer only <em>one</em> solution for a given class of problems</li>
<li>eTrice will offer coherent architecture conecpts</li>
</ul>
</li>
<li><em>Extensibility</em>: eTrice can be extended or changed in various ways by
<ul>
<li>extending the ROOM metamodel</li>
<li>extending or adding Editors</li>
<li>extending or adding new codegenerators e.g. for other languages or other middleware</li>
<li>extending or replacing the middleware</li>
</ul>
</li>
</ul>

<p>The eTrice project will create</p>

<ul>
<li>a ROOM metamodel</li>
<li>textual and graphical model editors for classes, structure, behavior, deployment and instance configuration</li>
<li>code generators</li>
<li>target middlewares as environment for the generated code</li>
</ul>

<h3>The ROOM Metamodel</h3>

<p>The ROOM metamodel will be based on EMF. Since there is no industry standard the project adopts
the language as it is described in <a href="#room-book">[ROOM]</a>.
The model will be completed by elements to describe deployment and configuration data.
Also the model will be refined where appropriate as the project evolves. E.g. a concept of
libraries will be introduced.</p>

<p>Model persistence will be in a textual, human readable form at a medium granularity.
This provides an easy way for a team to lock some parts and merge others if necessary.</p>

<h3>Model Editors</h3>

<p>Initial editors will be implemented using Xtext. The
concrete syntax will be similar to the one proposed in <a href="#room-book">[ROOM]</a> p. 493ff. Later graphical editors
for structure (component hierarchy) and behavior (state machines) will be added using GMF or
Graphiti.</p>

<h3>Code Generators</h3>

<p>Code generators will be based on Xpand and Xtend. Initially code generation in Java
and C++ will be supported. Later also an ANSI C version will follow.</p>

<h3>Target Middleware</h3>

<p>The middleware will
be supplied for exemplary OS/hardware combinations like Windows/PC and Linux/PC. A middleware
will be provided for each supported OS/hardware combination and target language.
Emphasis will be laid on easy portability.</p>

<h2>Relationship with other Eclipse Projects</h2>

<ul>
<li>eTrice will use EMF for the ROOM metamodel</li>
<li>eTrice will use Xtext for textual editors</li>
<li>eTrice will use Xpand and Xtend for code generation</li>
<li>eTrice will use GMF and/or Graphiti for graphical editors</li>
</ul>


<h2>Literature</h2>

<dl>
	<dt><a name="room-book">[ROOM]</a></dt>
	<dd>Bran Selic, Garth Gullekson, Paul T. Ward:
	<em>Real-Time Object Oriented Modeling</em>,
	New York: John Wiley, 1994 (ISBN 0-471-59917-4)</dd>
</dl>

<h2>Initial Contribution</h2>
<h3>Patterns and Experience from the Tool Trice</h3>
<p>The initial committers created between 1998 and 2002 a ROOM tool called Trice, that implements a good part of the ROOM language, including runtime and code generators for C++ and C. The tool is still used for the development of distributed realtime systems mainly in the domains of automation, automotive and telecommunications. 
Since the tool was developed completely from scratch with Visual C++, it would be very hard to migrate it to Eclipse, XText, EMF, GMF, ... The decision is to redo it completely. </p>
<p>So the contribution from the Trice Project will not be code, but </p>
<ul>
	<li>a deep understanding of the ROOM methodology and its application for real world problems</li>
	<li>lots of patterns and howtos</li>
	<li>a clear vision of what the eTrice project should achieve</li>
	<li>some pilot projects that can be converted from Trice to eTrice</li>
</ul>
<h3>eTrice Prototype</h3>
<p>The current eTrice prototype is a proof of concept that was created to get a better feeling for the way to go with XText Grammar, syntax and semantic models, code generators and runtime.</p>
<p>The prototype that will be contributed contains:</p>
<ul>
	<li>a ROOM grammar in XText</li>
	<li>a Java runtime</li>
	<li>Java code generators</li>
</ul>
<p>All other contributions will be developed from scratch.</p> 

<!-- 
<h2>Legal Issues</h2>
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->
 
<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

<p>The following individuals are proposed as initial committers to the project:</p>

<dl>
	<dt>Thomas Schuetz, project lead, protos (<a href="http://www.protos.de">www.protos.de</a>)</dt>
	<dd>Thomas has many years of experience in the development of real-time software for industrial automation, automotive and telecom applications. Together with Henrik and others he developed a ROOM tool
	which he used over a long period for modeling and code generation. He also has a sound knowledge in designing
	the middleware for event driven real-time software.</dd>
	
	<dt>Henrik Rentz-Reichert, committer, protos (<a href="http://www.protos.de">www.protos.de</a>)</dt>
	<dd>Henrik has many years of experience in the development of modeling tools. Together with
	Thomas S. and others he created a ROOM tool with code generators for C and C++ and a runtime
	library for the generated code. He has developed Eclipse RCP tools and successfully used
	technologies from the Eclipse Modeling Project like EMF, GMF, M2M/ATL and TMF/Xtext.</dd>
	
</dl>

<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Ed Merks</li>
	<li>Sven Efftinge</li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li>Thomas Jung, Tieto (<a href="http://www.tieto.com">www.tieto.com</a>)</li>
	<li>Klaus Birken, Harman Automotive (<a href="http://www.harman.com">www.harman.com</a>)</li>
	<li>Bjoern Eschrich, PARItec (<a href="http://www.paritec.de">www.paritec.de</a>)</li>
	<li>Georg Huba, Infineon (<a href="http://www.infineon.com">www.infineon.com</a>)</li>
	<li>Stephan Eberle, Geensys (<a href="http://www.geensys.com/">www.geensys.com</a>)</li>
	<li>Itemis (<a href="http://www.itemis.de/">www.itemis.de</a>)</li>
	<li>Christoph Kunz, CSC (<a href="http://www.csc.com/">www.csc.com</a>)</li>
	<li>Werner Keil, emergn (http://www.emergn.com/)</li>
	<li>Robert Schachner, RST (www.rst-automation.com)</li>
	<li>Vollrath Dirksen, NAT (http://www.nateurope.com/)</li>
	<li>Andrea Asprian, Individual</li>
	<li>Thomas Pibernik, Individual</li>
</ul>

<h2>Tentative Plan</h2>

<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->
 
<p>The project will start with modeling editors and codegenerators for static structure and statemachines. Other ROOM features will follow step by step.</p>

<table cellspacing=5>
	<tr>
		<td>July 2010:</td>
		<td>Proposal published and announced to Eclipse membership</td>
	</tr>
	<tr align="left">
		<td>August 2010:</td>
		<td>Initial Contribution</td>
	</tr>
	<tr align="left">
		<td>October 2010:</td>
		<td>Prototype of ROOM meta model and XText grammar</td>
	</tr>
	<tr align="left">
		<td>December 2010:</td>
		<td>Prototype of Java runtime and Java code generators, first tutorials</td>
	</tr>
	<tr align="left">
		<td>March 2011:</td>
		<td>Prototype of graphical editors for statemachines and actor structure, model level debugging</td>
	</tr>
	<tr align="left">
		<td>August 2011:</td>
		<td>First industrial project using eTrice</td>
	</tr>
	<tr align="left">
		<td>Further Steps:</td>
		<td>C++ runtime and code generators, further ROOM features, C runtime and code generators</td>
	</tr>
</table>


<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr align="left">
		<th>Date</th>
		<th>Change</th>
	<tr align="left">
		<td>16-June-2010</td>
		<td>Document created.</td>
	</tr>
	<tr align="left">
		<td>29-June-2010</td>
		<td>Added mentors and interested parties.</td>
	</tr>
	</tr>
		<tr align="left">
		<td>29-September-2010</td>
		<td>Added more interested parties.</td>
	</tr>	 
</table>

</body>
</html>