<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Kura - OSGi-based Application Framework for M2M Service Gateways</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<img src="images/kura_logo_small.png" width="160" height="48"/>

<p>The Eclipse Kura project is a proposed open source incubator project under the <a
	href="http://www.eclipse.org/projects/project_summary.php?projectid=technology">
	Eclipse Technology Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>


<h2>Background</h2>
<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->
<p>
Until recently, machine-to-machine projects have been approached as embedded systems 
designed around custom hardware, custom software, and custom network connectivity. 
The challenge of developing such projects was given by the large customization and 
integration costs and the small re-usability across similar engagements. 
The results were often proprietary systems leveraging proprietary protocols.
</p>
<p>
The emergence of the service gateway model, which operates on the edge of an M2M 
deployment as an aggregator and controller, has opened up new possibilities. 
Cost effective service gateways are now capable of running modern software stacks 
opening the world of M2M to enterprise technologies and programming languages. 
Advanced software frameworks, which isolate the developer from the complexity of the 
hardware and the networking sub-systems, can now be offered to complement the service 
gateway hardware into an integrated hardware and software solution.
</p>


<h2>Scope</h2>
<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->
<p>
The goals of the Eclipse Kura project can be summarized as:
</p>
<ol>
<li>Provide an OSGi-based container for M2M applications running in service gateways. 
Kura complements the Java 6 SE and OSGi platforms with API and services covering the most 
common requirements of M2M applications. These extensions include but they are not limited 
to: I/O access, data services, watchdog, network configuration and remote management.</li>
<li>Kura adopts existing javax.* API for its functionalities when available - for example 
javax.comm, javax.usb, and javax.bluetooth. When possible, Kura will select an open source
implementation of such API that is compatible with the Eclipse-license and package it in 
an OSGi bundle to include it in the Kura default build.</li>
<li>Design a build environment, which isolates the native code components and makes it 
simple to add ports of these components for new platforms in the Kura build and 
distribution.</li>
<li>Provide a development environment which allows developers to run M2M applications in 
an emulated environment within the Eclipse IDE, then deploy them on a target gateway and 
finally remotely provision the applications to Kura-enabled devices on the field.</li>
</ol>
<p>
Kura offers a foundation on top of which other contributions for higher-level M2M protocol 
implementations like ModBUS, CanBUS, ProfiBUS can reside.
</p>


<h2>Description</h2>
<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->
<p>
Kura aims at offering a Java/OSGi-based container for M2M applications running in service 
gateways. Kura provides or, when available, aggregates open source implementations for 
the most common services needed by M2M applications. Kura components are designed as 
configurable OSGi Declarative Service exposing service API and raising events. 
While several Kura components are in pure Java, others are invoked through JNI and 
have a dependency on the Linux operating system. 
</p>
<p>
Kura is currently planning this initial set of services: 
</p>
<ul>
<li><strong>I/O Services</strong>
	<ul>
		<li>Serial port access through javax.comm 2.0 API or OSGi I/O connection</li>
		<li>USB access and events through javax.usb, HID API, custom extensions</li>
		<li>Bluetooth access through javax.bluetooth or OSGi I/O connection</li>
		<li>Position Service for GPS information from a NMEA stream</li>
		<li>Clock Service for the synchronization of the system clock</li>
		<li>Kura API for GPIO/PWM/I2C/SPI access</li>
	</ul>
</li>
<li><strong>Data Services</strong>
	<ul>
		<li>Store and forward functionality for the telemetry data collected by the 
		gateway and published to remote servers.</li>
		<li>Policy-driven publishing system, which abstracts the application developer 
		from the complexity of the network layer and the publishing protocol used. 
		Eclipse Paho and its MQTT client provides the default messaging library used.</li>
	</ul>
</li>	
<li><strong>Cloud Services</strong>
	<ul>
		<li>Easy to use API layer for M2M application to communicate with a remote server. 
		In addition to simple publish/subscribe, the Cloud Service API simplifies the 
		implementation of more complex interaction flows like request/response or remote 
		resource management.</li>
		<li>Allow for a single connection to a remote server to be shared across more 
		than one application in the gateway providing the necessary topic partitioning.</li>
	</ul>
</li>	
<li><strong>Configuration Service</strong> 
	<ul>
		<li>Leverage the OSGi specifications ConfigurationAdmin and MetaType to provide 
		a snapshot service to import/export the configuration of all registered services 
		in the container.</li>
	</ul>
</li>	
<li><strong>Remote Management</strong>
	<ul>
		<li>Allow for remote management of the M2M applications installed in Kura 
		including their deployment, upgrade and configuration management. The Remote 
		Management service relies on the Configuration Service and the Cloud Service.</li>
	</ul>
</li>
<li><strong>Networking</strong>
	<ul>
		<li>Provide API for introspects and configure the network interfaces available 
		in the gateway like Ethernet, Wifi, and Cellular modems.</li>
	</ul>
</li>
<li><strong>Watchdog  Service</strong>
	<ul>
		<li>Register critical components to the Watchdog Service, which will force a 
		system reset through the hardware watchdog when a problem is detected.</li>
	</ul>
</li>	
<li><strong>Web administration interface</strong>
	<ul>
		<li>Offer a web-based management console running within the Kura container 
		to manage the gateway.</li>
	</ul>
</li>
</ul>
<p>
Kura will provide a pre-made build for popular open hardware platforms like the RaspberryPi.
</p>

 
<h2>Why Eclipse?</h2> 
<!-- 
	Answer these two questions:	What value does this project bring to the Eclipse
	community? What value do you expect to obtain from hosting your project at Eclipse?
	
	What value do you get by having your project at Eclipse over and above the value
	of hosting at Eclipse Labs?
 -->
<p>
Being an M2M project, Kura aims at becoming another component of the interesting 
set of technologies grouped under the Eclipse M2M Industry Working Group umbrella. 
Kura already has a strong relationship with other Eclipse projects:
</p>
<ul>
<li><a href="http://www.eclipse.org/paho/">Eclipse Paho</a> is the default message 
protocol library used in Kura. The default communication protocol in Kura is MQTT and 
it is used to transport both data and device management messages.</li>
</li>
<li>Eclipse Equinox is the default OSGi container for Kura.</li>
<li><a href="http://www.eclipse.org/proposals/rt.concierge/">Eclipse Concierge</a>, 
a small-footprint OSGi container optimized for embedded devices, will be considered 
as another runtime platform for Kura.</li>
</ul>
<p>
Kura originates from Eurotech Everyware Software Framework. 
The UI client view of that framework has already been contributed to the Eclipse Paho 
project.
</p>


<h2>Initial Contribution</h2>
<!-- 
	Projects are expected to arrive at Eclipse with existing code.
	
	Describe the existing code that will be contributed to the project. Please provide
	a couple of paragraphs describing the code with modest detail, including important
	information like code ownership (who holds the copyright?), and some consideration
	of community that exists around the code. Include a listing of third-party libraries 
	and associated licenses.
 -->
<p>
The initial contribution of the Kura project will be a large subset of the current
Eurotech Everyware Software Framework. In particular:
</p>
<ul>
<li>Kura source code and build system for most of the services described above 
including the web management administration UI</li>
<li>A Kura M2M application developer's guide with an example application</li>
<li>Documentation on the Kura application protocol to perform remote resource management 
and remote administration of M2M applications</li>
<li>A build of Kura for an open-hardware platform like the RaspberryPi</li>
</ul>

 
<h2>Legal Issues</h2>
<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->
<p>
Eurotech will go through the Eclipse legal review process to make sure all Kura 
dependencies are eligible for contribution. While Kura has selected open-source 
components with friendly licenses, a detailed review will be conducted with 
the Eclipse legal team.
</p>
<p>
Kura has already been tested on open Java VM implementations like OpenJDK.
</p>


 
<h2>Committers</h2>
<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->
<p>
The following individuals are proposed as initial committers to the project:
</p>
	<dl>
		<dt>Wes Johnson, Eurotech</dt>
		<dd>Wes Johnson is the Director of Software Engineering at
			Eurotech Inc. He is one of the initial promoters and developers of
			Eclipse Kura. He specializes in framework design, Java, OSGi, Cloud
			Computing, the Internet of Things (IoT), Machine-to-Machine (M2M)
			communications, and application software development. He lives in
			Vancouver Washington where he earned a BS in Computer Engineering and
			Physics from Portland State University.</dd>
		<dt>Marco Carrer, Eurotech</dt>
		<dd>Marco Carrer is leading the M2M software efforts at Eurotech.
			He was one of the architects of the Eclipse Kura project and he
			actively contributed the integration of the Eclipse Paho technology
			into the Kura Data Services. Before joining EUROTECH as VP of
			Software Engineering, Marco spent 13-years in Oracle, where he was
			responsible for the design and development of several products in the
			areas of Web Services, Enterprise Collaboration, and CRM Service.
			Marco Carrer holds a Laurea in Electronic Engineering from University
			of Padova, a Master in Computer Science from Cornell University, New
			York.</dd>
		<dt>Cristiano De Alti, Eurotech</dt>
		<dd>Cristiano De Alti is a principal software developer at
			Eurotech S.p.A. He is one of the initial developers and designers of
			Eclipse Kura where he contributed the remote configuration management
			service, the remote deployment management service, and the data
			service. He has 10+ years of experience in designing and deploying
			embedded systems, especially in the area of firmware development for
			microcontrollers, embedded Linux distributions and wireless sensor
			networks. He received his degree in electronic engineering at the
			University of Padua.</dd>
		<dt>Dave Woodard, Eurotech</dt>
		<dd>David was the project lead for Arrow Electronic's Arrow Cloud
			Connect initiative. This initiative provides engineers with a low
			overhead introduction to IoT technologies. Since joining Eurotech,
			David has been contributing to the Eclipse Kura project especially in
			the area of the networking and web administration. David holds a
			Masters of Engineering degree in Electrical Engineering from Portland
			State University</dd>
		<dt>Elbert Evangelista, Eurotech</dt>
		<dd>Elbert has over ten years of experience, previously
			developing embedded software for printers and multi-function devices
			at Xerox, then working on data collection services for cloud
			connected energy monitors at Verlitics. He joined Eurotech in 2012
			and has been contributing primarily to the networking and web admin
			services on the Kura project. He has a BS in Computer Science and
			Physics from the University of Portland.</dd>
		<dt>Pierre Pitiot, Eurotech</dt>
		<dd>Pierre has been working in embedded software development for
			25 years with systems deployed in a variety of verticals including
			oil fields, industrial and railway transportation. Pierre is part of
			the Eclipse Kura development team in Eurotech contributing the
			support for high-level protocols. He holds a PhD in geology and an
			engineering degree in Software and Electronics in Lyon (France)
			University.</dd>
		<dt>Walt Bowers, Hitachi</dt>
		<dd>Chief Architect OSGi Solutions at Hitachi.</dd>
		<dt>Will Frazer</dt>
		<dd>Will Frazer has more 20+ years of experience in embedded
			systems especially in the area of embedded Linux customization and
			firmware development. His contributions to Kura focus on the I/O
			Service including support for GPIO and I2C. He graduated in
			Electronic Engineering at Limerick Institute of Technology, Ireland.
		</dd>
		<dt>Ilya Binshtok</dt>
		<dd>Ilya has over 15 years of embedded software development
			experience across multiple platforms. He specializes in C, Java,
			OSGi, and wireless networking. Ilya contributes to the Eclipse Kura
			project by supporting various cellular and Wi-Fi devices. He earned a
			BS in Computer Engineering from the University of Kansas.</dd>
		<dt>Alberto Codutti</dt>
		<dd>Alberto is the newest member of the Eclipse Kura project. He
			recently joined the team and is going to contribute on the Kura web
			management interface.</dd>
		<dt>Hitesh Dave</dt>
		<dd>Mr. Hitesh Dave is a Solutions Architect in Hitachi with
			expertise in enterprise application integration and development. He
			bring over more than 20 years of experience in developing software
			solutions utilizing various middleware technologies. His expertise
			include service oriented architecture, middleware messaging
			technologies, networking protocols and java based application
			development. He is well versed in the M2M architecture and embedded
			systems development. He was one of the key contributor of the
			Internet of Things (IoT) in Motion demo development presented during
			JavaOne 2013.</dd>
	</dl>

	<p>We welcome additional committers and contributions.</p>

<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>
<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->
<p>The following Architecture Council members will mentor this
project:</p>
<ul>
	<li>Benjamin Cabé</li>
	<li>Gunnar Wagenknecht</li>
</ul>


<h2>Interested Parties</h2>
<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->
<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>
<ul>
	<li>Dave Locke, IBM</li>
	<li>Andy Piper, Eclipse Paho project co-lead</li>
	<li>Pascal Rapicault, Rapicorp, Inc.</li>
	<li>Kai Kreuzer, openHAB</li>
	<li>Werner Keil, Eclipse UOMo Project lead</li>
	<li>Paul Pishal, Hitachi CTA</li>
	<li>Walt Bowers, Hitachi CTA</li>
</ul>


<h2>Project Scheduling</h2>
<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->
<p>
Kura aims at the initial contribution to be completed by Q4 2013.
</p>

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>16-June-2013</td>
		<td>Document created</td>
	</tr>
	<tr>
		<td>30-June-2013</td>
		<td>Incorporated Eurotech's internal feedback</td>
	</tr>
	<tr>
		<td>11-July-2013</td>
		<td>Incorporated Benjamin Cabé's feedback</td>
	</tr>
	<tr>
		<td>15-July-2013</td>
		<td>Incorporated Dave Locke's feedback</td>
	</tr>
	<tr>
		<td>10-Aug-2013</td>
		<td>Add more interested parties and Kura logo</td>
	</tr>
	<tr>
		<td>21-Oct-2013</td>
		<td>Added additional initial committers. Added bios for all committers.</td>
	</tr>
	</table>
</body>
</html>