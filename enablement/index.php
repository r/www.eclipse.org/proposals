<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Proposal for the DTP Enablement Project";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
      <div id="midcolumn">
        <h1>
        The DTP Enablement Project
        </h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("The DTP Enablement Project");
?>
<P><B>An Eclipse Data Tools Platform Project Proposal<BR>April 2006</B>

</P>
<H2>Introduction</H2>
<P>The Enablement Project is a proposed open source project under the
<A HREF="http://www.eclipse.org/datatools">Eclipse Data Tools
Platform</A> top-level project.</P>
<P>This proposal is in the Project Proposal Phase (as defined in the
<A HREF="http://www.eclipse.org/projects/dev_process/">Eclipse
Development Process</A> document) and is written to declare its
intent and scope. This proposal is written to solicit additional
participation and input from the Eclipse community. You are invited
to comment on and/or join the project. Please send all feedback to
the <A HREF="http://www.eclipse.org/newsportal/thread.php?group=eclipse.dtp">eclipse.dtp</A> newsgroup.</P>
<H2>Background</H2>
<P>The <A HREF="http://www.eclipse.org/datatools">Eclipse Data Tools
Platform</A> (DTP) project provides extensible frameworks and
exemplary tools for working with heterogeneous data sources. DTP is
thus a powerful enabler for data-centric applications build both in
the Eclipse IDE and RCP environments.</P>

<P>While DTP is a solid foundation, the development of data-centric
applications typically assumes supported access to particular data
sources. For example, DTP 0.7 concentrated on relational data
structures and hence relational databases, taking <A HREF="http://db.apache.org/derby/">Apache
Derby</A> as the example implementation target. We at DTP recognize,
however, that many potential users and extenders require access to
other data sources. To this end, DTP 0.7 provided generic JDBC
connection capability for relational data stores. While such generic
capability is welcome, it often does not meet the specialized
requirements necessary to fully work in particular cases. Finally,
DTP 0.7 provides an abstraction of the type of data being read via
the <A HREF="http://www.eclipse.org/datatools/project_connectivity/connectivity_doc/OdaOverview.htm">Open
Data Access</A> (ODA) components. As with the relational data store
examples, DTP 0.7 provided a couple of examples leveraging ODA. For
full use of ODA, however, specialized support for data sources is
required as well.</P>
<P>To meet these needs, the DTP Project Management Committee (PMC) is
now proposing the &ldquo;Enablement&rdquo; project. The intention of
this project is to invite groups willing to provide specialized data
source support into the DTP project. While DTP could simply encourage
an external community &ndash; both open source and commercial &ndash;
providing such specialized support (and we will), there are numerous
advantages to also having the Enablement project. These advantages
fall into two main categories:</P>

<UL>
	<LI><P STYLE="margin-bottom: 0in">Being an Eclipse project, all
	contributions will adhere to the Eclipse Intellectual Property (IP)
	policies. Thus, the Eclipse community at large will benefit by
	having access to open-source, IP-verified specialized data source
	support for DTP. 
	</P>
	<LI><P>Groups providing specialized data source support within
	Enablement will work closely with committers on the core DTP
	frameworks and tools projects. This will enable a tight feedback
	loop where Enablement committers can learn how best to leverage DTP
	frameworks and tools, and core DTP framework and tools committers
	can gain invaluable input for evolving their components to better
	align with community needs. 
	</P>
</UL>
<H2>Scope</H2>
<P>The Enablement project is intended to provide specialized support
built on DTP core extension points and API. Enablement members can
use the full range of these or any subset they see fit to provide
specialized support for their particular data source. In general the
PMC expects to see contributions to Enablement that demonstrate good
usage of core DTP frameworks and tools, and not those of greater
scope than could be reasonably allowed by a simple specialization
delivery. Functionality beyond the confines of simple specialization
is always welcome, but must be contributed by working with the DTP
core frameworks and tools committers to ensure that it is delivered
in a vendor neutral way as part of DTP core, and hence available to
any DTP extender.</P>
<P>To insure that contributions to the Enablement project remain
within the intended scope, component teams in Enablement will submit
a design document detailing the planned contributions prior to any
deliveries. This document will be first reviewed by the DTP
Architecture Council and then the DTP community at large. Final
approval of the design will be made by the DTP PMC.</P>
<H2>Existing standards and projects leveraged</H2>
<P>Standards supported within Enablement are up to the specific data
source being targeted. For example, it is reasonable to expect that
relational data sources would use JDBC as part of specialization
support.</P>

<H2>Organization</H2>
<P>The Enablement project will be divided into a number of
components, one for each specialized data source. Each component will
contain one or more Eclipse plug-ins as part of their deliverables. A
component lead will be designated for each component, and these leads
will work with the Enablement project lead to insure successful
execution of project tasks. An important responsibility for all
component leads is make sure that sufficient committer support is
supplied in an on-going basis to fix bugs and evolve components in
line with DTP core changes. Committer rights within Enablement will
be limited at the component level.</P>
<H3>Proposed initial committers and project lead</H3>
<P>The proposed project lead for Enablement is John Graham
(<A HREF="mailto:john.graham@sybase.com?subject=DTP%20Enablement%20Project:">john.graham@sybase.com</A>),
the current PMC chair for DTP. Additional committers already members
of DTP will contribute default specializations (as described below)
and perhaps full specializations in certain cases.</P>
<P>Interested potential committers are kindly invited to express it
on the newsgroup or via email.</P>
<H3>Interested parties</H3>
<UL>
	<LI><P STYLE="margin-bottom: 0in">Actuate: For XML data source. 
	</P>

	<LI><P STYLE="margin-bottom: 0in">IBM: For IBM databases. 
	</P>
	<LI><P>Sybase: For Sybase databases. 
	</P>
</UL>
<H3>Initial contribution</H3>
<P>Within DTP 0.7 there are a number of default support components
for a set of relational databases. The support offered by these
default components varies, but it is a start. The intention is to
move all of such components into the Enablement project where, in the
ideal scenario, they will be picked up by Enablement committers who
will extent them for full support. At the very least, Enablement will
provide partially specialized support for parity with DTP 0.7 and the
<A HREF="http://www.eclipse.org/webtools/wst/components.html">WTP
&ldquo;rdb&rdquo;</A> component set.</P>
      </div>
    </div>
   
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
