<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Higgins, The Trust Framework");
?>	
	
  
    <h1>higgins, the trust framework</h1><br>
        
      <p>This proposal 
    is in the Project Proposal Phase (as defined in the
    <a href="/projects/dev_process/" style="color: blue; text-decoration: underline; text-underline: single">
    Eclipse Development Process document</a>) and is written to declare the 
    intent and scope of a proposed Technology PMC Project called Higgins, the 
    Trust Framework Project, or Higgins. (Note that its original name was the
      Eclipse Trust Framework or ETF, so some of the material will still have
      the old name.) In addition, this proposal is written to 
    solicit additional participation and inputs from the Eclipse community. You 
    are invited to comment on and/or join the project. Please send all feedback 
    to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.etf" style="color: blue; text-decoration: underline; text-underline: single">
    http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.etf</a> newsgroup.</p>
  


  <h2>Background</h2>
  
     
      <p><span style='color:black'> </span>
      

 The need to improve interoperability, security and privacy in loosely 
      coupled architectures, especially those that span organizational 
      boundaries has in recent years increased the prominence of identity 
      management systems. These systems maintain a real or virtual directory of 
      identities, each with profile attributes, roles, access permissions and so 
      on. There is often a need to manage more than just �point� identities 
      (i.e. the digital identifiers and profiles of people and systems). Many 
      applications need to manage relationships <i>between</i> identities�what 
      we call the <i>social context</i>. Examples of these applications include 
      groupware, virtual directories, social networking and patient centered 
      healthcare.
      <p>We use the term <i>context</i> to cover a range of underlying 
      implementations from directory systems like LDAP to social networking 
      systems like FOAFnet or Friendster. A context can be thought of as a 
      distributed container-like object that contains the digital identities of 
      multiple people or processes. A context can represent a team, a 
      department, an association, a mailing list, a website, a customer group, a 
      personal buddy list, or a list of web services.</p>
      <p>The Higgins Trust Framework platform intends to address four challenges: the lack of common 
      interfaces to identity/networking systems, the need for interoperability, 
      the need to manage multiple contexts, and the need to respond to 
      regulatory, public or customer pressure to implement solutions based on 
      trusted infrastructure that offers security and privacy.</p>
      <p><b>Lack of common interfaces.</b> The application developer who needs 
      to integrate an identity/networking system is forced to learn the 
      intricacies of each different system. The lack of a common API means that 
      this learning investment is not transferable. This project intends to 
      develop a common API/framework, provide sample plug-ins, and 
      encourage developers to create �provider� plug-ins for existing and new 
      identity/networking systems. </p>
      <p><b>The need for interoperability. </b>Although there have been and will 
      likely continue to be attempts to create a single universal identity 
      system, the reality is that we�ll live in a heterogeneous world for a very 
      long time. Rather than introduce yet another new identity system, instead
      Higgins introduces a new �context� abstraction and allows developers to create 
      adapters to legacy systems. Systems operating above the abstraction layer 
      have to potential to link identities across identity system boundaries.</p>
      <p><b>The need to manage multiple contexts.</b> The existence of common 
      identity/networking framework also makes possible new kinds of 
      applications. Applications that manage identities, relationships, 
      reputation and trust across <i>multiple</i> contexts. Of particular 
      interest are applications that work on behalf of a user to manage their 
      own profiles, relationships, and reputation across their various personal 
      and professional groups, teams, and other organizational affiliations 
      while preserving their privacy. These applications could provide users 
      with the ability to: discover new groups through shared affinities; find 
      new team members based on reputation and background; sort, filter and 
      visualize their social networks. Applications could be used by 
      organizations to build and manage their networks of networks.</p>
      <p><b>The need for trusted infrastructure. </b> Working in partnership with our development partners and academic research 
      groups, this project will create a key part of the open source 
      infrastructure required for an open, accountable, socially-searchable web 
      while ensuring privacy and personal control over identity information.</p>


  


  <h2>Description 
      (scope and goals)</h2>
   
     
     <p>Our goals are to:<p>1.&nbsp;&nbsp;&nbsp;&nbsp;Create a framework/API � an abstraction layer for 
      identity and social networking services</p>
      <p>2.&nbsp;&nbsp;&nbsp;&nbsp;Create a set of exemplary context �provider� 
      implementations (plug-ins)</p>
      <p>3.&nbsp;&nbsp;&nbsp;&nbsp;Create an exemplary app that demonstrates how to use the 
      extensible 
      framework</p>
      <p>4.&nbsp;&nbsp;&nbsp;&nbsp;Enable developers to leverage Higgins in their applications</p>
      
          <p><b>(1) Framework/API</b></p>
      <p>The extensible framework will support an API for use by Eclipse plug-ins and 
      applications. The API could also be accessible via a web services 
      interface. The API will provide:</p>
      <p>1.&nbsp;&nbsp;&nbsp;&nbsp;
      Initialization of the framework platform </p>
      <p>2.&nbsp;&nbsp;&nbsp;&nbsp;
      The <i>context</i> interface (implemented by extensions to 
      the context provider extension point)</p>
      <p>3.&nbsp;&nbsp;&nbsp;&nbsp;
      Context management services (e.g. managing the registry of 
      context provider plug-ins; resolving a context reference to a network 
      location and a context provider implementation plug-in; loading and 
      discarding of contexts, etc.)</p>
      <p>The center of the extensible framework design is the plug-able <i>context</i> 
      interface. A context is a container of <i>facets</i>. A facet a person or 
      process that has been authenticated within its containing context. A facet 
      has a <i>profile</i> which is comprised of a set of RDF properties and 
      values (e.g. name, address, etc.). A facet also has one or more <i>roles</i> 
      within the context. The set of profile properties and the set of roles and 
      the access rights for each role are defined by and controlled by the 
      context provider implementation. </p>
      
      <p>Context provider implementations are responsible for:</p>
      <p>1.&nbsp;&nbsp;&nbsp;&nbsp;
      Authentication of credentials for access</p>
      <p>2.&nbsp;&nbsp;&nbsp;&nbsp;
      Authentication of each facet within the context</p>
      <p>3.&nbsp;&nbsp;&nbsp;&nbsp;
      Authorization of access to facet profile data using 
      role-based access control lists</p>
      <p>4.&nbsp;&nbsp;&nbsp;&nbsp;
      Facet search and editing functions</p>
      <p>5.&nbsp;&nbsp;&nbsp;&nbsp;
      Support for adding tag properties to facets and on the 
      links between facets</p>
      <p>6.&nbsp;&nbsp;&nbsp;&nbsp;
      Replication/distribution of context data to Higgins clients</p>
      <p>7.&nbsp;&nbsp;&nbsp;&nbsp;
      Synchronization of context data </p>
      <p>8.&nbsp;&nbsp;&nbsp;&nbsp;
      Persistence and encryption of context data</p>
      <blockquote>
      <p>Note1: Some context providers will provide only a subset of the 
      features listed above. </p>
      <p>Note2: The communications protocols and topology (e.g. client/server or 
      P2P) are implementation dependent.</p>
      
      </blockquote>
      <p><b>(2) &nbsp;Exemplary Context plug-ins</b></p>
      <p>Our plan is to create the following exemplary set of context �provider� 
      plug-ins:</p>
      <p>1.&nbsp;&nbsp;&nbsp;&nbsp;
      A simple Buddy List plug-in. We will create an EMF-based 
      Context data model and use EMF, SDO and Eclipse ECF for replication and 
      synchronization</p>
      <p>2.&nbsp;&nbsp;&nbsp;&nbsp;
      A plug-in to an existing enterprise directory server</p>
      <p>3.&nbsp;&nbsp;&nbsp;&nbsp;
      A plug-in for the Identity Commons (OASIS XRI-based) 
      identity system</p>
      <p>4.&nbsp;&nbsp;&nbsp;&nbsp;
      A plug-in for a WS-Trust/etc. based identity system</p>
      <p>5.&nbsp;&nbsp;&nbsp;&nbsp;
      A plug-in for the FOAFnet networking system</p>
      <p>6.&nbsp;&nbsp;&nbsp;&nbsp;
      A plug-in for Microsoft�s Outlook email client that 
      creates a context containing a network of interlinked facets representing 
      the user�s social network</p>
      
      <p><b>&nbsp;(3) Exemplary Application</b></p>
      <p>The extensible Higgins Trust Framework makes possible new kinds of applications that manage 
      the user�s identity across <i>multiple</i> contexts. We plan on creating 
      an RCP demonstration application that can manage contexts from any of the 
      above exemplary plug-ins that includes:</p>
      <p>1.&nbsp;&nbsp;&nbsp;&nbsp;A UI for viewing, editing and linking identities 
        in multiple contexts </p>
        <p> 2.&nbsp;&nbsp;&nbsp;&nbsp;A UI for rating/reputation </p>
        <p>3.&nbsp;&nbsp;&nbsp;&nbsp;Network visualization: ability to overlay the 
        networks of multiple contexts to determine common relationships and 
        characteristics</p>
        <p>4.&nbsp;&nbsp;&nbsp;&nbsp;Social network search functionality</p>
      
      <p><b>(4) Enable developers to leverage Higgins </b></p>
     <p>Our hope 
      is that developers can use Higgins to more easily implement identity- and 
      networking-related functionality in their applications, instead of 
      creating this functionality from scratch. Here are some examples. They 
      could use an existing Higgins context provider to manage the list of 
      identities, member records, etc. as well as all associated attribute data 
      used by their application. They could use Higgins� context abstraction as 
      �glue� to integrate multiple existing enterprise directories.&nbsp; They could 
      add �peripheral vision� of other co-worker�s member�s online presence, 
      contact information, and reputation to existing apps. 
    
 
  


  <h2>Organization</h2>
  
    
      <p>We propose this project should be undertaken as a Technology project 
        rather than as part of the Eclipse Platform. Being a Technology project 
        gives it room to experiment without disruption to other Eclipse Platform 
        development work.</p>
<p>The life cycle of this project may be similar to the Equinox project.� That 
  is, once this work matures, it may be incorporated into the Eclipse Platform 
  to provide a common open source substrate that industrial/commercial users could 
  use to build solutions. Alternatively, it may continue as a technology subproject. 
</p>
      <p><b>Suggested Project Lead and Committers</b></p>
      <p>This section captures the list of organizations that have expressed interest 
  in the project and/or its components, and as such will be updated periodically 
  to reflect the growing interest in this project.<br>
  <br>
  Rather than canvas the Eclipse community at large, the submitters of this proposal 
  welcome interested parties to post to the <a
href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.etf">eclipse.technology.etf newsgroup</a> 
  and ask to be added to the list as interested parties or to suggest changes 
  to this document.</p>
      <b>Initial Set of Committers </b> 
      <p>Paul Trevithick 
  (Project Lead)<br>
  SocialPhysics.org; paul at SocialPhysics.org</p>
      <p>Dmitry Bakuntsev <br>
      International Technology Group (ITG)</p>
      <p>John Beatty</p>
      <p>Andy Dale <br>
      ooTao</p>
      <p>Ken Gilmer</p>
      <p>Scott Lewis <br>
        Composent, Inc.</p>
      <p>Peter Nehrer <br>
  S1 Corporation</p>
      <p>Mary Ruddy<br>
  SocialPhysics.org; mary at SocialPhysics.org</p>
      <p>Paul Weitz</p>
      
  


  
    <strong>Interested Parties</strong>
    &nbsp;
  
  
    
    
  
  
    SocialPhysics.org� 
    <a href="http://www.socialphysics.org/">http://www.socialphysics.org</a>
  
  
  
    IBM&nbsp;
     <a href="http://www.ibm.com/">http://www.ibm.com/</a>
  
  
  
    Inpriva&nbsp;
     <a href="http://www.inpriva.com/">http://www.inpriva.com/</a>
  
  
 
    ITG&nbsp;
     <a href="http://www.intertechnogroup.com/">http://www.intertechnogroup.com/</a>
  
  
    ooTao&nbsp;
     <a href="http://www.ootao.com/">http://www.ootao.com/</a>
  
  
    Berkman Center for Internet &amp; Society at 
      Harvard Law
     <a href="http://cyber.law.harvard.edu/home/">http://cyber.law.harvard.edu/home/</a>
  

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
