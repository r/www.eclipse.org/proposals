<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "The Riena Project";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>The Riena Project</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Riena");
?>


		<h2>Introduction</h2>


<p>

This proposal is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and is written to declare its intent and scope. This

 proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please

  send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.riena">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.riena</a> newsgroup.



<h2>Background</h2>

<P>



The Eclipse RCP forms a solid and sound basis for building rich client applications. This has been proven by the number of applications built on 

the Eclipse platform.

<p>

As this platform matures, it continues to attract interest from the enterprise world. The typical scenario for enterprise applications involves a 

multi-tier system architecture with a strong emphasis on a sophisticated UI and robust back end for business processing, data storage and system 

integration.

<p>

Eclipse RCP based on Equinox and other Eclipse technologies provide most of the components required to develop enterprise applications e.g. user interface, data binding, extension configuration, software update, authentication/authorization, reports, persistence. In the scope of a multi-tier enterprise application these components are either located on client or server or on both, therefore additional support is needed.



<h2>Description</h2>

<p>

The Riena platform will be the foundation for building multi-tier enterprise client/server applications. As such Riena will broaden the usage of the service oriented architecture of OSGi/Equinox by providing access to local and remote services in a transparent way. Using this uniform programming model, the components of Riena and the business components of the enterprise application can be developed regardless of their target location. Components are later easily placed on client or server depending on the business requirements.

<p>

Components from existing Eclipse projects will be adapted to support the distributed aspect of enterprise applications. Furthermore new components 

will be provided to support the special needs of building and running large enterprise applications.

<p>

As an example the Riena user interface components will support a business process oriented user interface visualization and navigation metaphor 

which can be used as alternative to the default workbench layout.

<p>

So Riena will provide a homogeneous platform for enterprise business applications from the user interface to backend integration, all based on the 

proven Equinox/Eclipse Platform.





<h2>Scope</h2>

<p>

Riena contains a set of features and components that are required to create an enterprise application from the front-end of the UI to the backend server. Some of these components listed here are created within this projects, others are existing Eclipse components adapted for the use within Riena.



<h3>Support for distributed Enterprise Applications</h3>

<p>
The Eclipse OSGi framework (Equinox) can run not only in a standalone client but also on the server side. Developing integrated rich enterprise applications in a homogeneous and consistent way not only requires components to be run either on the client or server side, it also requires a robust and flexible communication support between the application tiers. All of this is based on the standard Equinox Platform, so that the application developers don�t have to worry about the technical issues.

<p>
The Riena remote service support will make OSGI Services remotely accessible. It will be possible to access remote OSGi Services as if they exist locally. It will also support OSGi's Declarative Services and Dependency Injection for these Remote OSGi Services.

<p>
Eclipse already contains a multipurpose communication framework called ECF. It can be used for chat, VoIP, application- and data sharing and many of other purposes inside the IDE and in equinox-based applications on client and server. It also contains a remote services concept that supports OSGi Services.

<p>
Riena�s own remote service implementation is only focused and optimized for web service communication based on the equinox platform. 
<p>
Both, ECF and Riena remote services are able to fulfill the requirements of enterprise applications.


<h3>Support for business process oriented User Interfaces</h3>



<p>

Typical RCP applications are based on the workbench metaphor used heavily in the Eclipse IDE. The workbench metaphor is very flexible and generic, which supports the developers in the development stage. However, enterprise applications very often need a different business process oriented UI metaphor in order to support end users more effectively.

<p>

Riena will provide a UI metaphor that supporting the interaction of typical business process oriented applications. The navigation and visualization should help the user to focus on the current process and task, but also allow him to work simultaneously on a different process.

<p>

Riena�s approach will be based as much as possible on already existing RCP constructs (e.g. Layout Managers, views, Extension Points) and use existing Eclipse technologies (e.g. SWT, JFaces, Data-Binding).




<h3>Integrate and leverage existing Eclipse components into the Riena platform</h3>

<p>

Some of the components needed for an enterprise application already exist within the Eclipse platform. Those components need to be identified and adapted. Extending these components should only be done through their provided extension mechanisms. New requirements or necessary changes need to be discussed with the components owner. Other components that do not yet exist need to be created either as part of Riena or in related projects.



<h3>Software Update/Provisioning</h3>

<p>

Enterprise applications share many components and resources between client and server. This requires that server and client application versions are kept in sync. Additionally software updates are not triggered by the end user in enterprise applications but are governed by the backend-system based on profiles, roles and permissions.

<p>

This requires a different software update (provisioning) system than for example an IDE which is used by a developer. Frequent and mandatory updates require a higher level of optimization regarding how software components are updated on the client to reduce bandwidth and time. Deltas, binary patches between two software versions, support this by keeping update packages small even when enterprise applications become large and complex. As a side effect it also improves performance in mobile scenarios with low bandwidth like GPRS or ISDN connections.

<p>


The ongoing provisioning work in the Equinox incubator and the Maya project already target most of these requirements. The Riena project will offer contributions for features needed for distributed enterprise applications.



<h3>Authentication and Authorization</h3>

<p>

Enterprise applications access business logic and data remotely. Authentication and Authorization provides information about the currently active user and its individual access rights available to every component and presentation part concerned, independent whether that component resides on the client or on the server. 

<p>

The Equinox Security effort in the incubator is a very good candidate to be used by Riena. Riena will adapt Equinox Security within its enterprise application platform to fulfill these aspects.

<p>

Single-Sign-On is an optional feature which is not part of this Authentication/Authorization. Single-Sign-On can be used to share identities between different enterprise applications or other systems on the server. The Eclipse Higgins project addresses Single-Sign-On and could be adapted within Riena.



<h3>Reporting</h3>

<p>

Reporting in an environment where components are located on client and server offers a whole new set of functionality. Similar to the business application, the reporting components are also distributed between client and server. The server might only do the selection, sorting and grouping, while the client might render the output in tables or graphics. It might also resort, filter or regroup data. It is also easy for clients to download data only as it is needed and save bandwidth. Shifting of functionality between client and server is easily possible since they share the same platform. In addition to the typical output rendering for HTML and PDF, Riena will provide an output rendering based on SWT/Swing.

<p>

Eclipse BIRT provides a very powerful report engine. Riena will adapt the BIRT report engine fulfill these aspects in the future. However, this will not be in focus for release 1.0.



<h3>Persistence</h3>



<p>

Persistence is a complex topic in every enterprise application. It becomes more complex in the context of distributed component architectures. A client is no exception to this rule. Furthermore in an enterprise application a large number of clients need to access persistent data using different connection settings in terms of speed and reliability. This ranges from LAN over DSL, ISDN to mobile connection. Additional effort is required to minimize the amount of data from persistent objects that is transferred between client and server.

<p>

To achieve this Riena has to address and use features like optimistic locking, object identity, object versioning and data compression for remote services.

<p>

However Riena has no intention of reinventing existing persistence functionality as it exists in EclipseLink, Hibernate or other frameworks. Riena will add the missing bits and it will combine the persistence component with the object transaction component to optimize the exchange of large business objects between client and server.



<h3>Object Transaction</h3>

<p>

Enterprise applications share some of their business objects on client and on server. This compounding of business objects can become quite large and are often exchanged over remote services between client and server. Object transaction is a mechanism describing how changes in business objects can be captured and transferred without always transferring the entire compounded objects.

<p>

A typical use case is a customer object (and its relations) that is loaded into the client application where only one or two properties are changed before the customer record is stored again. With object transaction only the changed properties are sent back to the server which then stores it in the database and can send further modification back to the client. 

<p>

A second very typical use case is the use within the UI, where a number of properties of a set of business objects are changed and have to be rolled back if the user presses cancel. The object transaction can roll back all changes in the object to the time before the object transacted was started.





<h3>External Application Integration</h3>

<p>

To allow users of enterprise applications individual processing of their data, it is important to integrate the functionality to export data to a spreadsheet or word processor. 
However, the integration is not restricted to exporting the data but also the control of those external applications, e.g. form letter processing. Other possible standard applications that can be 
integrated are PDF Viewers or Mail Applications. In some cases content that needs to be integrated is available as a web page. In that case a Web Browser needs to be included and controlled within the client.





<h3>Monitoring of client state</h3>

<p>

In enterprise applications system, tracking and collecting application state information becomes critical. Beyond the scope of collecting information about any kind of error conditions in the server environment, error conditions that occur on the client are more complicated to collect.

<p>

The Riena platform  will support the consolidation of log events from the many clients into a central location on the server, the ability to trigger alarms for error conditions and correlate events and errors on clients with log entries on the server.



<h2>Out of Scope</h2>

<p>

<ul>

	<li>Riena has no primary focus to supply tooling to build enterprise applications (this may change in the future).</li>

</ul>



<h2>Organization</h2>

<h3>Mentors</h3>
<ul>

<li>Wayne Beaton, Eclipse Foundation, Evangelist</li>
<li>Jochen Krause, Innoopract, RAP project lead</li>
</ul>

<h3>Initial committers</h3>

<p>

The initial committers will focus on evolving and hardening the Riena platform. Our agile development process will follow eclipse.org's standards for openness and transparency. Our goal is to provide a home for application developers building and running sophisticated enterprise application systems. As such we will actively encourage contributions to Riena. The initial committers and contributors are:

<p>

<ul>

	<li>Christian Campo (compeople AG) : project lead, architect, committer</li>

	<li>Stefan Liebig (compeople AG) : architect, committer</li>
	
	<li>Alexander Ziegler (compeople AG) : architect, committer</li>

	<li>Uwe Teutscher (compeople AG) : architect, committer</li>	

	<li>Sebastian Stanossek (compeople AG): committer</li>

	<li>Frank Schepp (compeople AG): committer</li>

	<li>Heiko Barth (compeople AG): committer</li>

	<li>Thorsten Schenkel (compeople AG): committer</li>

	<li>Heiko Seeberger (metafinanz): contributor</li>

</ul>



<h3>Code Contribution</h3>

<p>

<ul>

	<li>compeople AG offers an initial code contribution for some of the components</li>

</ul>



<h3>Interested parties</h3>

<p>



<ul>

	<li>Andrena Objects AG (Oliver Renn)</li>
	
	<li>BEA Systems, Inc (Scott Lewis)</li>

	<li>Deutsche Verm�gensberatung AG (Reiner Neusser)</li>
	
	<li>Gerhardt Informatics Kft. (Frank Gerhardt)</li>

	<li>Innoopract (Jochen Krause)</li>

	<li>metafinanz GmbH (Heiko Seeberger)</li>

	<li>MicroDoc GmbH (Hendrik H�fer)</li>
	
	<li>mobilkom austria AG (G�rge Albrecht)</li>

	<li>SOPERA GmbH (Ricco Deutscher)</li>
	
	<li>Swordfish Project (Oliver Wolf)</li>


</ul>



<h3>User community</h3>

<p>

Riena is a new technology that promises to seamlessly bring together the Eclipse client and server side. As such, supporting and soliciting feedback from a large user community of developers is critical to creating a scalable platform and sophisticated UIs. 


<h3>Tentative Plan</h3>

	<h4>2008-01 - Version 0.1</h4>
	<ul>
		<li>Remote services support</li>
		<li>Authentication /authorization support</li>

	</ul>
	<h4>2008-04 - Version 0.4</h4>
	<ul>
		<li>Common functionalities (logging, exception handling, system configuration)</li>
		<li>Object transactions</li>
		<li>Persistence support</li>
	</ul>

	<h4>2008-07 - Version 0.7</h4>
	<ul>
		<li>Staging support</li>
		<li>Monitoring mechanism for client-side log events</li>
	</ul>
	<h4>2008-10 - version 1.0 - Release 1.0</h4>
	<ul>

		<li>Business process oriented navigation/UI</li>
		<li>Sample application</li>
	</ul>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
