<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Eclipse Model-to-Model Transformation (M2M) Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Model-to-Model Transformation");
?>

<p>Proposal Updated: 30 October 2006</p>

<h2>Introduction</h2>

<p>
	The Model-to-Model Transformation (M2M) Project is a proposed open source project under the <a href="http://www.eclipse.org/modeling/">Eclipse Modeling Project</a>.
</p>

<p>
	This proposal is in the Project Pre-Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process</a> document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment and/or join the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling.m2m">http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling.m2m</a> newsgroup.
</p>

<p>
	This proposal focuses on model-to-model transformation, while model-to-text transformation (M2T) is covered by the M2T project proposal.
</p>

<h2>Background</h2>

<p>
	Model-to-model transformation is a key aspect of model-driven development (MDD).� There are many existing technologies for model-to-model transformation, including implementations found within the Eclipse Modeling Project, such as the ATL component of the GMT project.� 
</p>

<p>
	Model-to-model transformations are being standardized by the OMG in the MOF Query, View, Transformation standard. This standard reached the final adopted state and is being finalized. Because the support of modeling standards is an element of the Eclipse Modeling Project charter, the QVT standard is integral part of this project.
</p>

<h3>ATL</h3>

<p>
	ATL (Atlas Transformation Language) is currently a component of the <a href="http://www.eclipse.org/gmt/">GMT project</a>. ATL is a hybrid language (a mix of declarative and imperative constructions) designed to express model transformations. ATL is described by an abstract syntax (a MOF meta-model), and a textual concrete syntax. A transformation model in ATL is expressed as a set of transformation rules. The recommended style of programming is declarative. 
</p>

<p>
	An initial library of ATL transformations is available at: <a href="http://www.eclipse.org/gmt/atl/atlTransformations/">http://www.eclipse.org/gmt/atl/atlTransformations/</a>. 
Any user is welcome to contribute to this library as open source under the EPL License. The documentation on ATL is available from: <a href="http://www.eclipse.org/gmt/atl/doc/">http://www.eclipse.org/gmt/atl/doc/</a>. Additional information may also be found at: <a href="http://www.sciences.univ-nantes.fr/lina/atl/">http://www.sciences.univ-nantes.fr/lina/atl/</a>. ATL is a QVT-like language but is not, in the present state, 100% compatible with the 12 levels of compliance stated in the OMG document. A comparative description of ATL and QVT may be found at: <a href="http://www.sciences.univ-nantes.fr/lina/atl/bibliography/SAC06a">http://www.sciences.univ-nantes.fr/lina/atl/bibliography/SAC06a</a>.
</p>

<p>
Currently the main ATL mailing list is running under Yahoo groups: <a href="http://tech.groups.yahoo.com/group/atl_discussion/">http://tech.groups.yahoo.com/group/atl_discussion/</a>. However, as soon as the M2M project is active, this list will migrate to an Eclipse list.
</p>

<p>
ATL is based on a transformation virtual machine. Its architecture is very modular so that it can evolve by adding new functionalities or building in improved performance. The complete code of the virtual machine is available. The documentation of the control machine (description of bytecodes) will also be made available under the M2M project. Also available is the complete development environment.
</p>

<p>
	It is anticipated that several bridges may be established between ATL and various other transformation languages.
</p>

<h3>QVT</h3>

<p>
	QVT addresses the need for standardizing the way transformations are achieved between models whose languages are defined using MOF.�In addition, QVT offers a standard way of querying MOF models, and creating views onto these models. This is similar to the need for XSLT for XML, where an XSLT script defines the mapping between a pair of DTDs and dictates how XMLs (compliant with the DTDs) are transformed.
</p>

<p>
	Queries on MOF models are required both to filter and select elements from a model on an ad-hoc basis, as well as to select elements that are the sources for transformations. This is similar to the need for Xpath within XSLT.
</p>

<p>
	The QVT specification defines three languages, two declarative and one imperative:
	<ul>
		<li>
			Relations: A user-friendly Relations meta model and language which supports complex object pattern matching and object template creation. Traces between model elements involved in a transformation are created implicitly.
		</li>
		<li>
			Core: A Core meta model and language defined using minimal extensions to EMOF and OCL. All trace classes are explicitly defined as MOF models, and trace instance creation and deletion is defined in the same way as the creation and deletion of any other object.
		</li>
		<li>
			Operational: an imperative language.
		</li>
	</ul>
</p>

<h2>Description</h2>

<p>
	The M2M project will deliver a framework for model-to-model transformation languages.�The core part is the transformation inftrastructure. Transformations are executed by transformation engines that are plugged into the infrastructure. There are three transformation engines that are developed in the scope of this project. Each of the three represents a different category, which validates the functionality of the infrastructure from multiple contexts.
</p>

<p>
	The three are:
	<ol>
		<li>
			ATL
		</li>
		<li>
			Procedural QVT (Operational)
		</li>
		<li>
			Declarative QVT (Core and Relational)
		</li>
	</ol>
</p>

<p>
	An exemplary implementation will be for the QVT Core language, using EMF as implementation of Essential MOF and the OCL implementation from the OCL subproject.� The main deliverable for this part of the project will be an execution engine that supports transformations.� The engine will execute the Core language in either interpreted or compiled form.
</p>

<p>
	Following Core, the M2M project will provide an implementation of the QVT Relations language, based on the QVT Core execution engine, EMF and OCL.� For both languages full language support will be delivered.
</p>

<p>
	Concrete examples will be developed as part of the project.� The ATL project already has a published set of example transformations, which will be complemented by a set of QVT transformation definitions.
</p>

<p>
	Follow-on development phases will accommodate community feedback and the knowledge gained during the initial development phase.
</p>

<h2>Organization</h2>

<p>
	We propose that this project be undertaken as part of the Eclipse Modeling Project. It uses EMF and OCL and provides capabilities for Querying, Viewing and Transforming EMF models.� The project will initially be comprised of infrastructure, ATL, procedural QVT and declarative QVT components.
</p>

<h3>Initial contributions</h3>

<p>
	The ATL component will migrate to M2M from GMT, where�information on ATL can be found as can a list of transformation definitions for common domain models.
</p>

<p>
	Borland's Together Architect modeling product has an implementation of QVT which is being reviewed for contribution.� A current dependency on the Kent OCL library will need to be processed for third party inclusion, and will ultimately be replaced by the MDT OCL implementation.
</p>

<h3>Initial committers</h3>

<p>
	The following companies will contribute committers to get the project started:
	<ul>
		<li>
			INRIA - ATL component
			<ul>
				<li>
					Fr�d�ric Jouault (proposed project lead)
				</li>
				<li>
					Freddy Allilaire 
				</li>
			</ul>
		</li>
		<li>
			Borland (<a href="http://www.borland.com/">www.borland.com</a>)
			<ul>
				<li>
					Radek Dvorak (procedural QVT component lead)
				</li>
				<li>
					Aleksandr Igdalov 
				</li>
				<li>
					Sergey Boyko 
				<li>
			</ul>
		</li>
		<li>
			Compuware (<a href="http://www.compuware.com/">www.compuware.com</a>)
			<ul>
				<li>
					Peter Braker (Infrastructure and declarative QVT component lead)
				</li>
				<li>
					Wim Bast 
				</li>
				<li>
					Ronald Krijgsheld 
				</li>
			</ul>
		</li>
	</ul>
</p>

<h3>Interested parties</h3>

<p>
	The following companies/individuals have expressed interest in the project:
	<ul>
		<li>
			IBM <a href="http://www.ibm.com/">www.ibm.com</a>
		</li>
		<li>
			Unisys <a href="http://www.unisys.com/">www.unisys.com</a>
		</li>
		<li>
			France Telecom <a href="http://www.francetelecom.com/">www.francetelecom.com</a>
		</li>
		<li>
			Software Engineering, University of Twente, the Netherlands <a href="http://trese.cs.utwente.nl/">trese.cs.utwente.nl</a>
		</li>
		<li>
			Software Composition and Modeling Laboratory, University of Alabama at Birmingham <a href="http://www.cis.uab.edu/softcom">www.cis.uab.edu/softcom</a>
		</li>
		<li>
			Giovanni Tosto 
		</li>
	</ul>
</p>

<h3>Participation</h3>

<p>
	Critical to the success of this project is participation of developers in the modeling application development community. We intend to reach out to this community and enlist the support of those interested in making a success of the Model-to-Model Transformation project. We also anticipate that several industry verticals will have an interest in M2M, and will therefore contact these companies and associated standard organizations. Otherwise, we ask interested parties to contact the M2M newsgroup to express interest in contributing to the project.
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
