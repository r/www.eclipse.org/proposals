<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">&nbsp; 
	
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Test and Performance Tools Platform Top-Level");
?>	

  <h1>eclipse test & performance tools platform project</h1><br>

<p>This project proposal is in the <a href="/projects/dev_process/"> 
  Proposal Phase</a> and is posted here to solicit additional project participation 
  and ways the project can be leveraged from the Eclipse membership-at-large. 
  You are invited to comment on and/or <a href="contributing.html">join the project</a>. 
  Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.test-and-performance">eclipse.test-and-performance 
  newsgroup</a> or the <a href="https://dev.eclipse.org/mailman/listinfo/test-and-performance-proposal"> 
  test-and-performance-proposal</a> mailing list.</p>

   
    <h2>Project Organization</h2>
  
  
   
     
      <p>The <i>Eclipse Test &amp; Performance Tools Platform Project</i> is an 
        open source Top Level Project of eclipse.org.The <a href="charter.html">Project 
        Charter</a> describes the organization of the project, roles and responsibilities 
        of the participants, and top-level development process for the project. 
        The Top Level Project is overseen by a <a href="pmc.html">Project Management 
        Committee</a> (PMC). The PMC divides the Top Level Project into <a href="project_descriptions.html">Projects</a> 
        coordinating identified resources of the Projects against a <a href="dev_plan.html">Development 
        Plan</a> and working against a <a href="http://dev.eclipse.org/viewcvs/index.cgi/" target="_blank">CVS 
        repository</a>. Frequently asked questions and corresponding answers may 
        be found in the <a href="faq.html">Project FAQs</a> page. For more information 
        on participation, see <a href="contributing.html">Contributing to the 
        Project</a>.</p>
      
  


   
    <h2>Project Principles</h2>
  
   
    Among the key principles on which this project has been initiated, and 
      will be run, are the following: <br>
      <br>
      <b>Extension of the Eclipse Value Proposition</b><br>
      The Eclipse Project has set a high standard for technical excellence, functional 
      innovation and overall extensibility within the universal tool platform 
      domain. We intend to apply these same standards to the test &amp; performance 
      tools platform domain. <br>
      <br>
      <b>Vendor Ecosystem</b><br>
      A major goal of this project is to support a vital application development 
      tools market. Its exemplary functionality will be useful on its own but 
      it will be designed from the start to be extensible so commercial vendors 
      can use what this project delivers as a foundation for their own product 
      innovation and development efficiency. <br>
      <br>
      <b>Vendor Neutrality</b><br>
      Vendor neutrality will be at the core of this project. We aim to encourage 
      Eclipse participation and drive Eclipse market acceptance by strengthening 
      the long-term product value propositions of the widest possible range of 
      application development vendors. <br>
      <br>
      <b>Standards-Based Innovation</b><br>
      This project will deliver an extensible, standards-based tooling foundation 
      on which the widest possible range of vendors can create value-added development 
      products for their customers and end-users. Where standards exist, we will 
      adhere to them. At least, at first, where standards are emerging, we will 
      wait for them to emerge; this can be re-evaluated later according to user 
      needs and contributor availability. Where multiple technologies are widely 
      used for a given functional need, we will attempt to support each, subject 
      only to technical feasibility and our goal of providing the most capable 
      and extensible foundation long term.<br>
      <br>
      <b>Agile Development</b><br>
      Our aim is to incorporate into our planning process the innovations that 
      arise once a project is underway, and the feedback from our user community 
      on our achievements to date. An agile development and planning process, 
      in which progress is incremental, near-term deliverables are focused, and 
      long-term planning is flexible, will be the best way to achieve this. <br>
      <br>
      <b>Inclusiveness &amp; Diversity</b><br>
      We aim to assimilate the best ideas from the largest number of participants 
      representing the needs of the widest range of end-users. So we will encourage 
      organizations across a broad range of technical, market and geographical 
      domains to participate in this project. 
  

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
