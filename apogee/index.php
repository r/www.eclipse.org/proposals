<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>

    <div id="maincontent">
	<div id="midcolumn">

<h1>The ECM Rich Client Platform (Apogee) Project</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("ECM Rich Client Platform (Apogee)");
?>

<b>An Eclipse Technology Project Proposal<br />
March 2006</b>



<h2>Introduction</h2>
<p>The Apogee Project is a proposed open source project under the <a href="http://www.eclipse.org/technology/">Eclipse Technology Project</a>.</p>

<p>This proposal is in the Project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process</a> document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.apogee">eclipse.technology.apogee newsgroup</a>.</p>

  <h2>Background</h2>

  <p>This project comes from our thought about next steps in the <a href=
  "http://en.wikipedia.org/wiki/Enterprise_content_management">Enterprise Content Management</a> (ECM) market. More and more customers need specific application related to ECM to properly handle their data and integrate in a seamless way all their digital assets and involved processes. These applications
  share a lot of features and need many common services. ECM applications developers definitely need
  a framework that would ease the creation of this kind of desktop
  applications, when the browser is not enough...</p>

  <p><a href=
  "http://wiki.eclipse.org/index.php/Rich_Client_Platform">Eclipse
  RCP</a> is expected to play a major role for next generation
  desktop applications. In the ECM business, web interfaces are great, but
  some customers / applications need more (highly responsive interface,
  rich UI, desktop integration, offline mode support, etc.).</p>

  <p>At <a href="http://www.nuxeo.com/en">Nuxeo</a>, we already have delivered some projects in this area and have begun to
  create such a framework for ECM applications, called Apogee. It's time to take it to the next level.</p>

  <p>This project aims at building a framework to create ECM-oriented
  desktop applications, independent from vendor or technologies. This
  framework could be used to create applications that will be integrated
  with <a href="http://www.documentum.com/">Documentum</a> , <a href=
  "http://interwoven.com/">Interwoven</a> , <a href=
  "http://www.cps-project.org/">Nuxeo CPS</a> or any ECM
  platform.</p>

  <h2>Scope</h2>

  <p>This project's goals are to :</p>

  <ul>
    <li>Define generic concepts and models required to create ECM desktop
    applications (documents, content types, storage, communication,
    standards, workflow, search, synchronisation, online/offline,
    etc.).</li>

    <li>Build the core infrastructure needed by desktop ECM applications,
    implementing the concepts and models defined above.</li>

    <li>Build a set of highly-customizable application-level components that
    would ease the creation of ECM desktop applications (mail client, IM,
    forms, calendar client, document editor, etc.).</li>
  </ul>

  <h2>Description</h2>

  <p>This project is divided into 2 area:&nbsp;</p>

  <ul>
    <li><strong>Core Platform</strong>: offers the low level document
    management infrastructure</li>

    <li><strong>Application Platform</strong>: a set of highly extensible
    applicative components</li>
  </ul>

  <h3>Core Platform</h3>

  <ul>
    <li>A <strong>generic resource model / content repository</strong> that can be integrated with Eclipse's resource model. <a href="http://www.jcp.org/en/jsr/detail?id=170">JSR-170</a> (<a href="http://www.jcp.org/en/jsr/detail?id=170">Java Content Repository</a>) is considered as the native local resource model for Apog�e (JSR-170 begins to be widely adopted in the Java community). Adapters between JSR-170 and Eclipse resource model will have to be written. The open source implementation of the JCR at <a href="http://www.apache.org/">Apache</a> is <a href="http://jackrabbit.apache.org/">JackRabbit</a> and seems to be a good candidate for Apog�e. For JackRabbit's backend and non content-related data storage, we plan to study <a href="http://www.db4objects.org/">db4o</a>  (it could also be used as PersistenceManager for JackRabbit). Vendors should be allowed to write their own Apog�e connector, allowing them to connect Apog�e to ECM platforms using proprietary interfaces.<br/>
    As the JCR does not define a data model, Apog�e will still need to provide a convenient way to handle content / data model (this is more detailed in the content repository and storage specification of the apog�e project).
    </li>

    <li>A <strong>form services</strong> (based on <a href="http://www.w3.org/MarkUp/Forms/">XForms</a>) to define content
    creation/edition forms, user interface forms (like workflow action forms), etc. that would be directly delivered by the ECM platform using XForms forms, according to the content model used.
    <br/>This service can generate Eclipse Forms / SWT from the XForms description using the Eclipse XForms engine.</li>

    <li>A <strong>generic model to define actions / views</strong> available on a document,
    integrated with the ECM platform (edit, check-in, check-out, properties, etc.) actions model. Actions have to be integrated with Eclipse's menuitems / views model.</li>

    <li>A pluggable <strong>unified security model</strong> based on
    roles/permissions that can be easily integrated with the ECM platform
    (on the server). We are currently investigating <a href="http://www.eclipse.org/higgins/">ETF/Higgins</a> and <a href="http://acegisecurity.org/">Acegi</a> for this part.</li>

    <li>An <strong>Indexing and search service</strong> will be provided by JackRabbit built-in index and search features. Apog�e may allow developers to plug other search engines, that could be needed for specialized needs.</li>

    <li>A relation service to handle relation between resources
        (<a href="http://www.w3.org/RDF/">RDF</a> based). This component may be based on <a href="http://www.openrdf.org/">Sesame</a>. This service should be independant from the content repository.</li>

    <li>A communication service that can be used by
    components to communicate with the ECM platform and other component of the information system. This component will use the <a href="http:// www.eclipse.org/ecf">Eclipse Communication Framework</a> (ECF) (and protocols like <a href="http://www.xmpp.org/XMPP">XMPP</a> / <a href=
    "http://www.w3.org/TR/soap/">SOAP</a> / XML-RPC or <a href="http://java.sun.com/products/jms/">JMS</a>).</li>

    <li>A subscription-based <strong>user notification</strong> mechanism (to allow the user to be
    notified by the ECM platform through various channels like UI glitches (ex: windows tray), email, instant messaging (or SMS) when events occur. This service may be based on ECF.</li>

   <li>A service to handle <strong>online/offline mode</strong> and enabling/disabling
    actions/capabilities according to the connexion availability. This has to provide local and remote content repository synchronisation. SyncML (through Sync4J/Funambol) may be used here.</li>

    <li>A <strong>digital signature service</strong> to sign documents and workflow
    actions.</li>

  </ul>

  <h3>Application Platform</h3>

  <ul>
    <li><strong>Mail client component</strong> that can be used to create a specific,
    business-oriented, mail client (for example : custom mail-based
    application like workflow-based collaborative mail management or CRM application).</li>

    <li><strong>Calendar component</strong> (customizable to create calendar-style
    application like planning management).</li>

    <li><strong>Instant Messaging component</strong> (presence and synchronous communication between
    users, user notification channel). This would be based on ECF.</li>

    <li><strong>Rich document editor</strong> (based on XHTML) that can be used to create
    custom content editors (for example a custom <a href=
    "http://www.newsml.org/pages/index.php">NewsML</a> document
    editor or a DocBook one).</li>

    <li><strong>Collaborative workspaces</strong> (that would offer access to collaborative
    workspaces on the ECM platform).</li>

    <li><strong>Synchronous document</strong> editing (plugged into the document
    editor).</li>

    <li><strong>Annotation service</strong> (that may be based on <a href=
    "http://www.w3.org/2001/Annotea/">Annotea</a>).</li>

    <li><strong>Tagging and categorization</strong> service.</li>

    <li><strong>Reporting service</strong> (BIRT based) to allow the creation of activity reports.</li>
  </ul>

  <h2>Existing standards and projects leveraged</h2>

  <p>This project will be based on a number of high-quality open source framework:</p>

  <ul>
    <li><a href="http://www.eclipse.org/rcp">Eclipse RCP</a> (infrastructure, UI,
    Perspectives, OSGi core, etc.)</li>

    <li><a href="http://www.eclipse.org/ecf">Eclipse ECF</a> to handle
    client/server communication, IM and communication needs</li>

    <li><a href=
    "http://www.eclipse.org/webtools/wst/main.html">Eclipse
    WTP/WST</a> to handle web services (XSD and SOAP/WSDL support)</li>

    <li><a href="http://www.eclipse.org/gef/">Eclipse GEF</a> to
    handle graphic-based user interfaces (relation visualization and
    management for example)</li>

    <li><a href="http://jackrabbit.apache.org">Apache JackRabbit</a> and <a href="http://www.db4o.com">db4o</a> for storage</li>

    <li><a href="http://eclipse.org/birt/">Eclipse BIRT</a> for the
    reporting service</li>

    <li><a href="http://www.sync4j.org/">ObjectWeb Sync4J</a> for
    the synchronization service</li>

    <li><a href="http://lucene.apache.org/java/docs/index.html">Apache
    Lucene</a> for the seach service</li>

    <li><a href="http://www.openrdf.org/">OpenRDF Sesame</a> for the
    relation service / RDF database</li>

  </ul>

  <p>This project will be heavily based on open standard :</p>

  <ul>
    <li><a href="http://www.w3.org/XML/SchemaXML">W3C XMLSchema</a> for
    content/data schemas</li>

    <li><a href="http://www.w3.org/MarkUp/Forms/">W3C XForms</a> for
    forms provided by the ECM platform (edit view, workflow actions forms, search form, etc.)</li>
    
    <li><a href="http://www.w3.org/RDF/">RDF</a> as relation format</li>

    <li><a href="http://www.xmpp.org/">XMPP</a> as communication
    protocol / XML bus / event channel</li>

    <li><a href="http://www.w3.org/TR/soap/">SOAP</a></li>

  </ul>

  <h2>Organization</h2>

  <p>This project will be organized into 2 top-level components :</p>

  <ul>
    <li>Core Platform (CP) : build all core services like data storage,
    relation, form, content type, indexing / search, communication,
    synchronization, etc.</li>

    <li>Applicative Platform (AP) : mail client, calendaring, rich text
    editor, workflow editor, collaborative workspaces, digital signature,
    IM, etc.</li>
  </ul>

    <h3>Proposed initial committers and project lead</h3>
    
    <p>The initial committers will focus on developing and creating core services. The agile development process adopted will follow eclipse.org's standards for openness and transparency.  Our goal is to provide the infrastructure and APIs needed to build a strong developer community that would empower the plateform to make it quickly grow. The initial committers are:
    
<ul>
  <li>Eric Barroca (&#101;&#098;&#064;&#110;&#117;&#120;&#101;&#111;&#046;&#099;&#111;&#109;) : project manager</li>
  
 <li>Bogdan Stefanescu (&#098;&#115;&#064;&#110;&#117;&#120;&#101;&#111;&#046;&#099;&#111;&#109;): technical leader</li>
 
 <li>Eugen Ionica (Nuxeo): relation and search services</li>
 
 <li>C&eacute;dric Bosdonnat (Nuxeo): content editors and synchronization</li>
 
 <li>Carl Rosenberger (db4objects): storage service</li>
</ul>


  <p>Interested potential commiters are kindy invited to express it on the newsgroup or via email.</p>
  
  
  <h3>Interested parties</h3>
 <p>Interest have been notified by follwoing organization so far:</p>
 <ul>
 <li><a href="http://www.afp.com/">AFP</a> (consumer)</li>
 <li><a href="http://www.chalmers.se/">Chalmers University of Technology</a> (consumer / commiter)</li>
 <li><a href="http://www.cncc.fr">French Institute of Statutory Auditors</a> (consumer)</li>
 <li><a href="http://www.db4objects.com">db4objects</a> (commiter)</li>
 <li><a href="http://tmtec.biz/index.html">TMT</a> (consumer / commiter) </li>
 </ul>
 <p>To express support, concern, or constructive opinion regarding the formation of this proposed technology project, all are encouraged to utilize the newsgroup.</p> 


    <h3>Initial contribution</h3>
    <p>The current source code and documentation repository for this project currently live here : <a href="http://svn.nuxeo.org/trac/pub/browser/Apogee/">http://svn.nuxeo.org/trac/pub/browser/Apogee/</a> (direct SVN access : <a href="http://svn.nuxeo.org/pub/Apogee">http://svn.nuxeo.org/pub/Apogee</a>). A website is also available for documentation publishing: <a href="http://apogee.nuxeo.org">apogee.nuxe.org</a></p>


  <h2>Roadmap</h2>

  <p>Development model is time based. Major release cycle is yearly.</p>

  <h3>1.0</h3>

  <ul>
    <li><strong>M1 - April 2006</strong>

      <ul>
        <li>[CP] Technical specifications of the core platform (60%
        done)</li>

        <li>[CP] Resource model (done &mdash; currently being refactored on top of JackRabbit)</li>

        <li>[CP] Relation service / RDF (done)</li>

        <li>[CP] Indexing and Search service / Lucene (done)</li>

        <li>[CP] Rich Content editors (MSWord, OpenOffice, XHTML) (done)</li>
        
        <li>[CP] Eclipse Forms generation service (XForms based, alpha support, done)</li>
        
        <li>[CP] On-the-fly validation of a SWT form with a XML Schema (XSD) (done)</li> 
      </ul>
    </li>

    <li><strong>M2 - June 2006</strong>

      <ul>
        <li>[CP] Communication service (XML-RPC based)</li>

        <li>[CP] Integration of server-defined actions available on
        resources (documents)</li>

        <li>[CP] Content type service (use content types
        defined by the server with XSD)</li>

        <li>[AP] Collaborative workspaces</li>
        
        <li>[CP] Rich Text Editor (done)</li>
        
      </ul>
    </li>

    <li><strong>M3 - August 2006</strong>

      <ul>
        <li>[CP] Security model and service</li>

        <li>[CP] Form service (XForm full support)</li>

        <li>[CP] Client / Server synchronization -&gt; online/offline
        mode</li>
      </ul>
    </li>

    <li><strong>M4 - September 2006</strong>

      <ul>
        <li>[CP] Communication framework (SOAP support)</li>

        <li>[AP] Instant Messaging (XMPP based)</li>

        <li>[AP] Mail client (alpha)</li>

        <li>[AP] Calendar client (alpha)</li>
      </ul>
    </li>

    <li><strong>RC - October 2006</strong>

      <ul>
        <li>Feature freeze</li>
      </ul>
    </li>

    <li><strong>Final - December 2006</strong></li>
  </ul>

  <h2>2.0 - September 2007</h2>

  <ul>
    <li>[AP] Digital signature</li>

    <li>[AP] Reporting service</li>

    <li>[AP] Mail component (complete)</li>

    <li>[AP] Calendar component (complete)</li>

    <li>[AP] Relation management (relation graphs, visual relation
    management)</li>

    <li>[AP] Synchronous collaborative rich document edition</li>

    <li>[CP] Annotation service</li>
  </ul>

      </div>
  </div>
  
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
