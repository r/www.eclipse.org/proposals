<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Eclipse PHP Integrated Development Environment";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">


<h1><?= $pageTitle ?></h1><br>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse PHP Integrated Development Environment");
?>
</p>

<h1>Introduction</h1>

<p>The PHP IDE Project is a proposed open-source project under the Eclipse Tools Project.  
This document describes the content and the scope of the proposed project.</p>

<p>This proposal is in the <a href="/projects/dev_process/proposal-phase.php">Project Proposal Phase</a> 
and is written to declare its intent and scope. 
This proposal is written to solicit additional participation and input from the 
Eclipse community. You are invited to comment on and/or join the project. 
Please send all feedback to the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.php">http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.php</a> newsgroup.<p>

<h1>Background</h1>

<p>PHP started as an open source project about 10 years ago. During the last 10 years, 
PHP has gained significant momentum with now more than 23 million Web domains 
running PHP.  PHP�s simplicity and low cost of ownership have all contributed to 
the fast and wide adoption of the language. PHP has now reached the point 
of maturity where organizations are seeking standard tools and solutions that 
will further facilitate application development with PHP.</p>

<h1>Project Goal</h1>

<p>The PHP IDE project will deliver a PHP Integrated Development Environment framework for the Eclipse platform. 
This project will encompass the development components necessary to develop PHP-based Web Applications 
and will facilitate extensibility. It will leverage the existing Web Tools Project 
in providing developers with PHP capabilities. 
</p>

<h1>Project Scope</h1>
<p>The project will contribute an initial set of Eclipse plug-ins that will add PHP IDE capabilities to Eclipse, and will provide APIs to further extend the functionality of the project. The project will be composed of a set of extensible features covering the development life cycle of PHP including functionality that deals with developing, deploying, documenting, debugging and testing PHP-based applications.
</p><p>
The next section describes in detail the design, features and extension points needed in order to provide a comprehensive IDE.
</p>

<h1>Project Description</h1>
<h2>Functional Requirements</h2>
The PHP IDE Project will include the following components:
<h3>PHP IDE Core</h3>
The PHP IDE Core provides the infrastructure for the PHP IDE and includes:
<ul>
<li>PHP Language Model - provides infrastructure for the PHP language and the most popular extensions. 
This Core component is the basis of all PHP language classes, functions and constants and it will support both PHP 4 and PHP 5.
The two PHP major versions support will affect the code assist, syntax coloring and PHP Functions view.
</li>
<li>PHP Inspector / Code Builder - analyzes PHP language elements such as: classes, functions, constants, variables, include statements, PHPDoc blocks and errors.
The PHP Inspector component is based on the PHP language Lexer and Parser and provides analysis for all PHP source code in either PHP 4 or PHP 5.
</li>
<li>PHPDoc Support - provides infrastructure that helps overcome some of the PHP language complexities. It adds capabilities to the PHP Language model and the PHP Inspector. 
</li>
<li>PHP User Model - provides an API for navigating through PHP inspected data (PHP classes, functions, constants, variables and include statements). Used by the search engine, Code Assist and PHP Explorer View.
</li>
<li>PHP Source Code Formatter - Format PHP code according to personal preferences. The tool works in tandem with the Web Tools Project formatter.
</li>
</ul>

<h3>PHP IDE UI</h3>
The PHP IDE UI provides the user interface for the PHP IDE. It includes several contributions to the Eclipse workbench:
<ul>
<li>PHP Editor � The PHP Editor is built over the Web Tools Structured Editor and should inherit all its capabilities and actions. The PHP as a Structured Editor allows working on more then one programming language at the same time. All features in the PHP Editor support both PHP 4 and PHP 5. 
</li>
<li>PHP Outline View � Supports two types of Outline Views:<ul>
<li>PHP specific - shows the structure of an inspected PHP file (classes, functions, variables, include files and constants).</li>
<li>Generic web view - shows an HTML structural View � inherited from the Web Tools Project.</li>
</ul></li>
<li>Project Outline View - Very similar to the Outline View except that it shows the structure of an entire PHP Project.
</li>
<li>PHP Explorer View - PHP element hierarchy of the PHP projects in the workbench. It provides a PHP specific view of the resources shown in the Navigator.
</li>
<li>PHP Language View - A graphical representation of the PHP language Model.
</li>
<li>PHP Manual View - An offline view of the official PHP Manual (http://www.php.net/download-docs.php).
</li></ul>

The UI will also provide the following functionalities:
<ul>
<li>PHP Searching � Integrated in the Eclipse workbench�s search dialog, finds declarations of PHP classes, functions, constants and variables.</li>
<li>Open PHP Type Dialog - Similar to JDT�s open Type dialog, the PHP open type dialog will show all PHP types in the workspace in order to open the selected type in the Editor.</li>
</ul>

<h3>PHP IDE Debug</h3>
The PHP IDE debug provides the infrastructure for PHP debugging, implemented on top of the debug model provided by the Eclipse Platform Debugger and on top of the Web Tools Projects.
The PHP IDE will support both types of PHP Servers: PHP local web server and PHP executable.
The debug protocol is based on Zend Studio�s debug protocol with extension points for new protocol messages. 

<h4>Debug Perspective</h4>
The Debug Perspective will be based on the Eclipse Debug Perspective with compliance to the relevant PHP Debug actions. 
The perspective will contain the standard debug views.

<h4>Debug Preview </h4>
The Preview view will display what will be rendered on the browser as a result of processing the PHP file that is being edited. This will be part of a Multi-Page Editor, which will contain a Source page and a Preview page aggregated in a Tabbed Container.

<h2>Extension Points</h2>
The following list is work in progress and will most likely expand as requirements are identified.
The PHP IDE project will provide extension points to facilitate the extensibility of the project.  
The extensions points will provide the ability to 
<ul> 
<li>Extend the debug communication protocol, between the PHP IDE and the Web Server, with new messages. This will enable to increase the debugging capabilities with new features and functions.
</li><li>Integrate additional PHP builders.
</li><li>Extend the PHP data models by providing new data model that will reflect
code assist and outline view.
</li><li>Launch of a PHP project on any Web server.  
</li></ul>
In addition, the PHP IDE project will define several extension points to enhance the PHP Editor. The extensions points will provide the ability to 
<ul>
<li>Add new folding structured (regions) for the PHP Editor (similar to org.eclipse.jdt.ui.foldingStructureProviders)
</li><li>Enhance the PHP IDE with new text hover to the PHP Editor
</li></ul>
<img src="picture1.gif"/><br>

<p>The PHP IDE project will be built using several Eclipse Web Tools Platform (WTP) project components. The WTP provide generic, extensible and standards-based tools for Web-centric application development. It provides editors for various Web centric languages such as: HTML, CSS, JavaScript, WSDL, etc. In addition, WTP provides facilities for configuring and connecting to Web Servers. The PHP IDE will use the WTP Server facilities to manage and configure Web Servers.  PHP IDE project will benefit from the client web development support, already available in the WTP (HTML, CSS, etc) and will add the server side web development support (PHP). The integration between the PHP IDE and WTP will provide the PHP IDE�s users with a comprehensive solution for WEB development.
</p>
<p>Main plug-ins:
<ul>
<li>PHP Editor, will extend the WTP�s Structure Editor. The Editor will add PHP support to the Structured Editor using PHP Lexical Analyzer for the PHP language. The use of the WTP�s Structure Editor as a baseline for the extended functionality will provide the PHP developer with the benefits and capabilities that are already available with the WTP editor.
</li>
<li>Debugger - The debugger communication protocol between the IDE and the server side in the PHP IDE will be based on Zend�s proven debugging protocol. This protocol defines the agreed-upon format for transmitting data between both the server and the client side in order to facilitate debugging of a PHP application. The debug protocol will support PHP web server debugging and PHP stand-alone application debugging. Debug initiation will be done through an HTTP request made to the debugger server side, then the debugger server will open a communication channel to the PHP IDE to start debugging communication.
</li>
<li>Inspection � The PHP IDE will make use of an existing Zend inspection model and tool for parsing PHP code. 
The PHP parser establishes a model constructed by file data for each of the project files. Each file�s data includes: classes, functions, constants, errors and include file data. The model is used for Code Assist, Outline View, Project Outline View and PHP Explorer View. The model is updated while you type in the editor and for every change in the PHP Project�s structure.
</li>
</ul>

<h1>Code contributions</h1>

Zend and IBM will be making an initial code contribution that will encompass the core functionality for the PHP project including:
<ul>
<li>High-quality and proven PHP model infrastructure incl. editor, syntax highlighting, code completion, and PHP explorer
</li><li>PHP perspectives
</li><li>PHP debugging capabilities
</li>
</ul>

<h1>Development Plan</h1>

We plan to have an initial prototype version available by the end of Q1/2006.  After the availability of the initial version we will work on incorporating changes and feedback as needed and strive to release the first functional version as early as possible. 

<h1>Organization</h1>
<h2>Project Leads</h2>

The project leads will be initially the following people:
<ul>
<li>Guy Harpaz, 	 Zend 	(guy@zend.com) </li>
<li>Robert Goodman,IBM 	(goodmanr@us.ibm.com)</li>
</ul>

<h2>Interested Parties</h2>

Support has been expressed by:
<ul>
<li>Actuate</li>
<li>IBM</li>
<li>Intel</li>
<li>SAP</li>
<li>Zend</li>
</ul>

<h2>Potential Committers  	</h2>

<ul>
<li>Guy Harpaz, 	 Zend 	(guy@zend.com) </li>
<li>Yossi Leon, 	 Zend 	(yossi@zend.com)</li>
<li>Shachar Ben-Zeev 	 Zend 	(shachar@zend.com)</li>
<li>Shalom Gibly,	 Zend	(shalom@zend.com)</li>
<li>Guy Gurfinkel,	 Zend 	(guy.g@zend.com)</li>
<li>Alik Elzin,	 Zend 	(alik@zend.com)</li>
<li>Michael Spector, Zend 	(michael@zend.com)</li>
<li>Robert Goodman,IBM 	(goodmanr@us.ibm.com)</li>
<li>Brian Burns,	 IBM 	(bburns@us.ibm.com)</li>
<li>Phil Berkland, 	 IBM  	(berkland@us.ibm.com)</li>
</ul>

<p>&nbsp;</p>


</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
