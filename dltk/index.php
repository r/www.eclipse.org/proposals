<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "PHP, Python, Ruby, Tcl, JavaScript, ECMAScript, dynamic languages, scripting";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Dynamic Languages Toolkit</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Dynamic Languages Toolkit");
?>

<h2>Introduction</h2>
<p>The Dynamic Languages Toolkit is a proposed open source 
project under the <a href="/technology/">Eclipse 
Technology Project</a>.</p>
<p>This proposal is in the Project Proposal Phase (as defined in the
<a href="/projects/dev_process/">
Eclipse Development Process document</a>) and is written to declare its intent 
and scope. This proposal is written to solicit additional participation and 
input from the Eclipse community. You are invited to comment on and/or join the 
project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology</a> newsgroup.&nbsp;

<h2>Background</h2>
Eclipse provides a flexible and extensible framework for third parties to implement solutions for authoring, editing, and building files in a particular language. Dynamic langauges is a valuable subset of programming languages but lack of support from Eclipse side. Support of dynamic languages in Eclipse Project(s) would extend
Eclipse influence on non-Java world as well as dynamic languages would benefit from Eclipse Tools and Technologies.
<h2>Description</h2>
<p align="left">
Dynamic Languages Toolkit project goal will be to develop framework for tool vendors, researchers, and end-users  who rely on dynamic languages (like TCL, Python, and Ruby).
The toolkit will support of editing, code navigation, debugging, and refactoring of code in dynamic languages and
must be an extensible such that new languages such as PHP and Perl could be plugged in easily. 
</p>
<p>Also project will explore potential reuse amoung other Eclipse language projects and result in recomendations/code to push down to LTK (in addition to ltk.refactoring).
</p>
<h3>Proposed Components</h3>
<ul>
<li><b>Generic Language Model.</b> Provides common reusable base generalized from popular dynamic language
models, defines API and contracts for Common Infrastructure components and extensions. 
Coupled with partial or full, where appropriate, implementations of source/model related services like code indexer, search engine, type evaluators Generic Language Model will help to plug-in new lanuguages easily (instead of "copy-and-paste") to reach primary goal.
<p>Another goal is to make dynamic language models interoperable. While this is not the primary goal, it has an advantages for multi-language evironments like dynamic language VMs designed for wide range of languages (see Perl Foundation's <a href="http://www.parrotcode.org">Parrot VM</a>) and cross-language tools.</p>
</li>
<li><b>Type Inference Framework.</b> Type inference is an important task for dynamic language tools. DLTK goal is to explore Type Inference algorithms (like
<a href="http://lexspoon.org/ti/">DDP</a>), and explore possibilities to build a framework for tool implementors.
<li><b>Common UI Components.</b> Generic Language Model will provide services on top of which Common UI Components
will be build. These UI components includes but not limited to: structural code explorer and browser, 
type hierarchy and call graph viewers, code outline viewer, code navigation and code search UI parts.
</li>
<li><b>Debugging Infrastructure.</b> Creating infrastructure for support debugging of scripting languages including
common debugger protocol, implementation and extensions to Eclipse Platform Debug UI. Xdebug project's <a href="http://www.xdebug.org/docs-dbgp.php">DBGp</a> protocol is designed for scripting languages and proposed as a common debugger protocol to use in the project. 
</li>
<li><b>Exemplary Implementations.</b> Plugged in exemplary implementations of scripting languages (Ruby, Python, TCL) will prove the Dynamic Languages Toolkit design and architecture and will be
  examples for technology adopters.
</li>
<li><b>JDT Integration.</b> Scripting languages widely used for Java scripting with pure Java scripting language interpreters like
JRuby (Ruby), Jython (Python), Jacl (Tcl), Rhino (ECMAScript), etc. DLTK JDT integration component targeted to provide an integration layer
between DLTK based tools and JDT. Potentially this will extend JDT with refactoring partcipants to include script code in refactoring operations as
well as allow script programmer to use Java code completion on Java objects inside script, etc.
</ul>

<h3>Relationship with other Eclipse Projects</h3>

<p>Dylamic Languages Toolkit project will be build on top of Eclipse Platform. Eclipse projects which 
rely on scripting languages might be interested, including WTP (JavaScript), WTP-based projects 
(scripting languages for web development, like PHP IDE project), Laszlo Eclipse Frameworks which rely on JavaScript and other scripting languages, etc.</p>

<p>DLTK JDT Integration will also rely on Eclipse JDT project codebase.</p>

<h2>Organization</h2>
		<p align="left">We propose this project should be undertaken as a Technology project rather than as part of the Eclipse Platform. Being a Technology project gives it room to experiment without disruption to other Eclipse Platform development work.</p>

		<p align="left">Finally, since Technology sub-projects are not meant to be ongoing, we foresee three possible evolutionary paths for the subproject:<br>
        <br>
   1. The project is merged back into the mainline Eclipse Platform top-level project.<br>
   2. The project is moved to a permanent state as a Tools Platform project.<br>
   3. The technology proves to be insufficient for Eclipse and the work is set aside.<br>
        <br>
        These paths are not mutually exclusive. Some combination of some or all of these paths may be the result of the work done in this project.&nbsp;</p>


		<h3 align="left">Proposed initial committers</h3>
		<ul>
			<li>Andrei Sobolev - xored software, Inc.</li>
			<li>Andrey Platov - xored software, Inc.</li>
			<li>Mikhail Kalugin - xored software, Inc.</li>
			<li>Dmitriy Kovalev - xored software, Inc.</li>
			<li>Andrey Tarantsov - xored software, Inc.</li>
			<li>Yuri Baburov - xored software, Inc.</li>
		</ul>
		<h3 align="left">Interested parties</h3>
<p align="left">The following companies
		 have expressed interest in this project. Key contacts listed.<ul>
			<li>Cisco Systems, Inc. - Ed Warnicke (eaw@cisco.com)</li>
		</ul>

<h3 align="left">Code Contributions</h3>

<p align="left">xored software, Inc. is offering TruStudio Foundation PHP & Python IDE, as an initial codebase 
(see <a href="http://www.xored.com/trustudio">http://www.xored.com/trustudio</a>). TruStudio IDE built with
proposed concepts in mind and could be used as a potential codebase for some parts of Dynamic Languages Toolkit project. </p>

<p align="left">Cisco Systems, Inc. contributed TCL language support code for Dynamic Languages Toolkit project. </p>

<h2>Tentative Plan</h2>
<p align="left">Development is ongoing at <a href="http://www.eclipsedltk.org">www.eclipsedltk.org</a>, Plan for DLTK core and TCL exemplary tools 
available here: <a href="http://www.eclipsedltk.org/plan-1_0">DLTK TCL Project Plan</a> and scheduled for release on Jan, 23 2007. We plan to release DLTK 1.0 Final with exemplary tools for Ruby, Python, and TCL by the end of Q1/2007.
</p>

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
