<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>B3 Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse B3");
?>

<h2>Introduction</h2>
<p>B3 is a proposed open source project under the EMFT project. This proposal is in the Project
Proposal Phase (as defined in the Eclipse Development Process document) and is written to declare its
intent and scope and to solicit additional input from the Eclipse community. You are invited to comment
on and/or join the project. Please send all feedback to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.b3">eclipse.b3</a> newsgroup.</p>
<h2>Objectives</h2>
<p>The purpose of the project will be to develop a new generation of Eclipse technology to simplify software build and assembly.</p>
<p>The work of the project will be a complement and potential successor to the PDE Build component.</p>
<p>The project will have three key objectives:

<ul> 
        <li>Making build processes simpler, repeatable, reproducible and adaptable</li>
        <li>Bridging discontinuities between building and provisioning software</li>
        <li>Aligning potentially overlapping Eclipse build technologies</li>
</ul>

        <p>Build specifics are often buried within a complicated build script or process definition, so when
        builds fail, they often do so for mysterious reasons. Similarly, build execution can be highly
        dependent on the details of a given developer's development setup, which makes builds difficult
        to run in different environments. The ambition of this project will be (1) to make it possible
        for a given build to execute in the exact same way when run by anyone, anywhere in the
        develop-test-deploy cycle and (2) to ensure that when a build fails, it fails for reasons
        that are clear and reproducible. Developing a declarative, model-driven approach to build definition
        and execution will be essential to realizing this ambition. Clear separations will be created between
        concerns of build definition and build execution. Build definitions will describe build input,
        tooling, platform constraints and processes in ways that are amenable to transformation and manipulation.
        Where possible, build models will be automatically generated from project structure.</p>
        
        <p>Software developers are linked
        in an iterative chain of building and provisioning, in which the output of a given build becomes an input
        in a subsequent build. Today, the tools and processes of building and provisioning are poorly integrated,
        resulting in broken hand-offs and wasted effort across the cycle. Automating the connection between each
        link in the chain will be a goal of this project. Build reproducibility will be a key means to realizing
        this goal. Each element of a given build process will be captured and reflected in the build definition,
        so that it can be executed as needed in subsequent steps in the cycle.</p>
        
        <p>Several key Eclipse technologies, most
        notably PDE Build and Buckminster, extend into the build & assembly domain. PDE Build provides the
        standard Eclipse framework for generating and orchestrating scripts used to build plug-ins. Buckminster
        has been developed primarily to automate component assembly and provisioning for Eclipse workspaces, but
        also can be used more generally in build automation. Delineating the relationship between these two
        technologies will be an important goal of this project, although further exploration will be needed to
        determine the best approach going forward, i.e., folding one technology into the other, creating a
        common successor technology for both, etc.</p>

<h2>Technical Scope</h2>
<p>Key technologies to be developed in this project will be (1) models describing various aspects of the build
domain as well as the relationship between build and provisioning and (2) engines that interpret and act on these
models.</p>
<p>Leverage and support of established Eclipse technologies will be a priority.</p>
<p>Buckminster and PDE Build are directly relevant to the work of this project, and we intend to combine their
respective strengths and capabilities.</p>
<p>Creating a common successor technology, or folding one technology into the other, will be within scope of this
project.</p>
<p>P2 will be a key technology to exploit, given our goal of addressing connections between build and provisioning
concerns. P2 and PDE Build are highly interdependent. Clarifying the boundary between p2/provisioning and build
concerns will be within scope of this project. Extension into the provisioning domain, as addressed by p2, will
be out of scope. We also expect non-Eclipse build and continuous integration technologies to influence the project
architecture and implementation. However, developing similar technology, to the extent not currently addressed by
Buckminster or PDE Build will be out of scope.</p>
<p>We plan to use EMF and related technologies to create the necessary build models and interpretation engines.</p>
<p>Various community build/assembly tools have been implemented based on Buckminster, PDE Build and other relevant
technologies. Continuing and expanding support for these and other community initiatives will be a priority for
this project.</p>
<h2>Related Projects</h2>
<h3>Athena Common Builder</h3>
<p>Athena performs high-level orchestration using mostly ant-scripts to make Hudson drive a build that executes the
ant-tasks provided by PDE-build. It isn't really visible in the IDE (nor was it intended to) but what it does is
certainly related.</p>
<p>Athena has an implicit build-model that consists of artifacts from PDE, plus maps, xml-files, and properties.
All that information put together describes how to check things out from SCM's, assemble, build, test, and promote.
All concepts that will be described in the B3 model.</p>
<h3>Maven</h3>
<p>Several initiatives exists to integrate Maven with Eclipse. Maven has a build/provisioning model of its own
witch significant overlaps both PDE-build and P2. Rather than "converting" to the maven way of doing things, we
want B3 to have clear points of integration where maven functionality can be plugged in. One interesting point of
integration that is beyond the scope of this project but still closely related would be a P2 front-end to a Maven
repository.</p>
<h3>Other Ant based build systems</h3>
<p>Several projects within Eclipse are using their own ant-based build systems. We encourage the build masters in
charge of those systems to get involved so that any specifics needed by their builds can be encompassed by B3.
Several projects within Eclipse are using their own ant-based build systems. We encourage the build masters in
charge of those systems to get involved so that any specifics needed by their builds can be encompassed by B3.</p>
<h2>Project Team</h2>
<h3>Initial committers</h3>
<ul>
	<li>Ed Merks (Project Co-Lead)</li>
	<li>Thomas Hallgren (Project Co-Lead)</li>
	<li>Henrik Lindberg</li>
	<li>Karel Brezina</li>
	<li>Stefan Daume</li>
	<li>Bjorn Freeman-Benson</li>
	<li>Oisin Hurley</li>
	<li>Eike Stepper</li>
	<li>Chris Aniszczyk</li>
	<li>Andrew Niefer</li>
	<li>Ian Bull</li>
	<li>Filip Hrbek</li>
</ul>
<p>Each initial committer is currently an active Eclipse committer on other projects.</p>
<h3>Mentors</h3>
<ul>
	<li>Chris Aniszczyk</li>
	<li>Ed Merks</li>
</ul>
<h3>Interested parties </h3>
<ul>
<li><a href="http://nick.divbyzero.com">Nick Boldt</a></li>
<li>Curtis Windatt</li>
<li>Darin Wright</li>
<li><a href="http://www.linkedin.com/in/danrubel">Dan Rubel</a></li>
<li><a href="http://www.linkedin.com/in/blacksmith">Mark Russell</a></li>
<li>Kim Moir</li>
<li>Paul Webster</li>
<li>Benjamin Cab&eacute;</li>
<li><a href="http://www.ralfebert.de/">Ralf Ebert</a></li>
<li><a href="http://blog.efftinge.de/">Sven Efftinge</a></li>
<li><a href="http://www.eclipticalsoftware.com/">Peter Nehrer</a></li>
</ul>
<h2>Roadmap</h2>
<p>A formal project roadmap and plan will be developed following approval and creation of this project.</p>
<h2>Code Contributions</h2>
<p>We do not currently plan any outside code contributions in connection with this project.</p>
<p>All project code will either be developed in connection with this project or sourced from another
Eclipse project.</p> 
      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
