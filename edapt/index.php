<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Edapt -- Project Proposal";
$pageKeywords	= "emf, emft, ecore, refactoring, evolution, meta model evolution, compare, diff, patch, history, model";
$pageAuthor		= "Moritz Eysholdt, Markus Herrmannsdoerfer";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Edapt -- Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Edapt");
?>

<!-- ----- BEGIN inserted from Edapt-v*.html ----- -->
<b>Framework for Ecore model adaptation and instance migration</b>

<h2>Introduction</h2>
<p>
Edapt is an open source project proposed under the <a
href="http://www.eclipse.org/modeling/emft">Eclipse Modeling Framework
Technology</a> (EMFT).
</p>
<p>
The project is in the Project Proposal Phase (as defined by the <a
href="http://www.eclipse.org/projects/dev_process/">Eclipse Development
Process</a>) and this proposal declares its intent and scope. The proposal is
written to solicit additional participation and input from the Eclipse community.
You are invited to comment on and/or join the project. Please send all feedback
to the <a
href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">EMFT
newsgroup</a>.
</p>



<h2>Background</h2> 
<p>
Due to their high level of abstraction, modeling languages are a promising
approach to decrease software development costs by increasing productivity.
Significant work in both research and practice has been invested into tool
support for the initial development of modeling languages. Over the last years,
the Eclipse Modeling Framework (EMF) has become the de-facto standard solution
for the development of modeling languages. As modeling languages are more widely
used in both academia and industry, their maintenance is gaining importance.
</p>
<p>
Even though often neglected, a language is subject to change like any other
software artifact. The languages behind the Graphical Modeling Framework (GMF)
for instance -- although relatively young -- have already been evolved due to
technological progress or changing requirements. In EMF, a modeling language is
evolved by first adjusting its Ecore model to the new requirements. Other
artifacts like instances, editors, interpreters and transformations (M2M, M2T)
may no longer conform to the evolved Ecore model. Manually migrating these
artifacts to the evolved Ecore model is tedious and error-prone, and thus heavily
hampers cost-efficient maintenance of modeling languages.
</p>



<h2>Scope</h2>
<p>
Edapt will provide an extensible framework for reducing the migration effort that
results from the evolution of Ecore models.
Since the number of existing instances of a successful
modeling language typically outnumbers the number of editors, interpreters and
transformations, instance migration effort dwarfs tool reconciliation effort.
Consequently, Edapt clearly focuses on the migration of instances in the first
step. However, we plan to make it extensible with respect to the migration of
other artifacts like editors, interpreters and transformations. The basic idea
behind Edapt is to obtain the changes between two versions of an Ecore model, and
to enrich them with information about how to automatically migrate existing
instances. 
Edapt will support a pluggable mechanism to obtain the changes between two
Ecore model versions along with two examples:
the operation-based and the difference-based approach.
</p>
<p>
The operation-based approach is similar to code refactoring. The developer
modifies the Ecore model using predefined operations which encapsulate the
adaptation of the Ecore model as well as the migration of the instances. The
execution of these operations is recorded in a history which can then be used to
migrate existing instances. Being fully automated, these operations thus
significantly reduce the effort associated with migration. Moreover, they provide
a more systematic way to adapt Ecore models, e.g. they allow the developer to
better assess the impact on instances. However, not all changes can be covered by
existing operations. Therefore, Edapt provides a way to manually change the Ecore
model and attach information on how to migrate instances.
</p>
<p>
The difference-based approach compares two versions of an Ecore model, and
evaluates the differences using EMF Compare. As opposed to the operation-based
approach, this neither requires the developer to modify the Ecore model in
pre-planed steps nor editor integration. Furthermore, it makes the difference
based approach easily applicable to Ecore models which are not the primary
artifact of modification, but are derived from other artifacts.
</p>
<p>
Both approaches complement each other: The operation-based approach allows the
developer to capture the migration of instances while adapting the EMF model.
While this can be more systematic, it also causes some overhead. In contrast, the
difference-based approach allows the developer to enrich the changes later on
with instructions on how to migrate existing.
While we will provide these two approaches as examples, Edapt will be extensible
with respect to other mechanisms to obtain the changes.
</p>



<h2>Description</h2>
<p>
More specifically, Edapt will have to define APIs and provide implementations for
the following functionality which is common to both approaches:
</p>

<ul>
<li>Ecore models to specify the changes between two versions of an Ecore model and to
compose these as a complete history of all versions. These models should provide
means to attach migration instructions to the changes.</li>
<li>A slim runtime implementation which can be integrated into existing
applications and which interprets the change models to automatically migrate old
instances to the version needed by the application.</li>
</ul>

<p>
In addition, the operation-based approach requires the following functionality.
</p>

<ul>
<li>A way to define new operations and to extend existing ones with instructions on how to migrate other artifacts.</li>
<li>A generic user interface to offer operations to the developer and to inspect the impact of their executions.</li>
<li>A mechanism to record the operations that are executed on an Ecore model in the editor.</li>
<li>A way to re-apply the recorded operations to migrate existing artifacts which are not accessible during development.</li>
</ul>

<p>
In addition, the difference-based approach requires the following functionality:
</p>

<ul>
<li>A way to obtain the changes between two versions of an EMF model.</li>
</ul>



<h2>Relationship with other Eclipse Projects</h2>
<ul>
<li>Edapt will be built on top of EMF.</li>
<li>Edapt will allow to integrate languages for model transformation (e.g. ATL, QVT, Xtend) to specify the migration of instances.</li>
<li>Edapt will make use of the Language Toolkit (LTK) as far as possible or make contributions to it.</li>
<li>Edapt will need functionality that can be applied to refactoring of models in general.</li>
<li>Edapt will integrate with the Ecore Tools project.</li>
<li>Edapt will need EMF Compare to obtain differences between models.</li>
<li>Edapt's difference-based approach will be based on the Epatch format which is currently being contributed to EMF Compare.</li>
<li>Edapt can utilize TMF Xtext for providing textual representations of the patch format.</li>
</ul>

<h2>Organization</h2>

<h3>Mentors</h3>
<ul>
<li>Ed Merks (Macro Modeling, Canada)</li>
<li>Sven Efftinge (itemis AG, Germany)</li>
</ul>

<h3>Proposed initial committers</h3>
<ul>
<li>Markus Herrmannsd&ouml;rfer (Technische Universit&auml;t M&uuml;nchen, Germany)</li>
<li>Moritz Eysholdt (itemis AG, Germany)</li>
</ul>

<h3>Interested parties</h3>
<ul>
<li>The need for a tool supporting the evolution of EMF models has been discussed within the Modeling Symposium at the Eclipse Summit Europe 2008.</li>
<li>Ed Merks (Macro Modeling, Canada)</li>
<li>Laurent Goubet, St&eacute;phane Lacrampe, C&eacute;dric Brun (Obeo, France)</li>
<li>Enrico Schnepel (b+m Informatik AG, Germany)</li>
<li>Michael Rudorfer (BMW Car IT, Germany)</li>
<li>itemis AG, Germany</li>
<li>Eike Stepper (Eclipse CDO Team, Germany)</li>
<li>Bernd Kolb (SAP, Germany)</li>
<li>Ersin Er (Hacettepe University)</li>
<li>C&eacute;dric Vidal (ProxiAD, France)</li>
</ul>


<h3>Code contributions</h3>
<p>
The initial code contribution will be a set of plug-ins from the COPE project
(Coupled Evolution of Metamodels and Models). Visit the <a
href="http://cope.in.tum.de/">project's website</a> for further information and
documentation.
</p>
<!-- ----- END inserted from Edapt-v*.html ----- -->



      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
