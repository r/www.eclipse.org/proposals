<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>UFacekit</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("UFacekit");
?>

<a name="Introduction"></a><h2> Introduction</h2>

<p><a href="http://code.google.com/p/uface/" >UFacekit</a> is a proposed open source component under the Eclipse Incubator project. UFacekit's purpose is to:
</p>
<ul><li> improve adoption of Eclipse Core technologies (like Eclipse Data Binding) outside RCP and SWT (e.g. Swing, GWT, QT)
</li><li> improve usability of Eclipse Data Binding by providing a high-level Widget-API
</li></ul>

<p>This proposal is in the <a href="http://www.eclipse.org/projects/dev_process/development_process.php" >Project Proposal Phase</a> and and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the effort. Please send all feedback to 
the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.ufacekit">eclipse.ufacekit</a> newsgroup.</p>
</p>

<a name="Background"></a><h2> Background</h2>

<p>Since the 3.3 release, Eclipse ships an actively developed and extended data binding framework (Eclipse Data Binding) that eases the development of UI applications backed by a model. At the time of the 3.4 release of Eclipse, the Eclipse Data Binding framework had been adopted by many RCP applications and new libraries supporting model implementations (EMF) besides JavaBeans(tm). So far, we have not seen support for other widget toolkits (such as for example Swing, GWT, etc.) although the main framework is designed to make this possible. This means that the Eclipse Data Binding framework cannot currently be used in applications which are not using SWT and JFace, although there is no good reason not to do so.
</p><p>UFacekit was started in December 2007 by James Strachan and Tom Schindl (Angelo Zerr and Kenneth Westelinck joined the project later) with the following set of targets:
</p>
<ul><li> Promote reuse of rich UI code across Eclipse (JFace/SWT), GWT and Swing runtimes by providing a facade around different UI-Technologies.
</li><li> Promote the use of Eclipse Data Binding within GWT and Swing communities by providing Eclipse Data Binding Observable implementations for them.
</li><li> Make new applications stacks accessible to EMF (e.g. Swing).
</li><li> Simplify rich UI development by providing simpler facades and powerful binding.
</li></ul>
<a name="Scope"></a><h2> Scope</h2>

<p>UFacekits scope is to develop highlevel API and framework modules to develop applications in a widget-toolkit independent fashion which are small and easy to extend (e.g. create an implementation for other widget-toolkits, use different widget types, Nebula-Grid instead of SWT-Table). UFacekit differs in this scope with SWT as it won't provide access to lowlevel drawing operations like GC but only provides the minimal needed subset of methods needed to develop applications which makes the task of extending and porting to new technologies easier. 
</p><p>Besides the widget-toolkit neutral API UFacekit will provide established eclipse technologies/modules for currently not support platforms/runtimes (EMF for GWT, Databinding for GWT) and implementation of Databinding-Observables and JFace-Viewer implementations for currently not support model-technologies and widget-toolkits.
</p> 
<a name="UFacekit_Design"></a><h2> UFacekit Design</h2>

<p>UFacekit is designed in a modular way as OSGi-Bundles to use its subcomponents with a minimal set of dependencies. For example, if a developer only wants to use Eclipse Data Binding in his/her project she will only use the Swing-Observables-Bundle, if he/she only wants to use a light weight model implementation he/she only takes the UBean-Bundles.
</p><p>As already mentioned all UFacekit deliverables are packaged as OSGi-Bundles. By following the OSGi-standards it can be used with any OSGi implementation following the standards (Felix, Knopflerfish, ...).
</p><p><a href="/Image:UFaceKitUML.png" class="image" title="UFaceKitUML.png"><img alt="" src="1000px-UFaceKitUML.png" width="1000" height="700" border="0" /></a>
</p><p>The above design keeps the UI code free from the toolkit Code and holds as few information as possible about the modeling technology (maybe this could be removed completely in the future).
</p><p>An UBean-Example:
</p>
<div dir="ltr" style="text-align: left;"><pre class="source-java"><span class="kw2">public</span> <span class="kw4">void</span> createUI<span class="br0">&#40;</span>UIComposite root, UBeanForm form, IObservableValue value<span class="br0">&#41;</span> <span class="br0">&#123;</span>
  UIFactory ui = root.<span class="me1">getFactory</span><span class="br0">&#40;</span><span class="br0">&#41;</span>;

&nbsp;
  <span class="co1">// The UI setup</span>
  InputFieldUIInfo uiInfo = = <span class="kw2">new</span> InputFieldUIInfo<span class="br0">&#40;</span>GridLayoutData.<span class="me1">fillHorizontalData</span><span class="br0">&#40;</span><span class="br0">&#41;</span>,InputFieldUIInfo.<span class="me1">SIMPLE</span><span class="br0">&#41;</span>;
  UIInputField idField = ui.<span class="me1">newInputField</span><span class="br0">&#40;</span>detailComposite, uiInfo<span class="br0">&#41;</span>;

&nbsp;
  <span class="co1">// The Binding setup</span>
  InputFieldBindingInfo bindingInfo = InputFieldBindingInfo.<span class="me1">newTextFieldInfo</span><span class="br0">&#40;</span>form.<span class="me1">detailValue</span><span class="br0">&#40;</span>Person.<span class="me1">ID</span>, <span class="kw4">int</span>.<span class="kw2">class</span><span class="br0">&#41;</span><span class="br0">&#41;</span>;
  form.<span class="me1">add</span><span class="br0">&#40;</span>idField, bindingInfo<span class="br0">&#41;</span>;

&nbsp;
  <span class="co1">// Bind the master value</span>
  form.<span class="me1">bind</span><span class="br0">&#40;</span>value<span class="br0">&#41;</span>;
<span class="br0">&#125;</span></pre></div>
<p>An EMF-Example:
</p>
<div dir="ltr" style="text-align: left;"><pre class="source-java"><span class="kw2">public</span> <span class="kw4">void</span> createUI<span class="br0">&#40;</span>UIComposite root, EMFForm form, IObservableValue value<span class="br0">&#41;</span> <span class="br0">&#123;</span>

  UIFactory ui = root.<span class="me1">getFactory</span><span class="br0">&#40;</span><span class="br0">&#41;</span>;
&nbsp;
  <span class="co1">// The UI setup</span>
  InputFieldUIInfo uiInfo = = <span class="kw2">new</span> InputFieldUIInfo<span class="br0">&#40;</span>GridLayoutData.<span class="me1">fillHorizontalData</span><span class="br0">&#40;</span><span class="br0">&#41;</span>,InputFieldUIInfo.<span class="me1">SIMPLE</span><span class="br0">&#41;</span>;
  UIInputField idField = ui.<span class="me1">newInputField</span><span class="br0">&#40;</span>detailComposite, uiInfo<span class="br0">&#41;</span>;

&nbsp;
  <span class="co1">// The Binding setup</span>
  InputFieldBindingInfo bindingInfo = InputFieldBindingInfo.<span class="me1">newTextFieldInfo</span><span class="br0">&#40;</span>form.<span class="me1">detailValue</span><span class="br0">&#40;</span>PersonPackage.<span class="me1">Literals</span>.<span class="me1">PERSON__ID</span>, <span class="kw4">int</span>.<span class="kw2">class</span><span class="br0">&#41;</span><span class="br0">&#41;</span>;
  form.<span class="me1">add</span><span class="br0">&#40;</span>idField, bindingInfo<span class="br0">&#41;</span>;

&nbsp;
  <span class="co1">// Bind the master value</span>
  form.<span class="me1">bind</span><span class="br0">&#40;</span>value<span class="br0">&#41;</span>;
<span class="br0">&#125;</span></pre></div>
<a name="Examples"></a><h3> Examples</h3>
<p>The following screenshots are based on the exact same code base, the only difference being the initial composite (the Shell used as a root).

</p><p>SWT/JFace
</p><p><a href="/Image:UFaceSWT.png" class="image" title="Image:UFaceSWT.png"><img alt="Image:UFaceSWT.png" src="UFaceSWT.png" width="400" height="299" border="0" /></a>
</p><p>Swing
</p><p><a href="/Image:UFaceSwing.png" class="image" title="Image:UFaceSwing.png"><img alt="Image:UFaceSwing.png" src="UFaceSwing.png" width="400" height="342" border="0" /></a>
</p><p>MyGWT (ancient and has to be rewritten)
</p><p><a href="/Image:UFaceMyGWT.jpg" class="image" title="Image:UFaceMyGWT.jpg"><img alt="Image:UFaceMyGWT.jpg" src="UFaceMyGWT.png" width="400" height="290" border="0" /></a>
</p>
<a name="Relationship_with_other_Eclipse-based_Projects"></a><h2> Relationship with other Eclipse-based Projects</h2>
<p>UFacekit has a strong relation to the following projects:
</p>
<ul><li>Eclipse-Databinding (Core & JFace) providing the foundation for the high-level API</li>
<li>EMF (As a Databinding possibility, GWT support for EMF-Models)</li>
<li>E4 (SWT-Port for the web, create a class-lib for missing GWT-classes (e.g. Runnable) )</li>
<li>Riena where some of the targets overlap (e.g. highlevel Databinding API)</li>
</ul>
<a name="Organization"></a><h2> Organization</h2>
<a name="Initial_Committers"></a><h3> Initial Committers</h3>
<ul><li> <b>Tom Schindl - BestSolution Systemhaus Gmbh - tom.schindl@bestsolution.at (component lead)</b><p>Tom is self-employed and CEO of BestSolution.at Systemhaus Gmbh a software company building applications (RCP, J2EE) for companies around the world. Besides implementing solutions their own BestSolution.at consulted companies to introduce Eclipse Technologies into their software stack by providing its knowledge about Eclipse Technologies and Software Design experience.</p><p>Tom is one of the Platform-UI and Nebula committers working on JFace-Viewers, Nebula-Grid and contributed patches to other eclipse projects (EMF, ...). He is part of the E4 project team and wrote the EMF based platform prototype used as the starting point for the implementation of the next generation of the Eclipse-Platform.</p><p>He is a regular contributor to the eclipse newsgroups and received the top contributor award in 2007 for his work on JFace-Viewers.</p> 
</li><li> <b>James Strachan - Progress Software - james.strachan@gmail.com</b><p>James works in the <a href="http://open.iona.com/" class="external text" title="http://open.iona.com/" rel="nofollow">FUSE group at Progress</a> and is a long time contributor to open source projects mostly at Apache and works on projects like <a href="http://activemq.apache.org/" class="external text" title="http://activemq.apache.org/" rel="nofollow">ActiveMQ</a>, <a href="http://activemq.apache.org/camel/" class="external text" title="http://activemq.apache.org/camel/" rel="nofollow">Camel</a>  and <a href="http://servicemix.apache.org/home.html" class="external text" title="http://servicemix.apache.org/home.html" rel="nofollow">ServiceMix</a></p> 
</li><li> <b>Angelo Zerr - Independent - angelo.zerr@gmail.com</b><p>Angelo is the developer of <a href="http://tk-ui.sourceforge.net/" class="external text" title="http://tk-ui.sourceforge.net/" rel="nofollow">TK-UI</a>, a toolkit to manage CSS Styling/Declarative UI (XUL, XHTML) which renders to Swing and SWT.  He has also developed <a href="http://akrogen.sourceforge.net/" class="external text" title="http://akrogen.sourceforge.net/" rel="nofollow">Akrogen</a>, an Eclipse plugin for code generation where you can describe Eclipse Wizard with XUL and Javascript and link it with template (Freemarker, Velocity, XSL...) or Ant Task to generate code.</p> 
</li><li> <b>Kenneth Westelinck - Ebit NV - kenneth.westelinck@gmail.com</b><p>Kenneth is a senior Java Software Engineer at Ebit NV specialized in building custom J2EE, Hibernate and Spring based applications for disparate types of clients. In the past, his main focus has been solving application integration problems for clients, using both Commercial and Open Source software (Mule). Kenneth is a regular contributor to the <a href="http://mulesource.com" class="external text" title="http://mulesource.com" rel="nofollow">Mule ESB platform</a> and active on the mailing lists. Currently he's working on technical projects for an engineering company involving Eclipse RCP GUIs for monitoring and administration purposes.</p> 
</li></ul> 
<a name="Initial_Code_Contribution"></a><h3> Initial Code Contribution</h3>
<p>EPL licensed code coming from <a href="http://code.google.com/p/uface/" class="external text" title="http://code.google.com/p/uface/" rel="nofollow">UFacekit-Project</a>. 
</p><p>UFacekit dependes on some external opensource modules:
</p> 
<ul><li> GWT (Apache License V2.0 already <a href="https://dev.eclipse.org/ipzilla/show_bug.cgi?id=2616" class="external text" title="https://dev.eclipse.org/ipzilla/show_bug.cgi?id=2616" rel="nofollow">IP</a> approved)
</li><li> GWT-logging (Apache License V2.0, See <a href="http://code.google.com/p/gwt-log/" class="external text" title="http://code.google.com/p/gwt-log/" rel="nofollow">Homepage</a>)
</li><li> Rhino (MPL, Consumed from Orbit)
</li><li> Log4j (Apache License V2.0, Consumed from Orbit)
</li><li> SwingX (LGPL)
</li></ul> 
<a name="Interested_Parties"></a><h3> Interested Parties</h3>
<p>Anyware Technologies
</p>
<a name="Tentative_Plan"></a><h2> Tentative Plan</h2>
<h3>Initial Objectives</h3>
<ul><li> a highlevel, widget-toolkit and model-indepent Widget API to easily bind widgets (including validation and decoration support)
<ul><li> UI
<ul><li> implementation for SWT/JFace
</li><li> implementation for Swing
</li></ul> 
</li><li> Model
<ul><li> JavaBeans
</li><li> EObjects (EMF)
</li><li> UBeans
</li></ul> 
</li></ul> 
</li><li> Observable implementations for Swing
<ul><li> JFace-Viewers for Structured-Swing-Controls (List, Table, Tree)
</li></ul> 
</li><li> Observable implementations for UBean
</li><li> Observable implementations for XML-DOM
</li><li> org.eclipse.core.databinding for use in GWT applications based upon current stable Eclipse release
</li><li> Observable implementations for GWT-Widgets
</li></ul> 
<h3>Additional objectives considered for later</h3>
<ul><li> GWT support for EMF-Core-Modules
</li><li> Support for Eclipse-Forms
</li><li> Support to define UI and UFacekit-Applications (Upplication) using EMF
</li><li> Support to design Upplications using GEF
</li><li> Support for other Java-UI-Toolkits (QT-Jambi, DOJO, ...)
</li><li> Declarative Styling (EMF-Model/CSS)
</li></ul> 
<p>Q4 - 2008: Milestone 1 (all objectives implemented (SWT &amp; Swing))
</p><p>Q1 - 2009: Milestone 2 (all objectives for an initial version implemented)

</p>
      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
-->