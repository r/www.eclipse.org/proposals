<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Eclipse Model-to-Text (M2T) Transformation Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Model-to-Text (M2T) Transformation");
?>

<h2>Introduction</h2>
<p>Revised: Dec 7, 2006</p>
<p>The Model-to-Text Transformation (M2T) Transformation Project is
a proposed open source project under the <a
	href="http://www.eclipse.org/modeling/">Eclipse Modeling Project</a>.</p>

<p>This proposal is in the Project Pre-Proposal Phase (as defined in
the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse
Development Process</a> document) and is written to declare its intent and
scope. This proposal is written to solicit additional participation and
input from the Eclipse community. You are invited to comment and/or join
the project. Please send all feedback to the <a
	href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling.m2t/">http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling.m2t</a>
newsgroup.</p>

<h2>Background</h2>

<p>Model Driven
Development (MDD) and OMG <a href="http://www.omg.org/mda/">Model
Drive Architecture (MDA)</a> emphasize the creation of models of a software
solution which are then refined and transformed until ultimately source
code (e.g. Java, C++) and associated resources (e.g. properties files,
configuration files) are created. The <a
	href="http://www.eclipse.org/proposals/m2m/">Model-to-Model (M2M) Transformations Project proposal</a>
(http://www.eclipse.org/proposals/m2m/) focuses on technologies to
transform between different models. This proposal focuses on
technologies for transforming models into text (typically language
source code and the resources it consumes).</p>

<p>Solutions for
model-to-text transformations generally make use of "templates". A
template may be generally described as a text sequence interspersed with
commands that extract information from a model. Commands are not limited
to information extraction � they often serve as flow control in a
template; controlling whether and how many times blocks of text are
processed. </p>

<p>There are a large
number of template languages � a Google search on �template language�
yields 258,000 hits. The parties interested in this proposal represent
or have interest in the following languages.</p>

<ul>
<li><a href="http://www.eclipse.org/emft/projects/jet">JET</a>, a
derivative of JavaServer Pages (JSP) initially developed as part of the
Eclipse Modeling Framework (EMF), and currently an incubator project in
the Eclipse Modeling Project.</li>

<li>XPand, the template language that is part of <a
	href="http://www.eclipse.org/gmt/oaw/">openArchitectureWare</a>.</li>

<li>Acceleo template language, part of OBEO�s <a
	href="http://www.acceleo.org/">Acceleo MDA generator</a>.</li>

<li><a href="http://www.omg.org/cgi-bin/doc?ad/2004-4-7">OMG MOF Model to Text</a>,
a recently approved OMG standard for text generation from MOF models; intended to complement OMG QVT.</li>
</ul>

<p>One of the
concerns of this project is to reduce the duplicate effort involved in
developing and maintaining template languages.</p>

<h3>Technologies of a Model-to-Text solution</h3>

<p>Expanding
templates is only a part of the requirements for a Model-to-Text
solution. In addition, solutions must typically have:</p>
<ul>
<li>A mechanism for accessing and navigating diverse input models.</li>

<li>A mechanism to add "derived" properties to existing meta
models. Such additional properties are almost always required for
real-world generators.</li>

<li>A mechanism for orchestrating template execution, and saving
their results to a file system. In the Eclipse environment, this
includes playing well with Team providers, and correctly handling file
encodings.</li>

<li>A mechanism for integrating generated and manually written
code. This encompasses respecting and reconciling user changes when
regenerated output is merged with user modified output, as well as
providing tool support for "enforcing" conventions (such as e.g. that
developers have to create a subclass from a generated class and
implement certain methods)</li>
</ul>

<p>In many cases,
these "template language technologies" can be implemented in a language
independent fashion.</p>

<h2>Project Description</h2>

<p>The M2T project will deliver the following:</p>

<ul>
<li>An extensible framework (the M2T Invocation Framework) for
integrating model-to-text languages into MDD/MDA solutions. The initial
goals of this framework would be to allow API consumers to:</li>
<ul>
<li>Invoke a component supplied template independently of the
template language used.</li>

<li>Permit runtime substitution of a user supplied template for a
component supplied template, regardless of the template languages used
by either. That is, the two templates may be implemented by different
languages.</li>

<li>Invoke a component supplied code generator independently of
the technology used to create the code generator. </li>

<li>Permit runtime substitution of a user supplied code generator
for a component supplied code generator, regardless of the code
generator technologies employed.</li>
</ul>
<li>APIs to implement common template language technologies. The
primary consumers of these APIs would be template language
implementations themselves.</li>

<li>Implementations of template languages strategic to the Eclipse
Modeling Project. These include:</li>
<ul>
<li>JET</li>

<li>XPand</li>

<li>OMG MOF Model to Text</li>
</ul>
<li>For each strategic template language:</li>
<ul>
<li>Integrations into the M2T Invocation Framework. </li>

<li>Template editors.</li>
</ul>
<li>Various tools/approaches for integrating generated and
manually written code, such as protected region management, merging
support, a recipe framework</li>
</ul>

<p>Implementations will use EMF Ecore meta-models (<a
	href="http://www.eclipse.org/emf">www.eclipse.org/emf</a>) as the
primary meta-model for consumed models. </p>

<p>Because M2T should not become a warehouse for model-to-text
languages, the project will host only languages deemed strategic to the
goals of the Eclipse Modeling Project. Most existing languages already
have a "home", and there would be little benefit from migration to a new
project. At inception, the JET language has been identified as strategic
because of its long association with EMF. The XPand language from
openArchitectureWare is also strategic. 
The OMG MOF Model to Text language is strategic because it is represents an emerging a standard
in the model-to-text domain.
Each strategic language will be
a separate component so as to be separately distributable and
installable.</p>

<p>Similarly, integrations with the M2T Invocation Framework should
be hosted in the same project as the model-to-text language itself.
However, in cases where this is not possible and the integration is
strategic to the goals of the Eclipse Modeling Project, the M2T project
can serve as the host project. Again, each such integration will be a
separate component so as to be separately distributable and installable.</p>

<h2>Initial Contributions</h2>

<p>The following code will be contributed to the project:</p>

<ul>
<li>JET, from the EMFT JET subproject.</li>

<li>Utility features (JMerge, ImportsManager) from the JET
component of EMF.</li>

<li>JET Editor, from the EMFT JETEditor subproject.</li>

<li>XPand and its required oAW components (expression language,
XTend)</li>

<li>Appropriate oAW editors (XPand editor, XTend editor)</li>

<li>oAW�s Recipe Framework for integrating generated and manually
written code</li>

<li>Compuware has offered two committers to implement the OMG MOF-Model-to-Text (MM2T) standard. 
There would be entirely new development.</li>
</li>
</ul>

<h2>Organization</h2>

<p>We propose that this project be undertaken as part of the Eclipse
Modeling Project. It will have dependencies on the Eclipse Platform,
including the JDT and EMF.</p>

<p>
Further more, it is proposed that the project be divided into components as follows. Each component would have a lead and its own committers (although nothing would prevent one committer working on several components).
</p>
<ul>
<li>Model-to-Text Invocation Framework (M2T-IF)</li>
<li>Shared components</li>
<li>JET</li>
<li>XPand</li>
<li>OMG MOF Model to Text (MM2T)</li>
</ul>

<h3>Initial committers</h3>
<p>The following
companies will contribute committers to get the project started:</p>
<ul>
<li>IBM (<a href="http://www.ibm.com/">www.ibm.com</a>)</li>
 
    <ul>
		<li> Paul Elder (proposed project lead) - JET, M2T-IF, Shared components</li>
	</ul>
	<li>OpenArchitectureWare</li>
	<ul>
		<li>Markus Voelter - XPand, Shared components</li>
		<li>Sven Efftinge - XPand</li>
		<li>Bernd Kolb - XPand</li>
	</ul>
	<li>Compuware</li>
	<ul>
		<li>Two (2) committers for OMG Model to Text</li>
	</ul>
	<li>ILog</li>
	<ul>
		<li>Joel Cheuoua - JET (JET editor)</li>
	</ul>
	<li>OBEO</li>
	<ul>
		<li>TBD - shared components</li>
	</ul>
</ul>

<h3>Initial Contributors</h3>

<p>Artem Tikhomirov
(Borland) - a version of XPand that uses LPG rather than Antlr</p>

<h3>Interested parties</h3>

<p>The following
organizations have expressed interest in the project:</p>
<ul>
<li>openArchitectureWare (<a
	href="http://www.openarchitectureware.org/">www.openarchitectureware.org</a>)</li>

<li>Borland (GMF)</li>
<ul>
<li>GMF has proposed embedding XPand as template technology for
GMF..</li>
</ul>
<li>OBEO (<a href="http://www.obeo.fr/">www.obeo.fr</a>) �
creators of Accelio MDA.</li>
<ul>
<li>OBEO is interested in contributing the following technologies:</li>
<ul>
<li>Reflective editor between model and text.</li>

<li>Editor technology for JET.</li>
</ul>
</ul>
<li>ILOG (<a href="http://www.ilog.com/">www.ilog.com</a>) �
currently working on the EMFT Jet Editor</lip>

<li>IBM (JET)</li>
<li>Sybase</li>
</ul>
<h3>Participation</h3>

<p>Critical to the success of this project is participation of
developers in the modeling application development community. We intend
to reach out to this community and enlist the support of those
interested in making a success of the Model-to-Text Transformation
project. We ask interested parties to contact the M2T newsgroup to
express interest in contributing to the project.</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
