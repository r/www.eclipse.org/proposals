<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Kitalpha</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->

<style>
dt {
	display: list-item;
	list-style-position: outside;
	list-style-image:
		url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
	margin-left: 16px;
}

dd {
	margin-left: 25px;
	margin-bottom: 5px;
}
</style>

<body>
	<p>
		The Kitalpha project is a proposed open source project under the <a
			href="http://Polarsys.org/">PolarSys Top level Project</a>.
	</p>

	<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
	<p>
		This proposal is in the Project Proposal Phase (as defined in the
		Eclipse Development Process) and is written to declare its intent and
		scope. We solicit additional participation and input from the Eclipse
		community. Please send all feedback to the <a
			href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse
			Proposals</a> Forum.
	</p>

	<h2>Background</h2>

	<p>In system and software engineering, a common need during the
		phases of analysis, design, and implementation, is to describe a
		system. Several standards have been established to define a shared
		notation or concepts for system description. For instance:</p>
	<ul>
		<li>The objective of <a href="http://www.uml.org/">UML</a> is to
			provide system architects, software engineers, and software
			developers with tools for analysis, design, and implementation of
			software-based systems as well as for modeling business and similar
			processes [UML 2.5, FTF].
		</li>
		<li>The <a href="http://en.wikipedia.org/wiki/ISO/IEC_42010">ISO/IEC
				42010 standard</a> defines requirements on the description of system,
			software and enterprise architectures. It aims to standardize the
			practice of architecture description by defining standard terms,
			presenting a conceptual foundation for expressing, communicating and
			reviewing architectures and specifying requirements that apply to
			architecture descriptions, architecture frameworks and architecture
			description languages.
		</li>
	</ul>


	<p>
		Multiple tools implement such standards. At Eclipse, <a
			href="http://wiki.eclipse.org/MDT-UML2">UML2</a> and <a
			href="http://eclipse.org/papyrus/">Papyrus</a> implement the UML
		standard. While it would be possible to implement the ISO/IEC 42010
		standard with a UML tool, no Eclipse or PolarSys component has
		natively implemented it yet.
	</p>

	<h2>Scope</h2>

	<p>Kitalpha is a modeling component which implements the ISO/IEC
		42010 standard for system description in system and software
		engineering. It provides both a development and runtime environment to
		create and execute rich MBE (model-based engineering) workbenches
		(e.g., edition with diagrams, documentation, import/export, model
		transformation / analysis / validation) for system / software
		architects and engineers in small- to large-scale projects.</p>
	<p>For reusability, the development environment contains a kit of
		MBE core components, i.e. a set of engineering bricks common to
		different variants of MBE workbenches such as a semantic browser,
		impact analyzer, or import/export functions.</p>


	<h2>Description</h2>

	<p>Conforming with this standard, an MBE workbench is an
		architecture framework which aggregates viewpoints clearly separating
		concerns (e.g., performance, safety, security, cost). A viewpoint is
		an engineering extension which comes with its own metamodels,
		representations (e.g., diagrams, tables, user interfaces), rules
		(e.g., validation, analysis, transformation), services and tools to
		address an engineering specialty. Consequently, an MBE workbench is
		the result of a flexible assembly of core viewpoints extended by new
		ones which are, in the context of co-engineering, appropriate and
		valuable for specialty engineers. The set of all the viewpoints
		defines the complete description of a system.</p>

	<p>To build MBE workbenches, designers must be autonomous in
		creating and maintaining their own viewpoints, without coding.
		Developers can enrich them afterward, for instance for algorithm
		implementation. To meet this requirement, Kitalpha offers a
		development environment made of DSLs (Domain-Specific Languages) to
		assist designers and developers in their architecture frameworks and
		viewpoints development activity activities. For instance, textual
		editors make it possible to declare viewpoint metamodels, user
		interfaces, diagrams, or services. From those DSLs, generators build
		all the architecture framework and viewpoint artifacts. For example,
		the declaration of diagrams using DSLs becomes the technical
		description of Sirius diagrams. During the stages of edition with DSLs
		and generation, the notion of target application is introduced to
		manage the variability of environments in which the artifacts are to
		be deployed and executed (e.g., DSL vs. UML, CDO vs. XMI
		environments).</p>

	<center>
		<img src="Kitalpha-ProcessOverview.png" width="480px">
		<p>
			<i>Figure 1. Development Process Overview</i>
		</p>
	</center>

	<br>

	<p>The Kitalpha development environment also provides MBE core
		components to develop complete MBE workbenches, for instance for the
		functions of import/export (e.g., for data exchange with MBE
		workbenches or specialty tools), model transformations with
		traceability, or html documentation generation.</p>
	<p>Kitalpha potentially has the ability to implement architecture
		framework standards (e.g., TOGAF/MODAF) but also proprietary method or
		domain workbenches.</p>
	<br>

	<center>
		<img src="Kitalpha-MBEWorkbench.png" width="480px">
		<p>
			<i>Figure 2. Illustration of MBE workbench</i>
		</p>
	</center>

	<p>This rest of this section describes the key features of
		Kitalpha.</p>

	<h3>Anatomy of Kitalpha</h3>

	<h4>Architectural position of Kitalpha</h4>
	<p>Kitalpha can be considered as a meta-tool. The following picture
		presents the different working contexts with regard to Kitalpha.</p>
	<ol>
		<li>At the top, stakeholders describe a system architecture,</li>
		<li>An MBE workbench enables designers to describe system
			architecture,</li>
		<li>Kitalpha is a workbench allows developing and executing MBE
			workbenches,</li>
		<li>Finally, Kitalpha is built atop Eclipse and mainly uses
			modeling components.</li>
	</ol>

	<br>

	<center>
		<img src="Kitalpha-Position.png" width="480px">
		<p>
			<i>Figure 3. Position of Kitalpha</i>
		</p>
	</center>

	<h4>Elements of internal architecture</h4>

	<p>Kitalpha is divided into two layers:</p>
	<ul>
		<li>An <i>Engineering Layer</i> which is a set of components and
			services for developing and executing MBE workbenches. This layer
			contains all the architecture framework / viewpoint metamodels, DSLs
			with their representations, and services (Cf. section about the
			Kitalpha services).
		</li>
		<li>A <i>Core Technology Kit (CTK)</i> which is a set of
			technical components and frameworks required by the Engineering
			Layer.
		</li>
	</ul>

	<p>The CTK is dedicated to host a set of added-value technological
		components which are not provided by Eclipse components or to adapt
		existing tools for a specific purpose (e.g., adaptation of the
		standard EGF factories). A substantial tool or set of tools can be
		grouped in a Kitalpha sub-project for a better partitioning and
		decoupling of the sub-components development lifecycles.</p>


	<h3>Precision of the ISO/IEC-42010 standard</h3>
	<p>The ISO/IEC-42010 standard is intentionally general in order to
		be open and declined into multiple implementations. The following
		parts present the choices made to implement the standard.</p>

	<h4>Kitalpha viewpoint</h4>
	<p>In compliance with the standard, a Kitalpha architecture
		framework aggregates viewpoints. A Kitalpha viewpoint is defined as:</p>
	<ul>
		<li>A set of metamodels</li>
		<li>A set of notations (e.g., icons)</li>
		<li>A set of representations (e.g., textual, graphical)</li>
		<li>A set of rules (e.g., check, transformation)</li>
		<li>A set of services</li>
		<li>A set of tools</li>
		<li>Moreover, a viewpoint inherits and aggregates viewpoints.</li>
	</ul>


	<h4>Kitalpha services</h4>

	<p>Kitalpha provides both development and runtime services to
		define, use and manage architecture frameworks and viewpoints.</p>

	<p>Main services at development time are:</p>
	<ul>
		<li>For Architecture Framework (AF): 1) definition of an AF, 2)
			generation of AF artifacts, 3) packaging of AF artifacts with the
			viewpoints it aggregates.</li>
		<li>For Viewpoint: 1) definition of a viewpoint, 2) generation of
			viewpoint artifacts, 3) packaging of viewpoint artifacts.</li>
	</ul>


	<p>Main services at runtime are:</p>
	<ul>
		<li>Core services:</li>
		<ul>
			<li>System architecture description with an architecture
				framework and its viewpoints</li>
			<li>Viewpoint Manager in order to monitor viewpoints</li>
			<li>Activation / Deactivation of a viewpoint</li>
			<li>Detachment / Attachment of viewpoint data</li>
			<li>Migration of a viewpoint</li>
		</ul>
		<li>Additional services:</li>
		<ul>
			<li>Versioning</li>
			<li>Team Working</li>
			<li>Reporting</li>
			<li>Architecture Assessment</li>
			<li>Test</li>
			<li>Simulation</li>
		</ul>
	</ul>

	<p></p>


	<h2>Why PolarSys?</h2>

	<p>By releasing and developing Kitalpha publicly, the objectives
		are the following:</p>
	<ol>
		<li>Strengthening the PolarSys and Engineering Eco-system by
			enabling the creation of rich Model-Based Engineering workbenches at
			a very low cost.</li>
		<li>Providing an open-source implementation of the ISO/IEC-42010
			standard.</li>
		<li>Being an enabler to provide viewpoints among the PolarSys
			community.</li>
		<li>Unleashing the potential collaborations with other partners
			and projects in a healthy environment.</li>
	</ol>

	<h3>Relationship with other Eclipse and PolarSys Projects</h3>

	<p>The Kitalpha project is based on Eclipse Modeling projects,
		notably:</p>
	<ul>
		<li>Xtext to textually define architecture frameworks and
			viewpoints.</li>
		<li>Sirius is used at development time (e.g., for a graphical
			rendering of the architecture frameworks and viewpoints, or
			documentation) and runtime (e.g., for edition with diagrams in MBE
			workbenches).</li>
		<li>Ecore Tools 2+ for edition of viewpoint metamodels.</li>
		<li>EGF to mass-produce architecture framework and viewpoint
			artifacts from textual descriptions.</li>
		<li>Each aspect of a viewpoint description is generally
			implemented by at least one Eclipse / PolarSys component. For
			example, OCL can be used to express constraint rules.</li>
	</ul>


	<h2>Initial Contribution</h2>

	<p>The initial contribution includes:</p>
	<ul>
		<li>The development and runtime environment to create and execute
			MBE workbenches.</li>
		<li>The runtime environment limited to the core services listed
			in the section of Kitalpha services.</li>
	</ul>


	<h2>Legal Issues</h2>
	<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->
	<p>All contributions will be distributed under the Eclipse Public
		License.</p>


	<h2>Committers</h2>
	<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

	<p>Project lead:</p>
	<ul>
		<li>Benoit Langlois, Thales Global Services</li>
	</ul>


	<p>The following individuals are proposed as initial committers to
		the project:</p>
	<ul>
		<li>Benoit Langlois, Thales Global Services</li>
		<li>Boubekeur Zendagui, Obeo</li>
		<li>Guillaume Gebhart, Obeo</li>
		<li>Jean Barata, Thales Global Services</li>
		<li>Matthieu Helleboid, Thales Global Services</li>
		<li>Philippe Dul, Thales Services SAS</li>
		<li>Thomas Guiu, Soyatec</li>
		<li>Tristan Faure,Atos</li>
		<li>Pierre Gaufillet, Airbus</li>
	</ul>

	<p>We welcome additional committers and contributions.</p>

	<h2>Initial Contributors</h2>
	<p>Thales will provide the initial contribution which has been
		developed in the context of the Sys2Soft (BGLE) and Crystal (Artemis)
		project and that is already deployed in an industrial context. AIRBUS
		and ATOS will contribute minor modelling features developed in
		TOPCASED. Additional contributors of the initial version:</p>
	<ul>
		<li>Amine Lajmi, Itemis</li>
	</ul>

	<h2>Mentors</h2>

	<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

	<p>The following Architecture Council members will mentor this
		project:</p>

	<ul>
		<li>Cedric Brun</li>
		<li>Kenn Hussey</li>
	</ul>

	<h2>Interested Parties</h2>

	<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

	<p>The following individuals, organizations, companies and projects
		have expressed interest in this project:</p>

	<ul>
		<li>Airbus Group</li>
		<li>Altran</li>
		<li>Artal</li>
		<li>INRIA Sophia Antipolis M&eacute;diterran&eacute;e</li>
		<li>Obeo</li>
		<li>Samares</li>
		<li>Soyatec</li>
		<li>Thales Global Services</li>
		<li>Universit&eacute; de Rennes 1</li>
	</ul>

	<h2>Project Scheduling</h2>

	<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->
	<ul>
		<li>February 2014: code submission and IP review</li>
		<li>March 2014: 0.2 release</li>
		<li>June 2014: 0.3 release based on Luna</li>
	</ul>

	<h2>Changes to this Document</h2>

	<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

	<table>
		<tr>
			<th>Date</th>
			<th>Change</th>
		</tr>
		<tr>
			<td>10-December-2013</td>
			<td>Document created</td>
		</tr>
		<tr>
			<td>22-January-2014</td>
			<td>Version for proposal</td>
		</tr>
	</table>
</body>
</html>