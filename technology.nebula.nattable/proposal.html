<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>NatTable</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<p>The NatTable project is a proposed open source project under the <a
	href="http://www.eclipse.org/projects/project_summary.php?projectid=technology.nebula">Nebula
Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<h2>Background</h2>

<p>NatTable is a SWT data grid widget designed to handle sophisticated, performance sensitive data visualization requirements. There are numerous SWT table/grid widgets available, but many of these face performance or resource utilization issues when attempting to display very large result sets, or they lack features or flexibility required to build complex and customizable grid-centric user interfaces. This is a limiting factor for creating complex applications such as spreadsheets or trading systems on SWT and RCP. NatTable was designed to address these gaps and provide an enabling solution for SWT/RCP applications that require sophisticated data grid functionality.</p>

<p>NatTable was started as an open source project by Andy Tsoi in 2007 and is currently led by Edwin Park, with more than a dozen committers from various companies contributing to the codebase over the years. It was originally targeted towards implementing financial trading systems, and some of its initial design goals were to support operation over data sets with hundreds of thousands of rows and hundreds of columns, with data being updated in realtime and dynamically styled according to custom rules, with no perceived slowdown in performance. NatTable has been used in production for years in financial systems, but has also been leveraged in many other application domains.</p>

<h2>Scope</h2>

<p>NatTable aims to provide a powerful and flexible data grid implementation for applications with complex data grid requirements. The goal is to provide a rich set of useful grid functionality, and also to provide a framework that allows these features to be created and composed to meet users’ specific needs. Another key goal for NatTable is to provide a performant grid implementation even when displaying very large data sets and rapidly changing data.</p>

<p>NatTable will integrate with established Eclipse technologies where appropriate. It is designed to work well within an RCP application context but it has no dependencies on RCP and will also work well in a standalone SWT environment.</p>

<h2>Description</h2>

<p>NatTable is a custom drawn SWT widget. It is distributed as a core OSGi bundle with a minimal set of dependencies, and one or more optional OSGi bundles for various extensions. Currently there are extension bundles that provide integration with GlazedLists and export functionality via Apache Poi.</p>
<p>NatTable is designed around the concept of layers and layer transformations. A layer is a two dimensional array of cells, and a grid is composed by assembling layers together in various ways.</p>
<p>A layer may be stacked on top of another layer and provide a transformed view of the cells in the underlying layer to the outside world. For example, column reordering, hiding, and viewport scrolling are implemented in this way.</p>
<p>Layers may also be arranged next to each other. The column header, row header, corner and body regions of a standard data grid are implemented this way.</p>
<img src="NatTable_Architecture.png">
<p>NatTable provides a mechanism for commands to be propagated down the layer stack, with each layer in the layer stack having the option of intercepting and processing the command. There is also a complementary mechanism for events to be fired up the layer stack and handled by layers along the way.</p>
<p>Virtually everything in NatTable is designed to be configurable: key/mouse bindings, cell painters, cell editors, validators, display converters, etc. Configurable elements can be applied globally or bound to particular grid regions, rows, columns, or individual cells. These bindings can also be dynamically specified. This allows you to do things like highlight rows with errors, flash data values that have changed, apply custom validation to particular data types, etc.</p>
<p>NatTable can easily be configured to acquire its backing data from any data source that implements its data provider API:</p>
<pre>
	public interface IDataProvider {
	   public Object getDataValue(int columnIndex, int rowIndex);
	   public void setDataValue(int columnIndex, int rowIndex, Object newValue);  // optional
	   public int getColumnCount();
	   public int getRowCount();
	}
</pre>
<p>In addition to being extremely simple to implement, this API allows NatTable to ask for only the data that it needs. This is key to allowing NatTable to perform well for extremely large data sets.</p>

<p>The following screenshot shows a NatTable instance with many features enabled, including:</p>
<ul>
	<li>Collapsible column groups</li>
	<li>Sorting, filtering</li>
	<li>Frozen columns/rows</li>
	<li>Custom date, numeric formatting, alignment, color</li>
	<li>Selected region and highlighting of rows/columns with selected cells</li>
</ul>
<img src="NatTable screenshot.png">

<h2>Why Eclipse?</h2> 

<p>NatTable will benefit the Eclipse community by providing a solution for SWT applications with sophisticated data grid visualization requirements. Eclipse is an ideal community for NatTable because it is the hub of SWT activity. Being part of Nebula will make it much easier for clients to discover NatTable and help to establish better alignment and integration with the other widgets in the SWT/Nebula catalog.</p>

<h2>Initial Contribution</h2>

<p>The initial contribution will consist of EPL-licensed code from the <a href="http://nattable.org">NatTable</a> project.</p>
<p>The NatTable Core depends on the following open source modules:</p>
<ul>
	<li>SWT (EPL)</li>
	<li>JFace (EPL)</li>
	<li>Eclipse Core Commands (EPL)</li>
	<li>Eclipse Core Runtime (EPL)</li>
	<li>Apache Commons-Lang (Apache License V2.0)</li>
	<li>Apache Commons-Logging (Apache License V2.0)</li>
</ul>
<p>The optional NatTable GlazedLists extension has an additional dependency on:</p>
<ul>
	<li>GlazedLists (LGPL and MPL, see <a href="http://www.glazedlists.com">Homepage</a>)</li>
</ul>
<p>The optional NatTable Poi extension has an additional dependency on:</p>
<ul>
	<li>Apache Poi (Apache License V2.0)</li>
</ul>
 
<h2>Committers</h2>

<p>The following individuals are proposed as initial committers to the project:</p>

<dl>
	<dt>Edwin Park, Mecha1 Industries - Project lead</dt>
	<dt>Matt Biggs, Gresham Computing</dt>
	<dt>Stefan Bolton, Gresham Computing</dt>
	<dt>Justin Dolezy, Digital Software Technology Ltd</dt>
	<dt>Dirk Fauth, BeOne</dt>
	<dt>James Hunt, Gresham Computing</dt>
	<dt>Stephan Wahlbrink, Independent Developer</dt>
</dl>

<p>We welcome additional committers and contributions.</p>

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Andrew Overholt</li>
	<li>Kim Moir</li>
</ul>

<h2>Project Scheduling</h2>

<p>Q1 - 2012: Code contribution and migration to Eclipse</p>

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>13-Jan-2012</td>
		<td>Architecture Council mentors added.</td>
	</tr>
	<tr>
		<td>14-Dec-2011</td>
		<td>Document created</td>
	</tr>
</table>
</body>
</html>