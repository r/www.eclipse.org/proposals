<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Financial Platform Proposal";
$pageKeywords	= "Financial, Credit Management, Market Platform, Business Solution";
$pageAuthor		= "knowis AG, WeigleWilczek GmbH";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Financial Platform</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Financial Platform");
?>


<h2>Table of Content</h2>
<ol>
<li><a href="#intro">Introduction</a></li>
<li><a href="#over">Overview</a></li>
<li><a href="#vision">Vision</a></li>
<li><a href="#scope">Scope</a></li>
<li><a href="#structure">Functional Structure</a></li>
<li><a href="#plan">Tentative Plan</a></li>
<li><a href="#liaison">Liaison with Industry Working Groups</a></li>
<li><a href="#integration">Integration Points with other Eclipse Projects</a></li>
<li><a href="#contrib">Code Contributions</a></li>
<li><a href="#other">Other Open Source projects used in current code</a></li>
<li><a href="#orga">Organization</a></li>
<li><a href="#interested">Interested Parties</a></li>
</ol>

<h2><a name="intro">Introduction</a></h2>
<p>knowis AG and WeigleWilczek GmbH are proud to propose the Eclipse <strong><em>Financial Platform</em></strong> project as an open source project under the Eclipse Technology Project.</p>
<p>This proposal:</p>
<ul>
    <li>is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and is written to declare its intent and scope.</li>
    <li>is written to solicit additional participation and input from the Eclipse community.</li>
</ul>
<p>The project <em>Financial Platform</em> will contain other sub-projects like <em>Financial Platform/Common</Em> or <em>Financial Platform/Credit Management</em>.
Initial Code-Contribution is made from knowis.</p>
<p>You are invited to comment and/or join this project or build another sub-project in <em>Financial Platform</em>. Please send all the feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.financial-platform">financial-platform newsgroup</a>.</p>

<h2><a name="over">Overview</a></h2>
<p>The goal of the <em>Financial Platform</em> project is to extend the Eclipse platform to create an open source framework and common infrastructure for interoperable and extensible financial systems.</p>
<div><img src="structure.png" alt="Initial Structure of Financial Platform" width="500" height="110" style="border:none;"/></div>
<p>The <em>Financial Platform</em> project proposal includes also more generic sub-projects, such as <em>Financial Platform/Common</em> and <em>Financial Platform/Credit Management</em>.</p>
<h3>Sub-Project: Financial Platform/Common</h3>
<p>The <em>Financial Platform/Common</em> will develop common tools, concepts and services. All concepts and tools will be made extensible and adaptable for national and international requirements.</p>
<h3>Sub-Project: Financial Platform/Credit Management</h3>
<p>The goal of <em>Financial Platform/Credit Management</em> is to extend the <em>Financial Platform</em> project to create business-components in the field of credit- and back-office processing as well as to provide continuous business processes support for financial organizations.</p>
<p>The <em>Financial Platform/Credit Management</em> will develop banking-specific tools, concepts and services. All concepts and tools will fit in with the <em>Financial Platform</em> project and will be extensible and adaptable for national and international requirements. </p>
<div style="margin-top:20px;" />
<p>Client development will be based on Eclipse RCP and server components will be based on the EQUINOX OSGI together with the core Eclipse concepts such as plug-in-based extensibility. The results of <em>Financial Platform</em> can be used as base-components for other, external parties and projects.</p>
<p>The essential functional elements of <em>Financial Platform V 1.0</em> are well understood by domain experts. While there is significant engineering effort required we feel that the work plan as proposed here can be implemented given sufficient resources, and does not involve an unrealistic level of technical risk.</p>
<p>There is no overlap with existing projects. We are aware of no other efforts to build a platform for constructing common financial applications. Consequently, we expect it will take both time and effort to grow a <em>Financial Platform</em> community within the financial industry.</p>
<p>We are optimistic that there will be adequate growth in the team within the first year after passing the creation review and being established as an Eclipse Project. Our development plans are deliberately open ended so that we can employ these additional resources effectively as they become available. If we are wrong in our optimism, our development plan can still succeed, but it clearly will take longer to reach its goals.</p>

<h2><a name="vision">Vision</a></h2>
<p>The financial industry has experienced a continuous standardization of its main products since the early 90�s. <em>Financial Platform's</em> goal is to deliver extensible and replaceable open source common business functionality. Several financial open source tools and mathematic libraries can benefit from this platform as an integration point.</p>
<p><em>Financial Platform</em> project will gain interoperability (between <em>Financial Platform</em> components), ensure an easy integration and will provide the environment for knowledge exchange.</p>
<p>We believe there is the possibility of establishing business components, financial tools and functions, supporting workflows for the financial business. Together with a process for customization to adapt to national standards and requirements <em>Financial Platform</em> allows segmentation from commercial products.</p>
<p>We consider there is a chance to found an extensible vendor-neutral platform that includes common components and basic tools and functions which all support workflows for financial business.</p>

<h2><a name="scope">Scope</a></h2>
<p>The scope of the project and all of its sub-projects is business-based; <em>Financial Platform</em> will not develop generic middleware (process server functionality, business rules engine, ESB, etc.). All integration-points are based on available (open-source) products and common standards.</p>
<ul>
<li><em>Financial Platform/Common</em> will develop a basic framework, extensible with other sophisticated financial components such as credit related functions or risk management.</li>
<li><em>Financial Platform/Credit Management</em> will develop extensible banking components and credit-management related tools.</li>
</ul>
<p>knowis will initial contribute code of field-tested components that apply to common and/ or specific financial requirements in an extensible framework. Latest results from academic research and legal requirements can be more easily adopted while using this framework.</p>
<div><img src="scope.png" alt="Scope of Financial Platform" width="500" height="548" style="border:none" /></div>

<h2><a name="structure">Functional Structure</a></h2>
<h3>Presentation</h3>
<p>All UI components will be developed in an integrated workplace using Eclipse-RCP technology.</p>
<h3>Business Logic</h3>
<p>The project-core of the <em>Financial Platform</em> can be subdivided into two major parts:</p>
<ul>
<li>A highly flexible, specialist banking meta model makes it possible to replicate real world situations easily and completely.</li>
<li>An abstract controlling layer provides specialist banking operations on top of the Meta model. These can be reused by external applications as banking services.</li>
</ul>
<h3>Persistence and data integration</h3>
<p>Access to back-end systems, interfaces and services will be completely encapsulated within the data and integration layer. This allows each banking entity to modify the data link to its back-end systems or databases in accordance with customer requirements. Open standards (e.g. web services, XML) can be employed as well as proprietary communication protocols.</p>

<h2><a name="plan">Tentative Plan</a></h2>
<p>We plan to follow a milestone based delivery schedule with incremental progress visible in each milestone. Here is a rough outline of the time line we expect to follow for milestones:</p>
<div><img src="plan.png" alt="Tentative Plan of Financial Platform" width="500" height="459" style="border:none" /></div>

<h2><a name="liaison">Liaison with Industry Working Groups</a></h2>
<ul><li>Open Source Business Foundation</li></ul>

<h2><a name="integration">Integration Points with other Eclipse Projects</a></h2>
<ul>
<li><strong>Equinox</strong> is used to implement both client-side and server-side OSGi services as evolved by the OSGi Enterprise Expert Group.</li>
<li><strong>Riena</strong> is used for client-server communication</li>
<li><strong>RCP</strong> is used for client side</li>
<li><strong>BIRT</strong> is expected to be used for Business Reporting</li>
<li><strong>DTP</strong> is expected to be used for database definition</li>
<li><strong>EclipseLink</strong> is expected to be used for ORM</li>
<li><strong>Higgins</strong> is expected to be used for identity, profile, and relationship information across multiple data sources and protocols</li>
<li><strong>Swordfish</strong> is expected to be used for SOA-enabling</li>
</ul>

<h2><a name="contrib">Code Contributions</a></h2>
<ul>
<li>knowis AG</li>
</ul>

<h2><a name="other">Other Open Source projects used in current code</a></h2>
<ul>
<li>Hibernate</li>
<li>JBoss jBPM</li>
<li>Drools</li>
<li>JasperReports</li>
<li>Spring Framework</li>
<li>Joda Time</li>
</ul>

<h2><a name="orga">Organization</a></h2>
<div style="margin-bottom:20px;"><img src="organisation.png" alt="Organization of Financial Platform" width="500" height="539" style="border:none" /></div>
<table style="max-width:800px;" border="1" rules="rows" cellspacing="5" cellpadding="5">
<tr><td>Alexey Aristov</td><td>Alexey is a senior architect with WeigleWilczek, building enterprise applications for more then 10 years. Alexey is co-project lead and will work on FP Common architecture.</td></tr>
<tr><td>Gerald Gassner</td><td>Gerald is in software development for financial institutions for more then 10 years. Gerald is co-project lead and will work on FP Credit's technical architecture.</td></tr>
<tr><td>Frank Mildner</td><td>Frank is senior software engineer with knowis, building enterprise applications for more then 15 years. Frank is responsible for the design and realization for different modules in FP/Common and FP/Credit.</td></tr>
<tr><td>Sergey Vasiljev</td><td>Sergey is an Independent IT-Consultant. Working for years in the field of Java-EE, he is responsible for the design and realization of different modules in FP.</td></tr>
<tr><td>Jan Blankenhorn</td><td>Jan Blankenhorn is software developer with WeigleWilczek, building enterprise applications for more then 5 years. He is responsible for creating a testing framework for different modules.</td></tr>
<tr><td>Christian Sternkopf</td><td>Christian is senior product manager with knowis. He has been working for several financial institutes for 10 years, and is currently responsible for the product strategy of FP/Credit.</td></tr>
<tr><td>Ralph Schreder</td><td>Ralph is senior software engineer with knowis, building enterprise applications for financial institutions for more then 15 years. Ralph is responsible for different modules in FP/Common and FP/Credit.</td></tr>
<tr><td>Johannes Bossle</td><td>Johannes is solution architect with knowis, working with Eclipse for more than 5 years. He is responsible for technical architecture in FP/Credit and integration with other FP projects.</td></tr>
<tr><td>Herbert Riedl</td><td>Herbert is consultant with knowis. He has been working for several financial institutions for about 5 years, and is responsible for different modules in FP/Common and FP/Credit.</td></tr>
</table>
<p>Other participants are more than welcome and being actively sought.</p>

<h2><a name="interested">Interested Parties</a></h2>
<ul>
<li>knowis AG</li>
<li>WeigleWilczek GmbH</li>
<li>Sopera</li>
<li>Actuate</li>
<li>OSBF</li>
</ul>


      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
