<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">


<h1>Supplemental Widgets for SWT (Nebula)</h1><br>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Supplement Widgets for SWT (Nebula)");
?>
</p>

<h2>Introduction</h2>
<p>The Supplemental Widgets for SWT (Nebula) is a proposed open source
project under the <a href="/technology/">Technology Project</a>.</p>
<p>This proposal is in the Project Proposal Phase (as defined in the
<a href="/projects/dev_process/">
Eclipse Development Process document</a>) and is written to declare its intent
and scope. This proposal is written to solicit additional participation and
input from the Eclipse community. You are invited to comment on and/or join the
project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology</a> newsgroup.&nbsp;

<h2>Background</h2>
Through use of both Eclipse RCP and SWT, the development of applications based upon the Eclipse platform is growing.  These developers will require new and custom widgets to use within their applications.  The SWT team does an excellent job of producing and maintaining SWT, but there is an endless list of new widget requests and new feature requests.  This project will attempt to answer some of those requests, while working closely with the main SWT team.

<h2>Description</h2>
		<p align="left">This project will provide new custom/emulated widgets built upon SWT.  The project will work as a proving ground for these new widgets.  If/when each new widget is deemed useful and complete, and is approved by the main SWT team, the widget will graduate into the main SWT project.</p>
		<p>
		An initial code contribution will be made to Nebula.  Chris Gross is donating his personal code from the SWTPlus project.  SWTPlus was conceived provide custom SWT widgets for SWT/RCP developers.  The initial code contribution will include:
		<ul>
			<li>An expandable group widget with 3 existing drawing strategies</li>
			<li>hyperlink widget</li>
     		<li>combo widget with 4 drawing strategies includes a Tree, Table, and Color combo</li>
     		<li>list widget with 3 drawing strategies</li>
			<li>a tab-like shelf widget with 2 drawing strategies</li>
			<li>an attachable auto complete</li>
		</ul>

A web start demo of these widgets can be viewed at <a href='http://www.swtplus.com/jws2/swtplusExamples2.jnlp'>http://www.swtplus.com/jws2/swtplusExamples2.jnlp</a>.
<p>
Future widgets under consideration include:
		<ul>
			<li>Table widget</li>
			<li>Calendar widget</li>
			<li>Property sheet widget</li>
		</ul>

<h2>Principles</h2>
The project will be based upon the following principles:
		<ul>
			<li>Flexibility (the ability to override default drawing/painting behavior)</li>
			<li>SWT-ish API (following SWT paradigms and idioms)</li>
			<li>Fewer components, higher quality</li>
		</ul>

<h2>Concerns</h2>
There is a potential for overlap between Nebula and the main SWT development.  The Nebula project will endeavor to minimize this overlap.
Nebula's focus is not on widgets offered by the native operating systems, but rather higher level custom components.  Only in rare situations, where
there is a significant demand and it does not overlap with any existing or upcoming SWT work, will Nebula develop counterparts to existing SWT/native widgets.
<p>
Additionally, the project committers will work closely with the SWT team to ensure that the widgets follow the Eclipse and SWT vision in order to be approved for inclusion into the main SWT project.

<h2>Organization</h2>

			<p align="left">We propose this project should be undertaken as a sub-project within the Eclipse
            Technology Project.  It is intended to augment the existing SWT project and has the potential to be
            utilized within the Eclipse Platform itself.</p>


		<h2 align="left">Proposed project lead and initial committers</h3>
		<ul>

			<li>
			Chris Gross (IBM)</li>
			<li>Veronika Irvine (IBM, existing SWT committer)</li>

		<li>
					Additional committers to be picked up as project progresses.</li>
		</ul>

			<h2 align="left">Code Contributions</h3>
			<p align="left">Initial code contribution from Chris Gross.<br>

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
