<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Eclipse Git Team Provider (EGit)</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("EGit");
?>


<h1>Introduction</h1>

<p>
The EGit project is a proposed open source project under the Eclipse Technology Project.
</p>
<p>
According to the Eclipse Development Process, this proposal is intended to declare the 
intent and scope of the EGit project as well as to gather input from the Eclipse 
community. You are invited to comment on and/or join the project. Please send 
all feedback to 
the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.egit">http://www.eclipse.org/newsportal/thread.php?group=eclipse.egit</a> newsgroup. 
</p>
<p>
EGit is an Eclipse Team provider for <a href="http://git.or.cz">Git</a>. Git is the version control system originally developed for the Linux kernel by Linus Torvalds. One of Git's strength is its ability to support distributed version tracking as well as more centralized schemes, in contrast with traditional SCM tools that offer only a centralized model (such as CVS and Subversion) . Git is also very fast. Git usage has evolved beyond the Linux kernel team and is now used by many other projects -- both for open source and commercial development -- as a fast and distributed general purpose Source Code Management (SCM) tool.
</p>
<p>
Git is a distributed SCM, which means every developer has a full copy of all history of every revision of the code, making queries against the history very fast and versatile. The storage model also means most operations are ten to a hundred times faster than with traditional centralized SCM such as CVS or Subversion. The integrity of the code is guaranteed through the use of secure hashes on the content and history, plus optional GPG/PGP key signing of tags. 
</p>

<h1>Background</h1>
<p>
Traditional source code control systems have been based around a single centralized server housing all of the project's revision history and assets. Historically this has worked well for smaller projects, especially when all project contributors have access to the same computer system and are located in the same physical workplace. Distributed open source projects (and commercial entities or virtual companies) have broken this model, making it much more difficult for contributors to participate. A centralized model promotes two classes of contributors: "blessed people" (those who have access to make changes to the project and use the source code control system) and "everyone else" (the rest of the world). Moving between classes is painful for everyone involved, and in many cases moving from "everyone else" to "blessed people" requires months of proving the contributor is trustworthy enough.
</p>
<p>
Multiple distributed version control systems (DVCS) have recently developed to fully support the truly distributed nature of modern open source projects, in particular large scale efforts such as those supported by Eclipse, Mozilla, KDE, Open Solaris, and the Linux kernel. Git, Mercurial, Monotone, Bazaar and darcs are all popular implementations of the DVCS concept, however their implementation details differ significantly.
</p>
<p>
True to their name, DVCS implementations eliminate the centralized server and place all project participants on an equal footing by providing them with identical capabilities. Brand new project contributors have access to the same tools and historical data as long-term trusted contributors, making it possible for new contributors to participate on an equal footing with no risk to the overall project. Most projects using DVCS tools flow changes from newer contributors to trusted contributors, arranging a loose hierarchy of review.
</p>
<p>
DVCS tools tend to perform better then their centralized counterparts, as network latency is only a factor when exchanging batches of new revisions. In a DVCS the most commonly used operations such as recording changes, file comparison, history inspection, branch creation and branch merging are handled entirely offline using locally stored information, allowing these operations to complete as quickly as the disk buffer cache and CPU will permit. Offline operation can be very useful in distributed open source projects where not all participants have large network bandwidth with low latency to connect them to the project's central server(s). For some participants, being able to work offline may be a necessity for them to contribute, due to limited availability (or high cost) of Internet access. 
</p>
<p>
Finally, the implications of adopting a distributed SCM approach can bring significant benefits, especially for large open source projects such as Eclipse, and can help lower the barrier to entry for new contributors and make source code experimentation easier.
</p>

<h1>Scope</h1>
<p>
The project will implement Eclipse tooling for the JGit Java implementation of Git. Specific attention will be focused on performance.  The EGit plugin meta data shall be fully compatible with the meta data created by the native Git version, so both can be used on the same checkout.
</p>
<p>
Beyond the Git support implementation, the project will work towards defining new essential extensions to the Eclipse core platform Team framework to account for the specific issues and features provided by distributed version control systems.
</p>

<h1>Roadmap</h1>
<p>
<ul>
	<li>2009 Q2: Release 0.5, initial contribution from 0.5</li>
	<li>2009 Q3: Release 1.0 M1</li>
	<li>2009 Q4: Release 1.0 M2</li>
	<li>2010 Q1: Release 1.0 M3</li>
	<li>2010 Q2: Release 1.0</li>
</ul>
</p>

<h1>Licensing and IP</h1>
<p>
A point of note is that EGit depends on JGit, a pure Java implementation of Git. JGit was originally licensed under a combination of GPL and LGPL, but is now made available under a BSD style license. The developers of JGit are also the original developers of EGit. Git itself is licensed under the GPL V2, but JGit is a clean implementation of the core Git data structures and as such has received the approval of the core Git maintainers to be licensed under any open source license the authors wish. Therefore the JGit authors have elected to license JGit under a BSD style license which is compatible with the Eclipse Public License, and most other open source licenses. As part of the original code contribution review, JGit will be also submitted for approval per the Eclipse development process. 
</p>

<h1>Organization</h1>

<h2>Mentors</h2>
<p>
The following members of the Eclipse Architecture Council have kindly agreed to mentor this project: 
<ul>
	<li>Chris Aniszczyk (EclipseSource)</li>
	<li>Andrew Overholt (Red Hat)</li>
</ul>

</p>

<h2>Initial Committers</h2>
<p>
<ul>
	<li>Robin Rosenberg (Dewire AB)</li>
	<li>Shawn Pearce (Google)</li>
	<li>Gunnar Wagenknecht</li>
	<li>Mik Kersten (Tasktop)</li>
	<li>Matthias Sohn (SAP AG)</li>
 	<li>Christian Halstrick (SAP AG)</li>
 	<li>Stefan Lay (SAP AG)</li>
	
</ul>
</p>

<h2>Interested Parties</h2>
<p>
<ul>
	<li>Ismael Juma</li>
	<li>Neil Bartlett</li>
	<li>Chris Aniszczyk (EclipseSource)</li>
	<li>Marek Zawirski</li>
	<li>C&eacute;dric Brun (Obeo)</li>
	<li>Ketan Padegaonkar (Thoughtworks)</li>
	<li>Tor-Arne Vestb&oslash;</li>
	<li>Martin Oberhuber (Wind River)</li>
	<li>Gunnar Wagenknecht</li>
	<li>Markus Knauer (EclipseSource)</li>
	<li>Antoine Toulme</li>
	<li>Daniel Spiewak</li>
	<li>Manuel Woelker (EclipseSource)</li>
	<li>Markus Alexander Kuppe</li>
	<li>Thomas Hallgren (Cloudsmith)</li>
	<li>Ahti Kitsik</li>
	<li>Alex Blewitt</li>
</ul>
</p>


      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
