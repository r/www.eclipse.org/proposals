<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>
<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Requirements Modeling Framework (RMF)</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<p>The "Requirements Modeling Framework" (RMF) project is a proposed open source project under the <a href="http://www.eclipse.org/modeling/mdt/">Model Development Tools Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<p>The vision is to have at least one clean-room implementation of the <a href="http://www.omg.org/spec/ReqIF/">OMG ReqIF</a> standard in form of an EMF model and some rudimentary tooling to edit these models. The idea is to implement the standard so that it is compatible with Eclipse technologies like GMF, Xpand, Acceleo, Sphinx,  etc. and other key technologies like CDO.</p>

<h2>Background</h2>

<p>The Eclipse ecosystem provides a number of  projects to support software development and systems engineering. However, in the open source community, one important aspect of the engineering process is very much neglected: requirements management, consisting of a number of sub-disciplines including requirements capturing, requirements engineering, requirements traceability, change management and product line engineering, to name just a few.</p>

<p>The goal of RMF is to provide the technical basis for open source projects that implement tools for requirements management. The conditions for the inception of such a project are perfect: Until now, all tools for requirements engineering suffered from the lack of a standard for the exchange and storage of requirements. Each tool provider invented his own method and meta-model for requirements, thereby limiting the common user base and the possibility for exchange between tools.</p>

<p>The OMG just released the Requirements Interchange Format (ReqIF), an XML-based data structure for exchanging requirements.  The first draft of this standard with the name RIF was created in 2004, and various requirements tools (commercial and otherwise) already support it to some degree.  Currently there are three actively used versions of the standard: RIF 1.1a, RIF 1.2 and ReqIF 1.0.1.</p>

<p>This open standard could have as much impact on requirements structuring as the UML had on modeling. The implementation of the ReqIF standard as an Eclipse project could similarly be as important for the requirements community as was the implementation of UML2  in Ecore for the modeling community by paving the way for such tools as Topcased and Papyrus MDT.</p>

<p>Providing such a project under the Eclipse umbrella would offer a possibility for many projects that are involved in requirements management to find a common implementation of the standard. It would push Eclipse in to phases of the development process where it is currently under-represented.</p>


<h2>Scope</h2>

<p>The RMF project's focus is the creation of libraries and tools for working with ReqIF-based requirements.  The objective is to provide the community with a solid implementation of the standard upon which various tools can be built.  RMF will provide a means for data exchange between tools, an EMF-based data model, infrastructure tooling and a user interface.</p>

<p>RMF will not provide support for Requirements Management.  Instead, it is expected that users will use specialized tools or work with the available Eclipse tooling (EMF Compare, version control integration, etc.). Generic or specific parts of the tooling can be hosted as part of the RMF project.</p>

<h2>Description</h2>

<p>The following diagram depicts the architecture of the current development and indicates which elements will be part of the initial contribution:</p>

<center>
<img src="overview.png"/>
</center>

<p>We created an EMF-based implementation of the ReqIF core that supports persistence using the RIF XML schema. Further, we created a GUI for capturing requirements.</p>

<p>These contributions have their origins in research projects, where they are actively used.  In particular, these research projects already produced extensions, demonstrating the value of the platform.</p>

 
<h2>Why Eclipse?</h2> 

<p>The Eclipse ecosystem will benefit from an implementation of a requirements standard to cover more aspects of system development.  Currently, modelling is covered well on the low level (EMF, TMF, CDT, etc.) and high level (UML, SysML, etc.).  Adding the domain of requirements capturing would extend the coverage.  To promote it, we need not only a standard, but also a common implementation that tools build upon.</p>

<p>Being an Eclipse project will draw the interest of more parties to the project. The implementation of a standard benefits greatly from the participation of many parties (improvement of quality, reduction of cost). In addition, long-term support through the participation of many parties is essential for many domains.</p>


<h2>Initial Contribution</h2>

The initial contribution will consist of an EMF-based ReqIF Core that supports persistence, and a tool front-end for working with ReqIF-data.

<h3>ReqIF Core</h3>

<p>The first initial contribution will come from <a href="http://www.itemis.com">itemis</a> and will include our implementation of the ReqIF and RIF metamodels as .ecore models, including special (de)serializers that map the EMF-models to a ReqIF conforming standard. The model and (de)serializers are already available at itemis and need only be provided.</p>

<p>The initial contribution has been internally tested. A ReqIF export from production data from the automotive domain from a commercial tool has been imported into Eclipse and exported again, keeping all the structural data, with the exception of ReqIFs XHTML extensions (see table below).</p>

<p>They have been implemented according to the specification, but since ReqIF is a new standard, no extensive tests with ReqIF files coming from other sources / other tools have been made.</p>

<center>
<img src="table1.png"/>
</center>

<h3>GUI (known as ProR)</h3>

<p>The second contribution will come from the <a href="http://www.stups.uni-duesseldorf.de/">University of D&uuml;sseldorf</a> and will include a <a href="http://pror.org">front end that facilitates working with ReqIF data (ProR)</a>.  While ReqIF data could also be edited with the default EMF editor, this is not even remotely practical: a tree view of the requirements, with the details shown in the property view, doesn't allow users to efficiently navigate requirements or get an overview of what's there.</p>

<p>The GUI allows users to arrange only those requirements attributes that they care about in a grid view and implements a number of shortcuts for frequent operations that actually consist of a number of model transformations.  Further, it contains an extension mechanism that allows integration with other EMF-based tools and supports custom rendering.
The GUI currently only support RIF 1.2, and not all RIF features are implemented yet.</p>

<p>The GUI so far has been developed under the name ProR.  This name, including the pror.org property, will be part of the contribution to the project (see legal issues below).</p>

<center>
<img src="pror.png"/>
</center>

<h2>Legal Issues</h2>

<p>All contributions will be distributed under the Eclipse Public License.  The ReqIF metamodel has been fully developed by itemis. The GUI has been developed by University of D&uuml;sseldorf with changes from itemis.</p>
 
<p>The GUI development to date has been branded as ProR, supported by the pror.org website.  The rights to the brand and the name reside with Michael Jastram, who is willing to agree to the <a href="http://www.eclipse.org/legal/Trademark_Transfer_Agreement.pdf">Eclipse Trademark Transfer Agreement</a>.</p>

<h2>Related Projects</h2>

<p>Sphinx: Requirements models tend to grow quite large in commercial projects. Using Sphinx will improve the performance and scalability. The current implementation is not yet based on / integrated with Sphinx.</p>

<p>EMF Compare: Since the RMF is based on EMF, EMF Compare could be a key technology for the comparison of requirements documents.</p>

<p>M2T/BIRT: With the new Indigo Release, BIRT includes an EMF adapter. BIRT could be one of the technlogies used to create documents out of requirement models. The M2T technologies (Xpand, Acceleo) are possible technologies as well.</p>


<h2>Committers</h2>

<p>The initial committers will deliver the initial release. Funding can be provided through the ITEA2 research project until June 2012 and through the Deploy project until February 2012.  Funding may be available thereafter.</p>

<p>The following individuals are proposed as initial committers to the project:</p>

<dl>
	<dt>Nirmal Sasidharan, itemis - RIF Core (Project Lead)</dt>
		<dd>Nirmal Sasidharan is a developer and software architect at itemis. His interests are in Model Driven Software Development (MDSD) based on Eclipse platform. He has over 10 years of software development experience in different domains such as Automotive, Aerospace and Telecommunication. Before joining itemis, Nirmal Sasidharan has worked several years with Robert Bosch architecting tools. He works and lives in Stuttgart, Germany.
		</dd>
	<dt>Michael Jastram, Formal Mind GmbH - GUI</dt>
		<dd>Michael is coauthor of the German Book "Eclipse Rich Client Platform" and has been working with Java technologies as developer and architect since 1996.  He is currently pursuing a Ph.D. in Computer Science at the University of D&uuml;sseldorf.  He is founder and managing director of Formal Mind GmbH.  He also founded and runs the local Java User Group (rheinjug).  He holds a Master degree from M.I.T.</dd>
	<dt>Lukas Ladenberger, University of D&uuml;sseldorf - GUI</dt>
		<dd>Lukas is currently a Ph.D. student in Computer Science at the University of D&uuml;sseldorf and an employee at Formal Mind GmbH.  He has been working with Java and Eclipse as developer since 2004. He is also an active member of the local Java User Group (rheinjug). </dd>
	<dt>Andreas Graf, itemis</dt>
		<dd>Andreas Graf is a  Business Development Manager at the automotive division of itemis. He is an expert in MDSD for automotive software. Apart from his managerial role at itemis, Andreas is writing tools based on Eclipse platform. Before joining itemis, he has worked several years with BMW in the areas of process definition, ECU software development and Software logistics.</dd>
</dl>

<p>We welcome additional committers and contributions.</p>

<h2>Mentors</h2>

<p>The following Architecture Council member will mentor this project:
<ul>
	<li>Ed Merks</li>
<li>Kenn Hussey</li>
</ul>
</p>

<h2>Interested Parties</h2>


<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
    <li><a href="http://www.airbus.com">Airbus</a></li>
    <li><a href="http://atos.net">Atos</a></li>
    <li><a href="http://www.emergn.com">emergn Ltd</a></li>
	<li><a href="http://formalmind.com">Formal Mind</a></li>
	<li><a href="http://www.stups.uni-duesseldorf.de/">Heinrich-Heine University D&uuml;sseldorf</a></li>
	<li><a href="http://www.hood-group.com">HOOD GmbH</a></li>
	<li><a href="http://www.itemis.com">itemis AG</a></li>
	<li><a href="http://www.mks.com">MKS</a></li>
	<li><a href="http://www.modelalchemy.com/">ModelAlchemy Consulting</a></li>
    <li><a href="http://www.obeo.fr/">Obeo</a></li>
 	<li><a href="http://prostep.com">Prostep</a></li>
    <li><a href="http://www.linkedin.com/pub/luis-carlos-moreira-da-costa/b/8aa/558">TCL Software Ltd. (LuisCM)</a></li>
	<li><a href="http://www.eit.h-da.de/">University of Applied Sciences Darmstadt (Prof. Fromm)</a></li>

</ul>

<h2>Project Scheduling</h2>

Initial contribution is anticipated in July or August 2011.

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>29-Jun-2011</td>
		<td>Document created</td>
	</tr>
	<tr>
		<td>25-Jul-2011</td>
		<td>Added more interested parties; provided a link to the ProR website; fixed umlauts.</td>
	</tr>
	<tr>
		<td>26-Jul-2011</td>
		<td>Added even more interested parties; Updated the feature table (XHTML Support)</td>
	</tr>
	<tr>
		<td>27-Jul-2011</td>
		<td>Added one more committer biographies of committers.  Updated the feature table (XHTML and Tool Support)</td>
	</tr>
	<tr>
		<td>28-Jul-2011</td>
		<td>Added information about the transfer of the ProR trademark to the Eclipse Foundation.</td>
	</tr>
</table>
</body>
</html>
