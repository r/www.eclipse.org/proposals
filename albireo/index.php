<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Swing/SWT Integration (Albireo)</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Albireo");
?>


<h2>Introduction</h2>

<p>The Swing/SWT Integration (Albireo) project is a proposed project under the
   <a href="http://www.eclipse.org/technology/">Eclipse Technology Project</a>.</p>

<p>This proposal is in the
   <a
   href="http://www.eclipse.org/projects/dev_process/development_process.php#6_2_2_Proposal">Project
   Proposal Phase</a> (as defined in the
   <a href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse
   Development Process</a> document) and is written to declare its intent and scope. This
   proposal is written to solicit additional participation and input from the Eclipse
   community. You are invited to comment on and/or join the project. Please send all
   feedback to the
   <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.albireo">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.albireo</a>
   newsgroup.</p>

<h2>Background</h2>

<p>

Many adopters of the Eclipse Rich Client Platform (RCP) have a history of developing applications for the AWT and Swing user interface toolkits. These developers often have a rich library of Swing components that have been created over time for use in their applications. These Swing components provide high business value in their specific application domains and have no existing SWT equivalent.It is impractical to expect that such users will be able to convert their Swing component libraries to SWT in their entirety as they begin to adopt the RCP. The component libraries may be very large, and they may not be controlled by the group interested in using the RCP.

<p>

So, in order to eliminate the RCP "barriers of entry" and to provide the ability to incrementally convert Swing component libraries to SWT, these users need a way to include their existing Swing components in their SWT-based RCP applications. The inclusion may take the form of:

<ul>

<li>embedding AWT/Swing components in an SWT composite

<li>embedding SWT composites in an AWT frame

<li>opening a modal Swing dialog from an SWT component

<li>opening a modal SWT/JFace dialog from an AWT/Swing component

</ul>

<p>

Since Eclipse 3.0, SWT has provided basic support for integrating SWT with AWT/Swing in the form of the <a href="http://help.eclipse.org/stable/index.jsp?topic=/org.eclipse.platform.doc.isv/reference/api/org/eclipse/swt/awt/package-summary.html">SWT_AWT bridge</a>. The bridge provides the critical infrastructure necessary to integrate the two toolkits, but it takes a very minimalistic approach. The recent Eclipse Corner <a href="http://www.eclipse.org/articles/article.php?file=Article-Swing-SWT-Integration/index.html">article</a> on Swing/SWT Integration details a number of additional steps that all applications must take in order to create production-quality integrated applications. These steps include:

<ul>

<li>setting the proper platform look and feel

<li>avoiding excessive flicker

<li>arranging proper tab traversal among components from both toolkits

<li>making dialogs modal across both toolkits

<li>handling keystroke contention between the toolkits

<li>working around bugs that have amplified effects when the two toolkits are mixed

</ul>

<p>

Without this extra work, the initial "out-of-the-box" experience with the SWT_AWT bridge can be very disappointing to developers. They see Swing components which look unpolished and out of place in the RCP environment. It's easy to conclude that Swing/SWT integration is not truly possible for production-quality applications. In fact, the <i>Official Eclipse 3.0 FAQs</i> book <a href="http://wiki.eclipse.org/index.php/FAQ_How_do_I_embed_AWT_and_Swing_inside_SWT%3F"> reinforces this concern</a> by claiming the SWT_AWT bridge alone is production-quality only on Windows. Even for those that are aware of the techniques to improve integration, the extra code is tricky to implement and maintain. It requires expertise that is not necessarily available to application developers.

<h2>Description</h2>

The Albireo project will build on the SWT_AWT bridge to provide more complete Swing/SWT integration. It will include the capabilities in the example code from the Swing/SWT Integration article, and it will improve upon these capabilities to support

<ul>

<li>easier operation in multiple event thread environments

<li>more complete multi-platform support (including reduced flicker on Linux/GTK)

<li>management of SWT popups on AWT/Swing components

<li>integration of cursors between the two toolkits

<li>solutions to focus problems on Windows

<li>integration of size (layout) management between the two toolkits.

</ul>

<p>

... and hopefully much more. The end result will be a better "out-of-the-box" experience  and more credibility for the entire notion of Swing/SWT integration. The additional credibility extends both to developers considering integration and to end users who have concerns about applications that mix the two toolkits.

<p>

It will be necessary to implement some portion of this project in platform-specific code. We plan to support at least these platforms:

<ul>

<li>Windows

<li>Linux/GTK

<li>X11/Motif

<li>Mac OS X 10.4

</ul>

<h3>Relationship with other Eclipse Projects</h3>

<p>

Albireo will be built on top of the SWT component of the Eclipse Platform (especially the SWT_AWT bridge). It may also require JFace from the Platform UI component in order to provide integration at higher levels (such as actions).

<p>

Similar to the Nebula project, Albireo will provide custom SWT widgets. Unlike the emulated widgets in the Nebula project, these widgets will need to be partially implemented in a platform-specific way.

<h2>Organization</h2>

We propose this project as a sub-project the Eclipse Technology Project. Early on, there will be need for the experimentation that an incubation status would allow. As the project matures, it might make sense to consider a merge somewhere into the Eclipse Platform, but only if:

<ul>

<li>there is sufficient interest

<li>there is a high-quality implementation for all of the necessary platforms

</ul>

<h3 align="left">Proposed initial committers</h3>

<ul>

<li>Bruno Haible - ILOG S.A.</li>

<li>Gordon Hirsch - SAS Institute, Inc.</li>

</ul>

<h3 align="left">Interested parties</h3>

<p>The following companies have expressed interest in this project. Key contacts listed.<ul>

<li>Genuitec, Inc. - Todd Williams (todd@genuitec.com)</li>

<li>Instantiations, Inc. - Eric Clayberg (clayberg@instantiations.com)</li>

</ul>

<h3 align="left">Code Contributions</h3>

<p>

SAS Institute, Inc. contributed its Swing/SWT integration code as part of the Eclipse Swing/SWT Integration article. This code would form part of the initial code contribution.

<p>

ILOG S.A. will contribute its Swing/SWT integration code to the effort <b>(tentative)</b>.

</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
