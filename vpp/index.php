<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">


<h1>Virtual Prototyping Platform (VPP)</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("VPP");
?>
<h2>Please note that the VPP project will become part of a DSDP Incubator project in the near future.</h2>
 <h1>Introduction</h1>

  The <i>Virtual Prototyping Platform (VPP)</i> project is a proposed open
  source project under the Device Software Development Platform
  Project. VPP is intended to collect a number of smaller technologies
  together that, when combined, will enable Eclipse to be used for the tasks
  associated with constructing, debugging, visualizing, analyzing, and
  using (models of) systems constructed from components that may be
  based on hardware or software, or a combination of both.

  <p>This proposal is in the Project Proposal Phase (as defined in the <a
  href="{field{*fldinst{HYPERLINK "http://www.eclipse.org/projects/dev_process/"}}{fldrslt http://www.eclipse.org/projects/dev_process/}}">Eclipse
  Development Process document</a>) and is written to declare its
  intent and scope. This proposal is written to solicit additional
  participation and input from the Eclipse community. You are invited
  to comment on and/or join the project. Please send all feedback to
  the <a
  href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.dsdp.vpp">http://www.eclipse.org/newsportal/thread.php?group=eclipse.dsdp.vpp</a> newsgroup.</p>

  <h1>Background</h1>

  Today, complex systems are no longer designed monolithically, but
  rather as amalgamations (at the system level) of complex
  interconnected components, each of which may consist of both
  hardware and hardware dependent software components.

  In order to design such systems, often models are used. They can be
  used to explore potential implementations (in terms of hardware,
  software, or the division between hardware and software), enable
  early software development on top of the system "platform," or
  indeed as a reference model that can be used for verification. The
  models can encompass both hardware and software components.

  Models can be written to be more or less faithful to the final
  implementation, both in terms of their functionality, and in terms
  of the "meta" information that they model (for instance execution
  time or power consumption).

  <p>Functional accuracy can range markedly, from very little (pure
  traffic generators), through partially accurate models (which may
  not model some specific features of a hardware implementation, which
  may or may not be visible to a programmer), to models that are
  totally functionally accurate. Further more, models may be
  specifically designed to "stress" software components, often for the
  purpose of assisting in debug, and ensuring software correctness.

  This level of activity is often referred to as the "Electronic
  System Level" or ESL.

  ESL Design activity often occurs prior to or at the point of making
  a split between hardware and software, which is often recognized as
  a highly influential point in the design process. The wide range of
  uses that ESL models are put to means that there are many "views" of
  them that are appropriate.</p>

  <p>
  These models may be of components destined to for implementation in
  hardware or in software. The models constructed (and debugged) in
  this way then provide a starting point for the development of
  hardware dependent software and the hardware itself. While the
  processor-centric view of a system is addressed by other projects
  within Eclipse, and other projects focus on connections with
  hardware, this project focuses on how to construct, connect, debug,
  analyze, visualize and use models (of virtual prototype components destined for
  implementation in hardware, or software) to Eclipse.

  The language used for the ESL Design activity is predominantly
  (though not exclusively) <a href="http://www.systemc.org">SystemC.</a> 
  SystemC is a language implemented
  as a C++ library, so it shares many of the debug requirements of C++
  itself. However, there are additional views of a SystemC model that
  are useful to the designer (for instance, SystemC keyword
  highlighting, model connectivity diagrams etc). It is expected that
  the VPP will cover other languages over time that are relevant to
  ESL (e.g. System Verilog).

  Once SystemC models are debugged, and executable, they can be run
  either using a freely available SystemC library implementation
  (provided by the Open SystemC Initiative), or one of the many
  proprietary simulators. In either case, a model may wish to interact
  with the user, for a number of reasons:</p>

  <ul>
    <li> Configuration</li>
    <li> Run time Control</li>
    <li> User input</li>
    <li> Visualization (output)</li>

    <li> Analysis and Profiling</li>
  </ul>
  <p>
  In each case, mechanisms will be required in Eclipse to support this
  user interaction.

  <h1>Use Case Scenarios</h1>

  The use cases for VPP can be divided into two main categories, those
  associated with initial model creation, and those associated with
  model execution and analysis.

  In the first category, model creation shares many of the same use
  cases as other relatively complex software designs. As the languages
  (such as SystemC) are increasingly based on C++, many of the
  standard Eclipse plugins can be used. However additional plugins are
  of some use, for instance specific SystemC syntax highlighting, or
  model topology visulisation.  In the second category, the scope of
  useful plugins is much wider. In general, these will all be
  concerned with user interaction with a running (or completed)
  model. They will encompass plugins to display statistics about the
  execution of the model (analysis data), and others to elicit input
  from the user to control a model's execution.

  <p>
  Overall, a typical use case would be for an Electronic System
  Engineer to initially import a number of pre-existing models into
  their environment. They may use some VPP topology visualization
  plugins to help understand the connectivity of the model they are
  trying to build. They may then go on to build a few "component
  models" themselves, using many already existing Eclipse plugins, and
  some VPP additions. Having debugged the model, again with a
  combination of plugins, they would then go on to execute the model,
  controlling its execution through other VPP plugins. Finally, they
  will need to extract data from the model (the reason for
  constructing the model in the first place!). This data will be
  provided to the user through VPP plugins.</p>

  <h1>Project Principles</h1>

  Among the key principles on which this project has been initiated,
  and will be run, are the following:

  <p>
    <b>Leverage Eclipse Ecosystem </b>- A major goal of this project
    is to apply the application development strengths of Eclipse to
    the System Level Design Domain (especially as, in the end, a
    System Level model is nothing more than a relatively constrained,
    but none the less complex, application). The project will work
    closely with other Eclipse project areas whenever possible to
    leverage the capabilities being developed in those areas.
  </p>

  <p>
    <b>Vendor Neutral </b>- We aim to encourage Eclipse participation
    and drive Eclipse market acceptance by providing vendor-neutral
    capabilities and to encourage participation for complementary
    capabilities through additional projects.
  </p>

  <p>
     <b>Open, Standards-based Development </b>- Where market adopted
     standards exist that meet the design goals, our aim is to
     leverage and adhere to them. Where market adopted standards do
     not exist, we will develop and publish any new capabilities in
     the Eclipse open forum.
  </p>

  <p>
    <b>Incremental Delivery </b>- In order to meet the pent-up demand
    for a standardized framework for device software development
    tools, we aim to deliver functionality to the market as rapidly as
    possible via an incremental delivery model.
  </p>

  <h1>Project Scope </h1>

  Within the development of Device Software, three phases are identified:<p>

  <ul>
    <li>
      <b>Hardware Bring-up </b></li>
    <li>
      <b>Platform Software Development </b></li>
    <li>
      <b>Application Software Development </b></li>
  </ul>
  </p>

  <p>
  System Level Design includes all of these, but with respect to
  models. In addition, the Bring-up phase will apply to models of
  functional components that may be destined for implementation in a
  combination of hardware and software and there is one more phase:
  Hardware/Software partitioning, or model abstraction level decisions
  In this phase, designers decide at what level they need to (or can)
  model items in their system. At high levels of abstraction it may
  not be clear if this item will be implemented in hardware or in
  software. At lower levels details about the implementation become
  important. The VPP will use or adapt other projects within DSDP to
  address the other three design phases as they relate to system level
  design and will add specific exemplary tools aimed at assisting in
  the hardware/software partitioning task.</p>

  <p>Having designed a model, and chosen a level of abstraction suitable
  for the use cases to which the model is to be applied, the wider
  range of Eclipse tools and plug-ins will be required including:</p>

  <ul>
    <li> Model Debug</li>
    <li> System Visualization</li>
    <li> Model specific Analysis</li>
    <li> Transaction level analysis (communication between Virtual
    components)</li>
    <li> Model execution profiling</li>
    <li> User interactions</li>
    <li> Configuration</li>
    <li> Control (User based control, and script based control)</li>
    </ul>

  <h3>SystemC (and related languages)</h3>

  Specifically within the domain of System Level Design, SystemC is
  used as the modeling language of choice. It is based on C++ and can
  therefore be used to build software and hardware components. As it
  is a language in its own right, there are language related
  extensions to Eclipse that fall within this project.

  <h3>Initial technologies</h3>

  <ul>
    <li> SystemC Syntax highlighting </li>
    <li> SystemC topology viewer (to view the hardware components of a
    system, typically fed by a SystemC parser) </li>
    <li> Display of analysis information published by SystemC models </li>
    <li> Profiling of SystemC simulations (this requires hooks into
    the SystemC kernel to provide simulation activity in order to
    improve simulation performance) </li>
  </ul>

  <p>For each of these, work in disparate organizations is already taking
  place, GreenSocs (see below) aims to pull this work together in one central
  place and to feed that into VPP.</p>

  <h1>Organization</h1>

  There is already a project similar in conception to
  Eclipse called the <a href="http://www.greensocs.com/GreenSocsEclipse">GreenSocs Eclipse Project</a>,
  which is focused solely on System Level Design
  infrastructure and built for the most part around SystemC, within which
  Eclipse should form a part. Hence the logical point of interaction
  between Eclipse and GreenSocs is the VPPy project, which
  GreenSocs will run within Eclipse. GreenSocs will aim to collect
  funding and supply engineering to continue the development effort,
  but also to coordinate activities such as those within universities
  worldwide that are engaged in developing Eclipse extensions for ESL.
  Greensocs is already committed to offer initial contributions.
  This will occur soon after the project is
  created. GreenSocs will encourage and coordinate contributions for
  other organizations, expected from a number of industrial and
  academic institutions.

<h2>Interested Parties</h2>

 <h3>Industrial Organizations</h3>
 <a href="http://www.xilinx.com">Xilinx Research</a> investigates ESL technologies for FPGA designers and strongly 
  believes that a robust open-source solution for model development, debug and analysis is needed. 
  This project's goal (to build open-source modeling extensions to Eclipse) is both timely and 
  well focused on the set of core problems that an Eclipse open source solution could address.  
  In terms of direct support, Xilinx Research will continue to offer technical guidance to the 
  project and may donate code to accelerate the development of Eclipse plug-in modules.
  
  <h3>University Organizations</h3>
  <a href="http://www.fzi.de">FZI</a> (a research organization of German Universities), <a href="http://www.unicamp.br">Unicamp</a> (a similar organization 
  based in Brazil), and <a href="http://www.eis.cs.tu-bs.de/eis/english/welcome.html">EIS</a> (a research lab at TU Braunschweig, Germany) have contributed to the current efforts GreenSocs has made in building 
  some of the components required by VPP. 
  
  <h2>Contact Person</h2>
  Mark Burton (mark.burton@greensocs.com)
  <h1>Roadmap</h1>

  The intention is to provide basic plugins to support SystemC
  modeling and debug straight away. These will not be of sufficient
  quality to warrant a 1.0 release, but will be worked on to achieve
  that goal.  Other parallel activities will focus on model to user
  interactions. This work is expected to be industrially funded, with
  GreenSocs providing the resource.

  <p>
  </p>

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>