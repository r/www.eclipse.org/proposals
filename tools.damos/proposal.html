<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Damos</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<p>The Damos project is a proposed open source project under the <a
	href="http://www.eclipse.org/projects/project.php?id=tools">Tools project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<h2>Background</h2>

<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->

<p>In the field of science and engineering, a data flow-oriented approach is commonly used for analyzing and designing systems.
On the one hand it allows scientists and engineers to focus on the data processing aspects of a system,
and on the other hand it provides a means to specify the dynamics of the mechanical components and the physical environment of the system to
facilitate close-to-real-world system simulation.</p>

<p>Since many scientists and engineers already use Eclipse for software development,
it would be beneficial to perform system design, simulation, and code generation in Eclipse rather than using an external tool
like MATLAB/Simulink, Scicos, or LabVIEW.</p>

<h2>Scope</h2>

<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->
 
<p>The scope of this project is to provide an integrated development environment for developing data flow-oriented systems using block diagrams.</p>
<p>This will include:</p>
<ul>
<li>Metamodel for data flow-oriented system models</li>
<li>Graphical and textual editors</li>
<li>A simulation environment for simulating continuous, synchronous and asynchronous systems</li>
<li>Code generators to transform system models into source code</li>
<li>A declarative programming language for defining block behavior</li>
<li>A Block library containing commonly used blocks</li>
<li>Support for customizing and extending various aspects of the environment, such as simulation engine, code generators, and block libraries
<li>Support to derive custom data flow-oriented languages based on the structural and diagram editing components of this project</li>
</ul>

<p>Note that this project is unrelated to process flow and work flow modeling.
A description of the conceptual differences is given in the next section.</p> 

<h2>Description</h2>

<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->

<p>This project will provide a development environment for analyzing and designing systems using a data flow-oriented approach.
Such systems are specified using block diagrams, which
contain the software components (e.g. control algorithm) that will later be executed on a target device (e.g. digital controller)
and the mechanical components as well as the physical environment to allow for system simulation.
The mechanical and physical components of the system can be represented as a dynamical system,
which is specified by providing the transfer function of the corresponding linear time-invariant (LTI) system.</p>

<p><img src="simple-pi-controller.png"/></p>

<p>Even though data flow-oriented design is not bound to a specific domain,
it is most commonly used in control systems engineering and digital signal processing.</p>

<h3>Relationship to Process Flow and Work Flow Modeling</h3>

<p>Process flow and work flow models specify the high-level activities of a system.
In other words, they do not specify a concrete implementation of the activities of such systems.
For instance, business process models may specify that when receiving an order, the order must be checked 
and then an invoice must be created before the ordered parts can be shipped.
However, the concrete implementation (e.g. software components) of these activities is not specified.
Because of the missing implementation and since process flow and work flow models tend to contain prose text,
executing process models for simulation purposes is challenging.
Thus, process flow and work flow models can be seen as top-level architectural models.</p>

<p>Damos models, on the other hand, specify the concrete implementation of the software components and
optionally the physical/mechatronical environment (for simulation purposes) using a data flow-oriented approach.
Data flow-oriented modeling focuses on the data passing the system, rather than the state of the system.
Since the structure and behavior (i.e. implementation) of the components are formally specified,
such models can be simulated and transformed into executable code (e.g. C code).
Thus, Damos can be considered as a graphical, data flow-oriented, declarative programming language.</p>

<p>An important concept of data flow-oriented modeling is that components within such systems
can only access the input values and their own state variables to compute the
output values (i.e. components cannot access the global state of a system or the state of other components).
This results in the fact that data flow-oriented systems inherently parallelizable.</p>

<h3>Deriving Custom Data Flow-Oriented Languages</h3>

<p>Damos models contain structural and behavioral information.
The structural information specifies how systems are composed (i.e. system hierarchy) and how system components are interconnected.
The behavioral information specifies how the data that passes the system is transformed by the system components.
Damos provides a set of features that facilitate the definition of the system behavior.
These features include a type system and a declarative programming language.</p>

<p>Sometimes the provided behavioral concepts of Damos cannot be directly applied to a certain application.
Yet the structural concepts of Damos may fit very well.
In these cases, a new language can be derived from the structural features (e.g. structural metamodel and graphical editor) of Damos,
and then implementing the custom behavioral features.</p>

<h2>Why Eclipse?</h2> 

<!-- 
	Answer these two questions:	What value does this project bring to the Eclipse
	community? What value do you expect to obtain from hosting your project at Eclipse?
	
	What value do you get by having your project at Eclipse over and above the value
	of hosting at Eclipse Labs?
 -->

<p>Currently, Eclipse does not provide tool support for designing data flow-oriented systems.
Engineers and scientists are required to use tools which do not integrate very well with other Eclipse technologies.
Furthermore, due to the closed source nature of commercial tools, it is difficult to customize and extend these tools for better
integration with other Eclipse technologies.</p>

<p>This project will benefit from being hosted at Eclipse due to the broader visibility,
and better integration and coordination with other Eclipse-based technologies
such as EMF, RMF, CDT, RTSC, TCF, and other tools like Papyrus, eTrice, and Koneki.
Because of the perceived higher reliability in terms of long-term support,
it is expected to attract more interest and participation from other parties.
This will lead to greater market penetration, while improving quality and reducing development costs.</p>

<p>The following diagram shows how Damos can be integrated into a development workflow using other Eclipse-based technologies.
The green boxes represent the Damos-related activities and artifacts, whereas the blue boxes denote existing Eclipse-based
tools and frameworks.</p>

<p><img src="workflow.png"/></p>

<h2>Initial Contribution</h2>

<!-- 
	Projects are expected to arrive at Eclipse with existing code.
	
	Describe the existing code that will be contributed to the project. Please provide
	a couple of paragraphs describing the code with modest detail, including important
	information like code ownership (who holds the copyright?), and some consideration
	of community that exists around the code. Include a listing of third-party libraries 
	and associated licenses.
 -->

<p>The initial contribution consists of sources found in the current Damos repository at
<a href="http://eclipselabs.org/p/yakindu">EclipseLabs</a>, which include:
<ul>
<li>GMF-based block diagram editor</li>
<li>Simulator</li>
<li>C code generator</li>
<li>Basic block library</li>
<li>Declarative scripting language <i>Mscript</i></li>
</ul>
<p>

<h3>Architecture</h3>

<p>In order to facilitate customization, Damos is split into a set of top-level components (i.e. Eclipse features).
There are two main groups of components. The first group contains components providing the structural semantics
and the diagram editing features (orange).
The second group provides simulation and code generation components as well as the standard block library (green).
This enables other projects to build their own data flow-oriented languages by reusing the structural semantics
and the diagram editing components, without being required to include unnecessary components.</p>

<p><img src="architecture.png"/></p>

<h3>Main Components</h3>

<h4>Models</h4>

<p>EMF-based models are used to specify the semantics of data flow-oriented systems.
This enables users to perform model analysis and transformation using well-known Eclipse modeling technologies.<p>

<p>System models are comprised of models describing the mechanical components and the physical environment of a device,
and the software model that later will be executed on the device.
These models are specified using block diagrams.</p>

<p>When performing model simulation or code generation, an execution flow is first derived from the system model.
The execution flow can be seen as the imperative representation of the system model.
For instance, it describes the execution order of the components so that components that drive other components
are executed first.
This information is later used by the simulator and the code generator to execute and to generate the components in the right order, respectively.</p>

<p>Simulation and code generation can be configured using a Damos-specific configuration language.
Such configurations contain global properties and properties for individual Damos model elements (using an annotation mechanism).
Configurations can extend and/or override other configurations, so that, for instance,
properties that are common to simulation and code generation can be extracted to a base configuration.</p>

<p>To configure computation-related aspects (e.g. fixed-point arithmetic operations),
computation models can be specified in the configuration for individual model elements (e.g.components and subsystems).
Since Damos models only utilize high-level numeric data types (e.g. real and integer),
computation models are used to map high-level data types to machine data types (e.g. double and int32).
The computation models are incorporated by both the simulation environment and the code generators to ensure
that the simulation results match the behavior of the generated code.</p>

<p>Damos provides a GMF-based graphical block diagram editor for the system model.
For all other models, textual model editors based on Xtext are provided.</p>

<p><img src="models.png"/></p>

<h4>Block Diagram Editor</h4>

<p>The graphical block diagram editor is used for editing system models.
It can also be used to visualize real-time simulation results using virtual instruments.
Virtual instruments display simulation results in real-time, or provide a means to interact with the running simulation (e.g. using buttons and sliders).</p>

<p>Block diagram files contain two model types: the semantic model of the system and
the notation model containing the graphical information of the block diagram.
When processing block diagrams (i.e. simulation and code generation), the notation model is ignored.</p>

<p><a href="block-diagram-editor.png"><img src="block-diagram-editor-small.png"/></a></p>

<p>While editing block diagrams, the model is continuously validated.
Validation errors are shown as annotations on invalid model elements.
For instance, when using units of measurement for numeric values,
the compatibility of block inputs and outputs are checked based of their corresponding units.</p>

<p><img src="validation.png"/></p>


<h4>Mscript</h4>

<p>This project will include the declarative programming language <i>Mscript</i>.
Mscript can be used to specify the block behavior within the block type definitions.
The simulation engine and the code generators will then derive the simulation behavior and the source code from the specified Mscript functions, respectively.
This ensures that the simulation behavior and the generated code match as closely as possible.
Furthermore, when adding new code generators, those block types do not need to be adapted to the new code generators.</p>

<p>With Mscript, the user defines the relationship between the inputs and outputs
of a function using a set of equations.
In contrast to functional languages, Mscript functions can be stateful.
Consequently, such functions must be instantiated (function objects).
Stateful function equations can reference input and output values from previous computation steps,
which enable the specification of difference equations.
Difference equations are widely used to specify discrete transfer functions,
for instance in the field of digital signal processing (e.g. FIR and IIR filters).
In future versions, differential equations may be supported as well to
allow for specifying the behavior of time-continuous blocks.</p>

<p><img src="mscript.png"/></p>

<h4>Simulator</h4>

<p>The simulator utilizes adaptive and fixed step size solvers to simulate time-continuous, synchronous, and asynchronous systems.
Solvers encapsulate the numerical integration algorithm to solve the differential equations that represent the system.</p>

<p>When real-time simulation is performed, simulation results can be displayed in real-time using virtual instrument blocks.
The simulation engine incorporates a target-specific computation model,
so that&mdash;for instance&mdash;overflows can be detected when using fixed-point arithmetic.
Simulation aspects (e.g. solver configuration) can be configured using a simulation configuration model.</p>

<h4>Code Generator</h4>

<p>The code generation tooling initially includes a C code generator.
Other languages may be added in the future.
Each system component can provide its own code generator.
When using Mscript to describe the block behavior, an Mscript-specific code generator is used that
transforms the Mscript functions into C code.
The code generators incorporate a target-specific computation model containing information on how arithmetic operations should be handled.
The code generation process can be configured using a generator configuration model (e.g. target directories).</p>

<p><img src="codegen.png"/></p>

<h4>Block Library</h4>

<p>The standard block library contains commonly used blocks.
If required, the user can create his/her own block libraries.
The block behavior can be specified using the declarative programming language Mscript, which is part of this project.</p>

<h4>Extensibility</h4>

<p>The Eclipse extension mechanism can be used to customize and extend various aspects of the tooling, e.g:</p>
<ul>
<li>Registering block libraries</li>
<li>Contributing the visual representation (i.e. symbols) of blocks</li>
<li>Providing simulation behavior</li>
<li>Providing additional solvers</li>
<li>Providing code generators</li>
</ul>

<h2>Legal Issues</h2>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->
 
 All contributions will be distributed under the Eclipse Public License.
 
<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

<p>The following individuals are proposed as initial committers to the project:</p>

<dl>
	<dt>Andreas Unger, itemis (Project Lead)</dt>
	<dt>Michael Krauter, itemis</dt>
	<dt>Benjamin Schwertfeger, itemis</dt>
</dl>

<p>We welcome additional committers and contributions.</p>

<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Martin Oberhuber (<a href="http://www.windriver.com/">Windriver</a>)</li>
	<li>Wayne Beaton</li>
	
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li><a href="http://www.ksb.com/">KSB</a></li>
	<li><a href="http://www.fh-dortmund.de/">University of Applied Sciences Dortmund</a></li>
	<li><a href="http://www.itemis.com/">itemis</a></li>
	<li>Werner Keil, <a href="http://www.eclipse.org/uomo/">Eclipse UOMo</a></li>
	<li>Joseph Carroll (Financial OpenSource)</li>
	<li>Anthony Ford (University of Texas at Brownsville)</li>
</ul>

<h2>Project Scheduling</h2>

<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->

Initial contribution is expected in the third quarter of 2012. 

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>22-08-2012</td>
		<td>Added Joseph Carroll and Anthony Ford as interested parties.</td>
	</tr>
	<tr>
		<td>01-05-2012</td>
		<td>Document created</td>
	</tr>
</table>
</body>
</html>
