<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>e4</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("e4");
?>

<h2>Introduction</h2>
<p>The Eclipse e4 Project is a proposed open source project under the Eclipse Project.</p>

<p>This proposal is in the <a href="http://www.eclipse.org/projects/dev_process/development_process.php">Project Proposal Phase</a> and and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project.</p>

<p>Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.e4">eclipse.e4 newsgroup</a>.</p>

<h2>Background</h2>
<p>The Eclipse platform was first targeted at  building an extensible IDE component framework.  It has since grown to include a Rich Client Platform, enabling whole new categories of scenarios and domains. As the software landscape changes, so must the Eclipse platform in order to remain relevant and vibrant. These trend lines point to web technologies, new user interface metaphors, and distributed infrastructure. Now is the time to rethink elements of the platform so that Eclipse may remain at the forefront of application development.</p>

<h2>Scope</h2>
<p>The mission of the e4 project is to build a next generation platform for pervasive, component-based applications and tools.</p>

<p>Specifically, efforts will be directed towards but not limited to:
<ul>
<li>Simplify the programming model, make it easier to write plugins
 <ul>
 <li>More uniform APIs</li>
 <li>Rationalized and consistent application model exposed as services</li>
 <li>Scripting (initially Javascript)</li>
 <li>Plugins in other (non-Java) languages</li>
 </ul>
</li>
<li>More flexible, sophisticated UI styling
 <ul>
 <li>DOM based access to model of workbench</li>
 <li>Improve separation of appearance and underlying function</li>
 </ul>
</li>
<li>Enable a broader array of applications to be built from Eclipse 
 <ul>
 <li>Scale down to simpler RCP applications</li>
 <li>Server-enabled Eclipse to permit Eclipse in a web browser</li>
 <li>Client/server programming</li>
 <li>Remote workspaces</li>
 <li>RESTful services against workbench DOM</li>
 <li>Multi-user enable the platform</li>
 <li>SWT Browser Edition: SWT widgets in native web technologies (e.g. AJAX frameworks, Flex, Silverlight)</li>
 <li>More flexible resource and build model.</li>
 <li>More integrated <a href="http://wiki.eclipse.org/E4/Connection_Frameworks">connection frameworks</a> (both UI and non-UI) across projects</li>
 </ul>
</li>
</ul>
</p>

<p>As this is first and foremost a community effort, the actual set of items will expand and change to reflect the interests of those who join e4.</p>

<p>Also of importance are the necessary tools to be able to self-host, i.e. to develop e4 using tools based on e4. Wherever appropriate, we will use or adapt tools that already exist in other Eclipse pojects. </p>

<p>We expect the resulting platform to be backwards-compatible to Eclipse 3.x for API-clean plug-ins, possibly at the cost of 3.x plugins depending on compatibility plug-ins.</p>

<p>Additional information can be found on the <a href="http://wiki.eclipse.org/E4">e4 wiki</a>.</p>

<h2>Out of Scope</h2>

<p>It is not our intention to duplicate efforts underway in other Eclipse projects. However, given the broad scope, there is potential overlap with other Eclipse community projects. Where possible and practical, we will seek reuse and collaboration.</p>

<h2>Organization</h2>

<p>This project is proposed under the Eclipse top-level project. Being a separate project from the existing Platform project gives it room to innovate without disruption to the ongoing 3.x stream work taking place in the Eclipse Platform project. It also welcomes a new set of contributors.</p>

<p>We foresee the following possible paths for the e4 project: 
<ol>
<li>Work areas of the e4 project, as they graduate, are merged into the mainline Eclipse Platform project.</li>
<li>The project as a whole graduates as a permanent project under the Eclipse project.</li>
</ol>
These paths are not mutually exclusive. Some combination of some or all of these paths may be the result of the work done in this project.<p>

<h2>Proposed Initial Committers</h2>

<ul><li> Mike Wilson, IBM Canada (project lead) - McQ is an Eclipse Project PMC member, representing the Platform and Incubator projects. He is also the Eclipse Project Platform UI team lead and has in the past lead the Resources, Team and SWT teams. He is a member of the Eclipse Architecture Council, and one of the original founders of Eclipse. His technical interests are programming languages, UI design, and web development. He is actively involved in the technical leadership of e4, which is one of his main focuses.
</li><li> Andreas Ecker, <a href="http://1and1.com">1&amp;1 Internet AG</a> Andreas is the project lead of <a href="http://qooxdoo.org">qooxdoo</a>. At 1&amp;1 he leads a team of engineers who are dedicated full-time to qooxdoo development. Andreas has extensive know how in JavaScript and web technologies as well as designing platforms for web application development. He is interested in contributing to e4, possibly implementing a SWT browser edition based on GWT and qooxdoo.
</li><li> Angelo Zerr - Angelo is the developer of <a href="http://tk-ui.sourceforge.net/">TK-UI</a>, a toolkit to manage CSS Styling/Declarative UI (XUL, XHTML) which renders to Swing and SWT.  He has also developed <a href="http://akrogen.sourceforge.net/">Akrogen</a>, an Eclipse plugin for code generation where you can describe Eclipse Wizard with XUL and Javascript and link it with template (Freemarker, Velocity, XSL...) or Ant Task to generate code.
</li><li> Benjamin Muskalla, <a href="http://innoopract.com">Innoopract</a> - Benny is one of the developers of the <a href="http://www.eclipse.org/rap">Rich Ajax Platform (RAP)</a> and committer on several other Eclipse projects like the <a href="http://www.eclipse.org/examples/">Examples</a> project. As long-term contributor to the Eclipse platform and it's subcomponents he's interested in elevating the platform to the next level. His main interest is the SWT/RAP combination and the client-server slit. In addition he strongly cares about the tooling to leverage the adoption of these new technologies.
</li><li> Bogdan Gheorghe, IBM Canada - Bogdan is a committer on the Platform SWT component. He is interested in SWT Browser Edition.
</li><li> Boris Bokowski, IBM Canada - Boris is a committer on the Platform UI component.  He is interested in developing a new, simpler programming model for Eclipse-based applications, and generalizing the Eclipse platform to support multiple programming languages, distributed computing, and the web.  He also would like to get rid of having to write listeners in UI programming.
</li><li> Brian Fitzpatrick, Sybase - Brian Fitzpatrick is a software engineer with Sybase, Inc., who has contributed to the Data Tools Project (DTP) since its inception. Brian's focus has mainly been on Eclipse tooling development for Sybase and he hopes to continue helping out with DTP for the forseeable future.
</li><li> Carolyn MacLeod, IBM Canada - Carolyn is a committer on the Platform SWT component. She is interested in accessibility issues.
</li><li> Chris Aniszczyk, Code 9 - Chris leads the Plug-in Development Environment (PDE) project.  He is interested in making e4 a reality and widely adopted in industry. His interests in e4 are improving self-hosting workflows, modularity, and being able to write plug-ins in different languages.
</li><li> Chris Recoskie, IBM Canada - Chris is a committer on the CDT and PTP projects.  His main focus on e4 is the flexible resource model, particularly with a view towards supporting users with large existing C/C++ codebases, and also with a view towards supporting remote development tools.
</li><li> <a href="/E4/position_papers/Christian_Campo">Christian Campo</a>, <a href="http://www.compeople.de">compeople AG</a> - Christian leads the <a href="http://www.eclipse.org/riena">Riena</a> project. He is interested in client/server split, declarative UI and server-side Eclipse.
</li><li> Dave Orme, Coconut Palm Software - Dave started VE, Eclipse DataBinding, and runs XSWT, the XML-based SWT page description language.  Dave is an active Eclipse RCP contractor.  He is interested in working on XSWT in Eclipse
</li><li> Doug Schaefer, Wind River - Doug is the CDT project lead and has been a CDT committer since the early days of CDT. He is also a member of the Tools PMC and Eclipse Architecture Council. His main focus in e4 is on the resource management system, good ol org.eclipse.core.resources and friends.
</li><li> Duong Nguyen, IBM Canada - Duong is a committer on the Platform SWT component. He is interested in SWT Browser Edition.
</li><li> Ed Merks, <a href="http://www.macromodeling.com">Macro Modeling</a> Ed does lots of stuff at Eclipse and has done so for years.  He keeps his bio up to date at <a href="http://www.macromodeling.com/about">his website</a>.
</li><li> Eric Moffatt, IBM Canada - Eric has been a Platform UI Committer since 2006. He works on SWT related topics mostly, Trim Management, Detached Windows, Perspective layouts... 
</li><li> Fabian Jakobs, <a href="http://1and1.com">1&amp;1 Internet AG</a> Fabian works as a JavaScript framework architect for 1&amp;1 since November 2006. He is connected to e4 through qooxdoo, the JavaScript framework used in RAP. His recent work involved designing and implementing the new widget and layout system for the qooxdoo 0.8. His interests regarding e4 are primarily in the SWT browser edition field, especially in the combination of SWT-qooxdoo-GWT. 
</li><li> Felipe Heidrich, IBM Canada - Felipe has been an SWT committer for many years. He is a jack of all trades contributing to all areas of SWT. His main focus is on StyledText and Internationalization. 
</li><li> Francis Upton IV, <a href="http://oaklandsoftware.com">Oakland Software</a> - Francis is a committer on the Platform UI component, responsible for the maintenance of the Common Navigator.  For his day gig he works on Eclipse-based data transformation.  He is interested in making RCP easy for the masses, advanced RCP applications and how they can be migrated to the Browser using E4.
</li><li> Frank Appel, <a href="http://innoopract.com">Innoopract</a> - Frank Appel is the technical lead on the Eclipse Rich Ajax Platform (RAP) project. At Innoopract, a recognized leader in Eclipse distribution and web application development, Frank led the team that developed the W4Toolkit, the base code donation for the RAP project. Frank was also responsible for developing W4T Eclipse, a visual web application development tool for the W4Toolkit. He has been developing Eclipse extensions since 2002, having joined the Java world in 2000 after a short stopover in Visual Basic and Delphi programming.
</li><li> Grant Gayed, IBM Canada - Grant has been a committer on the Platform SWT component since 2001. He is interested in SWT Browser Edition.
</li><li> <a href="http://code9.com/team#jeff">Jeff McAffer</a>, <a href="http://code9.com">Code 9</a> Jeff does lots of stuff at Eclipse and has done so for years.  He keeps his bio up to date at <a href="http://code9.com/team#jeff">his website</a>.
</li><li> Jochen Krause, <a href="http://innoopract.com">Innoopract</a> Jochen is the project lead for the Eclipse Rich Ajax Platform (RAP) project, co-leads the Eclipse RT PMC and is a member of the Board of Directors of the Eclipse Foundation. He is interested in making e4 a comprehensive platform for desktop and web applications.
</li><li> John Arthorne, IBM Canada - John has worked on the Eclipse project for the past decade in almost all components. In recent years he has focused on the workspace model, concurrency infrastructure, provisioning (p2), and overall platform API quality. He intends to contribute to e4 in the areas of application/service model, API design, and help out with workspace/resource changes as needed.
</li><li> Kai T�dter, Siemens - Kai is software engineer in the architecture department of Siemens Corporate Technology. He has more than 10 years of professional Java experience. His main interest is software architecture for smart clients and he focuses on Java rich client platforms. Kai is Siemens' primary contact in the Eclipse Foundation.
</li><li> Kenn Hussey, Embarcadero Technologies - Kenn is lead of the MDT project and committer on the EMF project, in addition to several components of MDT. He is a member of the Modeling PMC and is interested in contributing to the workbench and resource models of e4.
</li><li> Kevin Barnes, IBM Canada - Kevin joined the SWT team in 2006. Before that, he has been working on the Eclipse Debug Framework and JDT Debugger. Previous to his employment at IBM, Kevin worked at IAM Training and Consulting, on projects such as IAM-&gt;Developing - a collaborative java IDE.
</li><li> Kevin McGuire, IBM Canada - Kevin is a Platform UI committer. He was one of the original Eclipse team members, in charge of the Team/CVS component. In the interim he's shipped enterprise level Eclipse based applications (IBM WSAD/IE and WID) in the role of UI Design and Development Lead.
</li><li> Kim Horne, IBM Canada - Kim has worked on the Eclipse platform as an IBM employee since its 2.1.X releases.  She is interested in the client/server split, perspectives (and their descendants), and the workbench in general.
</li><li> Martin Oberhuber, Wind River - Martin is the lead of the DSDP Target Management Project, and chair of the Eclipse Architecture Council. He's been working for Wind River in Salzburg, Austria since 1998. Driven by his desire for constant improvement, he is involved in many areas around Eclipse and Open Source. Martin holds a Master of Engineering degree in Telematics from the University of Technology Graz/Austria.
</li><li> Michael Scharf, Wind River - Michael is one of the architects of the Wind River Workbench, CDT based IDE for embedded development. He works for WindRiver since 1994. Earlier in his career, he worked for 9 years in the area of computational molecular biology using object oriented technology for analysis and visualization of complex data. He is working with eclipse since 2001.
</li><li> Mike Morearty, Adobe Systems - Mike Morearty has been a developer on Adobe Flex Builder, an Eclipse-based IDE, since 2005.
</li><li> Olivier Thomann, IBM Canada - Olivier Thomann worked in the JDT/Core team since its inception. He is an active committer to JDT/Core and PDE/Api Tools.
</li><li> Paul Webster, IBM Canada - Paul Webster joined the IBM Eclipse Platform UI team in May 2005 and is currently working for IBM Rational Software. Paul is responsible for command and handlers, keybindings, and menu contributions, and is interested in the evolution of commands in E4.
</li><li> Philippe Mulet, IBM France  - Philippe Mulet leads the Eclipse project and the Java Development Tooling (JDT) Eclipse subproject; working at IBM since 1996; he is currently located in Saint-Nazaire (France). In late 1990s, Philippe was in charge of the compiler and code assist tools in IBM Java Integrated Development Environments (IDEs): VisualAge for Java Standard and Micro editions. Philippe became responsible for the infrastructure of the Java tools for Eclipse (2000), then the entire Java tools subproject (2005), and more recently for the overall Eclipse project (2007). Philippe has also been a member of the expert group on compiler API (JSR199), representing IBM. His main interests are in compilation, performance, scalability and meta-level architectures.
</li><li> <a href="http://wiki.eclipse.org/Ralf_Sternberg">Ralf Sternberg</a>, <a href="http://innoopract.com">Innoopract</a> - Ralf is a committer to the Rich Ajax Platform (RAP), working for Innoopract in Karlsruhe, Germany. His main work area is RWT, the RAP SWT implementation. He's especially interested in the declarative UI and CSS styling features of e4.
</li><li> R�diger Herrmann, <a href="http://innoopract.com">Innoopract</a> - R�diger is one of the initial committers on the <a href="http://eclipse.org/rap">Rich Ajax Platform (RAP)</a> Project, contributing to all areas of RAP.
</li><li> Scott Kovatch, Adobe Systems - Scott is a developer on Adobe Thermo, which is a Flex design tool based on Eclipse. Prior to Adobe, he was the technical lead for the AWT for Mac OS X at Apple, and has contributed to the SWT on Mac OS X. He was named an SWT committer earlier this year, and is currently focused on the SWT port to Cocoa.
</li><li> <a href="http://code9.com/team/#scott">Scott Lewis</a>, <a href="http://code9.com">Code9</a>.  Scott is the project lead for the <a href="http://www.eclipse.org/ecf">Eclipse Communication Framework project (ECF)</a> and was instrumental in it's initiation, design, and implementation.  He is interested in contributing to the inter-process communications and application-level collaboration support within E4.
</li><li> Szymon Brandys, IBM Poland - Szymon has worked on the Eclipse project since 3.3. He is a committer on the Platform UI and Core components. He is interested in contributing to e4 in the workspace/resources area.
</li><li> Serge Beauchamp, Freescale Semiconductor - Serge has been working on the Flexible Project Structure improvement project, and pushing for increase in scalability and performance in the Eclipse CDT and Platform.
</li><li> Silenio Quarti, IBM Canada - Silenio is the technical lead for SWT. His areas of expertise include graphics, widgets, threading, optimization and operating system programming. He has been intimately involved with both the design and implementation of SWT for many years.
</li><li> Steve Northover, IBM Canada - Steve is the father of SWT (The Standard Widget Toolkit). His interests include programming languages, API design, operating systems, user intefaces and code optimization.
</li><li> Thomas Schindl, Bestsolution GmbH - Tom joined the Platform UI team in 2007 as an external committer. Tom's main working area was JFace and his main interests in E4 are "Modeling the workbench" and "Declarative UI".
</li><li> Yves YANG, Soyatec - Yves YANG, Main founder of Soyatec and Committer of Eclipse VE, has over 17 years of experience working with OO software development. He was the chief architect of EclipseUML (First EMF/UML native Eclipse Modeler) and co-founder/CTO of Omondo from 2002 to early 2006. In Soyatec, regarding UI programming, he has leaded and developed a world-wide first solution of XAML for Java: eFace. He is interested in contributing to the Declarative UI/CSS Styling and the workbench model of e4.
</li></ul>

<h2>Interested Parties</h2>

<p>Support has been expessed by:
<ul>
<li>Code 9</li>
<li>IBM</li>
<li>Innoopract</li>
<li>Wind River</li>
</ul>
</p>

<h2>Relationship to other Eclipse projects</h2>

<p>This project will be an incubator for changes to the Eclipse project platform.  It may make use of tooling in other projects such as JavaScript editing in WST.  It may consume components from Orbit such as Javascript support.</p>

<h2>Code Contribution</h2>

<p>The Eclipse Incubator project has a component named "e4". We plan to move code from that component into the new Eclipse e4 project when it has been created.</p>

<h2>Tentative Plan</h2>

<p>A technical preview is planned for the summer of 2009, with the first regular release tentatively scheduled for the summer of 2010.</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

<!--
</li><li> Bernd Kolb, Kolbware 
</li><li> Bjorn Freeman-Benson, Eclipse Foundation 
</li><li> Henrik Lindberg, Cloudsmith 
</li><li> Istvan Ballok, CAS Software AG 
</li><li> Matt Flaherty, IBM Westford (Lotus) 
</li><li> Matt Hatem, IBM Westford (Lotus) 
</li><li> Maxime Daniel, IBM France 
</li><li> Mik Kersten, Tasktop 
</li><li> Nikola Zelenkov, IBM Canada 
</li><li> Pascal Rapicault, IBM Canada 
</li><li> Thomas Hallgren, Cloudsmith 
</li><li> Tim deBoer, IBM Canada 
</li><li> Wayne Beaton, Eclipse Foundation 
-->