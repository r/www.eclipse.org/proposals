<html>
<head>
<title>Aether</title>
</head>

<style>
dt {
	display: list-item;
	list-style-position: outside;
	list-style-image:
		url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
	margin-left: 16px;
}

dd {
	margin-left: 25px;
	margin-bottom: 5px;
}
</style>

<body>

	<p>
		The Aether project is a proposed open source project under the <a
			href="http://www.eclipse.org/projects/project_summary.php?projectid=technology">Technology
			Container Project</a>.
	</p>

	<p>
		This proposal is in the Project Proposal Phase (as defined in the
		Eclipse Development Process) and is written to declare its intent and
		scope. We solicit additional participation and input from the Eclipse
		community. Please send all feedback to the <a
			href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse
			Proposals</a> Forum.
	</p>

	<h2>Scope</h2>

	<p>The Aether project provides a general-purpose artifact
		resolution mechanism that can be embedded in any Java program. The
		Aether project also provides Ant Tasks that allow Ant builds to have
		the exact same dependency management capabilities as Maven 3.x. The
		Aether project does not providing any UI related to dependency
		management.</p>

	<h2>Description</h2>

	<p>Aether is a library for working with artifact repositories.
		Aether deals with the specification of local repository, remote
		repository, developer workspaces, artifact transports, and artifact
		resolution. Aether's primary implementation is for Maven repositories
		and is the resolution engine used in Maven 3.x.</p>

	<p>The primary task of Aether is to resolve transitive
		dependencies. This task can be split into two sub tasks:</p>

	<ul>
		<li>Determine the coordinates of the artifacts that make up the
			transitive dependencies.</li>
		<li>Resolve the files for the artifacts that have been identified
			in step 1.</li>
	</ul>

	<p>Artifacts and their dependencies among each other form a
		dependency graph. So in other words, step 1 means to calculate this
		dependency graph and step 2 is a simple graph traversal that fetches
		the file for each artifact in the dependency graph. In Aether, this
		dependency graph can be easily inspected and extension points are
		provided to allow for more control over the construction of the
		dependency graph. To understand those extension points, we will have a
		closer look at the way the dependency graph is constructed.</p>

	<p>
		Starting from a given root dependency like <a
			href="http://aether.sonatype.org/using-aether-in-maven-plugins.html">org.sonatype.aether:aether-impl:1.7</a>,
		the repository system first reads the corresponding artifact
		descriptor (i.e. the POM). The artifact descriptor tells about direct
		dependencies, dependency management and additional remote repositories
		to consider during the resolution. For each direct dependency, a
		dependency selector is given a chance to exclude the dependency from
		the graph. If the dependency is included, a dependency manager applies
		dependency management (if any). Next, the declared dependency version
		is expanded to a list of matching versions from the repositories. For
		a simple version like "1.0", the resulting list contains only that
		version. For a version range like "[1.0,2.0)", the version list
		generally contains multiple versions. For each matching version of the
		dependency, a child node is added to the dependency graph. Recursion
		of the process for each child dependency is controlled by a dependency
		traverser.
	</p>

	<p>The above process creates a dependency graph that often contains
		duplicate or conflicting dependencies and as such is called a dirty
		graph. A chain of dependency graph transformers is then used to trim
		this graph down and to form the resolved dependency graph.</p>

	<p>So more technically, the dependency graph that the repository
		system returns to its caller is affected by instances of
		org.sonatype.aether.collection.DependencySelector,
		org.sonatype.aether.collection.DependencyManager,
		org.sonatype.aether.collection.DependencyTraverser and
		org.sonatype.aether.collection.DependencyGraphTransformer. Users of
		the repository system can directly control those extension points when
		creating the repository system session by providing implementations
		that fit their needs.</p>

	<p>For example, a dependency selector can process exclusions on
		child dependencies, exclude optional dependencies or dependencies with
		certain unwanted scopes. A dependency traverser can be used to decide
		whether the dependencies of a (fat) WAR should be included in the
		dependency graph or not. Dependency graph transformers can identify
		and mark conflicting nodes in the dirty tree and resolve conflicting
		versions or scopes by pruning unwanted parts from the graph.</p>

	<p>
		The class MavenRepositorySystemSession from maven-aether-provider
		provides a session that mimics the resolution rules used by Maven. In
		case you want to customize the graph construction, feel free to have a
		look at the source of its constructor to learn about the
		implementation classes being used there to achieve Maven style
		behavior, you might want to reuse some of those for your own
		repository system session as well. Maven plugins can easily get access
		to the current repository system session via the usual parameter
		injection, see <a
			href="http://aether.sonatype.org/using-aether-in-maven-plugins.html">Using
			Aether in Maven Plugins</a> for the actual code bits.
	</p>

	<p>
		There's also a <a
			href="http://aether.sonatype.org/uml-class-diagram.html">UML
			Class Diagram</a> that illustrates a subset of the API graphically.
	</p>

	<h2>Relationship with other Eclipse projects</h2>

	<p>Aether has a lot of alignment with m2eclipse, Tycho, Buckminster
		and p2. Aether is used heavily within m2eclipse and from within Tycho
		to make Maven artifacts available to OSGi builds. Buckminster has many
		artifact resolution, and artifact related uses cases. Of particular
		interest to Aether developers is the concept of the Buckminster Omni
		Version that tries to provide interoperability between Maven-based
		versions and OSGi-based versions. The p2 project has its own artifact
		resolution mechanism and ultimately if Maven-based resolution can be
		encoded using SAT4J and resolved using p2 then that would be ideal.</p>
		
	<h2>Why Eclipse?</h2>

	<!-- 
    Answer these two questions:    What value does this project bring to the Eclipse
    community? What value do you expect to obtain from hosting your project at Eclipse?
    
    What value do you get by having your project at Eclipse over and above the value
    of hosting at Eclipse Labs?
 -->

	<p>Maven appears to be increasing in importance within the Eclipse
		community: the Maven 3.x Tycho plugins are rapidly becoming the de
		facto choice for builds, and m2eclipse is one of the most popular
		plugins for Indigo. Aether currently plays a vital role within Tycho
		and m2eclipse and providing access to Aether to those parties who are
		interested in Tycho and m2eclipse helps build the collaboration
		between the projects. A greater understand of Aether is generally of
		value for systems that integrate Maven technologies as Tycho and
		m2eclipse do. There are a number of companies who are interested in
		working on Aether, but in particular the Aether project looks forward
		to the participation of SAP. SAP has been an enormous help on the
		Tycho project and they have shown a similar interest in working on
		Aether. We believe that over time many companies leveraging
		Eclipse-based technologies will be interesting in participating in the
		Aether project, SAP alone makes the move to Eclipse worthwhile.</p>

	<p>Tycho and m2eclipse are already projects heavily in use within
		Eclipse and Aether is already production ready. The Aether feels that
		Eclipse proper is a more appropriate place for Aether at its stage of
		maturity than Eclipse Labs.</p>

	<h2>Initial Contribution</h2>

	<p>
		The initial contribution of the Aether would consist of the core
		codebase that can be viewed <a
			href="https://github.com/sonatype/sonatype-aether">here</a> and the
		Aether Ant Tasks which can be viewed <a
			href="http://aether.sonatype.org/using-aether-in-maven-plugins.html">here</a>.
		The implementation of the Maven 3.x connector and resolution mechanism
		is complete and the Aether Ant Tasks are also complete. Inside the
		core a transport based on Jeanfrancois Arcand's Async HTTP Client is
		also included. Currently Aether itself does not depend on any Maven
		3.x implementation classes, but the first connector implemented is the
		Maven 3.x connector which allows Maven 3.x to use Aether for all its
		dependency resolution.
	</p>

	<h2>Legal Issues</h2>

	<p>There are no known legal issues.</p>

	<h2>Committers</h2>

	<p>The following individuals are proposed as initial committers to
		the project:</p>

	<ul>
		<li>Benjamin Bentmann, Sonatype (lead)</li>
		<li>Jason van Zyl, Sonatype (lead)</li>
		<li>Igor Fedorenko, Sonatype</li>
		<li>Benjamin Hanzelmann, Sonatype</li>
		<li>Jeanfrancois Arcand, Sonatype</li>
		<li>Alin Dreghiciu, Sonatype</li>
		<li>Herve Boutemy</li>
		<li>Kristian Rosenvold</li>
		<li>Stephen Connolly</li>
		<li>Olivier Lamy (Talend)</li>
		<li>Mark Struberg</li>
		<li>Brian Fox (Sonatype)</li>
		<li>John Casey (Red Hat)</li>
	</ul>

	<h2>Mentors</h2>

	<p>The following Architecture Council members will mentor this
		project:</p>

	<ul>
		<li>Mik Kersten</li>
		<li>Wayne Beaton</li>
	</ul>

	<h2>Interested Parties</h2>

	<p>The following individuals, organisations, companies and projects
		have expressed interest in this project:</p>

	<ul>
		<li>Christophe Thiebaud, SAP</li>
		<li>Louis Lecaroz, SAP</li>
		<li>Andreas Brehmer, SAP</li>
		<li>Dain Sundstrom, Proofpoint</li>
	</ul>

	<h2>Project Scheduling</h2>

	<p>The Aether project intends to make the first code contribution
		by August 15th, and we hope for the first builds to be available soon
		after that.</p>

	<h2>Changes to this Document</h2>

	<table>
		<tr>
			<th>Date</th>
			<th>Change</th>
		</tr>
		<tr>
			<td>18-August-2011</td>
			<td>Added several new committers</td>
		</tr>
	</table>

</body>
</html>
