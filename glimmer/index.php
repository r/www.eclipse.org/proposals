<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Proposal for the Glimmer Project</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Glimmer");
?>

<h2>Introduction</h2>

<p>
	Glimmer is a JRuby DSL that enables easy and efficient authoring of user-interfaces using the robust platform-independent Eclipse SWT library. Glimmer comes with built-in data-binding support to greatly facilitate synchronizing UI with domain models.
</p>

<p>
	Glimmer is proposed as an open source project under the <a href="http://www.eclipse.org/technology">Eclipse Technology Project</a>. This proposal is in the Project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process document</a>) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the 
	<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.glimmer">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.glimmer</a> newsgroup.
</p>

<h2>Background and Goal</h2>

<p>
	Ruby, a dynamically-typed object-oriented language, provides great productivity gains due to its powerful expressive syntax and dynamic nature as demonstrated by the Ruby on Rails framework for web development. However, it currently lacks a robust platform-independent framework for building desktop applications.
</p>

<p>
	Given that Java libraries can now be utilized in Ruby code through JRuby, Eclipse technologies, such as SWT, JFace, and RCP can help fill the gap of desktop application development.
</p>

<p>
	The goal of the Glimmer project is to create a JRuby framework on top of Eclipse technologies to enable easy and efficient authoring of desktop applications by taking advantage of the Ruby language.
</p>

<h2>Packaging and Deployment</h2>

<p>
	As Glimmer solidifies its DSL to write SWT and RCP applications, the next challenge is to figure out how to elegantly integrate it as an Eclipse plug-in.
</p>
<p>
	Additionally, we will consider packaging Glimmer (or parts of it) as a RubyGem to make it easy to consume by the Ruby community.
</p>

<h2>Project Scope</h2>

<p>
	The scope of the project is simply to make it possible to use a JRuby framework to author Eclipse-based applications.
</p>

<p>
	Future work may involve enhancements to meet new use-cases or take advantage of other Eclipse technologies like the Eclipse Modeling Framework and other Ruby technologies like Rails.
</p>

<p>
	While Glimmerís original goal is to enable desktop application development with JRuby, that does not preclude it from growing to cover other areas, such as web development. For example, when SWT starts supporting Ajax widgets, Glimmer may be enhanced to further simplify web development (e.g. provide a Glimmer Rails plug-in.)
</p>

<h2>Organization</h2>

<h3>Mentors</h3>

<ul>
	<li>Chris Aniszczyk</li>
	<li>Ed Merks</li>
</ul>

<h3>Initial Committers</h3>

<ul>
	<li>Annas "Andy" Maleh (<a href="http://www.obtiva.com">Obtiva</a>)</li>
	<li>Nick Malnick (<a href="http://www.obtiva.com">Obtiva</a>)</li>
	<li>Dave Hoover (<a href="http://www.obtiva.com">Obtiva</a>)</li>
	<li>Kevin P. Taylor (<a href="http://www.obtiva.com">Obtiva</a>)</li>
</ul>

<h3>Interested Parties</h3>

<ul>
	<li>PDE - Chris Aniszczyk [zx@us.ibm.com]</li>
	<li>EMF - Ed Merks [merks@ca.ibm.com]</li>
</ul>


      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>


