<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Sirius</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<p>The Sirius project is a proposed open source project under the <a
	href="http://www.eclipse.org/projects/project_summary.php?projectid=modeling">Modeling 
top-level Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<h2>Background</h2>

<p>
Sirius has been developed by Thales as an internal component and is deployed within the whole Group. It is also deployed in other organizations as part of the Obeo Designer product. It is the core platform of several modeling environments already widely used, some of them with more than 2,000 installations a month via the Eclipse Marketplace.
</p>
<p>From the specifier/developer perspective, Sirius provides:</p>
<ul>
  <li>The ability to define workbenches providing editors including diagrams, tables or trees.</li>
  <li>The ability to integrate and deploy the aforementioned environment into Eclipse IDE’s or RCP applications.</li>
  <li>The ability to customize existing environments by specialization and extension.</li>
</ul>

<p>From the end user perspective, Sirius provides:</p>
<ul>
 <li>Rich and specialized modeling editors to design their models.</li>
 <li>Synchronization between these different editors.</li>  
</ul>

<h2>Scope</h2>

<p>
Sirius aims at providing specific multi-view workbenches through graphical, table or tree modeling editors.
Users can easily define their own modeling workbench, even with very little technical knowledge of Eclipse, while still being able to deeply customize it when needed.
</p>

<p>
The project provides a runtime and the associated tools required to describe, test, and distribute these workbenches on top of the Eclipse platform.
</p>

<h2>Description</h2>

<h3>Approach</h3>

<p>
Sirius enables the specification of a modeling workbench in termes of graphical, table or tree editors with validation rules and actions using declarative descriptions. All shape characteristics and behaviors can be easily configured with a minimum technical knowledge. This description is dynamically interpreted to materialize the workbench within the Eclipse IDE. No code generation is involved, the specifier of the workbench can have <b>instant feedback</b> while adapting the description. Once completed, the modeling workbench can be deployed as a standard Eclipse plugin.
</p>

<p>
Thanks to this short feedback loop a workbench or its specialization can be created in a matter of hours.
</p>

<h3>Multi-concern editors</h3>

<p>
We believe in the power of dedicated representations to analyze specific problems. Sirius provides a set of customizable and highly dynamic representations working seamlessly together on top of models. These representations can be combined and customized according to the concept of Viewpoint, inspired from the ISO/IEC-42010 standard. Views, dedicated to a specific Viewpoint can adapt both their display and behavior depending on the model state and on the current concern. The same information can also be simultaneously represented through diagram, table or tree editors.
</p>

<h3>Model Management</h3>


<p>
Sirius provides a runtime that works with models within the Eclipse IDE, providing the notion of «Modeling Project», an integration with the Project Explorer, handling the synchronization of workspace resources and in memory instances and the sharing of command stacks and editing domains across editors.
</p>
<p>
The main non-functional focuses for this runtime are scalability, performance and an easy-to-use UI’s.
</p>

<h3>Flexibility</h3>

<p>The workbench description is loosely connected to the structure of the model thanks to queries. They are able to define the diagram content even in a way which does not match the model structure. As long as an EMF model is in memory, being a UML model or a DSL done with Xtext, the runtime operates the same.
</p>

<h3>Extensibility</h3>
<p>
For supporting specific use cases and requirements, there is always a need for customization. Sirius is extensible in many ways, notably by providing new kinds of representations, new query languages and by being able to call Java code to interact with Eclipse or any other system. Extensibility is achieved through the use of extension points and, when needed, by reusing existing APIs available in the underlying components (e.g. using the GMF runtime extension points to adapt a graphical modeler).
</p>
 
<h2>Why Eclipse?</h2> 

<p>By releasing and developing Sirius publicly we are pursuing two objectives:</p>
<ol>
  <li>Strengthening the Eclipse Modeling Eco-system by enabling the creation of rich modeling workbenches at a very low cost. </li>
  <li>Unleashing the potential collaborations with other partners and projects in a healthy and commercially-friendly environment.</li>  
</ol>

<p>
Furthermore Sirius is tightly based and integrated with most of the Eclipse Modeling projects (see the «Relationship with other Eclipse Projects» section).
</p>

<p>
Finally, Sirius has been developed for Thales during the last few years by Obeo, already a strong contributor to the Eclipse projects.
</p>

<p>So, it feels just right to start this adventure in Eclipse.
</p>

<h3>Relationship with other Eclipse Projects</h3>

<p>The Sirius project is making heavy use of all the Eclipse Modeling projects, notably:</p>
<ul>
  <li>EMF Core as the basics for every model access and introspection, but also EMF Transaction and Validation.</li>
  <li>The GMF runtime and GMF notation projects are the basis of the dynamic graphical modeling capability of Sirius.</li>
  <li>Acceleo and MDT-OCL as the foundation of the query languages used to define the mapping between the models and the representations.</li>
  <li>Xtext is also supported as models are loaded and saved using their textual syntax, enabling concurrent use of diagram, table and textual editors.</li>
  <li>EEF (Extended Editing Framework) also integrates with Sirius in order to provide rich properties editing.</li>
  <li>Sphinx : Sirius brings a declarative definition of modeling workbenches which could benefit from being integrated in Sphinx.</li>  
</ul>

<h2>Initial Contribution</h2>

<p>The initial contribution includes :</p>
<ul>
  <li>Sirius runtime: Life-cycle management, query and dynamic interpretation of the environment descriptions.</li>
  <li>Sirius runtime IDE: Integration with the Eclipse IDE.</li>
  <li>Sirius editors: Tools to specify modeling workbenches.</li>
</ul>

<p>Thales holds the copyright of this code.</p>
  
 
<h2>Legal Issues</h2>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->
 
<h2>Committers</h2>

<p>All contributions will be distributed under the Eclipse Public License.</p>
<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->
 
<dl>We propose a co-lead for the Sirius project:
  <dt>Stephane Bonnet, Thales Global Services</dt>
  <dd>in charge of the team developing and deploying this technology within Thales.</dd>
  <dt>Pierre-Charles David, Obeo</dt>
  <dd>the technical lead of the project at Obeo.</dd>
</dl> 

<p>The following individuals are proposed as initial committers to the project:</p>

<ul>
	<li>Alex Lagarde, Obeo</li>
	<li>Benoit Langlois, Thales Global Services</li>
	<li>Cedric Brun, Obeo</li>
	<li>Esteban Dugueperoux, Obeo</li>
	<li>Florian Barbin, Obeo</li>
	<li>Hugo Marchadour, Obeo</li>
	<li>Jean Barata, Thales Global Services</li>
	<li>Julien Dupont, Obeo</li>
	<li>Laurent Redor, Obeo</li>
	<li>Matthieu Helleboid, Thales Global Services</li>
	<li>Maxime Porhel, Obeo</li>
	<li>Nathalie Lepine, Obeo</li>	
	<li>Phillipe Dul, Thales Global Services</li>	
	<li>Steve Monnier, Obeo</li>
	<li>Yann Mortier, Obeo</li>	
</ul>

<p>We welcome additional committers and contributions.</p>

<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Ed Merks</li>
	<li>Kenn Hussey</li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li>Airbus</li>
	<li>Soyatec</li>
	<li>Itemis AG</li>
	<li>Eike Stepper, ES Consulting</li>	
</ul>

<h2>Project Scheduling</h2>

<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->
<ul>
 <li>April 2013: code submission and IP review</li>
 <li>September 2013: first builds on the Eclipse infrastructure.</li>
 <li>November 2013: 0.9 release </li>
 <li>November 2013: declaration as part of the Luna simultaneous release</li>
 <li>June 2014: 1.0 release with Eclipse Luna</li>
</ul>

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>06-March-2013</td>
		<td>Document created</td>
	</tr>
	<tr>
		<td>11-March-2013</td>
		<td>Reformulated scope</td>
	</tr>
	<tr>
		<td>17-April-2013</td>
		<td>Add interested parties</td>
	</tr>
</table>
</body>
</html>