<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>EGF Proposal</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("EGF");
?>

<h1>Introduction</h1>
<p>
The EGF (Eclipse Generation Factories) component is a proposed open
source project under the <a href="http://www.eclipse.org/modeling/emft">EMFT</a>
project to provide a model-based generation framework.
</p>
<p>
This proposal is in the Project Proposal Phase (as defined in the <a href="http://wiki.eclipse.org/Development_Resources">Eclipse
Development Process</a> document) and is written to declare its
intent and scope. This proposal is written to solicit additional
participation and input from the Eclipse community. You are invited to
comment and/or join the project. Please send all feedback to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft</a>
newsgroup.
</p>
<h1>Background</h1>
<p>
For improving software development industrialization, a major step
consists in mass-producing software. This mass-production must be
customizable and support complex and heterogeneous generations. Given
the today available tools in Eclipse, the issue here is not to provide
a new transformation engine or DSL editor but to realize their
integration with fitted formalisms (e.g., generation pattern) and
mechanism (e.g., generation orchestration) for flexible software
mass-production.
</p>
<h1>Scope</h1>
<p>
This proposal focuses on an implementation project of an Eclipse
model-based generation framework, named EGF.
</p>
<h3>Definition</h3>
<p>
EGF federates generation around the pivotal notion of <b>factory
component</b>. A factory component is a unit of generation with a
clear objective of generation. It has a contract to declare the factory
component parameters. Generation data are organized by <b>viewpoints</b>,
such as generation pattern or model mapping declarations. The
generation orchestration is defined in a <b>production plan</b>.
A generation step of the production plan can either be a Java task or,
by assembly, a call to another factory component. A factory component
can be <b>edited</b> and <b>executed</b>.
</p>
<p>
A factory component encapsulates generation know-how. A <b>portfolio</b>
is a consistent set of factory components with a common generation
objective which is valuable for a development team or a user community.
The purpose is to create off-the-shelf factory components. For
instance, a technical infrastructure factory can provide all mechanisms
needed for the development of an application (e.g., transaction
management, persistence policy).
</p>
<p>
As a consequence, EGF is built on a two-layer architecture.
</p>
<ol>
<li>
<i>EGF engine</i> is the tool that allows editing and
executing factory
components. It has its own model and is mainly built on EMF. It also offers a generation
pattern viewpoint for model-to-text transformation.
</li>
<li>
A <i>Factory Component Portfolio</i> layer for solutions of
generation.
</li>
</ol>
<p>
<b>Example:</b> The following figure exemplifies a simple
generation. It shows a generation decomposition and assembly. Code
generation is realized by generation patterns, which supports pattern
inheritance. By default for the model/edit/editor generation, the EMF
generation is applied. This generation can be specialized by inherited
patterns.
</p>
<p>
</p>
<center><img src="case-study.png" height="511" width="800">
<p><i>Figure 1 - Example of generation</i></p>
</center>
<p></p>
<h3>EGF contributions</h3>
<p>EGF contributions can take two
kinds of different forms:
</p>
<ol>
<li>
<i>Development of Factory Component portfolios.</i>
<ul>
<li>
The most natural contribution is the development of factory components
and factory component portfolios.
</li>
<li>
With the generation patterns, further than the pattern inheritance
mechanism, combination of factory component generation allows merge of
generation, for instance for introducing specific behaviors in the
standard EMF generation.
</li>
</ul>
</li>
<li>
<i>Extension of EGF engine.</i>
<ul>
<li>
Extensibility of the default EGF viewpoints. By model extension, a
child viewpoint adds its own language and editor, such as deployment
specification, licensing policy or feature modeling. An associated
factory component can define one viewpoint execution mode. For
instance, generation patterns conform to a pattern language, are edited
with a pattern editor, and are executed with a pattern runtime factory
component.
</li>
<li>
All factory components depends on a root factory component. A new
default factory component can add its specific behaviors.
</li>
</ul>
</li>
</ol>
<p>
It becomes relevant to make the viewpoints cooperate. For instance, generation patterns can use a feature model for addressing the product line engineering.
</p>
<h1>Relationship with Other Eclipse Projects/Components</h1>
<h3>Transformation tools</h3>
<ul>
<li>
<i>Model-to-text.</i> EGF generation patterns are <a href="http://www.eclipse.org/modeling/m2t/?project=jet">JET</a>-based.
The target in this proposal is to also support <a href="http://www.eclipse.org/modeling/emf/docs/architecture/jet2/jet2.html">JET2</a>
and <a href="http://www.eclipse.org/modeling/m2t/?project=mtl">MTL</a>.
The EGF engine
infrastructure is designed to support other engines with different
editors and lifecycles (compiler vs. interpreter production lifecycle),
such as <a href="http://www.eclipse.org/modeling/m2t/?project=xpand">Xpand</a>.
</li>
<li></li>
<li><i>Model-to-model and text-to-model.</i> Tools
of this category,
such as <a href="http://www.eclipse.org/m2m/atl">ATL</a>,
<a href="http://wiki.eclipse.org/M2M/Operational_QVT_Language_%28QVTO%29">QVTo</a>,
<a href="http://wiki.eclipse.org/M2M/Relational_QVT_Language_%28QVTR%29">QVTr</a>
for model-to-model transformations, or <a href="http://www.eclipse.org/modeling/tmf/?project=xtext">Xtext</a>,
<a href="http://www.eclipse.org/gmt/tcs">TCS</a>
for text-to-model, can be used through Java tasks.
</li>
</ul>
<h3>Orchestration</h3>
<ul>
<li>
For complex orchestrations, <a href="http://www.eclipse.org/bpel">BPEL</a>
and <a href="http://wiki.eclipse.org/MDT-BPMN2-Proposal">BPMN2</a>
projects will be considered.
</li>
</ul>
<h1>Organization</h1>
<p>
We propose that this project be undertaken as part of an <b>Eclipse
Modeling Framework Technology</b> subproject.
</p>
<h2>Initial committers</h2>
<p>The following companies will contribute committers to get the
project started :</p>
<ul>
<li>Beno&#238;t Langlois, Thales/TCS/EPM, proposed project lead &#8211; <a href="mailto:benoit.langlois@thalesgroup.com">benoit.langlois@thalesgroup.com</a></li>
<li>Thomas Guiu, Soyatec &#8211; <a href="mailto:thomas.guiu@soyatec.com">thomas.guiu@soyatec.com</a></li>
<li>Xavier Maysonnave, Thales/TCS/EPM &#8211; <a href="mailto:xavier.maysonnave@external.thalesgroup.com">xavier.maysonnave@external.thalesgroup.com</a></li>
<li>Laurent Goubet, Obeo &#8211; <a href="mailto:laurent.goubet@obeo.fr">laurent.goubet@obeo.fr</a></li>
</ul>
<h2>Initial Contributions</h2>
<p>
Thales will provide the initial contribution which is already deployed
in an industrial context. These people have contributed to the iniital code:
<ul>
<li>Guillaume Brocard, Thales/TCS/EPM &#8211; <a href="mailto:guillaume.brocard@thalesgroup.com">guillaume.brocard@thalesgroup.com</a></li></li>
<li>St&#232;phane Fournier, Thales/TCS/EPM &#8211; <a href="mailto:stephane.fournier@thalesgroup.com">stephane.fournier@thalesgroup.com</a></li>
</ul>
</p>
<h2>Mentors</h2>
<ul>
<li>Wayne Beaton &#8211; <a href="mailto:wayne.beaton@eclipse.org">wayne.beaton@eclipse.org</a></li>
<li>Ed Merks &#8211; <a href="mailto:ed.merks@gmail.com">ed.merks@gmail.com</a></li>
</ul>
<h2>Interested Parties</h2>
<p>
The following organizations have expressed interest in the project :
</p>
<ul>
<li>Thales/TCS/EPM - <a href="http://www.thalesgroup.com">www.thalesgroup.com</a></li>
<li>Crescendo Technologies - <a href="http://www.crescendo-technologies.com/">www.crescendo-technologies.com</a></li>
<li>HiPeS - <a href="http://www.hipes.nl">www.hipes.nl</a></li>
<li>Obeo - <a href="http://www.obeo.fr">www.obeo.fr</a></li>
<li>Skyway Software - <a href="http://www.skywaysoftware.com/">www.skywaysoftware.com</a></li>
<li>Soyatec - <a href="http://www.soyatec.com">www.soyatec.com</a></li>
<li>University of Augsburg - <a href="http://www.uni-augsburg.de/en/">www.uni-augsburg.de</a></li>
</ul>
<h2>Developer and user communities</h2>
<p>
Critical to the success of this project is participation of interested
third parties to the community. We intend to reach out to this
community and enlist the support of those interested in making a
success of the EGF project. We ask interested parties to contact <a href="news://news.eclipse.org/eclipse.tools.emft">news://news.eclipse.org/eclipse.tools.emft</a>
to express interest in contributing to the project.
</p>
<h1>Tentative Plan</h1>
<p>
The first community technical preview release is scheduled in Summer,
2009.
</p>
</div>
</div>

<?php 
$html = ob_get_contents(); 
ob_end_clean();

# Generate the web page 
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html); 
?>

