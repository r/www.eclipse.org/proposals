<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">&nbsp; 
	
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Communication Framework");
?>

  <h1>Eclipse Communications Framework (ECF)</h1><br>


<p>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/">Eclipse 
  Development Process document</a>) and is written to declare the intent and scope 
  of a proposed project called the Eclipse Communications Framework Project, or 
  ECF. In addition, this proposal is written to solicit additional participation 
  and inputs from the Eclipse community. You are invited to comment on and/or 
  join the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ecomm">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ecomm</a> 
  newsgroup. <br>
</p>

   
    <h2>Description</h2>
  
   
     
      <h3>Project Goals</h3>
      <p>The goal of the ECF Project is to provide an open source framework to 
        support the creation of communications applications on the Eclipse platform. 
        The framework will consist of Eclipse plugins that provide high-level 
        APIs to support asynchronous and real-time messaging for human-to-human, 
        human-to-component, and component-to-component communications and collaboration. 
      </p>
      <h3>ECF-Enabled Applications</h3>
      <p>A wide variety of Eclipse applications will be easily creatable within 
        this framework: </p>
      <ul>
        <li><b>Human Communication and Collaboration:</b> Instant Messaging, Chat, 
          Application Sharing, File Sharing, Video/Audio Conferencing, others 
        </li>
        <li><b>Communication with Web and Peer-Based Services:</b> Access to web-based 
          services via the Eclipse platform such as Weblogs, RSS, Web-based Content 
          Creation, Web-based Project Management Systems, others </li>
        <li><b>Component-to-Component Communication:</b> Distributed Modeling, 
          Remote Debugging, Team Content Management, Team Workflow Applications, 
          others </li>
      </ul>
      <p>As part of the framework develoment and testing, the ECF team will implement 
        a reference subset of applications to demonstrate the utility, and usability 
        of the framework. We will also work with plugin developers, application 
        developers, and the Eclipse community to support the creation of other 
        applications that use this framework. Our ultimate goal is that plugin 
        developers use and/or extend this framework to allow them to build compelling 
        communications applications with the Eclipse platform.</p>
      <h3>Technical Goals</h3>
      <h4>Communication Components</h4>
      <p>The framework will provide a small set of high-level communications abstractions. 
        Rather than provide yet another messaging API, the ECF project will look 
        to provide plugin programmers with a component API for communications 
        applications so that high-level communications components (e.g. real-time 
        IM/chat, presence, file sharing, audio/video conferencing, etc) may be 
        reused in multiple application contexts and with multiple user interfaces. 
      </p>
      <h4>Interoperability</h4>
      <p>The framework will provide a simple API for accessing multiple IP-based 
        protocols, allowing plugin developers to focus on providing application-level 
        integration of communications features and the rapid composition of distributed 
        applications. </p>
      <p>To allow interoperability with existing servers and clients, ECF APIs 
        will provide extension points so that multiple protocols may be plugged 
        in to the framework, allowing Eclipse-based applications to interoperate 
        with other clients via standard protocols such as XMPP, SIP/SIMPLE and 
        others, while also being able to easily support communications with legacy 
        and proprietary systems. </p>
      <h4>Security</h4>
      <p>The ECF framework will provide for application-level flexibility in the 
        use of authentication, user and component identity, encryption, and authentication. 
        The framework will allow third parties to substitute alternative open 
        source and/or proprietary implementations of any/all of these security 
        mechanisms to support integration with existing security systems, and 
        the satisfaction of application-specific security requirements. </p>
      <p>ECF will also provide security mechanisms for authorization and runtime 
        restriction of fine-grained components for controlling the behavior of 
        potentially untrusted components. </p>
      <h4>Application Extensibility</h4>
      <p>The framework will support the runtime distribution of components and 
        dynamic introduction of new protocols to allow for dynamic composition 
        of communication and collaboration applications. </p>
      <p>In summary, the major technical goals of ECF will be:</p>
      <ol>
        <li><b>Simple-Yet-General Communications Abstractions:</b> To support 
          both peer-to-peer and client-server applications</li>
        <li><b>Interoperability:</b> Through the use of open, layered, replaceable 
          protocols</li>
        <li><b>Security:</b> Identity/Authentication, Encryption, and Authorization 
          for applications and fine-grained components</li>
        <li><b>Application Extensibility:</b> Via dynamic distribution of components</li>
        <li><b>Component/Plugin Architecture Integrated with Eclipse 3.0:</b> 
          Use of the OSGI Component and Service Models Present in Eclipse 3.0</li>
      </ol>
      <h3>People</h3>
      <p>The project expects to have commiters from Composent, Inc., Parity Communications, 
        and others. We are looking for others to participate in all aspects of 
        this project. If you are interested in participating, please contact one 
        of these team members and contribute to the mailing list.<br>
        The initial set of commiters will be: <br>
        <br>
<p>John Beatty<br>
<p>Scott Lewis (project lead)<br>
Composent, Inc.</p>
<p>Pete Mackie<br>Seaquest Software</p>
<p>Peter Nehrer<br>
S1 Corporation<br>
</p>
<p>Rhett Savage</p>
<p>Paul Trevithick<br>
Parity Communications</p>

      <h3>Status</h3>
      <p>The project has been formally proposed and is in the Creation Review 
        stage. It will be placed in the Technology PMC as an incubator project 
        to develop this framework, reference applications, and supporting tools. 
      </p>
      <h3>Platforms</h3>
      <p>ECF plugins will be built in Java and will be portable to any platform 
        supported by Eclipse. For components that might not run on Eclipse (e.g. 
        servers), all ECF-created code will be built to depend only upon pure 
        Java Standard Edition (1.4+). We also will look to support OSGI Foundation 
        as a minimum runtime platform. </p>
      <h3>Development plan</h3>
      <p>The plan for the ECF project is still being developed.</p>
      <p>We plan to follow a milestone based delivery schedule with incremental 
        progress visible in each milestone. Here is a rough outline of the time 
        line we expect to follow for milestones:</p>
      <p>M1 - Q4 2004<br>
        Initial release of framework plugins for identity/authentication, asynchronous 
        communication, and basic component abstractions </p>
      <p>M2 - Q1 2005<br>
        Deploy initial set of demonstration applications upon the communications 
        framework. Finalize APIs and extension points for identity/authentication, 
        asynch communication, and component abstraction after community usage 
        and review </p>
      <p>M3 - Q2 2005<br>
        Deliver additional demonstration applications, and a broader set of user-level 
        features. Finalize APIs for component model, extension points, and security. 
        Provide working, integrated Eclipse applications for instant messaging/chat, 
        file sharing, data/voice conferencing, and shared debugging</p>
      <p>M4 - Q3 2005<br>
        Revise and improve demonstration applications, update infrastructure plugin 
        APIs in response to community usage and feedback. Provide demonstration 
        application with Eclipse Modeling Framework-based shared model creation 
        and editing<br>
      </p>
    
  



</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
