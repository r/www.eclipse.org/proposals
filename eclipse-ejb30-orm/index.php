<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
  
     
      <h1> EJB 3.0 Object-Relational 
        Mapping Project</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("EJB3.0-ORM (Dali)");
?>
  
<h2>Introduction</h2>

<p>The EJB 3.0 Object-Relational Mapping Project is a proposed open source project 
  under the <a href="/technology/">Eclipse Technology Project</a>.</p>
<p>This project proposal is in the <a href="/projects/dev_process/">Proposal 
  Phase</a> and is posted here to solicit community feedback, additional project 
  participation, and ways the project can be leveraged from the Eclipse membership-at-large. 
  You are invited to comment on and/or join the project. Please send all feedback 
  to the <a
href="/newsportal/thread.php?group=eclipse.technology.ejb-orm">eclipse.technology.ejb-orm</a> 
  newsgroup. </p>

<h2>Project Goal</h2>
  
<p>The goal of this project is to add comprehensive support to the Eclipse Project 
  for the definition, editing, and deployment of Object-Relational (O/R) mappings 
  for EJB 3.0 Entity Beans.� EJB 3.0 O/R mapping support will focus on minimizing 
  the complexity of mapping by providing creation and automated initial mapping 
  wizards, and programming assistance such as dynamic problem identification.� 
  The implementation will be extensible so third party vendors can add to its 
  functionality.</p>
  <h2>Project Scope</h2>
  
<p>This project will provide tooling to map EJB 3.0 Entity Beans to relational 
  databases tables, as detailed in <a href="http://www.jcp.org/en/jsr/detail?id=220">JSR 
  220: Enterprise JavaBeans&#8482; 3.0 Persistence API</a>.� From a J2EE perspective, this tooling 
  would be an integrated component beside its peers in the JST project under the 
  larger umbrella of the WTP.� It will leverage the work being done under the 
  J2EE Core Model and Tools in the areas of J2EE Artifacts and Components and 
  J2EE Server Tooling.� As EJB 3.0 persistence is also intended to be the persistence 
  standard for J2SE, it will be necessary for this tooling to support non-J2EE 
  usage.</p>
  
<p>Session and Message Driven Bean configuration is not a part of this project, 
  as it is expected that the current JST support will be evolved to cover the 
  changes required by EJB 3.0.� O/R mapping is not applicable to these bean types 
  and so they are therefore out of scope.</p>
<p> Support for persistence technologies other than JSR 220 (e.g., JSR 222: Java&#8482; 
  Architecture for XML Binding (JAXB) 2.0) is out of scope.</p>
<p >The goal of the project is the development of tooling and frameworks to support 
  the construction of EJB 3.0 Entity Beans, not the development of an O/R mapping 
  framework.�� The proposed tools and frameworks are all design time only.� No 
  runtime libraries or frameworks are included in this proposal.� Runtime support 
  for EJB 3.0 will be provided by JSR 220 compliant application server implementations.</p>
  <p >Extension points will be provided to support vendor extensions to EJB 3.0; 
    however, while this proposal acknowledges and supports the need for vendor 
    extensions, the scope of this proposal is limited to the functionality defined 
    in the EJB 3.0 specification.</p>
  
<p>At the time of this writing, the EJB 3.0 specification has not been finalized 
  and changes are expected before the final draft is issued. Accordingly, the 
  scope of this proposal will track the draft specification and the tooling will 
  be validated against the EJB 3.0 reference implementation and available commerial 
  implementations.</p>
  <h2>Project Principles</h2>
  <p><b>Leverage the Eclipse Ecosystem</b>:� It is our goal to extend 
    the Eclipse user experience as well as leverage other Eclipse project functionality 
    where applicable.</p>
  <p><b>Vendor Neutrality</b>: We intend to build an EJB 3.0 tool that is not 
    biased toward any particular EJB container or persistence vendor.</p>
  
<p><b>Extensibility</b>:� It is our intention to provide a generic and extensible 
  set of editors and frameworks that provide a vendor neutral ability to add proprietary 
  O/R mapping features that extend the EJB 3.0 specification (e.g., advanced mappings 
  and querying provided by implementations such as BEA Weblogic, IBM WebSphere, 
  JBoss Hibernate, and Oracle TopLink).</p>
<p>Extensibility of the EJB 3.0 tooling will not be limited to add-ons extensions. 
  By being architected as a set of plug-in components, individual components (e.g., 
  Entity generation from tables) may be replaced by vendor specific implementations.</p>
  <p><b>Incremental Development</b>:� As this project will be subject 
    to an evolving specification, an agile development process will be employed 
    to manage and respond to changing requirements.</p>
  
<p><strong>Standards Compliance: </strong>The project tooling will be focused 
  on providing support for persistence Entities as defined by the JSR 220 Enterprise 
  JavaBeans&#8482; 3.0 specification.</p>
<p><strong>Inclusiveness:</strong> Project participation by all interested parties 
  is welcome. The initial committers, for example, are from three vendors that 
  compete agressively in the market but recognize the value of working together 
  on pre-competitive Eclipse-based tools and technologies.</p>
<h2>Project Description</h2>
  
<p>It is now generally accepted that tooling applied in the right scenarios can 
  increase developer productivity, saving time and money for their organizations.� 
  O/R mapping is one of these scenarios that has benefited from tool support in 
  the past from providing design time validation of mappings and configuration 
  to visually managing the mapping of large object models.� Examples of this type 
  of tool include the <a
href="http://www.oracle.com/technology/products/ias/toplink/mwdemos/index.html">Oracle 
  TopLink Workbench</a>, <a href="http://www.solarmetric.com/jdo/Kodo_JDO/kododevelopersworkbench.php">Solarmetric's 
  Kodo Development Workbench</a>, and the <a href="http://www.hibernate.org/Projects/HibernateTools">JBoss 
  Hibernate Mapping Editor</a>.� IDE�s will support annotations, but an O/R mapping 
  tool should go beyond this to offer further context and intelligence to aid 
  decision-making and reduce developer errors.</p>
  
<p>The specific intent of this project is to fill the current void in lightweight 
  tooling support for EJB 3.0 persistence.� The project will address this void 
  with a non-proprietary tooling solution that can be used to map to any EJB 3.0 
  compliant runtime implementation.� In addition, container and persistence vendors 
  and open source projects could extend this project with proprietary plug-ins 
  for their own advanced mapping or configuration support.� While the spec is 
  in flux, the project would target the reference implementation for EJB 3.0 O/R 
  Mapping.� See the diagram below for a visual representation.</p>
<p align=center> <img border=0 width=568 height=357
src="images/EJB30ToolingDiagram.jpg"
v:shapes="_x0000_i1025"> </p>
  <p>As stated above, this project is 100% tooling, and should be completely decoupled 
    from any particular runtime implementation.� </p>
  <p>Below are areas of targeted support for the project:</p>
  <B>EJB Specific Functionality</B>
  
<p>This section illustrates the type of mapping support intended, but is not meant 
  to be a comprehensive list of supported functionality.</p>
<ul>
  <li>Basic mapping (field/column) and relationship (foreign key) mapping support</li>
  <li>Embeddable Classes</li>
  <li> Entity Bean Inheritance</li>
  <li>Entity Manager (Query API Support) 
    <ul>
      <li>Named Queries</li>
      <li>EJBQL Queries</li>
      <li>Native Queries</li>
    </ul>
  </li>
  <li>Entity Bean Lifecycle management 
    <ul>
      <li>Callback and EntityListener configuration</li>
    </ul>
  </li>
  <li>Version Locking</li>
  <li>ID Generators</li>
  <li>Multi-table mapping </li>
  <li>LOB mappings</li>
  <li>Reading and writing of EJB 3.0 O/R Annotations</li>
  <li> Reading and writing of EJB 3.0 O/R XML Descriptors</li>
  <li>Creation and editing of persistence archives (.par).</li>
  <li>Creation and editing of persistence.xml files.</li>
</ul>
<B>Automation/Generation Functionality</B>
<p>Wizards will be provided to assist in prototyping and jump starting of projects.� 
  Features like automated initial mapping of classes to tables based on syntactic 
  name similarity remove much of the basic work associated with O/R mapping.</p>
  
<ul>
  <li>Automated initial mapping process for basic mappings, relationship mappings, 
    and inheritance</li>
  <li>Generation of Entity Beans from table definitions</li>
  <li>Generation of table definitions from Entity Beans</li>
</ul>
<B>Additional Functionality</B>
  <p>Developer productivity will be enhanced with the following features.</p>
  
<ul>
  <li>Coding assistance for annotation parameters (e.g., valid table/column names)</li>
  <li>Mapping assistance and validation through dynamic problem reporting</li>
  <li>Ability to define and manage persistence needs of large projects</li>
  <li>Visual EJBQL Builder</li>
  <li>Ability to keep EJBQL queries in sync with Java class definitions in the 
    face of refactoring</li>
</ul>

<b>Extension Points</b>
  <p>The project should be architected as a set of plug-ins, each of which provides 
    the ability for other groups or vendors to further extend and customize the 
    functionality in a well-defined manner.  In particular, it is 
    expected that the core mapping functionality would be extended by:</p>
  
<ul>
  <li>EJB 3.0 container tools</li>
  <li>EJB 3.0 O/R persistence tools</li>
</ul>
 
 
<p>Extension points will be defined to allow tool vendors and open source projects 
  to extend the core mapping facilities with additional functionality. For example, 
  extension points would likely be defined for the following areas:</p>
 
<ul>
  <li>Addition of mapping types</li>
  <li>Addition of descriptor types</li>
  <li>Extension of descriptor configuration with policies</li>
  <li>Addition of query types and configuration</li>
  <li>Preferences and other standard extensions</li>
</ul>

<B>Integration Points with Other Eclipse Projects</B>
  <p>Given the scope of the project, the EJB 3.0 O/R mapping tools will interact 
    with and leverage the following existing Eclipse projects:� </p>
  
<ul>
  <li><a href="/jdt/index.html">JDT (Java Development Tools) 
    Core, UI, and LTK (Language ToolKit)</a> 
    <ul>
      <li>Java class metadata</li>
      <li>Refactoring support</li>
      <li>UI Integration</li>
    </ul>
  </li>
  <li><a href="/proposals/eclipse-dtp/">DTP (Data 
    Tools Project)</a> and/or <a href="/webtools/wst/main.html">WST 
    (Web Standard Tools)</a> � rdb component 
    <ul>
      <li>Table Definitions</li>
      <li>Login specifications</li>
      <li>Query execution support</li>
    </ul>
  </li>
  <li><a href="/webtools/jst/main.html">JST (j2ee Standard 
    Tools)</a> 
    <ul>
      <li>J2EE Core Model</li>
      <li>J2EE Server Models</li>
      <li>EJB packaging</li>
      <li>Application Deployment</li>
      <li>Annotation support</li>
    </ul>
  </li>
</ul>
<B>High Level Use Cases</B>
  <ul>
    <li>Map EJB 3.0 Entities, test in J2EE environment</li>
    <ul>
      <li>Connect to Database</li>
      <li>Import Table Definitions</li>
      <li>Perform automated initial mapping on existing EJB 3.0 Entities</li>
      <li>Customize mappings as necessary</li>
      
    <li>Package EAR and PAR contents</li>
      <li>Run, Test, Debug EJB 3.0 Entity Beans on J2EE server</li>
    </ul>
    <li>Generate EJB 3.0 Entities from Tables, test in J2SE environment</li>
    <ul>
      <li>Connect to Database</li>
      <li>Import Table Definitions</li>
	<li>Generate EJB 3.0 Entities (with mappings) from Table Definitions</li>
      <li>Customize mappings as necessary</li>
      <li>Run, Test, Debug EJB 3.0 Entity Beans on J2SE JVM</li>
    </ul>
    <li>Generate Tables from EJB 3.0 Entities, test in J2EE environment</li>
    <ul>
	<li>Generate Tables from existing EJB 3.0 Entities</li>
	<li>Perform automated initial mapping on existing EJB 3.0 Entities
      <li>Customize mappings as necessary</li>
	<li>Package EAR and PAR contents</li>
      <li>Run, Test, Debug EJB 3.0 Entity Beans on J2SE JVM</li>
    </ul>
  </ul>
  <h2>Organization</h2>
  
<p>We propose this project should be undertaken as an incubator within the Eclipse 
  Technology Project. EJB 3.0 O/R mapping will be the Java standard persistence 
  API for both J2EE and J2SE. As such, this project will complement WTP and DTP, 
  which are found in the Eclipse Tools Project, and JDT which is a part of the 
  core Eclipse project.</p>
<p>Initially, EJB 3.0 Expert Group members and other individuals and organizations 
  that have serious interest will be solicited to participate.� This list would 
  cover a broad base of organizations interested in EJB 3.0, including J2EE vendors 
  and third party persistence providers.</p>
  
<p> The submitters of this proposal welcome interested parties to post to the 
  <a
href="/newsportal/thread.php?group=eclipse.technology.ejb-orm">eclipse.technology.ejb-orm 
  </a> newsgroup and ask to be added to the list as interested parties or to suggest 
  changes to this document. </p>
  
<p><B>Initial Committers</B> </p>
<p>The three initial contributors to this project proposal are all members of 
  the JSR 220 EJB 3.0 expert group and bring with them significant practical experience 
  in the area of O/R mapping technology and tooling.</p>
<ul>
  <li> Oracle&#8212;Oracle has committed to providing project leadership, development 
    resources experienced in the construction of commercial quality O/R mapping 
    tools, and an initial codebase to draw from.</li>
  <li>SolarMetric&#8212;SolarMetric is contributing O/R mapping expertise and 
    development resources experienced in commercial O/R tool development.</li>
  <li>JBoss&#8212;JBoss is supporting the project through the committment of experienced 
    O/R tool development resources and Eclipse development expertise.</li>
</ul>
<p>The project leadership will continue to pro-actively contact interested parties 
  to invite their contribution of code and development resources.</p>
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

