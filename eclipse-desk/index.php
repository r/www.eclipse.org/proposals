<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
  
  
    <h1>Eclipse Desktop Integration
    Project</h1>
 
 <?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Desktop Technology Integration");
?>
  
  
    <h2>Introduction</h2>
  
 <p>The Eclipse Desktop Integration Project is a proposed open source
          project under the <a href="/technology/">Eclipse Technology Project</a>. </p>
        <p>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/">Eclipse
          Development Process document</a>) and is written to declare its intent
          and scope. This proposal is written to solicit additional participation
          and input from the Eclipse community. You are invited to comment on
          and/or join the project. Please send all feedback to the
        <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.desk">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.desk</a> newsgroup.</p>
        <p> </p>

  
    <h2>Background</h2>
       

<p>
Recently the industry has seen a resurgence in the use of Java technology
to create desktop applications.
One advantage of Java is its platform independence, which gives users the freedom to
chose alternative platforms and vendors based upon their changing business needs.
However, platform independence is a two-edged sword.
Most users expect applications to seamlessly
integrate into the desktop and the host operating system,
following its user interface guidelines and conventions.
This is especially true for users of business applications.
For Java to gain more design wins with this audience it needs to be just as
tightly integrated as a native application written in other languages like C++ or C#.
</p>
<p>
The Java platform has enabled a certain level of seamless desktop integration with
functionality such as Swing look and feels,
drag and drop, and clipboard support.
Java Web Start comes with features to place application shortcuts into
the start menu and on the desktop.
Developers who want to go beyond what already exists in the standard Java platform,
however, have limited choices.
</p>
<p>
Eclipse's SWT provides additional native capabilities such as
a Browser widget that uses the platform browser,
and a way to add icons in the system tray.
This is a good start but more is needed, for example
the ability to associate file types with Java applications,
to launch those applications by double-clicking on files in the desktop
or file explorer.
</p>



      

  
  
    <h2>Description</h2>
    
      
<p>
To enable better native platform integration,
we propose the Desktop Integration project,
an open-source project hosted on eclipse.org.
Its mission is to take cross-platform Java desktop integration to the next level.
</p>
<p>
The project will produce a set of cross-platform APIs that abstract the contracts
between an application and the desktop it runs on.
Desktop integrated applications will run on any platform
that the API has been ported to and will
realize the behavior associated with these contracts in a way that's suitable
for each particular desktop environment.
</p>
<p>
This approach is possible because many desktop concepts
are implemented across all environments.
Additionally some desktop services are an enhancement
to an application rather than necessary to its operation.
Such features can be left unimplemented on platforms where the service is not available.
</p>
<p>
Desktop Integration is an exploratory project.
While you're welcome to use it in your products, be aware that all current
APIs are subject to change.

</p>

      
    

  
    
      <h2>Interested Parties</h2>
    
    
      <h2>Suggested Project Lead and Committers</h2>
    
    
      <p>This section captures the list of organizations
          that have expressed interest in the project and/or its components, and
          as such will be updated periodically to reflect the growing interest
          in this project.<br /><br />
          We, the submitters of
          this proposal, welcome and encourage interested parties to post to the
      <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.desk">
      http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.desk</a>
          newsgroup with comments, critiques, questions, and requests to be added to the
          list as interested parties. </p>
        <p><strong>Initial Committers</strong></p>
        
          
            Ed Burnette (Project Lead)
             <br />
            ed.burnette at sas.com
          
          
            Wayne Parrott
             <br />
            wayne at genuitec.com
          
          
            Todd Williams
             <br />
            todd at genuitec.com
          
          
            Mikhail Shynaderman
             <br />
            mschnayd at yahoo.com
          
          
            Jed Anderson
             <br />
            jed.anderson at codenovo.com
          
      
        <p> </p>
        
    
  

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

