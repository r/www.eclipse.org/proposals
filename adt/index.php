<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">


<h1>Ada Development Tools (ADT)</h1>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php"); 
generate_header("ADT"); 
?>

<h2>Introduction</h2>

<p>The Ada Development Tools is a proposed open source project
under the <a href="http://www.eclipse.org/tools/">Eclipse
Tools Project</a>.</p>

<p>This proposal is in the Project Proposal Phase (as defined in
the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse
Development Process document</a>) and is written to declare its
intent and scope. This proposal is written to solicit additional
participation and input from the Eclipse community. You are invited to
comment on and/or join the project. Please send all feedback to
 the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.adt">http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.adt</a>
newsgroup. &nbsp; </p>

<h2>Background</h2>

<p>The Ada language was designed by a team led by Jean Ichbiah at CII
Honeywell Bull and became an ISO standard in 1983.&nbsp;Ada is a
type safe
procedural programming language with support for many object oriented
features, as well as support for runtime checking, exceptions, generics
and parallel processing. Subsequent revisions to the language, commonly
known as Ada95 and Ada
2005, were created to correct perceived shortcomings to the language
and strengthen support for OO programming practices. Ada is frequently
found in mission-critical and safety-critical systems such as those
used in avionics, aeronautics and rail transportation. </p>


<p>Ada compilers and development environments are available as proprietary
commercial products as well as open source projects. Different Ada
environment providers often have their own IDE. In fact, due to the
longevity of Ada, some providers&nbsp; offer multiple IDEs for
their
various Ada tools. The same challenges (editor functionality,
integration with third party tools and CM systems, displaying
debugging information, etc.) have been faced and solved in a myriad of
ways, with varied results.</p>


<h2>Scope</h2>

<p align="left">The objectives of the ADT project are to: </p>

<ul>
  <li> 
    <p align="left">Create a standard, vendor
neutral&nbsp;Ada
development environment for Eclipse. ADT should maintain as close as
possible, the look and feel of the CDT and JDT, in order to mesh well
with the Eclipse environment, as well as aid developers who need
multi-language capabilities, or who need to transition between
languages.</p>
  </li>

  <li>   
    <p align="left">Provide a standard Ada environment
with which third
party tool developers can easily work, regardless of the choice of
underlying compiler technology. Such tools would include, but are not
limited to, modeling tools, editor tools, static and dynamic code
analysis tools (ex coverage, profiling), refactoring tools,
configuration management tools.&nbsp;</p>
  </li>
  <li>   
    <p align="left">Act as an additional reference for the
ongoing
process of making the Eclipse framework more able to easily support a
variety of native and embedded language development environments
(through interaction with projects such as CDT, DSDP and Safari.)</p>
  </li>
</ul>
<h2>Description</h2>
<p align="left">We propose to contribute the sources to
AonixADT as a
baseline for the Eclipse
ADT project.&nbsp;AonixADT, modeled after the CDT and JDT, is a
commercial plugin for&nbsp;Ada development. It has been in
development
since 2004 and in use by industrial customers since 2005. AonixADT
currently provides supports for&nbsp;ObjectAda (Aonix's own Ada95
offering)&nbsp;as well as&nbsp;GNAT
"commercial" (GMGPL'ed) and &nbsp;non-commercial (GPL'ed) open
source
offerings.&nbsp;GNAT is the Ada component of GCC, the Gnu Compiler
Collection.&nbsp;AonixADT v3.2.1 is available on Windows, Linux x86
and
Sparc
Solaris, and supports native as well as cross (i.e. embedded)
development.&nbsp; </p>
<p align="left">AonixADT's
core features include Ada project configuration and navigation, Ada
builder, customizable project creation wizards, coloring Ada95
syntactic editor with semantic code assist and semantic browse/search
capabilities, Ada debugger interface, an Ada source reformatter as well
as support for various Eclipse/CDT basic functionalities (tasks,
bookmarks,
error navigation from the problem view, etc.)&nbsp;</p>
<p align="left">AonixADT is organized into core features,
and separate
toolchain support. The primary focus of the ADT project will be on core
functionalities. The integration of specific Ada toolchains will be
done either as components within ADT (assuming sufficient community
interest), or by interested parties (vendors, developers) outside of
the context of the ADT project.&nbsp; It is the goal of ADT to
provide
a&nbsp; well documented manner to add toolchains, and to be
structured
and developed in such a way as to make common core functionality
available to all toolchain components. </p>

<h2>Organization</h2>
<h3>Mentors</h3>
<ul>
<li><b>Doug Schaefer</b> (QNX, <a href="mailto:dschaefer@qnx.com">dschaefer@qnx.com</a>), Tools PMC, CDT Project Lead</li>
<li><b>Doug Gaff</b> (Windriver, <a href="mailto:doug.gaff@windriver.com">doug.gaff@windriver.com</a>), DSDP PMC Lead</li>
</ul> 
<h3>Committers</h3>

<p align="left">The initial committers will initially
focus on
re-architecturing/extending the&nbsp;code base to provide an open,
well
documented API.&nbsp; Our agile development process will follow
eclipse.org's standards for openness and transparency.&nbsp; Our
goal
is to provide the infrastructure and APIs needed to allow the
integration of additional tools. We also plan to help improve the
Eclipse platform by submitting patches and extension point
suggestions.&nbsp; The initial team will consist of:</p>

<ul>
  <li>    
    <p align="left"><b><span style="font-weight: 400;"><span style="font-weight: bold;">Tom
Grosman</span> (Aonix, <a href="mailto:grosman@aonix.fr">grosman@aonix.fr</a>): </span>project
lead</b></p>
  </li>

  <li> 
    <p align="left"><b><span style="font-weight: 400;"><span style="font-weight: bold;">Adam
Haselhuhn</span></span>&nbsp;<span style="font-weight: 400;">(Aonix, <a href="mailto:haselhuhn@aonix.fr">haselhuhn@aonix.fr</a>) </span></b> <span style="font-weight: bold;"></span> <span style="font-weight: bold;"></span></p>
</li>

 <li> 
    <p align="left"><b><span style="font-weight: 400;"><span style="font-weight: bold;">Lisa Jett</span></span>&nbsp;<span style="font-weight: 400;">(DDC-I, <a href="mailto:ljett@ddci.com">ljett@ddci.com</a>) </span></b> </p>
</li>

<li> 
    <p align="left"><b><span style="font-weight: 400;"><span style="font-weight: bold;">Mandy McMillion</span></span>&nbsp;<span style="font-weight: 400;">(CohesionForce, <a href="mailto:amcmillion@CohesionForce.com">amcmillion@CohesionForce.com</a>) </span></b> </p>
</li>
<li> 
    <p align="left"><b><span style="font-weight: 400;"><span style="font-weight: bold;">Quentin Ochem</span></span>&nbsp;<span style="font-weight: 400;">(AdaCore, <a href="mailto:ochem@adacore.com">ochem@adacore.com</a>) </span></b> </p>
</li>
<li> 
    <p align="left"><b><span style="font-weight: 400;"><span style="font-weight: bold;">David Phillips</span></span>&nbsp;<span style="font-weight: 400;">(CohesionForce, <a href="mailto:dphillips@CohesionForce.com">dphillips@CohesionForce.com</a>) </span></b> </p>
</li>
<li> 
    <p align="left"><b><span style="font-weight: 400;"><span style="font-weight: bold;">Pat Rogers</span></span>&nbsp;<span style="font-weight: 400;">(AdaCore, <a href="mailto:rogers@adacore.com">rogers@adacore.com</a>) </span></b> </p>
</li>

</ul>



<p align="left"><b>Interested parties</b>
</p>



<p align="left">We have had discussions with various
customers and
members of the Ada community who have expressed interest in
contributing to an open source ADT project. As this project proposal is
publicized, we expect to be able to name specific organizations and
people who will be interested in becoming committers, contributors,
testers and users. Here are the types of people and organizations who
would be interested-&nbsp;
</p>



<ul>



  <li>
    
    
    <p align="left">All Ada vendors (either with or
without an existing Ada plugin product) </p>



  </li>



  <li>
    
    
    <p align="left">The Gnu Ada open source project </p>



  </li>



  <li>
    
    
    <p align="left">The Mac Ada open source project </p>



  </li>



  <li>
    
    
    <p align="left">Makers of complementary Ada
development tools (analysis tools, refactoring tools, etc.) </p>



  </li>



  <li>
    
    
    <p align="left">Current users of AonixADT</p>



  </li>



  <li>University faculty and students</li>



</ul>



<p align="left"><b>Developer community</b>
</p>



<p align="left">The developer community will be mostly
drawn from the list above. Additionally, since a skillset combining Ada
and Eclipse plugin development experience is almost certainly
restricted to a handful of individuals at this point, developers with
knowledge of the Eclipse API and Eclipse plugin development, but who
may not be familar with Ada (ex- JDT, CDT, Photran developers) are
solicited.&nbsp;</p>


<p align="left">We are expecting and will actively pursue during the
proposal and incubation phases, active participation from Ada vendors. It is possible that they may not be able to
budget a full-time committer, but perhaps each vendor can contribute
(funding, or a partly funded developer) in order to create one or
more full-time committers.
</p>



<p align="left"><b>User community</b>
</p>



The existing Ada developer community will be the primary user base.
This includes an important presence in academia since Ada is
frequently used in programming courses. <br>



<br>



In addition, Aonix has made available a free download of AonixADT. On
average 50 people a week download the product, leaving a valid email
address. These 1000+&nbsp;downloaders will be contacted to inform
them
of the proposal and solicit their participation and/or comments.

<h2>Tentative Plan</h2>



<p>This initial plan is based on the equivalent of three to four full-time committers.
</p>



<p>1) ADT 0.5.0 Initial&nbsp;release.&nbsp;10/2007.</p>



<p style="margin-left: 40px;">The goal of this release is
to bring the initial code contribution under the Eclipse project
framework and make sure that the resources, procedures and roles
necessary to&nbsp;release an Eclipse project are understood,
defined,&nbsp;put in place and functioning properly. To incite the
user community to take this initial version of ADT out for a spin,
0.5.0 will include initial versions of ObjectAda and GNAT toolchain
components. It is anticipated that the committers will be coming up to
speed during the development of this release, so part of the role of
the committers already familar with ADT will be to help the other
committers (and contributers) to obtain a working understanding of ADT
internals.&nbsp;</p>

<p>2) ADT 0.6.0 Multi-vendor support.&nbsp;01/2008.</p>
<p style="margin-left: 40px;">The goal of this release will be to produce a version of Hibachi that is usable
with <i>at least</i> all the compilers provided by the committers' companies. No major new functionality is 
planned, however bugs discovered in 0.5.0 will be addressed.</p>

<p>3) ADT 0.9.0 CDT / API Re-architecture.&nbsp;06/2008.</p>
<p style="margin-left: 40px;">By this point, the committers will be far enough along the learning curve to be 
able to re-architecture Hibachi to take 
advantage of the latest CDT developments and to create a stable, robust set of APIs.</p>

<p>4) ADT 1.0.0 -&nbsp; Robust DSDP Integration.&nbsp;10/2008.
&nbsp;</p>
<p style="margin-left: 40px;">For its first major release, Hibachi must support embedded development as well as it does native development. 
This release is scheduled to be able to take advantage of the improvements that DSDP will see in the Ganymede release.</p>


<h2>Codename</h2>


<p style="font-style: italic;"><big><b><span style="font-weight: 400;">As a tribute to Jean Ichbiah the
designer of Ada,&nbsp;we propose to give ADT the codename
"Hibachi".&nbsp;Hibachi is:</span></b><br>



a) A small portable charcoal grill (US) <br>



b) Slang for a basketball player "in the zone"<br>



c) An anagram for (Jean) Ichbiah, b. 25 March 1940 &nbsp;d. 26
January 2007</big></p>


</div>
</div>

<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
