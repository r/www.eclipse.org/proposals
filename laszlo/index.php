<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn"><P STYLE="margin-bottom: 0in">&nbsp; 
</P>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("IDE for Laszlo");
?>

            <h1>Laszlo Eclipse Frameworks</h1>
	
<h2>Introduction</h2>

The Laszlo Eclipse Frameworks Project is a proposed open source project to be incubated under the
Eclipse <a href="/technology/">Technology Project</a>.

<p> The proposal is posted here to solicit community feedback, additional project participation, and ways the project can be leveraged from the Eclipse membership-at-large.
You are invited to comment on and/or join the project.
Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.laszlo">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.laszlo</a> 
newsgroup.
</p>

<h2>Background</h2>
<p>
Rich internet client platforms are becoming increasingly popular, as witnessed by the success of the OpenLaszlo platform, 
an open source, XML-native platform for 
developing rich Internet applications that run in any modern Web browser.
OpenLaszlo has become a reliable platform for thousands of developers, creating applications with the rich user interface capabilities of desktop client software and the instantaneous no-download Web deployment of HTML.
To date, millions of users are have used applications developed for the OpenLaszlo platform. 
See the <a href="http://www.openlaszlo.org" target="_new">OpenLaszlo</a> project page for more information 
</p>
<p>
It is important to have the proper eclipse frameworks in place to support the OpenLaszlo platform, and other rich internet client platforms. 
These frameworks would allow integration of the client development and the server development, or allow integration of various
rich internet client technologies.
</p>
<h2>Description</h2>
<p>
The Laszlo Frameworks subproject's goal will be to develop a framework for development of 
the OpenLaszlo platform, and other rich client platforms, with full support for editing, refactoring,
and debugging.  The Frameworks would be an extensible such that new language features can easily be added, or other   
xml/script based Rich Internet Markup Languages, such as XUL or XForms could be plugged in.
</p>

<p>
Another goal of this project will be to provide the ability to easily and quickly define Eclipse UI Plugins 
(extension point for views,wizards,dialogs,property/preference pages) using the LZX (Laszlo xml) markup, without writing any Java code. 
While this is not the primary goal of the project, it has several advantages:
<ul>
<li>Provides added productivity to framework developers such that more functionality can quickly and easily be added.
</li>
<li>Could possibly benefits the entire eclipse developer community with the added productivity .
</li>
</ul> 

<p>
The frameworks would allow creation and testing of OpenLaszlo and other rich client applications . 
Laszlo frameworks would also provide a rich editing environment for the LZX mark-up language. 
 The editing features would provide content assistance, syntax highlighting, and code formatting for both XML and script.
<p>
<p/>
In addition, the Laszlo frameworks would provide visual editing (WSYIWYG) frameworks.
 It would support markers for reflecting compilation and syntactical errors.  
<p/>
In addition to the editor, the following views would aslo be provided:
<ul>
	<li>The Palette View would enable developers to drag and drop new XML/LZX elements into the editor.</li>
	<li>The Properties View would be used to edit the attributes of the selected element in the editor.</li>
	<li>The Outline View would the document as a tree, allowing the developer to make changes to the document's structure.</li>

</ul>
<p/>
<p>
The frameworks would provide typical debugging capabilities to the extent allowed by the runtime.  This include
variable inspection, expression evaluation, and console output.
<p/>

<p>
A major focus of this project will be to bring the broader community
of tool developers, application specialists, testers, and day-to-day OpenLaszlo programmers into the development and refinement of this tool.
</p>

<h2>Proposed Design</h2>
<p>
The Laszlo Frameworks would be be based on the WebTools project, specifically the Structured Editor capabilities (XML Editing).
A schema-like XML file would define the xml language grammar (such as lzx).  This file would contain the necessary information 
to provide rich editing capabilities on that grammar.  This would include such information such as what icon is associated with
a tag, what kind of edit field should be used to edit an attribute, and what are the valid drag drop combinations.  
Different versions of this file could be used to provide editing of different grammars, such as XUL, or XForms.
</p>
<p>The frameworks would tightly integrate XML tags and scripting, such as JavaScript.  It would know what parts of a document are
script, vs what parts are XML.  It would maintain an internal model
of the script, similar to the way the JDT models the Java model.  This script model would be kept in sync with the XML Model.
</p>
<p>
One part of the frameworks would be an SWT renderer of LZX.  This would allows certain plugins or extension points to be implmented
in XML (LZX).  For instance, a "view" extension-point would point to the LZX view renderer class, and contain the LZX to define the
view.  As an example:
<pre>
&lt;xmp&gt;
    &lt;extension
         point=&quot;org.eclipse.ui.views&quot;&gt;
      &lt;view
            name=&quot;A LZX View&quot;
            class=&quot;org.eclipse.laszlo.ViewRenderer&quot;
            id=&quot;org.eclipse.lzxview&quot;&gt;
            &lt;canvas&gt;
              &lt;checkbox&gt;a option&lt;/checkbox&gt;
            &lt;/canvas&gt;
      &lt;/view&gt;
&lt;/xmp&gt; 
</pre>
<p>
The LZX renderer would interpet LZX tags, generate the corresponding SWT controls,
and handle actions appropriately.  The renderer would possibly use the rhino JavaScript engine for it's JavaScript support.
It could also use the Bean Scripting Framework to expose eclipse Java objects (i.e. IResource) to the script.
</p>

<h2>Extension Points</h2>
<p>
The project will define appropriate extension points that would enable others to provide 
value-added functionality. These include:
<ul>
<li>ability to "plug-in" a different grammer other than LZX, such as XForms or XUL.  The grammar would be defined in an
xml file, so this would not involve writing Java code.
</li> 
<li>ability to "plug in" a different language.  This would enable the suppor for scripting languages other than JavaScript.
</li> 
<li>ability to integrate language validators. This would allow for inline validation as well as syntax highlighting for pluggable models of other languages.
</li> 

<li>ability to extend the communication protocol for moving visual
elements. By opening the protocol that exists to communicate between
the running laszlo application and the framework, this allows other
kinds of embedded applications to communicate state change information
to and from Eclipse, and keep the editor's data model in sync. This
could be extended for use by many kinds of information, not limited to
visual state.
</li> 

<li>ability to create debugging tools based on the remote debugger
protocol, and to connect romotely to an application for debugging in
real time. The framework includes a debugging protocol for
communicating debug state information between Eclipse and a running
Laszlo application.  By extending this portocol, other kinds of
applications can provide debugging information to Eclipse.  For
example, an applet, even one residing on another machine, could be
debugged if it was compiled with a small stub using the Eclipse remote
debugging protocol.</li>

<li>ability to "plug-in" third-party debugging tools. The debugging
protocol can also be extended to define interfaces that allow third-party
vendors to plug in existing debugging tools. </li>

<li>ability to "plug-in" compiler implementations or versions.
</li> 

<li>ability for a third party vendor to add visual components and
component libraries, by extending the framework to accept UI representations of new components.</li>

<li>ability to create custom property editors for runtime visual
components, by extending the framework to allow control over which
properties and what types of properties are available.</li>

<li> ability to include listeners for runtime events and logging operations, etc.
</li> 
</ul>


<h2>Status</h2>
<p>
IBM is offering its IBM technology preview <b>IDE for Laszlo</b> as an initial code base (see
<a href="http://www.alphaworks.ibm.com/tech/ide4laszlo">http://www.alphaworks.ibm.com/tech/ide4laszlo</a>).
IDE for Laszlo is a technology preview of an Eclipse-based development environment for creating, editing, debugging, and testing 
applications based on the LZX declarative mark-up language.
<p/>
<p>
IBM has produced two technology preview releases. The first release was based upon the IBM initial contribution to the Eclipse web tools project,
and Eclipse 3.0.
The second release added the visual editing of OpenLaszlo Applications.
The latest release (2.1) was based on the WebTools 0.7.
</p>

<p>
A demonstration and a gallery of screenshots can be 
found at: <a href="http://www.alphaworks.ibm.com/tech/ide4laszlo">http://www.alphaworks.ibm.com/tech/ide4laszlo</a>.<br>
</p>

<h2>Development Plan</h2>
<p>
The goals up to the point of this technology preview have been to 
develop a functional feature and plug-in set
that supports editing and debugging.
These goals have been met.
</p>
<p>
A formal development plan is being formulated, which includes:
<ul>
<li>Eclipse (Swt) rendering of LZX, implement UI plugins in LZX markup, possibly within the plugin.xml itself 
</li>
<li>refactoring support
</li>
<li>define extension points for plugging in other xml/script based UI markups.
</li>
<li>compiler performance improvements
</li>
<li>improved debug support - JavaScript code stepping, breakpoints, live code update, http request monitoring, extending the debugging protocol
</li>
<li>Quick navigation to named elements ("meta-dot")
</li>
<li>WYSIWYG interface to performance tools
</li>
</ul>

<h2>Relationships to other projects</h2>
<p>
This subproject is built on top of the Web Tools project (and thus it's prereqs).
</p>

<h2>Interested Parties</h2>
                <p><b>Prospective Interim Leads</b><ul>
                  <li>Phil Berkland, IBM (berkland at us.ibm.com)</li>
                  <li>Amy Muntz, OpenLaszlo.org (amuntz at laszlosystems.com)</li>
                  </ul>
                <b>Interested Organizations</b><ul>
              <li><a href="http://www.ibm.com">IBM</a></li>
              <li><a href="http://openlaszlo.org">OpenLaszlo.org</a> 
		   (<a href="http://laszlosystems.com">Laszlo Systems, Inc.</a>)</li>
            </ul>
	<p><b>Potential Committers / Contributors</b></p>
            <ul>
              <li>Max Carlson, OpenLaszlo.org (max at laszlosystems.com)</li>
              <li>Phil Berkland, IBM (berkland@us.ibm.com)</li>
              <li>Gino Bustelo, IBM (lbustelo@us.ibm.com)</li>
              <li>Don Sedota, IBM (sedota@us.ibm.com)</li>
              <li>Henry Minsky, OpenLaszlo.org (hqm at laszlosystems.com)</li>
              <li>Amy Muntz, OpenLaszlo.org (amuntz at laszlosystems.com)</li>
              <li>Oliver Steele, OpenLaszlo.org (osteele at laszlosystems.com)</li>
              <li>P.T. Withington, OpenLaszlo.org (ptw at laszlosystems.com)</li>
           </ul>

<P><BR><BR>
</P>
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
