<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
	
<h1>Application Lifecycle Framework</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Application Lifecycle Framework");
?>	

						<p>
							This Eclipse project proposal (refer to the <a href="/projects/dev_process/">
								Eclipse Development Process Document</a>) is posted to declare the intent 
							and scope of a Technology PMC Project called the Application Lifecycle 
							Framework project (ALF). In addition, 
							this proposal is written to solicit additional participation and input from the 
							Eclipse community. You are invited to comment on and join the project. Please 
							send all feedback to <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.almiif">
							http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.almiif</a>.</p>

<h2>Background</h2>
						<p >An <b> Application Lifecycle</b> is the continuum of activities 
							required to support an enterprise application from its initial inception 
							through its deployment and system optimization.&nbsp;&nbsp;
						</p>
						<p > The <b> Application Lifecycle 
							Management</b> (ALM) integrates and governs the planning, definition, design, 
							development, testing, deployment, and management phases throughout the 
							application lifecycle.&nbsp; Currently, the application lifecycle is supported 
							by numerous heterogeneous, multi-vendor applications which are integrated in a 
							point-to-point manner.&nbsp; Point-to-point integrations are adequate only for 
							small numbers of integration endpoints and typically create more complexity in 
							developing and managing tools than they solve.&nbsp; Given the myriad of 
							products, tools, and services that are typically integrated to manage an 
							application change from its inception to fulfillment, the number of 
						point-to-point integrations drastically increases and 
						administration and maintenance become complicated for 
						both customers and vendors alike, with 
						the risk of error increasing significantly.&nbsp; For example, consider the following
                        set of tools that are typically integrated across ALM 
						solutions:&nbsp;
						</p>
						<ul>
							<li>
								
									Requirements Management (e.g., Borland 
									CaliberRM, Serena RTM, Telelogic DOORS, etc.)</p>
							<li>
								
									Change &amp; Configuration Management (e.g., 
									Rational ClearCase, Serena ChangeMan, etc.)</p>
							<li>
								
									Version Control (e.g., CVS, etc.)</p>
							<li>
								
									Application Modeling (e.g., Borland 
									Together, Rational Rose, , etc.)</p>
							<li>
								
									Software Development (e.g., Eclipse IDE, IBM WebSphere 
									Studio, Microsoft Visual Studio, etc.)</p>
							<li>
								
									Defect Tracking (e.g., Bugzilla, Mantis, 
									Serena TeamTrack, etc.)</p>
							<li>
								
									Quality Assurance (Mercury TestDirector, Segue SilkCentral, etc.)</p>
							<li>
								
									Deployment (e.g., IBM Tivoli, Macrovision 
									InstallShield, etc.)</p>
							<li>
								
									Application Security (e.g., CA Netegrity, 
									Entrust, Secure CodeAssure, etc.)</p>
							<li>
								
									Project Portfolio Management (e.g., Niku Clarity, etc.)</p>
							</li>
						</ul>
						<p><span style="font-family: Arial">T</span><font face="Arial" size="2"><span style="font-size: 10pt; font-family: Arial">he 
						number of possible combinations of a set of <i>n</i> 
						integration points taken 2 at a time is given by </span>
						</font><font size="-1" face="Arial"><i>(n!)/(2!(n - 2)!)</i>.
						</font>This short list includes 22 products. Even if you 
						take a smaller subset of a typical cross-product 
						integrations within ALM involving say 10 products, a 
						point-to-point integration of these 10 products would involve 
						45 unique integrations, e.g., Telelogic DOORS &lt;-&gt; Rational
                        Rose, Telelogic DOORS &lt;-&gt; Mercury TestDirector, Serena ChangeMan &lt;-&gt; Rational Rose, Serena ChangeMan
                        &lt;-&gt; Mercury TestDirector, etc. This <b>does
                        not scale.</b>
						</p>

        <h3><b>Motivation for the Eclipse Project</b>
						</h3>

        <p>To effectively manage the application change across the 
							lifecycle, these various product silos and tools need to be integrated.&nbsp; 
							This introduces a complex technical challenge for both vendors and customers 
							alike as the point-to-point integration becomes cost prohibitive to develop and 
							maintain.&nbsp; Vendors know that the creation of any interoperability 
							framework is an enormous undertaking for any one vendor. &nbsp;In addition, 
							this complex and difficult work does not provide a competitive advantage for 
							the vendor's commercial solutions. &nbsp;Furthermore, if developed as a 
							proprietary component, this framework will not guarantee the necessary level of 
							interoperability and will push the customers into a single vendor suite of 
							solutions which is neither technologically practical nor financially feasible 
							in most cases.
						</p>

<h2>Solution</h2>
						
						<h3>Goal</h3>
						                        The goal of the ALF 
							project is to solve this integration problem by 
		introducing a central negotiator that manages interactions between 
		applications.&nbsp; This service-oriented event manager provides a very 
		useful uncoupling function.&nbsp; Using an intermediate communication 
		format prevents having to integrate an application several times with 
		several other applications, and simply carries out one integration on 
		the central node.&nbsp; The central node then carries out communication 
		with other ALM applications through orchestration of their corresponding 
		web services.&nbsp;
						<p>The ALF Solution consists of three main parts, 
						namely, ALF Events, Provider Services, and ALF Service 
						Flows.</p>
						<h3>1.&nbsp; ALF Events and Event Vocabulary
						</h3>
						<p class="MsoNormal">The ALF project will create an 
						event (i.e., signal) vocabulary for initiating the
                        conversations between tools. The event vocabulary will 
						cover common concepts (requirement added, new 
						defect created, relationship created, etc.). The project 
						deliverable for the event vocabulary is a common set of 
						WSDL definitions and the associated 
						specifications/documentation.&nbsp; </p>
		<p class="MsoNormal">ALF Committers, organized into subject-matter 
		sub-groups, will extend the event vocabularies. To provide a 
		framework that leads to a homogenous and consistent overall model, 
		ALF will specify the overall style, organization, and constraints.</p>
		<p>An Event is a web service message sent from the tool to the ALF 
		infrastructure.&nbsp; Within ALF, an Event Manager determines which ALF 
		Service Flows should be invoked.&nbsp; Event handling is based upon a 
		publish/subscribe model between Events and Service Flows. The Event 
		Manager dispatches the appropriate ALF Service Flow to start a service 
		flow instance.&nbsp; Tools 
		from multiple vendors should emit the same event type thus making the 
		operation of ALF independent of the particular tool. </p>
		<p>The ALF committers will specify the set 
		of standard Events.&nbsp; Software developers may extend these events 
		using ALF Designer, an Eclipse-based tool. </p>
						<h3>2.&nbsp; Provider Services and Service Vocabulary</h3>
						<p>For a tool to participate in the ALF framework, it 
						needs to expose critical large-grained set of web 
						services (WSDL contracts) to query and return 
						information or take actions.
						</p>
						<p class="MsoNormal">The ALF project will create a 
						common service standard for the conversations between 
						tools to provide a framework that leads to a homogenous 
						and consistent overall model.&nbsp; ALF will specify 
						the overall style, organization, and constraints.</p>
		<p class="MsoNormal">ALF Committers, organized into subject-matter 
		sub-groups, will create the service vocabularies. The service vocabulary 
		will cover common entities (requirement, defect, etc.).&nbsp; Software 
		developers may extend these services using ALF Designer, an Eclipse-based 
		tool. </p>
		<h3>3.&nbsp; ALF Service Flows&nbsp;
						</h3>
        <div dir="ltr" align="left">
			<span class="534253921-05072005">Web Services registered with the 
			ALF framework can be orchestrated to query information (e.g., get 
			data, status, etc.) and/or take action (e.g., save data, change 
			data, etc.) according to the ALF Service Flows defined at 
			design-time.&nbsp;ALF Service Flow definitions will be written in <span class="headingtext">
			OASIS Web Services Business Process Execution Language (BPEL).&nbsp; </span>
			The ALF project will provide an example implementation using an 
			existing BPEL execution engine<span class="headingtext">. It is 
			anticipated that any specification-conforming BPEL execution engine 
			will be compatible with ALF.</span><p>For example, using the ALF Events and 
			Service Flows, upon a &quot;check-in&quot; event, one could automatically 
			annotate &quot;check-in&quot; messages for the configuration management system 
			with information from the bug tracking and requirements tracking 
			systems.&nbsp; To accomplish this, the ALF Event Manager invokes the ALF 
			Service Flow associated with that event, passing it all the 
			necessary context.&nbsp; Subsequently, the Service Flow executes the 
			appropriate providers' web services to query the related information 
			from the bug tracking and requirements tracking systems and 
			annotates a check-in messages that is then saved to the 
			configuration management system.&nbsp;&nbsp; </span></div>
		<p>The ALF project packages an extensible and exemplary engine to 
		execute these rules.</p>
		<p><b>ALF Component Map</b></p>
		<p>The following diagram depicts the main components of the ALF 
		framework.&nbsp; The foundation for ALF is the Eclipse platform.&nbsp; 
		The Eclipse IDE provides the default client environment.&nbsp;
		<span style="font-size: 10.0pt; font-family: Arial">For development 
		managers and others whose primary user interface is not an IDE, we will 
		use the Eclipse Rich Client Platform (RCP).&nbsp; </span>For creating 
		user interfaces for the IDE and RCP, ALF expects to leverage Eclipse 
		frameworks such as the Eclipse Modeling Framework (EMF) and the 
		Graphical Editor Framework (GEF) for graphical interfaces.&nbsp; For development dashboard reporting, 
		ALF expects to 
		leverage the Business Intelligence and Reporting Tools (BIRT) project.&nbsp;
		<span style="font-size: 10.0pt; font-family: Arial">ALF will leverage 
		industry standards and industry frameworks and tools to the maximum 
		extent possible.&nbsp; </span>Being based on web services, ALF will 
		rely on a stack of web service standards. The components in the Eclipse 
		ALF 
		Framework layer may be based on&nbsp;the generic components in the layers 
		below, but will require additional capabilities or tailoring for ALF.&nbsp; 
		The Service Provider Interfaces provide an extensibility model for the 
		execution layer and will include Presentation, Worklist, Relationship 
		store, and Logging interfaces to name just a few.</p>
		<p>
		<img border="0" src="ALF_Files/ALF_MAP.jpg"></p>
		<p>&nbsp;</p>
        <h2>Organization</h2>
        There are many organizations with commercial offerings and technology 
		within the scope of the project.&nbsp; We will be inclusive and invite 
		full participation in reviewing, categorizing the incoming requirements 
		and in prioritizing the requirements. The project will use an iterative 
		and incremental delivery plan.<p>The ALF project will have a major focus on customer participation. The 
						project will find a set of initial customers/users/companies that want to 
						develop tools on top of the framework and that these customers will use their 
						requirements to help drive the project.&nbsp; These key customers and 
						technology partners will participate as committers, users and developers. These 
						organizations and individuals will assist in the architecture, planning, 
						requirements and testing of ALF.</p>
						<p>
							<b>Potential Committers </b>
		<table border="0" width="46%" id="table1">
			<tr>
				<td width="166">- Ali Kheirolomoom</td>
				<td width="108">Serena </td>
				<td>(<a href="http://www.serena.com/">www.serena.com</a>)</td>
			</tr>
			<tr>
				<td width="166">- Kevin Parker &nbsp;</td>
				<td width="108">Serena </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="166">- Brian Carroll</td>
				<td width="108">Serena </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="166">- Tim Buss </td>
				<td width="108">Serena </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="166">-
									Walter Berli </td>
				<td width="108">UBS&nbsp; </td>
				<td>(<a href="http://www.ubs.com/">www.ubs.com</a>)</td>
			</tr>
			<tr>
				<td width="166">-
									Aniello Bove&nbsp;&nbsp; </td>
				<td width="108">UBS&nbsp; </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="166">-
									Helge Scheil </td>
				<td width="108">Niku </td>
				<td>(<a href="http://www.niku.com/">www.niku.com</a>)</td>
			</tr>
			<tr>
				<td width="166">-
									Ernst Ambichl &nbsp;&nbsp;</td>
				<td width="108">Segue</td>
				<td>(<a href="http://www.segue.com/">www.segue.com</a>)&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td width="166">- Mark Patterson &nbsp;&nbsp;</td>
				<td width="108">Secure Software</td>
				<td>(<a href="http://www.securesoftware.com/">www.securesoftware.com</a>)</td>
			</tr>
			<tr>
				<td width="166">-
									Siva 
										Visweswaran&nbsp;</td>
				<td width="108">Cognizant</td>
				<td>(<a href="http://www.cognizant.com/">www.cognizant.com</a>)</td>
			</tr>
			<tr>
				<td width="166">-
									Suresh 
										Venugopal</td>
				<td width="108">Cognizant</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="166">-
									Mohana Krishna&nbsp;&nbsp;</td>
				<td width="108">Cognizant</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="166">- Emeka Obianwu </td>
				<td width="108">BuildForge</td>
				<td><a href="http://www.cognizant.com/">(www.buildforge.com)</a></td>
			</tr>
			<tr>
				<td width="166">- Richard Landry </td>
				<td width="108">BuildForge</td>
				<td>&nbsp;</td>
			</tr>
		</table>
                        <p >Critical to the success of this project is participation of 
							developers in the Application Lifecycle Management (ALM)
                        community. We intend to reach out to this community and 
							enlist the support of those interested in making a success of the Application 
							Lifecycle Framework project. We also 
							anticipate that several industry verticals will have an interest and we will 
							therefore contact these companies and associated standard organizations. In 
							addition, we ask interested parties to contact the 
						ALF newsgroup to express 
							interest in contributing to the project.</p>
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

