<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Pave Proposal";
$pageKeywords	= "pave, pattern";
$pageAuthor		= "Kaloyan Raev";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Pave</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Pave");
?>

<h2>Introduction</h2>

<p>The <b>Pave</b> project is a proposed open source project under the <a href="http://www.eclipse.org/webtools/">Eclipse Web Tools Platform Project</a>.</p>

<p>This proposal is in the Project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process document</a>) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.pave">eclipse.pave</a> newsgroup. </p>

<p>The Pave project provides a framework for composing sequences of frequently executed operations. These sequences are called <i>patterns</i> and are meant to provide error proof path for users in the execution of routine tasks. </p>

<h2>Background</h2>

<p>Currently, in Eclipse, there is a set of atomic operations intended to help users for performing variety of tasks. It is a usual case that several of these operations need to be called in a sequence to achieve a result on a higher level of complexity. It is very often that the same sequences of operations are executed frequently to achieve a family of complex tasks. It would be an effective time-saver if these sequences are automated. Such automation could also provide an error proof path for users to perform these tasks and be sure that at the end they will have results that follow the established conventions. </p>

<p><b>Definition:</b> <i>Pattern</i> &ndash; a frequently executed sequence of operations that transforms the state of the workspace in an error proof way by following well-established conventions. </p>

<p>There is no easy mechanism in Eclipse for assembling sequences of operations in a way that they share data between each other and in the same time operations keep their independency from the automation framework. Providing such framework would foster developers to implement proven patterns in different areas of application development.</p>

<p>The Pave framework proposed here enables: 
<ul>
<li>End users to use patterns to generate error proof code, thus improving the learning curve and accelerating development. </li>
<li>End users to see only the patterns that are applicable to the context of the provided input &ndash; like the current selection in the workbench. </li>
<li>PDE developers to define patterns and their properties in a plug-in descriptor file. </li>
<li>PDE developers to create complex patterns by taking advantage of already existing operations and their UI. </li>
</ul>
</p>

<p>Patterns should provide meaningful default values to enable users with quick completion. Any input parameters for intermediate operations should be glued with the corresponding output data of previous operations where possible. </p>

<h2>Scope</h2>

<p>The objective of the Pave project is to create an extensible framework, which supports the following use cases:
<ul>
<li>Enable PDE developers to define new patterns. </li>
<li>Allow reusing of existing operations and their UI in patterns. </li>
<li>Provide a single point of entry for all patterns based on a contextual input. </li>
<li>Manage enablement of patterns &ndash; make available only patterns that are applicable to the current object given as input. </li>
<li>Manage validation within patterns and operations &ndash; enable overriding existing validations and add new validations. </li>
<li>Enable data sharing between the operations within a single pattern &ndash; the output data of previous operations should be matched to the input data of the following operations, where possible. </li>
<li>Allow creation of complex patterns &ndash; composition of patterns. </li>
<li>Allow contribution of UI elements to the patterns: wizard pages, dialog boxes, etc. </li>
<li>Enable headless execution of patterns. </li>
<li>Define a pattern that generates a skeleton for creating a new pattern. </li>
<li>Define patterns that exemplify the usage of the framework. If any of these patterns fit better in the scope of an existing Eclipse project, then they may be moved to that project later. </li>
</ul>
</p>

<h2>Description</h2>

<p>The initial contribution consists of a framework that achieves most of the objectives defined in the Scope. The contributed code is mature and included in an adopter's product. </p>

<p>The Pave framework consists of Core and UI parts. </p>

<p>The Core part is completely independent of the UI. This is where all patterns are registered along with their enablement, validation extensions, model synchronizers (if necessary), etc. Using only the Core part enables headless execution of patterns. </p>

<p>The UI part is where UI elements, like wizard pages, are registered to a certain pattern. The Pave framework provides a default wizard and a first page for the selection of applicable patterns. These default UI elements can be replaced with different implementations by adopters. </p>

<p>The implementation of the Pave framework strongly depends on the WTP Data Model Wizard Framework. However, the latter is not a necessary requirement for defining new patterns. The Pave framework extends the features of the WTP Data Model Wizard Framework by adding a next level of abstraction for operations &ndash; <i>patterns</i>. The following is a summary of all noticeable features that the Pave framework introduces on top of the WTP Data Model Wizard Framework.</p>

<p>
<ul>
<li>The Pave framework provides an easier way of composing operations. Such sequences are called <i>patterns</i> and are defined in extension points.</li>
<li>Patterns are context sensitive &ndash; they are applicable for certain input. The applicable rules are described declaratively in the enablement expression of the extension point. </li>
<li>An external <i>synchronizer</i> class provides an easier way for connecting the output of previous operations with the input of the following operations in the pattern. This is done without modifying the operations themselves. Synchronizer classes are described in the pattern extension point.</li>
<li>Validation of Data Model operations is extended to pattern level. Additional validation classes are declared in the pattern extension point to validate the execution of the whole sequence of operations. This extended validation is possible again without altering any existing operations that are reused in the pattern. </li>
<li>Functions of the Data Model Operations (like validation) can be overridden on pattern level to ensure better integration between the reused operations in the pattern. </li>
</ul>
</p>

<p>The initial contribution will also include a couple of patterns that exemplify the usage of the framework. The patterns are in the context of Java EE development. </p>

<h3>Session CRUD Fa&ccedil;ade pattern</h3>

<p>The Session CRUD Fa&ccedil;ade is an exposed EJB session bean with methods for Create, Read, Update and Delete functions of the corresponding JPA entities. </p>

<p>This pattern creates a new EJB session bean and generates the needed CRUD methods and queries depending on the given entities as input. The pattern reuses the operation and UI of the Session Bean wizard that is already part of the Eclipse WTP project and defines additional operations. </p>

<h3>JSF Application pattern</h3>

<p>This pattern generates a skeleton of a complete Java EE application &ndash; up to the JSF front end &ndash; based on the given JPA entities as input. This enables Java EE developers to quickly test their JPA domain model and database structure, or even jump start with a working sketch of the entire Java EE stack. </p>

<p>The pattern is a composition of the Session CRUD Fa&ccedil;ade pattern and additional operations defined for:
<ul>
<li>Generating JSF manage beans. </li>
<li>Generating JSP pages. </li>
<li>Including navigation rules in the faces-config.xml descriptor. </li>
</ul>
</p>

<h2>Organization</h2>

<h3>Initial committers</h3>

<p>
<ul>
<li>Dimitar Giormov (SAP): project lead</li>
<li>Milen Manov (SAP): committer</li>
</ul>
</p>

<h3>Mentors</h3>

<p>
<ul>
<li>David M Williams, IBM, WTP PMC Lead</li>
<li>Bernd Kolb, SAP</li>
</ul>
</p>

<h3>Interested parties</h3>

<p>SAP AG</p>

<h3>Developer community</h3>

<p>We expect to extend the initial set of committers by actively supporting a developer community that will implement new patterns based on this framework. As the number of patterns developed increases, more requirements to the framework pop up. This will naturally lead to more contributions to the project. </p>

<h3>User community</h3>

<p>End users should not use the Pave framework directly. They should use the framework indirectly through the patterns contributed by other tools. </p>

<h2>Tentative Plan</h2>

<p>
<ul>
<li><i>Apr 2009</i>: Submit project proposal. </li>
<li><i>May 2009</i>: Construct web site with documentation and tutorials. </li>
<li><i>Jun 2009</i>: Creation review. </li>
<li><i>Jun 2009</i>: Prepare project infrastructure. </li>
<li><i>Jul 2009</i>: Initial contribution and IP review. </li>
<li><i>Jul 2009</i>: Release of version 0.5. </li>
<li><i>Nov 2009</i>: Release of version 0.7. This release includes changes in response to the community feedback. </li>
<li><i>Dec 2009</i>: Consider move to Eclipse WTP Commons, Eclipse Tools or Eclipse Platform and commit a Move review. </li>
<li><i>Feb 2010</i>: Become part of the 2010 simultaneous release. </li>
<li><i>Jun 2010</i>: Release of version 1.0 together with the 2010 simultaneous release. </li>
</ul>
</p>

<h2>About the name</h2>

<p>The project name "Pave" comes from the word "&#x43f&#x430&#x432&#x435;" <i>[p&aacute;ve]</i>, which in Bulgarian means "paving stone." The idea behind 
this name is that the framework is helping application developers with automating the "boring" operations they regularly execute. It creates a skeleton for their creative work. In other words, it paves the way for application developers. Like pavement is built by paving stones, the Pave framework combines small building blocks to generate the result of a complete set of operations. Like paving stones these building blocks are small and independent.</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
