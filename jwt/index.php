<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Java Workflow Tooling (JWT)";
$pageKeywords	= "Workflow Editor; Workflow engine Administration and Monitoring";
$pageAuthor		= "Marc Dutoo and Florian Lautenbacher";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Java Workflow Tooling (JWT)</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Java Workflow Toolbox");
?>

<p>
<h2> Introduction </h2>
<p>The JWT project is a proposed open source project under the Eclipse Technology Project.
</p><p>This proposal is in the Project Proposal Phase (as defined in
the Eclipse Development Process document) and is written to declare its
intent and scope. It is written to solicit additional participation and
input from the Eclipse community. You are invited to comment on and/or
join the project. Please send all feedback to the <b>eclipse.technology.jwt</b> newsgroup. The project wiki is available at <a href="http://wiki.eclipse.org/index.php/Java_Workflow_Toolbox_Project" class="external free" title="http://wiki.eclipse.org/index.php/Java Workflow Toolbox Project" rel="nofollow">http://wiki.eclipse.org/index.php/Java_Workflow_Toolbox_Project</a> .
</p><p><br>
</p>
<h2> Overview </h2>
<p>The JWT project aims to provide both <b>build-time and runtime generic tools for workflow engines</b>.
It will initially be composed of two tools, WE (Workflow Editor) and
WAM (Workflow engine Administration and Monitoring tool). JWT also aims
to provide generic APIs for defining and administrating business
processes, in order to achieve said genericity.
</p><p><b>WE (Workflow Editor)</b> will be a visual tool for creating,
managing and reviewing process definitions. Straightforward and simple,
WE will let users quickly create workflow process definitions, check
and store them for further use. Once a process definition is proved
valid, it can be imported/referenced into new ones, thus shortening the
time and effort needed to define the workflow process. WE provides
several views: for business managers to change the control flow of a
process, for technical experts to invoke services, or others.
</p><p><b>WAM (Workflow engine Administration and Monitoring tool)</b>
will be used to deploy and test a workflow in a workflow engine so to
handle an engine's process definition external repository, to load some
process definitions into a specific Workflow Engine, unload it, update
it, instantiate it, monitor the Workflow Engines processes, perform
mappings among participant definitions and real users, and among
application definitions and tool agents.
</p><p><br>
</p>
<h2> Background </h2>
<h3> State of the commercial BPM tooling market </h3>
<p>Companies typically loose productivity and effort because of their
IT system being inefficient or not adapted to the existing and
reality-grounded business processes. Since companies want to get
increased control over their IT systems, they adapt them to their
business needs rather than the opposite. This is sometimes called
"business driven development" or "top-down approach".
</p><p>Some major ISVs such as WebMethods, SeeBeyond and others
propose a complete galaxy of development and runtime tools that enable
this strategy. These tools allow a company to simply define its
business processes and map the different activities of the processes to
existing technical services or applications. Therefore such tools - and
indeed such a strategy - obviously resonate with integration
problematics.
</p><p>However, they have limitations. First, these proprietary tools
only have one graphical representation and generate one output format.
Second, they can only interact with one specific workflow engine.
Third, the fact that these tools are proprietary, closed source
projects implies that the diffusion and adoption of the technology is
very limited and the benefits of the business driven development are
still largely unappreciated.
</p>
<h3> Evolution of BPM in light of the new information system architecture and integration standards </h3>
<p>The arrival of new standards like the JSR 208 (the Java Business
Integration, JBI), SDO (Service Data Objects) or SCA (Service Component
Architecture) not only means that existing products will change again,
but that the time is right for opening up the field and welcome such a
new, generic framework that is adaptable to any specific requirements.
</p><p>The <i>Java Business Integration (JBI)</i> specification
proposes a standard solution to integration problems for Enterprise
Service Buses (ESB). In such a service platform, the orchestration
services are critical proponents in allowing discrete messages to be
routed in the context of the global business process. Business process
engines providing such orchestration services are already being
integrated in JBI solutions (as shown by open source ESBs like Apache
ServiceMix or Objectweb Petals), in a way that is hence bound to be
increasingly standardized and tightly linked to run-time and even
build-time tools.
</p><p>On the other side, the <i>SCA specification</i> builds on the
SOA philosophy so as to describe a model for assembling composite
applications, and therefore encompasses a set of steps that are
required in order to deploy most of SOA-style information system
architectures. There is a natural complementarity between the
SCA-specified architectural model and the vision of information
system-wide business dynamics enabled by BPM, and that is exemplified
in top-level tooling platforms that, like Eclipse STP or the recent
SCOrWare proposal made by several Objectweb members (including INRIA,
Amadeus, Open Wide), aim at easing the development and deployment of
SOA and SCA architectures. That's why both SOA models and tooling
platforms will concur to better and more standardized BPM tooling.
</p>
<h3> What JWT proposes </h3>
<p>The goal of the JWT project is to develop a set of tools that help
to develop, deploy and test workflows. JWT is not just another modeling
tool and workflow engine, but provides an adaptable framework for
different graphical representations and XML notations, as well as
different workflow engines.
</p><p>The JWT tooling suite is a natural member of top-level tooling
platforms that, like Eclipse STP, can be used to ease development
targeting integration platforms such as ESBs. Both single users and
ISVs can use such set of tools either for direct use or for building a
complete development software stack for their integration platform. In
such light, one of the natural goals of JWT is good integration with
the Eclipse STP platform.
</p><p><br>
</p>
<h2> Description </h2>
<h3> Workflow Editor (WE) </h3>
<p>WE will be a tool for the modeling of process definitions. The final
output of this process modeling phase is an XML file which can be
interpreted at runtime by any workflow engine compliant with the XML
format of the process definition. WE will provide four core
features&nbsp;:
</p>
<ul><li> Different graphical representations of the process definition
</li></ul>
<ul><li> Export of process definitions to several XML-formats
</li></ul>
<ul><li> Import of valid XML process definition and its graphical
representations: process can be defined in several formats (mostly
based on XMI) and the graphical figures can be described in an abstract
way.
</li></ul>
<ul><li> Additional features easing the development of business
processes, like business process validation, business process reuse (in
conjunction with the WAM's Process Repository) and semantic matching.
Partners of JWT are currently working on several semantic process
matching algorithms and procedures which will be included in WE.
</li></ul>
<p><br>
WE can be subdivided into two different layers&nbsp;:
</p>
<ul><li> <b>the graphical representation layer</b>. The intent behind
this is to be able to plug in several views for the same workflow
definition file: for example, a business manager might want to see
other details than an IT expert. Additionally it might be necessary to
have one view for the certification of ISO 9000, one for ITIL, and
others. Having several pluggable graphic representation will allow us
to expose simplified, business oriented user-friendly views of the same
workflow definition. Note that Amadeus will contribute on the
extensibility mechanism allowing business-specific activities and
integrate business specific representation views in the scope of the
SCOrWare project.
</li></ul>
<ul><li> <b>the grammar definition layer</b>. The intent behind this is
to be able to plug in several different grammars for the same
representation: this is of particular interest when a graphical
representation (such as BPMN or UML) can support different grammars.
For example, BPMN can support both XPDL and BPEL. WE will be able to
support not only XPDL and BPEL but other proprietary notations (e.g.
jPDL from JBoss jBPM) or future standards as well.
</li></ul>
<p>As process definitions and graphical representations can vary, we
propose to build a generic, extendable framework that enables several
representations and grammars to be implemented and plugged in, thanks
to a common, pivotal business process definition metamodel. The list of
generatable process definitions as well as viewable graphical
representation will be extensible.
</p>
<h3> Workflow engine Administration and Monitoring tool (WAM) </h3>
<p>The WAM aims to provide tooling for managing business processes
independently of the underlying workflow engine. In order to achieve
that, the WAM will rely on a generic workflow engine administration
API, whose definition is at the heart of the project.
</p><p>Building on this API, the WAM intends to implement the following functionalities&nbsp;:
</p><p><b>Process definition management&nbsp;:</b>
</p>
<ul><li> Repository Management&nbsp;: The repository management
displays all available files in the engines package repository. This is
the place where you can manage the repository of the engine. You can
upload a new package file from your local machine into the repository
or delete one from the it.
</li></ul>
<ul><li> Package Management&nbsp;: The package management displays all
packages that are loaded into the engine. It enables you to load and
unload packages to/from engine, as well as to update some already
loaded packages.
</li></ul>
<p><b>Process execution management&nbsp;:</b>
</p>
<ul><li> Process Instantiation Management&nbsp;: Lets you view the
package-process definition tree of the loaded packages. The graphical
workflow representation will be used to show the state of execution of
the instantiated process.
</li></ul>
<ul><li> Process Monitor&nbsp;: Lets you manage process instances like suspend, stop, view process or activities.
</li></ul>
<p><b>Process runtime resources mapping&nbsp;:</b>
</p>
<ul><li> User Management&nbsp;: Lets you manage the users of the
process engine by defining new ones, deleting existing ones or changing
their properties.
</li></ul>
<ul><li> Application Mapping&nbsp;: Lets you map a package and its
process applications to the real applications handled by a tool agent.
</li></ul>
<p><b>User-oriented process management&nbsp;:</b>
</p>
<ul><li> Worklist Management&nbsp;: Lets you perform the work items of
the instantiated processes. As an admin user, you can perform your work
items, and you can see work items from others. Also, you have a
possibility to re-assign the work item from one user to another.
</li></ul>
<ul><li> "Desktop Business Process" tooling&nbsp;: this is more of a
simple, generic use case implementation allowing standalone,
desktop-integrated business processes for common end user tasks such as
flight booking. Its aim is to provide a ready-to use application out of
the box and to open up the BPM field to end user needs.
</li></ul>
<p><br>
Fulfilling these requirements will be achieved by&nbsp;:
</p>
<ul><li> Defining a simple, but extensible API for each of the
requirements topics, as said before. These APIs will allow vendors to
make their own implementation to get their workflow engine managable.
JWT will provide default or stub implementations for them, allowing
progressive integration of a workflow engine model in the WAM. A
generic API to start and stop a workflow engine is the most critical of
them, and will be the first step to achieve for the WAM.
</li></ul>
<ul><li> Defining specific workflow engine models, in a similar
approach to the one used for J2EE server model in the JST project.
These workflow engine models could then be used by end users to define
their own engine in their own specific environment. As soon as a
workflow engine has been defined by a user, it would be possible to
deploy a process definition to this engine. JWT will provide such a
model for a specific workflow engine (which will have to be chosen, but
will probably be XPDL compliant).
</li></ul>
<h3> Use cases and demonstrators </h3>
<p>As said above, these comprise&nbsp;:
</p>
<ul><li> Dedicated views for business-specific use cases
</li></ul>
<ul><li> "Desktop Business Process" standalone applications
</li></ul>
<ul><li> Various other contributions foreseen by JWT partners
</li></ul>
<p><br>
</p>
<h2> Architecture </h2>
<p>JWT's architecture will be based on an MDA approach and will address
the several business-specific extensions problematics through the DSL
paradigm. The Eclipse framework and libraries will be used in order to
provide the required tools. Note that additionnally, Obeo SA (the
creator of the Open Source DSL platform Acceleo) will contribute its
MDA / DSL expertise to JWT.
</p><p>Here are some elements furthering and depicting what has been outlined above&nbsp;:
</p>
<h3> WE Architecture </h3>
<p>The WE will allow to plug any process XML-based grammar in the JWT
Workflow Model framework and display its graphical representation.
</p><p>The principles of mapping processes to representations will be
based on the Workflow-Model API. This API will identify commonly used
entities within a workflow definition model, their relationships and
attributes following their XML grammar and map them to a common,
pivotal process definition metamodel. This workflow model will be our
pivot model which supports all other views.
</p><p>As a starting point JWT will provide support only for a single
grammar and a single representation. Their choice have to be validated,
but BPMN and XPDL are strong candidates.
</p><p>The WE architecture is shown in the following picture&nbsp;:
</p><p>
<center>
<a href="./Jwt_we_architecture.png" class="image" title="Image:Jwt_we_architecture.png"><img src="./Jwt_we_architecture.png" alt="Image:Jwt_we_architecture.png" longdesc="/index.php/Image:Jwt_we_architecture.png" height="398" width="578"></a>
</center>
</p>
<h3> WAM Architecture </h3>
<p>The WAM aims to manage the definition process into a workflow
engine. In order to be workflow-engine agnostic, it will allow to
define a workflow engine based on a workflow engine model.
</p><p>This JWT workflow engine model will be first developed as an
extension of the basic start / stop / deploy API over BPM engines used
in WAM towards a meta-model for the easy adoptation of new server types
allowing more extensive server management features.
</p><p>As a starting point JWT will provide support only for a single
workflow engine. Its choice will have to be consistent with the
available WE grammar implementation.
</p><p>The WAM architecture is shown in the following picture&nbsp;:
</p><p>
<center>
<a href="./Jwt_wam_architecture.png" class="image" title="Image:Jwt_wam_architecture.png"><img src="./Jwt_wam_architecture.png" alt="Image:Jwt_wam_architecture.png" longdesc="/index.php/Image:Jwt_wam_architecture.png" height="482" width="674"></a>
</center>
</p><p><br/></p>
<h2> Extension points </h2>
<p>The strategy to build the JWT project features will be "divide and
conquer". Things will be split as much as possible into separate
working groups and plugins in order to parallelize work, boost
development and provide the ability for other groups or vendors to
further extend and customize the functionality in a well-defined
manner.
</p><p>In WE as well as between WE and WAM, there will be an
architectural decoupling. WE and WAM will, at least, be two separate
plugins, but presumably it will take the form of this list of plugins:
</p>
<h3> For the WE </h3>
<ul><li> Workflow graphical representation
</li></ul>
<ul><li> Workflow grammar definition
</li></ul>
<p><br>
Concretely, the WE will take the form of a MultiPageEditor(Workflow Editor) and a wizard composed of&nbsp;:
</p>
<ul><li> Graph view&nbsp;: composed of two graphical viewers. It allows to graphically model processes and activities
</li></ul>
<ul><li> Source view&nbsp;: represents the XML file which it the result of graphical edition.
</li></ul>
<ul><li> Outline view&nbsp;: gives an overview of process' activities.
</li></ul>
<ul><li> Attribute view&nbsp;: based on property view, used to edit entities attributes.
</li></ul>
<ul><li> Entities Property view&nbsp;: based on property view, used to edit the entities properties.
</li></ul>
<h3> For the WAM </h3>
<ul><li> Workflow engine model
</li></ul>
<ul><li> Workflow monitoring tool&nbsp;:
<ul><li> Repository Management
</li><li> Package Management
</li><li> Process Instantiation Management
</li><li> Process Monitor
</li><li> User Management
</li><li> Application Mapping
</li><li> Worklist Management
</li></ul>
</li></ul>
<p><br>
Thus concretely, the WAM plugin will be composed of&nbsp;:
</p>
<ul><li> A workflow engine model in the run menu and the launcher
</li></ul>
<ul><li> The graph view of WE for displaying the state of process instances
</li></ul>
<ul><li> Several views to represent worklist, user management, process
instanciation management, application mapping, repository management.
These views will have to be defined in detail in the scope of the
project.
</li></ul>
<h3> Integration Points with Other Eclipse Projects </h3>
<p>Given the scope of the project, the JWT project will interact with and leverage the following existing Eclipse projects:
</p>
<ul><li> WST (Web Standard Tools)
</li></ul>
<ul><li> GEF (Graphical Editing Framework)
</li></ul>
<ul><li> EMF (Eclipse Modeling Framework)
</li></ul>
<ul><li> GMF (Graphical Modeling Framework)
</li></ul>
<ul><li> STP (SOA Tools platform) has been created at the end of 2005
to provide tooling around the Service Oriented Architecture paradigm,
according to the SCA (Service Component Architecture) specification.
Therefore JWT's integration with STP is important. Along its primary
founders (Iona, IBM, Sybase and BEA), Objectweb is involved as a member
of the Project Management Committee, and through the involvement in STP
the work at JWT and STP can be aligned easily. Moreover, JWT has been
recognized as a natural friend project of BPM-themed STP subprojects,
such as STP BPEL and STP BPMN, with whom collaboration will be as
thorough as possible. Note that putting JWT's priorities initially on
the XPDL grammar and business-level activity representation would make
JWT a natural complement to the existing BPMN and BPEL projects, while
not precluding any further collaboration.
<br>(Contact has been made with the BPMN project as a starting
point for further collaboration, which may include code or feature
integration in either way.)
</li></ul>
<ul><li> Validation frameworks (like the EMF Validation Framework or Recipe from GTM/OAW)
</li></ul>
<ul><li> Callisto / Europa
</li></ul>
<p><br>
Additionally JWT will have impact on project which are not hosted by Eclipse, e.g.
</p>
<ul><li> The Petals Objectweb ESB solution, which is the outcome of the JoNES project
</li></ul>
<ul><li> The recently approved SCorWare proposal, which gathers several
Objectweb members (including INRIA, Amadeus, Open Wide) aims to build
an SOA development and deployment platform on top of other ObjectWeb
projects.
</li></ul>
<p><br/></p>
<h2> Organization </h2>
<p>The life cycle of this project may be similar to the Equinox
project. That is, once this work matures, it may be incorporated into
the Eclipse Platform to provide a common open source substrate that
industrial/commercial users could use to build solutions.
Alternatively, it may continue as a Technology subproject.
</p>
<h3> Sponsoring Organizations </h3>
<ul><li> ObjectWeb is an international consortium fostering the
development of open-source middleware for enterprise applications: ESB,
EAI, e-business, clustering, grid computing, managed services and more.
</li></ul>
<ul><li> SCOrWare is a recently approved project funded by european
research public funds that aims to develop an SOA development and
deployment platform. Its members include INRIA, Amadeus, Open Wide,
Obeo, INT Evry. The SCOrWare-produced BPM tooling (comprising business
process orchestration, semantic process matching, business use cases
and an MDA / DSL approach) will be contributed and integrated in JWT.
</li></ul>
<ul><li> Open Wide is a French company created in 2001 providing
services around open source components and a national leader in the
entreprise portal integration field. Open Wide's developers are
contributing to if not leading some open source projects such as RTAI
or GForge. Open Wide has developed one of the first integration
platform entirely based on open source components and called NoSICA. It
is also part of the ObjectWeb's ESB initiative, of ObjectWeb's
distributed, JBI based, ESB framework called Petals / JOnES and of
Objectweb's forthcoming SCOrWare SCA platform.
</li></ul>
<ul><li> The Programming Distributed Systems Lab of the University of
Augsburg, Germany, is researching in the focus on software engineering in general
and more detailed in the model-driven software development related to
distributed systems. Their core competences are on software
engineering, interoperability and software automation in the field of
business process modeling, autonomic computing and semantics.
</li></ul>
<h3> Proposed project lead and board </h3>
<ul><li> Marc Dutoo (Project Lead) - Open Wide
</li></ul>
<ul><li> Florian Lautenbacher (Project Lead) - University of Augsburg
</li></ul>
<ul><li> Fabrice Dewasmes (Vision Advisor) - Pragma Consult
</li></ul>
<ul><li> Alain Boulze (Market Advisor) - INRIA / Objectweb
</li></ul>
<ul><li> Miguel Valdez (Technology Advisor) - Bull
</li></ul>
<ul><li> G&uuml;nther Palfinger (Technical Advisor) - eMundo GmbH
</li></ul>
<h3> Interested parties </h3>
<p>At this time, the following people and companies have expressed their interest in actively participating to this project:
</p>
<ul><li> Pardeep Sood - Independent Consultant
</li></ul>
<ul><li> Roman Zulauf - Object Engineering GmbH
</li></ul>
<ul><li> Guennadi V. Vanine - Independent Consultant
</li></ul>
<ul><li> Surrendra Reddy - Optena Corp.
</li></ul>
<ul><li> Object Engineering GmbH
</li></ul>
<ul><li> School of Software - Shanghai JiaoTong University
</li></ul>
<ul><li> Yang Guang - Allen Young (Shanghai JiaoTong University)
</li></ul>
<ul><li> Liu Yu - Tony Liu (Shanghai JiaoTong University)
</li></ul>
<ul><li> Sun Zhongyi - Sliver Sun (Shanghai JiaoTong University)
</li></ul>
<ul><li> Xiong Xiao Peng - BeiJing ChiPu network technic limited company.
</li></ul>
<h3> Initial code contributions </h3>
<p>JWT will use as a starting point the <a href="http://www.agilpro.eu/">AgilPro</a> contribution made by
the University of Augsburg (through its partnering with eMundo GmbH)
which is available here: <a href="https://sourceforge.net/projects/agilpro/" class="external free" title="https://sourceforge.net/projects/agilpro/" rel="nofollow">https://sourceforge.net/projects/agilpro/</a>.
</p><p>In addition, several other code contributions have been submitted by various companies and individuals&nbsp;:
</p>
<ul><li> Object Engineering GmbH&nbsp;: <a href="https://forge.openwide.fr/frs/download.php/83/ObjectEngineering_JWT_Contribution.zip" class="external free" title="https://forge.openwide.fr/frs/download.php/83/ObjectEngineering JWT Contribution.zip" rel="nofollow">https://forge.openwide.fr/frs/download.php/83/ObjectEngineering_JWT_Contribution.zip</a>
</li></ul>
<ul><li> School of Software - Shanghai JiaoTong University&nbsp;: <a href="https://forge.openwide.fr/frs/download.php/86/Elune_Workflow_Designer.zip" class="external free" title="https://forge.openwide.fr/frs/download.php/86/Elune Workflow Designer.zip" rel="nofollow">https://forge.openwide.fr/frs/download.php/86/Elune_Workflow_Designer.zip</a>
</li></ul>
<ul><li> Open Wide&nbsp;: <a href="https://forge.openwide.fr/frs/download.php/88/OpenWide-JaWESwing-contribution.zip" class="external free" title="https://forge.openwide.fr/frs/download.php/88/OpenWide-JaWESwing-contribution.zip" rel="nofollow">https://forge.openwide.fr/frs/download.php/88/OpenWide-JaWESwing-contribution.zip</a>
</li></ul>
<h3> Initial Roadmap </h3>
<p>The initial roadmap of JWT mirrors its first objectives of
</p>
<ol><li> having "something that works", an advanced proof of concept
that actually achieves cross-grammar and cross-representation
integration. That means it will benefit from having a release-oriented
development process, different teams working in parallel on supporting
each format, and integrating existing contributions.
</li><li>laying the groundwork to a generic workflow edition API
in the previous process. Being as much as possible open about other BPM
format implementation and also other BPM tooling projects will be
critical. The board will help in keeping in touch with key people in
this context.
</li></ol>
<p>The following roadmap takes into account the fact that we will use
as much as possible existing code and contributions in order to provide
our features, in order to boost both of these objectives. This task
list is not exhaustive in any way, and rather describes a minimal set
of what is needed to </p>
<ul><li> Prerequisite&nbsp;: decision process on the model
</li></ul>
<ul><li> Decision process on languages (includes studies and proof of concept) and engines
</li></ul>
<ul><li> WE&nbsp;: Making the model flexible. This comprises singleing
out extension points in it for grammars, representation views and their
business-specific extensions.
</li></ul>
<ul><li> WE&nbsp;: Doing or integrating an actual implementation over a chosen grammar language
</li></ul>
<ul><li> WE&nbsp;: Doing or integrating an actual implementation for a chosen representation language
</li></ul>
<ul><li> WE 1.0 release packaging - aiming at March 2007
</li></ul>
<ul><li> WAM&nbsp;: further decision process in order to chose a prefered engine (includes proof of concept)
</li></ul>
<ul><li> WAM&nbsp;: Process management API design and implementation for the chosen engine
</li></ul>
<ul><li> WAM&nbsp;: Dummy versions of other APIs needed for a basic version.
</li></ul>
<ul><li> WAM 1.0 packaging - aiming at June 2007
</li></ul>
<ul><li> other tracks&nbsp;: other WE extension implementations, SCOrWare contributions, AgilPro contributions...
</li></ul>


</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
