<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Handly</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<p>The Handly project is a proposed open source project under the <a
	href="http://www.eclipse.org/projects/project_summary.php?projectid=technology">Technology 
Top-Level Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<h2>Summary</h2>

<p>The Handly project aims to provide a foundation for construction of 
language-oriented handle-based models not unlike the JDT Java Model in 
their essential qualities. It borrows from core design patterns and principles 
used in the Java Model, but aims for a generalized albeit partial basic 
implementation, on top of which complete handle-based models for languages 
of diverse kinds could be built with relative ease. The project includes 
a layer of integration with Xtext.</p>

<h2>Background</h2>

<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->

<p>It is well known that the Java Model is one of the pillars of Eclipse 
Java development tools (JDT). To a great extent, it is the Java Model that 
makes possible seamless tool integration and unified user experience in JDT. 
The Java Model is designed to be a lightweight, fault-tolerant model with 
elements to which a reference can be kept; the model that is well suited 
for presenting in views. It meets those design goals with a handle-based, 
lazily-populated model that keeps no resolved information and wrappers the 
workspace resource model. Meanwhile, models with similar design properties 
might play an equally important role in Eclipse-based development tools 
for other languages.</p>

<p>It seems that core design patterns and principles used in the Java Model 
can be just as relevant to handle-based models for other languages. But as 
the Java Model shows, implementation of those patterns and principles might 
be a nontrivial undertaking. Doing it from scratch, over and over again for 
each language does not appear to be an attractive proposition, especially 
when dealing with languages on a large scale (e.g. DSL engineering). Can 
those patterns and principles be (at least partially) implemented in 
a framework of general usefulness? That is the question that seems 
worthwhile to investigate.<p>

<h2>Scope</h2>

<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->

The project is going to encompass the following areas of work:

<ul>
	<li><b>Core framework.</b> The main proposition of the project is a framework 
that attempts to distill the core abstractions of a handle-based model 
into a uniform API and aims to provide the raw materials deemed generally 
useful for implementing such models.</li>
	<li><b>Common UI components.</b> The API provided by the core framework 
can potentially enable construction of reusable UI components. 
This might be an area for future work.</li>
	<li><b>Integration with other Eclipse projects.</b> A layer of integration 
with the Xtext project is provided initially. Other integration layers 
may be devised in the future, when and if appropriate.</li>
	<li><b>Exemplary implementations.</b> To encourage adoption of the framework, 
comprehensive examples of its use must be provided. These examples would be 
most fruitful if they could also serve as practical tools.</li>
</ul>

<h2>Description</h2>

<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->

<p>It can be said that handle-based models are an important ingredient in 
an Eclipse IDE, if only because the workspace resource model is handle-based. 
The handle-based Java Model of JDT provides further evidence. The Java Model 
also shows that the (programming-language agnostic) resource model is often 
not enough: a language or a group of languages may need its own handle-based 
model that defines a code-centric view on resources and supports navigation 
down to structural elements inside a source file (which requires parsing 
the contents of the file). It is this class of language-oriented handle-based 
models that is the main interest of this project.</p>

<p>The project aims to investigate technology needed to deal with such 
handle-based models on a large scale. In particular, the project will attempt 
to distill the core abstractions of a handle-based model into a uniform API 
and supply basic building blocks for construction of such models. It borrows 
heavily from some of the design patterns and principles used in the Java Model, 
but aims for a generalized albeit partial implementation.</p>

<p>Thus, the project will provide a uniform API and a partial implementation for 
the central notion of a handle that acts like a key to a model element and has 
the following principal characteristics:</p>

<ul>
	<li>a value object - immutable, equality isn't based on identity</li>
	<li>can define the behavior of an element, but doesn't keep any element 
state (beyond the key information) - lightweight, never stale, references can 
be kept for a long time</li>
	<li>can refer to non-existing elements - existence can be tested with 
exists()</li>
</ul>

<p>A handle-based design gives a lot of freedom when resolving handles to find 
the corresponding element state (the body). For instance, an implementation 
can maintain a (not strictly) bounded LRU cache of element bodies. The handle 
acts like a key into the cache. The model swaps out bodies to make room for 
other elements (unsaved changes can never be swapped out). Bodies store the 
cached structure and attributes for a particular element and are computed on 
demand. The basic building blocks for such implementation will be supplied 
by the project.</p>

<p>Furthermore, the project will support the notion of a working copy 
(of a source file), its editing and reconciling. A layer of integration 
with Xtext will be provided for editing and reconciling a working copy 
connected to an Xtext editor.</p>

<p>While providing a uniform handle-based API, the project will try to impose 
as minimal restrictions as possible on the shape or the underlying language(s) 
of attainable models.</p>
 
<h2>Relationship with other Eclipse projects</h2>

<p>The project rests upon the Eclipse Platform and includes a layer of 
integration with Xtext; integration with other Eclipse projects is possible 
in the future.</p>

<p>There are existing efforts at Eclipse that might be viewed as related to 
the project in a way:</p>

<ul>
	<li><b>The IDE Meta-tooling Platform (IMP)</b> has a much wider scope 
than this project, and a different emphasis. Among other things, it provides 
a 'universal' language-oriented handle-based model, with limited extensibility. 
The current implementation doesn't support elements inside compilation units, 
the notion of a reconcilable working copy, or a handle/body design with a 
bounded LRU cache of bodies, all of which we'll have already covered in the 
initial contribution. More important, our approach is fundamentally different. 
Instead of providing a single complete handle-based model supposed to be a 
universal fit for all languages, we intend to provide an abstract API and a 
partial implementation for constructing any number of specific language-oriented 
handle-based models, with few restrictions on the shape of the model. 
<b>Update:</b> the IMP project has recently been archived (January 2014).</li>
	<li><b>Dynamic Languages Toolkit (DLTK)</b> provides (among other things) 
an extensible handle-based model intended for dynamic languages. The design 
and implementation are largely influenced by the Java Model. The DLTK model 
does support elements inside source modules and is otherwise similar to the 
Java Model in all essential qualities. However, it is predefined to contain 
elements pertaining to (a subset of) dynamic languages (e.g. a source module 
may contain types, fields and methods; a type may contain fields, methods and 
inner types). We aim to support implementation of custom handle-based models 
of almost arbitrary shapes, for languages of diverse kinds (including 
domain-specific languages).</li>
	<li><b>Xtext</b> is a wonderful tool for language engineering. It covers 
many important components of an Eclipse-based IDE in a generic and extensible 
way, but does not explicitly address handle-based models. As part of the 
initial contribution we'll provide an integration layer to support editing and 
reconciling of a working copy connected to an Xtext editor. We regard both 
projects as complementary and hope to collaborate with the Xtext team on 
further facilitating construction of handle-based models for Xtext-based 
languages. That said it's our explicit goal to keep the core framework 
completely neutral of any specific language implementation technology and 
encapsulate such dependencies strictly within corresponding integration layers.
	</li>
</ul>

<h2>Why Eclipse?</h2> 

<!-- 
	Answer these two questions:	What value does this project bring to the Eclipse
	community? What value do you expect to obtain from hosting your project at Eclipse?
	
	What value do you get by having your project at Eclipse over and above the value
	of hosting at Eclipse Labs?
 -->

<p>We believe the nature of this project makes Eclipse the right home for it. 
As stated above, handle-based models can play an important role in an 
Eclipse-based IDE. This supports our hope that the area of work to be developed 
by the project might be interesting and useful to the Eclipse community. 
At the same time, such project should definitely benefit from the wider 
exposure with the Eclipse community. We do welcome and hope for open 
participation and contributions from the community; diversity is really 
important for this project to succeed. The project would also benefit from 
the commendable Eclipse development process and IP management.</p>

<h2>Initial Contribution</h2>

<!-- 
	Projects are expected to arrive at Eclipse with existing code.
	
	Describe the existing code that will be contributed to the project. Please provide
	a couple of paragraphs describing the code with modest detail, including important
	information like code ownership (who holds the copyright?), and some consideration
	of community that exists around the code. Include a listing of third-party libraries 
	and associated licenses.
 -->

<p>The initial code base will be contributed by 
<a href="http://v8.1c.ru/eng/about_1c/">1C LLC</a> 
and includes the core framework and a layer of integration with Xtext.</p>

<p>Although the code is pretty stable and has been used internally with 
promising results, we regard it only as a starting point. The intent is to 
come up with a really nice design in this problem area through an open and 
transparent development process informed by community feedback.</p>

<p>Some parts of the initial design and implementation were adapted from 
Eclipse JDT and Xtext projects. The code makes direct references to the 
following third-party libraries, already present in the Eclipse Orbit 
repository:</p>

<ul>
	<li>com.google.inject 3.0.0 - Apache License, Version 2.0</li>
	<li>com.google.guava 10.0.1 - Apache License, Version 2.0</li>
	<li>org.apache.log4j 1.2.15 - Apache License, Version 2.0</li>
</ul>
 
<h2>Legal Issues</h2>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->

<p>There are no known legal issues.</p>
 
<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

<p>The following individuals are proposed as initial committers to the project:</p>

<dl>
	<dt>Vladimir Piskarev, 1C (lead)</dt>
	<dd>Vladimir is the main developer of the existing framework</dd>
	<dt>George Suaridze, 1C (co-lead)</dt>
	<dd>George is an early adopter of the existing framework</dd>
</dl>

<p>We welcome additional committers and contributions.</p>

<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Doug Schaefer</li>
	<li>Marcel Bruch</li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li>itemis AG</li>
<!--
	<li>Somebody else, Affiliation</li>
-->
</ul>

<h2>Project Scheduling</h2>

<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->

<p>The initial contribution is to be expected shortly after creation of the 
project and approval of the Eclipse IP Team. The first milestone build based 
on the initial code base should follow soon after that. Although no exact dates 
are given, we intend to make the initial contribution available to the 
community as soon as technically and legally possible.</p>

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>10-Jan-2014</td>
		<td>Document created</td>
	</tr>
</table>
</body>
</html>