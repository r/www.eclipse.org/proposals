<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Buckminster, Component
  Assembly Project</h1>

<h2>
Introduction</h2>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Buckminster Component Assembly");
?>

<p>The Buckminster, Component Assembly Project is a proposed open source project under the <a href=
"/technology/">Eclipse Technology Project</a>.</p>

<p>This proposal is in the Project Proposal Phase (as defined in
the <a href="/projects/dev_process/">
Eclipse Development Process</a> document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.buckminster">eclipse.technology.buckminster</a> newsgroup.</p>

<h2>
Overview</h2>

Developers are increasingly working with complex component structures comprising
code artifacts from a variety of sources implemented in disparate technologies.
The Eclipse Plug-In Development Environment (PDE) currently provides an
outstanding framework for developing componentized plug-ins and feature-sets.
However, Eclipse provides only limited support for materializing a workspace�i.e.
so that all component dependencies are fulfilled�a problem which is
particularly acute in team-based development. Moreover, the PDE�s component
management support is available only to components implemented as Eclipse
plug-ins, which limits its applicability to other types of development projects.
<p>The objective of the Buckminster, Component Assembly Project will be to leverage and extend the
Eclipse platform to bring mixed-component development comparable to Eclipse
plug-in development in terms of efficiency and ease of use, as well as to extend
the Eclipse component dependency model to allow component materialization based
on match rules. Buckminster will accomplish this objective by introducing a
project-agnostic way of describing the component structure and dependencies of
an arbitrarily complex development project, plus a mechanism for materializing
all corresponding source and binary artifacts.</p>
<p>Buckminster will be implemented as a set of Eclipse plug-ins that complement
existing Eclipse infrastructure. Functionality within the project�s initial
functional scope will include the following:</p>
<ul>
  <li><b>Complex dependency resolution</b>, providing recursive resolution of
    dependencies leveraging existing Eclipse &quot;Team Providers,&quot; with
    the addition of new retrievers, for exemplary purposes, covering source and
    binary artifacts that are not version-controlled in a traditional sense.
    Resolution will use a variety of versioning schemes and will be based on
    match rules similar to those found in the Eclipse plug-in framework. This
    will allow comparison of current and prior dependency resolutions to support
    update impact analyses. </li>
  <li><b>Uniform component dependency format</b>, using a component-type
    agnostic mechanism for describing components and their respective targets
    and dependency requirements. Most Eclipse projects, and many other component
    types, have some level of dependency information that can be leveraged.
    Extensions can be added to provide additional strategies for dependency
    pattern recognition. </li>
  <li><b>Intelligent retrieval mechanisms</b>, leveraging the Eclipse &quot;Team
    Project Set&quot; mechanism by separating the bill of material needed for a
    given configuration from its actual materialization. This is important since
    dependencies may appoint software that is locally installed on one machine
    but lacking on another. The bill of materials is typically shared between
    team members, although the materialization information may vary.</li>
  <li><b>Flexible project workspace binding</b>, allowing components
    materialized on disc to be bound to a workspace in different ways, including
    &quot;Proxy Projects&quot; consisting of links to physical artifacts and
    auto-generated Eclipse project information. These capabilities are helpful
    when sharing code or other artifacts that are not eclipse projects.</li>
</ul>
<p>The Buckminster Project�s initial scope will focus primarily on
materialization of components based on dependencies and rules, although this
scope may be expanded over time, to the extent appropriate and consistent with
its objectives. One of a number of requirements that may be addressed in the
future is tighter integration with Eclipse build systems. For example, this
would allow fulfillment of pre-built artifacts by actually building source
artifacts and then caching the results for group access.








<h2>Project
  Organization</h2>




We propose that this project be undertaken as a Technology subproject rather
than part of the Eclipse Platform Project. Being a Technology subproject gives
the project room to experiment with the technology without disruption to the
mainline Eclipse developers and users.
<p>The Buckminster Project will assist in the working with complex,
multi-sourced component structures, which is of increasing interest to open
source consortia, commercial vendors and internal users and developers of open
source packages and distributions. Accordingly, it will be vitally important to
include in this project individuals who are active participants in or consumers
of other open source projects and can therefore represent a range of experiences
and requirements.</p>
<p>It will also be important to retain as much continuity as possible with the
existing Eclipse Platform; therefore, we will reach out to existing Platform
committers for advice and for inclusion in this project.</p>
<p>Finally, since Technology Sub-projects are not meant to be ongoing, we
foresee three possible evolutionary paths for the subproject:</p>
<ol>
  <li>The project is merged back into the mainline Eclipse Platform top-level
    project.</li>
  <li>The project is moved to a permanent state as a Tools Platform project. </li>
  <li>The technology proves to be insufficient for Eclipse and the work is set
    aside. </li>
</ol>
<p>These paths are not mutually exclusive. Some combination of some or all of
these paths may be the result of the work done in this project.








<h2>
Interested Parties</h2>




This section captures the list of companies that have expressed interest in the
project and/or its components, and as such will be updated periodically to
reflect the growing interest in this project.
<p>The Buckminster Project is being proposed by ObjectWeb in association with
its individual members listed below. We anticipate adding members to the
Buckminster Project team in conjunction with approval and launch of the project.</p>
<ul>
  <li>Individuals from a variety of commercial software vendors and end-user
    organizations, including organizations that are members of ObjectWeb, have
    expressed an interest in participating in the project.</li>
  <li>Additional participants from the Eclipse community at large are also
    invited to participate. </li>
</ul>
<p><b>Sponsoring Organization</b></p>
<ul>
  <li>ObjectWeb</li>
</ul>
<p><b>Initial Committers</b></p>
<ul>
  <li>Mitch Sonies</li>
  <li>Thomas Hallgren</li>
  <li>Kenneth Olwing</li>
  <li>Pontus Rydin</li>
</ul>




</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

