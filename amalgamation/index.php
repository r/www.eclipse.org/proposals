<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Eclipse Modeling Amalgamation Project";
$pageKeywords	= "modeling, model-driven development, ocl, dsl, uml, qvt, model transformation";
$pageAuthor		= "Richard C. Gronback";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Modeling Amalgamation Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Amalgamation");
?>

<p>This proposal is presented in accordance with the <a href="">Eclipse Development Process</a> and is written 
to declare the project's intent and scope as well as to solicit additional participation and feedback from the 
Eclipse community. You are invited to join the project and to provide feedback using the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling.amalgam">newsgroup</a>.
</p>

<h3>Introduction</h3>
<p>
The Eclipse Modeling Project (EMP) was formed as a top-level project at Eclipse in order to bring together a number 
of modeling-related projects.  While the Modeling project has been a success in many respects, it remains somewhat 
a collection of individual projects with little or no focus on integration or ease-of-use.  As stated in the EMP 
<a href="http://www.eclipse.org/modeling/modeling-charter.php">charter</a>, the "Eclipse Modeling Project will focus 
on the evolution and promotion of model-based development technologies within the Eclipse community. It will unite 
projects falling into this classification to bring holistic model-based development capabilities to Eclipse."  While 
the task of unification is underway, the task of promotion is hindered by difficult installation, lack of integration, 
and poor usability.  The intention of the Modeling Amalgamation project proposal is to augment the current Modeling 
project with one focused solely on refining the "user experience" when using Modeling project technologies.
</p>

<h3>Project Scope</h3>
<p>
The Amalgamation project will provide integrations and improved packaging of Modeling projects, through the use of product 
definition builds and the <a href="http://www.eclipse.org/epp">Eclipse Packaging Project</a>.  Configurations will 
be driven by community demand and offer additional usability offerings when working with several Modeling projects.  
Specifically, the Amalgamation project will provide:
</p>
<ul>
	<li>Modeling package downloads with update site for commercial vendor consumption of modeling technologies</li>
	<li>A Modeling product download with focus on "toolsmith" functionality (e.g. EMF/GMF/M2M/M2T/TMF SDKs)</li>
	<li>A Modeling product download with focus on "practitioner" functionality (e.g. UML diagramming)</li>
	<li>A unified set of modeling capability definitions with Welcome screen activation/deactivation</li>
	<li>A Modeling project type with facilities to add/remove modeling-related natures and builders</li>
	<li>Integration and UI features that don't "fit" within individual Modeling projects</li>
</ul>

<p>
It is believed that a considerable number of bugs will be submitted to the underlying Modeling projects as the result 
of such an effort, leading to the improved quality, user interface, commercial adoption, and end user ease-of-use that is 
expected of all Eclipse projects.
</p>

<p>
Note that this project proposal does not fit within the current scope of the Modeling project charter, and therefore will 
require PMC approval, followed by Board approval, for its addition to the Modeling project.
</p>

<h3>Project Organization</h3>
<p>
The project will be hosted within the top-level Modeling project, with CVS module /csvroot/modeling/org.eclipse.amalgam 
at dev.eclipse.org, <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling.amalgam">newsgroup</a>, 
amalgam-dev mailing list, build, download, and update site.
</p>

<h4>Interested Parties</h4>
<p>
The following individuals have expressed interest in supporting the Modeling Amalgamation project through contribution:
</p>
<ul>
	<li>Richard Gronback - Borland Software Corporation (proposed Project Lead)</li>
	<li>Ian Buchanan - Borland Software Corporation</li>
	<li>Sven Efftinge - Independent</li>
	<li>Bernd Kolb - Independent</li>
	<li>Peter Friese - Gentleware</li>
</ul>
<p>
The following individuals/organizations have expressed their support for the project:
<ul>
	<li>openArchitectureWare - Markus Voelter, et al</li>
	<li></li>
</ul>
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
