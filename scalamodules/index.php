<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1>ScalaModules</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("ScalaModules");
?>

<div>

<p>The ScalaModules project is a proposed open source project under the
<a href="http://www.eclipse.org/equinox/"> Equinox Project</a>.</p>


<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send feedback to 
the <a href="http://www.eclipse.org/forums/eclipse.scalamodules">ScalaModules forum</a>.</p>

<h3>Introduction</h3>

<p>Java has emerged as one of the leading platforms for enterprise
computing: There are lots of mission-critical web applications, rich
clients and mobile applications based on the Java platform. It is
commonly agreed that the Java Virtual Machine will continue to shape the
future of enterprise computing for years to come. But it is also clear
that the importance of the Java Language will decrease, as other
languages for the Java Virtual Machine enter the game. This is mainly
because these languages make software development easier.</p>

<p>Perhaps the most promising new language for the Java Virtual Machine
is Scala. It feels a lot like a dynamic language, but it is statically
typed and offers all the benefits of type checking. It is extremely
flexible, e.g. offers type and semicolon inference, infix operator
notation and implicit conversions, etc. It is a very pragmatic blend of
object oriented and functional approaches. In the end, many developers
simply find that developing in Scala is easier than in Java.</p>

<p>One of the biggest challenges in today's software development is
complexity. To be successful, it is crucial to identify methods and
technologies to reduce complexity in order to increase productivity and
flexibility. One such method is modularization, and one such technology
is the OSGi Service Platform. OSGi is even more: It is the Dynamic
Module System for Java, which offers installing, updating and
uninstalling modules at runtime, as well as a service-oriented way for
modules to collaborate. OSGi has emerged as the de facto standard for
Java modularization, and currently there are even efforts to integrate
modularization into the platform itself.</p>

<p>Scala and OSGi both aim at very important concerns in software
development: Ease of use and reduced complexity. Scala operates at the
programming language level, and OSGi at the higher level of a module
system. Hence it is natural to combine those two to get the best out of
both. And this is straightforward, because Scala compiles to "usual"
Java bytecode and offers full interoperability with Java. OSGi is
agnostic with regard to the programming language, because it works only
with the compiled artifacts and the metadata in the bundle manifest.
Hence it is time for OSGi on Scala.</p>

<p>ScalaModules aims at Scala based OSGi development. The mission of
ScalaModules is to employ the power of the Scala programming language to
ease OSGi development.</p>

<p>Of course using Scala for OSGi will itself be beneficial, because of
the great simplifications Scala brings compared to Java. But
ScalaModules will also make use of the additional possibilities offered
by Scala, mainly the chance to create a Domain Specific Language (DSL).
Therefore with ScalaModules your code will be more intuitive and concise
or less verbose and less involved compared to Java-based OSGi
development.</p>

<h3>Scope</h3>

<p>This project will create a Scala DSL for OSGi as a sub-project of
Eclipse Equinox. It will build on the existing codebase (1.x release)
and enhance it further. Graduation is planned with the 2.0 release.</p>

<p>This project will not create any tools.</p>

<h3>Organization</h3>

<h4>Status quo</h4>

<p>ScalaModules currently is an open source project led by Heiko
Seeberger (WeigleWilczek). It was released in version 1.0 in March 2009
and is currently under active development towards version 2.0. The
current licence is Apache License, Version 2.0, but this will be changed
to Eclipse Public License v1.0 as soon as the project is moved to
Eclipse. All ScalaModules developers will be initial committers (see
below) and have agreed to change the license.</p>

<p>ScalaModules relies on OSGi-fied Scala libraries, because the
official Scala distribution, that is licensed under the Scala License
(http://www.scala-lang.org/node/146) does not offer OSGi bundles. These
OSGi-fied Scala libraries are provided by another small project led by
Heiko Seeberger, also licensed under Apache License, Version 2.0. In
order to avoid depending on this external project it will be considered
to OSGi-fy the Scala Libraries withing the ScalaModules project itself.
On the long run there are chances that the official Scala distribution
will offer OSGi bundles.</p>

<h4>Initial Committers</h4>

<p>The initial committers will take the existing codebase and
documentation and move it to Eclipse. They will continue the existing
work necessary for the release of version 2.0.</p>
<ul>
	<li>Heiko Seeberger (WeigleWilczek): Project Lead</li>
	<li>Roman Roelofsen (ProSyst): Developer</li>
	<li>Kjetil Valstadsve: Developer</li>
</ul>

<h4>Mentors</h4>
<ul>
<li>Chris Aniszczyk</li>
<li>Wayne Beaton</li>
</ul>

<h4>Interested Parties</h4>
The following people or organizations have expressed interest in
contributing to ScalaModules:
<ul>
	<li>Neil Bartlett (WeigleWilczek UK)</li>
	<li>Kevin Wright (Vyre Ltd.)</li>
	<li>Miles Sabin (Chuusai Ltd.)</li>
</ul>

<h3>Tentative Plan</h3>
<p>ScalaModules is aligned with the <a
	href="http://wiki.eclipse.org/Helios_Simultaneous_Release">Helios
release plan</a>.
<p>
<table>
	<tr>
		<td>2010-02-01:</td>
		<td>Start moving the codebase and documentation to Eclipse</td>
	</tr>
	<tr>
		<td>2010-03-19:</td>
		<td>Milestone 1</td>
	</tr>
	<tr>
		<td>2010-05-07:</td>
		<td>Milestone 2</td>
	</tr>
	<tr>
		<td>2010-05-28:</td>
		<td>Release Candidate 1</td>
	</tr>
	<tr>
		<td>2010-06-11:</td>
		<td>Release Candidate 2</td>
	</tr>
	<tr>
		<td>2010-06-23:</td>
		<td>ScalaModules 2.0 Release && Graduation</td>
	</tr>
</table>

<p>Scala is a trademark of <a href="http://www.epfl.ch/index.en.html">EPFL</a>.</p>

</div>

</div>
</div>
<?php
$html = ob_get_contents();
ob_end_clean();

# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
