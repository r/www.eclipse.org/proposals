<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
  
    <h1>phoenix</h1><br>
       
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Phoenix");
?>
  

      <p>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/">Eclipse 
        Development Process document</a>) and is written to declare the intent 
        and scope of a proposed Technology PMC Project called Project Phoenix. In addition, this proposal is written 
        to solicit additional participation and inputs from the Eclipse community. 
        You are invited to comment on and/or join the project. Please send all 
        feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.phoenix">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.phoenix</a> 
        newsgroup.
 </p>
   
     
  <h2>
    <b>Introduction</b>
  </h2>
  
    
      <p>The scope of this project is to improve the accessibility and quality 
        of the information needed by the community to perform their tasks and 
        provide a single access point to that information. The &quot;community&quot; 
        is the very broad set of constituents: consumers, committers, contributors 
        and members who all have requirements which need to be met by the Eclipse.org 
        website.</p>
      <p>Keen observers of the Eclipse Foundation will have noticed that we have 
        successfully completed our hardware migration. There is still work to 
        be done as we have a few more servers to bring up. But we are now running 
        the site on brand new servers. </p>
      <p>Many thanks to our webmaster Denis Roy for several lost weekends and 
        many long hours getting this done. Thanks also to HP, IBM, Intel and Novell 
        for donating the hardware and software to run this new infrastructure. 
        Thanks are also due to Susan Iwai and others who kept the website up and 
        its content fresh during the past several years of wild growth in projects, 
        members and users.</p>
      <p>The next stage is to get started on improving the functionality of our 
        website. We believe that it is fair to say that everyone agrees that an 
        overhaul is needed.
      
  


  <h2>
    <b>Description</b>
  </h2>
  
     
      <h3><br>
        1. Background</h3>
		<hr>
      <p> The purpose of this project is to improve the usability of the eclipse.org 
        site, improving productivity and reducing duplication of effort across 
        the eclipse user community. One of the key goals of the project is to 
        employ a Content Management System, in addition to making critical improvements 
        to the website that will include: improved navigation and information 
        support for an expanded user base; new channels for members to message 
        and collaborate with each other and the community; new channels for developer 
        interactions.</p>
      <p>This project will analyse the information needs of the Eclipse.org audience[1]. 
        Initial project findings include the following opportunities:</p>
      <ol>
        <li> Improve the accessibility and quality of the information needed by 
          the community to perform their tasks; provide a single access point 
          to that information; link and rationalise the information, thereby reducing 
          duplication of information and removing access to out-of-date information<br>
        </li>
        <li>Make technology available (easy to find, understand and download)<br>
          a) Tools (such as plug-ins)<br>
          b) Resources (support information for those tools)</li>
        <li>Establish publishing models (workflow) and guidelines for Eclipse.org</li>
        <li>Implement cross-jurisdictional collaborative tools (e.g. WIKI, Blogs) 
          and make the most of networking opportunities</li>
        <li>Create a showcase for members and the community</li>
        <li> Establish user profile administration if necessary</li>
      </ol>
      <p>This project addresses the first opportunity above &#150; &#147;Improve 
        the accessibility and quality of the information&#133;&#148; - and aligns 
        with the following vision statement and web site values&#133;<br>
        <br>
        <b>Vision</b><br>
        The Eclipse.org web site should: enable collaboration and networking within 
        the community, encourage usage, create opportunities and thrive on the 
        active participation of its audience.<br>
        <br>
        <b>Values</b> <br>
        The Eclipse.org web site should&#133;</p>
      <ol>
        <li>Be community focused</li>
        <li>Place emphasis on clarity of content in its delivery</li>
        <li>Provide an open, meritocratic and equal playing field for users</li>
        <li>Reflect the character of the entire ecosystem.</li>
      </ol>
	  [1] The audiences for Eclipse.org have been initially identified 
      as: <br>
      1. Users<br>
      2. Committers<br>
      3. Contributors<br>
      4. Plug-In Developer<br>
      5. Members  
      <h3><br>
        <br>
        2. Project Objectives</h3>
		<hr>
      <p>&#147;Eclipse.org&#148; is an essential part of the Eclipse Foundation&#146;s 
        initiative to share information and knowledge across its world-wide community. 
        This requires the creation of an information system that can be easily 
        used by community members of diverse skill levels. This information system 
        will reside on one web site that will: </p>
      <ol>
        <li>Provide access to information across the Eclipse community in a manner 
          that:<br>
          &middot; Enables easy processing &#150; providing a hierarchical navigation 
          scheme (taxonomy) <br>
          &middot; Providing multiple routes to information<br>
          &middot; Offers visual feedback<br>
          &middot; Meets the audience&#146;s purpose</li>
        <li>Be a vehicle for community-wide communication, collaboration and networking 
          <br>
          &middot; providing access to collaborative tools such as WIKIS, Blogs<br>
          &middot; storing information in English while supporting other language 
          groups as required</li>
        <li>Places our audience at the focal point of design and development</li>
        <li> Seeks feedback (on the design &amp; development) from the community 
          by using the open source rules of engagement</li>
        <li> Reflect the professional character of the Eclipse development platform 
          and its community</li>
        <li> Ensures CMS tool <br>
          i. allows for easy additions &amp; restructuring<br>
          ii. has a clear separation of content and presentation</li>
        <li> Identify content ownership while enabling content owners to manage 
          their own content, eliminate redundant and/or expired information, and 
          be the key drivers in the site's development (e.g. projects)</li>
      </ol>
      <p><br>
        <br>
      </p>
      <h3>3. Draft Deliverables</h3>
	  <hr>
      <p><b>Phase Milestone Deliverables</b> <br>
      </p>
      <ol>
        <li> Visualisation and Analysis<br>
          &#149; Project proposal &amp; objectives <br>
          &#149; Design &amp; functional targets April 2005</li>
        <li> Site Architecture and Planning<br>
          &#149; Information Architecture Plan [site vocabulary, directory catalogue, 
          information data model, use case analysis &amp; storyboards]<br>
          &#149; User interface Plan [graphic specifications]<br>
          &#149; Systems Plan [CMS tool analysis technical specifications, enterprise 
          architecture coordination} June 2005</li>
        <li> Initial Site Design<br>
          &#149; navigation design, <br>
          &#149; thematic development<br>
          &#149; site wireframes </li>
        <li>Site Design Refined<br>
          &#149; on line alpha testing<br>
          &#149; directory catalogue refinements<br>
          &#149; technical refinements </li>
        <li>Site Development and Testing<br>
          &#149; install development environment<br>
          &#149; implement data model<br>
          &#149; information and content migration<br>
          &#149; front and GUI development<br>
          &#149; admin GUI development</li>
        <li>Deploy to Production<br>
          &#149; online beta testing &amp; refinements </li>
        <li>Site Launch </li>
        <li>Support and Maintenance </li>
        <li>Site Evaluation </li>
        <li>Prioritize ongoing enhancements October 2005</li>
      </ol>
    
  


  <h2>
    <b>Organization</b>
  </h2>
  
     
      <p>We are looking for others to participate in all aspects of this project. 
        If you are interested in actively participating, please take part in the 
        newsgroup discussions or ask to be added to the list of interested parties. 
        <br>
        The proposed initial set of commiters will be: </p>
      <ul>
        <li>Mike Milinkovich, Eclipse Foundation</li>
        <li>Denis Roy, Eclipse Foundation</li>
        <li>Susan Iwai, IBM</li>
        <li>Bjorn Freeman-Benson, Eclipse Foundation</li>
        <li>Ian Skerrett, Eclipse Foundation</li>
        <li>Andrew Geraghty, Eclipse Foundation</li>
      </ul>
    
  

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
