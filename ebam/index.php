<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>eBAM</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("eBAM");
?>

<h2>Introduction</h2>

<p align="left">EBAM (Extended Business Activity Monitoring) is an open source project, proposed under the 
Eclipse SOA top-level project. 
This proposal is presented in accordance with the Eclipse Development Process. It is intended to declare the project 
scope as well as to solicit the Eclipse community's participation and feedback using the 
<a href="http://www.eclipse.org/forums/eclipse.ebam">eBAM</a> forum.
</p>
	
<h2>Overview</h2>
<p align="left">This project is intended to realize a monitoring platform supporting the performance  and management 
analysis of external systems. In an enterprise context, applications are typically executed in different environments 
and can produce many types of event logging, eBAM allows you to build a monitor to analyse the business process, 
it supplies a solution to aggregate information in a central repository which can be used to produce real-time KPI, 
SLA and notifications for different channels. One of the main aspect is that you can correlate different type 
of information coming from different environments.
Eclipse has several projects that deal with profiling, monitoring and reporting activities. The goal of eBAM is to provide 
both build-time and runtime tools for a BAM platform. 
This document presents the scope and organizational structure of the project. A project charter will be created soon 
in order to describe in more detail the principles of the project, the roles and responsibilities of its 
participants as well as its development process.
</p>

<h2>Project Principles</h2>
The key principles of the project are:
<ul>
<li><b>Leverage Eclipse Ecosystem</b>: the project aims to closely work with other Eclipse project areas such as 
Eclipse SOA Tools Platform, Eclipse RT, Eclipse Business Intelligence and Reporting Tools (BIRT), Eclipse 
modelling Tools and Eclipse TPTP.</li>
<li><b>Vendor Neutral</b>:  the project promotes open source runtime technology, based on Equinox,  in order 
to create a comprehensive BAM solution</li>
<li><b>Modularity</b>: the project architecture is based on a modular solution, so the users can always select and 
and use the components that more suit their needs.</li> 
</ul>


<h2>Project Scope</h2>
<p align="left">The eBAM project focuses on the evolution and promotion of a middleware solution for Business 
Activity Monitor, which can be used for any type of services (infrastructure, applications, BPM, SOA).  
it also enables the real-time control of heterogeneous applicative and technological contexts, presenting 
monitoring dashboards, producing alarms in critical situations (which are defined according to specific rules) 
and sending events which can interact with external systems.
</p>

eBAM main features are:
<ul>
<li>Real-time data management,  based on the event logic</li>
<li>Automatic data collection from distributed systems</li>
<li>Settlement of event rules through CEP engine</li>
<li>Settlement of alarms and SLA</li>
<li>Monitoring console and control over services processes </li>
<li>Bidirectional interaction with external services.</li>
<li>Integration with STP Intermediate Model.</li>
</ul>

<p align="left">The integration between eBAM and source systems is based on Adapters, specialized for each 
channel/protocol (log file; JMS, DB, SOAP code; etc.), which enable to:
<ul>
<li>Recover external messages, on which users can apply CEP rules to generate relevant events and alarms</li>
<li>Send notifications to external systems or actors.</li>
</ul>
</p>
A dashboard, based on BIRT reports and/or an applicative console, allow users to monitor and control the systems processes and/or services, managed through a bidirectional modality.
eBAM main components will be developed according to the OSGI and Equinox technology, in order to help the reuse, hot deploy as well as the management of the components versioning, without restarting the source systems. 
The integration between eBAM and STP Intermediate Model is useful to share information through the various STP/SOA TLP editors and tools. With this integration, eBAM is able to import or export all 
the information in "intermediate model" format.

<h2>Proposed Components</h2>
<p align="left">The following components define the internal structure of the eBAM project:
<ul>  
<li><p align="left"><b>Adapter DataFlow Services:</b> It gathers the information of one's interest from many heterogeneous and 
distributed data sources (JMS, DB, log files, XML, JMX, SOAP, etc.). Each one extracts the information from an 
external service (JMS, SOAP, JDBC, Log, workflow, BPEL, ESB, OSGi, ...) and turns them into an Eclipse CBE 
(Common Base Event) Format. Several adapters will be developed during the project life cycle. The analysis will 
require the integration of  the Generic Log Adapter of the Eclipse TPTP Monitoring Tools Project.</li>
<li><p align="left"><b>Event Manager</b>: the project implements new OSGi modules for Equinox which work as a gateway for 
different rule engines (e.g. OpenL Table and JBoss Drools). Starting from the SLA and alert definition of every KPI.</li> 
<li><p align="left"><b>Meta model area:</b> Metadata repository (CEP, SLA, alarm rules) and cache systems for the input of data. 
This area is close to the meta model (EMF) project and will be realized as new OSGi modules for Equinox, by 
EclipseLink and SDO.</li>
<li><p align="left"><b>Alert Services:</b> It will be based on a component which can detect the threshold alarms on CEP 
events and interact with external notification systems through the definition of a standard interface.</li>
<li><p align="left"><b>Dashboard Services:</b> graphic front-end interface, enabling the analysis of the indicators of 
one's interest and providing processes monitoring dashboards. It will be based on the use of BIRT and, 
if necessary, on the extension of an applicative console.</li>
</ul>
</p>
<div><img src="architecture.jpg" alt="Architecture of eBAM" style="border: medium none ;" width="500" height="400">
</div>
<p align="left">
The project adds the following tools to support runtime execution:
<ul>
<li><b>SLA Editor:</b> to define the Thresholds.</li>
<li><b>Alarm Editor:</b> to define how to handle an alarm.</li>
<li><b>Rules Editor:</b> to define CEP rules and alarms configuration.</li>
</ul>
</p>

<h2>Relation with other Eclipse Projects</h2>
<ul>
<li>Eclipse TPTP</li>
<li>Eclipse CBE (Common Base Event)</li>
<li>Eclipse Swordfish</li>
<li>EclipseLink</li>
<li>Eclipse BIRT</li>
<li>Eclipse EMF</li>
<li>Eclipse JWT (Workflow Administration and Monitoring)</li>
<li>Eclipse STP Intermediate Model</li>
</ul>

<h2>Organization</h2>
<p align="left">Everyone is invited to express their concern or opinion about the above-proposed 
eBAM project and to offer their support to its development using the newsgroup.
Our intention is to start developing the project directly in the source repository of Eclipse, just from the 
the incubation phase. Below is a preliminary list of the developers directly involved in the project. 
Any design or development contribution is welcome.
</p>

<p align="left"><b>Mentors</b></p>
<p align="left">
<ul>
<li>Dave Carver</li>
<li>C&eacute;dric Brun</li>
</ul>
</p>

<p align="left"><b>Committers</b></p>
<p align="left">
<ul>
<li>Angelo Bernabei, Engineering (proposed as Project Lead)</li>
<li>Andrea Zoppello, Engineering </li>
<li>Antonietta Miele, Engineering </li>
<li>Andrea Gioia, Engineering </li>
<li>Giulio Gavardi, Engineering </li>
<li>Marco Cortella, Engineering </li>
<li>Antonella Giachino, Engineering </li>
</ul>
More committers expected by partners.
</p>


<p align="left"><b>Interested parties</b></p>
<p align="left">
The following companies have shown their interest in this project. Key contacts listed.
<ul>
<li>Alain Boulze, INRIA</li>
<li>Ricco Deutscher,  SOPERA </li>
<li>Florian Lautenbacher, University of Augsburg</li>
</ul>
</p>

<h2>Tentative Plan</h2>
<p align="left">
The development of the eBAM project is still on-going. We would greatly appreciate 
improvement suggestions and feedback coming from the Eclipse community.
The incremental development process includes the following milestones:
<ul>
<li><p align="left"><b>2010-06:</b> when we'll release the Release Candidate of eBAM</li>
<li><p align="left"><b>2010-08:</b> when we'll release the 0.7 version of eBAM</li>
</ul>
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
