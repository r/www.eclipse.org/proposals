<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
  <h1>Native Application Builder (eWideStudio)</h1><br>
   </p>

<h2>Introduction</h2>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Native Application Builder (eWideStudio)");
?>
<p>The <i>Native Application Builder (eWideStudio)</i> is a proposed 
  open source project under the <a href="/dsdp/">Device Software Development Platform Project</a>.</p>
<p>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/"> 
  Eclipse Development Process document</a>) and is written to declare its intent 
  and scope. This proposal is written to solicit additional participation and 
  input from the Eclipse community. You are invited to comment on and/or join 
  the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology</a> newsgroup.&nbsp; </p>
<h2>Background</h2>
<p align="left">The <i></i><i>NAB (eWideStudio)</i> project was born from WideStudio/MWT, 
  it is a open source software of multi-platform GUI tools. That is mostly used 
  for making a desktop GUI application on a PC by individuals and professional 
  developer. And another major usage is for building and running GUIs on embedded 
  devices, it is used by embedded device software engineers. </p>
<p align="left">WideStudio/MWT is composed by 2 part, one is a original visual 
  GUI application builder that we port (It seems like as a VE), and other one 
  is multi-platform GUI library called MWT (Multiplatform Widget Tookit). </p>
<p align="left">MWT is a light weight GUI library but it have a variety of GUI 
  parts from simple buttons to complex tree list. And it have non-GUI parts such 
  as event handling, threading/synchronization, Client-Server functions for remote 
  computing and so on. Once, if GUI application was developed on the MWT library, 
  it is possible to run on the other platforms (such as Win32, a Linux/X-window, 
  ITron and so on) only by compiling again because there is interchangeability 
  of the source code. </p>
<p align="left">This <i>NAB</i> project started at WideStudio/MWT. But it aim 
  for an extensible framework for various tools. </p>
<table border="0" cellspacing="1" cellpadding="2">
  <tr> 
    <td rowspan="13" width="431"><img src="figure1.gif" width="440" height="234"></td>
    <td bgcolor="#0080C0"> 
      <div align="center"><font color="#FFFFFF"><b>Supported Platforms of MWT</b></font></div>
    </td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;X11</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;Linux + DirectFB</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;Linux /dev/fb</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;mu-CLinux /dev/fb</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;ZAURUS (Frame Buffer)</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;Windows 95/98/Me/NT/2000/XP</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;WindowsCE</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;MacOSX native</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;Standard T-Engine ARM / SH / MIPS(T-Shell)</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;Standard T-Engine ARM / SH / MIPS(FrameBuffer)</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;ITRON (Frame Buffer)</td>
  </tr>
  <tr> 
    <td bgcolor="#D3E9ED"> &nbsp;BTRON/Cho-Kanji</td>
  </tr>
</table>
<p align="left">Please see the following WEB sites for more details of WideStudio/MWT.<br>
  <a href="http://www.widestudio.org/EE/index.html">English</a>, <a href="http://www.widestudio.org/ja/index.html">Japanese</a>, 
  <a href="http://www.widestudio.org/cn/index.html">Chinese</a>, <a href="http://www.widestudio.org/ko/index.html">Korean</a>, 
  <a href="http://www.widestudio.org/pt/index.html">Portuguese</a></p>
<h2>Description</h2>
		
<h3>Architecture</h3>
<p align="left">We have a tentative architecture plan which is &quot;Framework 
  and example Tool&quot; model.</p>
<p align="left"><i>NAB</i> stand on the two frameworks there are <i>NAEF</i> (Native 
  Application Editor Framework) and <i>NCGF</i> (Native Code Generation Framework). 
  <i>NAEF</i> have the functions for visual editing an application which is builded 
  on the <i>NTK</i> (Native Tool Kit). <i>NCGF</i> cooperate with <i>NAEF</i> 
  for driving a <i>NCG</i> (Native Code Generator). And <i>NAB</i> is a tool implementation 
  part. We make a <i>MWTAB</i> (MWT Application builder) as example.</p>
<table border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td><img src="Architecture.gif" width="566" height="231"></td>
  </tr>
  <tr>
    <td>
      <div align="center"><i>NAB</i> arcitecture</div>
    </td>
  </tr>
</table>
<h3>Functions</h3>
<p align="left">The example application <i>MWTAB</i> provides the function to 
  make GUI application in the WYSIWYG using the MWT library, it is equal to original 
  WideStudio Application Builder. The Functions are below.</p>
<ul>
  <li> GUI Edit Function<br>
    MWT application are generated as well as the VE plug-in for the Java language, 
    and can visually arrange, edit, and the source code be generated. Various 
    GUI parts can be arranged with the mouse to build the application window, 
    and attributes of parts can be specified visually. </li>
</ul>
<ul>
  <li>C/C++ Source Code Generation and Edit Function<br>
    Event procedures of each MWT parts are generated automatically. Moreover, 
    the program can be adding described at each event processing. </li>
</ul>
<ul>
  <li> Application Build/Execution Function<br>
    The source code can be compiled and an executable application can be generated. 
    Moreover, the application can be executed in the Eclipse environment. </li>
</ul>
<p align="left">The <i>MWTAB</i> needs 2 other parts, one is CDT and another one 
  is MWT library plug-ins distributing from WideStudio.org. That includes MWT 
  binary libraries (<i>MWT</i>, <i>MWTBI</i> and <i>MWTCG</i>) and their very 
  thin Java wrapper.</p>
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="ScreenShot.gif" width="743" height="534" ></td>
  </tr>
  <tr>
    <td>
      <div align="center">screen shot of the <i>MWTAB</i> Beta on Aug. 2005</div>
    </td>
  </tr>
</table>
<h2>Organization</h2>

<h3>Proposed project lead and initial committers</h3>
<ul>
  <li>Shigeki Moride, Fujitsu (proposed Project lead)</li>
  <li>Shun-ichi Hirabayashi, Fujitsu / WideStudio.org Founder</li>
</ul>
		<h3>Interested parties</h3>
<p align="left">The following companies and projects have expressed interest in 
  this project. Key contacts listed. 
<ul>
  <li><a href="http://www.fujitsu.com">Fujitsu</a></li>
  <li><a href="http://www.widestudio.org/">WideStudio Development Team (WideStudio.org)</a></li>
  <li><a href="http://www.eclipse.org/japanwg/">Eclipse Japan Working Group</a></li>
</ul>

<h3>Relationship with WideStudio project</h3>
<p>Proposed comitter Mr. Hirabayashi is a founder of WideStudio, and it is the 
  main developer and it is representative of that community now. Moreover, he 
  is developing the <i>NAB</i> on the other hand as an employee of Fujitsu. We 
  think the relation of <i>NAB</i> and WideStudio can be kept intimate through 
  him.</p>
<h3>Developer / User community</h3>
<p> WideStudio community have a many users and developers. WideStusio's ML 
  have thousands of subscribers from over 20 countries and FTP site counts 800,000 
  downloads. And we expect half of users come to use the <i>NAB (eWideStudio)</i>. 
  As a result our projects community will grow up quickly and we will encourage 
  all active contributors to become committers. </p>
<p>We plan on doing this by using the standard eclipse.org mechanisms of supporting 
  an open project and community, and we'd like to prepare some mechanisms to communicate 
  with Japanese in Japan WG.
</p>
<h3>Code Contributions</h3>

<p align="left">Fujitsu have developed facilities similar to what the <i>NAB (eWideStudio)</i> 
  proposes, we intend to conduct a review of all potential contributions during 
  the Validation Phase of the project. Those contributions which best align with 
  the goals of the project will be refactored and used as the starting point for 
  the <i>NAB (eWideStudio)</i>.
<h3>Tentative plan</h3>
<p align="left">We think there is two directionality of the project for the future. 
  One is direction to standard of cross GUI development platform. Another one 
  is direction to the platform that have minimal resource devices, cooperating 
  with eRCP and others. We think we can do some contributions in both cases. 
<p align="left">We will continue the developing and we think that cooperation 
  with eRCP, SWT, AWT is better. We'd like to have a discussion about that.<font color="#CC6699"><br>
  </font> 
<p align="left">&nbsp; 
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
