<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Model Execution Framework (MXF)</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("MXF");
?>

<p>
<h2>Introduction</h2>

<p>
The Model Execution Framework (MXF) is a proposed open source project under 
the <a href="http://www.eclipse.org/modeling/emft">Eclipse Modeling Framework Technology</a> (EMFT).
The main aim of MXF is to provide a framework for development, execution and debugging of 
models with operational semantics.</p>
<p>You are invited to comment and/or join the project. Please send all feedback to the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">emft 
newsgroup</a>.</p>

<h2>Background and Motivation</h2>

<p>
Over the last years, the eclipse modeling projects have been successfully applied &#8212; in both, academic and industry &#8212;
to develop and integrate model-driven tooling, especially for domain specific languages (DSLs).
While <a href="http://www.eclipse.org/modeling/mdt/?project=emf">EMF</a> is used as core to specify (meta-)models in
conjunction with <a href="http://www.eclipse.org/modeling/mdt/?project=ocl">OCL</a> for constraints, supplementary
components support the quick development of tooling by means of editors (textual or
graphical), model transformations, code generators, and so on.
However, there is currently no component to specify execution semantics (i.e. operational semantics) and interpret
these definitions.
We see the need for a framework that supports the definition of executable models and that instantly supports
design validation by running those models.
</p>

<p>
A prototypical implementation of the framework has been developed as <strong>M3Actions framework</strong> and is currently
available at
<a href="http://sourceforge.net/projects/m3actions/">sourceforge.org</a> (see the
<a href="http://www.metamodels.de">project's website</a> for further information). 
This framework is centered around a graphical action language to precisely define models with their executable behavior.
A reflective interpreter is able to execute and debug these definitions.
The overall framework supports multiple meta-layers via an explicit <em>instantiation</em> concept where runtime models are
defined as logical instances of the abstract syntax. These runtime models are the model parts that change over time and these changes can be recorded during execution as a trace.
</p>

<h2>Scope</h2>
<p>
The MXF will build extensible frameworks and exemplary tools for executable 
models, e.g. an editor to define metamodels and runtime models and their behavior 
in terms of an action language. 
An interpreter and debugger will support the execution and testing of these models as well as recording of simulation runs
for further analysis. 
The language for operational semantics might have a graphical and textual syntax and shall integrate other languages by
means of black-box operations, library implementations, and so on. The common execution infrastructure will define 
common concepts on top of the Eclipse debugging framework and will enable applications to share runtime models, adapters
for specific editors and debuggers, tracing capabilities, and more.
MXF will provide integration with GMF for building DSL simulators that are seamlessly integrated into a generated editor.     
</p>

<h4>Proposed Components</h4>
<ul>
<li><b>Graphical Editor</b> A graphical editor capable of specifying the operational semantics of EMF models.
The definitions will include the abstract syntax of a language, the runtime model (both EMF models) as well as the behavior
model.  
</li>
<li><b>Interpreter and execution environment</b> Reflective execution environment to interpret the models.
</li>
<li><b>Generic debugger</b> A debugger for the interpreter.
</li>
<li><b>Execution Trace model</b> A generic trace model and adapter concept to record execution runs.  
</li> 
<li><b>Sample trace recorder</b> An implementation that performs recording of runs, either file-based or via database.
</li>
</ul>

<p>
In the long term, language-specific simulator-/debugger-generators will be developed that optimize runtime performance
of the generic model interpreter. Moreover, a set of resuable models/runtime libraries might be developed to support
rapid development of simulators for new languages.       
</p>

<h2>Organization</h2>
<p>
We propose to develop this technology as a new project under the Eclipse Modeling Framework Technology project.
</p>

<h3>Mentors</h3>
<ul>
<li>Ed Merks (Macro Modeling)</li>
<li>Richard Gronback (Borland)</li>
</ul>

<h3>Proposed initial committers</h3>
<ul>
	<li>Michael Soden (<a href="http://www.ikv.de">ikv++ technologies ag</a>), proposed project lead</li>
	<li>Hajo Eichler (<a href="http://www.ikv.de">ikv++ technologies ag</a>), proposed project co-lead</li>
	<li>Markus Scheidgen (<a href="http://www2.informatik.hu-berlin.de/sam/">Humboldt University Berlin</a>), Committer (<i>to be confirmed</i>)</li>
</ul>

<h3>Interested parties</h3>
<p align="left">The following companies and organizations have expressed interest in this project. Key contacts listed.</p>
<ul>
	<li>ikv++ technologies ag - Dr. Marc Born (born@ikv.de)</li>
	<li>Humboldt University Berlin - Prof. Joachim Fischer (fischer@informatik.hu-berlin.de)</li>
	<li>itemis AG - Wolfgang Neuhaus (wolfgang.neuhaus@itemis.de)</li>
	<li>Fraunhofer FOKUS - Tom Ritter (Tom.Ritter@fokus.fraunhofer.de)</li>
	<li>Obeo -  Stephane Lacrampe (stephane.lacrampe@obeo.fr)</li>
	<li>CEA LIST - S&egrave;bastien G&egrave;rard (sebastien.gerard@cea.fr)</li>
	<li>Anyware Technologies - David Sciamma (david.sciamma@anyware-tech.com)</li>
	<li>INRIA - Didier Vojtisek (didier.vojtisek@irisa.fr)</li>
	<li>FeRIA - Marc Pantel (Marc.Pantel@enseeiht.fr)</li>
	<li>Atos Origin - Rapha&euml;l Faudou (raphael.faudou@atosorigin.com)</li>
	<li>University of Agder - Terje Gj&oslash;s&aelig;ter (terje.gjosater@uia.no)</li>
</ul>

<h3 align="left">Code Contributions</h3>

<p align="left">The contributors will offer the initial code contribution currently managed as open source
project at <a href="http://sourceforge.net/projects/m3actions/">sourceforge.org</a> under
<a href="http://www.eclipse.org/legal/epl-v10.html">EPL</a>; visit the
<a href="http://www.metamodels.de">project's website</a> for further information and documentation.</p>

<h2>Tentative Plan</h2>
<p>
The project plan heavily depends on approval and feedback of the community. Development is currently ongoing at sourceforge.org, so
after migrating to Eclipse and setting up the project, a first step will be to revise and stabilize the existing code base
until October 2009. During that setup phase, we want to better integrate with and connect to existing projects (EMF, Ecore tools, etc.).  
</p>
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
