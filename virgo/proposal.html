<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Dynamic Enterprise Application Platform Project (Virgo)</title>
</head>

<body>
<p>The Dynamic Enterprise Application Platform project (nicknamed
Virgo) is a proposed open source project under the <a
	href="http://www.eclipse.org/rt">Eclipse Runtime Project</a>.</p>

<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the <a
href="http://eclipse.org/forums/eclipse.virgo">Virgo</a> 
Eclipse Forum.</p>

<h2>Introduction</h2>

<p>
This proposal recommends the creation of a new project called "Dynamic
Enterprise Application Platform", nicknamed Virgo, to provide a
runtime platform for the development of server-side enterprise
applications built on top of Equinox, and optionally using modules
from the Gemini project1. Virgo will be a subproject of the Eclipse
Runtime Project and will strive to leverage the functionality of
existing projects. We encourage and request additional participation
and input from any and all interested parties in the Eclipse
community. 
</p>

<h2>Background</h2>

<p>
The development of server-side enterprise applications, packaged as
OSGi bundles, and deployed to an Equinox based runtime has proven to
require considerable support at runtime over and above that provided
by the OSGi Service Platform itself.  SpringSource have gained
considerable expertise in this area over the last few years through
the development of the dm Server open source project. Other projects
such as Apache Karaf, Apache Aries, and Paremus' Newton also explore
this space. Some of the key requirements include: 
</p>
<ul>
  <li>a notion of an &quot;application&quot; as an entity comprised of one or more bundles</li>
  <li>deployment and provisioning of applications and their dependencies</li>
  <li>scoping of applications (bundles and services)</li>
  <li>support for use of third-party enterprise libraries within an OSGi context</li>
  <li>configuration, administration and management support</li>
</ul>
<p>
The Virgo project will develop a Dynamic Enterprise Application
Platform runtime that supports these requirements by building on top
of Equinox, the initial contributions to the Virgo project will be
made by SpringSource based upon the dm Server codebase. See the
section on <a href="#initcontrib">Initial Contributions</a> below for more details.
</p>

<h2>Scope</h2>

<p>
The scope of the Virgo project is restricted to:
</p>
<ul>
<li>the development of an Equinox-based dynamic runtime platform for
  server-side enterprise applications deployed as OSGi bundles</li>
</ul>
<p>
Developer tools supporting the runtime platform are out of scope for
Virgo, see the following section on <a href="#relatedcontrib">related contributions</a> below for
details of how these requirements will be addressed.</p>

<h2>Description</h2>

<p>
The goal of the Virgo project is to provide a Dynamic Enterprise
Application Platform for the development of server-side enterprise
applications based on Equinox. Examples of server-side enterprise
applications include web applications, integration applications, batch
processing applications, or any combination thereof.
</p>
<p>
The core components of Virgo at inception will be:
</p>
<ul>
 <li>a dynamic enterprise kernel supporting the core application,
  deployment, and provisioning model</li> 
 <li>an integrated server runtime that builds on top of the kernel and
 adds in web container, configuration, and administration support</li>
 <li>a remote repository application for hosting a remote provisioning
 repository</li>
 <li>the medic module, providing configurable per-application logging
 services for enterprise applications deployed to the kernel.</li>
</ul>
<p>
Virgo will be licensed under the EPL.
</p>

<h3>Relationship to existing Eclipse projects</h3>

<h4>RCP and RAP</h4>
<p>
Virgo complements the Eclipse RT Rich Client Platform (RCP), which
targets the development of rich client applications based on Equinox,
and the Rich Ajax Platform (RAP), which targets the development of
rich Ajax-enabled web applications. Whereas these projects are focused
on rich client applications, Virgo targets server-side applications.
</p>

<h4>Equinox</h4>
<p>
Virgo is built on top of Equinox and embeds the Equinox runtime in its
distribution. Virgo takes advantage of leading edge features in
Equinox such as the developing support for nested frameworks. In
addition, Virgo will depend upon the Event Admin and Configuration
Admin implementations from Equinox. The existing dm Server code uses
the Apache Felix versions of these services, and the intention is to
migrate to the Equinox implementations once the Virgo project is
established.
</p>

<h4>Gemini</h4>
<p>
Virgo provides a runtime enterprise application model on top of
Equinox, with integrated application and provisioning support. The
Gemini project at Eclipse.org provides implementations of enterprise
capabilities that can be exploited by such applications. The Virgo
kernel provides just the foundational capabilities and depends solely
on the Gemini Blueprint Service project. The Virgo server distribution
pre-integrates the Gemini Web Container and Blueprint Service
projects. Additional capabilities provided by Gemini projects can
easily be added to the Virgo Server simply by deploying the
appropriate bundle provided by those projects.
</p>

<h4>Jetty</h4>
<p>The Virgo server obtains its web application support via the Eclipse
Gemini Web Container project. This project in turn uses an embedded
Apache Tomcat servlet container. For Virgo to support Jetty, two thin
adapters will be written in collaboration with the Jetty team. The
adapters will reside in Gemini and Virgo and will be maintained by the
committers of those projects.</p>

<h4>ECF</h4>
<p>
The Virgo Server provides a core application, provisioning, and
administration model. The Eclipse Communication Framework (ECF)
provides bundles that support the creation of distributed
applications, including support for the Distributed OSGi specification
from R4.2 of the OSGi Compendium Services specification. ECF bundles
can be deployed into a Virgo runtime to support the development of
distributed server-side applications.
</p>

<h4>P2</h4>
<p>
Equinox p2 is the Eclipse provisioning framework first introduced in
Eclipse 3.4. Provisioning in Virgo is based on the configuration of a
repository chain, where a repository implements a specified repository
interface. By implementing a p2-backed version of the Virgo repository
interface, it will be possible to integrate p2-based provisioning with
the Virgo runtime.
</p>

<h4>Swordfish</h4>
<p>
Swordfish is an OSGi-based SOA framework that builds on Apache
ServiceMix and Apache CXF to create a distributed Enterprise Service
Bus (ESB). Whereas Swordfish builds on JBI (Java Business Integration)
to support a web services based communication model, Virgo is neutral
with respect to application style and provides no explicit support for
business processes or web services.  
</p> 
 
<h2><a name="initcontrib"></a>Initial Contribution</h2>

<p>
The initial contributions for Virgo and its related contributions (<a
href="#relatedcontrib">see below</a>)  will be made by SpringSource
from the existing SpringSource dm Server, dm Server Tools, and Bundlor
projects.
</p>

<p>
The SpringSource dm Server project has been under active development
for over two years, and version 2.0 was released in January 2010. The
project is well known within the enterprise OSGi community for
pioneering the use of OSGi not just as a means for building an
application server, but also as the means for building applications to
be deployed to that server. The current dm Server project page can be
found at <a
href="http://www.springsource.org/dmserver">http://www.springsource.org/dmserver</a>.
The dm Server today is an open source project released under the
GPL (some parts of the code base are licensed under the Apache
License, version 2). All of the code from the dm Server project will be relicensed
under the EPL as part of the donation to Eclipse.org.
</p>

<p>
Through a rich set of interactions with a user community of both open
source and commercial developers over the last two years, the dm
Server has evolved a key set of capabilities and features needed to
build enterprise applications on OSGi. <a href="
http://www.springsource.com/customers/case-studies">Customer case
studies</a> relating to the use of dm Server in industry can be found
on the SpringSource web site.
</p>

<p>
The online <a
href="http://static.springsource.org/s2-dmserver/2.0.x/programmer-guide/html/">Programmers
Guide</a>  details the supported deployment formats
(regular bundles, WAR files, PAR files - a packaging construct that
groups related bundles into an application, Plan files - a description
of resources to be installed into the server, and properties files),
how to build web applications taking advantage of shared libraries and
shared services, and how to work with common third-party enterprise
libraries. The dm Server runtime contains extensive support to enable
third-party enterprise libraries to work "out of the box" in an OSGi
Service Platform - handling issues such as context class loading, JPA
instrumentation, load-time weaving, resource loading and more.
</p>

<p>
The dm Server <a
href="http://static.springsource.org/s2-dmserver/2.0.x/user-guide/html/">User
Guide</a> covers the installation and administration
features including the administration console and the shell. It also
discusses the provisioning and repository support, and the extensive
serviceability features built into the server: event logging,
per-application tracing, and service dump support. 
</p>
 
<h2>Legal Issues</h2>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->
<p>The Bundlor trademark will be assigned to the Eclipse Foundation as
   part of this move.</p>

<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

<p>The following individuals are proposed as initial committers to the project:</p>

<ul>
	<li>Andy Wilkinson, SpringSource</li>
	<li>Ben Hale, SpringSource</li>
	<li>Chris Frost, SpringSource</li>
	<li>Christian Dupuis, SpringSource</li>
	<li>Costin Leau, SpringSource</li>
	<li>Glyn Normington, SpringSource (Project Lead)</li>
	<li>Leo Dos Santos, Tasktop</li>
	<li>Rob Harrop, SpringSource</li>
	<li>Steffen Pingel, Tasktop</li>
	<li>Steve Powell, SpringSource</li>
</ul>

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Jeff McAffer</li>
	<li>Mik Kersten</li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li>Tasktop</li>
	<li>SAP</li>
</ul>

<h2><a name="relatedcontrib"></a>Related Contributions</h2>

<p>
Developer tools that provide support for Virgo are out of scope for
the Virgo project as part of Eclipse RT. However, the Virgo team also
propose to work with the existing WTP and PDE projects to contribute
relevant developer tools for Virgo to those projects. These
contributions will be based upon the existing Bundlor and dm Server
development tools projects detailed below.
</p>

<h3>Bundlor</h3>
<p>
The Bundlor project is designed to help enterprise application
developers create and maintain high quality manifests for their
bundles. <a href="http://www.springsource.org/bundlor">Bundlor</a> is
currently hosted at springsource.org. 
</p>
<p>
With the increasing focus on OSGi in Enterprise Java, there is a need
to create OSGi bundles for deployment. Bundlor provides a manifest
templating mechanism that makes it much easier for developers to
create and maintain high quality OSGi manifests for their bundles and
can be integrated into both a build system and the Eclipse
IDE. Bundlor is also very useful for generating manifests for existing
enterprise libraries that you may wish to use in conjunction with your
project and which are not already packaged as OSGi bundles.
</p>
<p>
It can be very hard for a developer to know exactly what dependencies
a library or project requires using simple inspection, and maintaining
this list over time is error prone. Bundlor is a tool that automates
the detection of dependencies and the creation of OSGi manifest
directives for JARs after their creation. Bundlor takes as input a JAR
and a template consisting of a superset of the standard OSGi manifest
headers. Bundlor analyses the source code and support files contained
in the JAR, applies the template to the results, and generates a
manifest. Currently Bundlor can detect dependencies in Java source
code, Spring configuration files, JPA configuration files, Hibernate
mapping files, and web.xml files.
</p>
<p>
The use of Bundlor can take different forms, from tasks for Apache ANT
and plugins for Apache Maven, to simple command line execution for
integration into any existing build system. An <a
href="http://blog.springsource.com/2009/03/26/using-bundlor-in-eclipse/">overview
of using Bundlor from inside Eclipse</a> is provided on the SpringSource blog.
</p>
<p>
The <a
href="http://static.springsource.org/s2-bundlor/1.0.x/user-guide/html/">Bundlor
User Guide</a> provides full details of these capabilities. 
Bundlor operates at the OSGi Service Platform level, and does not
contain any explicit support for Eclipse Plugin Development. The
Eclipse PDE project provides tools to create, develop, test, debug,
build and deploy Eclipse plug-ins, fragments, features, update sites
and RCP products. The scope of the <a href="http://www.eclipse.org/pde/">PDE</a> project also includes the
provision of "comprehensive OSGi tooling, which makes it an ideal
environment for component programming, not just Eclipse plug-in
development". Bundlor is a very good match with this latter remit,
and is proposed for contribution to Eclipse.org as a PDE Component.
</p>
<p>
The PDE "New Plug-in from Existing JAR Archives" wizard contains
functionality that overlaps in part with Bundlor, in that it can be
used to generate a manifest for existing jar files. Unlike Bundlor,
the manifest generation is not controllable via template, and does not
understand non-source code artefacts commonly used in enterprise
projects. Bundlor could be used behind the scenes in this wizard for
enhanced manifest generation if so desired.
</p>

<h3>Virgo Development Tools</h3>
<p>
The existing <a href="http://www.springsource.org/dmservertools">dm
Server tools project</a> at SpringSource.org provides functionality
that may be contributed to the WTP5 and PDE projects. This support
includes: 
</p>
<ul>
  </li>PDE user interface extensions</li>
  <ul>
    <li>editor support for Virgo PAR files and Plan files;</li>
    <li>a Bundle Classpath Container that manages a bundle project's
    classpath based on a manifest (with incremental manifest
    generation via Bundlor); </li>
    <li>Manifest editing and validation support: content-assist,
    hyperlinking and validation for manifests, with quick fixes for
    common problems</li>
  </ul>
  <li>WTP integration for the Virgo server platform</li>
  <ul>
    <li>drag-and-drop deployment</li>
    <li>graphical bundle & packages overview</li>
    <li>integrated shell support</li>
    <li>integrated provisioning for Virgo repository management</li>
  </ul>
</ul>
<p>
The PDE project currently provides a manifest editor with multiple
pages, including support for plugin.xml artefacts. The dm Server tools
editor is focused on the job of editing OSGi manifests for developers
who are creating enterprise OSGi bundles (not Eclipse plugins). The
features in common between the two editors have a shared
look-and-feel.
</p>
<p>
The ideal direction would be to create a Bundle Development Environment (BDE) on top of which PDE could build in the future.
</p>
<p>
For more information on the dm Server tools capabilities, see the
following <a
href="http://blog.springsource.com/2009/03/05/osgi-development-tools-in-sts/">SpringSource
blog entry</a>, and the <a
href="http://download.springsource.com/release/STS/doc/STS-new_and_noteworthy-2.2.1.200910210131-RELEASE.pdf">STS
New and Noteworthy</a> documentation for the latest release. 
</p>

<h2>Project Scheduling</h2>

<p>
The initial milestone will be to get the IP code contributions for
these projects approved, and the projects provisioned on Eclipse.org
infrastructure. This will be based off of the 2.0 release version of
the donor projects. A provisional plan for subsequent development is
to release a 2.0.1 service release in 1Q10, and a 2.1 release
alongside the 2010 Eclipse release train. The intention is for the
Virgo project to become a formal part of the release train in 2011.
</p>

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>25-Jan-2010</td>
		<td>Section concerning Virgo's relationship with Jetty updated.</td>
	</tr>
	<tr>
		<td>12-Jan-2010</td>
		<td>Document created</td>
	</tr>
</table>
</body>
</html>