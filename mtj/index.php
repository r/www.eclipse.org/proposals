<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Mobile Tools for Java</h1><br>
.
</p>

<h2>Introduction</h2>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Mobile Tools for Java");
?>
Mobile Tools for Java (MTJ) is a proposed open source project under the <b>
<a target="_top" href="/dsdp/">Device Software Development 
Platform (DSDP)</a></b>.<p>This proposal is in the Project Proposal Phase (as defined 
in the
<a href="/projects/dev_process/">
Eclipse Development Process document</a>) and is written to declare its intent and 
scope. This proposal is written to solicit additional participation and input from 
the Eclipse community. You are invited to comment on and/or join the project. Please 
send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.dsdp.mtj">http://www.eclipse.org/newsportal/thread.php?group=eclipse.dsdp.mtj</a> 
newsgroup.</p>

<h2>Background</h2>
<p>The Java programming language is becoming more and more popular in mobile 
devices. Also, the richness of the device Java environment is getting better all the 
time and more applications can be written using Java, instead of native 
languages. As Java continues to grow in popularity, along with the proliferation 
of higher functioning mobile devices, it is apparent that Java applications can 
be developed to run on multiple targets, with a common set of application code.
<br>
Developing applications to the mobile Java environment presents unique 
challenges to developers. Specifically, unlike the straightforward J2SE and J2EE 
environments, there are a number of configurations and profiles (such as 
<a href="http://java.sun.com/products/midp/index.jsp">MIDP</a> on 
top of the <a href="http://java.sun.com/products/cldc/index.jsp">CLDC</a> 
and <a href="http://java.sun.com/products/foundation/index.jsp">Foundation</a> and
<a href="http://java.sun.com/products/personalprofile/index.jsp">Personal Profiles</a> on top of the 
<a href="http://java.sun.com/products/cdc/">CDC</a>), along with a number of JSRs 
(and umbrella JSRs such as <a href="http://java.sun.com/products/jtwi/">JTWI</a> or 
<a href="http://www.jcp.org/en/jsr/detail?id=248">JSR-248</a>) that require development tools to assist 
in managing the runtimes/class libraries for development work and runtime 
binding. This ability to develop for multiple targets and use common source code 
with different build configurations is critical in mobile Java development 
projects. In addition to this management of runtimes and the challenges it 
presents, mobile Java applications have unique launching and debug requirements 
and unlike J2SE or J2EE, applications are not always just placed on a server for 
download as needed. Rather, developers require device emulators for 
on-development-system test and debug, the ability to launch, test, debug and 
analyze performance of the applications on the devices themselves, where varying 
classes of these devices have different methods and levels of connectivity. 
There is also the need for a robust deployment solution to deliver the code 
under development to the device, and also be able to map that to a production 
deployment solution. Of course, this is all in addition to the �normal� set of 
application development tools (such as code creation, UI design and so on) for a 
specific market segment.<br>
A common set of tooling and frameworks across these targets, and the mobile Java 
space makes the development effort and cost manageable for developers.&nbsp; This is 
why it is important to have robust Eclipse frameworks in place to support mobile 
Java application development alongside the rich client and server counterparts</p>

<h2>Description</h2>
<p>The goal of the Mobile Tools for Java project is to extend existing Eclipse 
frameworks to support mobile device Java application development.<br>
MTJ will enable developers to develop, debug and deploy mobile Java applications 
to emulators and real devices.</p>
<h3>Core features:</h3>
<h4>Device &amp; Emulator framework</h4>
<p>Normally runtimes are managed in Eclipse as a JRE. In the mobile segment, the 
runtime environment is a device emulator or real device. The MTJ project will 
provide features to manage mobile runtimes and provide frameworks for device 
vendors to add those runtimes to the development environment.</p>
<h4>Deployment framework</h4>
<p>One vital part in mobile development is testing on real devices. To make that 
as easy as possible the developer must have methods to transfer mobile 
applications to handheld devices using local methods (e.g. Bluetooth, USB, 
IrDA). The idea of MTJ is to develop a framework that provides an API for vendor 
specific plug-ins, which then do the actual deployment. </p>
<h4>Generic and customizable build process for mobile application development</h4>
<p>The goal is to enhance normal build process with mobile extensions like JAR 
and JAD file generation. With the varying configurations and profiles of mobile 
Java, this is a critical feature to enable developers to manage code production. 
Another requirement is to provide ways to add additional tasks to the build 
process e.g. signing. This work should extend the builder frameworks of Eclipse.</p>
<h4>Debugging</h4>
<p>This is related to Device &amp; Emulator framework. The goal of this item is to 
enhance the current Debugging environment so that it is possible to use mobile 
runtimes, either emulator or a real device. This task extends to launching the JVM 
and applications(s) on the local emulator or on the device itself, and allowing 
the developer to attach to that application under test. In the mobile space, 
this is tightly integrated with the Device and Emulator frameworks, and will 
need to provide a robust framework for device and platform makers to extend to 
their devices� specific connectivity. This work should extend the debug 
frameworks of Eclipse.</p>
<h4>Application creation wizard</h4>
<p>Application wizards in general exist in the Eclipse platform. It is possible 
to generate Java projects with or without application skeleton code but the 
existing wizards are not usable for Mobile development. This task is tied to the 
differing configurations and profiles of mobile Java and relieves the developer 
of needing to worry about the boilerplate code for each of the application 
configuration/profile types. One of the goals of MTJ is to enhance the existing 
wizards by providing Mobile specific project wizard.</p>
<h4>UI design tools</h4>
<p>The goal of these tools is to improve efficiency (easy drag and drop without 
coding) and also decrease the entry barrier for newcomers. There are already 
visual designers in the Eclipse platform but they don�t contain support for 
mobile devices. The target is to bring mobile UI styles, components and layout 
rules to Eclipse. The idea is to create a framework that enables the use of 
different kinds of UI implementations e.g. different resolutions, different 
vendor specific look and feel.. Also in the scope of this project, in addition 
to this framework is at a minimum. a generic UI designer implementation. <br>
A Screen Flow designer tool would provide ways to develop application logic 
easily. It would provide easy drag and drop functionality to add different kind 
of screens and transitions between them. These transitions are caused by mobile 
specific ways like commands, list item selections.<br>
The idea is to utilize Eclipse <a href="/vep/">Visual Editor 
Project </a>and extend it so that device screen 
engines from different vendors can be plugged in.</p>
<h4>Localization</h4>
<p>The mobile application market is world wide so applications typically need to 
be localized. Therefore, an important requirement is for tooling that makes this 
task easier.</p>
<h4>Obfuscation and Code Size/Performance Optimization</h4>
<p>Mobile devices are restricted by memory so it is important that code is 
compresses as small as possible. There are a number of ways this can be 
accomplished. Obfuscation is one possibility, along with tooling and frameworks 
to enable performance and size analysis on the emulators or physical devices, 
which can be driven back into the build process or just for the developer to 
streamline their own code.</p>
<h4>Signing tool </h4>
<p>Security becomes more important in mobile devices because they are open 
and accessible to 3rd party developers. One solution for this is to require that 
the applications be signed with an authorized signing certificate. The signing 
process is very similar to signing a normal JAR signing but there are some 
specific mobile needs. The goal of MTJ is to support tooling to sign mobile 
applications. This signing tool will provide extensibility to add solution
specific signing tools. This is exemplary of other external tools for mobile 
Java that need to be able to be integrated to a customizable build process. </p>
<h2>Status</h2>
<p>This project is currently in proposal phase and Nokia is actively seeking 
interested parties for this project. Detailed planning for initial delivery of 
MTJ is underway and development is beginning.</p>
<h2>Organization</h2>
<a href="http://www.nokia.com/">Nokia</a>, as the submitter of this proposal, welcomes 
interested parties to post to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.dsdp.mtj">eclipse.dsdp.mtj</a> newsgroup 
and ask to be added to the list as interested parties or to suggest changes to this 
document. The proposed project members include:
<h3>Proposed project lead and initial committers</h3>
<ul>
	<li>
	<p>Mika Hoikkala, Nokia (proposed project lead)</p>
	</li>
	<li>
	<p>Arto Laurila, Nokia</p>
	</li>
	<li>
	<p>Marius Voicu, Nokia</p>
	</li>
	<li>
	<p>Minna Bloigu, Nokia</p>
	</li>
	<li>
	<p>+ 2 TBD, Nokia<br />
	</p>
	</li>
</ul>
<h3>Interested parties</h3>
<h4>The following companies will participate in this project.</h4>
<ul>
	<li>IBM, David Reich</li>
</ul>
<h4>The following companies have expressed interest in this project.</h4>
<ul>
	<li>
	<p>Sybase</p>
	</li>
</ul>
<h2>Tentative Plan</h2>
<p>The goal for the MTJ project is to have a first release simultaneous with the 
next main release of the Platform. Due to the aggressive schedule, the premise 
for this first release is to have the core set of functionality to provide a 
project that is usable by a mobile Java developer to generate and test code.</p>
<p>It is hoped and assumed that a fair amount of needed functionality will be 
contributed by Nokia and also by other project contributors. The first step for 
the project is to understand scope of initial contribution and then plan what 
new code needs to developed, what to utilize from contributions and how to 
refactor and integrated new and existing code to one initial package.</p>
Regardless of the initial contribution, focus in the first phase is to develop 
mandatory pieces from a mobile development point of view. The most important 
features are Device &amp; Emulator framework, Deployment framework and Mobile 
enhanced build process. This work will be immediately followed by code 
development tooling such as wizards and UI builder. Then followed by other 
tooling like localization and obfuscation.<p>
The working draft of the plan calls for interim milestone releases in December 
2005, March 2006 with a first release level delivery concurrent with the next 
major release of the Eclipse platform (at the time of this writing, this would 
be Eclipse 3.2). Due to the early stage of this project this is subject to 
change.</p>
It is a challenging task to define the content and future directions of the 
Mobile Tools for Java project at this point. The high level intent is to provide 
mobile tool frameworks for any interesting enhancements in the mobile world. 
There will be a placeholder for any JSR or de facto standard that gets industry 
acceptance. Here are a few examples: 
<a href="http://www.jcp.org/en/jsr/detail?id=232">JSR-232</a> (Mobile Operational Management),
<a href="http://www.jcp.org/en/jsr/detail?id=249">JSR-249</a> (Mobile Service Architecture for CDC), 
<a href="http://www.jcp.org/en/jsr/detail?id=271">JSR-271</a> (MIDP 3.0) and so on.</p>

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
