<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
<div id="maincontent">
	<div id="midcolumn">

		<h1>
			FireFly DevKit - The Mobile Web Developer Kit
		</h1>

		<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Blinki formerly FireFly");
?>

		<h2>
			1. Introduction
		</h2>

<p> 
  The&nbsp;FireFly DevKit Project is a proposed open source subproject under the 
  <a href="http://www.eclipse.org/dsdp" id="p-s_" title="Device Software Development Platform">Device 
  Software Development Platform</a> (DSDP). This proposal is in the Project 
  Proposal Phase, as defined in the 
  <a href="http://www.eclipse.org/projects/dev_process">Eclipse Development 
  Process document</a>, and is written to declare its intent and scope. This 
  proposal is written to solicit additional participation and input from the 
  Eclipse community. You are invited to comment on and/or join the project. 
  Please send all feedback to the 
  <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.firefly" title="http://www.eclipse.org/newsportal/thread.php?group=eclipse.firefly">http://www.eclipse.org/newsportal/thread.php?group=eclipse.firefly</a> 
  newsgroup.&nbsp;</p>
  
		<p> 
			The FireFly DevKit project intends to develop an extensible mobile 
			web developer kit for use in creating and testing traditional and 
			next-generation mobile RIA applications.&nbsp;  
			The new generation of high-speed data networks and powerful mobile 
			internet devices such as smartphones equipped with web browsing 
			capabilities like the iPhone are enabling a dramatic new level of 
			personal and social connected-ness for millions of mobile users. 
			These users effectively take the web with them wherever they go. 
			Unfortunately much of the web content and services currently 
			available to mobile users is designed for use on laptops and work 
			stations and is generally not well suited for use on the much smaller 
			mobile internet devices. The FireFly DevKit will help address the 
			development challenges that web designers and programmers face when 
			modernizing existing web content and developing new innovative web 
			applications and services for use on mobile internet.&nbsp;</p>
			
		<h2>
			2. Scope
		</h2>
		<p>
			The FireFly mobile web developer kit will consist of the following
			extensible frameworks and toolkits, and equally important,
			educational resources:
			<ul>
				<li>
						Eclipse Tool Frameworks
					<ul>
						<li>
							Mobile Web Previewer Framework (<a href="#section2_1">2.1</a>)
						</li>
						<li style="margin-bottom:10pt">
							Mobile Web Debugger Framework (<a href="#section2_2">2.2</a>)
						</li>
					</ul>
				</li>
				<li style="margin-bottom:10pt">
					Mobile Web Rendering Kit (<a href="#section2_3">2.3</a>)
				</li>
				<li>
					Mobile Rich Internet Application Services
					<ul>
						<li>
							Device Service Access Framework (<a href="#section2_4">2.4</a>)
						</li>
						<li style="margin-bottom:10pt;">
							Deployment Framework (<a href="#section2_5">2.5</a>)
						</li>
					</ul>
				</li>
				<li>
					Educational Resources (<a href="#section2_6">2.6</a>)
				</li>
			</ul>
		</p>
		<p> 
			It seems that the most successful Eclipse projects include developer 
			tools at some level; the FireFly DevKit project is no exception. Two 
			tool frameworks will be developed that enable mobile web developers 
			to visualize and debug mobile web applications from within an 
			Eclipse-based IDE. Beyond this the FireFly project will develop 
			next-generation technologies and frameworks to support the creation of 
			mobile web applications that look and behavior similarly to native 
			applications and are able to interact with device services such as 
			GPS, accelerometers and personal data. Lastly, unlike many OSS 
			projects, educational resources and community development will be 
			given a level of prominence at least as high as code development. We 
			plan to develop how-to articles, tutorials, best practice guidelines 
			and webinars for the mobile web developer community. Additionally we 
			will provide architectural, design and API documentation for 
			developers wishing to extend any of the FireFly frameworks. 
		</p>
		<p> 
			Initially, example implementations of the project frameworks will be 
			provided for the iPhone. As resources become available, examples for 
			the G1-Android platform will also be developed. Additionally, the 
			project will actively recruit and accept contributions for other 
			mobile platforms such as Symbian, Windows Mobile and others. 
		</p>
		<p>
			The following diagram provides a graphic organization of the FireFly
			DevKit components.
		</p>
		<p>
			<img src="diagram.gif">
		</p>
		<p>
			Each of these components is described in more detail in the remainder
			of this section. For a discussion of what is not in scope see <a href="#section_3">Section
			3</a>.
		</p>
		<h3>
			<a id="section2_1"/>2.1 Mobile Web Previewer Framework</a>
		</h3>
		<p>
			When programming, most developers dislike switching between
			unintegrated tools and environments. Frequent change of focus
			interrupts their flow of concentration, reduces their efficiency and
			makes them generally grumpier :). For mobile web application
			development, web designers and programmers need to quickly and
			seamlessly perform incremental development and testing directly
			within an IDE environment rather than switching from an IDE to a
			device testing environment and back again. The Mobile Web Previewer
			Framework is an extensible Eclipse tools framework for the creation
			of mobile device emulators specifically for previewing web content in
			a realistic WYSIWYG manner. This framework will enable developers to
			preview web content and services in an emulated mobile device web
			browser from within an Eclipse IDE. Some of the key features of the
			Previewer Framework will include:
		</p>
		<ul>
			<li>
				an embeddable WebKit browser component and mobile Firefox when made
				public
			</li>
			<li>
				device description and layout engine
			</li>
			<li>
				skin support for creating full sized, photo realistic device
				faceplates
			</li>
			<li>
				simulation of device orientation
			</li>
			<li>
				simulation of touch screen gestures and events
			</li>
			<li>
				simulation of browser user-agents
			</li>
			<li>
				extensible Eclipse plugin model for incorporating new browser
				components and device descriptions
			</li>
			<li>
				exemplary iPhone previewer will be provided to support framework
				testing and to serve as a guide for developers creating custom
				previewers. Additional device previewers will be considered from
				other contributors.
			</li>
		</ul>
		<p>
			A key expectation of this framework is that it will enable mobile
			internet device providers and tool developers to rapidly create
			custom previewers for the growing population of new devices as well
			as existing devices.
		</p>

		<h3>
			<a id="section2_2"/>2.2 Mobile Web Debugger Framework</a>
		</h3>
		<p>
			Efficient mobile web application development requires the support of
			debugging tools. The Mobile Web Debugger Framework will provide APIs
			and a pluggable implementation model for supporting the debug
			capabilities of multiple mobile web browsers. The framework will
			integrate with the Eclipse debugger infrastructure and provide
			developers insight into the execution state of their mobile web
			content and dynamic UI behaviors. Initial development effort will
			provide debugging support for simulated mobile web environments
			provided by previewers built on top of the Mobile Web Previewer
			Framework. Following that, support will be provided for remote
			inspection and debugging directly on the device in its mobile web
			browser. Key features of this framework will include:
		</p>
		<ul>
			<li>
				execution breakpoints
			</li>
			<li>
				execution stepping
			</li>
			<li>
				inspection of current web DOM content
			</li>
			<li>
				console output support
			</li>
			<li>
				local and remote debugger support for WebKit
			</li>
			<li>
				local debugger support for the Mozilla component
			</li>
		</ul>
		
		<h3>
			<a id="section2_3"/>2.3 Mobile Web Rendering Toolkit</a>
		</h3>
		<p>
			The costs and complexity associated with native mobile application
			development, especially across multiple mobile platforms, can be very
			high. There is an emerging trend to extend the well established web
			programming model as an alternative strategy for developing mobile
			applications. To support this strategy mobile web application
			designers are challenged to provide web user interfaces that
			replicate the more native look and feel of the host mobile device.
			For example, imagine a web application that runs on the iPhone's
			Safari browser yet looks and behaves similar to a native iPhone
			application. To help simplify the development of this type of mobile
			web application we plan to develop an extensible web rendering
			toolkit that provides device-specific widget rendering and behaviors.
		</p>
		
		<p>
			The toolkit will be composed of a core tier and an API programming
			tier. The core tier will implement the device-specific widget
			presentation and interaction customizations using HTML, CSS and
			JavaScript technologies. The API tier will provide a
			device-independent web programming API. Both JavaScript and JSP
			versions of the API programming tier are planned. We will solicit
			community contributions of additional implementations of the API
			using alternative programming models such as PHP and Java Server
			Faces.
		</p>
		
		<h3>
			<a id="section2_4"/>2.4 Mobile Device Services Access Framework</a>
		</h3>
		<p> 
			Rich internet application (RIA) technologies are rapidly improving 
			and will ultimately introduce a new wave of innovative mobile web 
			applications that marry the best of the web with device-resident 
			information and device services. The Mobile Device Services Access 
			Framework will provide a technology bridge to enable mobile web 
			applications access to key device services. Example services 
			available on many mobile devices include global position measurement, 
			accelerometer sensors, camera instrumentation and vibration 
			generation. Common APIs and web components for interacting with 
			primary services will be provided along with a device-side pluggable 
			driver model. An example iPhone implementation and possibly a 
			G1-Android implementation of the framework will be provided to 
			support framework testing and to serve as a guide for web application 
			developers. Additional contributions for other devices will be 
			accepted. 
		</p>

		<h3>
			<a id="section2_5"/>2.5 Mobile Device Web Application (Short-cut) Installer Framework</a>
		</h3>
		<p> 
			The line where the desktop stops and the web starts is becoming 
			increasingly blurred as rich internet application technologies 
			mature. Many mobile internet devices will support the creation of 
			application short-cuts that will launch the device's web browser with 
			a target web application URL and optional application branding and configuration 
			parameters. The developer kit will provide a common API and framework 
			for the creation of custom device-side web browser application 
			short-cuts. Device-specific implementations will be supported by the 
			framework using the Eclipse plugin pattern. An example iPhone 
			implementation and possibly a G1-Android implementation is 
			planned to support testing of the framework and to demonstrate how 
			other device specific installers may be developed. 
		</p>
		
		<h3>
			<a id="section2_6"/>2.6 Educational Resources</a>
		</h3>
		<p>
			Educational resources and community development are vital for the
			success of this project. We plan to develop a series of how-to
			articles, tutorials, best practice guidelines and present webinars
			for the FireFly community. Additionally we plan to provide
			architecture, design and API documentation for developers wishing to
			extending any of the FireFly frameworks.
		</p>
		
		<h2>
			<a id="section_3"/>3. Out of Scope</a>
		</h2>
		<p> 
			The FireFly DevKit objectives are to provide a common solution base 
			for the creation of key mobile web developer tools, common web UI 
			rendering frameworks for mobile web devices and bridging 
			technology to enable rich internet applications access to device-side 
			services. Scope limitations of this project include the following: 
		</p>
		<ul>
			<li>
				<p>
				IDE Components vs an IDE - This project will provide some of the key
				enabling mobile web technologies and development tools on the
				Eclipse platform. These modular components may be combined with
				other Eclipse-based projects and products to produce a complete
				mobile web end-user IDE product. Yet there are no plans to expand
				the project scope to the level of a standalone mobile web
				application IDE.
				</p>
			</li>
		</ul>
		<ul>
			<li>
				<p>
				Limited Server-side Concerns - While this project is open to
				supporting the development of server-side UI technologies such as
				PHP and JSP custom tag libraries as part of its mobile web UI
				toolkit concentration, there are no plans to address server-side
				hosting concerns such as JEE web application packaging and
				application server deployment.
				</p>
			</li>
			<li>
				<p> 
				No Project Model Support Requirement - There is no developer tool 
				project structure requirement in scope for this project. Therefore 
				the technologies and frameworks of this project will be developed as 
				independent and agnostic to any specific IDE project 
				structure as much as possible. The FireFly project will provide 
				guidance and support to other Eclipse projects that seek to reuse 
				the work products of this project. 
				</p>
			</li>
			<li>
				<p> 
					No Native Device Application Development Tools - This project will 
					concentrate on supporting web-centric applications. No native 
					application development efforts are planned beyond those required 
					to support the service access and web short-cut installation 
					frameworks. 
					<br>
				</p>
			</li>
		</ul>
		
		<h2>
			4. Organization
		</h2>
		<p>
			The FireFly DevKit Project is proposed as an Eclipse subproject under
			the
			<a href=http://www.eclipse.org/dsdp / id=p-s_
				title="Device Software Development Platform">Device Software
				Development Platform</a> (DSDP) due to the expertise and mobile device 
			industry support that shares an interest and desire for the success of 
			this project. The project leadership plans to join the Mobile Working 
			Group and to work closely with its participants. 
			Genuitec, as the submitter of this proposal, welcomes interested 
			parties to use the FireFly DevKit newsgroup to discuss this proposal 
			and to request inclusion as an interested party. 
		</p>
		
		<p>
			The proposed project members include:
		</p>
		
		<h3>
			4.1 Initial Committers
		</h3>
		<ul>
			<li>
				Wayne Parrott, Genuitec - project lead
			</li>
			<li>
				Todd Williams, Genuitec - evangelism and development
			</li>
			<li>
				Brian Fernandes, Genuitec - development
			</li>
			<li>
				Eugene Ostroukhov, Genuitec - development
			</li>
			<li>
				Greg Amerson, Genuitec - development &amp; educational resources
			</li>
			<li>
				Jens Eckels, Genuitec - evangelism &amp; marketing
			</li>
			<li>
				Pete Carapetyan, Genuitec - development &amp; educational resources
			</li>
			<li>
				Riyad Kalla - Genuitec - educational resources
			</li>
			<li>
				Rosario Aguilar, Genuitec - web design
			</li>
		</ul>
		
		<h3>
			4.2 Interested Parties
		</h3>
		<ul>
			<li>
					Motorola
			</li>
			<li>
					ICEsoft Technologies, Inc.
			</li>
		</ul>
		
		
		<h3>
			4.3 Project Mentors
		</h3>
		<ul>
			<li>
				Doug Gaff, DSDP PMC Lead
			</li>
			<li>
				Gunnar Wagenknecht
			</li>
		</ul>
		
		<h2>
			5. Relationship to other Eclipse Entities
		</h2>
		<p>
			This project is expected to provide key technologies, frameworks and
			tools that may be incorporated by other Eclipse projects. A high
			project priority will be collaboration with the DSDP leadership, the
			Mobile Working Group and DSDP subprojects that have overlapping or
			complimentary interests.
		</p>
		<h2>
			6. Tentative Plan
		</h2>
		<p>
			The initial goal of this project is to provide an incubation release
			with the Eclipse 3.5 platform release.
		</p>

		<h2>
			7. Code Contributions
		</h2>
		<p>
			Genuitec tentatively plans to release the source code to an Eclipse
			Java WebKit browser component, a device preview framework and
			debugger implementation. An exemplary iPhone simulator may also be
			included.
		</p>

		<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>