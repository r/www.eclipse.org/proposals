<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Extended Editing Framework (EEF) proposal";
$pageKeywords	= "eclipse, emf, SWT, Eclipse Forms, properties, properties views, editing, emf editing, view, editor";
$pageAuthor		= "Obeo - Goulwen Le Fur";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Extended Editing Framework (EEF)</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("EEF");
?>

<h2>Introduction</h2>

<p>The Extended Editing Framework component is a proposed open source project under the <a href="http://www.eclipse.org/modeling/emf/">EMF</a> project.
</p>

<p>This proposal is in the Project Pre-Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development 
Process</a> document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the 
Eclipse community. You are invited to comment and/or join the project. Please send all feedback to the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">emft newsgroup</a>.</p>

<h2>Background</h2>

<p>The <a href="http://www.eclipse.org/modeling/">Eclipse Modeling Project</a> focuses on the evolution and promotion of model-based development 
technologies within the Eclipse community by providing a unified set of modeling frameworks, tooling, and standards implementations.</p>
<p>Model based technologies leverage on having a common standard (EMOF/ECORE) to represent their data (models) and the implementation in Eclipse is 
the EMF Framework. There is a great adoption of this technology by the community and there are a lot of tools being built on top of the EMF Framework.
</p>

<p>The need of creating tools (like editors, ...) allowing an easier editing of EMF models (more powerful than the default arborescent editor) 
becomes stronger day after day. The <a href="http://www.eclipse.org/gmf/">GMF</a> Framework already provides an answer to this need in the specific 
case of graphical editors based on the <a href="http://www.eclipse.org/emf">EMF</a> and <a href="http://www.eclipse.org/gef">GEF</a> frameworks. The 

<a href="http://www.eclipse.org/modeling/tmf/">TMF</a> framework also tackles this need by defining a concrete textual syntax for EMF metamodels.</p>

<h2>Scope</h2>

<p>The EEF project will focus on the tooling for the production of enhanced environments dedicated to EMF models editing. This will be obtained by 
three means :</p>
<li>Providing widgets and architecture improving user experience for editing of EMF model.</li>
<li>Providing a way to modelize graphical components and views.</li>
<li>Providing a sample generation for producing a &laquo; ready to use &raquo; set of views conform to the user modelization.</li>

<h2>Project Description</h2>

<h3>Project Goals</h3>

<p>This project aims at giving another way to improve the EMF model creation phase by providing new <b>services dedicated to editing</b>
and using more appealing editing elements. The way to obtain these services and elements is based on a generative approach similar to the 
EMF.Edit one. The framework provides <b>advanced editing components for the properties</b> of EMF elements and a default generation based
 on standard metamodels using these components. The generic generators create a standard architecture with advanced graphical components to 
edit EMF model objects. These components are meant to leverage every aspects of the Eclipse Platform as for instance the Eclipse Dynamic 
Help.</p>

<p>Another goal of the project is to give new transversal <b>editing abilities</b> to the EMF users. An example of these abilities is to 
lock/unlock the edition of given objects. With an extensible mechanism of providers, users will be able to define whether, in a given context, 
an EObject can be modified or not. This context can be the containing resource state (read-only or not), an attribute in the meta-model, ...</p>

<h3>Technical Goals</h3>

<p>This <b>enhancement of the EMF models editing</b> capabilities can be achieved in a generative way like other projects of the Eclipse Modeling 
Project (EMF, GMF, ...). 

<p>The Extended Editing Framework runtime contains a set of advanced widgets and a generic and extensible MVC architecture that eases the edition of EMF 
models. </p>
<p>The Extended Editing Framework also comes with standard metamodels. These allow the definition of models that will parameterize the actual editing 
components that are to be generated. For example, any given EMF element can be related to a variable number of editing views. Another example is the
ability to define with these models which kind of widget will be used to edit the properties of an EMF element.</p>
<p>Last but not least, the framework provides default generators for the editing components. These are organized as an MTL module that generates a 
standard architecture extending the framework's runtime. The result of the generation can be included in standard SWT/JFace elements. The figures 
1 and 2 show an example leveraging the generated result.</p>
<p><a href="PropertiesTab.png" class="image" title="tabbed properties view example"><img alt="tabbed properties view example" src="PropertiesTab.png" 
border="0"/></a></p>
<p><a href="PropertiesWizard.png" class="image" title="Properties Wizard example"><img alt="EEF generation architecture" src="PropertiesWizard.png" 
border="0"/></a></p>
<p>This project will grow by including more and more editing components along with their associated generators may they be contributed or developed by
the Extended Editing Framework team. This policy aims at covering more and more use cases for applications based on EMF models. As an EMFT sub-project, the
Extended Edit Framework also leverages other technologies like EMF Validation to improve the user experience on applications using this project and to
smooth the integration with existing EMF-based softwares.</p>

The figure 3 show the links between the three components of the EEF project.

<p><a href="achi-eef.png" class="image" title="Generative approach to define advanced EMF edition components"><img alt="EEF generation architecture" 
src="achi-eef.png" border="0"/></a></p>
<p>An important project which this framework could be interfaced with is the EMF Databinding project. This project aims at providing an EObject's 
observable architecture which can be reused by the controllers of this project.</p>
<p>The initial code for this project will be based on code contributions from Obeo. This contribution has been successfully used in some business use 
cases (P&ocirc;le Emploi MDA tooling, ...) as well as the tabbed properties views of Eclipse Papyrus, the UML graphical editor of the MDT project.</p>

<h2>Organization</h2>

<p>We propose that this project be undertaken as part of an Eclipse Modeling Framework Technology sub-project.</p>

<h3>Initial Committers</h3>

<ul>
<li>Goulwen Le Fur (Obeo) - proposed project lead</li>
<li>St&eacute;phane Bouchet (Obeo)</li>
<li>Nathalie L&eacute;pine (Obeo)</li>

<li>Patrick Tessier (CEA)</li>
</ul>

<h3>Initial Contributors</h3>

<ul>
<li>Goulwen Le Fur (Obeo) - current version of the Extended Editing Framework</li>
</ul>

<h3>Mentors</h3>

<ul>
<li>Ed Merks</li>

<li>C&eacute;dric Brun</li>
</ul>

<h3>Interested Parties</h3>

<p>The following organizations have expressed interest in the project :</p>

<ul>
<li>Obeo</li>
<li>P&ocirc;le Emploi (Former Unedic) - The first user of the project</li>
<li>CEA LIST</li>

<li>Ed Merks</li>
<li>Richard Gronback</li>
<li>Itemis</li>
<li>Oracle</li>
<li>Queensland University of Technology</li>
<li>Soyatec</li>
<li>Olivier Mo&iuml;ses</li>
<li>Thales ATMS</li>
<li>CDO Team</li>
</ul>

<h3>Participation</h3>
<p>Critical to the success of this project is participation of interested third parties to the community. We intend to reach out to this community and 
enlist the support of those interested in making a success of the Extended Editing Framework project. We ask interested parties to contact 
<a href="news://news.eclipse.org/eclipse.technology.emft">news://news.eclipse.org/eclipse.technology.emft</a> to express interest in contributing to 
the project.</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
