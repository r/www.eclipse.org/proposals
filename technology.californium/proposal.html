<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Californium (Cf) CoAP Framework</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<p>The Californium (Cf) CoAP framework project is a proposed open source project
under the <a href="http://projects.eclipse.org/projects/technology">Eclipse Technology Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<h2>Background</h2>

<p>In July 2013, the Internet Engineering Task Force (IETF) accepted a new Web
protocol for machine-to-machine (M2M) communication as Proposed Standard: the
Constrained Application Protocol (CoAP). It is designed within the Constrained
RESTful Environments (CoRE) working group. Implementing the REST architectural
style, it can be transparently mapped to HTTP. However, CoAP also provides
features that go beyond HTTP such as native push notifications and group
communication.</p>

<p>This makes CoAP an excellent choice for M2M applications. Thus, it is part of
several M2M standards such as OneM2M and OMA Lightweight M2M. The Eclipse M2M
Industry Working Group already encompasses several framework and protocol
projects, but is still missing an implementation of CoAP, which currently
gains more and more attention.</p>

<h2>Scope</h2>

Californium (Cf) provides an implementation of the Constrained Application
Protocol (CoAP) standard. The scope of this project can be summarized as follows:

<ul>
	<li>Maintain the base implementation of CoAP by providing bug-fixes,
		optimizations, and necessary documentation.</li>
	<li>Continue implementing complementary specifications by the IETF CoRE
		working group.</li>
	<li>Maintain the included Web resource framework for M2M and Internet of
		Things applications.</li>
	<li>Provide seamless integration with M2M projects for OMA Lightweight M2M
		and OneM2M such as the <a
		href="http://eclipse.org/proposals/technology.om2m/">OM2M proposal</a>.
		</li>
	<li>Implement a security API that allows for authentication and
		authorization as defined by IETF, OMA, and OneM2M activities.
		Californium (Cf) comes with the <a
		href="https://github.com/mkovatsc/Scandium">Scandium (Sc)</a> project
		as DTLS implementation.</li>
	<li>Maintain the <a href="https://github.com/mkovatsc/Actinium">Actinium
		(Ac)</a> App-server for Californium with its server-side JavaScript
		support for lightweight M2M and Internet of Things applications.</li>
	<li>Extend Android support of the CoAP base implementation.</li>
</ul>

<h2>Description</h2>

<p>Californium (Cf) is an open source implementation of the Constrained
Application Protocol (CoAP). It is written in Java and targets unconstrained
environments such as back-end service infrastructures (e.g., proxies, resource
directories, or management services) and less constrained environments such as
embedded devices running Linux (e.g., smart home controllers or vehicle
sensors). Californium (Cf) has been running code for the IETF standardization of
CoAP and was recently reimplemented to straighten changed design decisions, but
also to improve its performance with focus on scalability. The new
implementation was successfully tested at the ETSI CoAP#3 and OMA LWM2M
Plugtests in November 2013.</p>

<p>The CoAP framework already provides the following features:</p>

<ul>
	<li>Implementation of the Proposed Standard of CoAP
		(<a
		href="http://tools.ietf.org/html/draft-ietf-core-coap-18">
		draft-ietf-core-coap-18</a>)</li>
	<li>Implementation of the Observe draft
		(<a
		href="http://tools.ietf.org/html/draft-ietf-core-observe-11">
		draft-ietf-core-observe-11</a>)</li>
	<li>Implementation of the Blockwise Transfers draft
		(<a
		href="http://tools.ietf.org/html/draft-ietf-core-block-14">
		draft-ietf-core-block-14</a>)</li>
	<li>Implementation of the Resource Directory draft
		(<a
		href="http://tools.ietf.org/html/draft-ietf-core-resource-directory-00">
		draft-ietf-core-resource-directory-00</a>)</li>
	<li>CoAP-HTTP cross-proxy support through httpcore-nio and guava</li>
	<li>Web resource framework with an advanced concurrency model for the
		implementation of Internet of Things applications</li>
	<li>Basic DTLS 1.2 support through <a
		href="https://github.com/mkovatsc/Scandium">Scandium (Sc)</a></li>
	<li>Wrapper for OSGi</li>
	<li>Benchmark tool for measuring the performance of CoAP servers</li>
</ul>

<p>Additional work is currently carried out on the following topics:</p>

<ul>
	<li>Group communication through IP multicast and management through
		<a href="http://tools.ietf.org/html/draft-ietf-core-groupcomm-17">
		draft-ietf-core-groupcomm-17</a></li>
	<li>Advanced congestion control</li>
	<li>Upgrade of the Actinium (Ac) JavaScript application server to the
		new version of Californium (Cf)</li>
</ul>
 
<h2>Why Eclipse?</h2> 

The Eclipse foundation can provide a long-term home for the project. With the
M2M Industry Working Group, it can produce more attention to the Californium
(Cf) project to attract more contributors to improve the support. In the other
direction, the M2M Industry Working Group can gain from the CoAP support, which
is currently missing for corresponding M2M projects.

<h2>Initial Contribution</h2>

<p>The Californium (Cf) project will be provided with an initial contribution
from ETH Zurich:</p>

<ul>
	<li>The <a href="https://github.com/mkovatsc/Californium">Californium
		(Cf)</a> CoAP framework in Java</li>
	<li>The <a href="https://github.com/mkovatsc/Scandium">Scandium (Sc)</a>
		DTLS 1.2 implementation</li>
	<li>The <a href="https://github.com/mkovatsc/Actinium">Actinium (Ac)</a>
		application server implementation</li>
	<li>The <a href="https://github.com/mkovatsc/element-connector">element-connector</a>
		module, which allows modularization of the project for different
		platforms and use cases.</li>
</ul>
 
<h2>Legal Issues</h2>

<p>Californium will be dual-licensed under the Eclipse Public License (EPL) 1.0
and the Eclipse Distribution License (EDL).</p>

<p>Matthias Kovatsch and ETH Zurich own the full copyright of the initial
contribution, which was published under 3-clause BSD license.</p>

<p>Californium (Cf) currently includes the following third party libraries
via Maven for the optional HTTP-CoAP cross-proxy support:
<ul>
	<li>Apache HttpCore NIO 4.2.1, ASL 2.0</li>
	<li>Apache HttpClient 4.2.1, ASL 2.0</li>
	<li>Guava, ASL 2.0</li>
</ul>
</p>

<p>The optional Actinium (Ac) extension currently includes the following
third party libraries via Maven:
<ul>
	<li>Mozilla Rhino 1.7R3, MPL 2.0</li>
	<li>E4XUtils 1.0 by Anthony Elder, permission to use and distribute</li>
</ul>
</p>
 
<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

<p>The following individuals are proposed as initial committers to the project:</p>

<dl>
	<dt>Matthias Kovatsch, ETH Zurich</dt>
	<dd>Matthias Kovatsch is the creator and main architect of the Californium
		(Cf), Scandium (Sc), and Actinium (Ac) projects and will be project
		(co-)lead.</dd>
	<dt>Martin Lanter, ETH Zurich</dt>
	<dd>Martin Lanter is the main developer of the initial Californium (Cf)
		and Actinium (Ac) projects and will contribute to the base
		implementations of CoAP.</dd>
	<dt>Julien Vermillard, Sierra Wireless</dt>
	<dd>Julien Vermillard is the author of the Leshan OMA Lightweight M2M server
		implementation. He will contribute LWM2M functionality in this new
		project.
	</dd>
</dl>

<p>We welcome additional committers and contributions.</p>

<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Chris Aniszczyk</li>
	<li>Wayne Beaton</li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li>Sierra Wireless</li>
	<li>Thierry Monteil and Mahdi Ben Alaya, LAAS-CNRS (France)</li>
	<li>Kai Hudalla, Bosch Software Innovations GmbH</li>
</ul>

<h2>Project Scheduling</h2>

The initial contribution is already available on GitHub with automated Travis-CI
builds and a Maven repository.

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>10-December-2013</td>
		<td>Added Martin Lanter as committer and Android under scope</td>
	</tr>
	<tr>
		<td>04-December-2013</td>
		<td>Added Sierra Wireless and LAAS-CNRS as interested party</td>
	</tr>
	<tr>
		<td>29-November-2013</td>
		<td>Document created</td>
	</tr>
</table>
</body>
</html>