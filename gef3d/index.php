<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "GEF3D Proposal";
$pageKeywords	= "GEF, 3D";
$pageAuthor		= "Jens von Pilgrim";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>GEF3D Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("GEF3D");
?>

<!-- *********************************************************************** -->

<h2>Introduction</h2>

<p>The GEF3D project is a proposed open source project under the Eclipse Technology Project.</p>

<p>
According to the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process</a>, this proposal is intended to declare the intent and scope of the GEF3D project as well as to gather input from the Eclipse community. 
You are invited to comment on and/or join the project. Please send all feedback to 
the<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.gef3d"> http://www.eclipse.org/newsportal/thread.php?group=eclipse.gef3d</a> newsgroup.
</p>

<h2>Background</h2>
<p>
Three-dimensional visualizations are often very helpful when two-dimensional representations of complex systems become too cluttered or when domain specific diagrams are already using three-dimensional notations. On the other hand, 3D visualizers are often developed separately from existing development tools or IDEs, such as Eclipse. These special solutions are created using 3D frameworks such as OpenGL, Java3D or VRML/X3D. These frameworks are very different from the ones used for implementing 2D visualizations in the Eclipse environment, such as GEF. Thus, when existing 2D solutions should be extended to 3D, e.g. because the models represented become more and more complex, completely new solutions have to be implemented; switching from 2D to 3D requires completely new programming skills as well as new tools -- eventhough in many cases the models are not that different (or should even be the same) and many graphical properties are quite similar.
</p>


<h2>Project Description </h2>

<p>
GEF3D will provide a framework for implementing 3D graph-based editors as simple as 2D graphical editors and to use 2D editors in a 3D environment.
</p>
<p>
GEF3D extends the Eclipse GEF framework and adds the ability to create 3D visualizations and editors just the way 2D graphical editors are created with GEF. Instead of GEF's 2D package Draw2D, it comes with an OpenGL based version, called Draw3D; for handling 3D related tasks appropriate controller classes extend or replace existing GEF based controller parts.
</p>
<p>
By projecting 2D content on 3D planes, it is possible to reuse existing 2D GEF-based editors and use them in a 3D environment. This way certain 3D features can easily be added without much implementation effort. GEF3D provides necessary techniques and design patterns in order to reduce extra implementation effort for 3D as much as possible.
</p>


<h3>Goal</h3>
<p>
The goal of the GEF3D is to:
<ul>
<li>implement view classes similar to the Draw2D library</li>
<li>implement 3D navigation tool classes such as cameras or 3D picking techniques</li>
<li>provide GEF-like techniques in a 3D environment, such as handles, feedback figues and so on</li>
<li>enable multiple graphical editors (2D/3D) to be displayed in a single 3D scene</li>
<li>enable the seamless combination of 2D and 3D elements</li>
</ul>
</p>

<h3>Scope</h3>
<p>
The project will provide a framework for enabling 3D graph-based editors, that is editors using visualizations based on nodes, edges and nested nodes. These nodes and edges are 3D or 2D shapes, the latter are to be projected on 3D shapes. The shapes are all rendered using OpenGL (using LWJGL), basic shapes will be provided by the framework in order to minimize necessary OpenGL knowledge.
</p>
<p>
Additionally, special navigation and layout techniques will be implemented for achieving good usability,such as tracking shots or detail on demand.
</p>

<h3>Features</h3>
<ul>
<li>Support visualization of graph-like models in a 3D manner</li>
<li>Support basic editing features such as selection, feedback figures and handles and direct editing in 3D</li>
<li>Support basic 3D navigation techniques (cameras)</li>
<li>Support seamless combination of 2D and 3D elements</li>
</ul>

<h4>Perspective and Views</h4>
<ul>
<li>OpenGL canvas displaying 3D figures</li>
<li>Possibly a birds eye view and / or outlines</li>
</ul>

<h4>Extension points</h4>
<ul>
<li>Provide extension points for enabling multiple editors</li>
</ul>

<h2> Project Organization</h2>
<p>We propose this project should be undertaken as an incubator project with the Eclipse Technology Project.
</p>

<h3>Mentors</h3>
<ul>
<li>Chris Aniszczyk</li>
<li>Ed Merks</li>
</ul>

<h3>Initial Committers</h3>
<ul>
<li>Jens von Pilgrim (project lead) - FernUniversitaet in Hagen</li>
<li>Kristian Duske - FernUniversitaet in Hagen</li>
</ul>

<h3>Initial Code Contribution</h3>
<p>The initial code contribution is based on the existing GEF3D code base licensed under the EPL. The current project and its source code can be obtain at <a href="http://gef3d.org">http://gef3d.org</a>
</p>

<h4>Licensing and Intellectual property</h4>
<p>A point of interest is that GEF3D depends on the LJWGL, an OpenGL Java wrapper.
LWJGL itself is licensed under the BSD license. GEF3D contains an LWJGL Eclipse plugin which bundles LWJGL.</p>

<h3>Interested Parties</h3>
<ul>
<li>Ian Bull (GEF, Zest)</li>
<li>St&eacute;phane Lacrampe (Obeo)</li>
<li>Sven Efftinge (itemis)</li>
<li>Yuri Strot (Xored Software)</li>
</ul>

<h3>User community</h3>
<p>
GEF3D is developed as part of a Ph.D. project at the Software Engineering group at the FernUniverstitaet (distance teaching university) in Hagen, Germany (<a href="http://www.fernuni-hagen.de/se/">http://www.fernuni-hagen.de/se/</a>). Several students thesis are using or extending GEF3D. A website, mailing list and bug tracking system are available. 
</p>

<h3>Tentative Development Plan</h3>
<p>
Some functionality already exists, but there is no real plan at the moment for lack of committed resources. The project's rough priorities are:
</p>

<p>
Already done:
<ul>
<li>Basic support for visualization of graph-like models in a 3D manner</li>
<li>Support basic editing features such as selection, feedback figures and handles</li>
<li>Support basic 3D navigation techniques (cameras)</li>
<li>Support seamless combination of 2D and 3D elements</li>
</ul>
</p>
<p>
Future:
<ul>
<li>Known bugs are probably fixed in the near future, see GEF3D's bugzilla for details</li>
<li>New features will be added depending on user's needs</li>
<li>Possibly a birds eye view and / or outlines</li>
<li>Extension points</li>
<li>Port existing GEF-based editors, such as the UML tools or the ecore editor, to 3D. These editors will probably be provided as examples.</li>
</ul>
</p>

<h2>Relation to other Eclipse projects and other open source projects</h2>

<ul>
<li>GEF3D is an extension of <a href="http://eclipes.org/gef">GEF</a>, the Eclipse Graphical Editing Framework</li>
<li><a href="http://lwjgl.org">LWJGL</a>, the Lightweight Java Game Library is used as an OpenGL wrapper</li>
</ul>

<!-- *********************************************************************** -->


      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
