<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
	
		
			<h1>Device Software Development Platform</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Device Software Development Platform Top-Level");
?>

<P>This project proposal is in the <A HREF="/org/documents/Eclipse Development Process 2003_11_09 FINAL.pdf">Proposal
  Phase</A> and is posted here to solicit community feedback,
  additional project participation, and ways the project can be
  leveraged from the Eclipse membership-at-large. You are invited to
  comment on and/or join the project. Please send all feedback to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.dsdp">eclipse.dsdp newsgroup</a>.</P>
	
		<h2>Project
			Organization</h2>
	
	
		
			<P> The <em>Device Software Development Platform </em>is a proposed open source Top Level Project of eclipse.org.
            <a href="charter.html"> The Charter</a> describes the organization of the project, roles and responsibilities of the participants, and top-level development process for the project. </P>	  
		
	


	
		<h2>Overview</h2>
	
	
		
			<P>In the Enterprise , applications are typically developed and executed on the same host platform, typically Windows, Linux, or a Unix variant. These applications may also be run on a virtual machine or a web browser. Device software development is vastly more complicated for a number of reasons. First, applications must be cross-compiled and deployed on a target or �embedded system,� which is usually based on a different hardware configuration and operating system. Because of this, device software development heavily relies on a stable custom hardware and software platform and is therefore closely tied with hardware bring-up and the configuration of real-time operating systems. </P>
	        <p>Second, the device or target system is often constrained by processor type, speed and available memory and must respond to external inputs within a guaranteed period of time, typically 10 ms or less. Third, depending on the industry and type of application, devices may have many peripherals, may be headless (without display), may run none or multiple operating systems, and may have one or many connection interfaces. The sweet-spot for device software applications are medical devices (blood-test machines, EKG's), network equipment (routers, switches), consumer electronics (digital cameras, mobile phones), automotive applications (car infotainment, engine controllers), military applications (cruise missiles, combat systems) and industrial devices (manufacturing robots, process instrumentation). </p>
	


	
		<h2>Project
			Principles</h2>
	
	
		
			<P> Among the key principles on which this project has been initiated, and will be run, are the following: <br>
              <br>
              <strong>Leverage Eclipse Ecosystem </strong> - A major goal of this project is to apply the application development strengths of the Eclipse Project to the device software development domain. The project will work closely with other Eclipse project areas whenever possible to leverage the capabilities being developed in those areas. <br>
              <br>
              <strong>Vendor Neutral </strong> - We aim to encourage Eclipse participation and drive Eclipse market acceptance by providing vendor-neutral capabilities and to encourage participation for complementary capabilities through additional projects. <br>
              <br>
              <strong>Open, Standards-based Development </strong> - Where market adopted standards exist that meet the design goals, our aim is to leverage and adhere to them. Where market adopted standards do not exist, we will develop and publish any new capabilities in the Eclipse open forum. <br>
              <br>
              <strong>Incremental Delivery </strong> - In order to meet the pent-up demand for a standardized framework for device software development tools, we aim to deliver functionality to the market as rapidly as possible via an incremental delivery model. </P>	  
	


	
	
		<h2>Project
			Scope </h2>
	
	
		
			<P>Device software development typically involves three distinct phases. </P>
	        <ul>
	          <LI><strong>Hardware Bring-up </strong> � In this phase developers test prototype hardware by verifying correct processor reset and functionality of target memory and peripherals. They also run hardware-oriented diagnostics and confirm the ability to run simple software applications. They provide Platform developers with a stable target. </LI>
	          <LI><strong>Platform Software Development </strong> � In this phase developers create the software foundation that sits closest to the hardware including device drivers and board support packages (BSP's). They also configure the target operating system and services providing connectivity, management and security. They provide Application developers with a stable platform. </LI>
	          <LI><strong>Application Software Development </strong> � In this phase developers create the software that delivers the purpose and value of the device. They build, download and debug applications running on a target system utilizing an array of tools. </LI>
          </ul>	        <p>The Device Software Development Platform (DSDP) will provide frameworks and exemplary tools to support activities in each of these phases. </p>
	        <p>The needs and requirements for&nbsp;the Application Software&nbsp;Development step above can often be viewed as a superset of the requirements for Enterprise software development. We envision that the Device Software Development Platform will provide a home for embedded extensions across a wide range of existing and future Eclipse projects. Projects like CDT, JDT and TPTP provide general-purpose functionality that appeals to a large audience, whereas DSDP will create complimentary extensions, thereby increasing the usefulness and applicability of these projects for embedded development. For example, DSDP relies on CDT for generic IDE capabilities for C/C++, but by extending CDT with Target Management and Device Debugging capabilities, device software developers will find much greater value in the overall tools platform for their applications. </p>
	        <p>The DSDP project will also act as an advisor and resource provider for&nbsp;other Eclipse projects that wish to become more applicable for embedded development. For example, the DSDP project relies on the build system provided by CDT and can help drive requirements and contribute code to support multiple tool chains for different processor variants. We&nbsp;envision that in addition to providing code and infrastructure to Eclipse, part of our role&nbsp;is to ensure a coherent feature set across all top-level projects. </p>
	        <p>Finally, the DSDP project will focus exclusively on tools for device software development, including embedded rich client applications. It will not focus on additions to operating systems </p>	        
  
	
		
	


	
	
		<h2>Interested Parties</h2>
	
	
		
		  <P>	          <a href="http://www.windriver.com">Wind River Systems</a>, as the submitter of this proposal, welcomes interested parties to post to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.dsdp">eclipse.dsdp newsgroup</a> and ask to be added to the list as interested parties or to suggest changes to this document. The
          proposed PMC members include:
		  <P>		    <B>Project Management Committee </B>		    <UL>
              <LI>Doug Gaff, Wind River Systems (proposed PMC lead)</LI>
              <LI>Martin Klaus, Wind River Systems</LI>
			  <LI>Adam Abramski, Wind River Systems</LI>
			  <LI>Michael Scharf, Wind River Systems</LI>
			  <LI>Rudi Frauenschuh, Wind River Systems</LI>
			</UL>
<p>A number of companies that have expressed interest in the project and/or its components thus far. This list will be updated periodically to reflect the growing interest in the Device Software Development Platform.</p>
            <B>Interested Companies</B>
            <UL>
              <LI><a href="http://www.windriver.com">Wind River Systems</a> </LI>
<LI><a href="http://www.acceleratedtechnology.com/">Accelerated Technology</a> </LI>
              <LI><a href="http://www.ibm.com">IBM</a></LI>
              <LI><a href="http://www.intel.com">Intel</a></LI>
              <LI><a href="http://www.mvista.com/">MontaVista</a></LI>
              <LI><a href="http://www.ti.com/">Texas Instruments</a></LI>
              <LI><a href="http://www.timesys.com/">Timesys</a></LI>
              <LI><a href="http://www.qnx.com/">QNX</a></LI>
           </UL>
      
  
	
		
			<P><BR>
			</P>
		
	


	
	
		<h2>Projects
			</h2>
	
	
		
			<P>We see a need for projects to support the device software phases outlined above in the following areas. Please note that this list is far from complete since device software development is a vast domain driven by advancements in both software and hardware design. </P>
			<UL>
			  <LI> Target Management </LI> 
			  <LI> Device Debugging </LI> 
			  <LI> Hardware bring-up </LI> 
			  <LI> Silicon vendor tool chain support </LI> 
			  <LI> FPGA and DSP programming </LI> 
			  <LI> Simulation and emulation tools </LI> 
			  <LI> Operating system and middleware configuration </LI> 
			  <LI> Electronic Design Automation (EDA) </LI> 
		  </UL>		  <p>Initially, we'll focus on Target Management and Device Debugging. We encourage and intend to recruit participation from other vendors and developers to lead projects that will focus on areas mentioned above and any other related technologies. </p>
			<P><strong>Target Management </strong>
			</P>
			<P>A lot of software is developed that needs to be run on a remote system. Developers&nbsp;have to connect to these systems to download software and data, start and stop programs, debug programs and more. The goal of the Target Management project is to provide data models and frameworks to manage remote systems, their connections and their services. Although Target Management is a fundamental requirement in the embedded world, it could also be used in the Enterprise for remote development to manage connections between clients and servers to build and debug applications remotely. </P>
			<p>There are many different processors, boards, operating systems, connection protocols (e.g. TCP/IP, serial, JTAG, proprietary) and services (console, query, run control, ping, download, events, debug) used. Often many of these interfaces are present at the same time, so developers need to manage the different devices and their configurations. The Target Management project will provide plug-ins to simplify configuration and management of a wide range of connection mechanisms. </p>
			<p>A first attempt for a �Remote System (Target) Definition� has been made in the CDT. The proposal discusses a framework that&nbsp;provides an abstraction&nbsp;of connections and&nbsp;services on a remote target. Such an abstraction&nbsp;will allow for the creation of tools that unify the access to remote targets and simplify the creation of&nbsp;launch configurations. For details, see Bugzilla entry: <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=65471">Bug 65471</a> </p>
			<p>We propose to extend this work in the following steps: </p>
			<ul>
			  <LI> Extensions and additional data models for connection protocols, services, devices and whatever needs to be abstracted </LI>
			  <LI> Frameworks to enable interaction with remote devices like: connect, query, download, start, stop, debug, etc. </LI>
			  <LI> Abstractions and a framework for functionality needed to launch programs including sequencing, parameter passing, conditions, timing, etc. </LI>
			  <LI> Capabilities to manage numerous shared devices over the network </LI>
		  </ul>			<p>The target manager plugin created in this project will provide general-purpose capabilities to manage connection configurations, monitor status, transfer data, and launch and debug programs on remote devices based on gdb. Vendors can extend the target manager with connection mechanisms specific to their environment and operating system. </p>
			<p><strong>Device Debugging </strong><br>
                <br>
  Debugging software running on embedded devices or a remote system can be quite different from debugging host applications. First, the hardware model is usually different, with embedded devices often employing multiple processors in multi-core and/or processor + DSP configurations. Second, the memory models are more complex and varied. Memory is usually very limited, often shared between multiple cores, often of variable width, and often used in multiple MMU configurations depending on the target operating system and the device configuration. Third, debugging device software requires a deeper visibility into the target, including access to on-chip peripherals, visibility into on-chip and off-chip processor cache, ability to program target flash, ability to trace instruction flow using on-chip trace buffers, and ability to perform low-level target hardware diagnostics. Fourth, embedded devices often have specialized means for debug access that usually require additional hardware and/or target software. This debug access can be slower than typical host application debugging and can therefore put a premium on overall debugger performance and the amount of interaction the debugger has with the target. Many of these requirements cannot be well covered today using the Platform debug API's and views or the CDI interface in CDT. <br>
  <br>
  This project will focus on addressing the specific needs of device debugging and include the following activities: </p>
			<ul>
			  <LI>Collect detailed requirements for Device Software Debugging </LI>
			  <LI> Discuss and propose API and extension point changes and modifications </LI>
			  <LI> Improve existing debugger views and develop new views </LI>
			  <LI> Improve existing connections to debug engines like gdb </LI>
		  </ul>			<p>The overriding goal of the Device Debugging Project is to create a framework that enables other Eclipse plug-ins to communicate with and control a device through a common API, regardless of the underlying target connection. The API needs to be powerful enough to support debugging engines for conventional processors, DSPs, network and I/O processors. <br>
              <br>
  The focal point for this effort will be changes and extensions of the platform debug model that is the core API between debug engines and debug views today. The goal is to make the platform debug model work also for device debugging and to create API's and views that work well with the many different capabilities of current and future debug engines. These different capabilities need also be reflected in the view implementations, e.g. through the usage of well chosen extension points. </p>
			<p>However, it may turn out that this is not possible and that a new API for device debugging is needed. The success of this project will depend on the amount of help and support received from the team currently working on the platform debug API's.<BR>
 </p>
        
  
	
		
			<P><BR>
			</P>
		
	

<P><BR><BR>
</P>
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

