<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">


<h1>Eclipse Modeling Framework Technology</h1><br>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Modeling Framework Technology");
?>
</p>

<h2>Introduction</h2>

<p>The Eclipse Modeling Framework Technology Project is a proposed open source 
project under the <a href="/technology/">Eclipse 
Technology Project</a>.</p>
<p>This proposal is in the Project Proposal Phase (as defined in the
<a href="/projects/dev_process/">
Eclipse Development Process document</a>) and is written to declare its intent 
and scope. This proposal is written to solicit additional participation and 
input from the Eclipse community. You are invited to comment on and/or join the 
project. Please send all feedback to the

<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.emf">
http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.emf</a> EMF newsgroup.&nbsp;

<h2>Background</h2>
<p align="left">
The <a href="/emf/">Eclipse Modeling Framework</a> project has attracted much interest in the past year 
and we'd like to make it easier for relatively small groups to develop their general purpose EMF models and EMF utilities at Eclipse.   

<h2>Description</h2>
<p align="left">
We've previously set up <a href="/emf/models/models.xml">EMF Corner</a> to help folks get more involved,
but we've progressed to the stage where other groups would like to contribute their technology to Eclipse 
and further develop it at Eclipse.  
Such new contributions need a staging area where they can begin to grow 
and be nurtured with feedback and participation from the Eclipse community.  
To foster such ease of participation, we'd like to propose a new project called Eclipse Modeling Framework Technology, EMFT, 
as part of the Technology PMC, to act as an EMF subproject for incubation,
i.e., for new components that provide general purpose EMF-based models and general EMF-based utilities.  
This will make the work available for the community to use and will allow the strong ideas to grow stronger 
(and the weak ideas to die off). 
</p>

<p align="left">
This project is intended not to overlap in scope with other projects.    
Specifically, it does not extend in scope beyond the current scope of the EMF project itself.
As with the EMF project, 
anything covered by the scope of another project, 
such has GMF, MDDI, or WTP, will be excluded from consideration.  
The scope is intended to include only the contribution of general purpose EMF-based models and general purpose EMF-based utilities 
that might be used across such other projects, 
not to overlap with the goals of those other projects.  
As such, contribution of any model for which the general purpose nature is not obvious is subject to PMC approval.
</p>
<p align="left">
EMF is being used to share metadata across a range of application life cycle tools, 
e.g., modeling, testing, web services, data, and so on.  
As such, the EMFT project can be used for initial trials of addressing cross life cycle data integration issues 
not being specifically addressed by other Eclipse projects.
</p>
<p align="left">
     <h2 align="left">Proposed incubation areas</h3>
The tentative project plan intends to address the following areas of work in which we have had proposals or interest from the community.
    <ul>
      <li>
      <p align="left">CDO - a relational persistance framework for EMF </li>

      <li>
      <p align="left">EODM (model for RDF/OWL) </li> 

      <li>
      <p align="left">Utilities for indexing, searching, querying, and validating EMF models</li>

      <li>
      <p align="left">Support for declarative constraints (Object Contraint Language). </li>
    </ul>

<h2>Organization</h2>

<p align="left">
We propose this project should be undertaken as a Technology project rather than as part of the EMF project. 
Being a Technology project gives it room to experiment without disruption to other EMF development work
and allows for prototyping changes to EMF that would not be considered appropriate in the core 
until detailed exploration proves the value.<br>
<br>
The life cycle of this project is somewhat similar to the Equinox project.  
That is, once subcomponent work matures, it may be incorporated into the EMF project 
to provide a common open source substrate that industrial/commercial users could use to build solutions (like XSD),
it may become an independent subproject (like UML2),
or it may become part of some other project (like the JEM model that's part of VE).
Since this project is intended to include a diverse and growing set of general purpose models and utilities,
the project itself will likely continue long term as a Technology subproject 
even as its subcomponents mature and move on.
</p>
<p align="left">
In general, it is expected that models will eventually evolve to become projects of their own 
or to become part of a larger project, rather than to become part of EMF,
and it is expected that utitilties will evolve to become part of EMF.
Components of EMFT are expected to follow the same planning process as would be expected of subprojects of a top-level project.
Clearly such a change in status will be subject to approval by the target PMC.
</p>

    <h2 align="left">Proposed project lead and initial committers</h3>
    <ul>

      <li>
      <p align="left">Ed Merks, IBM</li>

      <li>
      <p align="left">Other current EMF Committers who will provide project support</li>

      <li>
      <p align="left">Eike Stepper, <a href="http://www.sympedia.org/cdo/">CDO</a></li>

      <li>
      <p align="left">committers from contributed components</li>

    </ul>
    <h2 align="left">Interested parties</h3>
    <p align="left">The following projects have expressed interest in this project. Key contacts listed.<ul>
      <li>
      <p align="left">IBM has proposed contributions to GMF, 
      and some of those contributions are more generally applicable than just to GMF,
      so those will likely be best contributed to EMFT.  Daniel Leroux, IBM, and Richard Gronback, Borland
      </li>
    </ul>

      <h2 align="left">Code Contributions</h3>
      <p align="left"><a href="http://www.sympedia.org/cdo/">CDO</a> for persistence, 
      EODM (model for RDF/OWL) for ontology, 
      utilities for indexing, searching, querying, and validating,
      support for declarative constraints (Object Contraint Language),
      Emfatic (a Java-style textual representation of Ecore),
      and more...<br>

<h2>Tentative Plan</h2>
<p>
There is currently a group working on EMF models for RDF/OWL called EODM that would like to contribute their work, 
and also a project called <a href="http://www.sympedia.org/cdo/">CDO</a> 
that supports relational database persistence for EMF instance data.  
We expect that future contributions would include things like support for OCL and query/search support.

The EMF team, with its years of Eclipse experience can help to manage this project 
and help to set up the build and web infrastructure to get the Eclipse newbies up and running quickly.
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

