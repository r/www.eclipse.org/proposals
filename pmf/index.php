<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "PMF Component Proposal";
$pageKeywords	= "PMF, GUI";
$pageAuthor		= "Yves YANG";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Presentation Modeling Framework</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("PMF");
?>

<h2>Introduction</h2>

<p>PMF (Presentation Modeling Framework) is a proposed new open source project under <a href="http://www.eclipse.org/modeling/">Eclipse Modeling</a>  to provide a metamodel for UI modeling.</p>

<p>This component is in the Pre-Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse 
Development Process</a>) and this document is written to declare its intent and scope. This proposal is written to solicit additional participation and 
input from the Eclipse community. You are invited to comment on and/or join in the development of the component. Please send all 
feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling">eclipse.modeling</a> newsgroup.</p>

<h2>Background</h2>

<p> The Graphical User Interface (GUI) is the visible part of the software.  It  really consists of the gate through which business features are made  available and it reflects the application quality. Designing the visual composition and temporal behavior of GUI is an important part of software application programming. Its goal is to enhance the efficiency and ease of use for the underlying logical design. Typically, the user interacts with information by manipulating visual widgets that allow for interactions appropriate to the kind of data they hold.  The widgets of a well-designed interface are selected to support the  actions necessary to achieve the goals of the user.</p>
<p>Model-Driven Engineering is a modern, standard and industry software engineering. It refers to a range of development approaches that are based on the use of software modeling as a primary form of expression. Sometimes models are constructed to a  certain level of detail, and then code is written by hand in a separate  step. Sometimes complete models are built including executable actions. Model-driven architecture supports model-driven engineering of software systems. MDA provides a set of guidelines for structuring specifications expressed as models. The MDA approach defines system functionality using a platform-independent model (PIM) using an appropriate domain-specific language (DSL). </p>
<p>Obviously, MDE  in GUI needs an appropriate framework to simplify the presentation generation. It is in fact a critical component in  scalable enterprise data presentation applications such as ERP and BPMS in Manufacturing, Supply Chain, Financials, Customer Relationship Management (CRM), Human Resources, Warehouse Management and Decision Support System.</p>
<p>From  users' perspective, the UI presentation modeling should be composed of following steps:</p>
<ul>
  <li>Business modeling</li>
  <li>Importation of the business model and initialization of PMF structure</li>
  <li>High level UI presentation modeling in PMF editor</li>
  <li>Model transformation from PMF model to  displaying technology  execution code</li>
</ul>
<p><img src="architect.png" /></p>
<p>PMF focuses in fact on high level presentation modeling concepts on GUI by ignoring the displaying technology artifacts such as appearence, layouts and data binding support. It allows each technology to make its specific connection with this framework.</p>
<h2>Scope</h2>
<p>The purpose of this framework is to <span lang="EN-US" xml:lang="EN-US">provide the basic   functional concepts of user interaction in a PIM level UI  modeling language.&nbsp;The language can be extended in two ways:</span></p>
<ul>
  <li><span lang="EN-US" xml:lang="EN-US">Higher level   patterns can be defined using PMF itself and used as language   extensions</span></li>
  <li><span lang="EN-US" xml:lang="EN-US">Transformations/Generators   from the PIM level model to specific UI technology platforms</span></li>
</ul>
<p>Each displaying technology such as XUL, e4, JSF, SWT, Swing, GWT, Ajax, Silverlight or others, can extend this framework to implement a specific generator for producing  executable codes in Java or in declarative language.</p>
<p>Precisely, PMF provides following components:</p>
<ul>

  <li><span lang="EN-US" xml:lang="EN-US">An   EMF-based foundation that covers the basic functional concepts of user   interaction.</span></li>
  <li><span lang="EN-US" xml:lang="EN-US">Examples of higher   level patterns&nbsp; like Master/Detail, Finder and Selector defined in and using the   basic PMF concepts</span></li>
  <li>Template engines' integration for code generation</li>
  <li>Two integrations  as demonstration samples are proposed (integrations with other UI solution will be explored as the project evolves):
    <ul>
      <li>JFace UI platform</li>
      <li>e4's declarative UI and modeled workbench</li>
    </ul>
  </li>
  </ul>

<p>This framework is not a  specific UI framework  for a  displaying technology, neither a specific UI Markup language. It is a high level  modeling framework to design the data presentation scenario/pattern and user interactions.  </p>
<h2>Relationship with Other Eclipse Projects/Components</h2>
<p>PMF will use the most Eclipse tools to archive the UI Presentation modeling and code generation. The most important tool is the modeling transformation engine. Several engines are used in diffferent Eclipse projects:</p>
<ul>
  <li><a href="http://www.eclipse.org/emft/projects/jet">JET</a>, a  derivative of JavaServer Pages (JSP) initially developed as part of the  Eclipse Modeling Framework (EMF), and currently an incubator project <a href="http://www.eclipse.org/modeling/m2t/?project=jet">JET2</a> in  the Eclipse Modeling Project.</li>
  <li><a href="http://wiki.eclipse.org/Xpand">Xpand</a> is a statically-typed template language with a lot of advanced features</li>
</ul>
<p>Obvious integrations between PMF and other Eclipse components (EcoreTools,
UMLTools, M2T, MTL, EMF, EMFT, VE, etc.) will be explored as the component
evolves.</p>
<p><a href="http://wiki.eclipse.org/E4/">e4</a> has planed to develop a Declarative UI framework and modelled workbench based EMF. They are the perfect concrete UI solutions for testing and demonstrating the concepts of PMF.</p>
<h2>Organization</h2>

<h3> Mentors </h3>
<ul>
<li><a href="mailto:d.merks@gmail.com">Ed Merks</a></li>
<li><a href="mailto:d_a_carver@yahoo.com">Dave Carver</a></li>
</ul>

<h3>Initial Committers</h3>

The initial committers for this project would be:

<ul>
<li>Yves Yang (<a href="http://www.soyatec.com/">Soyatec</a>), proposed project co-lead</li>
<li>Jim van Dam (<a href="http://www.hipes.nl/">HiPeS</a><a href="http://www.soyatec.com/"></a>), proposed project co-lead</li>
<li>Thomas Guiu (<a href="http://www.soyatec.com/">Soyatec</a>), committer</li>
<li>Olivier Mo&iuml;ses (<a href="http://www.generic-concept.com//">Generic Concept</a>), committer</li>
</ul>

<h3>Code Contributions</h3>

<a href="http://www.soyatec.com">Soyatec</a> and <a href="http://www.hipes.nl/">HiPeS</a>  will offer an initial code contribution. An implementation of the PMF metamodel consisting of Java packages within the org.eclipse.pmf.* namespace will be generated and customized using <a href="http://www.eclipse.org/modeling/emf/">EMF</a>.

<h3>Interested Parties</h3>

<p>Thus far, interest in this component has been expressed by:</p>
<ul>
  <li><a href="http://www.embarcadero.com">Embarcadero Technologies</a> </li>
  <li><a href="http://www.soyatec.com/">Soyatec</a> </li>
  <li><a href="http://www.hipes.nl/">HiPeS</a> </li>
  <li><a href="http://www.lyria.com/">Lyria - group W4</a> </li>
  <li><a href="http://www.ordina.nl/index.asp?LanguageCode=EN">Ordina</a> </li>
  <li><a href="http://www.springsite.com/">Springsite</a> </li>
  <li><a href="http://www.oslo-software.com/">Oslo Software</a> </li>
  <li><a href="http://www.obeo.fr">Obeo</a> </li>
  <li><a href="http://www.itemis.de">itemis AG</a> </li>
  <li><a href="http://www.brane.com">Brane Corporation</a> </li>
  <li><a href="mailto:tom.schindl@BestSolution.at">Tom Schindl</a> </li>
  <li><a href="mailto:lautenbacher@informatik.uni-augsburg.de">Florian Lautenbacher</a> </li>
  <li><a href="mailto:cameron.bateman@oracle.com">Cameron Bateman</a> </li>
  <li><a href="http://www.proxiad.com">ProxiAD</a> </li>
  <li><a href="mailto:Sabri.Skhiri@alcatel-lucent.be">Sabri Skhiri</a> </li>
  </ul>
  
<h3>Developer Community</h3>

The team of initial committers will explore statements of interest from additional developers experienced with UI development and modeling or willing to gain such experience.

<h3>User Community</h3>

It is expected that the user community for this component will consist primarily of developers, given that it is essentially a foundation for building UI modeling tools.

<h2>Tentative Plan</h2>

The development plan of this project would be aligned with e4 plan. The first community technical preview release is scheduled at summar 2009 and the final release is targeted in June 2010.</div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
