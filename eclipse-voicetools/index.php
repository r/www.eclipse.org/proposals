<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">&nbsp;   
    
 <?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Voice Tools Project");
?> 

<h1>Voice Tools Project</h1>
   
    <h2>Introduction</h2>
  

<p>The Voice Tools Project is a proposed open source project under the
Eclipse Technology Project.</p>
      <p>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/">Eclipse 
        Development Process</a> document) and is written to declare its intent 
        and scope. This proposal is written to solicit additional participation 
        and input from the Eclipse community. You are invited to comment on and/or 
        join the project. Please send all feedback to the <a
 href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.voicetools">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.voicetools</a> 
        newsgroup.</p>
      
    Background
  
   
     
<p>The Eclipse open source project has been been exceptionally
successful and established Eclipse as a de facto standard IDE platform
and world class IDE. To date, focus has been on tools for Java
application development. With recent proposals, Eclipse is growing into
the Web application, J2EE, and embedded application spaces.</p>
<p>This project proposal serves to take Eclipse into the voice
application space. These tools can be used to develop interactive voice
response (IVR) systems based on VoiceXML standards, such as
speech-driven applications for performing bank transfers, retrieving
e-mail, or querying flight information over the phone<br>
</p>
    
  


   
    <h2> 
      Description</h2>
  
   
     
      <p>The Voice Tools Technology Project will focus on Voice Application tools in 
        the JSP/J2EE space, based on W3C standards, so that these standards become 
        dominant in voice application development.&nbsp; It will depend on and 
        extend the XML and Web development capabilities of the Eclipse Web Tools 
        Platform Project (<a
 href="/webtools/" target="_top">/webtools/index.html</a>.)&nbsp;&nbsp; 
        Voice Tools will be a set of Eclipse plugins that will provide development 
        tools for W3C Standards/Recommendations for Voice application markup.</p>
<p>Initially, Voice Tools will consist of editors for VoiceXML, the XML
Form of SRGS (Speech Recognition Grammar Specification), and CCXML
(Call Control eXtensible Markup Language).&nbsp; Implementations of
other tools that implement W3C voice standards, such as the LexiconML
(Pronunciation Markup Language), will be added as the standards
solidify and the Voice Tools Eclipse community grows.</p>
      <p>The Voice Tools editors will be extensions of the SSE(Structured Source Editor) 
        from the Web Tools Platform Project, and will have the capability of syntactical 
        validation and content-assistance of the markup tags for each of the W3C 
        standards.&nbsp; The DTD (Document Type Description) for the standard 
        markup will be used to perform these functions, and the user will have 
        the ability to choose the DTD from a preference page associated to each 
        of the markup editors.&nbsp; In addition, a new file wizard will be provided 
        for each markup type, to create an "empty" file based on the required 
        markup defined by the chosen DTD.</p>

  


   
    <h2>Organization</h2>
  
   
     
      <p>We propose this project should be undertaken as a Technology project 
        rather than part of the Eclipse Platform or Web Tools Platform projects. 
        Being a Technology project gives it room to experiment without disruption 
        to other Eclipse Platform or Web Tools Platform development work.</p>
      <p> </p>
      <p>Because Technology projects are not meant to be ongoing, we see this 
        project eventually becoming a project under the Web Tools Platform Project.&nbsp; 
        This would happen once this work matures and the Web Tools Platform Project 
        gets up and running.</p>
      <p><b>Initial Committers</b><br>
        The following companies will contribute committers to get the project 
        started:</p>
      <ul>
        <li>IBM&nbsp; <a href="http://www.ibm.com" target="_blank">http://www.ibm.com</a></li>
        <li>SBC Communications&nbsp; <a href="http://www.sbc.com" target="_blank">http://www.sbc.com</a></li>
        <li>VoiceGenie&nbsp; <a href="http://www.voicegenie.com" target="_blank">http://www.voicegenie.com</a></li>
      </ul>
      <p>Critical to the success of this project is participation of developers 
        in the voice application development community. We hope this proposal 
        phase will result in additional interest and participation from the community.</p>
      <p><br>
      </p>
    
  

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
