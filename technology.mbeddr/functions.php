<? 
/*******************************************************************************
 * Copyright (c) 2007,2014 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation) - initial API and implementation
 *    Wayne Beaton (Eclipse Foundation) - Bug 423377
 *******************************************************************************/

function get_title_from_html_document($file_name) {
	$header = get_header_from_html_document($file_name);

	/*
	 * If we can match a title tag, extract it.
	 */
	if (preg_match('/<title>(.*)<\/title>/i', $header, $title)) {
    	return $title[1];
	}
	
	return "An Eclipse Proposal";
}

function get_header_from_html_document(&$file_name) {
	$handle = @fopen($file_name, "r");
	$content = "";
    while (!feof($handle)) {
        $part = fread($handle, 1024);
        $content .= $part;
        
        /*
         * Only read up to the part that includes the
         * end tag for the header area.
         */
        if (preg_match('/<\/head>/i', $part)) break;
    }
    fclose($handle);
    return $content;
}