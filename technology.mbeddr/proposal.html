<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->
<title>Mbeddr</title>
<style type="text/css">
dt {
	display: list-item;
	list-style-position: outside;
	list-style-image:
		url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
	margin-left: 16px;
}

dd {
	margin-left: 25px;
	margin-bottom: 5px;
}
</style>
</head>
<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
<body>
	<p>
		The mbeddr project is a proposed open source project under the <a
			href="http://www.eclipse.org/projects/project_summary.php?projectid=technology">Technology
			Top-Level Project</a>.
	</p>
	<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
	<p>
		This proposal is in the Project Proposal Phase (as defined in the
		Eclipse Development Process) and is written to declare its intent and
		scope. We solicit additional participation and input from the Eclipse
		community. Please send all feedback to the <a
			href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse
			Proposals</a> Forum.
	</p>
	<h2>Background</h2>
	<p>The amount of software embedded in devices is growing. At the
		same time, embedded software development is challenging. In addition
		to functional requirements, strict operational requirements have to be
		fulfilled as well, including reliability, safety, efficiency or
		real-time constraints. Addressing these challenges requires any of the
		following approaches: abstraction techniques should not lead to
		excessive runtime overhead; programs should be analyzable for faults
		before deployment; and various kinds of annotations, for example for
		describing and type checking physical units, must be integrated into
		the code. Process issues such as requirements traceability and
		consistent documentation have to be addressed, and developers face a
		high degree of variability, since embedded systems are often developed
		in the context of product lines.</p>
	<p>
		Current approaches for embedded software development can roughly be
		distinguished into programming and modeling. The <em>programming</em>
		approach mostly relies on C, sometimes C++ and Ada in rare cases.
		However, because of C's limited support for defining custom
		abstractions, this can lead to software that is hard to understand,
		maintain and extend. Furthermore, C's ability to work with very
		low-level abstractions such as pointers, makes C code very expensive
		to analyze statically. The alternative approach uses <em>modeling</em>
		tools with automatic code generation. The modeling tools provide
		predefined, higher-level abstractions such as state machines or data
		flow block diagrams. Example tools include <a
			href="http://www.etas.com">ASCET-SD</a> or <a
			href="http://www.mathworks.com/products/simulink">Simulink</a>. Using
		higher-level abstractions leads to more concise programs and
		simplified fault detection using static analysis and model checking
		(for example using the Simulink Design Verifier). Increasingly, <em>domain
			specific</em> languages (DSLs) are used for embedded software, and studies
		show that domain-specific languages substantially increase
		productivity in embedded software development. However, most
		real-world systems cannot be described completely and adequately with
		a single modeling tool or DSL, and the integration effort between
		manually written C code and possibly several modeling tools and DSLs
		becomes a significant problem.<br> A promising solution to this
		dilemma lies in a much tighter integration between low-level C code
		and higher-level abstractions specific to embedded software. The
		mbeddr project, described in this proposal, implements this approach.
	</p>
	<p align="center">
		<img width="480" src="figures/ide.png"
			alt="Screenshot of the current mbeddr development environment in JetBrains MPS.">
	</p>
	<p>
		mbeddr supports embedded software development based on an extensible
		version of the C programming language, and a corresponding IDE (see
		image above). The advantages of C can be maintained: existing <em>legacy
			code</em> can be easily integrated, reused, and evolved, and the need for
		<em>efficient code</em> is immediately addressed by relying on C's
		low-level programming concepts. At the same time, domain-specific
		extensions such as state machines, components or data types with
		physical units can be made available as C extensions. This improves <em>productivity</em>
		via more concise programs, it helps improve <em>quality</em> in a
		constructive way by avoiding low-level implementation errors up-front,
		and leads to system implementations that are more amenable to <em>analysis</em>
		-- also supported by mbeddr. By directly embedding the extensions into
		C, the mismatch and integration challenge between domain specific
		models and general purpose code can be removed. mbeddr's
		industry-strength implementation of this approach also includes IDE
		support for C and the extensions: syntax highlighting, code
		completion, error checking, refactoring and debugging.
	</p>
	<h2>Scope</h2>
	<p>The project addresses the development and evolution of mbeddr
		language extensions (standalone DSLs and C extensions), their IDE,
		debuggers, and the integration of formal verification tools. mbeddr
		languages and extensions include:</p>
	<ul>
		<li>C99 (C99 is an informal name for ISO/IEC 9899:1999, a past
			version of the C programming language standard which is supported by
			most embedded compilers)</li>
		<li>Statemachines</li>
		<li>Components</li>
		<li>Requirements</li>
		<li>Documentation</li>
		<li>Product-Line Support</li>
		<li>A build language</li>
		<li>Integration with various formal analyses tools (e.g. CBMC,
			NuSMV, yices)</li>
	</ul>
	<p>As well as additional base languages (e.g. C++ or Ada), and
		additional higher level languages extensions (e.g. tracing, logging,
		data flow, protocol description). In addition the project provides
		example languages and language modules which illustrate the
		integration into existing environments. The project does not include
		development and evolution of MPS, the verification tools themselves or
		actual C compilers or debuggers.</p>
	<h2>Description</h2>
	<p>mbeddr provides a fully featured IDE for C, arbitrary extensions
		of C and any other language developed in mbeddr. The system is
		structured into layers and concerns.</p>
	<p align="center">
		<img width="480" src="figures/mbeddr-stack.png"
			alt="mbeddr is organized into three concerns and five layers. The concerns address C-based implementation, formal analysis as well as process. The five layers are the MPS platform, the mbeddr core facilities, default extensions as well as the ability for users to create their own extensions. Finally, a number of backend tools (primarily C compilers and verification tools) are integrated.">
	</p>
	<h3>Implementation Concern</h3>
	<p>
		The implementation concern addresses the development of applications
		based on C. On the core level, mbeddr comes with an implementation of
		C in MPS (MPS is discussed below). There are a few minor differences
		to C99, and the preprocessor is not exposed to the user. On the
		default extensions layer, the implementation concern comes with C
		extensions for interfaces and components, state machines and physical
		units, among others. The user extensions layer is by definition empty;
		users can easily extend the C core as well as any of the default
		extensions. The implementation concern also ships with an extensible
		debugger that is able to debug on the level of the extensions, so the
		abstractions do not break down when debugging becomes necessary. In
		the future, mbeddr will also support importing existing header and
		implementation files, to support interoperability with legacy
		libraries or implementation code. At the foundation layer, the
		implementation concern relies on a C compiler, a C debugger and tools
		for importing existing C code into mbeddr. By default, mbeddr generate
		make files that call
		<code>gcc</code>
		, the debugger relies on
		<code>gdb</code>
		debugger and the <a href="www.eclipse.org/cdt">Eclipse CDT</a> for
		analyzing and importing textual C code. However, all of these can be
		changed.
	</p>
	<h3>Analysis Concern</h3>
	<p>This concern adds static analyses to some of the default
		extensions provided by the implementation concern. The analysis itself
		is performed by different existing external tools. However, mbeddr
		integrates the tools tightly by (a) providing language abstractions to
		conveniently describe behavior that can be analyzed, (b) translating
		this description to the input of the analysis tool, (c) running the
		tool, and (d) lifting the output of the tool back to the original
		abstraction level, to make it easier to understand for the user. The
		integrated analyses are based on symbolic model checking, SMT solving
		and C-level model-checking.</p>
	<h3>Process concern</h3>
	<p>The process concern includes cross-cutting facilities that are
		useful to integrate mbeddr into the development process. They are
		generic in the sense that they can be integrated with arbitrary other
		languages, such as all the default and user-defined C extensions.
		mbeddr supports requirements engineering, product-line variability and
		documentation.</p>
	<p>The requirements engineering support provides a language for
		describing requirements. Each requirement has an ID, a short
		description, an optional longer prose, a priority and any number of
		additional attributes. Requirements can also be nested and express
		relationships to other requirements. Requirements traces can be
		attached to any program element expressed in any language, supporting
		ubiquitous traceability. The requirements facility also supports
		reverse-tracing, so users can find out which parts of implementation
		artifacts depend on any particular requirement. Importantly, the
		requirements tooling is extensible; arbitrary additional data,
		expressed in any language, can be added to a requirement. This way,
		support for use cases, collaborations and scenarios has been added in
		a modular way.</p>
	<p>mbeddr's support for product-line engineering can be split into
		two parts. First, there is a formalism for defining feature models and
		configurations based on these feature models. This represents a purely
		conceptual description of variability, independent of any particular
		implementation. The second part maps the variability expressed in
		feature models to arbitrary implementation artifacts by using presence
		conditions. These are Boolean expressions over features that determine
		how the program element they are attached to depends on the features
		selected for a given variant. If the expression evaluates to false,
		the element they are attached to is removed during generation. As a
		consequence of MPS' projectional editor, the presence conditions can
		also be evaluated in the editor. This allows program variants to be
		viewed and edited. While presence conditions are static and work for
		any language, there is also C-specific support to evaluate variability
		at runtime.</p>
	<p>
		Visualization provides a facility to render diagrams. The facility
		relies on <a href="http://plantuml.sourceforge.net/">PlantUML</a> as
		the backend, which supports most of UML's diagrams. The diagrams are
		shown directly inside the mbeddr IDE; by clicking on a node in the
		diagram, the respective source node in the MPS editor is selected,
		supporting navigability between diagram and code. Any program element
		can contribute visualizations, making the facility generically usable.
	</p>
	<p>The documentation aspect supports writing prose documents as
		part of an mbeddr project, exportable as HTML or LaTeX. What makes
		this aspect worthwhile is that it supports close integration with
		program elements: program elements can be referenced (with real
		references that are renamed if the element itself is renamed) and
		program code can be embedded as text or as an image. The embedded code
		is updated whenever the document is regenerated, avoiding manual
		synchronization of the documentation. In the same way visualizations
		(as described in the previous paragraph) can be embedded, also updated
		whenever the document is re-rendered to LaTeX or HTML.</p>
	<h3>Underlying Technology</h3>
	<p>
		mbeddr relies on the JetBrains MPS language workbench. The <a
			href="http://jetbrains.com/mps">JetBrains Meta Programming System</a>
		is a projectional language workbench available as open source software
		under the Apache 2.0 license. The term <a
			href="http://martinfowler.com/articles/languageWorkbench.html">Language
			Workbench</a> was coined by Martin Fowler. He defines a language
		workbench as a tool with the following characteristics, all of which
		MPS exhibits: (1) Users can freely define languages which are fully
		integrated with each other. (2) The primary source of information is a
		persistent abstract representation. (3) A DSL is defined in three main
		parts: schema, editor(s), and generator(s). (4) Language users
		manipulate a DSL through a projectional editor. (5) A language
		workbench can persist incomplete or contradictory information.
	</p>
	<p>
		One of the most important statement in this definition is <em>languages
			which are fully integrated with each other</em>. It is not enough for a
		language workbench to address the definition of a single language. It
		must be possible to define sets of languages, and, while each of them
		should be a well-defined module, it is also essential to be able to
		define how these languages integrate. MPS is particularly good at
		this.
	</p>
	<p>
		MPS' Projectional editing does not rely on parsers. As a user edits a
		program, the AST is modified <em>directly</em>. A projection engine
		then creates some representation of the AST with which the user
		interacts, and which reflects the resulting changes. This approach is
		well-known from graphical editors: when editing a UML diagram, users
		do not draw pixels onto a canvas, and a "pixel parser" then creates
		the AST. Rather, the editor creates an instance of
		<code>uml.Class</code>
		as a user drags a class from the palette onto the canvas. A projection
		engine renders the diagram by drawing a rectangle for the class.
		Programs are stored using a generic tree persistence format (such as
		XML). This approach can be generalized to work with any notation,
		including textual.
	</p>
	<p>
		What makes projectional editing interesting for language workbenches
		in general, and for mbeddr in particular, are the following two
		characteristics. First, the approach can deal with arbitrary syntactic
		forms including textual, symbolic/mathematical, tabular and graphical.
		This means that much richer notations can be used <em>in an
			integrated fashion</em>, improving the usability to the end user. An
		example from mbeddr is the decision table shown in the picture above.
		Traditionally the tools for building textual and
		tabular/symbolic/graphical editors were very different in terms of
		their architecture and user experience, and integrating them
		seamlessly was a lot of work, and sometimes impossible. Second, if
		independently developed languages are composed, the resulting
		composite program is never syntactically ambiguous.
	</p>
	<h3>Relationship to other Eclipse projects</h3>
	<ul>
		<li><b>Eclipse Platform:</b> mbeddr currently does not run on top
			of the Eclipse platform. Instead it runs on MPS. If MPS allows the
			generation of Eclipse plugins in the future, mbeddr will directly
			benefit from that</li>
		<li><b>CDT:</b> As mentioned above, mbeddr uses the CDT Debug
			bridge to enable its debugging capabilities. Although and better
			offers the capability to edit regular C code, this is not the main
			purpose of mbeddr. Mbeddr's advantage is the strong integration of
			multiple implementation paradigms, its strong focus on formal
			verification techniques as well as the integration with other
			development process concerns.</li>
		<li><b>EMF:</b> There is no direct relationship to EMF, however,
			conceptionally the MPS's storage format as well as the low level APIs
			are similar to what EMF offers. Thus, in the future it might be
			possible import / export models to / from mbeddr. A general
			integration with EMF is however out of scope for this project as this
			will be done by MPS</li>
		<li><b>Xtext:</b> Again, there is no direct relationship to
			Xtext. Mbeddr uses MPS as the underlying Technology platform. MPS is,
			similar to Xtext, a language workbench but with a very different
			focus. While Xtext persists its models in plain ASCII files, MPS
			stores its models in a proprietary binary or text format. A user in
			MPS does not edit the text but works directly on the AST. Thus no
			parsing technology is necessary. In addition this enables the usage
			of different notations (text, tables, graphics), different
			projections for the same code as well as partial projections or
			partial views on a model.</li>
	</ul>
	<h2>Why Eclipse?</h2>
	<ul>
		<li>The target domain of mbeddr is embedded software and the
			Internet of Things (it is currently used in energy/metering systems
			and automotive software, among others). Eclipse has a strong
			community in this space, so this is a good fit as well.</li>
		<li>Eclipse provides a well-defined governance structure and
			development process. The mbeddr project will benefit from this
			structure and this way, additional contributors can be phased in more
			easily.</li>
		<li>Fundamentally, mbeddr is a DSL-based modeling and programming
			environment. Since Eclipse already has a strong focus on modeling, it
			is a good fit.</li>
	</ul>
	<h2>Initial Contribution</h2>
	<p>
		The initial contribution consists of sources, tests, build, and
		documentation. It comprises the complete mbeddr system including all
		above-mentioned languages and C extensions and their IDE support. In
		addition, it will provide a development environment for Arduino
		Hardware. The project does <em>not</em> include MPS itself, which is
		open source under the Apache 2.0 license.<br> The code is hosted
		at github: <a href="https://github.com/mbeddr/mbeddr.core/">https://github.com/mbeddr/mbeddr.core/</a>.<br>
		The project website is at <a href="http://mbeddr.com">http://mbeddr.com</a>.
	</p>
	<h2>Legal Issues</h2>
	<p>There are no known legal issues.</p>
	<ul>
		<li>mbeddr has a required dependency to <a
			href="http://www.jetbrains.com/mps/download/license.html">MPS</a>
			(Apache 2.0 licensed)
		</li>
		<li>mbeddr has a works-with dependency to <a
			href="http://plantuml.sourceforge.net/faq.html">PlantUML</a> as well
			<a href="http://www.graphviz.org/License.php">Graphviz</a> (EPL 1.0
			licensed)
		</li>
		<li>mbeddr has a works-with dependency to <a
			href="http://nusmv.fbk.eu/open_nusmv/flier.html">NuSMV</a> (LGPL 2.1
			licensed)
		</li>
		<li>mbeddr has a works-with dependency to <a
			href="http://www.cprover.org/cbmc/LICENSE">CBMC</a> (BSD-style
			License)
		</li>
		<li>mbeddr has a works-with dependency to <a
			href="http://yices.csl.sri.com/yices-newlicense.shtml">yices</a>
			(proprietary license)
		</li>
	</ul>
	<h2>Committers</h2>
	<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->
	<p>The following individuals are proposed as initial committers to
		the project:</p>
	<dl>
		<dt>Bernd Kolb (itemis AG) Project lead</dt>
		<dt>Markus Voelter (independent/itemis)</dt>
		<dt>Dan Ratiu (fortiss)</dt>
		<dt>Domenik Pavletic (itemis)</dt>
		<dt>Kolja Dummann (itemis)</dt>
	</dl>
	<p>We welcome additional committers and contributions.</p>
	<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->
	<h2>Mentors</h2>
	<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->
	<p>The following Architecture Council members will mentor this
		project:</p>
	<ul>
		<li>Benjamin Cab&eacute;</li>
		<li>Doug Schaefer</li>
	</ul>
	<h2>Interested Parties</h2>
	<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->
	<p>The following individuals, organisations, companies and projects
		have expressed interest in this project:</p>

	<ul>
		<li>Sierra Wireless</li>
		<li>Continental</li>
		<li>Siemens/LMS</li>
		<li>Sioux</li>
		<li>BMW-CarIT</li>
		<li>Rohde &amp; Schwarz</li>
		<li>Method Park</li>
		<li>Robert Bosch GmbH</li>
	</ul>

	<h2>Project Scheduling</h2>
	<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->
	<h2>Changes to this Document</h2>
	<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->
	<table>
		<tr>
			<td>25-November-2013</td>
			<td>Added interested parties</td>
		</tr>
		<tr>
			<td>4-November-2013</td>
			<td>Added interested parties</td>
		</tr>
		<tr>
			<td>07-October-2013</td>
			<td>Document created</td>
		</tr>
		<tr>
			<td>25-October-2013</td>
			<td>Incorporated feedback</td>
		</tr>
	</table>
</body>
</html>
