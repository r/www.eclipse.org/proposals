<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>

    <div id="maincontent">
	<div id="midcolumn">

<h1>The Google Summer of Code Project</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Google Summer of Code");
?>

<b>An Eclipse Technology Project Proposal<br />
June 2006</b>



<h2>Introduction</h2>
 
<p>The <a href="http://code.google.com/">Google Summer of Code</a> is an opportunity to for students to engage in summer projects 
and receive funding (USD$4500) for their work from Google. The programme is structured around a student/mentor
arrangement in which a student receives help from a designated mentor for the duration of the project.
Projects are undertaken during the summer months, from June until the end of August.</p>


  <h2>Background</h2>

  <p>Registration for the programme started in May. Approximately 75 students submitted project proposals
  derived from a number of sources. Several prominent members of the Eclipse community agreed to
  become mentors. As mentors, their first task was to participate in the selection process in which
  the initial 75 projects were ranked according to interest, need, and ability of the student to
  actually complete the project. Most of the 75 proposals were for interesting,
  serious, and well thought out Eclipse-related projects. At the end the process, each of the mentors
  signed up to participate in one or more of the projects. Finally, Google's process determined
  that the eleven (11) highest-ranked projects would be funded for Eclipse.</p>


  <h2>Scope</h2>

  <p>This project is itself a container for other projects. These other projects are 
  <a href="#projects">described below</a>. Each of these projects has been proposed by
  a student participating in the Google Summer of Code programme and is supported by one or
  more mentors (as indicated below). The initial development effort will be complete at 
  the end of August 2006. At this time, the progress made in completing
  these projects will be reviewed and an appropriate course of action will be determined. It
  may be determined, for example, that the code should be moved into another Eclipse project.
  Alternatively, we may decide that a project be permitted to continue with input from other
  members of the community, or that the project be shutdown. </p>

  <h2><a name="projects"></a>Projects</h2>
<p>The following projects are being undertake:</P>
<ul><li> <b><a href="http://wiki.eclipse.org/index.php/ECF_BitTorrent_Provider" title="ECF BitTorrent Provider">ECF BitTorrent Provider</a></b> (<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=144133" class="external text" title="https://bugs.eclipse.org/bugs/show bug.cgi?id=144133" rel="nofollow">#144133</a>)
<ul><li> Student: Remy Chi Jian Suen
</li><li> Mentor(s): Chris Aniszczyk, Scott Lewis, Wayne Beaton
</li></ul>
</li></ul>
<ul><li> <b><a href="http://wiki.eclipse.org/index.php/Word_Wrap_for_Text_Viewer_and_Editor" title="Word Wrap for Text Viewer and Editor">Word Wrap for Text Viewer and Editor</a></b> (<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=35779" class="external text" title="https://bugs.eclipse.org/bugs/show bug.cgi?id=35779" rel="nofollow">#35779</a>)

<ul><li> Student: Ahti Kitsik
</li><li> Mentor(s): Philippe Ombredanne
</li></ul>
</li></ul>
<ul><li> <b><a href="http://wiki.eclipse.org/index.php/Generic_form_description_and_a_prototypical_implementation_of_a_render_engine_for_Eclipse_RCP" title="Generic form description and a prototypical implementation of a render engine for Eclipse RCP">Generic form description and a prototypical implementation of a render engine for Eclipse RCP</a></b>
<ul><li> Student: Steffen Gr�n
</li><li> Mentor(s): Gunnar Wagenknecht
</li></ul>
</li></ul>
<ul><li> <b><a href="http://wiki.eclipse.org/index.php/Mylar_Bugzilla_Connector_Enhancements" class="external text" title="http://wiki.eclipse.org/index.php/Mylar Bugzilla Connector Enhancements" rel="nofollow">Enhancing Mylar's Bugzilla Integration</a></b>

<ul><li> Student: Jeff Pound
</li><li> Mentor(s): Mik Kersten
</li></ul>
</li></ul>
<ul><li> <b><a href="http://wiki.eclipse.org/index.php/Mylar_Trac_Connector" class="external text" title="http://wiki.eclipse.org/index.php/Mylar Trac Connector" rel="nofollow">Mylar Trac Connector Plug-in</a></b> (<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=140512" class="external text" title="https://bugs.eclipse.org/bugs/show bug.cgi?id=140512" rel="nofollow">#140512</a>)
<ul><li> Student: Steffen Pingel
</li><li> Mentor(s): Mik Kersten
</li></ul>

</li></ul>
<ul><li> <b><a href="http://wiki.eclipse.org/index.php/RT_Shared_Editing" title="RT Shared Editing">Real-Time Shared Editing</a></b>
<ul><li> Student: Mustafa K. Isik
</li><li> Mentor(s): Scott Lewis
</li></ul>
</li></ul>
<ul><li> <b><a href="http://wiki.eclipse.org/index.php/Shared_Code_Plugin" title="Shared Code Plugin">Shared Code Plug-in (SCP)</a></b>
<ul><li> Student: Marcelo Mayworm
</li><li> Mentor(s): Scott Lewis, Ken Gilmer

</li></ul>
</li></ul>
<ul><li> <b>Eclipse RCP Installer/Packages Generator</b>
<ul><li> Student: Jacobo Garc�a
</li><li> Mentor(s): Francois Granade
</li></ul>
</li></ul>
<ul><li> <b>A Distributed Object Application Debugger for the Eclipse Platform</b>
<ul><li> Student: Giuliano Mega
</li><li> Mentor(s): Fabio Kon

</li></ul>
</li></ul>
<ul><li> <b>Basic Eclipse Mono Development Environment and Contributions Towards an Eclipse IDE Generator</b>
<ul><li> Student: Rebecca Chernoff
</li><li> Mentor(s): Doug Schaefer
</li></ul>
</li></ul>
<ul><li> <b><a href="http://wiki.eclipse.org/index.php?title=Duplicated_code_detection_tool_%28SDD%29&amp;action=edit" class="new" title="Duplicated code detection tool (SDD)">Duplicated code detection tool (SDD)</a></b>
<ul><li> Student: Iryoung Jeong
</li><li> Mentor(s): Pascal Rapicault
</ul>
</ul>
  <h3>Core Platform</h3>

  <h3>Application Platform</h3>

  <h2>Existing standards and projects leveraged</h2>
<p>This varies from SOC project to project. All projects leverage the Eclipse Platform. Some leverage other Eclipse 
projects, including Mylar, Eclipse Communication Framework, and CDT.</p>
  <h2>Organization</h2>
  <p>The Summer of Code project is itself a mid-level project. Each of its subprojects will
  have it's own directory structure.</p>
  
  <h3>Interested parties</h3>

<p>This project will be managed by Wayne Beaton and Phillippe Ombredanne. All participating students
and mentors will be initial committers.</p>
  <h2>Roadmap</h2>
<u>April 14, 2006:</u><br>
- Application period open for mentoring organizations; administrators may begin
submitting applications for their orgs<br>
- Organization administrators should begin taking mentor signups once the org
has been accepted by Google<br>
- Summer-Administrators Google Group opens for discussion between org
administrators, mentors, and Google program administrators<br>

- Summer-Discuss Google Group opens for discussions between organization
administrators, mentors, potential student applicants and Google program
administrators<br><br>
<u>April 24, 2006:</u><br>
- All mentoring organizations must have submitted their interest to Google due
by 08:00 Pacific Daylight Time <br><br>
<u>May 1, 2006:</u><br>
- Application period opens for student proposals at 13:00 Pacific Daylight Time<br><br>
<u>May 9, 2006:</u><br>
-Student proposals due by 11:00 Pacific Daylight Time<br><br>
<u>Interim Period:</u><br>
- Mentoring organizations review and rank student proposals; where necessary,
mentoring organizations may request further proposal detail from the student
applicant<br>

- Summer-Discuss Google Group opens for discussions between organizations
administrators, mentors student applicants and Google program
administrators<br><br>
<u>May 22, 2006:</u><br>
- All mentors must be signed up with their organization by 08:00 Pacific Daylight Time<br>
- All student proposals must be ranked and assigned to a mentor by 08:00 Pacific Daylight Time<br><br>
<u>May 23, 2006:</u><br>
- Google publishes list of accepted student proposals on code.google.com<br>
- Students commence work on their projects under the guidance of their
mentors<br>
- Summer-Accepted Google Group opens for discussions between accepted students
and Google program administrators <br>
<br>

<u>Interim Period:</u><br>
- Google begins issuing initial student payments <br>
- Mentors give students a helping hand and guidance on their projects<br><br>
<u>June 26, 2006:</u><br>
Google solicits mid-program mentor evaluations of student progress<br><br>
<u>June 30, 2006:</u><br>
- All mid-program evaluations of student progress due by 17:00 Pacific Daylight
Time<br>
- Google begins issuing mid-term student payments<br><br>
<u>OSCON, July 24-28, 2006:</u><br>

Google gives community update at
<a href="http://conferences.oreillynet.com/os2006/">OSCON 2006</a><br><br>
<u>August 21, 2006:</u><br>
- All student projects due by 08:00 Pacific Daylight Time<br>
- Final mentor evaluations of student progress commence<br>
- Student evaluations of mentors commence<br><br>
<u>September 5, 2006:</u><br>
- All mentor and student evaluations due by 08:00 Pacific Daylight Time<br>
- Google begins issuing final student payments<br>

- Google program administrators begin review of students' evaluations of their
mentors<br><br>
<u>September 25, 2006:</u><br>
Google begins issuing payments to mentoring organizations<br><br>

      </div>
  </div>
  
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
