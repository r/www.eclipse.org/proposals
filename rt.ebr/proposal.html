<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Eclipse Bundle Recipes</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->

<style type="text/css">
dt {
	display: list-item;
	list-style-position: outside;
	list-style-image:
		url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
	margin-left: 16px;
}

dd {
	margin-left: 25px;
	margin-bottom: 5px;
}
</style>

<body>
	<p>
		The Eclipse Bundle Recipes project is a proposed open source project
		under the Eclipse <a
			href="http://www.eclipse.org/projects/project_summary.php?projectid=rt">Runtime
			Project</a>.
	</p>

	<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
	<p>
		This proposal is in the Project Proposal Phase (as defined in the
		Eclipse Development Process) and is written to declare its intent and
		scope. We solicit additional participation and input from the Eclipse
		community. Please send all feedback to the <a
			href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse
			Proposals</a> Forum.
	</p>

	<h2>Introduction</h2>

	<p>This proposal recommends the creation of a new project called
		"Eclipse Bundle Recipes" (EBR), to provide "recipes" in the form of
		template files which can be used to convert open source library Jar
		files into OSGi Bundles. EBR will be a subproject of the Eclipse
		Runtime Project (RT) and will aim to exploit the functionality of
		existing projects. We encourage and request additional participation
		and input from any and all interested parties.</p>

	<h2>Background</h2>

	<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->

	<p>
		One of the challenges Java developers face in creating solutions that
		make use of the OSGi Service Platform is in finding OSGi bundle forms
		of the open source software they use. This need was partly addressed
		by the <a href="http://ebr.springsource.com/repository/app/">SpringSource
			Enterprise Bundle Repository</a> which is maintained by VMware (after
		their acquisition of SpringSource). Each entry in the SpringSource
		repository is created from a template and provides metadata about the
		bundle; namely:
	</p>
	<ul>
		<li>Ivy and Maven XML snippets describing how to declare a
			dependency on the bundle.</li>
		<li>The Java packages the bundle exports, i.e. its external
			contract.</li>
		<li>The Java packages the bundle imports, i.e. the contracts it
			depends on.</li>
		<li>Bundles that are able to provide those packages.</li>
	</ul>
	<p>The repository is transitively closed: all bundle dependencies
		are satisfied by other bundles in the repository.</p>
	<p>The SpringSource repository does not allow for external
		contributions and so is difficult for a single vendor to keep current
		(and, in practice, VMware has limited resources to apply to
		maintaining the repository). A templates project at Eclipse will help
		share the responsibility and better serve the needs of OSGi users.</p>

	<h2>Scope</h2>

	<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->

	<p>The scope of the EBR project is to provide template files for
		creating bundles for open source Jars. Scoping the project to
		templates rather than the resulting bundles facilitates bundle
		creation for a wide variety of open source projects, not just those
		with licenses acceptable to the Eclipse Foundation. Only templates
		whose creation and use do not violate the license of the original Jar
		will be permitted.</p>
	<p>The EBR project is aimed at open source Jars which do not
		already have reasonable OSGi manifests. If a project originating such
		a Jar provides a reasonable OSGi manifest for a subsequent version of
		the Jar, then the EBR project will discontinue its support of the JAR
		(for new versions - existing versions of the template will remain in
		the EBR project for anyone who needs them) in order to encourage the
		community to treat the originating project as the preferred source of
		the corresponding bundle. This is deemed to be a success story for the
		EBR project.</p>
	<p>Enhancements to the Eclipse Virgo Bundlor tool may be necessary
		to handle identification and processing of the Jar to which a template
		applies.</p>
	<p>The primary goal of the project is the provision of templates,
		but the project should also consider the following enhancements to the
		Eclipse Marketplace and Client:</p>
	<ol>
		<li>A new 'OSGi Bundles' market with entries for bundles
			(provided outside the EBR) and templates from EBR. These bundles and
			templates would be subject to the usual <a
			href="http://marketplace.eclipse.org/content/eclipse-marketplace-publishing-guidelines">Marketplace
				publishing restrictions</a>.
		</li>
		<li>Browser-based Bundlor equivalent for consuming templates from
			the Marketplace and creating the resulting bundle.</li>
		<li>Modification of the Eclipse Marketplace Client so that
			bundles from the OSGi Bundles market can be discovered directly in
			the Eclipse IDE and pulled into the user's workspace or PDE target
			platform. This is different to how the IDE operates with the
			Marketplace today as that interaction causes plugins to be installed
			into the IDE itself.</li>
	</ol>
	<p>Although the bundles, which may be generated from the project,
		will not be hosted at Eclipse, anyone who is able to may generate and
		host those bundles, for example in a Maven repository. Companies may
		choose to do this internally to provide repeatable builds. A vendor
		may choose to generate and publish the bundles to a publicly visible
		repository.</p>
	<p>A longer term goal of the project is to provide an OSGi R5
		Repository API implementation to programmatically search the bundle
		metadata associated with the generated bundles.</p>

	<h2>Description</h2>

	<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->

	<p>The EBR project provides a library of templates that can be used
		to create bundles for open source projects. The Eclipse Virgo Bundlor
		tool converts a Jar to a bundle driven by a given template. Each
		template guides the addition of OSGi manifest headers in order for the
		Jar to work correctly as an OSGi bundle.</p>
	<p>The project satisfies a broad set of bundle requirements by
		hosting only templates (not the original Jar or resulting bundle),
		thus allowing it to address a wide variety of open source projects,
		including those using licenses which are unacceptable to the Eclipse
		Foundation.</p>
	<p>In taking this template approach it is important that the bundle
		generation be a repeatable process and so enhancements to Bundlor and
		the templates it can consume will ensure the correct Jar is used.</p>
	<p>Contributions to the project, including templates, may be made
		using the regular Eclipse processes including attaching a patch to
		Eclipse bugzilla or issuing a pull request on github.</p>

	<h2>Why Eclipse?</h2>

	<!-- 
	Answer these two questions:	What value does this project bring to the Eclipse
	community? What value do you expect to obtain from hosting your project at Eclipse?
	
	What value do you get by having your project at Eclipse over and above the value
	of hosting at Eclipse Labs?
 -->

	<p>The vast majority of Eclipse projects are built using OSGi, and
		Eclipse Equinox has been the OSGi Core Reference Implementation for
		many years. As exploiters of OSGi, many Eclipse projects and their
		users will benefit from, and potentially contribute to, a project
		which addresses their need for pre-bundled copies of the libraries
		they use.</p>
	<p>The Eclipse Marketplace and associated client also offer the
		opportunity to make OSGi bundles easily discoverable and accessible to
		developers and users.</p>

	<h2>Initial Contribution</h2>

	<!-- 
	Projects are expected to arrive at Eclipse with existing code.
	
	Describe the existing code that will be contributed to the project. Please provide
	a couple of paragraphs describing the code with modest detail, including important
	information like code ownership (who holds the copyright?), and some consideration
	of community that exists around the code. Include a listing of third-party libraries 
	and associated licenses.
 -->

	<p>
		The initial contribution of templates to the Bundle Recipes project
		will be made by VMware based on the SpringSource Enterprise Bundle
		Repository content. Many of these templates have already been
		published in <a
			href="https://github.com/glyn/bundlerepo/tree/master/templates">github</a>
		under the Eclipse Public License.
	</p>

	<h2>Legal Issues</h2>

	<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->

	<p>In committing template files into Eclipse, committers need to be
		sure that the template, in its creation or use, does not violate the
		license of the Jar it references. For example, a license that did not
		permitted updates to the Jar manifest, or for the Jar to be packaged
		inside another artefact (e.g. wrapped in an OSGi bundle), would
		prohibit the creation of a bundle for the Jar without violating the
		license. Such a license may not actually exist, but contributors will
		need to be aware that this due diligence is necessary.</p>

	<h2>Committers</h2>

	<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

	<p>The following individuals are proposed as initial committers to
		the project:</p>

	<ul>
		<li>Neil Bartlett (Paremus)</li>
		<li>David Bosschaert (Red Hat)</li>
		<li>Graham Charters (IBM)</li>
		<li>Chris Frost (VMware/SpringSource, Project Lead)</li>
		<li>Jeremy Hughes (IBM, Project Lead)</li>
		<li>Toni Menzel (Rebaze)</li>
		<li>Glyn Normington (VMware/SpringSource)</li>
		<li>John Ross (IBM)</li>
		<li>Raman Gupta</li>
		<li>Gunnar Wagenknecht</li>
		<li>Tom Watson</li>
	</ul>

	<p>We welcome additional committers and contributions.</p>

	<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

	<h2>Mentors</h2>

	<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

	<p>The following Architecture Council members will mentor this
		project:</p>

	<ul>
		<li>Ian Bull (EclipseSource)</li>
		<li>Thomas Watson (IBM)</li>
	</ul>

	<h2>Interested Parties</h2>

	<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

	<p>The following individuals, organisations, companies and projects
		have expressed interest in this project:</p>

	<ul>
		<li>Peter Kriens (aQute)</li>
		<li>SAP AG</li>
	</ul>

	<h2>Project Scheduling</h2>

	<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->

	<ul>
		<li>January 2013, Bundle Recipes proposal submitted</li>
		<li>February 2013, Creation review</li>
		<li>August 2013, Graduation review</li>
	</ul>

	<h2>Changes to this Document</h2>

	<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

	<table>
		<tr>
			<th>Date</th>
			<th>Change</th>
		</tr>
		<tr>
			<td>28 January 2013</td>
			<td>Document created</td>
		</tr>
	</table>
</body>
</html>