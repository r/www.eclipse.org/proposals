<html>
<head>
	<title>EMF Diff/Merge (EDM)</title>
</head>
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>
<body>



<!-- START OF CONTENT -->

<p>
The "EMF Diff/Merge" (EDM) project is a proposed open source project under the <a
href="http://www.eclipse.org/projects/project_summary.php?projectid=modeling.emf">
EMF Project</a>.
</p>
<p>
This proposal is in the Project Proposal Phase (as defined in the
<a href="http://wiki.eclipse.org/Development_Resources">Eclipse
Development Process</a> document) and is written to declare its intent and
scope. This proposal is written to solicit additional
participation and input from the Eclipse community. You are invited to
comment and/or join the project. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a> Forum.
</p>



<h2>Background</h2>
<p>
Comparing and merging models is a common need in modeling activities.
A very frequent use case is version control: whenever team working is involved,
models require similar versioning and merge facilities as source code.
Even in collaborative environments such as CDO, the possibility for users
to work offline implies a later synchronization, and therefore diff/merge,
with the model repository.
Besides, many classic features involve some kind of model merging:
bridges between tools, model migration, model refactoring, 
incremental model transformation, etc.
</p>
<p>
However, merging models is more challenging than merging text or code.
EMF-based models are rich data structures constrained by their metamodel,
therefore a single change in an element may have consequences to other elements.
If users are not assisted by their diff/merge tool,
they may make contradictory or inconsistent merge decisions leading to data loss and incorrect,
unexpected model content. This problem can become critical with large, complex models whose merging
requires time-consuming efforts.
</p>



<h2>Scope</h2>
<p>
EDM provides a lightweight engine for comparing and merging models using IDs.
The emphasis is on scalability and reliability, with the objective of preserving
the integrity and consistency of models during the merge process.
EDM is intended to be a technological enabler: it includes both a customizable diff/merge engine
and Graphical User Interface (GUI) components which can be used by or integrated into other tools.
</p>



<h2>Description</h2>
<p>
EDM supports 2-way and 3-way comparison of models that conform to EMF-based metamodels.
Similarly to traditional file comparison, a 2-way comparison involves two models while
a 3-way comparison involves an additional "common ancestor" model which enables the engine to
identify the origin of differences. 
</p>
<p>
The usage process is illustrated by the figure below.
First, a comparison is created based on the models to compare.
The differences between those models are computed according to given policies.
Then, as long as differences remain, any subset of these differences can be selected for merging.
Every time, EDM uses predefined consistency rules and a user-defined consistency policy to compute
the minimal superset of differences that must be merged to preserve consistency.
The user may decide whether to confirm or cancel the merge of the whole set of differences.
</p>

<center>
<img src="process.png"/>
</center>

<p>
EDM primarily matches model elements by IDs, where an ID can be
any criterion that uniquely identifies the element within the scope of the comparison:
Ecore ID, qualified name, etc.
</p>
<p>
The EDM engine operates at a technical level: it is based on a notion of low-level difference which is
suitable to consistent merging.
EDM is thus not intended to provide high-level "semantic" feedback to the user.
However, higher-level comparison frameworks could use the EDM engine as a back-end for
enforcing consistent merging while relying on their own high-level difference model.
</p>
<p>
The EDM engine is expected to provide at least the following features:
	<ul>
		<li>
Support for different scopes.
The models being compared can be obtained from one or several files, a repository, a given set of model elements, etc.
		</li>
		<li>
Extensibility/adaptability.
Customization of the match, diff and merge phases thanks to user-defined policies and comparison scopes.
		</li>
		<li>
Conflict detection.
The notion of conflict in 3-way model comparison is significantly different from its counterpart in text comparison.
If users A and B rename the same model element E differently, then it is obviously a conflict.
However, if user A renames E and user B deletes E, then these two changes can also be considered in conflict because
accepting one of them implies to reject or nullify the other one. The engine must be able to detect such cases.
		</li>
	</ul>
</p>
<p>
The GUI is directly based on the concepts of the engine and hence reflects their technical nature.
It may not be best for users willing to have a high-level overview of differences; however,
it is helpful for experimenting comparison policies, or handle complex models that require
a good understanding of the merging process.
Additionally, the GUI is able to provide advanced feedback based on the features of the engine.
</p>
<p>
The GUI is expected to provide at least the following features:
	<ul>
		<li>
Overview of merge impact.
Whenever the user selects differences to merge, all consistency-dependent differences must be presented to him/her.
The objective is that the user clearly understands the global impact of his/her actions on the model.
		</li>
		<li>
Conflict notification.
Whenever the user attempts to merge differences that conflict with others, the GUI must highlight the cause of the conflicts
and ask for confirmation.
		</li>
		<li>
Undo/redo of merge actions.
Whenever the user makes wrong merge actions, (s)he must be able to undo them without any harm to the models.
		</li>
	</ul>
</p>



<h2>Why Eclipse?</h2> 
<p>
Since the main purpose of EDM is to integrate into higher-level tools, the interest of the community is critical to its success.
The Eclipse ecosystem is the central place where projects that could benefit from EDM are available.
</p>



<h2>Initial Contribution</h2>
<p>
	<ul>
		<li>
The initial engine solely depends on Eclipse runtime and EMF.
It has been successfully used for version control in several real-life projects.
It has also been embedded in a CASE tool as an enabler for model refactoring and
incremental model transformations, which allowed to check that it can effectively be reused and integrated.
		</li>
		<li>
The initial GUI is based on SWT/JFace, Eclipse UI and Eclipse Compare.
It supports visual comparison of models within Eclipse with integrability in mind.
Its overall design was worked out in collaboration with users from industry.
Although not as mature as the engine, it has been successfully tested on models of industrial size.
		</li>
	</ul>
</p>



<h2>Legal Issues</h2>
<p>
The whole content of the initial contribution is currently property of Thales/TGS.
</p>



<h2>Relationship with Other Eclipse Projects/Components</h2>
<ul>
	<li>
		EMF: EMF provides the foundations of the whole Eclipse modeling ecosystem. Since EMF defines what a model and a metamodel are, EDM is closely coupled to EMF.
	</li>
	<li>
		EMF Compare: Altough EMF Compare and EDM both provide comparison and merge facilities, they are essentially complementary. 
		EMF Compare is focused on being an end-user tool; as such, it provides integration mechanisms with components like GMF, eGit, EcoreTools, Papyrus.
		It also provides ways to specialize its difference model in order to obtain a semantic, user-friendly rendering of the changes.
		For example, a specialization exists for UML and SysML.
		By contrast, EDM operates at a lower level of abstraction: its difference model is technical so as to support the expression of consistency rules.
		Consequently, EMF Compare could rely on the EDM engine whenever a particular consistency policy must be enforced. 
	</li>
	<li>
		Eclipse Compare: Eclipse Compare provides hooks for contributing diff/merge tools to the Eclipse platform. The EDM GUI relies on Eclipse Compare.
	</li>
	<li>
		Eclipse Team: Eclipse Team allows interacting with Software Configuration Management systems (SCMs)
		such as SVN or Git within Eclipse. Tools that rely on Eclipse Team for supporting version control of models may use
		EDM whenever merging is needed.
	</li>
	<li>
		CDO and EMF Store: Model repositories such as CDO or EMF Store support collaborative modeling.
		They propose an offline mode which requires version branching and merge: EDM is typically intended to be useful in this context.
	</li>
	<li>
		Others: Every EMF-related project which is concerned with model merging involving consistency policies.
	</li>
</ul>



<h2>Organization</h2>

<h3>Initial committers</h3>
<p>
The following individuals are proposed as initial committers to the project:
</p>
<ul>
	<li>Olivier Constant, Thales/TGS, proposed project lead</li>
	<li>Thomas Guiu, Soyatec</li>
	<li>Matthieu Helleboid, Soyatec</li>
	<li>Maximilian K&ouml;gel, EclipseSource</li>
	<li>Eike Stepper, ES Consulting</li>
</ul>


<h3>Mentors</h3>
<p>
The following Architecture Council members will mentor this project:
</p>
<ul>
	<li>C&eacute;dric Brun</li>
	<li>Ed Merks</li>
</ul>


<h3>Interested Parties</h3>
<p>
The following organizations have expressed interest in the project :
</p>
<ul>
	<li>Thales/TGS - <a href="http://www.thalesgroup.com">www.thalesgroup.com</a></li>
	<li>Airbus - <a href="http://www.airbus.com/">www.airbus.com</a></li>
	<li>Atos - <a href="http://atos.net/en-us/">www.atos.net</a></li>
	<li>Budapest University of Technology and Economics - <a href="http://www.mit.bme.hu">www.mit.bme.hu</a></li>
	<li>EclipseSource - <a href="http://eclipsesource.com/munich">eclipsesource.com/munich</a></li>
	<li>ES Consulting - <a href="http://www.esc-net.de">www.esc-net.de</a></li>
	<li>Itemis - <a href="http://www.itemis.com">www.itemis.com</a></li>
	<li>Obeo - <a href="http://www.obeo.fr">www.obeo.fr</a></li>
	<li>Soyatec - <a href="http://www.soyatec.com">www.soyatec.com</a></li>
</ul>


<h3>Developer and user communities</h3>
<p>
Critical to the success of this project is participation of interested
third parties to the community. We intend to reach out to this
community and enlist the support of those interested in making a
success of the EDM project. We ask interested parties to contact
<a href="http://www.eclipse.org/forums/index.php/f/108/">EMF Forum</a>
to express interest in contributing to the project.
</p>



<h2>Tentative Plan</h2>
<p>
Initial contribution is expected during summer 2012.
</p>

<!-- END OF CONTENT -->

</body>
</html>
