<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Object Teams (OT)</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Object Teams (OT)");
?>

<h2>Introduction</h2>
<p>
    The Object Teams Project is a proposed open source project under the
    <a href="http://eclipse.org/tools/">Eclipse Tools Project</a>.
</p>
<p>
    This proposal is in the Project Proposal Phase (as defined in the
    <a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process</a> document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the <a href="http://www.eclipse.org/forums/eclipse.objectteams">Object Teams forum</a>.
</p>
<h2>Background and Description</h2>
<p>
In any software development that involves a certain level of complexity, is based on re-usable components and evolves over a significant period of time, there are typically some <b>tensions</b> between different people and sub-projects contributing to the overall code base. E.g., framework developers have to balance the flexibility of their framework with the safety provided by strong and strictly enforced encapsulation. Framework users (developers of components using the framework) will normally desire more (or just other) options for adapting the framework's behavior than what the framework already provides. Also, different developers may simply focus on different concerns that in the final code crosscut each other due to intricate dependencies. Put simply, these tensions tend to result from the necessity to create systems from a single coherent object oriented design.
</p>
<p>
In order to overcome much of these tensions the Object Teams (OT) programming model has been developed, which is centrally based on the concepts of <b>roles</b> and <b>teams</b>. Roles have first been introduced for role modeling, where different <b>perspectives</b> are expressed by different models which then relate to a shared set of base abstractions (classes). Roles in Object Teams <b>bring this power of multi-perspective modeling to the level of programming</b>. Each role model is mapped to the implementation as a team class, whose contained roles implement those facets of objects that are relevant to the given perspective. In this approach explicit <b>bindings</b> between a role and its base entity provide powerful means for integrating a system from components. Here integration clearly means more than just creating a system as the sum of its components, but those bindings define significant parts of the communication in the resulting system.
</p>
<p>
The benefits of developing in the Object Teams approach are twofold:
<ol>
<li>The <b>code structure</b> within each component can be much simpler and easier to understand because original concerns can faithfully be encapsulated as teams and roles, allowing for separate evolution of concerns over time. Any crosscutting interaction can concisely be defined using the mentioned bindings.</li>
<li><b>Re-use</b> of existing components is greatly improved, because bindings between different components can create client-defined adaptations of a re-usable component in a concise and disciplined way. With Object Teams many of the compromises that are necessary with traditional approaches become obsolete.<a href="http://www.eclipsecon.org/2009/sessions?id=347">[1]</a>
</li>
</ol>
</p>
<p>
The Object Teams approach has been materialized at two levels.
<ul>
<li>The programming language <b>OT/J</b> adds the concepts of Object Teams to the Java&#153; programming language. Development in OT/J is supported by a comprehensive IDE, the <b>OTDT</b>&nbsp;(Object Teams Development Tooling)&nbsp;<a href="http://www.eclipsecon.org/2008/?page=sub/&amp;id=352">[2]</a>, which builds on and extends the <a href="http://www.eclipse.org/jdt/">Eclipse Java development tools</a>.<br>
Development of the OTDT has started in 2003 (based on Eclipse 2.1.1) to large parts under the umbrella of the publically funded project <a href="http://www.topprax.de">TOPPrax</a>. After the 1.0 release in March 2007 new release have been published roughly every 6 weeks.
</li>

<li>The component runtime <b>OT/Equinox</b>&nbsp;<a href="http://www.jot.fm/issues/issue_2007_10/paper6.pdf">[3]</a> extends <a href="http://www.eclipse.org/equinox/">Equinox</a> as to support so-called aspect bindings accross bundle boundaries. Thus, Equinox bundles (plug-ins) can be implemented in OT/J and explicit bindings connect an Object Teams bundle to other bundles in much more powerful ways than the traditional approach.<br>
OT/Equinox is being developed since 2006 and has been used for the implementation of the OTDT itself ever since.
</li>
</ul>
</p>


<h2>Proposed Components</h2>
The following components define the internal structure of the Object Teams Project,
they do not define individual Eclipse projects:
<ul>
<li><b>OT/J</b>. Provides the compiler and runtime for OT/J programs. The compiler can be used as a batch compiler, via Apache Ant or Apache Maven 2 or as part of the OTDT (see below). The OTRE (Object Teams Runtime Environment) consists of a few foundation classes plus a set of byte code transformers that perform the weaving for any role-base bindings.</li>
<li><b>OT/Equinox</b>. Provides the runtime for running Equinox bundles written in OT/J. This includes providing an extension point by which aspect bindings are declared to the framework plus an integration of the OTRE byte code weavers into the Equinox framework.</li>
<li><b>OT/JPA</b>. Provides an integration of OT/J and the <a href="http://www.eclipse.org/eclipselink/">EclipseLink</a> implementation of the JPA.</li>
<li><b>OTDT</b>. Provides comprehensive support for developing OT/J programs. This includes specific views, editors, wizards, content assist and debugging support. The OTDT specifically supports development for the OT/Equinox runtime.</li>
</ul>
It will be checked whether the freely available OTJLD (OT/J Language Definition) shall also be maintained
within the Object Teams Project. More components may follow in the future.

<h2>Scope</h2>
<p>
The main focus of the project is to maintain and further improve the technology for
developing and executing programs written in OT/J.
This includes editing (several views, content assist, refactoring etc.)
compiling (batch and incremental) running and debugging.
</p><p>
The language is essentially considered fixed as defined in the
<a href="http://www.objectteams.org/def/OTJLDv1.2-final.pdf">OTJLD (OT/J Language Definition) version 1.2</a>,
i.e., big changes to the language OT/J are outside the scope of this project.
The project should, however, react to small changes in the OTJLD.
</p><p>
Current technology includes integration with the runtimes of Equinox and EclipseLink.
Creating similar integrations with other runtimes (like, e.g., servlet engines)
could be in the scope of the project, given that sufficient interest is expressed.
</p><p>
Further development tools like a graphic modeler for Object Teams could also be in the
scope of this project, again depending on interest expressed.
</p><p>
For executing OT/J programs, which currently involves load-time byte-code weaving,
a prototype of an alternative strategy exists that also supports run-time weaving.
Maturing this prototype is in the scope of this project.
</p><p>
Integration with more tools (see "Tentative Plan" below) is within the scope of
this project, interest provided.
</p>
<p>
Given the relative maturity of the code contribution, a significant goal behind
moving this project to Eclipse lies in more effectively promoting Object Teams
and actively <b>building a community</b>.
These activities will aim at establishing a long term perspective for
the Object Teams technology.
</p>

<h2>Relationship with other Eclipse Projects</h2>

<p>
The Object Teams Project is currently the only implementation of the Object Teams approach, so no direct competition or overlap exists.</p>
<p>
To a certain degree Object Teams shares motivation and a few core mechanisms with aspect-oriented programming and thus with <a href="http://www.eclipse.org/aspectj/">AspectJ</a> and the <a href="http://www.eclipse.org/ajdt/">AJDT</a>.
Historically, within the academic context of the AOSD (Aspect Oriented Software Development)
conference series, AspectJ and OT/J were occasionally discussed in a competitive way.
However, meanwhile both languages are commonly viewed as complementary:
At the core of AspectJ a sophisticated pointcut language has been developed.
OT/J uses much simpler and more explicit binding mechanisms.
On the other hand OT/J focuses more on providing capabilities for creating modules
that can be composed in various ways including nesting, layering and inheritance of complex modules. Apart from a few shared motifs AspectJ and OT/J are very different languages (a full discussion of the differences is beyond the scope of this document), and thus their tools do not overlap.</p>
<p>
The current OT/J compiler is a source-level branch of the Eclipse Compiler for Java&#8482; (ecj, part of the JDT/Core). As such it shares all properties like incremental compilation, support for eager parsing, public AST (dom) etc. with the original.
The OT/J compiler is compatible with the ecj, as demonstrated by the corresponding JDT test suites. These test suites find no significant differences between the original JDT and the Object Teams variant.
Within the source code all additions and modifications are clearly marked (see also the section <a href="#development">development</a>).
The parallel development of the JDT/Core and the OT/J branch has fostered quite some interaction between developers including numerous contributions to the JDT/Core.</p>
<p>
Other parts of Eclipse are re-used and extended/adapted using OT/Equinox: JDT/UI, JDT/Debug (Core and UI), PDE (Core and UI) etc. While adapting these components in unanticipated ways, their source code is not altered. OT/Equinox and the OTDT are very explicit regarding the facts of bundle adaptation, so both developers and users can easily inspect the status of the system using the "About" dialogs, a specific OT/Equinox monitor and the package explorer (during development).</p>
<p>
OT/Equinox is also related to the <a href="http://www.eclipse.org/equinox/incubator/aspects/index.php">Equinox Aspects</a> project. At some point it might be considered to merge these two efforts. However, currently OT/Equinox has a few requirements (regarding load order and eager activation) that are not covered by the current design of Equinox Aspects. A merger of both projects would facilitate the combined use of OT/Equinox with other components employing a byte code weaver.</p>
<p>
Many other projects can potentially benefit from Object Teams. E.g., a case study<a href="http://www.objectteams.org/publications/presentations.html#DemoCamp09">[4]</a> has shown how the maintainability of a graphical editor based on EMF/GMF can be significantly improved. Regarding the model driven approach it is a well-known problem that many issues of fine-tuning a generated application can best be solved using customizations of the generated source code, which eventually degrades the structure and thus maintainability of the software. The mentioned case study has demonstrated that <em>all</em> customizations of the class diagram editor from the UML2Tools project could effectively be restructured as an OT/Equinox plug-in that is bound to a pristine version of the generated source code. We expect similar scenarios to apply to several other Eclipse projects.</p>

<h2>Organization</h2>

<h3><a name="development">Development</a></h3>
All software of the Object Teams software has been developed using the following infrastructure:
<ul>
<li>Subversion source code repository (migrated from an initial CVS repository).</li>
<li>JUnit plug-in tests: a number of original Eclipse test suites are used (with slight modifications) plus additional 2150 JUnit plug-in tests specific for OTDT features.</li>
<li>A specific test suite for compiler and runtime comprising currently 2300 test programs.</li>
<li>Total number of test cases used in daily builds is currently 44520</li>
<li>Build automation based on PDE-build</li>
<li>Issue tracking and wiki using <a href="http://trac.edgewall.org/">Trac</a></li>
</ul>
<p>
All development resources are openly available at <a href="http://www.objectteams.org">www.objectteams.org</a> or more specifically at <a href="http://trac.objectteams.org/ot">trac.objectteams.org/ot</a>.
</p>
<p>
When adapting the JDT/Core great care has been taken in sustaining maintainability. For that purpose
a strict discipline of marking code changes has been applied throughout. For all other plug-ins beside the JDT/Core in-place modification has been strictly avoided, but OT/Equinox has been used to integrate any required adaptations. Since its inception we have migrated our branch to a new version of Eclipse for 5 times. By the experience from these migrations, the co-evolution of Eclipse and the OTDT has become routine. Integrating the Object Teams development with the Eclipse coordinated releases will require only minimal changes in our development process.
</p>

<h3>Initial Committers</h3>
<p>
The initial committers will focus on further improving the compatibility with the JDT, to the end that replacing the JDT/Core with its Object Teams variant has no negative impact on any user in any configuration. Wherever suitable this will include cooperating with other projects, especially the JDT/Core team, in order to help improving the flexibility of those core components.</p>
<p>
Also the usage of a few libraries (notably BCEL) will be sorted out as to fit within the Eclipse policies.</p>
<ul>
<li>Stephan Herrmann (Independent): Project Lead.</li>
<li>Marco Mosconi (<a href="http://first.fraunhofer.de">Fraunhofer FIRST</a>): Committer.</li>
<li>Olaf Otto (<a href="http://www.unic.com/ch/de.html">Unic AG</a>): Committer OT/JPA</li>
</ul>

<h3>Mentors</h3>
<ul>
<li>Chris Aniszczyk</li>
<li>Darin Wright</li>
<li>Ed Merks</li>
</ul>

<h3>Interested parties</h3>
The following companies/projects/individuals have expressed interest in this project.
Key contacts listed.
<ul>
<li><a href="http://www.itemis.de">itemis</a>: Steffen Stundzig</li>
<li><a href="http://www.eclipse.org/gef3d/">GEF3D</a>: Jens von Pilgrim</li>
<li><a href="http://www.intershop.de"/>Intershop Communications AG</a>: Peter H&auml;nsgen</li>
<li><a href="http://www.gebit.de">GEBIT Solutions</a>: Tom Krauss</li>
<li><a href="http://www.beaglesoft.de">BeagleSoft</a>: Tim Ehlers</li>
<li>Martin Lippert</li>
</ul>

<h3>Code contribution</h3>
<p>
The current software has been developed by <a href=" http://www.swt.tu-berlin.de">Technische Universit&auml;t Berlin</a> and <a href="http://first.fraunhofer.de">Fraunhofer FIRST</a> and is publically available under the EPL v1.0.</p>

<h3>Tentative plan</h3>
<p>
A new version can be released any time. It is planned that the project graduates in the summer of 2010, so that the official 1.0 release of Object Teams as an Eclipse project shall hopefully be published in conjunction with the Helios coordinated release.</p>
<p>
Future plan items include:
<ul>
<li>Maturing the JPA integration.</li>
<li>Potentially integrating with Equinox Aspects.</li>
<li>Integrating with more tools like <a href="http://www.eclipse.org/projects/project_summary.php?projectid=tools.mylyn">Mylyn</a>, <a href="http://www.eclemma.org/">EclEmma</a>, <a href="http://pleiad.cl/tod/index.html">TOD</a>, etc.</li>
<li>Continuous improvement of all existing components.</li>
</ul>

<h2>Current Home Page</h2>
Please visit <a href="http://www.objectteams.org">www.objectteams.org</a> for more information.

<h2>References</h2>
<ul>
<li>
<a href="http://www.eclipsecon.org/2009/sessions?id=347">[1] "Plugin reuse and adaptation with Object Teams: Don't settle for a compromise!"</a>, presentation by Stephan Herrmann at EclipseCon 2009</li>
<li>
<a href="http://www.eclipsecon.org/2008/?page=sub/&amp;id=352">[2] "The Object Teams Development Tooling: A high fidelity extension of the JDT"</a>, presentation by Stephan Herrmann at EclipseCon 2008</li>
<li>
<a href="http://www.jot.fm/issues/issue_2007_10/paper6.pdf">[3] "Integrating Object Teams and OSGi: Joint Efforts for Superior Modularity"</a>, Stephan Herrmann and Marco Mosconi in Journal of Object Technology, vol. 6, no. 9, October 2007, pages 105&mdash;125.</li>
<li>
<a href="http://www.objectteams.org/publications/presentations.html#DemoCamp09">[4] "Modular EMF/GMF customization with ObjectTeams/Java &mdash; case study: UML2 Tool"</a>, presentation by Marco Mosconi at Eclipse Demo Camps May/June 2009.</li>
</ul>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
