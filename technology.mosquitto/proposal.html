<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Mosquitto</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<p>The Mosquitto project is a proposed open source project under the 
<a href="http://www.eclipse.org/projects/project_summary.php?projectid=Technology">Technology
Container Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<h2>Background</h2>

<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->

<p><a href="http://mqtt.org">MQTT</a> is a protocol created in the late 1990s by Andy Stanford-Clark of IBM and Arlen Nipper, then of Arcom Control Systems.  It was designed for machine-to-machine (M2M) communication, and runs over TCP/IP.  The current version of MQTT is 3.1; the specification is for which is held on
<a href="https://www.ibm.com/developerworks/webservices/library/ws-mqtt/">IBM developerWorks</a>.   Its primary goals are:
<ol>
<li>to avoid polling of sensors, allowing data to be sent to interested parties the moment it is ready</li>
<li>lightweight, so that it can be used on very low bandwidth connections</li>
</ol>
MQTT is currently undergoing standardization at
 <a href="https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=mqtt">OASIS</a>.  
</p>

<p>MQTT-SN (currently being renamed from MQTT-S) is a protocol derived from MQTT, designed for connectionless underlying network transports such as UDP.   The definition of the MQTT-SN protocol is held at <a href="http://mqtt.org/MQTT-S_spec_v1.2.pdf">MQTT.org</a>. It is less mature than MQTT, the specification being published in 2008, by Andy Stanford-Clark and Hong Linh Truong, both of IBM. 
</p>

<p>Both MQTT and MQTT-SN are client-server protocols, for which a server is needed to distribute messages between the client applications.  They also both use the publish-subscribe paradigm, rather than queueing: a receiving application subscribes to topics of interest, and the sending application publishes messages to topics.  The <cite>publisher</cite> is disassociated from any <cite>subscribers</cite> - it has no knowledge of whether any application is receiving its messages. MQTT and MQTT-SN are undergoing an increase in interest as a consequence of the activity and publicity around the <cite>Internet of Things</cite>, to which they are suited.
</p>

<p>After several IBM servers were produced during the 2000s, Really Small Message Broker (RSMB) was released onto IBM alphaWorks in 2008.  It's aim was to be a minimalist MQTT server, and according to the usual alphaWorks practices, was closed source and released under an evaluation-only license.  During the following two years, it gained a small but enthusiastic following.  In 2010, Roger Light learned about MQTT and RSMB from an Andy Stanford-Clark presentation.  He created Mosquitto to be an open source alternative to RSMB.
</p>

<p>Since then Mosquitto has gained an enthusiastic following of its own, and has grown to include functions not available in RSMB.  It has been included in several Linux distributions.  RSMB has had MQTT-SN capabilities added but not released outside of IBM.  Now we have the chance to bring Mosquitto and RSMB back together as one Eclipse project, taking advantage of the collaboration of the authors of both previous projects.</p>

<h2>Scope</h2>

<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->

<p>The Mosquitto project provides a small server implementation of the MQTT and MQTT-SN protocols.  Small means that:
<ol>
<li>only necessary function is included (conditional compilation can be used to be able to omit unneeded function for a particular application),</li>
<li>that function is coded as efficiently as possible,</li>
<li>the externals are as simple as possible for the function provided.</li>
</ol>
</p>
<p>The server has the following features, which are not described in the MQTT specification:
<ol>
<li>An MQTT <cite>bridge</cite>, to allow Mosquitto to connect to other MQTT servers.</li>
<li>The ability to secure communications using SSL/TLS.</li>
<li>User authorization - the ability to restrict user access to MQTT topics.</li>
</ol>
</p>

<p>Mosquitto is able to translate and transfer messages between MQTT and MQTT-SN, so that it can act as a gateway between devices which communicate with either protocol.
</p>

<p>Mosquitto keeps up to date with any MQTT or MQTT-SN specification changes, for instance the outcome of the OASIS MQTT specification standardization.  Mosquitto adheres to the protocol specifications and standards as closely as possible, so it may be used in conformance testing. Any included non-standard behaviour is optional.
</p>

<h2>Description</h2>

<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->

<p>Mosquitto provides a lightweight server implementation of the MQTT and MQTT-SN protocols, written in C.   The reason for writing it in C is to enable the server to run on machines which do not even have capacity for running a JVM.   Sensors and actuators, which are often the sources and destinations of MQTT and MQTT-SN messages, can be very small and lacking in power.  This also applies to the embedded machines to which they are connected, which is where Mosquitto could be run.
</p>

<p>Typically, the current implementation of Roger Light's Mosquitto has an executable in the order of 120kB that consumes around 3MB RAM with 1000 clients connected. There have been reports of successful tests with 100,000 connected clients at modest message rates.
</p>

<p>As well as accepting connections from MQTT client applications, Mosquitto has a <cite>bridge</cite> which allows it to connect to other MQTT servers, including other Mosquitto instances.  This allows networks of MQTT servers to be constructed, passing MQTT messages from any location in the network to any other, depending on the configuration of the bridges.
</p>
 
<h2>Why Eclipse?</h2> 

<!-- 
	Answer these two questions:	What value does this project bring to the Eclipse
	community? What value do you expect to obtain from hosting your project at Eclipse?
	
	What value do you get by having your project at Eclipse over and above the value
	of hosting at Eclipse Labs?
 -->

<p>Over the past few years, Eclipse has become an obvious destination for projects involved in M2M through the creation of the M2M Industry Working Group and the number of related projects being created.
</p>

<p>Roger hopes that bringing Mosquitto to Eclipse will increase its exposure for both users and possible contributors to the project. By contributing RSMB to Eclipse, IBM hopes to strengthen the MQTT community within Eclipse, and enable the functions provided by RSMB to be enjoyed by a wider audience.
</p>

<p>The proposed Mosquitto project is complementary with existing Eclipse projects that make use of MQTT, such as Paho and Koneki.  Paho provides MQTT client libraries in various languages.  The Mosquitto project is complementary to Paho by providing the required server which allows client devices and applications to communicate with each other.   The combination of Paho and Mosquitto will allow complete MQTT and MQTT-SN based M2M solutions to be built.
</p>  

<p>The existing open-source Mosquitto project is already linked with Eclipse as it provides the MQTT sandbox server available at m2m.eclipse.org.
</p>

<h2>Initial Contributions</h2>

<!-- 
	Projects are expected to arrive at Eclipse with existing code.
	
	Describe the existing code that will be contributed to the project. Please provide
	a couple of paragraphs describing the code with modest detail, including important
	information like code ownership (who holds the copyright?), and some consideration
	of community that exists around the code. Include a listing of third-party libraries 
	and associated licenses.
 -->

<p>The initial contributions will be made from the two projects previously mentioned, which have a lot in common, but different implementations:
<ol>
<li><a href="http://mosquitto.org/">Mosquitto</a>, an open source project created by Roger Light</li>
<li><a href="https://www.ibm.com/developerworks/community/groups/service/html/communityview?communityUuid=d5bedadd-e46f-4c97-af89-22d65ffee070">Really Small Message Broker (RSMB),</a>, a closed source project owned by IBM, created by Ian Craggs.</li>
</ol>
</p>

<p>They are similar because Mosquitto was originally written as an open-source replacement for RSMB.  The incorporation of both codebases into the same Eclipse project, gives the opportunity of taking the best of both.
</p>

<h4>Mosquitto</h4>

<p>The Mosquitto code base is currently held at Bitbucket in a Mercurial repository (bitbucket.org/oojah/mosquitto). This includes the broker/server itself, as well as MQTT client libraries in C, C++, Python and JavaScript. The JavaScript client is considered deprecated because of the Paho JavaScript client and the Python client has already been submitted to Paho.
</p>

<p>There is a reasonable community starting to exist around Mosquitto. The bug tracker lists 133 bugs submitted, the mosquitto-users mailing list has 79 members and stackoverflow has 44 questions that mention mosquitto. The code is majority owned by the contributor, but there have been a few contributions from other parties. The external dependencies of Mosquitto are:
<ul>
<li>openssl (OpenSSL and SSLeay licenses: both BSD-style)</li>
<li>uthash (BSD revised license)</li>
<li>libwrap/tcp-wrappers (<a href="ftp://ftp.porcupine.org/pub/security/tcp_wrappers_license">license</a>)</li>
</ul>
</p>

<h4>Really Small Message Broker</h4>

<p>The second contribution is the Really Small Message Broker server and MQTT-SN client code, all of which is copyright IBM in its entirety - there is no third party licensed code.  It will be made available under the EPL and EDL.
</p>  
 
<h2>Legal Issues</h2>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->

<p>As noted above, there have been a few code contributions from other parties to the current Mosquitto codebase.
</p> 

<p>It is intended that the project be dual licensed under the <a href="http://www.eclipse.org/legal/epl-v10.html">EPL</a> and 
<a href="http://www.eclipse.org/org/documents/edl-v10.php">EDL</a>.  The inclusion of EDL is primarily so that the project
can be included in Linux distributions, which use the GPL, which Roger Light's existing Mosquitto project is already.
</p> 

<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

<p>The following individuals are proposed as initial committers to the project:</p>

<dl>
	<dt>Roger Light, University of Nottingham</dt>
	<dd>Roger is the creator and main contributor of Mosquitto. He will be the project leader.  <!-- He will contribute to the *** functionality in this new project.-->
	</dd>
	<dt>Ian Craggs, IBM</dt>
	<dd>Ian is the creator and main author of Really Small Message Broker. <!-- He will contribute
	to the *** functionality in this new project.-->
	</dd>
</dl>

<p>We welcome additional committers and contributions.</p>

<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Doug Schaefer</li>
	<li>Achim L&ouml;rke</li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li>Andy Piper, Pivtol Labs</li>
	<li>Dave Locke, IBM</li>
</ul>

<h2>Project Scheduling</h2>

<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>22-Oct-2013</td>
		<td>Second mentor added.</td>
	</tr>
</table>
</body>
</html>
