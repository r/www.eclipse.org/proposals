<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>SCA Tools Sub-project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("SCA Tools");
?>

<p>
<h2>Introduction</h2>
<p>
SCA Tools is a proposed sub-project under the top level project <a href="http://www.eclipse.org/stp/">Eclipse SOA Tools Platform (STP)</a>.
</p>
<p>
This proposal is in the <a href="http://www.eclipse.org/projects/dev_process/development_process.php#6_2_2_Proposal">Project Proposal Phase</a> (as defined 
in the <a href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse Development Process</a> document) and is 
written to declare the intent and scope of the SCA Tools sub-project. This 
proposal is written to solicit additional participation and input 
from the Eclipse community. You are invited to comment on and 
join the project. Please send all feedback to the SCA Tools newsgroup (<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.stp.sca-tools">http://www.eclipse.org/newsportal/thread.php?group=eclipse.stp.sca-tools</a>).
</p>

<h2>Background</h2>
<p>
The mission of the <a href="http://www.eclipse.org/stp/">STP project</a> is to build frameworks and extensible tools that enable the design, configuration, assembly, deployment, monitoring, and management of software designed around a Service Oriented Architecture. The project is guided by the values of transparency, extensibility, vendor neutrality, community collaboration, agile development, and standards-based innovation. 
</p>
<p>
Currently, <a href="http://www.eclipse.org/stp/">STP project</a> contains a component named <a href="http://www.eclipse.org/stp/sca/index.php">STP/SCA</a>. Among other things, this component provides a graphical designer, named <a href="http://wiki.eclipse.org/STP/SCA_Component#Overview">SCA Composite Designer</a>, which allows to construct SCA assembly files. This tool is based on <a href="http://www.eclipse.org/gmf/">GMF</a> and works with an <a href="http://www.eclipse.org/emf">EMF</a> meta model constructed from the <a href="http://www.osoa.org/display/Main/Service+Component+Architecture+Specifications">SCA specifications version 1.0</a> proposed by the <a href="http://www.osoa.org/display/Main/Home">Open SOA consortium</a>.
</p>
<p>
The aim of this proposal is to transform the actual <a href="http://www.eclipse.org/stp/sca/index.php">STP/SCA</a> component into a sub-project of <a href="http://www.eclipse.org/stp/">STP project</a> and to add several other SCA tools.
</p>

<h2>Description</h2>
<p>
The purpose of the Eclipse SCA Tools sub-project is to develop a set of tools to help developers of SCA applications (like the SCA meta model, editors and a graphical designer which allows to construct SCA assembly files).
</p>
<p>
Our goals are also to link these SCA tools with existing Eclipse tools that can fit some aspects of the SCA specifications like the components <a href="http://www.eclipse.org/stp/policy/index.php">STP Policy Editor</a> and <a href="http://www.eclipse.org/stp/sc/">STP Service Creation</a>. SCA tools will also interact with the <a href="http://www.eclipse.org/stp/im/index.php">STP Intermediate Model</a> component.
</p>

<h2>Project Scope</h2>
The SCA Tools sub-project will focus on tools covering the SCA specifications proposed by the Open SOA consortium:

<ul>
<li> SCA ecore meta model corresponding to the XSD scheme proposed by the Open SOA consortium,</li>
<li> Ecore meta models of additional implementations, interfaces, and bindings defined by the SCA runtime implementations Tuscany, Frascati, and Fabric3,</li>
<li> Editors (XML, form, graphical) helping developpers to construct SCA assembly files,</li>
<li> Tools that introspect existing code to complete the SCA model according to SCA annotations found in the code.</li>
</ul>

<h2>Out of Scope</h2>
Somethings are not in the scope of the SCA Tools sub-project. In particular some aspects of the Open SOA specifications that are already covered by existing components of the STP project :

<ul>
<li> Deployment aspect (covered by the SOAS component),</li>
<li> Tool to assist developpers in the definition of SCA annotations in Java code (covered by the SC component),</li>
<li> Model transformations between SCA and other technologies (covered by the IM component).</li>
</ul>

<h2>Proposed Components</h2>
<ul>
<li> <a href="http://wiki.eclipse.org/STP/SCA_Component/SCA_Composite_Meta_Model"><b>SCA Domain Model</b></a> (existing code in STP/SCA component). This component contains the EMF meta model based on the SCA specifications version 1.0 proposed by the <a href="http://www.osoa.org/display/Main/Home">Open SOA consortium</a>. This meta model will serve as the basis for many tools. This component contains also additional validation rules that appear in the SCA specifications document (<a href="http://www.osoa.org/download/attachments/35/SCA_AssemblyModel_V100.pdf?version=1">SCA Assembly Model V1.00</a>) and that are not checked by the EMF meta model implementation. These rules are listed <a href="http://wiki.eclipse.org/STP/SCA_Component/SCA_Composite_Meta_Model#Additional_validation_rules">here</a>. These rules will be implemented using the <a href="http://www.eclipse.org/modeling/emf/?project=validation#validation">EMF Validation Framework</a>. This component can be use in an Eclipse environment or standalone.</li>
<li> <b>SCA Integration</b>. This component will contain all plugins that implement integration with other STP tools in relation with SCA: STP Policy Editor, STP Service Creation, STP SOA System.</li>
<li> <a href="http://wiki.eclipse.org/STP/SCA_Component/SCA_Composite_Designer"><b>SCA Composite Designer</b></a> (existing code in STP/SCA component): this component is a graphical (GMF) development environment for the construction of composite applications.</li>
<li> <a href="http://wiki.eclipse.org/STP/SCA_Component/SCA_Composite_Editors"><b>SCA Composite Editors</b></a>: this component will contain a source editor, a tree editor, an XML editor and a Form editor to construct SCA assembly files.</li>
</ul>

<h2>Relationship with Other Eclipse Projects</h2>
The SCA Tools sub project will be built on top of the Eclipse Platform and will have relationship with other Eclipse projects. 

<ul>
<li> <a href="http://www.eclipse.org/stp/">STP</a>
	<ul>
	<li> <a href="http://www.eclipse.org/stp/policy/index.php">STP Policy Editor component</a> will be used to implements SCA policies.</li>
	<li> <a href="http://www.eclipse.org/stp/sc/">Service Creation component</a> will be used to introspect and to annotate Java code.</li>
	<li> <a href="http://www.eclipse.org/stp/soas/index.php">STP SOA System component</a> will be used to package and deploy SCA applications to runtime containers.</li>
	<li> <a href="http://www.eclipse.org/stp/im/index.php">Intermediate Model</a> component planned to work on transformations between the IM and the <a href="http://wiki.eclipse.org/STP/SCA_Component/SCA_Composite_Meta_Model">SCA Domain Model</a>.</li>
	</ul></li>
<li> <a href="http://www.eclipse.org/emf"> EMF project</a> is used to generate the <a href="http://wiki.eclipse.org/STP/SCA_Component/SCA_Composite_Meta_Model">SCA Domain Model</a>.</li>
<li> <a href="http://www.eclipse.org/modeling/emf/?project=validation#validation">EMF Validation Framework</a> is used to implement the additional validation rules.</li>
<li> <a href="http://www.eclipse.org/gmf/">GMF project</a> is used to generate the <a href="http://wiki.eclipse.org/STP/SCA_Component/SCA_Composite_Designer">SCA Composite Designer</a>.</li>
<li> <a href="http://www.eclipse.org/m2m/">M2M project</a> will be used to implement transformations between SCA models and IM models.</li>
</ul>

<h2>Organization</h2>
We propose that this sub-project will take place under the top level project <a href="http://www.eclipse.org/stp/">STP</a>.

<h2>Proposed Initial Committers</h2>
<ul>
<li> Stephane Drapeau  <a href="http://www.obeo.fr/">Obeo</a> (Leader)  committer of STP/SCA Component.</li>
<li> Etienne Juliot  <a href="http://www.obeo.fr/">Obeo</a>  committer of STP/SCA Component.</li>
<li> Vincent Zurczak  <a href="http://www.ebmwebsourcing.com/">EBM Websourcing</a>  He will be proposed as a new committer. Vincent has provided valuable patches for the STP/SCA Component and he plans to develop the XML and Form editors for SCA assembly files.</li>
</ul>

<h2>Code Contributions</h2>
The <a href="http://www.eclipse.org/stp/sca/index.php">Eclipse STP/SCA</a> component will be the initial code.

<h2>Interested Parties</h2>
At this time, the following peoples, companies and projects have expressed their interest to see the creation of the SCA Tools sub-project:
<ul>
<li><a href="http://cwiki.apache.org/TUSCANY/">Apache Tuscany project</a></li>
<li> Gaël Blondelle  <a href="http://www.ebmwebsourcing.com/">EBM Websourcing</a></li>
<li> Naci Dai  <a href="http://www.eclipse.org/webtools/">WTP PMC member</a>, <a href="http://www.eteration.com/">Eteration</a></li>
<li> Alain Boulze  <a href="http://www.inria.fr/index.en.html">INRIA</a></li>
<li> Adrian Mos  <a href="http://www.inria.fr/index.en.html">INRIA</a></li>
<li> Oisin Hurley  <a href="http://www.eclipse.org/stp/">STP project lead</a>, <a href="http://www.iona.com/">IONA</a></li>
<li> Florian Lautenbacher  <a href="http://www.eclipse.org/jwt/">JWT project lead</a></li>
<li> <a href="http://www.obeo.fr/">Obeo</a></li>
<li> Marc Dutoo  <a href="http://www.eclipse.org/jwt/">JWT project lead</a>, <a href="http://www.openwide.fr/">OpenWide</a></li>
<li> <a href="http://www.scorware.org/">OW2/SCOrWare project</a></li>
<li> Dimitar Dimitrov  <a href="http://www.sap.com/index.epx">SAP</a></li>
</ul>

<h2>Tentative Plan</h2>
<ul>
<li> June 2008, release V1.0 in Ganymede
<ul>
<li> Core: SCA nature, wizard for the creation of new SCA assembly files, preference pages.</li>
<li> SCA Domain Model: SCA specifications 1.0 from OSOA + additional validation rules which apply on the SCA assembly description files.</li>
<li> SCA Composite Designer: support of Tuscany SCA elements.</li>
<li> Update of the web site and the wiki.</li>
<li> Tutorial that explain how to use SCA tools.</li>
</ul></li>
<li> December 2008
<ul>
<li> SCA Domain Model: full support of the additional validation rules.</li>
<li> SCA Composite Designer: support of Frascati SCA elements.</li>
<li> SCA Composite Editors: XML editor + Form editor.</li>
<li> SCA Integration: Integration with the components STP Policy Editor, STP Service Creation, ans STP SOA System.</li>
</ul></li>
<li> March 2009
<ul>
<li> SCA Domain Model: support of SCA specifications that should be published in december 2008 by OASIS.</li>
<li> SCA Composite Designer: update with the new meta model.</li>
<li> SCA Composite Editors: update with the new meta model.</li>
</ul></li>
</ul>
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
