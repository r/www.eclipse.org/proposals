<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

	#*****************************************************************************
	#
	# proposal.php
	#
	# Author: 		Richard C. Gronback
	# Date:			2005-12-01
	#
	# Description: 
	#
	#
	#****************************************************************************
	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Eclipse Modeling Project Proposal";
	$pageKeywords	= "modeling,model-driven,metamodel";
	$pageAuthor		= "Richard C. Gronback";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# $Nav->addNavSeparator("My Page Links", 	"downloads.php");
	# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

	# End: page-specific settings
	#
		
	# Paste your HTML content between the EOHTML markers!	
ob_start();
?>

<div id="maincontent">
	<div id="midcolumn"><br/>
		<h1>Eclipse Modeling Project Proposal</h1>
		<h4>An Eclipse Top-Level Project Proposal</h4>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Modeling Top-Level");
?>
		<p>This proposal is presented in accordance with the <a href="/projects/dev_process/proposal-phase.php">Eclipse Development Process</a> and is written to declare the project's intent and scope as well as to solicit additional participation and feedback from the Eclipse community. You are invited to join the project and to provide feedback using the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling">http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling</a> newsgroup.</p>		
		<hr/>

<!-------------------------------------------------------------------------------------------------------------->
		
		<h3>Introduction</h3>
		<p>Eclipse has several projects that deal with modeling, in one form 
or another.  Currently, these projects are spread across the Tools and 
Technology top-level projects.  It is proposed that they be brought together 
under a new top-level Eclipse Modeling Project.  The goal of this reorganization 
would be to foster communication and interoperability among the projects, and to 
better control the growing list of Eclipse projects by means of a logical 
organizational structure.</p>
 
            <p>This document will propose the scope and organization of the 
project. A project charter will be created to describe in more detail the 
principles of the project, the roles and responsibilities of its participants, 
and the development process. As there are no new contributions included with 
this proposal, there are no new Intellectual Property (IP) items involved.</p>

<!-------------------------------------------------------------------------------------------------------------->

		<h3>Project Scope</h3>
	<p>The Eclipse Modeling Project will focus on the evolution and promotion 
of model-based development technologies within the Eclipse community. It will 
unify and refine existing Eclipse projects falling into this classification, and 
augment them with those needed to bring holistic model-based development 
capabilities to the Eclipse community. It will be the task of the newly formed 
Project Management Committee (PMC) of this proposed top-level project to solicit 
contributors in order to meet this goal.</p>
      
      <p>The items below delineate the scope of the proposed Modeling project.  
They do not do so uniquely, as other roughly equivalent terms and concepts could 
have been chosen to define the domain of modeling in the context of software 
development.  These items also serve as a logical grouping of functionality to 
be referenced in the Project Organization section.</p>
            
            <h4>Abstract Syntax Development (metamodeling)</h4>
            <p>Included in the scope of the project is a framework to support 
the definition of abstract syntax for modeling languages that support business, system, and software modeling, using an industry 
standard metamodelling facility or metalanguage.  The framework for developing 
abstract syntax will support editing, validating, testing, querying, and refactoring 
metamodels created with the metamodeling facility. This includes the production 
of general-purpose modeling languages in addition to application domain specific 
models.</p>
            <p> This set of capabilities is currently, in part, provided by the 
Eclipse Modeling Framework (EMF).  EMF is therefore an essential part of the 
Modeling project.</p>
            
            <p>The underlying metamodeling facility must support the production 
of a wide range of models, not all of which will themselves be suited for 
inclusion within the Modeling project.  Only those models and modeling languages 
of broad cross-domain utility fall within the scope of the Modeling project 
itself.  For example, implementations of the Object Constraint Language (OCL) 
and Unified Modeling Language (UML) specifications are appropriately maintained 
within the Modeling project. In contrast, it is expected that users will create 
and maintain languages for narrowly defined problem domains using the project 
facilities, but that these languages and models will not be of interest to the 
general modeling community. A section below includes a more complete list of 
standard languages to be supported by the Modeling project.</p>
		
		<h4>Concrete Syntax Development</h4>
		<p>Support for the production of textual and graphical concrete syntax for an abstract syntax is within the scope of the project. Both manual and generative approaches to the production of these are to be supported. As examples, graphical editing for the Unified Modeling Language (UML) as well as textual editing of UML models using Human-Usable Textual Notation (HUTN) fall within the scope of the project. Furthermore, the production of editors for any Domain-Specific Language (DSL) is to be supported.</p>
		
	<p>With respect to support of textual notation in the form of Eclipse editors, the Modeling project will focus on the generative aspect to producing these editors, targeting the facilities provided by the platform.</p>
		
		<h4>Model Transformation</h4>
		<p>The transformation of models using a transformation definition and associated technologies falls within the scope of the project, and is currently provided by the Generative Model Transformer (GMT) project. The support of industry standards is expected in this area, specifically the OMG's Query, View, Transformation (QVT) specification.</p>
		
		<h4>Model to Text Generation</h4>
		<p>Text generation from a model, typically source code of some programming source language, including the merger of user changes to generated output, is within the scope of the project and currently provided by the Java Emitter Template (JET) and JMerge portions of EMF. Alternative mechanisms have been requested by the community and along with support for patterns, falls within the scope of the project.</p>
				
		<h4>Industry Standards</h4>
		<p>The importance of supporting industry standards is critical to the success of the Modeling project, and to Eclipse in general. The role of the Modeling Project in the support of industry standards is to enable their creation and maintenance within the Eclipse community. Furthermore, as standards bodies such as the OMG have a strong modeling focus, the Modeling project needs to facilitate communication and outreach through its PMC and project contributors to foster a good working relationship with external organizations.</p>
		<p>The following industry standards are either supported by current modeling projects, or are anticipated to be supported in the future:</p>
		<ul>
			<li>Object Management Group (OMG) <a href="http://www.omg.org/technology/documents/modeling_spec_catalog.htm">standards</a></li>
		 	<ul>
		  		<li>Unified Modeling Language (UML) and UML Profiles not falling within the scope of other projects</li>
		  		<li>Model-Driven Architecture (MDA) related specifications</li>
		  		<li>Query, View, Transformation (QVT)</li>
		  		<li>Human-Usable Textual Notation (HUTN)</li>
		  		<li>MOF to Text (MOF2T)</li>
		  		<li>Diagram Interchange Specification (DIS)</li>
		  		<li>XML Metadata Interchange (XMI)</li>
			</ul>
		  	<li>XML Schema Infoset Model (XSD)</li>
		  	<li>Service Data Objects (SDO)</li>
		  	<li>etc...</li>
		</ul>
		
		<h4>Domain-Specific Modeling</h4>
		<p>The support of industry standards and specifications are an important aspect to the scope of the Modeling project, but not to the exclusion of the emerging trend of Domain-Specific Languages (DSLs). The scope of the project includes support for an Eclipse-based set of capabilities to offer an alternative to tooling such as Microsoft's <a href="http://msdn.microsoft.com/vstudio/teamsystem/workshop/DSLTools/default.aspx">DSL Tools</a> for the Visual Studio environment. The Eclipse Modeling Project will provide leadership in delivering these capabilities through its projects and in working with others within the Eclipse and external communities. The Graphical Modeling Framework (GMF) project is headed in the direction of providing support for Domain-Specific Modeling by leveraging EMF and the Graphical Editing Framework (GEF).</p>
	<p>The generative production of editors for textual notations is an essential component of DSL support within Eclipse, and required if Eclipse is to be used as a "language workbench." The Modeling project will provide, within its scope, the generative aspect of producing these editors to complement graphical editors for a modeled domain.</p>
		
		<h3>Out of Scope</h3>
		<p>Clearly, there are some things that are <b>not</b> in the scope of the Modeling Project. In particular, there are certain DSLs and industry-based models that the Modeling Project should support creation of at a fundamental level, but that are not approporiate to be housed within the project. Specific examples are listed below:</p>
		<ul>
			<li>A home for DSLs based on EMF which do not pertain to the modeling domain, e.g. the Java EMF Model (JEM) created for the Visual Editor (VE) project and used by the WebTools project.</li>
			<li>A number of OMG standards that fall into the modeling realm are not considered to be within the scope of this project:
				<ul>
					<li>Software Process Engineering Metamodel (SPEM), as it is part of the proposed Beacon project.</li>
					<li>Common Warehouse Metamodel (CWM), as it is likely to be included in DTP.</li>
					<li>UML Testing Profile, as it is included in the TPTP project.</li>
					<li>Reusable Asset Specification (RAS)</li>
					<li>etc...</li>
				</ul>
			</li>
			<li>As mentioned above, in support of textual notation editor generation, the Modeling project will work with the Platform or other teams, and not itself focus on the development of general purpose text editor frameworks.</li>
			<li>In the area of model transformation, XSL does not fall within the scope of the Modeling project and is provided within the context of the WebTools Project (WTP).</li>
		</ul>


		<h3>Project Organization</h3>
		<p>The Modeling Project will partly consist of projects which already exist as components of the Tools or Technology top-level projects. Those projects currently incubating within the Technology PMC will continue to incubate under the new Modeling PMC. Certain projects will be expanded, while others will be split into more fine-grained projects.</p>
		<p>Additional project expansion and new project formation is expected over the life of the Modeling Project to take into the current scope proposed above, and its inevitable alterations. It will be the responsibility of the PMC to actively recruit contributors and alter the project organization to maintain alignment and coverage of its defined scope.</p>

<p>The following structure corresponds more-or-less with the logical breakdown above, and is proposed for the project. Projects are listed within their category and linked to an existing project below, where applicable. In some cases, placeholder projects are listed with names which are subject to change. It is proposed that this structure reside within a <code>/cvsroot/modeling</code> repository location at dev.eclipse.org:</p>

		
		<h4><b>Eclipse Modeling Project</b></h4>
		<ul>
			<li>Metamodeling Facilities
				<ul>
					<li>Eclipse Modeling Framework (EMF)</li>
					<li>Validation Framework (VF)</li>
					<li>Model Query (MQ)</li>
				</ul>
			</li>
			<li>Model Notation Development
				<ul>
					<li>Graphical Modeling Framework (GMF)</li>
					<li>Textual Model Notation (TMN)</li>
				</ul>
			</li>
			<li>Model Transformation Frameworks
				<ul>
					<li>Generative Model Transformer (GMT) [not yet contacted]</li>
					<li>Model Query, View, Transformation (MQVT)</li>
				</ul>
			</li>
			<li>Model to Text Frameworks
				<ul>
					<li>Java Emitter Templates (JET)</li>
					<li>MOF to Text (MOF2T)</li>
				</ul>
			</li>	
			<li>Model-Driven Data integration (MDDi) [not yet contacted]</li>
			<li>Industry Models
				<ul>
					<li>Unified Modeling Language (UML)</li>
					<li>XML Schema Infoset Model (XSD)</li>
					<li>Service Data Objects (SDO)</li>
					<li>Object Constraint Language (OCL)</li>
				</ul>
			</li>
		</ul>		
		
		<table>
			<tr>
				<th align="left">Project</th>
				<th align="left">Name</th>
				<th align="left">Description</th>
				<th align="left">Impact</th>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/emf">EMF</a></td>
				<td valign="top">Eclipse Modeling Framework</td>
				<td valign="top">EMF is a modeling framework and code generation facility for building tools and other applications based on structured data models.</td>
				<td valign="top">Migration from Tools project. To be expanded to include visual and textual concrete syntax and editor support.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/uml2">UML</a></td>
				<td valign="top">Unified Modeling Language</td>
				<td valign="top">The UML project is an EMF-based implementation of the OMG's Unified Modeling Language specification.</td>
				<td valign="top">Migration from Tools project. To be expanded to include a visual modeling capability and textual notation (HUTN) support. Name adjusted to eliminate version number.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/xsd">XSD</a></td>
				<td valign="top">XML Schema Infoset Model</td>
				<td valign="top">XSD is a library that provides an <A href="http://download.eclipse.org/tools/emf/xsd/javadoc?org/eclipse/xsd/package-summary.html#details">API</A> for manipulating the components of an XML Schema as described by the <A href="http://www.w3.org/TR/XMLSchema-0">W3C XML Schema</A> specifications, as well as an API for manipulating the DOM-accessible representation of XML.</td>
				<td valign="top">XSD is currently part of EMF and will become its own project.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/sdo">SDO</a></td>
				<td valign="top">Service Data Objects</td>
				<td valign="top">SDO is a framework that simplifies and unifies data application development in a service oriented architecture (SOA).</td>
				<td valign="top">SDO is currently part of EMF and will become its own project.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/emf">JET</a></td>
				<td valign="top">Java Emitter Template</td>
				<td valign="top">JET provides JSP-like templating technology.</td>
				<td valign="top">JET is currently part of EMF and will become its own project within the M2T category.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/gmf">GMF</a></td>
				<td valign="top">Graphical Modeling Framework</td>
				<td valign="top">GMF provides the fundamental infrastructure and components for developing visual design and modeling surfaces in Eclipse.</td>
				<td valign="top">GMF will move from the Technology project.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/mddi">MDDi</a></td>
				<td valign="top">Model Driven Development integration</td>
				<td valign="top">MDDi allows developers to integrate modeling technologies and their supporting tools in a dedicated MDD platform on top of Eclipse.</td>
				<td valign="top">MDDi will move from the Technology project.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/gmt">GMT</a></td>
				<td valign="top">Generative Model Transformer</td>
				<td valign="top">GMT aims to produce a set of research tools in the area of MDSD (Model Driven Software Development).</td>
				<td valign="top">GMT will move from the Technology project.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/emft">OCL</a></td>
				<td valign="top">Object Constraint Language</td>
				<td valign="top">OCL is an implementation of the OCL OMG standard for EMF models.</td>
				<td valign="top">OCL is currently part of EMFT and will become its own project.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/emft">VF</a></td>
				<td valign="top">Validation Framework</td>
				<td valign="top">VF is an extended validation framework for EMF models.</td>
				<td valign="top">VF is currently part of EMFT and will become its own project.</td>
			</tr>
			<tr>
				<td valign="top"><a href="http://www.eclipse.org/emft">OCL</a></td>
				<td valign="top">EMF Query</td>
				<td valign="top">EMFQ is a query framework for EMF models.</td>
				<td valign="top">EMFQ is currently part of EMFT and will become its own project.</td>
			</tr>
		</table>

	<h4>Incubators</h4>
		<p>The Modeling Project will provide for the incubation of new modeling-related technologies and projects. Enhancements and extensions to current frameworks, in addition to the inclusion of new Domain-Specific Languages and industry standards are all possible causes for a new incubator within the Modeling Project.</p>
		

		<h3>Interested Parties</h3>
		<p>To express support, concern, or constructive opinion regarding the formation of this proposed top-level Modeling Project, all are encouraged to utilize the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.modeling">newsgroup</a>.
<p><b>Project Management Committee </b></p>

<ul>
   <li>Richard Gronback, Borland (co-lead)</li>
   <li>Ed Merks, IBM (co-lead)</li>
   <li>Kenn Hussey, IBM</li>
   <li>Fred Plante, IBM</li>
</ul>
<p>&nbsp;</p>

	</div>
</div>

<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
