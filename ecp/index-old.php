<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Proposal for Enterprise Component Platform";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
      <div id="midcolumn">
        <h1>
          Proposal for Enterprise Component Platform
        </h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Enterprise Component Platform");
?>
        <p>You are invited to join the project and to provide feedback using the
        <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ecp">eclipse.technology.ecp</a>
        newsgroup.&nbsp;</p>
        <h2>Introduction&nbsp;</h2>
        <p>The outstanding flexibility of the Eclipse Platform is provided by
        its comprehensive component model. The Eclipse Platform component model
        targets tools development, tools integration and rich client
        applications and offers users a strong GUI orientation. Unfortunately,
        there is nothing similar to the Eclipse Platform available for
        enterprise application developers. Existing components models do not
        provide the flexibility for <a href="http://en.wikipedia.org/wiki/Enterprise_application">enterprise
        applications</a> that the Eclipse Platform does for GUI-oriented
        applications.&nbsp;</p>
        <p>The Enterprise Component Platform Project's goal is to create a
        comprehensive component model for enterprise application development.
        The component model will provide a great deal of flexibility for
        building enterprise applications from components. In addition,
        applications would benefit from the component approach by acquiring such
        feature as adaptability e.g. ability to react on changes in environment
        at runtime. Finally, the project will create the ability to easily
        create enterprise application from existing components for development
        teams and users.</p>
        <p>A majority of the flexibility the Eclipse Platform inherited from the
        OSGi Service Platform. Similarly, the Enterprise Component Platform
        should also be based on and extend the OSGi Service Platform. The
        Equinox along with Eclipse Core will serve as a container for the
        enterprise components.&nbsp;</p>
        <p>This document proposes the scope and organization of the
        project.&nbsp;</p>
        <h2>Why A New Enterprise Component Model?&nbsp;</h2>
        <p>It is important to explore the specific market need for yet another
        component framework. By definition, the word, &quot;component&quot; is
        today much overloaded and overused. However, in this project, the
        following definition will be used: A component is a software unit that
        exposes the following features:&nbsp;</p>
        <ul>
          <li>Has well specified interface and explicit dependencies&nbsp;</li>
          <li>Can be customized&nbsp;</li>
          <li>Can be assembled&nbsp;</li>
          <li>Reusable by nature&nbsp;</li>
          <li>A unit of substitution&nbsp;</li>
          <li>A unit of delivery and deployment</li>
        </ul>
        <p>The primary reason for utilizing components is to provide a faster
        way to develop applications due to their high volume of reuse. In
        addition, components help to maintain existing application because of
        its customizability and substitutability. The following section
        describes the current market challenges that limit the usage of a
        component approach to develop enterprise applications and our
        propositions how to solve them.&nbsp;</p>
        <h2>Reusability&nbsp;</h2>
        <p>Today, there are no wide Java enterprise components on the market.
        The primary problem with this lack of diversity in the existing
        component models is that component models are mostly interface or
        object-oriented. They do not take in consideration that modern
        programming technologies require developers to use many different
        software artifacts such as services, resources, declarative programming
        specifications or object/relational mappings. Developers can't expose
        these artifacts as components because they are beyond of a component
        model. As a result, applications become monolithic because all of the
        artifacts are integrated into one big module such as war or ear.</p>
        <p>The Enterprise Component Platform project proposes a component model
        based on OSGi bundles that can contain different types of components and
        XML descriptors that describe these components in terms of exposed
        artifacts and required artifacts. Such components can be reused without
        internal modifications but by customization in using components. Eclipse
        plug-ins are a good example of such an approach.&nbsp;</p>
        <h2>Adaptability&nbsp;</h2>
        <p>All commonly used existing component models today are static (e.g
        components are supposed to be installed and work all the time). These
        component models do not define behavior of components when there are
        some changes are under way within runtime environment. It makes
        impossible or very difficult to maintain operational applications.
        Usually, applications based on a static component model need to be
        stopped, reconfigured or updated and then launched again.</p>
        <p>We propose a model that allows dynamic changes in the environment.
        Moreover applications can benefit from it by acquiring adaptability. In
        the same way as Eclipse IDE can be extended by plug-ins the proposed
        model allows extending enterprise applications.</p>
        <h2>Flexible Target Platform&nbsp;</h2>
        <p>The proposed component model will allow enterprise developers to
        create enterprise components without targeting any specific environment
        such as J2EE or J2SE. As a result, enterprise components can run both on
        server or client sides. Theoretically, the enterprise components can be
        deployed anywhere from a cell phone to grid infrastructure. The only
        limitation is possibility of the environment to satisfy the component
        dependencies.&nbsp;</p>
        <h2>Versioning&nbsp;</h2>
        <p>The proposed component model will have versioning will rely on the
        OSGi Platform for components versioning. As a result it will allow great
        level of adaptability for existing applications&nbsp;</p>
        <h2>High level architecture&nbsp;</h2>
        <p>This technical diagram illustrates the architecture of how the
        Eclipse with the Enterprise Components may become a complete platform
        for building business applications.</p>
        <p><img border="0" src="highlevel.gif" width="577" height="302"></p>
        <p>The architecture is comprised of the following:</p>
        <p><b>Java Platform </b>- the Java virtual machine plus the API
        specifications</p>
        <p><b>Eclipse OSGi Framework (Equinox) </b>- The Equinox provides the
        OSGi framework implementation and component model upon which the Eclipse
        RCP, IDE platforms and Enterprise Component Platform run</p>
        <p><b>Eclipse RCP </b>- Eclipse Rich Client Platform - The set of
        plug-ins needed to build a rich client application</p>
        <p><b>Enterprise Component Platform </b>- The proposed platform for
        building enterprise application components. Unlike enterprise components
        that run on server-side of the business applications, this Platform has
        no such requirement. Enterprise Components may run in the same container
        as RCP components.&nbsp;</p>
        <p><b>Enterprise Components, Rich Client Component, Eclipse Plug-ins </b>-
        The building blocks of the business applications and tools</p>
        <p><b>Business Application </b>- Any application that performs business
        functions and utilizes a GUI</p>
        <h2>Roles and responsibilities&nbsp;</h2>
        <p>We understand that the proposed platform will change the way how
        business applications are developed. We propose to recognize the
        following roles in software development process with the Enterprise
        components: technology providers, component providers and application
        developers.&nbsp;</p>
        <h3>Technology provider&nbsp;</h3>
        <p>Technology providers bundle their products into OSGi bundles. Some
        technology components may be exposed as enterprise components.
        Technology providers must have substantial knowledge of the OSGi
        platform. Some knowledge of the Enterprise Component Platform may be
        required.&nbsp;</p>
        <h3>Component provider&nbsp;</h3>
        <p>Component providers create business components that can be used to
        build applications. Component providers must have basic knowledge of
        OSGi platform and knowledge of Enterprise Components model (must know
        what component descriptors are). Components are developed using commonly
        used Java technologies (JavaBeans, JSP, JSF etc.). Component bundles
        contain Enterprise Component descriptor(s) if necessary.&nbsp;</p>
        <h3>Application developer&nbsp;</h3>
        <p>Application developers customize and assemble business components
        together to satisfy end users needs. Application developers must be
        familiar with OSGi platform (must know what bundle and service is) and
        Enterprise Component model (must know what component descriptors are).
        Application developers use Enterprise Component Development Tools to
        compose and customize applications.</p>
        <p><img border="0" src="roles.gif" width="468" height="881"></p>
        <h2>Project Scope&nbsp;</h2>
        <p>The Enterprise Component Platform Project will focus on the
        development of an enterprise component model, framework and common
        services that support this model and set of tools to support development
        and runtime management of enterprise components.</p>
        <p>The items below describe the scope of the proposed Enterprise
        Component Platform project.&nbsp;</p>
        <h3>Enterprise Component Model&nbsp;</h3>
        <p>The following principles should be employed during the enterprise
        component model development:&nbsp;</p>
        <ul>
          <li>The component model should be based on the OSGi Service
            Platform.&nbsp;</li>
          <li>The component model should support multiple software artifacts
            such as resources, JavaBeans, OR mappings and process definitions in
            the same way as service components are supported by Service
            Component Runtime (see OSGi Service compendium: 112 Declarative
            Services Specification)&nbsp;</li>
          <li>The component model should specify dynamic behavior of the
            components. The components should adapt to the dynamic availability
            (the arrival or departure) of the services or other components they
            are using&nbsp;</li>
          <li>The component model should employ XML descriptors (non-intrusive
            model) and/or Java 5 annotations to describe, customize and
            assembling components&nbsp;</li>
          <li>The component model should allow converting existing software
            artifacts into enterprise components without any modifications in
            the artifacts&nbsp;</li>
        </ul>
        <h3>Enterprise Component Framework&nbsp;</h3>
        <p>The project will specify and develop a framework and runtime services
        that support and enforce the enterprise component model. The framework
        would include XML descriptor definitions for every component type
        defined by the component model, descriptor parsers, runtime support for
        registering/un-registering components and component dependencies
        resolving. In addition, the framework would provide a way to develop
        component applications that are sets of components that work together
        within the same context.&nbsp;</p>
        <h3>Enterprise Component Tools&nbsp;</h3>
        <p>The project goal is to provide a set of tools for the enterprise
        component development and runtime component management. Such tools would
        include managing lists of installed components, component dependencies
        resolving and validation, search/install/publish components from/into
        the component repository, wizards for component creation, import/export
        component project from/to a bundle, conversion software artifacts into a
        components, editors for component descriptors.&nbsp;</p>
        <h3>Enterprise Component Repository&nbsp;</h3>
        <p>The Enterprise Component Repository is a service that provides
        storage for components. The Component Repository would also provide
        search and retrieval of components. An optional runtime service would
        provide automatic discovery, download, install and activation of
        required components from the repository.&nbsp;</p>
        <h2>Out of scope&nbsp;</h2>
        <p>The initial project would not focus on compatibility with the
        existing enterprise component models such as EJB or currently developing
        such as SCA. Nevertheless, components that provide such services could
        be created within the boundaries of the basic enterprise component
        model.</p>
        <p>The project would not focus on a security model. The security model
        is defined in OSGi Service Platform specification.</p>
        <p>The project would not focus on packaging and deployment. The OSGi
        Service Platform specification defines module layer for these purposes.</p>
        <p>The project would not define any specific component set that should
        be preinstalled at runtime. Only component dependencies can specify what
        is required at runtime.&nbsp;</p>
        <h2>Relationship with other Eclipse Projects&nbsp;</h2>
        <p>The Enterprise Component Framework will strongly rely on Equinox. The
        tool set will be built upon the Eclipse Platform as set of plug-ins.
        Project may have a relationship with RCP for UI components.&nbsp;</p>
        <h2>Interested Parties&nbsp;</h2>
        <p>There are two major interested parties in this project - potential
        contributors and potential adopters. The project leads have talked to
        many companies have expressed their interest in participating to this
        project as potential adopters: AIG, HSN, ABN AMRO Bank, Deutsche Bank,
        CNet and Echopass. To express support, concern, or constructive opinion
        regarding the formation of this proposed project, all are encouraged to
        utilize the newsgroup.&nbsp;</p>
        <h3>Initial committers&nbsp;</h3>
        <ul>
          <li>Igor Shabalov, Exadel, Project Lead&nbsp;</li>
          <li>Alex Antonau, Exadel, Project Architect and Committer&nbsp;</li>
          <li>Nick Belaevski, Exadel, Committer&nbsp;</li>
          <li>Andrew Yanchevsky, Exadel, Committer&nbsp;</li>
        </ul>
        <h3>Code contributions&nbsp;</h3>
        <p>Initial code contribution will be based on Exadel development of
        Eclipse-based Components Framework. We also encourage other parties to
        contribute Enterprise Components based on exiting and new open source
        technologies.<br />
      </div>
    </div>
   
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>