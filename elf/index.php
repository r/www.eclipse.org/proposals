<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "Dann Martens";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Logic Framework");
?>

<h1>Eclipse Logic Framework (ELF)</h1>
 
<h2>Introduction</h2> 
 
<p>Eclipse Logic Framework (ELF) is a proposed open source project under the 
Eclipse Technology Project.</p> 
<p>This proposal is in the Project Proposal Phase (as defined in the Eclipse 
Development Process document) and is written to declare its intent and scope. 
This proposal is written to solicit additional participation and input 
from the Eclipse community. You are invited to comment on and/or 
join the project. Please send all feedback to 
the <a href="http://www.eclipse.org/forums/eclipse.elf">ELF forum</a>.</p> 
 
<h2>Scope</h2> 

<p>Prolog (PROgramming LOGic) was developed in 1972 (although sources vary) by 
Alain Colmerauer and Phillipe Roussel, both of University of Aix-Marseille, 
France, in collaboration with Robert Kowalski of the University of Edinburgh, 
United Kingdom. While popular in the Artificial Intelligence community, which 
used it amongst other things for the development of expert systems and natural 
language processing, its significance has waxed and waned over time. Even though the 
language has been ISO standardized in 1995, it has never broken through in 
the computer industry in the way other languages have, especially when compared 
to the now ubiquitous Java, developed by James Gosling at Sun Microsystems 
and released in 1995.</p> 
<p>In 1996, Bart Demoen of the Katholieke Universiteit Leuven, Belgium, and Paul Tarau, 
at that time at the Universit� de Moncton, Canada, outlined a method to run Prolog 
programs on a Java virtual machine. A limited proof-of-concept implementation was 
made available under the moniker 'jProlog'. The idea was picked up again soon after 
by Mutsunori Banbara and Naoyuki Tamura of Kobe University, Japan, and developed 
into a feature-complete clean-room implementation under the moniker 
'Prolog Cafe' (http://kaminari.istc.kobe-u.ac.jp/PrologCafe/), of which the 
last update dates June 24th 2009.</p>
<p>In 2002, Tobias Rho created the �Prolog Development Tool� (PDT), 
the first Eclipse-based IDE for working with SWI-Prolog, as part of his 
diploma thesis at the University of Bonn. Since then the PDT project has 
continuously evolved in its academic environment, led by Dr. G�nter Kniesel, 
with contributions of Tobias Rho, Lukas Degener and Eva St�we. The PDT
 will be subsumed by the ELF, but for now the original project can still be
found at http://roots.iai.uni-bonn.de/research/pdt/.</p>
<p>With the advent of the Semantic Web, a new spin was put on the A.I. research 
accomplishments of the last 50 years. While Tim Berners Lee contended in 1998 
that a Semantic Web is not Artificial Intelligence, he did call it 
a 'language of logic'. Logic languages were designed with the specific 
purpose of making the expression of the solution to problems straightforward. 
Yet, Semantic Technologies are developed using popular contemporary languages, 
all of which are notoriously ill-suited for that very same purpose. Equally true, 
those (early) logic languages have become sidelined because they made many 
real-world everyday-programming tasks cumbersome.</p>
<p>The ELF represents a renewed interest in the pursuit of practical logic 
programming.</p>
 
<h2>Description</h2> 
 
<p>The Eclipse Logic Framework project will provide a complete Prolog 
development and run-time environment, adding Prolog to the existing Eclipse 
ecosystem of supported languages:
<ul>
<li>The IDE will support editing, code navigation and refactoring of Prolog code. 
It will be configurable to support external native Prolog run-time engines 
(such as SWI-Prolog) and the project's own internal run-time.</li> 
<li>The internal run-time will allow running Prolog programs on the Java Virtual 
Machine, providing the basis for a seamless integration of Prolog and Java in 
a multi-paradigm approach that lets developers use the best of each language 
for each particular task.</li>
</ul>
</p>
<p>As a platform, the ELF will provide the means to integrate with results from 
specific areas of research into the Prolog language. It will invite researchers 
and tool makers from the logic programming and multi-paradigm programming 
community to actively contribute to the development and critical evaluation of 
the project.</p>

<h2>Value Proposition</h2>

<p><strong>Industrial:</strong>&nbsp;While the ELF project recognizes its 
academic origins, the emphasis is on the development of an industry-grade, 
production-ready product. The project is aimed at developers and researchers 
who can benefit from logic programming solutions in general. Beyond that, it 
will provide a unique platform for novel families of logic-based applications 
that can take advantage from running on a Java virtual machine. The project 
provides complete support for the Equinox OSGi framework and Google App Engine 
for Java (GAE/J).</p>
<p><strong>Educational:</strong>&nbsp;Because it offers an all-in-one Prolog IDE, 
the ELF has the potential to become the tool-of-choice for educational purposes. 
It lowers the barrier for entry into the realm of logic programming. By supporting
multiple run-times out-of-the-box, teachers and students will be able to focus 
on the language issues, as opposed to the installation issues.</p>
<p><strong>Semantic:</strong>&nbsp;The ELF will increase the presence of 
the Eclipse community at the Semantic Technologies frontier and will leverage 
development of Semantic Web applications on the Java platform. It will prove
especially relevant in the area of 'Logic Frameworks' in the Semantic Web
Technology Stack.</p> 

<h2>Proposed Components</h2> 
 
<p>The project's initial target is the introduction of the following components:
<ol> 
<li>a set of IDE contributions to edit Prolog source files which meet the expected standards of well-known, existing Eclipse IDE projects,</li>
<li>a connector framework to interact with native Prolog run-time engines and interpreters,</li>
<li>a Java-based Prolog interpreter, which runs without the need of a native Prolog run-time environment,</li> 
<li>a Prolog-to-Java source-to-source translator system,</li> 
<li>a high-performance Java-based Prolog run-time,</li> 
<li>a 'Java Architecture for Prolog Binding' (JAPB) API to facilitate integration with pure Java programs,</li> 
<li>a set of built-in example projects.</li> 
</ol> 
</p> 
<p>Beyond the scope of the initial target, the following components are under consideration:
<ul> 
<li>support for Common Logic (CL) data formats: the Import Common Logic Interchange Format (CLIF), for the Conceptual Graph Interchange Format (CGIF) and the XML-based notation for Common Logic (XCL),</li> 
<li>support for Semantic Web data formats: the Resource Description Framework (RDF) and the Ontology Web Language (OWL).</li> 
</ul> 
</p> 
 
<h2>Relationship with other Eclipse Projects</h2> 
 
<p>The Eclipse Logic Framework project will be built on top of the Eclipse IDE. 
Although the Prolog IDE will stand on its own, the Java-specific hybrid language 
aspects of the ELF will depend on JDT components in order to function.</p> 
<p>This project has the potential to leverage the 'Eclipse IDE For Education' (IDE4EDU) project, by providing a Prolog IDE and run-time announced but unavailable at the time of this writing.</p> 
 
<h2>Organization</h2> 
 
<p>This project requests a place in the Eclipse Tools hierarchy as it facilitates programming in Prolog, and Java in combination with Prolog.</p> 
<p>While the main driver is a commercial entity, the product shall remain absolutely free and without restrictions. In addition, this project will strive to develop a working relationship with the academic world, and where possible, integrate existing and ongoing development efforts of academic origins.</p>   
 
<p>Mentors<br>
TBD</p>

 
<p>Proposed initial committers:
<ul> 
<li>TOMOTON N.V., Belgium - Dann Martens (Project Lead)</li>
<li>Bonn University, Germany - G�nter Kniesel (Project Lead)</li>
<li>Bonn University, Germany � Tobias Rho</li>
<li>Bonn University, Germany � Eva St�we</li>
</ul> 
</p> 
 
<h2>Interested parties</h2> 
 
<p>The following academic institutions have expressed interest in this project. Key contacts listed.
<ul> 
<li>Kobe University, Japan - Mutsunori Banbara</li> 
<li>Kobe University, Japan - Naoyuki Tamura</li> 
<li>Eclipse IDE for Education</li>
<li>Dwight Duego</li>
</ul>    
</p> 
 
<h2>Code Contributions</h2> 
 
<p>The Kobe University, Japan, has contributed the Prolog-to-Java source-to-source 
translator system. The individual contributors involved in the development of 
the original code base were Mutsunori Banbara and Naoyuki Tamura.</p> 
<p>Bonn University, Germany, has contributed the Prolog IDE and parts, from the 
externally hosted Prolog Development Tools (PDT) project. The code base was 
originally licensed under the Eclipse Public License. The individual 
contributors involved in the development of the original code base were Lukas 
Degener, Tobias Rho, Eva St�we and Frank M�hlschlegel.</p>
<p>TOMOTON, NV, Belgium, has requested the Kobe University to dual licence the 
translator system under the Eclipse License, next to the GNU General 
Public License as published by the Free Software Foundation (version 2 
of the License). In addition, TOMOTON has modified the existing code base to 
allow the run-time to work both in an OSGi container, as well as in the 
sandbox of Google App Engine for Java. TOMOTON NV has designed and 
developed the JAPB API. The individual contributor involved in the 
modification and separate development was Dann Martens.</p> 
 
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>