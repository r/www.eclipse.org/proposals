<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
	$App 	= new App();
	$Nav	= new Nav();
	$Menu 	= new Menu();
	include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
	
	$pageTitle 		= "SWTBot proposal";
	$pageKeywords	= "swtbot, proposal, functional testing";
	$pageAuthor		= "Ketan Padegaonkar [at] gmail [dot] com";

	ob_start();
?>

<div id="maincontent">
<div id="midcolumn">

<h1>SWTBot -- Functional Testing for SWT and Eclipse</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("SWTBot");
?>

<p>
	<h2>Introduction</h2>
	<a href="http://swtbot.org">SWTBot</a> is a functional testing tool for SWT/Eclipse based applications that eases and supports testing of multithreaded applications. SWTBot is capable of playback and recording of SWTBot Java "scripts", there are plans to provide scripting support for various other languages including <a href="http://jruby.codehaus.org">JRuby</a>, and <a href="http://groovy.codehaus.org/">Groovy</a>.
	<br/><br/>
	SWTBot provides an API that allows a developer to rigorously test his application functionally. SWTBot enables a developers to write tests that would interact with the application's user interface.
	<br/><br/>
	SWTBot is proposed as an open source project under the <a href="http://www.eclipse.org/technology/">Eclipse Technology Project</a>. This proposal is still in the Project Proposal Phase, and is being made in order to call for more community participation. You are invited to comment on and/or join the project. Please send all feedback to the<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.swtbot"> http://www.eclipse.org/newsportal/thread.php?group=eclipse.swtbot</a> newsgroup.
	<h2>Background and Goal</h2>
	The development of applications based on SWT, RCP and the Eclipse platform in general is growing fast. With complex desktop applications increasingly using the eclipse platform, manual testing of these applications becomes a major concern in the development lifecycle of the application. The need for automated testing is inevitable in such cases and SWTBot helps projects in such cases.
<br/><br/>
	Automated testing code needs the same love and affection that code does. This means better IDE support for writing tests, reusing test code, refactoring, etc. Simplifying this test code so that it is agnostic of SWT and Eclipse and deals with UI paradigms would be appealing to non-developers and would help address this issue to a large extent.
<br/><br/>
	The goal of SWTBot is to provide APIs that can be used by programmers to drive SWT based UIs.
<br/><br/>
	SWTBot has demonstrated some of these <a href="http://swtbot.sourceforge.net/screencasts/SWTBotEclipseHelloWorld-FullScreen.html">capabilities</a> very early on, and is being supported by a few contributors.
<br/><br/>
The goal of SWTBot is to provide APIs that can be used by programmers to drive SWT based UIs by providing a lightweight functional testing tool for SWT and Eclipse, and to make it easy for non-developers to automate testing of applications written using Eclipse in a way that is consistent with what has been stated above.
	<h2>Architecture and Extensibility</h2>
	SWTBot provides a foundation that is responsible for locating SWT and Eclipse widgets. Providing this foundation offers an extensible framework to build upon. This framework already includes the ability to find many of the SWT widgets. However it could be extended to add support for nebula items, eclipse forms, eRCP, GEF and possibly even AWT/Swing.
	<br/><br/>
	SWTBot's method for finding widgets can also be greatly extended to support locating other types of items. Matchers provide the rules for finding widgets and can be created or extended to advance the matching techniques.
	<br/><br/>
	Finally, SWTBot provides wrappers around widgets to encapsulate the operations and querying of state for the given widget. Since many widgets share functionality these wrappers provides new widgets an excellent starting point in most cases.
	<h2>Project Scope</h2>
	The scope of SWTBot is to make writing tests for SWT and Eclipse applications easier not just for developers who understand the technologies, but also for Quality Analysts who understand the application but not the underlying technologies used to build them.
	<br/><br/>
	SWTBot should integrate well with JUnit so as to make execution and reporting of tests independent of Eclipse, yet integrate well with Eclipse. This would mean that SWTBot tests can be run as an Ant task as part of Continuous Integration using CruiseControl.
	<br/><br/>
	There are also efforts by individuals to port SWTBot to eRCP/eSWT.
	<h2>Future Roadmap</h2>
	The immediate priorities for SWTBot in order of importance are:
	<ol>
		<li>Better support for eclipse contributions: view, menu, toolbar, editor contributions.</li>
		<li>Support for eclipse-forms</li>
		<li>Support for Nebula</li>
		<li>Support for GEF</li>
	</ol>
	To make writing scripts easier, it is also proposed to provide better Eclipse integration, introspection of the application, and basic support for scripting/debugging.
	<br/><br/>	
	It is proposed that SWTBot should provide APIs that enable scripting languages based on the JVM. This would mean that scripts could potentially be written with JRuby using <a href="http://www.eclipse.org/proposals/glimmer/">Glimmer</a> or with Groovy using <a href="http://groovy.codehaus.org/GroovySWT">GroovySWT</a>.
	<h2>Concerns</h2>
	There is currently some of overlap between SWTBot and TPTP's AGR. TPTP supports Eclipse plugins, but does not support plain SWT applications, nor easy Ant integration. SWTBot's focus is to become a low level driver to drive SWT and Eclipse applications while filling these shortcomings of AGR.
	
	<h2>Packaging and Deployment</h2>
	SWTBot currently ships in the form of an Eclipse update site, and also an independent .zip/.tgz download. It would continue to ship in these forms.

	<h2>Organization</h2>

	<h3>Mentors</h3>
	<ul>
	<li>Chris Aniszczyk</li>
	<li>Ed Merks</li>
	</ul>

	<h3>Initial committers</h3>
	<ul>
	<li>Ketan Padegaonkar (ketanpadegaonkar at gmail)</li>
	<li>Lechner Sami (lechner.sami at gmail)</li>
	</ul>

	<h3>Potential committers</h3> (individuals who have contributed patches)
	<ul>
	<li>Cedric Chabanois (cedric.chabanois at entropysoft.net)</li>
	</ul>

	<h3>Interested parties</h3>
	<ul>
	<li><a href="http://eclipse.org/pde">PDE</a> (Chris Aniszczyk)</li>
	<li>ThoughtWorks Studios (<a href="http://studios.thoughtworks.com">ThoughtWorks Studios</a>)</li>
	<li>Nirav Thaker (nirav.thaker at gmail)</li>
	<li>Ravi Chodavarapu (rchodava at gmail.com)</li>
	<li>Stefan Seelmann, Apache Directory Studio (seelmann at apache.org)</li>
	<li>ANCiT Consulting, (Annamalai C, annamalai at ancitconsulting.com)</li>
	<li>Ben Xu (xufengbing at gmail.com)</li>
	<li>Mirko Stocker (me at misto.ch)</li>
	<li>Emanuel Graf (egraf at hsr.ch)</li>
	<li><a href="http://eclipse.org/rap">RAP</a> (Benjamin Muskalla)</li>
	<li>Sriram Narayanan (ram at thoughtworks.com)</li>
	</ul>
</p>

</div>
</div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
