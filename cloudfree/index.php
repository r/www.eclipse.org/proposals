<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "CloudFree Commerce Platform Proposal";
$pageKeywords	= "cloudfree, commerce, platform, osgi, equinox, project, proposal";
$pageAuthor		= "Gunnar Wagenknecht, AGETO Service GmbH";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>CloudFree Commerce Platform</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("CloudFree");
?>

<p>
This project proposal is in the 
<a href="http://www.eclipse.org/org/documents/Eclipse%20Development%20Process%202003_11_09%20FINAL.pdf">Proposal Phase</a> 
and is posted here to solicit community feedback, additional project
participation, and ways the project can be leveraged from the Eclipse
membership-at-large. You are invited to comment on and/or join the project.
Please send all feedback to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.cloudfree" title="eclipse.cloudfree newsgroup">eclipse.cloudfree newsgroup</a>.
</p>

<h2>Background</h2>
<p>
	E-commerce has become an integral part of today�s ecosystem and is 
	material to many organizations businesses. It is one of the fastest 
	growing sectors in the Internet. More and more organizations 
	(manufacturers, retailers, education, travel and many others) are looking 
	at e-commerce as a cost effective and reliable channel to sell goods and 
	services to businesses and consumers. Unlike a few years ago, the 
	e-commerce initiatives are no longer seen as side track or pilots. As 
	the Internet usage grows exponentially with increasing bandwidths and 
	more and more Internet enabled devices, the development of integrated, 
	extendable e-commerce platforms is becoming critical to future growth 
	for any organization. This includes not only traditional business, but 
	also public organizations and governments, any educational organization, 
	or even charity. Everywhere an Internet transaction is performed, it is 
	safe to talk about "e-commerce" now. 
</p>
<p>
	While the past adoption of e-commerce was determined by the 
	availability of "on-premise", proprietary software solutions developed 
	by a handful software vendors, today�s vendor landscape looks different: 
	It spans from low end, easy to deploy yet inflexible open source 
	solutions to ISP based "SaaS" offerings. Which seems to be good a good 
	fit for some user groups is certainly a thread for others who want to 
	explore new business opportunities outside of their vendor's radar. 
	Coming from such a background, we recognize the need for a dynamic, 
	flexible and extensible platform which is capable of hosting e-commerce 
	sites varying from a few to thousands of visitors and transactions per 
	day. Flexibility and extensibility in today's e-commerce depends on a 
	software vendor's roadmap, which is often determined by "the least 
	common denominator" of potential users. Quick, unconditional 
	provisioning of new features, feature updates or custom extensions 
	- preferably with zero downtime - is hard to find. 
</p>
<p>
	Organizations who seek new, competitive e-commerce processes do 
	experience that adopting e-commerce is not as easy as download, unzip 
	and run. Besides sticking to the vendor's product strategy and having to 
	pay extensive maintenance fees, many custom requirements can be 
	integrated by finding a compromise between the software vendor's feature 
	roadmap and their own requirements. Having said this, if organizations want to 
	leverage the R&amp;D efforts and development of current e-commerce 
	vendors of any kind, they will certainly accept these compromises. But 
	organizations who want to explore innovative e-commerce initiatives and 
	at the same time rely on standardized components supported by a large 
	user group will barely find a solution today. They have to develop for 
	themselves a custom and proprietary solution which has high 
	maintenance efforts and will be constrained by high ongoing development 
	efforts. 
</p>
<p>
	While we have discussed the different user groups, also the bespoken 
	software vendors are struggling with legacy platforms which are hard to 
	maintain, extend and customize. Seeking a new technology platform to 
	develop a product offering, whether it is SaaS or on-premise, is a 
	gamble. What is the hottest technology today, can be outdated tomorrow. 
	Technology platform vendors coming and going and what seems to be a wide 
	choice is finally a selection between a couple of big software vendors 
	with very high fees, slow feature adoption and no interest in 
	e-commerce vendor specific needs. There is a development environment 
	needed, which allows e-commerce software vendors to develop their own suites 
	of products for their client base, combining it with other 
	technology and providing the right "runtime" environment for custom 
	development, high performance and scalability with low cost operations 
	and maintenance. 
</p>
<p>
	The Equinox Runtime provides a highly dynamic environment for building 
	component oriented applications. Equinox and CODA (Component Oriented 
	Development and Assembly) is a proven component model which is also 
	perfectly suited for building flexible, dynamic and extensible 
	enterprise solutions. We therefore aim to bring this model into the 
	e-commerce domain. E-commerce has been and still is one of the fastest 
	growing sectors in the Internet.
</p>

<h2>Description</h2>
<p>
	The CloudFree Commerce Platform is an open, dynamic and extensible 
	platform for e-commerce solutions of all kind. By using Equinox and CODA 
	we define a new platform that is designed from the beginning to gain 
	leverage from the key advantages of Equinox and CODA - flexibility, 
	extensibility and community. 
</p>
<p>
	CloudFree will support small, to medium, to large e-commerce sites by 
	providing a scalable architecture. It will be useful for standard 
	e-commerce shops to sell and merchandise goods as well as any public 
	organization who wants to leverage the Internet as a communication and 
	transaction channel, and for any organization who encourages their (potential) 
	users to perform a transaction, whether this is an order, an 
	application, online registration or content download provisioning. It 
	will be accessible and easy to use for small organizations, large 
	enterprises with multi-dimensional e-commerce initiatives or as a
	technology platform for software vendors. It will allow users to build and 
	publish third party components which can be used and refined by all 
	associated users. 
</p>
<p>
	No focus is set on a specific feature implementation technology. The 
	CloudFree concept will be extensible and pluggable so that it can be 
	exchanged and adapted from a standalone server deployment to a 
	large-scale enterprise cluster consisting of many nodes spread across 
	different locations. It grows with the user's needs and can be maintained 
	by anyone who is familiar with the Eclipse environment. By that it gets 
	the large support of the community to conquer the most complex 
	e-commerce scenarios and use cases. Organizations who decide to develop 
	their e-commerce processes in CloudFree will have the development 
	standards of the Eclipse community, the ideas of many contributors and 
	the reliability of proven e-commerce modules at hand. 
</p>
<p>
	The CloudFree Commerce Platform will develop infrastructure, components, 
	and services that will support development, provisioning and operation 
	of e-commerce solutions of all kind including content transactions. The 
	essential functional elements of CloudFree 1.0 are well understood by 
	domain experts. While there is significant engineering effort required, 
	we feel that the work plan as proposed here can be implemented given 
	sufficient and experienced resources, and does not involve an 
	unrealistic level of technical risk. Consequently we propose this 
	project be considered as an incubator within the Technology top-level project. 
</p>

<h2>Project Principles</h2>
<p>
  Among the key principles on which this project has been initiated, and will be run, are the following:
  <ul>
    <li>
		<b>Leverage Eclipse Ecosystem</b> - A major goal of this project is to 
		apply the application development strengths of the Eclipse Project to 
		the e-commerce domain. The project will work closely with other Eclipse 
		project areas whenever possible to leverage the capabilities being 
		developed in those areas. 
	  <br><br>
    </li>
    <li>
		<b>Vendor Neutral</b> - We aim to encourage Eclipse participation and 
		drive Eclipse market acceptance by providing vendor-neutral capabilities 
		and to encourage participation for complementary capabilities through 
		additional projects. 
	  <br><br>
    </li>
    <li>
		<b>Open, Standards-based Development</b> - Where market adopted 
		standards exist that meet the design goals, our aim is to leverage and 
		adhere to them. Where market adopted standards do not exist, we will 
		develop and publish any new capabilities in the Eclipse open forum. 
	  <br><br>
    </li>
    <li>
		<b>Incremental Delivery</b> - In order to meet the pent-up demand for a 
		standardized platform for e-commerce solutions, we aim to deliver 
		functionality to the market as rapidly as possible via an incremental 
		delivery model. 
	  <br><br>
    </li>
	<li>
		<b>Invite Users</b> - During project execution we are going to work 
		with potential user groups to understand the current needs even better 
		and get guidelines for further priotization of work. The group will have 
		its own schedule, but will actively seek for and involve potential 
		users.
    </li>
  </ul>
</p>
<p>

<h2>CloudFree Vision</h2>
<p>
The key vision on which this platform is based upon is:
<ul>
  <li>
    <b>No Clouds</b> - It must be a pleasure to work with the platform,
    develop for and extend it, operate it and provide solutions on it.
  </li>
</ul>
</p>
<p>
Among the principles behind this vision are the following:
<ul>
  <li>
    <b>Development Modularity &amp; Extensibility</b> - Everything will
    be build as components, can be developed and deployed individually, by the
    community, project committers and partners.
	<br><br>
  </li>
  <li>
    <b>Solution Flexibility</b> - The platform, its components and
    services will be open to new markets, market changes and must adapt to
    business requirements.
	<br><br>
  </li>
  <li>
	<b>Operational Simplicity</b> - Easy provisioning into data centers 
	combined with smart and simple, but not simplistic, administration and 
	monitoring is a key element in making the platform operation a pleasure. 
	<br><br>
  </li>
  <li>
    <b>Cheap Scalability</b> - It is important to grow
    with&nbsp; clients and support a scalability model from the ground up.
	<br><br>
  </li>
  <li>
	<b>Stackability</b> - Platform, modules and components can be put 
	together in packages by third parties to allow easy deployment and 
	shipment - this saves time and effort. 
  </li>
</ul>
</p>

<h2>Project Scope</h2>
<p>
	The CloudFree Commerce Platform will develop infrastructure, components, 
	services, and tools that will support development, provisioning and 
	operation of e-commerce solutions or e-commerce development 
	environments. Development of a fully-fledged e-commerce solution is 
	out-of-scope of this project. Instead, we will focus on the runtime 
	infrastructure and core components which are essential for building 
	e-commerce solutions or e-commerce products. Additionally, exemplary 
	services will be provided which demonstrate the extensibility and 
	flexibility of the CloudFree Commerce Platform. 
</p>
<p>
The CloudFree Commerce Platform Project will initially focus on the 
following components and exemplary services. 
<ul>
  <li>
	<b>Inventory, Customer & Order Service</b> - A set of "core" services 
	typically required by e-commerce sites. These services will allow users to 
	define, manage and retrieve products, customers and orders. 
  </li>
  <li>
	<b>Exemplary Storefront Site</b> - Typically, storefronts are tailored 
	to merchant needs. They may be deeply integrated into portals and 
	provide rich dynamic content. Therefore, only an exemplary storefront 
	will be created which demonstrates the usage and integration with the 
	services provided by the platform. 
  </li>
  <li>
	<b>Contextual Runtime</b> - This is a central model for establishing a 
	"context" for building multi-tenant applications offered as SaaS. 
	Typically, certain aspects in a SaaS system will not be static. For 
	example, the availability of extension X of a service may depend on a 
	service level agreement of the tenant using that service. 
  </li>
  <li>
	<b>Clustering & Monitoring Service</b> - With scalability and 
	operational simplicity in mind, the clustering service will enable 
	multiple installations to form an application cluster and provide 
	operators with a management interface. Monitoring will be built into the 
	platform as an essential concept and will also provide integration and 
	extension points. 
  </li>
  <li>
	<b>Admin Service</b> - A central administration interface provided by 
	the platform. The service will also provide extension points to allow 
	extenders to integrate nicely with the platform. 
  </li>
  <li>
	<b>Provisioning Tool</b> - A utility for provisioning updates and new 
	components across installations in a cluster in an automated and 
	controlled way. 
  </li>
</ul>
</p>
<p>
Outside the project's scope will be the development of vendor-specific 
integrations, such as integrations with specific ERP or CRM systems, as 
well as highly specialized services, such as a sophisticated product 
recommendation service. However, the CloudFree platform will provide 
frameworks and extension points to allow third parties building such 
integrations and specialized services. 
</p>
<h2>Relationship with other Eclipse Projects</h2>
<p>
    The CloudFree Commerce Platform project will be build on top of Equinox.
    Eclipse projects which provide useful required technologies and
    functionalities, including Riena, EclipseLink, and ECF, are candidates that the
    CloudFree Commerce Platform codebase might depend upon.
</p>
<h2>Organization</h2>
<p>
    We propose this project should be undertaken as a Technology project. Being
    a Technology project gives it room to experiment without disruption to other
    Eclipse Platform development work. Finally, since Technology sub-projects
    are not meant to be ongoing, we foresee three possible evolutionary paths
    for the subproject:
  <ol>
    <li>
      The project is moved to a permanent state as a Runtime project.
    </li>
    <li>
      The project is moved to a permanent state under a new "SaaS" top-level
      project.
    </li>
    <li>
      The technology proves to be insufficient for Eclipse and the work is set
      aside.
    </li>
  </ol>
</p>
<p>
    These paths are not mutually exclusive. Some combination of some or all of
    these paths may be the result of the work done in this project.<br id=yxif>
</p>
<p>

<h2>Interested Parties</h2>
<p>
    <a href=http://www.ageto.de/ id=j43l title="AGETO Service GmbH">AGETO
    Service GmbH</a>, as the
    submitter of this proposal, welcomes interested parties to post to the
    <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.cloudfree" title="eclipse.cloudfree newsgroup">eclipse.cloudfree newsgroup</a>
	and ask to be added to the list as interested parties or to
    suggest changes to this document.
</p>
<p>
  <b>Initial Committers</b><br>
  <ul>
    <li>
      Gunnar Wagenknecht, Project Lead (AGETO Service GmbH)
    </li>
    <li>
      Ronny Schmidt, Committer (AGETO Service GmbH)
    </li>
    <li>
      Ralph Spickermann, Committer (AGETO Service GmbH)
    </li>
    <li>
      Frank Rub, Committer (igniti GmbH)
    </li>
    <li>
      Christian G&uuml;nzl, Committer (igniti GmbH)
    </li>
  </ul>
</p>
<p>
  <b>Interested Companies</b><br>
  <ul>
    <li>AGETO Service GmbH, Jena (Germany)</li>
    <li>igniti GmbH, Jena (Germany)</li>
    <li>TowerByte e.G., Jena (Germany)</li>
    <li>PlanetEye Inc., Toronto (Canada)</li>
    <li>Weigle Wilczek GmbH, Esslingen (Germany)</li>
  </ul>
</p>
<h3>Code Contributions</h3>
<p>
    Gunnar Wagenknecht is offering a CloudFree prototype as an initial codebase.
    The prototype is built with proposed concepts in mind and will be used as a
    potential codebase.
</p>
<h2>Tentative Plan</h2>
<p>
    Development is ongoing and a first demonstrable showcase is planned for
    November 2008. This prototype will demonstrate key concepts such as
    extensibility and provisioning. It will be used to socialize community
    feedback and to make a decision on the ongoing development. In case of
    significant community interest the development continues following an
    Eclipse iterative development approach with version 1.0 scheduled for
    Q4/2009.
</p>
	
      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
