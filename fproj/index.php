<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Faceted Project Framework</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Faceted Project Framework");
?>

<h2>Introduction</h2>

<p>Faceted Project Framework is a mature component of Eclipse Web Tools Platform (WTP) that
facilitates treating Eclipse projects as composed of units of functionality (called facets)
that can be easily added or removed by users. This is a proposal to move this framework from
WTP to a standalone project so that it can be accessed by Eclipse Projects that do not
have a natural dependency on WTP.</p>

<p>The Faceted Project Framework is a proposed open-source project under the Eclipse
Development Process, and will incubate under the top-level Technology project. This proposal
is posted here to solicit community feedback and additional project participation. You are
invited to comment on and/or join the project. Please send all feedback to the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.fproj">eclipse.fproj</a> newsgroup.</p>

<h2>Creation Review</h2>

<ul>
  <li>The projection creation review has been scheduled for <b>December 10th, 2008</b>.</li>
  <li>The communication channel for the review is the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.fproj">newsgroup</a>. Use the newsgroup to post
    any comments or to request the review to be held on a conference call.</li>
</ul>

<h2>Scope</h2>

<p>The Faceted Project Framework project would be chartered with making it possible for
developers to build modular projects where functionality can be easily added and removed by
the user in a standard way. It will further be charted with providing the integration story
between Faceted Project Framework and other technologies at Eclipse in situations where
doing so would substantially broaden the applicability of the framework.</p>

<p>Here are some of the key requirements:</p>

<ul>
  <li>Reduce the need for creation of specialized project wizards for different variants
    of what is essentially the same project type by facilitating creation of flexible
    wizards that allow user to select among various optional project components.</li>
  <li>Reduce the need for creation of adhoc "Enable X functionality" project actions by
    providing a centralized facility for managing functionality that's enabled in a
    project.</li>
  <li>Provide a way to accurately model tooling capabilities as they map to capabilities
    of a target platform. Prevent user from accidentally using techology that's incompatible
    with user's target platform.</li>
</ul>

<h2>Initial Components</h2>

<p>The project would be initially composed of two components:</p>

<ol>
  <li>Core Framework
    <ol>
      <li>API for building facets and manipulating faceted projects.</li>
      <li>UI for manipulating faceted projects. Includes a project properties page for adding
        and removing facets and a base wizard implementation for creating new faceted projects.</li>
      <li>Documentation for writing facets.</li>
    </ol>
  </li>
  <li>JDT Enablement
    <ol>
      <li>Java facet that's built around JDT's Java Project Nature.</li>
      <li>Modeling of runtimes that are JVM-based.</li>
      <li>Tools for making it easier to build facets for Java-based technologies. Includes tools
        for helping facets manage libraries.</li>
    </ol>
  </li>
</ol>

<h2>Relationship with Other Eclipse Projects</h2>

<p>Faceted Project Framework leverages Resources and UI frameworks from Eclipse Platform. Facets
provide a functional superset over Project Natures (part of the Eclipse Platform Resources). Projects
taking advantage of the Faceted Project Framework can choose to rely solely on it or mix facets
and natures. The mixing scenario is particularly important in cases where an existing code base
already uses natures and backwards compatibility needs to be maintained. In those situation facets
can serve as wrappers around natures.</p>

<p>There is a possibility that a deeper integration with Eclipse Platform will happen as there has
been some interest from the E4 project.</p>

<p>JDT Enablement component leverages various API provided by the JDT project.</p>

<p>WTP would be the main consumer of artifacts produced by this project for immediate future. </p>

<h2>Organization</h2>

<h3>Initial Committers</h3>

The initial committers for this component would be:
<ul>
  <li><b>Konstantin Komissarchik, Oracle, proposed project lead:</b>
      Konstantin works on Oracle Enterprise Pack for Eclipse commercial product and has been a committer on
      Eclipse Web Tools Platform since the first release. At WTP, his biggest contribution has been
      the design and implementation of the Faceted Project Framework (subject of this proposal), but he
      is also involved in several other areas. He leads WTP Common Tools project and is a member of
      the Technology Project PMC.</li>
</ul>

<h3>Project Mentors (from Architecture Council)</h3>

<ul>
  <li>Neil Hauge, Oracle, leads Dali JPA Tools project</li>
  <li>Oisin Hurley, Progress Software, leads SOA Tools project</li>
</ul>

<h3>Code Contributions</h3>

<p>The initial code contribution will come from Eclipse Web Tools Platform project.</p>

<h3>Interested Parties</h3>

<p>WTP, Eclipse IAM, STP, PDT</p>

<h3>Developer Community</h3>

<p>Developers who start out leveraging this framework might eventually contribute bug fixes and features.</p>

<h3>User Community</h3>

<p>This project will not provide functionality that can be directly consumed by end-users. Users of this project
will be developers at other Eclipse Projects as well as developers in the broader Eclipse Community.</p>

<h3>Project Plan</h3>

<p>The first release of this project will be aggressively scheduled to line up with Galileo Simultaneous
Release (June 2009). The primary goals of the first release will be to (a) refactor code contribution from
WTP into project's namespace, (b) eliminate several years worth of deprecated API from the code base, and
(c) other work necessary to get established as an independent project.</p>

<p>Concurrently with the above work, a change will be prepared to alter the version of the Faceted Project
Framework in WTP to become a shell that delegates to the new framework. It is absolute requirement that the
existing API is not broken and continues to behave as before. This will be verified both via automated tests
as well as via in-depth manual tests. Any interested adopters will be given this patch if they wish to test
in the context of their products. The precise details and time of adoption of this project by WTP will be
worked out by WTP. Note that adoption can only happen once this project has exited incubation.</p>

<br/><br/>
<center><p>Copyright (c) 2008 Oracle<br/>Made available under Eclipse Public License v1.0</p></center>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
