<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
	

<h1>Proposal for Eclipse Packaging Project</h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Packaging Project");
?>

<h1>Introduction</h1>
<p>The Eclipse Packaging Project is a proposed open source project
under the Eclipse Technology Project.</p>
<p>This proposal is in the Project Proposal Phase (as defined in the
<a href="http://www.eclipse.org/projects/dev_process/">Eclipse
Development Process</a> document) and is written to declare its intent and
scope. This proposal is written to solicit additional participation and
input from the Eclipse community. You are invited to comment on and/or
join the project. Please send all feedback to the <a
	href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.packaging">
http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.packaging</a> newsgroup.</p>

<h1>Background</h1>
<p>Eclipse is seeing tremendous adoption of its tool and platform
offerings. With thousands of downloads every day the Eclipse Platform
SDK is the most popular download offering. The SDK includes everything
needed for Eclipse plug-in development (Platform, PDE, JDT and Sources).
When Eclipse was young, this was almost everything that Eclipse.org had
to offer. This situation has changed over the last five years, and
Eclipse is offering tools from A (AspectJ) to W (Web Tools Platform) in
more than 60 projects.</p>
<p>At the same time, Eclipse is not only serving plug-in developers
anymore, but also developers who want to explore Eclipse as a tool for a
specific language or domain. Those developers are interested in
downloading tools that may differ quite a bit from the Platform SDK
download. For instance, developers often don't require the source code
or Plug-in Development Environment (PDE), if they are just looking to
use Eclipse as a Java IDE. It is possible to extend the Eclipse Platform
SDK by using the update manager, but developers generally prefer a
single download to get started. This is especially important for
developers that are new to Eclipse.</p>

<h1>Project Overview</h1>
<p>The Eclipse Packaging project aims to provide a set of
entry-level downloads for different user profiles. The goal is to
improve the usability and out-of-box experience of developers that want
to use multiple Eclipse projects.</p>
<p>Although this project will define an initial set of user
profiles, it will also provide a platform to enable anyone to create
download packages.</p>

<h1>Scope</h1>
<p>The objectives of the Eclipse Packaging project are to:</p>
<ul>
	<li><b>Create entry level downloads based on defined user
	profiles.</b> The project will define an initial set of user profiles that
	will specify the requirements for a particular download package. It is
	expected that the initial user profiles will include Java Developer,
	Java EE Developer, C/C++ Developer and RCP Developer. Over time
	additional user profiles may be added or the initial set redefined.</li>
	<li><b>Provide an installer that improves the install
	experience of new users of Eclipse.</b> In the Europa time-frame the
	installer will provide the users with a well known and easy to use
	mechanism for extracting the download to an adjustable location and
	provide the possibility to create a link to the executable. We intend
	to integrate an integrity check of the download archive for the Europa
	downloads.</li>
	<li><b>Provide a platform that allows the creation of packages
	(zip/tar downloads) from an update site.</b> The core technology of the
	project will enable the creation of download packages that are created
	by bundling Eclipse features from one or multiple Eclipse update sites.
	The EPP platform will require a list of all features with version
	numbers be provided to the tooling. The tool platform will leverage
	existing technology. A discussion with the Eclipse Platform team
	indicates that the PDE build might be extended to serve the packaging
	project purposes. The EPP technology will be available to anyone,
	including other Eclipse projects, that want to create their own
	download package.</li>
</ul>

<h1>Out of Scope</h1>
<ul>
	<li>Extending an existing Eclipse installation, other aspects of
	provisioning, and extending the Eclipse Update Manager is not part of
	this project.</li>
	<li>Creating a general purpose multi platform install framework
	with all bells and whistles.</li>
</ul>

<h1>FAQ</h1>
<h2>Does the Eclipse Packaging project produce downloads on demand?</h2>
<p>Yes and no. EPP is able to create downloads based on the delivery
of an xml file containing the feature ids and versions, but it is
neither optimized for creating downloads as fast as possible nor does it
provide a web application to pick and choose features to be included
into a download.</p>

<h2>Does the Eclipse Packaging project provide an installer?</h2>
<p>A native installer can help to provide a positive initial user
experience. With the planned code contribution of Instantiations it will
be possible to provide an installer with those features short term
(Europa time-frame). Further useful features, like checking the
availability of a suitable Java environment, potentially packaging a
run-time environment, reducing the download size (e.g. with pack200)
will be addressed at a later time, and will be coordinated with the
developments in the area of provisioning.</p>

<h2>Which technology will be used in this project?</h2>
<p>In the initial project phase research will be done if the
packaging platform itself and the EPP tools can be built upon the PDE
build and packaging features.</p>

<h2>Will you include a runtime environment?</h2>
<p>With Java (TM) becoming open source and the latest efforts of the
Geronimo project this may become feasible. We would be interested in
community input on this topic, as there is a trade off between download
size, ease of selection and choice. It is not very likely that this
topic will be addressed in the Europa time-frame.</p>

<h2>Can the Eclipse Packaging tools be used by organizations to
build custom development environment?</h2>
<p>Yes. Organizations can use the platform to define and build an
Eclipse environment containing the plug-ins their developers need.</p>

<h2>Can the Eclipse Packaging be extended?</h2>
<p>Eclipse is about "extensible platforms and exemplary tools" - and
we take that seriously. The project will provide an extensible platform
for creating eclipse packages.</p>

<h1>Organization</h1>

<h2>Initial committers</h2>
<p>The initial committers will focus on providing the initial set of
entry level downloads and developing the Eclipse Packaging tools. Our
agile development process will follow eclipse.org's standards for
openness and transparency. As such we will actively encourage
contributions to the project. We also plan to help improve the Eclipse
update manager by submitting patches and extension point suggestions if
necessary. The initial committers are:
<ul>
	<li>Markus Knauer (Innoopract): Project Lead</li>
	<li>Dan Rubel (Instantiations): Co-Project Lead</li>
	<li>Leif Frenzel (Innoopract)</li>
	<li>Elias Volanakis (Innoopract)</li>
	<li>Mark Russell (Instantiations)</li>
	<li>Alexander Kazantsev (xored software)</li>
</ul>

<h2>Interested parties</h2>
The following parties have expressed interest extending the platform,
contributing ideas, guidance and discussion. Key contacts listed.
<ul>
	<li>Pascal Rapicault (IBM): Advisor</li>
	<li>Thomas Hallgren (Buckminster Project)</li>
	<li>Henrik Lindberg (Buckminster Project)</li>
</ul>

<h2>User community</h2>
<p>The Eclipse Packaging project is providing a service for a very
disparate group of developers, as such, supporting and soliciting
feedback from a large user community of developers is critical to
creating the right offering. We plan on doing this by using the standard
eclipse.org mechanisms of supporting an open project and community of
early adopters.</p>

<h1>Tentative Plan</h1>
<p>
2007-02 0.5M1: initial definition of entry level downloads<br>
2007-03 0.5M2: Basic implementation of EPP platform<br>
2007-05 0.5M3: Feature complete for Europa release<br>
2007-06 0.5: Beta availabilty for all Eclipse projects
</p>


      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
