<html>
<head>

<title>Sapphire</title>
</head>

<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
</style>

<body>
<p>The Sapphire project is a proposed open source project under the <a
	href="http://www.eclipse.org/projects/project_summary.php?projectid=technology">Technology
Container Project</a>.</p>

<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the <a href="http://www.eclipse.org/forums/eclipse.sapphire">Sapphire</a>
Forum.</p>

<h2>Background</h2>

<p>Little has changed in the way Java desktop UI is written since the original Java release. 
Technologies have changed (AWT, Swing, SWT, etc.), but fundamentals remain the same. The developer 
must choose which widgets to use, how to lay those widgets out, how to store the data being edited 
and how to synchronize the model with the UI. Even the best developers fall into traps of having UI 
components talk directly to other UI components rather than through the model. Inordinate amount of 
time is spent debugging layout and data-binding issues.</p>

<p>Sapphire aims to raise UI writing to a higher level of abstraction. The core premise is that the 
basic building block of UI should not be a widget (text box, label, button, etc.), but rather a 
property editor. Unlike a widget, a property editor analyzes metadata associated with a given 
property, renders the appropriate widgets to edit that property and wires up data binding. Data is 
synchronized, validation is passed from the model to the UI, content assistance is made available, etc.</p>
 
<p>This fundamentally changes the way developers interact with a UI framework. Instead of writing 
UI by telling the system how to do something, the developer tells the system what they intend to 
accomplish. When using Sapphire, the developer says "I want to edit LastName property of the person 
object". When using widget toolkits like SWT, the developer says "create label, create text box, lay 
them out like so, configure their settings, setup data binding and so on". By the time the developer 
is done, it is hard to see the original goal in the code that's produced. This results in UI that 
is inconsistent, brittle and difficult to maintain.</p>

<h2>Scope</h2>

<p>This project will be focused on identifying ways to significantly speed up UI development while 
simultaneously improving quality, maintainability and consistency.</p>

<p>Key deliverables:</p>

<ul>
  <li>A declarative UI framework with a renderer for SWT and possibly other widget toolkits.</li>
  <li>A compact and easy to learn domain-specific modeling framework tailored to the needs of UI writers.</li>
  <li>Developer tools, documentation and samples.</li>
</ul>

<p>While the modeling framework that's part of Sapphire could be used by itself for general modeling 
needs, it is not in scope of this project to promote such usage.</p>

<h2>Initial Contribution</h2>

<p>Oracle will contribute the initial codebase to the Sapphire project. This codebase has been used 
to deliver features in multiple releases of Oracle Enterprise Pack for Eclipse and is fairly mature, but 
it is still evolving rapidly to meet new requirements.</p>

<p>See <a href="http://lt-rider.blogspot.com/2010/06/sapphire.html">introduction to Sapphire</a> for a
detailed look.</p>

<p>The initial contribution will include:</p>

<ul>

  <li>The modeling framework, with support for value, element and list properties.</li>
  
  <li>A system for binding models to XML/DOM, including specific integration with the WTP XML editor. 
  This system makes it easy to deliver form-based editors on top of XML files that have live bi-directional 
  updates between the form view and the source view.</li>
  
  <li>The UI framework, with an XML-based UI definition parser and support for a variety of commonly-used 
  parts:</li>
  
  <ul>
  
    <li>Leaf Parts</li>
    
    <ul>
      <li>property editor</li>
      <li>label (for stand-alone use)</li>
      <li>separator</li>
      <li>spacer</li>
      <li>action</li>
    </ul>
    
    <li>Grouping Parts</li>
    
    <ul>
      <li>composite</li>
      <li>group</li>
      <li>section</li>
      <li>tab group</li>
      <li>page book</li>
    </ul>
    
    <li>Control Parts</li>
    
    <ul>
      <li>with</li>
      <li>if-else</li>
    </ul>
    
  </ul>
  
  <li>A UI renderer for SWT with a mature implementation of renderers for all UI parts, including the 
  following property editor renderers:</li>
  
  <ul>
    <li>string (default) - a text field with an optional browse button and other built-in facilities</li>
    <li>boolean - a check box</li>
    <li>enum - a radio buttons group or a combo box, as appropriate</li>
    <li>integer - a combination of radio buttons and a text box to handle special values situation</li>
    <li>integer - a scale with a text field for direct entry</li>
    <li>list - a fully editable table</li>
    <li>list - a slush bucket</li>
    <li>path - a tree for selecting an Eclipse workspace path, similar to many Eclipse wizards</li>
  </ul>
  
</ul>
    
<h2>Committers</h2>

<p>The following individuals are proposed as initial committers to the project:</p>

<ul>
  <li>Konstantin Komissarchik (Lead), Oracle</li>
  <li>Ling Hao, Oracle</li>
</ul>

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Tom Schindl</li>
	<li>Neil Hauge</li>
</ul>

<h2>Interested Parties</h2>

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li>Nitin Dahyabhai, IBM</li>
	<li>Neil Hauge, Oracle</li>
</ul>

<h2>Relation to EMF</h2>

<p>The focus of Sapphire is making UI development more productive. A core aspect of the architecture 
is tight coupling with a modeling framework. We did not chose to use EMF, as we feel that in many cases 
the needs of UI developers would be better served by a modeling framework designed to be easy to learn 
and tailored to specific scenarios that occur in UI development.</p>
 
<p>At the same time, we welcome participation of EMF experts interested in making it possible to use 
EMF models with Sapphire.</p>

<h2>Relation to e4</h2>

<p>Both Sapphire and e4 take a declarative approach to UI specification, so they may appear similar at 
first glance, but the two operate at different levels of abstraction. Sapphire deals with property 
editors and is tightly coupled to a model, while e4 talks about individual widgets and how to lay them 
out in detail. We believe that these two technologies complement each other and this project intends to 
support using Sapphire with e4.</p>
 
<p>We welcome participation of e4 experts interested in exploring the integration possibilities.</p>

<h2>Relation to JFace Data Binding</h2>

<p>The JFace data binding framework is a standard API to simplify synchronization of data between 
the UI and the model. It is possible to use the data binding framework as part of implementing a Sapphire 
property editor renderer, but the developer using Sapphire is not exposed to data binding concepts 
directly. This is possible due to a design decision that tightly couples the UI framework to a specific 
modeling framework.</p>

<h2>Project Scheduling</h2>

<p>The initial code contribution will come soon after the project is created. We intend to seek
parallel IP review for this contribution.</p>

<h2>Additional Resources</h2>

<ul>
  <li><a href="http://lt-rider.blogspot.com/2010/06/sapphire.html">Introduction to Sapphire</a></li>
  <li><a href="http://lt-rider.blogspot.com/2010/06/sapphire-focus-on-browsing.html">Focus on Browsing</a></li>
  <li><a href="http://lt-rider.blogspot.com/2010/07/sapphire-focus-on-localization.html">Focus on Localization</a></li>
</ul>

<h2>Changes to this Document</h2>

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>17-June-2010</td>
		<td>Document created</td>
	</tr>
	<tr>
		<td>25-June-2010</td>
		<td>Linked to an intro article</td>
	</tr>
	<tr>
		<td>08-July-2010</td>
		<td>Project mentors listed. New additional resources section with external links.</td>
	</tr>
</table>

</body>
</html>