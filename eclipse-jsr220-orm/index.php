<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">		
			
			<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("JSR220-ORM");
?>
				
					<h1>JSR220-ORM 
								Project</h1>
			
				
					<h2>Introduction</h2>
				


						<P>The JSR220-ORM Project is a proposed open source project under the <A href="/technology/">
								Eclipse Technology Project</A>. The goal of the project is to provide 
							tooling integrated with the Eclipse platform that generates artifacts for 
							runtime implementations of the <A href="http://www.jcp.org/aboutJava/communityprocess/edr/jsr220/">
								JSR 220</A> persistence and <A href="http://www.jcp.org/aboutJava/communityprocess/pr/jsr243/index2.html">
								JSR 243</A> specifications. Further, the goal of the project is to track 
							the changes to these specifications, so that in addition, as they merge 
							together towards a common standard, the tooling will fully support artifacts 
							for the combined specification.&nbsp; Finally, while the specific focus of this 
							project is on JSR 220 tooling, it is the goal of this project to implement 
							extensible frameworks that can easily accommodate alternative input sources and 
							output artifacts.</P>
						<P>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/">
								Eclipse Development Process</A> document)&nbsp; and is written to declare 
							its intent and scope. This proposal is written to solicit additional 
							participation and input from the Eclipse community. You are invited to comment 
							on and/or join the project. Please send all feedback to the <A href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.jsr220-orm">
								eclipse.technology.jsr220-orm </A>newsgroup.
						</P>
					
				
			
		
			
				
					<h2>Background</h2>
				
				
					
						<P>The requirement for this project stems from the increasing demand for 
							standardization around access to database technologies from object oriented 
							development languages such as Java. Java developers are often distracted with 
							the issues of persistence when developing domain specific applications. These 
							distractions come in the form of understanding and implementing storage 
							specific access mechanisms and object life cycle management which are really 
							cross cutting concerns of applications across all domains that need to store 
							and access persistent state. Abstracting out and separating these concerns is 
							well documented in and is the goal of both&nbsp;<A href="http://www.jcp.org/aboutJava/communityprocess/edr/jsr220/">JSR 
								220</A> and&nbsp;<A href="http://www.jcp.org/aboutJava/communityprocess/pr/jsr243/index2.html">JSR 
								243</A>&nbsp;&nbsp;and this project is an implementation of tooling 
							necessary to accommodate a runtime of those specifications.&nbsp; Further, the 
							goal of this project it to produce tooling that is extensible&nbsp;and in the 
							future would be able to&nbsp;accommodate alternate input, such as XML and 
							then&nbsp;generate artifacts for alternative runtimes.</P>
						<P>
							The Eclipse IDE is by far the dominating tool used by domain driven developers, 
							so it only makes sense to directly incorporate this technology into Eclipse to 
							foster easy adoption. Further, as an implementation of&nbsp;<A href="http://www.jcp.org/aboutJava/communityprocess/edr/jsr220/">JSR 
								220</A> persistence tooling in Eclipse, this will provide the foundation 
							for all major application server vendors who will be needing this technology 
							integration in future releases of their products as the&nbsp;<A href="http://www.jcp.org/aboutJava/communityprocess/edr/jsr220/">JSR 
								220</A>&nbsp;&nbsp;specification defines the application server vendors EJB 
							3.0 persistence implementation. Most major application server vendors are using 
							Eclipse as their future tools platform and as such will benefit from this 
							project and be able to extend the foundation implementation to their specific 
							needs.
						</P>
						<P>&nbsp;</P>
					
				
			
		
			
				
					<h2>Description</h2>
				
				
					
						<P>The JSR220-ORM Project will focus on tooling relevant for JSR220 runtimes while 
							creating an extensible framework for mapping.
						</P>
						<P>Specifically, the visual tooling fully integrated with Eclipse that is needed to 
							manage complex JSR 220/243 projects involving large numbers of user defined 
							classes. Tooling that facilitates the management of meta data related to a JSR 
							220/243 implementation. Tooling that facilitates how patterns found in the 
							object oriented Java language are correlated with patterns found in relational 
							data models. Where applicable and feasible, this project will also use and/or 
							collaborate with other sub projects within the <A href="/proposals/eclipse-dtp/">
								Data Tools Project</A> and&nbsp;<A href="/webtools/">Web 
								Tools Project</A> to achieve it's goals. The tooling will be developed as a 
							series of frameworks allowing the plug-in of <A href="http://www.jcp.org/aboutJava/communityprocess/edr/jsr220/">
								JSR 220</A> persistence and&nbsp;<A href="http://www.jcp.org/aboutJava/communityprocess/pr/jsr243/index2.html">JSR 
								243</A> &nbsp;runtimes.&nbsp;&nbsp;
						</P>
						<P>Eclipse ORM tooling will produce artifacts that can be used with <A href="http://www.jcp.org/aboutJava/communityprocess/edr/jsr220/">
								JSR 220</A> persistence and&nbsp;<A href="http://www.jcp.org/aboutJava/communityprocess/pr/jsr243/index2.html">JSR 
								243</A> runtime implementations and provide a framework for producing other 
							artifacts.&nbsp;&nbsp;As an example of exemplary generation of artifacts 
							for&nbsp;runtime integration, the Eclipse ORM tooling will use&nbsp;an open 
							source core runtime implementation of&nbsp;<A href="http://www.jcp.org/aboutJava/communityprocess/edr/jsr220/">JSR 
								220</A> persistence and&nbsp;<A href="http://www.jcp.org/aboutJava/communityprocess/pr/jsr243/index2.html">JSR 
								243</A> specifications, downloadable under the EPL from the Versant website 
							( <A href="http://www.versant.com">www.versant.com</A> ).&nbsp;</P>
						<P>
							This project is based on the continuation of the past 3 years of work on an 
							existing JSR 220/243 commercial implementation that is already integrated with 
							Eclipse and is to be contributed (source, documentation, tests) as open source 
							to this project. It&nbsp;was expected that there would be integration issues 
							with other sub projects of the top level <A href="/proposals/eclipse-dtp/">
								Data Tools Project</A>&nbsp;and <A href="/webtools/">Web 
								Tools Project</A>.&nbsp;&nbsp; Many of those issues have come to light 
							during discussion with other Eclipse members during the <A href="/proposals/eclipse-dtp/">
								Data Tools Project</A>&nbsp; "face to face" meetings which kicked off its 
							code integration efforts and with discussions&nbsp;with&nbsp;Eclipse EMO 
							members.&nbsp; A result of those interactions&nbsp;is a substantial adjustment 
							to the initial milestones and considerable rework of existing tooling to 
							properly integrate with the Eclipse platform.&nbsp;&nbsp; The&nbsp;<A href="JSR220-ORM Project Planr1.html">current 
								project plan</A> reflects the culmination of work done to accommodate a 
							proper integration.&nbsp;
						</P>
						<P>As Eclipse is an evolving platform, integration issues involving other elements 
							of the&nbsp;<A href="/proposals/eclipse-dtp/">Data 
								Tools Project</A> and <A href="/webtools/">Web Tools 
								Project</A>&nbsp;may impact future milestones as those integration issues 
							come to light and are factored into the plan.
						</P>
						<P>The&nbsp; <A href="JSR220-ORM Project Planr1.html">current project plan</A> is 
							based on milestones that are visible and achievable and will serves as a 
							starting point for the ongoing work.</P>
						<P>&nbsp;</P>
					
				
			
		
			
				
					<h2>Organization</h2>
				
				
					
						<P>We propose that this project is created as an incubator within the <A href="/technology">
								Eclipse Technology Project</A>. . It is expected that over time some 
							elements of the visual tooling will be common to other types of data access. As 
							the implementation of a standard, it is anticipated that as this work matures 
							it will become part of the core Eclipse platform where appropriate and likely 
							move as a sub project to either the top level <A href="/proposals/eclipse-dtp/">
								Data Tools Project</A>&nbsp;or <A href="/webtools/">Web 
								Tools Project</A>.
						</P>
						<P><STRONG>Initial Committers:</STRONG> Proposed Project Lead:&nbsp;Dirk&nbsp;le 
							Roux&nbsp;
						</P>
						<P>
							Additional Committers include: Carl Cronje, David Tinker&nbsp;
						</P>
						<P><STRONG>Interested Companies:</STRONG> IBM, BEA, Sybase</P>
						<P><B>Code Contributions</B><BR>
							Initial code contribution&nbsp;consists of&nbsp;the commercial implementation 
							of Versant's ORM tooling.&nbsp; In participation with DTP consolidation reviews 
							elements of our initial code contribution have changed to incorporate the code 
							from other Eclipse based projects.&nbsp;&nbsp; Where appropriate, this project 
							expects to share it's contributions to those other projects as well.&nbsp; 
							Further community contribution is expected and welcome.</P>
						<P>
							We, the submitters of this proposal, welcome and encourage interested parties 
							to post to the <A href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.jsr220-orm">eclipse.technology.jsr220-orm
							</A>newsgroup&nbsp;&nbsp;with comments, recommendations, questions, etc.
						</P>
						<P>&nbsp;</P>
					
				
			
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
