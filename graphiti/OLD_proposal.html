<html>
<head>
<title>Graphiti</title>
</head>

<body>
<p>The Graphiti project is a proposed open source project under the
<a
	href="http://www.eclipse.org/projects/project_summary.php?projectid=modeling">Eclipse
Modeling Project (EMP) Top-Level Project</a>.</p>

<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the <a href="http://www.eclipse.org/forums/index.php?t=thread&frm_id=107">Eclipse Modeling</a>
 forum.</p>

<h2>Background</h2>

Eclipse provides a modeling infrastructure evolving around the Eclipse
Modeling Framework (EMF). Offering graphical representations and editing
possibilities is an essential part of EMF. SAP has built and plans to
contribute an Eclipse-based graphics framework to enable easy
development of state-of-the-art diagram editors for domain models. SAP
plans to contribute the developed framework under the name Graphiti.

<h2>Scope</h2>

The objectives of the Graphiti project are the following:
<li>Provide an easy to use and well structured plain Java API for
building graphical tools</li>
<li>Provide documentation and tutorials for doing so</li>
<li>Limit the dependencies of the framework to an absolute minimum
to support RCP-like scenarios</li>
<li>Provide optional components beyond the RCP use case to ease
e.g. IDE integration</li>
<p>Graphiti will (initially) not cover these issues:
<li>Shape library</li>
<li>Layout algorithms (though existing ones might be used)</li>
</p>

As the
<a href="http://www.eclipse.org/modeling/gmf/">Graphical Modeling
Framework (GMF)</a>
already exists as a project targeting a similar objective, we
distinguish Graphiti from GMF as follows:
<p>
<table border="1">
	<tr>
		<td>&nbsp;</td>
		<td><b>Graphiti</b></td>
		<td><b>GMF</b></td>
	</tr>
	<tr>
		<td><b>Architectural Concept</b></td>
		<td>runtime centric</td>
		<td>generative</td>
	</tr>
	<tr>
		<td><b>API</b></td>
		<td>self-contained</td>
		<td>refers to GEF functionality</td>
	</tr>
	<tr>
		<td><b>Client Logic</b></td>
		<td>centralized (feature concept)</td>
		<td>functionality distributed</td>
	</tr>
</table>

<h2>Description</h2>

The Graphiti framework utilizes Eclipse's
<a href="http://www.eclipse.org/gef/">GEF</a>
and
<a href="http://eclipse.org/articles/Article-GEF-Draw2d/GEF-Draw2d.html">Draw2D</a>
for diagramming and supports EMF on the domain side. The diagrams are
described by a platform independent metamodel and the diagram data is
kept strictly separate from the domain data. This enables rendering an
existing diagram in various environments (besides Eclipse this could be
e.g. within a browser) even without having access to the underlying
domain data. Graphiti is strictly API-centric: a user of the framework
writes plain Java coding and (besides Graphiti) only needs to know EMF
to use the framework and to build an editor -- no knowledge of Draw2D or
GEF is required.
<p>Editors built with the framework are equipped with a standardized
look and feel which leads to a more coherent UI in Eclipse-based tools.
Rapid prototyping is supported by simple APIs and base classes which can
be used to refine an editor in an evolutionary way. The user of the
framework writes so-called features to add functionality to the editor.
For instance, one would write features for creating new model objects
and their graphical representation (Create Features), or for creating a
graphical representation for an already existing model object (Add
Features). The complete life cycle (creating, editing, renaming, moving,
deleting...) of model objects and their graphical representations can be
defined and controlled by implementing such features. The standard
behavior for the different operations is covered by so-called default
features, if the user of the framework does not declare any special
behavior. Step by step, these default features can be replaced or
extended with further functionality. Additionally, the framework comes
with a hook for so-called custom features to implement non-standard
behavior and further operations inside the tool.
<h2>Architecture</h2>

The following diagram visualizes the framework as perceived by a tool
developer.
<p><img alt="Architecture of Graphiti" src="archtools.png">
<p>The Interaction Component is provided by Graphiti, whereas the
Diagram Type Agent (DTA) is implemented by the user of the framework and
defines the new diagram type that is contributed by the tool. It
provides one or more Tool Behavior Providers that define how the tool
behaves in specific situations; the tool can influence, e.g., what will
be displayed in the palette of the editor, how selection and double
clicking is handled, that some special rendering is necessary for
certain objects, how zooming is handled and so on. It is also the
responsibility of the Tool Behavior Provider to define which context
menu and context buttons are available in a certain scenario. The
context buttons appear around the currently hovered shape and can be
used by the user in a very convenient way to trigger object specific
operations; they are completely rendered by Graphiti. The tool only
provides the operation, a name, and potentially an icon for it. Of
course, operations can also be triggered via context menu entries.
<p><img alt="Context Buttons" src="ContextButtons.jpg">
<p>These operations are defined by the tool by implementing features
or by using the default features provided by the framework. Besides,
there is a so called Feature Provider for a diagram type which is
responsible to define the features that are available and are to be used
in the tool in a specific scenario.
<h2>Relationship with other Eclipse Projects</h2>
<ul>
	<li>Graphiti uses EMF on the domain side.</li>
	<li>Graphiti uses Draw2D and GEF for diagramming.</li>
	<li>Graphiti complements the offering of GMF which follows a
	generative approach while Graphiti follows a runtime-oriented and API
	based approach.</li>
</ul>

<h2>Initial Contribution</h2>

The initial code contribution will come from
<a href="http://www.sap.com">SAP</a>
where Graphiti was developed originally as part of their MOF based
modeling infrastructure. Later Graphiti was ported to EMF; this version
will be the basis for the coding contributed to Eclipse.

<h2>Legal Issues</h2>

There are no known legal issue with Graphiti.

<h2>Committers</h2>

<p>The following individuals are proposed as initial committers to
the project:</p>

<ul>
	<li>Michael Wenz, <a href="http://www.sap.com">SAP</a>, lead</li>
	<li>Christian Brand, <a href="http://www.sap.com">SAP</a></li>
	<li>Matthias Gorning, <a href="http://www.sap.com">SAP</a></li>
	<li>Tim Kaiser, <a href="http://www.sap.com">SAP</a></li>
	<li>J&uuml;rgen Pasch, <a href="http://www.sap.com">SAP</a></li>
</ul>

<h2>Mentors</h2>

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Cedric Brun, <a href="http://www.obeo.fr">Obeo</a></li>
	<li>Bernd Kolb, <a href="http://www.sap.com">SAP</a></li>
</ul>

<h2>Interested Parties</h2>

<p>The following individuals, organisations, companies and projects
have expressed interest in this project:</p>
<ul>
	<li>Ronald Steinau, <a href="http://www.entimo.de">Entimo</a></li>
	<li>Jens von Pilgrim, <a href="http://www.fernuni-hagen.de/">FernUniversit&auml;t
	Hagen</a></li>
	<li>Markus V&ouml;lter, <a href="http://www.itemis.de">itemis</a></li>
	<li>Jan K&ouml;hnlein, <a href="http://www.itemis.de">itemis</a></li>
	<li>Alexander Ny&beta;en, <a href="http://www.itemis.de">itemis</a></li>
	<li>Dimitrios Kolovos, <a href="http://www.cs.york.ac.uk">University
	of York</a></li>
	<li>Eike Stepper, Lead <a href="http://wiki.eclipse.org/CDO">CDO Model Repository</a></li>
	<li>Mickael Istria, <a href="http://www.bonitasoft.com">BonitaSoft S.A.</a></li>
</ul>
<br>
The need for a runtime-centric graphical framework enabling easy
development of graphical editors in EMP was discussed at the Eclipse
Summit Europe 2009 with the above-mentioned parties.

<h2>Project Scheduling</h2>

We plan to present Graphiti at
<a href="http://www.eclipsecon.org/2010/">EclipseCon 2010</a>
to raise the community awareness of the project. We plan the initial
code contribution for end of March 2010 and hope to have an incubator
release by June 2010, aligned with the Helios release. Also after the
initial code contribution we plan to open the project for further
contributors from other interested parties.
<h2>Changes to this Document</h2>

<table>
	<tr>
		<th>December, 15th 2009</th>
		<th>Document created</th>
	</tr>
	<tr>
		<th>March 18, 2010</th>
		<th>Document updated</th>
	</tr>
</table>
</body>
</html>