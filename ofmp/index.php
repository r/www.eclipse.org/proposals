<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("OFMP");
?>

<p>

<h1>Open Financial Market Platform</h1>

<h2>Introduction</h2>

<p>Kaupthing Bank Luxembourg, in co-operation with Weigle Wilczek GmbH, is proud to propose the Eclipse Open Financial Market Platform Project as an open
source project under the <a href="http://www.eclipse.org/technology">Eclipse Technology Project</a>.<br /><br />
This proposal:
</p>
<ul>
  <li>is in the Project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process">Eclipse Development
      Process document</a>) and is written to declare its intent and scope.</li>
  <li>is written to solicit additional participation and input from the Eclipse community.</li>
</ul>
<p>The <a href="http://www.eclipse.org/proposals/eclipse-ohf">Eclipse Open Healthcare Framework proposal</a> has been used as a basis for this proposal.</p>
<p>You are invited to comment on and/or join this project. Please send all feedback to the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ofmp">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ofmp</a> newsgroup.</p>
<h2>Overview</h2>
<p>
  The goal of the Open Financial Market Platform (OFMP) is to extend the Eclipse
  Platform to create an open-source platform for interoperable,
  extensible financial market systems based on Equinox, Eclipse's OSGi
  framework. OFMP targets on tools and components for middle-office Financial
  Markets applications for the banking industry. OFMP will open-source
  field-tested components from a bank's production environment connecting to
  common back-office and front-office systems.
</p>

<p>This <b>Open Financial Market Platform </b>proposal might lead to more generic projects, 
such as an Open Banking &amp; Finance Framework.</p>

<p>
The Open Financial Market Platform will develop infrastructure, tools,
components, and services to support development of both client and server
applications. Client development will be based on Eclipse RCP and server
components will be based on Equinox OSGi together with core Eclipse concepts
such as plug-in based extensibility.
</p>

<p>
  The essential functional elements of OFMP V 1.0 are well understood by domain
  experts. While there is a significant engineering effort required, we feel
  that the work plan as proposed here can be implemented given sufficient
  resources, and does not involve an unrealistic level of technical risk.
  Consequently, we propose this project to be considered as an incubator within
  the Technology PMC.
</p>

<p>
  We are not aware of any other efforts to build a <b>open-source platform</b> for
  constructing financial applications. Consequently, we expect it will take both
  time and effort to grow an OFMP community within the finance industry. We are
  optimistic that within the first 6-12 months there will be a considerable
  growth in the team after successfully passing the Creation Review and being
  established as an Eclipse project. Our development plans are deliberately
  open-ended so that we can employ these additional resources effectively as
  they become available. If we are wrong in our optimism, our development plan
  can still succeed, but clearly will take longer to reach its goals.
</p>

<h2>Vision</h2>
<p>
  Financial industry has experienced continuous <b>standardization</b> of its
  main products and methods since the early 90's.
</p>
<p>
  OFMP's goal is to respond to the increasing need of a vertical market
  <b>integration</b> within current existing financial software offerings.
  Several financial open source tools and mathematical libraries can benefit
  from this platform as an integration point.
</p>
<p>The <b>componentization</b> enables contributors to more
  easily build reusable financial business components and applications, using
  Eclipse's powerful component model and extension mechanisms in client-side as
  well as server-side environments through OSGi.
</p>

<h2>Scope</h2>

<p>A lot of financial applications require a common set of services, for example:</p>
<ul>
  <li>Business Date</li>
  <li>Calendars</li>
  <li>Counterparties</li>
  <li>Currencies</li>
  <li>Quotes</li>
  <li>Order Management processes</li>
  <li>Limits</li>
  <li>Exposures</li>
  <li>Market Data</li>
</ul>

<p>To assemble those services, several types of tools are required:</p>
<ul>
  <li>Business Rules Engine</li>
  <li>Tools to integrate with data-oriented and service-oriented legacy EIS and external data source</li>
  <li>Reporting Infrastructure</li>
  <li>Background Job Scheduler</li>
  <li>Financial Mathematical Libraries</li>
  <li>Real time data Infrastructure</li>
  <li>Document Management System</li>
</ul>
<p>
  OFMP will provide a framework for implementing support for a broad range of
  financial instruments and financial algorithms/libraries, effectively
  combining live data with quantitative financial models. Specific tools and
  components for risk management can be easily integrated and will be provided
  as well.
</p>
<p>
  While providing field-tested components that connect to common financial
  systems in an extensible framework, the platform allows to integrate
  sophisticated methods from quantitative finance to observe, analyse and
  project data in a real life banking environment. Latest results from academic
  research can be more easily adopted while using this framework.
</p>
<p>
  We believe there is an opportunity to provide a common, vendor-neutral
  platform that includes generic components, integration mechanisms, supporting
  tools and workflows, together with a process for staged customization to meet
  national standards, allow differentiation of commercial products, and meet the
  requirements of financial organizations. As mentioned above, OFMP will only
  cover Financial Markets functionalities and will be established as a
  middle-office platform.
</p>

<h2>Functional Structure</h2>
<p>Proposed functional structure is presented on the following diagram:</p>
<div>
  <table style="height:74px; width:980px; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">
    <tbody>
    <tr>
      <td style="width=100%;">
        <div>
          <table style="height:44px;width:970px;border-style: hidden">
            <tbody>
            <tr>
              <td style="text-align:center;font-size:20px;width=50%;">&#8592; Technology oriented</td>
              <td style="text-align:center;align:right;width:50%;font-size:20px">Business oriented &#8594;</td>
            </tr>
            </tbody>
          </table>
        </div>
      </td>
    </tr>
    <tr>
      <td style="border-style:solid; border-width:1px; border-color:black; width:50%;">
        <div>
          <table style="height:38px; width:970px; border-style: hidden">
            <tbody>
            <tr>
              <td style="vertical-align:top;width:20%;">
                <p><b>Common</b></p>
                <ul>
                  <li>Background job scheduler</li>
                  <li>Business rules engine</li>
                  <li>Reporting infrastructure</li>
                  <li>Currency calendar</li>
                  <li>...</li>
                </ul>
              </td>
              <td style="vertical-align:top;width:20%">
                <p><b>Environment Definition</b></p>
                <ul>
                  <li>Market data management</li>
                  <li>Counterparty management</li>
                  <li>Trading guidelines management</li>
                  <li>Trading books management</li>
                  <li>...</li>
                </ul>
              </td>
              <td style="width:20%">
                <p>
                  <b>Risk Controlling</b>
                </p>
                <ul>
                  <li>Real time position keeping</li>
                  <li>Intra-day risk management</li>
                  <li>Reconciliation process</li>
                  <li>Position mark to market</li>
                  <li>...</li>
                </ul>
                <p>
                  <b>Business Standard</b>
                </p>
                <ul>
                  <li>CLS Compliancy</li>
                  <li>P&amp;L Calculation</li>
                  <li>Order management</li>
                  <li>...</li>
                </ul>
              </td>
              <td style="vertical-align:top;width:20%">
                <p>
                  <b>Risk Management</b>
                </p>
                <ul>
                  <li>Market risk measurements</li>
                  <li>Derivatives instrument pricing</li>
                  <li>Stress testing</li>
                  <li>Monte Carlo methods</li>
                  <li>Simulations</li>
                  <li>...</li>
                </ul>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </td>
    </tr>
    </tbody>
  </table>
</div>
<p>These components can be used in applications that are supporting Money Markets, Capital Markets and Foreign Exchange activities.</p>
<p>Though initially this structure is mainly focused on Financial Markets
divisions, it can also be extended to cover other financial divisions like
Accounting, Credit, Corporate, Insurance, Asset Management, etc.</p>

<h2>Tentative Plan</h2>
<p>
  2008-01 v0.1: Main Functional Business Components running on Eclipse OSGi server<br />
  2008-04 v0.2: Financial Mathematical libraries integration<br />
  2008-07 v0.5: Risk Models available and Risk Reporting deployed<br />
  2008-11 v1.0: Release 1.0 covering Financial Markets and Risk Management functionalities<br />
</p>
<p>As a further step the platform will be enriched by Risk Management components to evolve to a complete Risk Management Information System (MIS).</p>

<h2>Liaison with Industry Standards Groups</h2>
<ol>
  <li>
    The Financial Information eXchange (FIX) Protocol is a
    messaging standard developed specifically for the real-time electronic
    exchange of securities transactions. FIX is a public-domain specification
    owned and maintained by FIX Protocol, Ltd.<br />
    <a href="http://www.fixprotocol.org">www.fixprotocol.org</a>
  </li>
  <li>
    The Financial products Markup Language (FpML®) is the
    industry-standard XML protocol for complex financial products.<br />
    <a href="http://www.fpml.org">www.fpml.org</a>
  </li>
  <li>
    SWIFT messaging supplies secure, standardised messaging services and
    interface software to nearly 8100 financial institutions in 207 countries.<br />
    <a href="http://www.swift.com">www.swift.com</a>
  </li>
  <li>
    Continuous Linked Settlement (CLS) is the market standard for foreign exchange settlement between major banks.<br />
    <a href="http://www.cls-group.com">www.cls-group.com</a>
  </li>
</ol>

<h2>Integration Points with Other Eclipse Projects</h2>
<p>
  The existing code base includes a number of components that make use or will
  make use of frameworks provided by existing Eclipse projects. The initial list
  of projects that may be used by the Open Financial Platform include:
</p>

<h3>Business Intelligence and Reporting Tools (BIRT)</h3>
<ul>
  <li>OFMP is expected to use <b>BIRT</b> for its Business Reporting needs.</li>
</ul>
<h3>Data Tools Platform (DTP)</h3>
<ul>
  <li><b>Model Base</b> project to provide a model of relational database schemas and structures.</li>
  <li><b>Connectivity</b> project, which provides components for defining, connecting to, and working with data sources.</li>
</ul>
<h3>Device Software Development Platform</h3>
<ul>
  <li><b>eRCP</b> project to provide client-deployment in hand-held devices.</li>
</ul>
<h3>Eclipse</h3>
<ul>
  <li><b>Equinox</b> project to implement both client-side and server-side OSGi services as evolved by the OSGi Enterprise Expert Group.</li>
</ul>
<h3>SOA Tools</h3>
<ul>
  <li><b>SOA Tools</b> will provide support for service-based communication with other EIS.</li>
</ul>
<h3>Eclipse Technology Project</h3>
<ul>
  <li><b>Dynamic Languages Toolkit</b> project to provide tooling for the RCML language (<a href="http://www.rcml.net">www.rcml.net</a>)</li>
  <li><b>Java Workflow Tooling</b> project for Financial Workflows modeling.</li>
  <li><b>Maya</b> project for profile-based client deployment.</li>
  <li><b>Eclipse Persistence Services</b> project for interaction with relational databases, XML, and Enterprise Information Systems (EIS).</li>
</ul>

<h2>Code Contributions</h2>
<p>
  Kaupthing Luxembourg offers its Forex Financial Markets Middle Office Platform
  code base and tests for consideration by the project as an initial starting
  point. This platform has been developed internally by Kaupthing Luxembourg
  since April 2006 and in collaboration with Weigle Wilczek GmbH since October
  2006.
</p>

<h2>Other Open Source projects used in current code</h2>
<ul>
  <li>Rich Client Markup Language (<a href="http://www.rcml.net">www.rcml.net</a>)</li>
  <li>Spring Framework</li>
  <li>ACEGI Security</li>
  <li>Quartz scheduler</li>
  <li>ObjectLab DateCalc</li>
  <li>iBatis, XML-based object/relational mapping and persistence framework as well as a data access abstraction layer.</li>
</ul>

<h2>Organization</h2>

<h3>Kaupthing Project Initiator</h3>

<p>
Mr. Alexandre Simon<br />
Director, Head of Financial Markets<br />
Kaupthing Bank Luxembourg<br />
Phone:: +352 46.31.31.212<br />
Fax: +352 46.31.31 224<br />
Web:<a href="http://www.kaupthing.lu">www.kaupthing.lu</a>
</p>

<h3>Initial Participants</h3>

<table style="border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">
	<tr style="vertical-align:top; text-align:left; ">
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">
			Mr. Frederic Conrotte<br />
			Software Architect - Analyst<br />
			Kaupthing Bank Luxembourg S.A.<br />
			Phone: +352 46.31.31.548<br />
			Fax: +352 46.31.31 224<br />
			E-Mail: frederic dot conrotte at kaupthing.lu<br />
			Web: <a href="http://www.kaupthing.lu">www.kaupthing.lu</a>
		</td>
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">Co-Project Lead - System Integration, Committer</td>
	</tr>
	<tr style="vertical-align:top; text-align:left;">
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">
			Mr. Fabian De Keyn<br />
			Financial Engineer - Market Risk manager<br />
			Kaupthing Bank Luxembourg S.A.<br />
			Phone: +352 46 31 31 683<br />
			Fax: +352 46.31.31 224<br />
			E-Mail: fabian dot dekeyn at kaupthing.lu<br />
			Web: <a href="http://www.kaupthing.lu">www.kaupthing.lu</a>
		</td>
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">Co-Project Lead - Financial Enginering, Committer</td>
	</tr>
	<tr style="vertical-align:top; text-align:left;">
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">
			Mr. Aleksey Aristov<br />
			Software Architect - Analyst<br />
			RCML Project Creator<br />
			Weigle Wilczek GmbH<br />
			Phone: +49 (0)711. 45 999 8 - 0<br />
			Fax: +49 (0)711. 45 999 8 - 29<br />
			E-Mail: aristov at weiglewilczek.com<br />
			Web: <a href="http://www.weiglewilczek.com">www.weiglewilczek.com</a>
		</td>
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">Co-Project Lead - System Architecture, Committer</td>
	</tr>
	<tr style="vertical-align:top; text-align:left;">
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">
			Mr. Stephan Wilczek<br />
			Managing Partner<br />
			Weigle Wilczek GmbH<br />
			Phone: +49 (0)711. 45 999 8 - 0<br />
			Fax: +49 (0)711. 45 999 8 - 29<br />
			E-Mail: wilczek at weiglewilczek.com<br />
			Web: <a href="http://www.weiglewilczek.com">www.weiglewilczek.com</a>
		</td>
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">Project Coordination - Ecosystem Development, Committer</td>
	</tr>
	<tr style="vertical-align:top; text-align:left;">
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">
			Mr. Alexander Thurow<br />
			Software Developer<br />
			Weigle Wilczek GmbH<br />
			Phone: +49 (0)711. 45 999 8 - 0<br />
			Fax: +49 (0)711. 45 999 8 - 29<br />
			E-Mail: thurow at weiglewilczek.com<br />
			Web: <a href="http://www.weiglewilczek.com">www.weiglewilczek.com</a>
		</td>
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">Committer</td>
	</tr>
	<tr style="vertical-align:top; text-align:left;">
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">
			Mr. Sergey Vasiljev<br />
			Software Designer and Architect<br />
			Freelancer<br />
			Phone: +49 (0)163 6158713<br />
			E-Mail: serge dot vasiljev at gmail.com
		</td>
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">Committer</td>
	</tr>
	<tr style="vertical-align:top; text-align:left;">
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">
			Mr. Etienne Hermand<br />
			Financial Engineer<br />
			Phone: +352 26 31 12 91<br />
			E-Mail: ehermand05 at gsb.columbia.edu
		</td>
		<td style="width:8.498cm; border-style:solid; border-width:1px; border-color:black; border-collapse:collapse;">Contributor</td>
	</tr>
</table>

<p>Other participants are more than welcome and being actively sought.</p>
<p>We aim to attract 2 types of project contributors:</p>
<ul>
  <li>Contributors part of Financial Engineers Community (Business Analysts, Financial Mathematicians,...)</li>
  <li>Contributors part of Software Development Community</li>
</ul>


<h2>Interested parties</h2>
<p>The following organizations have expressed interest in joining the project:</p>
<ul>
  <li>Kaupthing Luxembourg S.A.</li>
  <li>Weigle Wilczek GmbH</li>
</ul>
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
