<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	
  <div id="midcolumn"> 
    <h1>Eclipse Persistence Services Project</h1>
</p>
    <?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("EPS");
?>
    <p>This proposal is in the Project Proposal Phase (as defined in the <a
href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process 
      document</a>) and is written to declare its intent and scope. This proposal 
      is written to solicit additional participation and input from the Eclipse 
      community. You are invited to comment on and/or join the project. Please 
      send all feedback to the EclipseLink newsgroup at 
      <a href="news:news.eclipse.org/eclipse.technology.eclipselink">news:news.eclipse.org/eclipse.technology.eclipselink.</a>
      </a> <a
	href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.eclipselink"><img
	src="http://www.eclipse.org/newsgroups/images/discovery.gif"
	alt="Web interface" title="Web interface"></a></p>
    <p>The Eclipse Java Persistence Platform (EclipseLink) project is a proposed incubator 
      project within the Technology Top Level Project of the Eclipse Foundation. 
    </p>
    <h2>Overview</h2>
    <p>EclipseLink, the Eclipse Java Persistence Platform project, will provide an 
      extensible framework that will enable Java developers to interact with relational 
      databases, XML, and Enterprise Information Systems (EIS). EclipseLink will 
      provide support for a number of persistence standards including the Java 
      Persistence API (JPA), Java API for XML Binding (JAXB), Java Connector Architecture 
      (JCA), and Service Data Objects (SDO).
      It is our intention to work with 
      the OSGi expert group to create a set of blueprints that illustrate the 
      use of these prominent persistence standards within the OSGi platform. Guidelines 
      for persistence usage within OSGi will enable OSGi-based applications to 
      access all types of enterprise data through existing standard APIs. The 
      combination of standardized persistence with the OSGi services framework 
      will bring increased portability and improved modularity to applications 
      and confirm the value proposition of merging these two technologies.</p>
    <h2>Description</h2>
    <p>Java standards for data access have been evolving on various fronts with 
      the latest milestone being the completion of the Service Data Object 2.1 
      specification, which defines a standard for the exchange of XML-based information 
      in Service Component Architecture applications.
      The Java Persistence API 
      for persisting Java POJOs to relational databases was also recently completed 
      and has garnered widespread industry and developer support.
      The emergence 
      of OSGi as an evolving Java standard in the enterprise space particularly 
      in combination with Spring is an additional recent development. </p>
    <p>The development of these standards provides Java developers with the means 
      to build portable Java applications that interact with relational databases 
      and other systems (both Java and non-Java based).
      However these are specifications, not implementations.
      The Eclipse Java Persistence Platform project will provide 
      implementations of Java standard persistence APIs for developing applications 
      that persist Java objects to relational databases, XML, and to Enterprise 
      Information Systems.
      The fundamental goal of the EclipseLink project is 
      to develop an extensible framework that provides a &quot;link&quot; between the Java 
      world of objects and other models including relational and XML.</p>
<h2>Scope</h2>
    <p>The Eclipse Java Persistence Platform will provide an extensible framework that 
      will provide JPA, JAXB, and SDO persistence as well as customizations for 
      other persistence mechanisms.� The following initial component frameworks 
      are planned:</p>
    <p> <b><u>EclipseLink-ORM</u></b> will provide an extensible Object-Relational 
      Mapping (ORM) framework with support for the Java Persistence API (JPA).
      It will provide persistence access through JPA as well as having extended 
      persistence capabilities configured through custom annotations and XML. 
      These extended persistence features include powerful caching (including 
      clustered support), usage of advanced database specific capabilities, and 
      many performance tuning and management options.</p>
    <p><b><u>EclipseLink-OXM</u></b> will provide an extensible Object-XML Mapping 
      (OXM) framework with support for the Java API for XML Binding (JAXB). It 
      will provide serialization services through JAXB along with extended functionality 
      to support meet in the middle mapping, advanced mappings, and critical performance 
      optimizations.</p>
    <p><b><u>EclipseLink-SDO</u></b> will provide a Service Data Object (SDO) 
      implementation as well as the ability to represent any Java object as an 
      SDO and leverage all of its XML binding and change tracking capabilities.</p>
    <p><b><u>EclipseLink-DAS</u></b> will provide an SDO Data Access Service (DAS) 
      that brings together SDO and JPA.</p> 
     <p><b><u>EclipseLink-DBWS</b></u> will provide a web services 
      capability for developers to easily and efficiently expose their underlying 
      relational database (stored procedures, packages, tables, and ad-hoc SQL) 
      as web services. The metadata driven configuration will provide flexibility 
      as well as allow default XML binding for simplicity.</p>
    <p><b><u>EclipseLink-XR</u></b> will deliver key infrastructure for situations 
      where XML is required from a relational database. The metadata driven mapping 
      capabilities of EclipseLink-ORM and EclipseLink-OXM are both leveraged for 
      the greatest flexibility. Using this approach to XML-Relational access enables 
      greater transformation optimizations as well as the ability to leverage 
      the Eclipse Java Persistence Platform's shared caching functionality. While this 
      capability is primary infrastructure for DBWS and DAS it will also be possible 
      for consuming applications to leverage this directly.</p>
    <p><b><u>EclipseLink-EIS</u></b> provides support for mapping Java POJOs onto 
      non-relational data stores using the Java Connector Architecture (JCA) API.</p>
      <p align="center"><img src="eclipselink.gif" width="555" height="334"></p>
    <h2>Project Principles</h2>
    <p>Among the key principles on which this project has been initiated, and 
      will be run, are the following: <br>
      <br>
      <b>Leverage the Eclipse Ecosystem</b>
      It is our goal to extend the Eclipse 
      ecosystem as well as leverage other Eclipse project functionality where 
      applicable.</p>
    <p><b>Extensibility</b>
      It is our intention to develop a set of extensible 
      frameworks that implement individual specifications and that provide the 
      ability to add features that extend the base specification (e.g., advanced 
      mappings and query features).</p>
    <p><b>Incremental Development</b>
      An agile development process will be employed 
      to manage and respond to requirements.</p>
    <p><strong>Vendor Neutrality</strong>
      We intend to develop frameworks that 
      are not biased toward any vendor.
      The Eclipse Java Persistence Platform will 
      provide relational persistence with any relational database, integration 
      with any Enterprise Information System providing a JCA connector, and deployment 
      into any application server, OSGi container, and Java runtime environment.<strong></strong></p>
    <p><strong>Inclusiveness</strong>
      We aim to encourage all interested parties 
      and vendors to participate and contribute to the project in all areas.
      We recognize the value of working with as many parties as possible on open 
      source Eclipse-based frameworks and technologies.</p>
    <h2>Integration Points with Other Eclipse Projects</h2>
    <h3>Building on the Eclipse Ecosystem</h3>
    <p>The existing Oracle TopLink code base includes a number of components that 
      could leverage frameworks provided by existing Eclipse projects.
      A detailed 
      analysis will be performed during the Validation phase of the project.
      The initial list of projects that may be leveraged by the Eclipse Java Persistence 
      Platform include:</p>
    <h4><b><u>Equinox</u></b><u></u></h4>
    <p>EclipseLink is expected to implement new OSGi services as evolved by the 
      OSGi Enterprise Expert Group. </p>
    <h4><b><u>Data Tools Platform (DTP)</u></b></h4>
    <ul type=square>
      <li>
        <p> <b><u>Model Base</u></b> project to provide a model of relational 
          database schemas and structures.</p>
      </li>
      <li>
        <p> <b><u>Connectivity</u></b> project, which provides components for 
          defining, connecting to, and working with data sources.</p>
      </li>
    </ul>
    <h4><strong><u>Web Tools Platform (WTP)</u></strong></h4>
    <ul type=square>
      <li>WTP provides some support for JCA. EclipseLink-EIS may be able to leverage 
        this support.<b><u></u></b></li>
    </ul>
    <h4><u>SOA Tools</u></h4>
    <ul type=square>
      <li>SOA Tools will provide WSDL editing support that can be leveraged by 
        EclipseLink-DBWS.<b><u></u></b></li>
    </ul>
    <h3>Contributing to the Eclipse EcoSystem</h3>
    <p>The availability of a number of persistence frameworks could be both leveraged 
      by tooling projects and consumed by them in the construction of applications.</p>
    <h4>WTP Dali JPA Tools project</h4>
    <ul type=square>
      <li>JPA mapping validation</li>
      <li>JPA query code assist and validation</li>
      <li>DDL generation</li>
    </ul>
    <h4>WTP Web Services</h4>
    <ul type=square>
      <li>Incorporate EclipseLink-DBWS for the construction of Web Services that 
        access relational database data.</li>
    </ul>
    <h4>SOA Tools</h4>
    <ul type=square>
      <li>Incorporate EclipseLink-DBWS for the construction of Web Services that 
        access relational database data.</li>
      <li>Incorporate EclipseLink-SDO for the construction of Service Component 
        Architecture applications</li>
      <li>Provide a JAXB implementation for development of JAX WS applications.</li>
    </ul>
    <h2>Code Contributions</h2>
    <p>Oracle offers its Oracle TopLink code base and tests for consideration 
      by the project as an initial starting point.</p>
    <h2>Interested Parties</h2>
    <p>Oracle, as the submitter of this proposal, welcomes interested parties 
      to post to the EclipseLink newsgroup and ask to be added to the list as 
      interested parties or to suggest changes to this document. </p>
     <p>A number of companies have expressed interest in the project and/or its components thus far.
      This list will be updated periodically to reflect the growing interest in EclipseLink:</p>
      <ul type=disc>
      <li>Genuitec LLC</li>
      <li>Innoopract</li>
      <li>Interface21</li>
      <li>Oracle Corporation</li>
      <li>TmaxSoft</li>
      </ul>
    <h2>Initial Committers</h2>
    <p>The following companies will contribute initial committers to get the project 
      started:</p>
    <ul type=disc>
      <li>Oracle Corporation</li>
    </ul>
    <h2>Developer community</h2>
    <p>With a large code base contribution from Oracle a number of developers 
      from the Oracle TopLink development team will be initial committers, however 
      we also expect the initial set of committers to include developers from 
      the other interested parties.
      We will make it easy for interested parties 
      to take an active role in the project by making our planning and process 
      transparent and remotely accessible.&nbsp; Contributions will be appreciated 
      and we will encourage all active contributors to become committers.</p>
</div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
