<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "EMF Index";
$pageKeywords	= "EMF,Modeling,Index";
$pageAuthor		= "Jan K&ouml;hnlein";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>EMF Index</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("EMF Index");
?>

<h2>Introduction</h2>
<p>EMF Index is a proposed open source project under the <a HREF="/modeling/emft/">Eclipse
Modeling Framework Technology Project</a> (EMFT).</p>
<p>This proposal is in the Project Proposal Phase (as defined in the
<a HREF="/projects/dev_process/">Eclipse Development Process
document</a>) and is written to declare its intent and scope. This
proposal is written to solicit additional participation and input
from the Eclipse community. You are invited to comment on and/or join
the project. Please send all feedback to the
<a HREF="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft</a>
newsgroup.</p>
<h2>Background</h2>
<p>Many development tools maintain an index on the processed
artifacts to allow fast and scalable queries for elements that match
certain criteria. In the Java Development Toolkit (JDT) for example,
the internal Java models are indexed to quickly find calls of a
method, navigate to the declaration of a field or find all subclasses
of a given class. It also allows to find classes whose names match a
wildcard pattern.</p>
<p>EMF Index will provide an index for modeling tools based on the
Eclipse Modeling Framework (EMF). That index will automatically
collect and synchronize index data on Ecore models and instances. It
will allow fast queries without the need to have EMF resources
instantiated in memory, thereby enabling scalable modeling tools
based on EMF.</p>
<h2>Scope</h2>
<p>The objectives of the EMF Index project are the following: 
</p>
<ul>
	<li>Define APIs and implement
	components for specifying search scopes, collecting and persisting
	index data, synchronization of index data with model changes, and
	querying of model elements. 
	</li>
	<li>Increase the usability of
	EMF-based modeling tools by providing a JDT-like user interface for
	following references, finding elements by type and querying scoped
	elements. This can also be the foundation of model refactoring 
	support.
	</li>
	<li>Increase the scalability of
	modeling tools by enabling queries on models that are not loaded.
	This will allow the processing of bigger models and a greater number
	of model resources. It will also help to deal with temporarily
	unavailable models, e.g. resources fetched from the network. 
	</li>
	<li>Integrate with other Eclipse components, e.g. the searching 
	support. 
	</li>
	<li>Provide a platform to collect and discuss requirements and
	use cases for model indexing. 
	</li>
</ul>
<p>EMF Index will not cover these issues: 
</p>
<ul>
	<li>Deal with models not based on EMF.</li>
	<li>Implement a model repository. Only specific, pre-selected
	properties of model elements should be stored in the index. 
	</li>
</ul>
<h2>Description</h2>
<p>EMF Index will have to define APIs and provide implementations for
the following functionality: 
</p>
<dl>
	<dt>Index scope 
	</dt><dd>
	EMF follows an open-world paradigm, i.e. it is possible to load
	every resource that is accessible, locally or from the network. 
	While that is fine for on-demand loading of referenced elements, a 
	search operation needs a confined scope. Clients must have a way 
	define the scope of indexed elements, e.g. as the resources in 
	their workspace.
	<dt>
	Collection of index data 
	</dt><dd>
	Retrieve index data from a given EMF resource. The index data will
	consist of generic data, e.g. the type of an element, as well as
	model-specific data, e.g. a collection of attributes that constitute
	a fully-qualified name. Any model-specific data can be stored in the 
	index. There will be a way to specify which data will be retrieved 
	from the EMF model. This also includes the definition of
	model-specific scopes for queries. 
	</dd><dt>
	Persistence 
	</dt><dd>
	Store the collected index data independently from the indexed
	resources in such a way that it can be queried easily later.</dd><dt>
	Synchronization of index data 
	</dt><dd>
	Whenever it is possible, detect model changes and update the model
	index automatically. 
	</dd><dt>
	Queries
	</dt><dd>
	Queries should consist of a set of criteria and a query scope. The
	results of a query must be descriptors (e.g. EMF proxies or fragment
	URIs) of model elements rather than the elements themselves, such
	that the client decides whether to load them or not. The descriptors 
	can carry any additional information on the element available in the 
	index.
	</dd>
	<dt>
	Exchangeable storage backend
	</dt><dd>
	The backend to actually store the index data will be exchangeable,
	allowing customized implementations for specific storage technologies,
	such as pure in-memory or database storage of index data. 
	</dd>
</dl>
<h3>
Relationship with other Eclipse Projects</h3>
<ul>
	<li>EMF Index will be built on top of EMF.</li>
	<li>As EMF Index will be generic, it will be usable by any 
	EMF-based framework, not depending on the specific concrete syntax.
	</li>
	<li>TMF/Xtext is expected to make
	strong use of EMF Index. As models are described textually in Xtext,
	a common problem is to resolve/find a model element by some
	domain-specific textual description. An index taking these
	descriptions into account will be a great benefit. 
	</li>
	<li>EMF Index will make contributions to Eclipse search. 
	</li>
</ul>
<h2>Organization</h2>
<h3>Mentors</h3>
<ul>
<li>Ed Merks (Macro Modeling, Canada)</li>
<li>Sven Efftinge (itemis AG, Germany)</li>
</ul>

<h3>Proposed initial committers</h3>
<ul>
	<li>Jan K&ouml;hnlein (itemis AG, Germany), lead </li>
	<li>Sven Efftinge (itemis AG, Germany)</li>
	<li>Dennis H&uuml;bner (itemis AG, Germany), release engineering</li>
	<li>Bernd Kolb (Independent, Germany)</li>
	<li>Jos Warmer (Ordina, Netherlands)</li> 
</ul>

<h3>Interested parties</h3>
<p>The need for a model index has been discussed in the Modeling
Symposium at the Eclipse Summit Europe 2009. 
</p>
<p>Parties indicating interest were 
</p>
<ul>
	<li>Macro Modeling (Canada), Ed Merks</li>
	<li>Geensys (France), Stephan Eberle</li>
	<li>paranor (Switzerland), Knut Wannheden</li>
	<li>SAP (Germany), Boris Gruschko</li>
	<li>ikv++ (Germany), Matthias Erche</li>
</ul>
<h3>Code contributions</h3>
<p>The initial code contribution will be a set of plug-ins from the
TMF/Xtext Eclipse project. 
</p>
    
      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
