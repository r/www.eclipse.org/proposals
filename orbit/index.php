<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>The Eclipse Orbit Project</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("The Eclipse Orbit Project");
?>

<h2>Introduction</h2>
<p>The Eclipse Orbit Project is a proposed open source project under the <a href="http://www.eclipse.org/tools/">Eclipse
    Tools Project</a>.</p>
    
<p>This proposal is in the Project Proposal Phase (as defined in the <a href="http://www.eclipse.org/projects/dev_process/">Eclipse
    Development Process</a> document) and is written to declare its intent and
    scope. This proposal is written to solicit additional participation and input
    from the Eclipse community. You are invited to comment on and/or join the
    project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.orbit">eclipse.tools.orbit
    newsgroup</a>.</p>
    
  <h2>Background</h2>
<p>The Eclipse community is increasingly embracing and using code from other
    open source projects. Traditionally each Eclipse project has submitted requests
    to use the libraries they need and, upon approval, the libraries have been
    integrated into the project's plug-ins and shipped. This situation presents
  a number of challenges.</p>
  
  <p><strong>Bundlings:</strong> There are often several ways of &quot;bundling&quot; a
    third party library. A JAR can be wrapped in a bundle or have bundle metadata
    injected
    into it. Bundle names and versions must be selected. Packages must be exported
    with the appropriate visibilities, directives and versions. Libraries that
    consist of multiple JARs may be packaged as one bundle or with a bundle
    for each JAR. In fact, libraries might be simply included directly in an
    existing bundle of the consuming project. Since each team performs this packaging
    independently, the chances are high that
    the approaches taken will diverge. This leads to confusion and challenges
    when composed systems contain incompatible bundlings of the same libaries.</p>
  <p><strong>Componentization:</strong> Often times a &quot;library&quot; is
    actually a composition of other libaries with some additional function. These
    internal libraries
    are often interesting in and of themselves. For example, in Eclipse 3.2 the
    Tomcat plug-in also includes various Apache Commons libraries (logging, collections,
    modeler, etc.), mx4j, Jakarta regexp, servlet API and Jasper. All of these
    JARs are interesting and useful, independent of Tomcat. Under the current
    approach, when a team requests the use of Tomcat, the function as a whole
    is analyzed and approved rather then looking at the individual components.
    In some cases components are not even needed for the required use of the
    requested function. For example, mx4j is not really needed for the Eclipse
    Help use of Tomcat. Further, since these common libraries appear in many
    configurations, they are repeatedly reviewed and analyzed as teams request
    approval for these different configurations.</p>
  <p><strong>Sharing:</strong> Even with a common vision as to how libraries
    should be converted to bundles, each team must duplicate the packaging effort
    and maintenance. Further,
    since the current approach is compartmentalized, it often happens that project
    teams are unaware of the libraries used by each other. This results in gratuitous
    duplication of content in the Eclipse downloads (e.g. Callisto contains
    several copies of Xerces and commons logging, etc.) and version misalignments.
    The lack of transparency also clogs the IP clearance process since teams
    do not know that others are already using the library they need or one which
    provides equivalent function.</p>
  <p><strong>Community:</strong> The Eclipse committer community is not unique
    in this need for bundled versions of existing libraries. Many systems built
    by others include
    the very same code. Again, these libraries are bundled by different people
    (duplicating effort and encouraging divergence) and managed and delivered
    separately (resulting in incompatibilities) and raising the bar for Eclipse
    adoption.</p>
  <h2>Scope</h2>

  <p>This project is intended to be</p>
  <ul>
    <li>a source of approved and bundled content for qualifying Eclipse projects
      and the community as a whole</li>
  </ul>
  <p>This project is <strong>not</strong> intended to</p>
  <ul>
    <li>support code development</li>
    <li>replace the IP process</li>
    <li>do all the packaging work for teams</li>
  </ul>
  <h2>Description</h2>

  <p>This project will provide a repository of bundled versions of
    third party libraries that are approved for use in one or more Eclipse projects.
    The repository will maintain old versions of such libraries to facilitate
    rebuilding historical output. It will also clearly indicate the status of
    the library (i.e. the approved scope of use). The repository will be structured
    such that the contained bundles are easily obtained and added to a developer's
    workspace or target platform.</p>
  <p>One of the key issues with the current situation
      is the inconsistency in naming, versioning and form of bundled third
    party libraries.
      This project
      will, as a mainline activity, provide a repository of approved and bundled
      third party libraries to authorized project teams. This eliminates duplicated
      work and bundle naming, structuring and versioning variations. </p>
  <p>Crucially, no development is carried out in this project. Teams proposing
    the use of a third party library for the first time are able to work with
    the community developed here and other potential users of the library to
    derive an appropriate selection and packaging of the function they need.
    For
    example,
    a team
    may start
    out
    with &quot;a need
    for Jetty.&quot; As we have seen, systems like Jetty often include several
    JARs and a wide variety of infrastructure pieces (e.g. Apache Commons Logging).
    In some cases this additional function is not needed, is already approved
    and available, or has
    special
    packaging needs. Through this project the teams needing function can arrive
    at a mutually agreeable form of the required function. </p>
  <p>It is important to
      note that this project will not be responsible for doing
      the packaging work. That responsibility remains with the teams seeking
    to use the requested library. This project is to act as a hub or focal point
    for people with needs related to third party libraries and those with related
    technical expertise.</p>
  <p>The bundled libraries are retained and made available within the project
    in various ways (e.g. download zip, update site, etc.) for use in the projects
    that
    have
    received
    the appropriate approvals. Note that teams seeking to use a library
      must
    still follow the IP process. For example, the current process calls for all
    third party code use to be cleared by the Foundation. So even if a
    library is approved by the Foundation for use by all projects, project teams
    must notify the Foundation
        of their intentions to use a library. This allows their use to be tracked
    and facilitates version
    synchronization
    (e.g.
    stepping
        up to new releases) as well as notification of issues found with approved
    and previously distributed libraries.</p>
  <h2>Existing standards and projects leveraged</h2>

  <p>This project will mainly use the existing developer infrastructure (e.g.
    CVS, Bugzilla, etc.) to manage library contribution requests and the libraries
    themselves.</p>

  <h2>Organization</h2>

  <p>This project will be organized into one top-level component:</p>

  <ul>
  <li>Bundles: bundlings of approved third party libraries in orbit around
    Eclipse.</li>

  </ul>

    <h3>Proposed initial committers and project lead</h3>
    
<p>The initial committers will be drawn from the Eclipse developer community.
  Developers with experience with, interest in, and/or an ongoing need for
  shared code and bundling are potential committers
  on this project. The initial committers are:
  
<ul>
  <li>Jeff McAffer (Project Lead)</li>
  <li>Pascal Rapicault </li>
  <li>David Williams</li>
  <li>Simon Kaegi</li>
  <li>Bjorn Freeman-Benson</li>
  <li>John Graham</li>
</ul>


  <p>Potential commiters are kindly invited to express their interest
    on the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.orbit">eclipse.tools.orbit
    newsgroup</a> or via email.</p>
  <p>As projects propose the use of additional third party libraries and their
    use is approved, one of their team will add the bundled libraries to the
    Orbit project. If they
    do not already have a committer on Orbit, they will be able
    to petition for a committer to be added on the basis that they are making
    an initial code contribution to the project. They will also commit to ongoing
    maintenance of that contribution as well as interacting with other Eclipse
    projects which have an interest in the same code base.</p>
  <h3>Interested parties</h3>
  <p>Interest has been expressed by the following projects:</p>
 <ul>
 <li>BIRT</li>
 <li>DTP</li>
 <li>WTP</li>
 <li>Corona</li>
 <li>Dash</li>
 </ul>
 <p>To express support, concern, or constructive opinions regarding the formation
   of this proposed Tools project, you are encouraged to utilize the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.orbit">eclipse.tools.orbit
    newsgroup.</a>
   </p> 


    <h3>Initial contribution</h3>

    <p>The team will initially look at the set of libraries currently approved
      for use in Eclipse (e.g. those included in Callisto) and attempt to rationalize
      their form. Below is an example list of the bundles to be added/maintained
      in Orbit:</p>
    <ul>
      <li>Various Apache Commons projects including Logging, HTTP Client, Codec</li>
      <li>Apache XMLRPC</li>
      <li>Log4J</li>
      <li>Servlet API 2.3 and 2.4</li>
      <li>MX4J</li>
      <li>Jasper</li>
</ul>
    <h2>Roadmap</h2>

  <p>This project has no particular deliverables so a standard milestone-based
    roadmap does not make sense here. Having said that, we expect to have an
    initial set of library entries in the database and bundles in the repository
    within the first three months of this project being provisioned. As the goal
    of Orbit is to support the other Eclipse projects through common shared bundles,
    the Orbit Roadmap will be coordinated with significant releases of the major
    Eclipse projects.</p>
  <p>&nbsp;</p>


</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
}