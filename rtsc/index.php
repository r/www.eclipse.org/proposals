<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Real-Time Software Components (RTSC)</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("RTSC");
?>

<p><h1><a name="Project_Proposal_Real_Time_Softw"></a><a name="_Project_Proposal_Real_Time_Soft"></a>  Project Proposal: Real-Time Software Components (RTSC) </h1>
<p />
<p />
<div class="twikiToc"><span class="twikiTocTitle"><h3>Contents</h3></span> <ul>
<li> <a href="#Real_Time_Software_Components_fo"> Real-Time Software Components for Embedded Systems</a>
</li> <li> <a href="#Summary"> Summary</a>
</li> <li> <a href="#Background"> Background </a> <ul>
<li> <a href="#The_RTSC_Model"> The RTSC Model</a>
</li></ul> 
</li> <li> <a href="#How_RTSC_fits_into_the_Eclipse_E"> How RTSC fits into the Eclipse Ecosystem</a>
</li> <li> <a href="#Scope"> Scope</a> <ul>
<li> <a href="#RTSC_Overview"> RTSC Overview</a> <ul>
<li> <a href="#Core_Concepts"> Core Concepts</a>
</li> <li> <a href="#Core_Architectural_Elements"> Core Architectural Elements</a>
</li></ul> 
</li></ul> 
</li> <li> <a href="#Organization"> Organization</a>
</li> <li> <a href="#Mentors"> Mentors</a>
</li> <li> <a href="#Proposed_Project_Lead_and_Initia"> Proposed Project Lead and Initial Committers</a>
</li> <li> <a href="#Interested_Parties"> Interested Parties</a>
</li> <li> <a href="#Code_Contributions"> Code Contributions</a>
</li> <li> <a href="#Participation"> Participation</a>
</li> <li> <a href="#References"> References</a> <ul>
<li> <a href="#Web_Links"> Web Links</a>
</li></ul> 
</li></ul> 
</div>
<p />
<p />
<h2><a name="Real_Time_Software_Components_fo"></a> Real-Time Software Components for Embedded Systems </h2>
<p />
The Real-Time Software Components (RTSC) project is a proposed open source project under the <em><a href="http://www.eclipse.org/dsdp/" target="_top">Device Software Development Platform</a></em> project.
<p />
This proposal is in the Pre-Proposal Phase as defined in the 
<a href="http://www.eclipse.org/projects/dev_process/development_process.php" target="_top">Eclipse Development 
Process</a> document and follows the <a href="http://www.eclipse.org/projects/dev_process/pre-proposal-phase.php" target="_top">Eclipse Proposal Guidelines</a>.  It 
is written to declare the intent and scope of the project and to solicit additional 
participation and input from the Eclipse and embedded developer community. 
You are invited to comment on and/or join the project. Please send all feedback to the<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.dsdp.rtsc"> http://www.eclipse.org/newsportal/thread.php?group=eclipse.dsdp.rtsc</a> newsgroup. </p>
<p />
<h2><a name="Summary"></a> Summary </h2>
The RTSC project is focused on developing Eclipse tools for the development and configuration of C/C++ applications from components for highly constrained devices such as Digital Signal Processors (DSPs) and micro-controllers. RTSC supports a C-based programming model for developing, delivering, and deploying embedded real-time software components targeted for diverse highly resource-constrained hardware platforms. To meet the size and performance constraints of DSPs and 16-bit micro-controllers, RTSC focuses on development-time and configuration-time tooling to generate highly-optimized C/C++ applications. Unlike typical Java runtimes, there is little to no infrastructure that needs to be pre-deployed onto a device for RTSC to work. In addition to a component's C/C++ runtime code, each component includes code - written in JavaScript - that runs both in the component's development environment during application assembly and in rich client platforms to monitor the execution of the C/C++ code within a deployed application.
<p />
Component developers use a set of Eclipse-based tools to specify components using an ANTLR-based IDL, implement the components using both C/C++ and JavaScript.  The JavaScript implementation part of a component runs on top of <a href="http://www.mozilla.org/rhino/" target="_top">Rhino</a> and enables the component to actively participate in all stages of its lifecycle, from its initial assembly into an application to the real-time monitoring of its execution within deployed end-equipment.
<p />
Because each RTSC component contains an element that runs on rich client platforms, these components can participate in and leverage "traditional" Java-based component environments while still satisfying the resource constraints of its embedded C/C++ element running within the embedded device.  Beyond the obvious <a href="http://www.eclipse.org/cdt/" target="_top">CDT</a> integration possibilities, this dual existence for RTSC components opens the possibility to leverage and extend other Eclipse projects including <ul>
<li> integration with <a href="http://www.eclipse.org/tptp/" target="_top">TPTP</a> to enable the test of deeply the embedded C content;
</li> <li> use of the Device Kit portion of the <a href="http://www.eclipse.org/ohf/components/soda/" target="_top">SODA</a> project (which has similar needs for monitoring);
</li> <li> the monitoring and data collection components of <a href="http://www.eclipse.org/cosmos/" target="_top">COSMOS</a>; and
</li> <li> use of <a href="http://www.eclipse.org/modeling/emf/" target="_top">EMF</a> to enable specification of components using existing UML tools but generating RTSC components that run in highly-resource constrained environments.
</li></ul> 
<p />
<h2><a name="Background"></a> Background </h2>
Component based development has proven itself to have significant benefits in the enterprise IT and web-based environments; complex applications are quickly created and very few, if any, are created from scratch  &#8211; they all leverage a rich set of third-party open-source or commercial components. Beyond object-oriented language support, component models address all phases of the software lifecycle and standardize software abstractions to the point where &#8211; without any ad-hoc conventions or schedule coordination among the participants &#8211; interfaces defined by one company can be implemented by a second and used by a third.  Moreover, tools that leverage the additional structure imposed by this standardization facilitate the development and (re)use of components, further accelerating the creation of applications assembled from components created by third-parties. 
<p />
Although real-time embedded systems are increasingly being developed with object-oriented languages and techniques, to enable the same level of cross-company reuse and rapid application development, a component model and supporting tools are needed.  However, existing enterprise models (such as JavaBeans, .NET, Corba, etc.) do not address the unique challenges of embedded systems: <ul>
<li> <em>embedded platforms are extremely cost and power sensitive (<a href="#AnchorEmbeddedCPUPoll" class="twikiAnchorLink">Ganssle2006</a>):</em> to minimize cost and power consumption, a wide variety of CPUs, peripherals, and memories are employed with limited code space and MIPS capacity.
</li> <li> <em>embedded software must be "optimal":</em> to work within the constraints of small memory and relatively slow clock rates necessitated by the cost and power constraints, software must be as small and fast as possible.
</li> <li> <em>existing software is predominantly written in C and assembly language (<a href="#AnchorEmbeddedPoll" class="twikiAnchorLink">Nass2007</a>):</em> no standard definition of interfaces nor a common runtime that enables multiple implementations of an interface within a single application exists.
</li> <li> <em>no standard C/C++ compiler toolchain exists for all devices:</em> while GCC supports many CPUs, to achieve the necessary performance from their "portable" ANSI C code bases, developers must leverage C/C++ compilers from the device manufactures that achieve "optimal" performance for their devices.
</li></ul> 
<p />
Several component models have been created to meet these challenges (<a href="#AnchorEcos" class="twikiAnchorLink">ECOS Component Model</a>, <a href="#AnchorKoala" class="twikiAnchorLink">Koala</a>, <a href="#AnchorKnit" class="twikiAnchorLink">Knit</a>, <a href="#AnchorTinyOsNesC" class="twikiAnchorLink">TinyOS/nesC</a>, <a href="#AnchorRealtimeCorba" class="twikiAnchorLink">Real-time Corba</a>, <a href="#AnchorMinimalCorbaSpec" class="twikiAnchorLink">Minimal Corba</a>), but these models and their toolchains are often tied to a specific compiler, embedded operating system, embedded hardware platform, or host development platform.  In addition, the more sophisticated models can't be scaled down to support popular but resource constrained devices such as an <a href="http://en.wikipedia.org/wiki/8051" target="_top">Intel 8051</a> or a Texas Instruments <a href="http://en.wikipedia.org/wiki/MSP430" target="_top">MSP430</a>; for example, Corba all but requires a C++ runtime but &#8211; because of device memory constraints &#8211; no practical C++ support exists for the MSP430.  As a result, embedded developers can rarely leverage these models and can't afford to invest the time required to learn them.  Components created for these models can only be used in a limited number of embedded platforms, defeating the opportunity to reuse these components or the skills required to create them in more than just a few closely related projects.
<p />
<h3><a name="The_RTSC_Model"></a> The RTSC Model </h3>
   <blockquote> <em>Sometimes through heroism you can make something work.  However, understanding why it worked, abstracting it, making it a primitive is the key to getting to the next order of magnitude of scale.</em> &#8211; <a href="http://www.ee.princeton.edu/people/Calderbank.php" target="_top">Robert Calderbank</a></blockquote>
<p />
The RTSC model and tools enable development of components written in C using <em>any</em> compiler toolchain on <em>any</em> development host for <em>any</em> embedded platform.  These components can be configured, assembled, and optimized for use within any embedded real-time system.  By focusing on <em>design-time</em> rather than on runtime component assembly, the RTSC model and tools enable many of the component-based benefits to scale down to even the most resource constrained embedded system while leveraging existing C/C++ code bases and tool chains.
<p />
The RTSC tools, developed over a period of 7 years, are already in use by several Texas Instruments (TI) development groups and have been used to produce "mass market" products such as the <a href="http://focus.ti.com/docs/toolsw/folders/print/dspbios.html" target="_top">DSP/BIOS</a> Real-Time Operating System and the <a href="http://focus.ti.com/docs/toolsw/folders/print/tmdmfp.html" target="_top">Codec Engine</a> multi-media middleware framework.  While these products enjoy the benefits of not having to reinvent the capabilities provided by the RTSC tools, the value of these tools and the motivation to create new tools increases dramatically as adoption of RTSC increases.  However, wide-spread adoption is only possible if the model and base tooling are open and freely available.
<p />
<h2><a name="How_RTSC_fits_into_the_Eclipse_E"></a> How RTSC fits into the Eclipse Ecosystem </h2>
<p />
The Eclipse DSDP and Tools projects already contain many projects applicable to the development of embedded applications. The goal of the "Real-Time Software Components" project is to complement those existing projects (e.g. <a href="http://www.eclipse.org/cdt/" target="_top">CDT</a>), extend Eclipse to provide component based developed environment appropriate for virtually any embedded device with components implemented using C/C++, and encourage extensions of existing component based tools used by Java developers to apply to RTSC components.
<p />
The goal is to create a component development platform that can drive the embedded C programmer through the component lifecycle from design (modeling tools), development (C/C++ cross-compiling), testing (unit test frameworks), deployment (package creation tools), and installation (component selection and inter-component compatibility checking).  
<p />
To be successful, this project needs to foster the development of a rich set of both tools <em>and</em> target content.  Although TI and some of its third parties currently ship a variety of interesting RTSC components (an RTOS, multi-media middlware, and codecs), a correspondingly rich set of tools built atop the Eclipse platform together with an open RTSC core component model will greatly accelerate the creation of interesting components and component-based applications on a variety of embedded platforms (including non-TI platforms).
<p />
<h2><a name="Scope"></a> Scope </h2>
<p />
The goal for the RTSC project is to refine and standardize the core RTSC component model and foundational tools in an effort to bring component-based development advantages to <em>all</em> embedded C/C++ developers.  The elements included in this project are listed in the <a href="#CoreArchitecturalElements" class="twikiAnchorLink">Core Architectural Elements</a> section below.
<p />
By making this core infrastructure open, extensible, and freely available, we expect to seed additional projects that provide <ul>
<li> more sophisticated tools for component development: unit test frameworks, refactoring tools, etc.
</li> <li> integration with other popular embedded tools and languages: UML, <a href="http://www.doxygen.org/" target="_top">Doxygen</a>, static checking tools (e.g., <a href="http://www.coverity.com/html/prod_prevent.html" target="_top">Coverity Prevent</a> or <a href="http://www.klocwork.com/" target="_top">Klockwork</a>), etc.
</li> <li> alternative or domain-specific component composition tools; e.g., a <a href="http://www.eclipse.org/gef/overview.html" target="_top">GEF</a> based tool to create an application from existing components or a multi-core component development environment such as Zeligsoft's <a href="http://www.zeligsoft.com/uploadFiles/CE30-Data-Sheet.pdf" target="_top">CE 3.0</a> product
</li> <li> rich visualization of component-based applications: graphical representations of the relationships among constituent components, <a href="http://en.wikipedia.org/wiki/Dependency_Structure_Matrix" target="_top">Dependency Structure Matrix</a> tools, etc.
</li> <li> extensions of existing component-based tools enjoyed by the Java developer to support RTSC components
</li></ul> 
<p />
It is <em>not</em> a goal of this project to create the tools described above, rather the goal is to provide the common foundation to enable the creation of these more advanced capabilities by other groups.  We want nothing less than a robust component-based development platform suitable for <em>any</em> embedded system built atop Eclipse.
<p />
Each of these projects benefits from a standard underlying component model and will, if we are successful, bring a complete set of modern component-based tools to all embedded C/C++ developers.  In addition, these projects will help shape the roadmap of the core RTSC project by adding new requirements or uncovering new use-cases that need to be supported.
<p />
<h3><a name="RTSC_Overview"></a> RTSC Overview </h3>
<p />
The RTSC tools and component model have enjoyed continuous development since 2000 by a small group of senior embedded software developers.  Since 2004, the DSP/BIOS 5.x RTOS &#8211; created using the RTSC tools &#8211; has shipped along with the RTSC tools to ensure that any development system that included DSP/BIOS could consume components (called packages) created by any other development group.  Today, internal groups within Texas Instruments regularly (re)build, test, and deploy hundreds of RTSC packages.  Many of these packages are used worldwide by thousands of developers both inside and outside Texas Instruments. <ul>
<li> <a href="#AnchorBIOS" class="twikiAnchorLink">DSP/BIOS 5.x</a> &#8211; one of the most popular embedded RTOS's <a href="#AnchorEmbeddedRTOSPoll" class="twikiAnchorLink">(Turley2006)</a> &#8211; is deployed as a bundle of more than 56 packages, 
</li> <li> <a href="#AnchorCodecEngine" class="twikiAnchorLink">Codec Engine</a> multi-media middleware runtime (which requires DSP/BIOS) is an independently deployed bundle of more than 21 packages,
</li> <li> a wide variety of video, imaging, speech, and audio codecs &#8211; developed by both Texas Instruments and its third parties &#8211; are delivered as a packages, and
</li> <li> the RTSC toolset itself is delivered as a bundle of over 125 packages
</li></ul> 
<p />
The fact that developers have been using DSP/BIOS 5.x without realizing that it is, in fact, a collection of RTSC components illustrates one of the strengths of the RTSC model: consumers of RTSC components can easily integrate them without converting their entire application into components.
<p />
The RTSC tools currently provide basic support for the entire software development cycle: Install, Develop, Debug, and Deploy.   <ul>
<li> Install: <ul>
<li> package selection, compatibility checks, and side-by-side installation
</li></ul> 
</li> <li> Develop: <ul>
<li> side-by-side multi-target build with managed toolchains for both cross and native compilers
</li> <li> toolchain-independent package build specifications
</li> <li> component configuration and assembly tool
</li> <li> document generation from component specifications
</li></ul> 
</li> <li> Debug:  <ul>
<li> component-specific views of internal data structures
</li> <li> component compatibility checking
</li></ul> 
</li> <li> Deploy: <ul>
<li> component packaging tools
</li> <li> on-device real-time logging and diagnostics to monitor system activity
</li></ul> 
</li></ul> 
<p />
<h4><a name="Core_Concepts"></a> Core Concepts </h4>
The RTSC component model centers around three top-level concepts: <em>modules</em>, <em>interfaces</em>, and <em>packages.</em>  Roughly speaking, modules correspond to Java or C++ classes, interfaces correspond to Java interfaces, and packages correspond to Java jars.  Unlike Java, however, RTSC components provide code for two distinct environments: development hosts (with "unlimited" resources) and embedded runtime platforms (with very limited resources).  It is this ability for components to operate in and be "configured" on the development host that allows them to scale their runtime requirements to a level appropriate for each embedded system in which they operate.
<p />
<img alt="concepts.gif" src="rsrc/Main/RtscOpenSource/concepts.gif" />
<p />
More specifically: <ul>
<li> All content is logically and physically structured around a trio of programmatic constructs:   modules, which encapsulate a related set of types and functions, and have both an external specification and a concrete internal implementation; interfaces, which effectively become abstract modules &#8211; a specification without an implementation &#8211; that other modules and interfaces can inherit; and packages, which serve as general-purpose containers for modules and interfaces as well as other software artifacts including legacy content.
</li> <li> All content in the form of modules and interfaces exists in two complementary programming domains:  a target domain, where target-content is bound into an application program executing on a particular hardware platform; and a host-based <em>meta</em> domain, where associated meta-content plays an active role in the design-time configuration as well as the run-time analysis of target programs.
</li> <li> All content ultimately resides within individual packages that become the focal point for managing content throughout its life-cycle:  all packages are built, tested, released, and deployed as a unit; and while largely self-contained, packages will in general require the presence of other packages that are likewise identified by their globally-unique name and time-varying compatibility key.
</li></ul> 
<p />
<a name="CoreArchitecturalElements"></a>
<h4><a name="Core_Architectural_Elements"></a> Core Architectural Elements </h4>
The core RTSC tools and runtime support is currently available as a separate product, known as the "XDC Tools" or XDCTOOLS, from TI as a free-of-charge download (<a href="https://www-a.ti.com/downloads/sds_support/targetcontent/rtsc/xdc_3_00/index.html" target="_top">https://www-a.ti.com/downloads/sds_support/targetcontent/rtsc/xdc_3_00/index.html</a>).  After being relicensed under <a href="http://www.eclipse.org/org/documents/epl-v10.php" target="_top">EPL</a>, the entire XDTOOLS product will form the starting point of this project. 
<p />
XDCTOOLS is host development system independent and command-line centric; all of the core tools are command-line based and all GUI tools build atop these commands and interface with the user via the Eclipse <a href="http://www.eclipse.org/swt/" target="_top">Standard Widget Toolkit (SWT)</a>.  With the exception of a few performance sensitive utilities that are written in C, all parts of the toolset are implemented in Java or in JavaScript (executed via Mozilla's <a href="http://www.mozilla.org/rhino/" target="_top">Rhino</a> JavaScript engine).  Both Windows and *nix development hosts are actively supported. 
<p />
XDCTOOLS includes the following elements: <ul>
<li> Component Interface Definition Language (IDL) - to specify interfaces all components 
</li> <li> IDL to C/C++ and JavaScript language binding - to generate C/C++ headers and a meta-domain object model used by clients of a component at runtime and design-time, respectively
</li> <li> Component configuration and assembly tool - to enable rapid creation of applications from components at design-time
</li> <li> Package build tool - to create a deployable component (i.e., a RTSC package) from pre-built artifacts and support managed multi-target side-by-side builds of C/C++ and Java sources.
</li> <li> Embedded runtime support package - a scalable platform-independent runtime that includes printf-like diagnostic support, memory allocation, and basic concurrency control
</li> <li> Package documentation tool - to automatically generate online package reference documentation from IDL specifications
</li> <li> Package management tool - to select, check compatibility of, and install specified packages (and their prerequisites)
</li> <li> Component runtime display tool - to support component-specific views of in-the-field runtime objects managed by the component
</li></ul> 
<p />
All of the elements listed above are part of this project and, by virtue of being openly and freely available, will encourage adoption and extensions of the RTSC component model.  These elements form the foundation for extensions that span eclipse-based development tools to deeply embedded C-based runtime support and roughly fall into one of three "levels". <ol>
<li> <em>Language Support</em> forms the basis for all RTSC components and includes the IDL compiler and the JavaScript and C/C++ language bindings,
</li> <li> <em>Core Packages</em> includes over one hundred packages necessary to create, install, configure, and monitor embedded runtime content on a variety of platforms using virtually <em>any</em> C/C++ compiler, and
</li> <li> <em>Essential Utilities</em> includes both command-line and SWT based tools to view package documentation, manage installation and updates of packages, and view the state of a deployed embedded application. 
</li></ol> 
<p />
<a name="ProductFigure"></a>
<div><img src="rsrc/Main/RtscOpenSource/arch.bmp" alt="XDCTOOLS 10KM Architecture"/></div>
<p />
<strong>Language Support</strong>.&nbsp;  Virtually everything in RTSC starts with the RTSC IDL specification language.  The XDCTOOLS product itself includes an IDL translator used to first parse spec files and then (among other things) to generate corresponding C headers as well as client programmer documentation.
<p />
XDCTOOLS likewise delivers the JavaScript meta-language, integrated more closely with the IDL through an embedded version of Rhino/JavaScript also shipped with the product; besides relying heavily on JavaScript to support the build/release/deploy cycle of RTSC packages as well as the configure/execute/analyze cycle of RTSC programs, XDCTOOLS encourages use of the JavaScript meta-language as a general-purpose scripting engine that leverages the power and familiarity of JavaScript.
<p />
As for C itself, the XDCTOOLS product does not necessarily bundle any particular compiler tool-chain(s); indeed, the XDCTOOLS can interoperate with <em>any</em> ANSI C compiler.  At the same time, the product does include knowledge of literally dozens of different C compilers from multiple vendors &mdash; so-called RTSC targets, which are actually spec'd IDL metaonly modules coupled with a JavaScript implementation that (in principle) anyone could develop and deliver in their own package.
<p />
<strong>Core Packages</strong>.&nbsp;  Moving up a level in the <a href="#ProductFigure" class="twikiAnchorLink">block-diagram</a>, the bulk of XDCTOOLS comprises over a hundred packages containing even more modules/interfaces that broadly fall into three major groups:
<p /> <ul>
<li> packages with metaonly modules/interfaces that themselves support the general build/release/deploy life-cycle of other RTSC packages; just as many contemporary programming environments bootstrap themselves (e.g., all of Java <em>is</em> Java classes), RTSC packages are ultimately managed through other well-known RTSC packages that lie at the core of XDCTOOLS.
</li></ul> 
<p /> <ul>
<li> packages with metaonly modules/interfaces that support the general configure/execute/analyze life-cycle of RTSC programs; here again, XDCTOOLS builds upon itself through special JavaScript meta-content that in turn drives the synthesis and analysis of target-content elements in executable programs.
</li></ul> 
<p /> <ul>
<li> packages with target modules/interfaces that provide a first layer of run-time support for C programs containing other RTSC modules; portable across all targets, these modules augment the standard C runtime library with better embedded support for pluggable memory allocators, event logging plus error handling, entry/exit of critical sections, as well as overall program startup/shutdown.
</li></ul> 
<p />
The latter content &mdash; shipped with the XDCTOOLS product in source-code form, to support migration to new RTSC targets &mdash; has its origins in some of the more rudimentary elements of DSP/BIOS.  Separating out this functionality from the kernel enables RTSC to serve a much broader class of embedded system environments.  By making the XDCTOOLS product openly and freely available (not unlike Sun's <em>Java Runtime Environment</em> or Microsoft's <em>.NET Framework</em>), we anticipate an ever-growing inventory of interoperable third-party target content populating the world of RTSC.
<p />
<img width="16" alt="info" align="top" src="rsrc/TWiki/TWikiDocGraphics/info.gif" height="16" border="0" /> <small>Next-generation kernels such as System/BIOS 6.00 &mdash; itself a collection of RTSC modules/interfaces delivered as RTSC packages &mdash; simply presume the presence of the XDCTOOLS run-time.</small>
<p />
<strong>Essential Utilities</strong>.&nbsp;  Finally, the XDCTOOLS product incorporates a number of basic utilities for: <ul>
<li> building and releasing packages
</li> <li> invoking other tools implemented in JavaScript
</li> <li> generating documentation from specs, and
</li> <li> managing package repositories
</li></ul> 
<p />
These utilities can be used directly from the command-line or else invoked through extension points within your own development environment.  In some instances, we will also include a corresponding GUI tool &mdash; delivered as a package (of course!) containing metaonly modules implemented in JavaScript in concert with the Java-based (and JavaScript-friendly) Eclipse/SWT graphical environment.  In other situations, we leave the provision of higher-level or domain-specific GUI tooling to others.
<p />
Here again, we see broad availability of the XDCTOOLS as a catalyst for others contributing compatible yet complimentary tooling &mdash; not so much for general-purpose use, but rather to address the needs of more specialized vertical markets through solutions that couple tooling <em>and</em> content more aggressively.
<p />
<h2><a name="Organization"></a> Organization </h2>
We propose this project should be undertaken within the top-level Eclipse Device Software Development Platform (DSDP) project.
<p />
<h2><a name="Mentors"></a> Mentors </h2> <ul>
<li> Doug Gaff</li>
<li> Martin Oberhuber</li>
</ul>
<p />
<h2><a name="Proposed_Project_Lead_and_Initia"></a> Proposed Project Lead and Initial Committers </h2> <ul>
<li> Dave Russo, TI (lead)
</li> <li> Bob Frankel, TI
</li> <li> Jon Rowlands, TI
</li> <li> Sasa Slijepcevic, TI
</li></ul> 
<p />
<h2><a name="Interested_Parties"></a> Interested Parties </h2>
The following companies have expressed interest in the project: <ul>
<li> <a href="http://www.s2technologies.com" target="_top">Ericsson</a>
</li> <li> <a href="http://www.nokia.com" target="_top">Nokia</a>
</li> <li> <a href="http://www.s2technologies.com" target="_top">S2 Technologies</a>
</li> <li> <a href="http://www.zeligsoft.com" target="_top">zeligsoft</a>
</li></ul> 
<p />
<h2><a name="Code_Contributions"></a> Code Contributions </h2>
We will conduct a review of all potential contributions, as several organizations have developed capabilities similar to what Eclipse RTSC proposes. Those contributions which best align with the goals of the project will be refactored and used as the starting point for RTSC.
<p />
<h2><a name="Participation"></a> Participation </h2>
The success of this project is dependent upon the participation of and adoption by embedded developers. We intend to reach out to this community and enlist the support of those interested in making a success of the RTSC project. 
<p />
<h2><a name="References"></a> References </h2>
<p />
<a name="AnchorEmbeddedCPUPoll"></a>
Ganssle, Jack; <a href="http://www.embedded.com/columns/technicalinsights/193101174?_requestid=419800" target="_top">"What processor is in your product?"</a>; Embedded Systems Design, October, 2006
<p />
<a name="AnchorEmbeddedPoll"></a>
Nass, Richard; <a href="http://www.embedded.com/design/opensource/201803499?_requestid=424003" target="_top">"Annual study uncovers the embedded market"</a>; Embedded Systems Design, VOL. 20 NO. 9, September, 2007
<p />
<a name="AnchorRealtimeCorba"></a>
Schmidt, Douglas C. and Vinoski, Steve; <a href="http://www.ddj.com/cpp/184403809" target="_top">"Real-time CORBA, Part 1: Motivation and Overview"</a>; C/C++ Users Journal, December, 2001. 
<p />
<a name="AnchorSzyperski1998"></a>
Szyperski,C.; &#8220;Component Software, Beyond Object-Oriented Programming&#8221;, 1998.
<p />
<a name="AnchorEmbeddedRTOSPoll"></a>
Turley, Jim; <a href="http://www.embedded.com/columns/showArticle.jhtml?articleID=187203732" target="_top">"Operating systems on the rise"</a>; Embedded Systems Design, June, 2006
<p />
<a name="AnchorKoala"></a>
van Ommering, Rob; <a href="http://www.computer.org/computer/co2000/r3toc.htm" target="_top">"The Koala Component Model for Consumer Electronics Software"</a>; Computer, March 2000 (Vol. 33, No. 3) pp. 78-85
<p />
<h3><a name="Web_Links"></a> Web Links </h3>
<a name="AnchorCodecEngine"></a>
Codec Engine and xDAIS Framework Components: <a href="http://focus.ti.com/docs/toolsw/folders/print/tmdmfp.html" target="_top">http://focus.ti.com/docs/toolsw/folders/print/tmdmfp.html</a>
<p />
<a name="AnchorBIOS"></a>
DSP/BIOS Real-Time Operating System: <a href="http://focus.ti.com/docs/toolsw/folders/print/dspbios.html" target="_top">http://focus.ti.com/docs/toolsw/folders/print/dspbios.html</a>
<p />
<a name="AnchorEcos"></a>
ECOS Component Model: <a href="http://ecos.sourceware.org/docs-latest/" target="_top">http://ecos.sourceware.org/docs-latest/</a>
<p />
<a name="AnchorKnit"></a>
Knit: <a href="http://www.cs.utah.edu/flux/alchemy/knit.html" target="_top">http://www.cs.utah.edu/flux/alchemy/knit.html</a>
<p />
<a name="AnchorMinimalCorbaSpec"></a>
Real-Time and Minimal Corba: <a href="http://realtime.omg.org/" target="_top">http://realtime.omg.org/</a>
<p />
<a name="AnchorESDSurveys"></a>
Embedded Systems Design Surveys: <a href="http://www.embedded.com/columns/survey" target="_top">http://www.embedded.com/columns/survey</a>
<p />
<a name="AnchorTinyOsNesC"></a>
TinyOS and nesC: <a href="http://webs.cs.berkeley.edu/tos/" target="_top">http://webs.cs.berkeley.edu/tos/</a> and <a href="http://nescc.sourceforge.net/" target="_top">http://nescc.sourceforge.net/</a>
<p />
<a name="AnchorXDCTools"></a>
XDC Tools Product Download: <a href="https://www-a.ti.com/downloads/sds_support/targetcontent/rtsc/xdc_3_00/index.html" target="_top">https://www-a.ti.com/downloads/sds_support/targetcontent/rtsc/xdc_3_00/index.html</a>
(may require free registration)
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
