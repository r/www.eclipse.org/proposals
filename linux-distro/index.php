<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Proposal for Eclipse on Linux (Linux Distro) Project";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
      <div id="midcolumn">
        <h1>
        The Eclipse on Linux (Linux Distro)
         Project
        </h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("The Eclipse on Linux (Linux Distro) Project");
?>

<h2>Proposal</h2>

<p>The Eclipse IDE is perhaps the fastest growing open source application development environment available today.  A mailing list posting of February 25, 2006 indicated almost 300,000 downloads of the Eclipse IDE in the preceding 30 day period.  High levels of interest combined with an open, versatile platform and a well run parent organization explain the widespread adoption of this platform.
</p><p>However, Eclipse hasn't enjoyed the type of success on the Linux platform that one might expect.  The open source heritage of both Eclipse and Linux would lead one to assume that Eclipse would be highly successful on Linux, but the numbers may surprise:  In the same period indicated above, only about 40,000 of the downloads were for Linux, comprising just over 13% of the total.
</p><p>The purpose of this proposal is to make a case for an Eclipse on Linux Working Group within the Eclipse Foundation.  The intent and goal of this working group would be to identify and address issues pertaining to the adoption of Eclipse on Linux, making Eclipse use on Linux more powerful and more widespread.

<P>You are invited to comment on the project in the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclispe.technology">
http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology newsgroup.</P>

</p><p><br />
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=3" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="Eclipse_on_linux_.E2.80.93_a_surprising_lack_of_adoption"></a><h2>Eclipse on linux � a surprising lack of adoption</h2>
<p>As stated earlier, despite the widespread adoption of Eclipse in general and the obvious open source affinity between Linux and Eclipse, a relatively small percentage of Eclipse downloads are for the Linux platform.  There are certainly many contributing factors to this phenomenon.  Not all of these factors can be addressed by any initiative directly.  For example, one factor may be the more prevalent use of Windows to Linux as a development workstation; another, the existence of many alternative open source IDEs on Linux, compared to the lack of open source IDEs on Windows.  No initiative or organization would want to remove this freedom of choice from an individual user.
</p><p>Still, other factors could be targeted and addressed to promote the further adoption of Eclipse on Linux.  Such factors might include:
</p>
<ul><li> Facilitation of Eclipse distribution via Linux package management methods (RPM, Deb) 
</li><li> Adoption and distribution of Eclipse by major Linux distributions 

</li><li> Promotion of Eclipse in the Linux community 
</li><li> Greater integration options with Linux systems and methodologies, such as 
<ul><li> Greater Linux system library support 
</li><li> Support for Linux technologies like GNU Autotools, RPM, man, and init 
</li><li> Better support for scripting languages like Perl and Python 
</li></ul>
</li></ul>
<p><br />
An organization focused on identifying and addressing these and potentially other problems could have a great positive impact on the adoption of Eclipse on Linux.
</p><p><br />
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=4" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="Eclipse_on_linux_Issues"></a><h2>Eclipse on linux Issues</h2>

<p>An Eclipse user wanting to run Eclipse on Linux faces a few minor, but still significant, problems and shortcomings that, if addressed, would greatly encourage and facilitate the use of Eclipse on Linux.  These problems have little to do with the execution of the Eclipse application on Linux itself; Eclipse runs well on Linux platforms.  These problems have more to do with how Eclipse is installed on the user's system, how that installation is managed once installed, and how well it helps him do his job of writing Linux software.
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=5" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="Issue_One_.E2.80.93_Eclipse_Installation_on_Linux"></a><h3>Issue One � Eclipse Installation on Linux</h3>
<p>The Eclipse package that one can download for Linux functions just fine on Linux.  However, the fact that it is not delivered in the same way as other Linux packages poses problems for users and Linux distributors alike.
</p><p>In order to better understand this problem, one needs a basic familiarity with Linux and purposes behind Linux distributions.
</p><p><b>Understanding Linux Distributions</b>
</p><p>Most major Linux distributions are comprised overwhelmingly of a series of software packages.  These packages are organized in a dependency hierarchy and laid down in an orderly fashion to form a functioning system.  The organization of these packages varies from vendor to vendor and from version to version, so although Red Hat and Novell both ship Linux, the distributions vary between vendor, product, and version.  No two distributions are necessarily alike.
</p><p>Almost without exception, the distributor will compile each package on their own and distribute their own binary versions of a software package.  Distributors do this by obtaining unmodified versions of the source code, where possible (called pristine sources), and then making any necessary modifications in order to build the package for their distribution.  This process provides higher levels of assurance that the package will actually compile and run on the system, provided that the stated dependencies are met.  This is particularly important for commercial distributions, whose customers have expectations of reliance and stability from the versions of Linux they purchase and from the software that has been tested and/or certified to run on these platforms.
</p><p>More and more, on any given system a single application has the responsibility for tracking and managing the installations on each system.  Vendors such as Red Hat and Novell use a technology called RPM, while Debian and other Debian-based distributions employ a technology called deb.  Regardless of the distribution, the purpose is the same:  To manage all software installations, removals, and updates from a single application via a single database.  Applications that leverage these technologies provide advantages to their users, primarily in the fact that these applications can now be managed through the same mechanisms, user interfaces, and infrastructure as other software on the installed system.  Distributors of Linux find that there is an ever-increasing demand to provide applications through their application management system of choice.
</p><p>As the interest in Eclipse grows on Linux, Linux distributors find an increasing demand for Eclipse to be distributed along with the Linux distribution itself, in RPM or Deb packages.
</p><p><b>Getting Eclipse Source Code</b>

</p><p>Since Linux distributors must build packages from pristine sources whenever possible, the first step in providing Eclipse packages is to acquire a complete source code snapshot so that the packages may be built.  Unfortunately, the Eclipse releng process, which greatly simplifies the process of building Eclipse from an Eclipse point of view, greatly complicates the building of Eclipse from a Linux distributor's point of view.
</p><p>In a nutshell, the Eclipse releng process allows one to check a single package out of a CVS repository; invoking the build script will check out all the additional code required to build the package properly.  However, this process assumes both that the build machine has an active network connection and that the build will be completed at the same time as the code is obtained.  These assumptions do not apply to Linux distributions; in particular, in the case of Red Hat and Novell at least, these assumptions are almost always false.
</p><p>Instead, Linux distributors must discover how to obtain a reliable source code snapshot on their own.  They must do this today primarily on their own; since the Eclipse community today does not generally face this problem, there is little to no documentation or assistance available to figure it out.  This costly process may be further complicated when each distributor learns a different way of doing this.  The end result is either a lack of Eclipse packages for Linux distributions or a highly variable Eclipse experience since it is not being built in a standardized way.  Neither of these scenarios is good for the future of Eclipse on Linux.
</p><p><b>Building Eclipse</b>
</p><p>Another problem presents itself when it comes time to do the actual build.  Incompatibilities between Linux distributions and the licensing of traditional Java implementations have prevented their inclusion in most distributions.  (Note:  "Enterprise" or "commercial" distributions are an exception here as some companies such as Red Hat and Novell have entered into agreements with JVM creators to re-distribute JREs and JDKs under special circumstances.)  The "free as in beer" (gratis) JVM implementations have served the needs of many despite their lack of integration into distribution packaging systems; however, for inclusion in most distributions, applications must be re-buildable with the distribution itself.  This has prevented inclusion of applications which build using JDKs.  Of course, users are still able to download and install one of many freely available Java implementations after their system is installed.  However, this lack of consistency creates another potential problem in terms of how Eclipse should be built and executed on Linux.
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=6" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="Issue_Two_.E2.80.93_Eclipse_Update_Mechanism"></a><h3>Issue Two � Eclipse Update Mechanism</h3>
<p>The Eclipse Update feature provides users with a great convenience in how they can install and update extensions to their Eclipse platform.  This same feature, however, becomes problematic for Linux distributors seeking to package and redistribute Eclipse.  Some of the reasons for this include:
</p>
<ul><li> <i>Permissions Issues.</i>  When Eclipse is installed as an RPM, for example, non-administrator users do not have sufficient permission to install features via Eclipse Update, because the directories where those features are installed are not writable by non-administrator users.  

</li><li> <i>Management Issues.</i>  It is true that addressing the permissions issue is a trivial matter, but a deeper issue remains.  From a fundamental point of view, files should not be installed into system locations on a Linux system without the system package management software having put them there.  Since the package management software created these directories via the package install, it is an improper action to populate those directories with files that are not managed by the package manager.  
</li></ul>
<p>With Eclipse on Linux, users may choose to install new plugins via Eclipse Update or, if available, as packages from their Linux distributor.  Users of Eclipse on Linux today frequently cite this inconsistency and lack of integration as a significant problem in using Eclipse on Linux.  It is a problem that needs solving in order to further Eclipse adoption on Linux.
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=7" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="Issue_Three_.E2.80.93_Linux_Integration"></a><h3>Issue Three � Linux Integration</h3>
<p>Adoption of Eclipse on Linux could benefit greatly from stronger platform support and integration.  The responsibility to address these issues may rest more with Linux distributors than with Eclipse itself, which is and should be primarily platform-neutral.  Still, the lack of a collaborative effort around Eclipse integration with the Linux platform is hindering the adoption of Eclipse on Linux.
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=8" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="Issue_Four_.E2.80.93_Linux_Promotion"></a><h3>Issue Four � Linux Promotion</h3>

<p>Just as Linux distributors lack a collaborative effort around integrating Eclipse more fully with Linux and the Linux desktop environment, they also lack a collaborative effort around promoting Eclipse as a development tool on Linux.  In other words, they not only lack in technology, but they also lack in marketing to make Eclipse on Linux more successful.  The reason for the shortfall is a lack of effective collaboration between the distributors.
</p><p><br />
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=9" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="Eclipse_on_Linux_Working_Group"></a><h2>Eclipse on Linux Working Group</h2>
<p>I propose the creation of an Eclipse on Linux Working Group within the Eclipse Foundation.  The purpose of such a working group would be to specifically address the problems that I've outlined and others that have yet to be identified.  This working group would work both within Eclipse, influencing necessary changes and patches, and outside of Eclipse, creating collaboration, standards, and momentum, with the goal of increasing the adoption of Eclipse on Linux platforms.
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=10" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="About_the_Eclipse_on_Linux_Working_Group"></a><h3>About the Eclipse on Linux Working Group</h3>
<p>The Eclipse on Linux Working Group would be an official working group within the Eclipse Foundation.  It would be founded with involvement from representatives from Novell, Red Hat, Debian, and Gentoo, and would seek and welcome involvement from others who are similarly interested in the goals of the working group.
</p><p>This working group would require a presence within the eclipse.org website organization.  The following services would be used by the working group:

</p>
<ul><li> <i>Wiki</i> � to capture information about the working group as well as to collaborate on solutions regarding Eclipse on Linux 
</li><li> <i>Bugzilla Component</i> � to provide a mechanism for users to report problems having to do with Eclipse packages or other Eclipse on Linux issues, and for participants to work through the solutions to these problems.  This also provides a destination for bugs submitted to other project groups that are believed to be due to Eclipse on Linux issues 
</li><li> <i>Subversion/CVS</i> � to provide hosting for resources such as scripts to obtain source code snapshots or to build packages 
</li></ul>
<p>Mailing List � for communication among members of the working group and with the user community
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=11" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="How_the_Eclipse_on_Linux_Working_Group_Will_Address_Problems"></a><h3>How the Eclipse on Linux Working Group Will Address Problems</h3>

<p>This working group will be specifically created and ideally positioned to address the problems that we have identified.  Addressing these problems will be key to the success of the working group and will be the working group's primary work activity.
</p><p><b>Addressing the Eclipse on Linux Installation Problems</b>
</p><p>As discussed earlier, the two primary issues around installation of Eclipse on Linux have to do with a) problems obtaining pristine sources, and b) problems with building Eclipse packages.  Both of these problems will be effectively addressed through the working group.
</p><p>First, with respect to obtaining complete, pristine source code snapshots, the working group will collaborate on the best way to obtain these source code snapshots from the Eclipse CVS repositories.  Documentation on the process and methodology for so doing will be maintained on a wiki site, and help provided to package maintainers via forums and mailing lists.  The process can be automated through scripts, and the scripts stored and version-controlled in a CVS repository.  In this way, the working group can drive a single, common way for all Linux distributors to obtain the sources for Eclipse components.
</p><p>Second, the working group will collaborate on the build methodology to be used with these code snapshots.  Advances in "free as in speech" (libre) JVM implementations are making it easier for applications like Eclipse to be included in "community" (or non-commercial) distributions such as Fedora, openSUSE, and Debian.  These distributions make use of the GNU Compiler for the Java programming language, GCJ.  GCJ-compiled shared libraries are distributed alongside the bytecode jars, allowing use of traditional JVMs should users so desire.  Libre JVM implementations have begun to remove this potential problem of how Eclipse is built and distributed on Linux.  Again, the working group will drive a single, common way to build Eclipse packages using the correct combination of these open source and proprietary tools in ways that are in harmony with the goals of their distributions and with license restrictions.
</p><p>The end result will be a standardized way to package Eclipse components for inclusion with Linux distributions.  Individual distributors will not �reinvent the wheel� when it comes to providing Eclipse on Linux; likewise, users will not have an inconsistent Eclipse experience on Linux platforms.  This will appeal greatly to Linux users and promote the adoption of Eclipse on Linux.
</p><p>A key feature of the working group in this regard is the involvement of working group members with Eclipse  Bugzilla.  Eclipse projects are likely to have concerns about Linux distributors rebuilding and redistributing Eclipse components, primarily that users may file defects with the Eclipse project groups that have to do not with the Eclipse project but with how that component is provided on Linux.  By becoming involved in Bugzilla with issues dealing with Eclipse on Linux, this working group is positioned to effectively address such issues.
</p><p><b>Addressing the Eclipse Update Problem</b>
</p><p>The working group will also address problems that we have discussed around the Eclipse Update feature, as well as other possible future problems.  As appropriate, the group will create patches and solutions if necessary, and will submit and promote these solutions appropriately within other Eclipse projects.  In fact, much of the work to address this issue has already been submitted (see <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=90630" class='external text' title="https://bugs.eclipse.org/bugs/show bug.cgi?id=90630" rel="nofollow">bug #90630 on bugs.eclipse.org</a>), but other issues remain. As these and other further issues arise, the working group can find and submit new solutions that work for all the group's constituents along with the Eclipse community in general.
</p><p><br />
</p><p><b>Addressing the Linux Integration Problem</b>
</p><p>Members of the group can take an active role in furthering the integration of Eclipse with Linux.  The working group will be a liaison between other Eclipse projects and those in the working group providing the integration features, to ensure that these features are in harmony with Eclipse as a whole.

</p><p>Some of the possibilities here include:
</p>
<ul><li> Integration with GNU Autotools 
</li><li> RPM Specfile Editors 
</li><li> Bugzilla tools 
</li><li> Man page editors 
</li><li> Init script editors 
</li><li> Greater support for scripting languages like Perl and Python 
</li><li> Integration with Linux system libraries and common third-party open source libraries 
</li></ul>

<p>The work for some of these features has already begun.  The working group can facilitate collaboration on these features so that they are useful for many Linux distributions, thus promoting Eclipse on all of these distributions instead of just a single distribution.
</p><p><br />
</p><p><b>Addressing the Linux Adoption Problem</b>
</p><p>Finally, the Eclipse on Linux Working Group will promote the adoption of Eclipse on Linux, not only by addressing technical problems, but also by increasing awareness and interest in Eclipse on Linux in general.
</p><p>Creating this working group has the effect of creating a sub-community of the larger Eclipse community.  This sub-community will have as its topic the facilitation and adoption of Eclipse on Linux in general.  One of the primary opportunities of this group will be to make presentations on this topic at key shows (such as EclipseCon, LinuxWorld, OSCON, or others), hold special-interest sessions (such as Birds of a Feather sessions), conduct periodic working group status meetings, and otherwise drive involvement in this sub-community through the provision of information and driving of change and improvement within Eclipse for the Linux platform.
</p><p>The working group will also have the purpose of facilitating the delivery of the documentation, tools, and best practices for developing with Eclipse on Linux.  This applies not only to those using Eclipse as a development tool, but also to those that create Eclipse plugins.  By working to simplify and standardize the use of Eclipse both as a development tool and a rich-client platform on Linux, the group should find success in increasing the adoption of Eclipse on Linux as a whole.
</p>
<div class="editsection" style="float:right;margin-left:5px;">[<a href="/index.php?title=Eclipse/EclipseOnLinux&amp;action=edit&amp;section=12" title="Eclipse/EclipseOnLinux">edit</a>]</div><a name="Likelihood_of_Industry_Adoption"></a><h3>Likelihood of Industry Adoption</h3>
<p>One question to answer is whether or not there really is any interest in such a working group across companies.  Will multiple Linux distributors really seize such an opportunity to collaborate on this effort?
</p><p>The answer to this question appears to be a resounding yes.  Conversations between representatives from Novell, Red Hat, Debian, Gentoo, and Ubuntu have already taken place, and indicate that all five distributors are eager to collaborate on such a solution.  In each case, all distributors have run into and identified practically the same set of problems.  The solution for collaboration in each case is the same:  A vendor-neutral place where information can be captured, displayed, and made available to all so that everyone is providing Eclipse in the same way.
</p><p><br />
</p>

      </div>
    </div>
   
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
