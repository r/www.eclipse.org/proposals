<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
  
    <h1>technology project infrastructure
      (aka &quot;Barn Raising&quot;)</h1><br>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Technology Project Infrastructure (Barn Raising)");
?>          

      <p>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/">Eclipse 
        Development Process document</a>) and is written to declare the intent 
        and scope of a proposed Technology PMC Project called the Eclipse
      Technology Project Infrastructure Project (TPI), also know as the
      &quot;Barn Raising&quot; project. In addition, this proposal is written 
        to solicit additional participation and inputs from the Eclipse community. 
        You are invited to comment on and/or join the project. Please send all 
        feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.tpi">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.tpi</a> 
        newsgroup. </p>
   
    
  


  <h2>Overview</h2>
  
    
      TPI's focus is to provide an easy to use common infrastructure for Eclipse
      Technology Projects so that each technology project can focus more on
      developing and delivering their unique aspects and less on the common
      tasks of actually delivering Eclipse based technology. This should reduce
      the sysadmin overhead for each project allowing more technology projects
      to be better served with fewer resources. Our first goal is to provide
      hardware and software for a continuous build environment for technology
      projects. There may be other goals that arise as we proceed or it may turn
      out that this is the only goal and the project is terminated when the goal
      has been achieved.
  


  <h2>Details</h2>
  
    Currently, each Eclipse Technology Project must spend time
      and energy developing their own infrastructure for building and testing
      their code. Much of this infrastructure is similar from project to project
      and should only need to be developed once and reused by all technology
      projects. Properly configured technology projects would automatically
      participate in this continuous build process. Our first goal is to
      configure a continuous build environment including:
      <ul>
        <li>Cruise Control&nbsp;</li>
        <li>an Ant based build infrastructure&nbsp;</li>
        <li>a schema based process for validating build scripts&nbsp;</li>
        <li>an Eclipse machine for building and testing technology
          projects&nbsp;</li>
        <li>a web, xml, and/or email based interface for reviewing build and
          test results&nbsp;</li>
        <li>an automated process for moving successful builds to the eclipse
          download server&nbsp;</li>
        <li>an example project containing and utilizing all elements of the
          build infrastructure to continuously &quot;prove&quot; the build
          system and provide a sample for others to follow</li>
      </ul>
      <p>We want this process as &quot;hands-off&quot; as possible, so there are
      several challenges we face including</p>
      <ul>
        <li>how technology projects need to be configured to
          &quot;automatically&quot; participate&nbsp;</li>
        <li>what build script validation is necessary before a build&nbsp;</li>
        <li>how to detect and recover from runaway builds&nbsp;</li>
        <li>how do smaller and larger projects participate together so that
          larger projects do not dominate the build machine.</li>
      </ul>
    
  


  <h2>Organization</h2>
  
    <p>If you are interested in participating, please take part
      in the newsgroup discussions or ask to be added to the list of interested
      parties. So far we have:</p>
      <p>Naci Dai, Eteration<br>
      Bjorn Freeman-Benson, Eclipse Foundation<br>
      Mik Kersten, UBC<br>
      Pete Mackie, Seaquest<br>
      Dan Rubel, Instantiations<br>
      </p>
    
  

</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
