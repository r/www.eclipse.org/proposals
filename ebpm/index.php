<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>

 
	<div id="maincontent">
	<div id="midcolumn">

<h1>eBPM</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("eBPM");
?>

<h2>Introduction</h2>

<p align="left">EBPM is an open source project proposed under the Eclipse SOA Top-Level Project, 
in accordance with the Eclipse Development Process. Everyone is invited to join the project 
and to provide their feedback using 
the <a href="http://www.eclipse.org/forums/eclipse.ebpm">eBPM forum</a>.
</p>
	
<h2>Overview</h2>
<p align="left">
At present, the OSGi technology appears to be the best answer to the demand of stable, modular and easily extensible solutions according to the SOA guidelines. On the other hand, OSGi really misses a central concept of a BPM platform that enables to describe the integration process in terms of OSGi services orchestration.
The eBPM aims at the realization of  a complete BPM solution for OSGi. The project will also provide also provide UI and runtime supports, to be able to deploy and manage OSGi services, in declarative way leveraging the capabilities of OSGi declarative services.
In eBPM  will propose two approach to realize a BPM Solution for OSGi:

<ul>
<li><b> Providing a BPM Gateway Bundle into the OSGi container that is able to realize the orchestration of  other services.</b> </li>
<li> <b>Embedding the OSGi runtime in an external BPM Engine, and extending this to be able to use the services provided by the container.</b></li>
</ul>

The proposal also includes a console ( eclipse view/perspective ) for management and monitoring activities.
The complete list of proposed components is in the proposed components paragraph.

</p>

<h2>Project Scope</h2>
<p align="left">The eBPM  project will focus on providing  both runtime, and support tools to realize a complete BPM solution based on OSGi 
For the runtime part the eBPM project will provide:
<ul>
<li>A core framework (eBPM Core framework ) to develop, configure and manage OSGi services based on messaging paradigm. The core framework will be built on top of Equinox runtime project.</li>
<li>A set of standard connectivity ( SOAP, JMS etc.. ) and business services built on top of eBPM core framework.  For connectivity service one of the project goals is to collaborate with ECF runtime project and with Swordfish based connectors.</li>
<li>Orchestration of OSGi services in the core framework, through one or more bundles (BPMGateway).</li>
<li>Extensions towards the Apache ODE engine so as to be able to embed  Equinox and eBPM core services in the BPEL engine.</li>
<li>A metadata model to have  a persistent model for eBPM projects.</li>
</ul>
</p>
<p>
The project will provide support and deployment tools for the configuration of services for the eBPM core framework and a monitoring ui perspective to manage runtime information, so that the proposal does not aim to replace or substitute existing OSGi development tools ( PDE ).
</p>
<p>
The proposal does not aim at providing process development tools and SOA metamodel, but it will use and extend existing Eclipse projects like Eclipse BPMN Modeler, Eclipse BPEL Editor and STP intermediate model ( now Mangrove project ).  eBPM will focus on code generation capabilities.</p>
</p>
<p>
The following schemes illustrate the proposed architectural runtime services that will be implemented. 
</p>
<p>
<img src="arch_general.jpg"/></p>
When we need an external BPM Engine like BPEL, the runtime part will become:
<p>
<img src="arch_bpel.jpg"/>
</p>
</p>


<h2>Proposed Components</h2>
<p align="left">
<b>OSGi Bundles:</b>
<ul>
<li><b>eBPM Core Framework:</b> Set of bundles, that leverage the capabilities of OSGi Declarative Services, to provide a common way to define/develop OSGi services.eBPM core framework will  provide a way to realize OSGi Services based on the messaging paradigm that use the OSGi Event Admin Service to communicate with each others.</li>

<li><b>eBPM Deployment Service:</b> Bundle that will provide an OSGi Service, that will look for service configuration file and will publish OSGi services using facilities offered by DS and DS Component Factories.</li>

<li><b>BPM Gateway API:</b> The bundle defining the interface for BPM Gateway implementations.</li>

<li><b>BPM Gateway Process Engine:</b> The bundle containing an implementation of the API based on an open source process engine ( for example jbpm )</li>

<li><b>Standard Connectors and  Services:</b> Bundles that provide common connectivity, and business services (SOAP, JMS, File System, FTP, TCP with pluggable applicative protocol implementations, Quartz, XSLT Transformation service, Groovy, JDBC, XSD Validation )</li>


<li><b>eBPM Monitoring Services:</b> The set of bundle that will leverage information from runtime and store into a relational database.</li>
</ul>

<b>Metadata Model</b>
<p>
The relational model used by eBPM to store monitoring information. Basically this will be based on the STP Intermediate Model.
</p>

<b>Eclipse IDE Plugins ( Extensions to Eclipse IDE )</b>

<ul>
<li><b>eBPM Project Wizard Tool:</b> Eclipse Plugin to create an eBPM project.</li>

<li><b>eBPM Service Editor:</b> visual tool for the configurations of OSGi service descriptor. This service descriptor file will be used by eBPM Core Service Framework, to register OSGi services into the runtime.This will not replace PDE tool, as it�s intended as a tool to create eBPM service descriptor to be used by eBPM Deployment Service.</li>

<li><b>BPMN to BPM Gateway Process Engine Bundle:</b> Based on Eclipse STP IM ( Mangrove ) it produces process definitions to be used by BPM Gateway default implementation ( jbpm ) bundle.</li>

<li><b>BPMN to BPEL:</b> Based on Eclipse STP IM ( Mangrove ), it produces BPEL processes with using the models realized by means of Eclipse BPMN Designer. The processes are then completed through the BPEL Designer.</li>

<li><b>Deploy Tools:</b> Utility Tools integrated in Eclipse, that allow to deploy an eBPM project to the runtimes.</li>

<li><b>Eclipse BPMN Editor Extension:</b> Extension to the eclipse bpmn editor to support the drag and drop of OSGi service definitions into the diagram.</li>

<li><b>Eclipse BPEL Editor Extension:</b> Extension to the eclipse bpel editor  o support the drag and drop of OSGi service definitions into bpel  diagram.</li>

<li><b>Eclipse eBPM Support Tools:</b>  Some support plugins ( menu contributions, preference page ), to help to work with eBPM.</li>

<li><b>Monitor and Management Perspective/View:</b> Eclipse plugins to provide a view with the list of processes, process instances, and status information</li>
</ul>

<b>Extension to BPEL Runtime ( eBPM is used in an OSGi runtime embedded in BPEL runtime )</b>
<ul>
<li><b>OSGi Extension for the BPEL Runtime:</b> extensions enabling the BPEL runtime to orchestrate  OSGi  services. It will be initially released for Apache ODE.</li>
</ul>
</p>

<h2>Relation with other Eclipse Projects</h2>
<p>The eBPM project will leverage technologies and work in collaboration with the following projects:<p>
<ul>
<li>Eclipse STP IM</li>
<li>Eclipse BPEL Designer</li>
<li>Eclipse BPMN Modeler</li>
<li>Eclipse Swordfish</li>
<li>Eclipse Equinox</li>
<li>Eclipse ECF</li>
<li>EclipseLink</li>
<li>Eclipse Gemini</li>
</ul>

<h2>Organization</h2>
<p align="left">Everyone is invited to express their concern or opinion about the above-proposed 
eBPM project and to offer their support to its development using the newsgroup.
Our intention is to start developing the project directly in the source repository of Eclipse, just from the 
the incubation phase. Below is a preliminary list of the developers directly involved in the project. 
Any design or development contribution is welcome.
</p>

<p align="left"><b>Initial Committers</b></p>
<ul>
<li>Andrea Zoppello,  Engineering  (proposed as project lead)</li>
<li>Gianfranco Boccalon, Engineering</li>
<li>Luca Rossato, Engineering</li>
<li>Antonella Miele, Engineering</li>
<li>Luca Barozzi, Engineering</li>
<li>Daniela Butano, Independent</li>
</ul>

<p align="left"><b>Mentors</b></p>
<ul>
<li>Oliver Wolf, SOPERA</li>
<li>Dave Carver, Intalio</li>
<li>Oisin Hurley, independent</li>
</ul>

<p align="left"><b>Interested parties</b></p>
<p align="left">
The following companies have shown their interest in this project. Key contacts listed.
<ul>
<li>Alain Boulze, INRIA</li>
<li>Adrian Mos, INRIA </li>
<li>Oliver Wolf, SOPERA </li>
<li>Ricco Deutscher, SOPERA</li>
</ul>
</p>

<h2>Tentative Plan</h2>
<p align="left">

<ul>
<li><p align="left"><b>2010-07</b> release of Release Candidate of eBPM</li>
<li><p align="left"><b>2010-08</b> release of 0.7 version of eBPM</li>
</ul>
</p>

      </div>
  </div>
  <?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

 