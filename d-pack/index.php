<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>DSDP Packaging</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("D-Pack");
?>


<p>DSDP Packaging ("D-Pack") is a proposed open-source sub-project under the <a href="http://www/eclipse.org/dsdp" class="external text" title="http://www/eclipse.org/dsdp" rel="nofollow">Device Software Development Platform</a> top-level project.  The goal of this project is to enable <i>multiple</i> entry-level distributions of Eclipse that respectively provide turnkey environments for very <i>specific</i> developer communities found in the broader embedded systems marketplace.   
</p><p>This proposal is in the Pre-Proposal Phase as defined in the <a href="http://www.eclipse.org/projects/dev_process/development_process.php" class="external text" title="http://www.eclipse.org/projects/dev_process/development_process.php" rel="nofollow">Eclipse Development Process</a> document and follows the <a href="http://www.eclipse.org/projects/dev_process/pre-proposal-phase.php" class="external text" title="http://www.eclipse.org/projects/dev_process/pre-proposal-phase.php" rel="nofollow">Eclipse Proposal 
Guidelines</a>. It is written to declare the intent and scope of the project, as well as to 
solicit additional participation and input from the Eclipse and embedded developer community. You 
are invited to comment upon and/or join this project. Please send all feedback 
to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.d-pack" class="external free" title="http://www.eclipse.org/newsportal/thread.php?group=eclipse.d-pack" rel="nofollow">http://www.eclipse.org/newsportal/thread.php?group=eclipse.d-pack</a> newsgroup. 

</p>

<a name="Background"></a><h2> <span class="mw-headline"> Background </span></h2>
<p>While usage of Eclipse continues to grow amongst embedded software developers, creation of turnkey (entry-level) downloads satisfying the needs of this community�not unlike what's currently provided through the <a href="http://www.eclipse.org/epp/" class="external text" title="http://www.eclipse.org/epp/" rel="nofollow">EPP</a> project�remains elusive for a number of reasons.  For starters, the community of embedded software developers will always remain a very fragmented one due to the sheer diversity of the underlying hardware platforms targeted by this community�ranging from powerful multi-core SOCs capable of supporting Linux and Java, right down to resource-constrained 8/16-bit MCUs with little/no runtime support.  Given this diversity, attempts to create a <i>single</i> distribution of Eclipse targeting embedded software developers would clearly prove futile.
</p><p>Making matters worse, any usage of Eclipse for embedded software development invariably requires integration of legacy host tooling (such as a compiler) along with legacy target content (such as an RTOS) specific to the hardware platform at hand.  Not only do these elements maintain an existence outside of the Eclipse IDE per se, these tools and content often carry non-EPL licenses that would ultimately <i>preclude</i> their provisioning from any eclipse.org server.

</p><p>Responding to this reality, turnkey Eclipse distributions (bundling the necessary tools/content) which serve specific segments of the embedded community do, in fact, exist outside of eclipse.org�whether offered by software vendors such as Wind River or by silicon vendors such as Texas Instruments.  The <a href="http://wascana.sourceforge.net/" class="external text" title="http://wascana.sourceforge.net/" rel="nofollow">Wascana</a> project at SourceForge�providing a turnkey Eclipse/CDT distribution for Windows development�follows a similar pattern.
</p>
<a name="Scope"></a><h2> <span class="mw-headline"> Scope </span></h2>
<p>To reiterate, the goal of this project is to enable multiple entry-level distributions of Eclipse that respectively provide turnkey environments for very specific developer communities found in the broader embedded systems marketplace.  As an initial objective, the project will focus upon a <i>generic packaging methodology</i> that can be adopted and tailored on a case-by-case basis to the needs of specific distributions serving particular embedded market segments.
</p><p>From a technical perspective, the new <a href="http://wiki.eclipse.org/Equinox/p2" class="external text" title="http://wiki.eclipse.org/Equinox/p2" rel="nofollow">Equinox p2</a> provisioning infrastructure affords an obvious starting point for any methodology formulated through this project's efforts.  The clean separation between installation metadata and the installable artifacts themselves, for example, seems particularly responsive to some of the challenges of bundling legacy tools/content outlined earlier; the metadata and artifacts, as a case in point, might respectively carry different licenses as well reside at different sites.  The ability to hierarchically compose installable units into larger (installable) entities also faciliates re-use of common sub-elements (say, a particular compiler) across multiple turnkey distributions ultimately served from different sites. 

</p><p>Recognizing that these turnkey distributions will ultimately be provisioned outside of eclipse.org�even some suitable baseline version of the Eclipse IDE containing CDT plus stock DSDP elements would ideally deliver a pre-populated list of p2 update sites which themselves might serve non-EPL tools/content�this project will proactively encourage and facilitate creation of <i>external</i> distribution sites required to deliver turnkey Eclipse-based environments to embedded software developers; such sites become models of the methodology and exemplars for others to emulate.
</p><p>Strange as it may seem, this project may <i>not</i> necessarily deliver any source code�at least not in its initial phases. Rather, the project will serve as a central meeting point for collectively crafting a packaging methodology that serves the provisioning needs of the embedded communities already engaged with the CDT and DSDP, by effectively applying the <i>existing</i> Equinox p2 technology to the specific use-cases at hand.  Without a dedicated eclipse.org project providing some necessary leadership, however, turnkey distributions of Eclipse serving embedded software developers will remain sparse, sporadic, and siloed.
</p>
<a name="Organization"></a><h2> <span class="mw-headline"> Organization </span></h2>
<p>This project would be lead by Bob Frankel, who recently retired from Texas Instruments after a long and productive tour of duty spanning three decades.  Besides serving as one of the earliest advocates for leveraging the Eclipse/CDT framework as the backbone for TI's own IDE, much of Bob's effort throughout this decade has been focused upon developing and evangelizing <a href="http://rtsc.eclipse.org/docs-tip/Main_Page" class="external text" title="http://rtsc.eclipse.org/docs-tip/Main_Page" rel="nofollow">RTSC</a>�software technology which enables component-based development in C/C++ targeting diverse embedded platforms, and which has formally joined the Eclipse/DSDP community through a <a href="http://www.eclipse.org/dsdp/rtsc/" class="external text" title="http://www.eclipse.org/dsdp/rtsc/" rel="nofollow">project</a> of its own.   

</p>
<dl><dt><b>Initial Committers</b>
</dt></dl>
<dl><dd><i>Bob Frankel, BiosBob.Biz</i> (Project Lead) � For more details on where Bob's been and where Bob's heading, visit his website at <a href="http://biosbob.biz" class="external text" title="http://biosbob.biz" rel="nofollow">BiosBob.Biz</a>.
</dd></dl>
<dl><dd><i>Martin Oberhuber, Wind River</i> � Martin is the lead of the <a href="http://www.eclipse.org/dsdp/tm" class="external text" title="http://www.eclipse.org/dsdp/tm" rel="nofollow">DSDP Target Management Project</a>, and chair of the Eclipse Architecture Council. He's been working for Wind River in Salzburg, Austria since 1998. Driven by his desire for constant improvement, he is involved in many areas around Eclipse and Open Source. Martin holds a Master of Engineering degree in Telematics from the University of Technology Graz/Austria.
</dd></dl>
<dl><dd><i>Dave Russo, Texas Instruments</i> � Dave is the lead of the <a href="http://www.eclipse.org/dsdp/rtsc/" class="external text" title="http://www.eclipse.org/dsdp/rtsc/" rel="nofollow">RTSC Project</a> and a Senior Software Engineering at Texas Instruments, where he continues to drive component technology into a number of flagship TI products.  Along with Bob, he co-founded Spectron Microsystems where he co-developed numerous embedded software products that continue to thrive in the market today.  Dave holds a PhD degree in Mathematics from the University of California Santa Barbara. 
</dd></dl>
<dl><dd><i>Doug Schaefer, Wind River</i> � Doug is the lead for the <a href="http://www.eclipse.org/cdt" class="external text" title="http://www.eclipse.org/cdt" rel="nofollow">CDT Project</a> and an Engineering Manager at Wind River Systems, where he manages the team putting together Wind River's next generation software distribution technology based on Eclipse p2. He also remains involved with the CDT working on his <a href="http://wascana.sourceforge.net/" class="external text" title="http://wascana.sourceforge.net/" rel="nofollow">Wascana</a> Eclipse/MinGW tools distribution, as well as getting more and more involved in open platforms for embedded development.
</dd></dl>

<dl><dt><b>Interested Parties</b>
</dt><dd><i>Motorola</i>
</dd><dd><i>Texas Instruments</i>
</dd><dd><i>Wind River</i>

</dd></dl>
<dl><dt><b>Project Mentors</b>
</dt><dd><i>Doug Gaff, Wind River</i>
</dd><dd><i>Martin Oberhuber, Wind River</i>
</dd></dl>
<a name="References"></a><h2> <span class="mw-headline"> References </span></h2>
<ul><li> initial discussions on packaging beginning in 2007 can be found <a href="http://wiki.eclipse.org/DSDP/Packaging/Background" class="external text" title="http://wiki.eclipse.org/DSDP/Packaging/Background" rel="nofollow">here</a>
</li></ul>
<ul><li> raw notes from a BoF session held at EclipseCon 2008 can be found <a href="http://wiki.eclipse.org/DSDP/Packaging/Meetings/Mar_18_2008_EclipseCon_BoF" class="external text" title="http://wiki.eclipse.org/DSDP/Packaging/Meetings/Mar_18_2008_EclipseCon_BoF" rel="nofollow">here</a>

</li></ul>
<ul><li> raw notes from a series of meetings held in early 2009 can be found <a href="http://wiki.eclipse.org/DSDP/Packaging/Meetings" class="external text" title="http://wiki.eclipse.org/DSDP/Packaging/Meetings" rel="nofollow">here</a>
</li></ul><br>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
