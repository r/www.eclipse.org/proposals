<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Proposal for Eclipse Project Incubator</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Project Incubator");
?>

<h2>Introduction</h2>
<p>The Eclipse Project Incubator is a proposed open source project under the Eclipse Project.</p>

<p>This proposal is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and 
is written to declare its intent and scope. This proposal is written to solicit additional participation 
and input from the Eclipse community. You are invited to comment on and/or join the project. 
Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.platform">
http://www.eclipse.org/newsportal/thread.php?group=eclipse.platform</a> newsgroup.</p>

<h2>Background</h2>
<p>The Eclipse Project currently has separate incubation components under three of its sub-projects: 
Equinox, Platform, and PDE. Although these components are successfully being used for 
exploratory/experimental/forward-looking development, they are constrained (because they are located 
within their respective projects) to follow the strictest IP guidelines.</p>

<p>In addition, the expectation is that the remaining Eclipse sub-project, JDT, will require an incubator 
area in the near future.</p>

<h2>Project Overview</h2>
<p>The Eclipse Project Incubator will move the existing incubator components (Equinox, Platform and PDE) under the new 
sub-project so that they fall within the new Eclipse Foundation IP guidelines, which allow projects in incubation 
more flexibility. The existing component level infrastructure will otherwise be maintained. An additional JDT component 
will be created when there is a clear need.</p>

<p>By doing this, we will also increase the potential for synergy between the components and make the separation between 
experimental efforts and "mainline" development work clearer to the community.</p>

<h2>Scope</h2>
<p>The Eclipse Project Incubator will focus on new development in areas that are relevant to the other Eclipse Project 
sub-projects, which because of their nature would not be appropriate for direct inclusion in the effected sub-project. 
This could be because the work is still experimental, will have a longer timeline than can be contained within a single 
release, has dependencies on external IP that has not yet cleared the Eclipse Foundation IP process, or is simply 
potentially too destabilizing in nature.</p>

<h2>Out of Scope</h2>
<p>Work in the Eclipse Project Incubator will be constrained to only those efforts that we expect to graduate (i.e. the code will 
eventually become part of one of the other Eclipse Project sub-projects). It is not a playground for arbitrary development 
efforts. In addition, we should ensure that investment in the incubator never leaves the community with the perception 
that it is coming at the cost of important work on the other Eclipse Project sub-projects.</p>

<h2>Organization</h2>
<p>The Eclipse Project Incubator will be a new sub-project of the Eclipse Project, containing one component for each of 
the other Eclipse sub-projects (i.e. Equinox, Platform, JDT, and PDE). The current incubator components will simply be 
moved under this sub-project.</p>

<h2>Initial committers</h2>
<p>The initial committers will be all committers on the current incubator components. Commit rights will be handled on a 
component-by-component basis.</p>
<ul>
<li>Mike Wilson: Project Lead</li>
<li>Mike Wilson: Platform Incubator Lead</li>
<li>Jeff McAffer: Equinox Incubator Lead</li>
<li>Wassim Melhem: PDE Incubator Lead</li>
</ul>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
