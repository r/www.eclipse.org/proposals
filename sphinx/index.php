<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">



<h1>Sphinx</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Sphinx");
?>

<h2>Introduction</h2>
<p>Sphinx is a proposed new open source project under the Eclipse <a
	href="http://www.eclipse.org/modeling/mdt/">Model Development Tools</a>
(MDT) subproject to provide an extensible platform that eases the
creation of integrated modeling tool environments supporting individual
or multiple modeling languages (which can be UML-based or native DSLs)
and has a particular focus on industrial strength and interoperability.</p>
<p>This project is in the Proposal Phase (as defined in the <a
	href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse
Development Process</a>) and this document is written to declare its
intent and scope. This proposal is written to solicit additional
participation and input from the Eclipse community. You are invited to
comment and/or join in the development of the project. Please send all
feedback to the <a
	href="http://www.eclipse.org/forums/eclipse.modeling.mdt">eclipse.modeling.mdt</a>
forum.</p>
<h2>Background</h2>
<p><a href="http://en.wikipedia.org/wiki/Model_based_design">Model-based
design</a> (MBD) and <a
	href="http://en.wikipedia.org/wiki/Model_Driven_Software_Development">Model
Driven Software Development</a> (MDSD) have become very popular and are
increasingly used in software and systems development. They were
introduced in the IT industry first, leading to the definition of <a
	href="http://www.uml.org/">Unified Modeling Language</a> (UML).
Subsequently, they penetrated vertical domains and were applied to
embedded software and systems development. Being a generic modeling
language, UML needs to be extended/specialized to fit into these
specific domains. For that purpose, there are mainly two alternatives
which are usually called the heavy-weight and the light-weight approach.
The former involves the definition of <a
	href="http://en.wikipedia.org/wiki/Domain-specific_language">Domain-Specific
Languages</a> (DSL), i.e., full meta-models which are implemented
independently of UML, while the latter leads to the definition of UML
extensions/specializations using the profile concept. From an end user
perspective, both approaches eventually yield the same result that is to
say a dedicated modeling language for a specific domain or/and for some
particular concerns. <a href="http://www.sysml.org/">SysML</a> (<b>S</b>ystems
<b>M</b>odeling <b>L</b>anguage), and <a href="http://www.omgmarte.org/">MARTE</a>
(<b>M</b>odeling and <b>A</b>nalysis of <b>R</b>eal-<b>T</b>ime and <b>E</b>mbedded
systems) are two of the most important standardized UML profiles. There
are destined to adapt UML to systems engineering and the real-time and
embedded domain respectively. Examples of &quot;native&quot; DSLs are <a
	href="http://www.autosar.org/">AUTOSAR</a> (<b>AUT</b>omotive <b>O</b>pen
<b>S</b>ystem <b>AR</b>chitecture) which is an industry standard in the
automotive domain, and the <a href="http://www.aadl.info/">AADL</a> (<b>A</b>rchitecture
<b>A</b>nalysis & <b>D</b>esign <b>L</b>anguage) standard which has its
origins in the avionics industry.</p>
<p>While offering significant advantages from a conceptual point of
view, MBD and MDSD still suffer from a major shortcoming: there is no
satisfying out-of-the-box tool support. Tools for UML often don't
provide sufficient support for profiles and tools for DSLs offer most of
the time a rather poor user experience and/or are not yet very mature.
It has therefore become a common practice that organizations intending
to use MBD and MDSD either develop their own modeling tools or make
substantial customizations of existing ones in order to obtain
appropriate tool support for the modeling languages they want to rely
on.</p>
<p>The Eclipse ecosystem at large and the <a
	href="http://www.eclipse.org/modeling/">Eclipse Modeling Project</a>
(EMP) in particular provide many useful frameworks and building blocks
for creating such tool support and are therefore a great deal of help in
this context. However, experience has shown that the matter of creating
an integrated modeling tool environment for a given modeling language
means much more than just putting existing Eclipse components together.
What is still missing is some sort of &quot;umbrella&quot; framework
which provides the necessary glue and guarantees that Eclipse components
can be consistently integrated in higher level modeling tool
environments. As a result, all effort related to this part needs to be
spent repeatedly in every modeling tool development project and makes
that the latter frequently become major endeavors demanding considerable
investment. This is especially true when modeling tools are required to
handle large models. A dreaded amount of effort goes into investigating
and optimizing the interplay of involved Eclipse components and making
sure that user expectations in terms of scalability and robustness are
met.</p>
<p>Another challenge is that modeling tools are rarely used in isolated
contexts. The question is much more about how to support to complete
model-based/model-driven development processes which may consist of
various stages and may involve different modeling languages at each
state. Until now, there is simply no guarantee that Eclipse-based
modeling tools from different vendors can effectively work together and
be complemented by special purpose in-house modeling tools. Setting up
integrated tool environments which smoothly support the modeling
languages required by a given development process and link and
synchronize them with each other therefore remains a largely unresolved
issue.</p>
<h2>Scope</h2>
<p>Sphinx is a project providing a modeling tool platform for Eclipse
that eases the development of IDE-like tool support for modeling
languages used in software and systems development.</p>
<p>The objectives of the Sphinx project are:</p>
<ul>
	<li>/O1/ To provide an open and extensible platform enabling rapid
	creation of integrated modeling tool environments (IME) for individual
	or multiple modeling languages;</li>
	<li>/O2/ To ensure that the resulting modeling tool support yields
	industrial strength scalability and robustness out-of-the box;</li>
	<li>/O3/ To support a domain- and vendor-independent interoperability
	layer (backbone) for off-the-shelf and in-house modeling tool
	components supporting the same or different modeling languages which
	users can easily combine to create individually tailored continuous
	modeling tool chains;</li>
	<li>/O4/ To support controlled and coordinated management either of
	individual modeling tool components (e.g., update, upgrade, addition,
	removal, activation, deactivation, etc.) or of entire modeling tool
	chains (e.g., coordinated switchover to new versions of multiple
	mutually-coupled modeling tool components, reverting to old modeling
	tool chain configurations in case of problems or when needing to work
	on retired projects).</li>
</ul>
<p>Sphinx does not aim at providing finished integrated modeling tool
environments for any specific modeling language. Instead, it will be
focused on common infrastructure that is typically required when
developing such tool environments. This infrastructure will all the same
be of strictly generic nature and in no way depend on the modeling
language(s) to be supported.</p>
<p>It is also important to note that the intention of Sphinx is not to
reinvent or replace any existing parts of Eclipse, in particular the
various components of the <a href="http://www.eclipse.org/modeling/">Eclipse
Modeling Project</a> (EMP). Sphinx will rather leverage many of those
and build upon them. The mission will be to provide the glue and
additional things that are necessary to orchestrate these components in
a consistent manner and to realize accustomed integrated tool
environments supporting one or several modeling languages at reasonable
effort and cost.</p>
<p>A good part of the ideas and code contributions behind this modeling
tool platform descend from the research project <a
	href="http://www.edona.fr/scripts/home/publigen/content/templates/show.asp?L=EN&P=55&vTicker=alleza">EDONA</a>
funded by the French government.</p>
<h2>Description</h2>
<h3>Initial components</h3>
<p>Sphinx will initially define APIs and provide implementations for the
services described in the following. Components realizing these services
already exist and will be contributed along with the creation of this
project (see <i>Code contributions</i>).</p>
<ul>
	<li><b>Workspace Management</b>: This component is built on EMF, EMF
	Transaction, and Eclipse Platform. It provides services for managing
	the lifecycle and editing domains of model instances that need to be
	centrally provided and shared in the workspace of Sphinx-based modeling
	tool applications. Included features are:
	<ul>
		<li>Mapping of workspace resources to editing domains;</li>
		<li>Thread-safe loading, reloading, and unloading of shared model
		instances;</li>
		<li>Management of dirty state and thread-safe saving of shared model
		instances;</li>
		<li>Automatic synchronization of shared model instances with changes
		on workspace resources.</li>
	</ul>
	</li>
	<li><b>Navigator View and Editor Sockets</b>: This component is built
	on EMF, EMF Transaction, Eclipse Common Navigator Framework, Eclipse UI
	Forms, and GMF (later on also Graphiti, see <i>Future components</i>).
	It provides common logic for creating explorer views, form editors, and
	graphical editors operating on shared model instances in Sphinx-based
	modeling tool applications. Included features are:
	<ul>
		<li>Thread-safe operation on shared model instances in the workspace
		rather than on individually loaded resources;</li>
		<li>Dirty state indication (asterisk on view/editor tab) and save
		button enablement according to dirty state of underlying shared model
		instance;</li>
		<li>View/editor-relative undo/redo context management for operations
		on model elements and workspace resources;</li>
		<li>Model-oriented editor behavior, i.e., rather than always having to
		open a complete resource containing model elements in an editor,
		editors can be opened on individual model elements coming from the
		same or different resource;</li>
		<li>&quot;Link with Editor&quot; capability, &quot;Show In&quot;
		capability, drag &amp; drop, and view state saving and restoration for
		model elements in explorer views;</li>
		<li>Advanced filter capabilities for model elements, Properties view
		content , and palettes in graphical editors;</li>
		<li>Advanced graphical editor support (multi-editor and multi-diagram
		support, palette customization, preferences);</li>
		<li>Support for creating links between model elements from different
		modeling languages.</li>
	</ul>
	</li>
	<li><b>Validation Runtime Extensions</b>: This component is built on
	EMF, EMF Transaction, EMF Validation, and Eclipse Platform. It provides
	extended runtime-level services for validating models or model
	fragments in Sphinx-based modeling tool applications and visualizing
	validation results. Included features are:
	<ul>
		<li>Manual (on-demand) and automatic (on-the-fly) validation;</li>
		<li>Validation problem marker management;</li>
		<li>Model-oriented Validation view (alias Problems view);</li>
		<li>Validation label decorator for model explorer view.</li>
	</ul>
	</li>
	<li><b>Compare &amp; Merge Integration</b>: This component is built on
	EMF, EMF Transaction, EMF Compare, and Eclipse Compare Support. It
	provides extensions enabling model-based compare/merge operations to be
	carried out on shared model instances in Sphinx-based modeling tool
	applications. Included features are:
	<ul>
		<li>Thread-safe operation on shared model instances in the workspace
		rather than on individually loaded resources;</li>
		<li>Thread-safe automatic merge operations;</li>
		<li>Comparison/merge of individual model elements or fragments
		(instead of complete resources only);</li>
		<li>Model-oriented dirty state management and editor-relative
		undo/redo contexts.</li>
	</ul>
	</li>
	<li><b>EMF Runtime &amp; Eclipse Platform Extensions</b>: This
	component is built on EMF, EMF Transaction, and Eclipse Platform. It
	provides common runtime-level enhancements such as description of
	meta-models, description of shared model instances including their
	scopes, meta-model compatibility services, as well as a couple of
	performance optimizations and utilities. They are used by all other
	Sphinx platform components and are also available to Sphinx-based
	modeling tool applications.</li>
</ul>
<h3>Future components</h3>
<p>The components that have been realized so far provide only a small
subset of services which are essential for building integrated modeling
tool environments. Much other functionality is necessary and useful and
will be added to Sphinx later on. Potential future Sphinx components
include but are not limited to:</p>
<ul>
	<li>Model query and indexing service for significantly improving
	performance and memory consumption based on <a
		href="http://www.eclipse.org/modeling/emf/?project=query2#query2">EMF
	Query 2</a>;</li>
	<li>Model repository and database persistence support based on <a
		href="http://www.eclipse.org/modeling/emf/?project=cdo#cdo">CDO</a>
	and <a href="http://www.eclipse.org/proposals/mtf/">MFT</a>;</li>
	<li>Connector for documentation and report generation with <a
		href="http://www.eclipse.org/birt/">BIRT</a>;</li>
	<li>Socket for <a
		href="http://www.eclipse.org/modeling/tmf/?project=xtext#xtext">Xtext</a>-based
	textual editors and <a
		href="http://www.eclipse.org/proposals/graphiti/">Graphiti</a>-based
	graphical editors supporting thread-safe operation on shared model
	instances in the workspace, model-oriented dirty state management, and
	editor-relative undo/redo contexts;</li>
	<li>Code generation interfaces supporting existing <a
		href="http://www.eclipse.org/modeling/m2t/">M2T</a> languages and
	runtimes (e.g., Xpand, MTL, JET);</li>
	<li>Model to model transformation interfaces supporting existing <a
		href="http://www.eclipse.org/m2m/">M2M</a> languages and runtimes
	(e.g., Xtend, QVT, ATL);</li>
	<li>Model search & replace using <a
		href="http://www.eclipse.org/modeling/emft/?project=search#search">EMF
	Search</a>;</li>
	<li>Model variant handling services using <a
		href="http://www.eclipse.org/proposals/feature-model/">EMF Feature
	Model</a>;</li>
	<li>Task focused modeling through integration with <a
		href="http://www.eclipse.org/mylyn/">Mylyn</a>;</li>
	<li>Dedicated communication facilities to enable effective
	collaboration on modeling tasks in distributed teams (e.g. by using
	Google Wave protocol from <a href="http://eclipse.org/ecf/">ECF</a>);</li>
	<li>Build system supporting mixed model-based and file-based processing
	(e.g., model validation, code generation, code compilation) and
	performing incremental, full, or continuous builds (could possibly use
	parts of <a href="http://wiki.eclipse.org/PDE/Incubator/b3/Proposal">B3</a>).</li>
</ul>
<h2>Relationship with other Eclipse Projects</h2>
<ul>
	<li>Sphinx will be built on top of the <a
		href="http://www.eclipse.org/platform/">Eclipse Platform</a> and <a
		href="http://www.eclipse.org/modeling/emf/">EMF</a>.</li>
	<li>Sphinx will also use complementary components of the <a
		href="http://www.eclipse.org/modeling/">Eclipse Modeling Project</a>
	(EMP). <a
		href="http://www.eclipse.org/modeling/emf/?project=transaction#transaction">EMF
	Transaction</a>, <a
		href="http://www.eclipse.org/modeling/emf/?project=validation#validation">EMF
	Validation</a>, <a
		href="http://www.eclipse.org/modeling/emf/?project=compare#compare">EMF
	Compare</a>, and <a href="http://www.eclipse.org/modeling/gmf/">GMF</a>
	will be needed immediately. Others like <a
		href="http://www.eclipse.org/modeling/emf/?project=query2#query2">EMF
	Query 2</a>, <a
		href="http://www.eclipse.org/modeling/emf/?project=cdo#cdo">CDO</a>, <a
		href="http://www.eclipse.org/birt/">BIRT</a>, <a
		href="http://www.eclipse.org/modeling/tmf/?project=xtext#xtext">Xtext</a>,
	<a href="http://www.eclipse.org/proposals/graphiti/">Graphiti</a>,
	parts of <a href="http://www.eclipse.org/modeling/m2t/">M2T</a> and <a
		href="http://www.eclipse.org/m2m/">M2M</a>, <a
		href="http://www.eclipse.org/modeling/emft/?project=search#search">EMF
	Search</a>, <a href="http://www.eclipse.org/proposals/mtf/">MTF</a>, <a
		href="http://www.eclipse.org/proposals/feature-model/">EMF Feature
	Model</a>, etc. are likely to be required later on.</li>
	<li>Sphinx will integrate services, examples and ideas from the <a
		href="http://wiki.eclipse.org/Papyrus_Developer_Guide">backbone</a> of
	<a href="http://www.eclipse.org/modeling/mdt/?project=papyrus#papyrus">Papyrus</a>.
	The latter already has a service-oriented <a
		href="http://dev.eclipse.org/svnroot/modeling/org.eclipse.mdt.papyrus/trunk/plugins/developer/org.eclipse.papyrus.doc/architecture/sashAndCoreArchitecture.doc%20for%20details%20on%20the%20Papyrus%20backbone%20architecture">architecture</a>
	and will provide an important set of contributions to the Navigator
	View and Editor Sockets component of Sphinx (see <i>Initial components</i>
	for details).</li>
	<li>In order to enable smooth integration of modeling tools created
	with Sphinx with complementary non-modeling tools used in software and
	systems development, Sphinx will make use of related Eclipse projects
	and components where applicable. For example, <a
		href="http://www.eclipse.org/cdt/">CDT</a> could be leveraged for
	linking modeling with implementation by enabling navigation,
	validation, quick fixing, auto completion, etc. across the two. Other
	candidate projects are <a href="http://eclipse.org/jdt/">JDT</a>, <a
		href="http://eclipse.org/mylyn/">Mylyn</a>, <a
		href="http://eclipse.org/ecf/">ECF</a>, and <a
		href="http://wiki.eclipse.org/PDE/Incubator/b3/Proposal">B3</a>.</li>
	<li>Sphinx will make contributions to the Eclipse projects and
	components it is using as appropriate. Such contributions may consist
	of bug fixes, performance optimizations, API extensions, or utility
	enhancements.</li>
	<li>Sphinx has conceptual kind of relationships to the <a
		href="http://www.eclipse.org/imp/">IDE Meta-tooling Platform</a> (IMP)
	and <a href="http://www.eclipse.org/tigerstripe/">Tigerstripe</a>
	projects:
	<ul>
		<li>Since its beginnings, Eclipse has been shipped with rich
		integrated tool support (IDEs) for various programming languages. IMP
		is a project providing building blocks which are necessary to create
		additional IDEs for programming languages that are not yet supported
		under Eclipse. Introducing a similar kind of meta-tooling platform for
		modeling languages, the Sphinx project represents a perfect analog to
		IMP for the modeling world.</li>
		<li>Tigerstripe is a set extensible set of modeling tools targeting
		the telecommunications vertical domain. It encompasses an
		implementation and graphical editing support for a DSL derived from
		UML2 which is dedicated to model-based design of communication
		systems. It also supports model-driven development through extensible
		generation of application code, APIs, specifications or documentation.
		Tigerstripe therefore represents an example of a concrete modeling
		tool environment for which Sphinx could provide the meta-tooling. Even
		if this does not become the case in practice, it could still make
		sense to investigate if the two projects could benefit from each
		other.</li>
	</ul>

</ul>
<h2>Organization</h2>
<h3>Mentors</h3>
<ul>
	<li>Ed Merks (<a href="http://www.macromodeling.com/">Macro Modeling</a>,
	Canada)</li>
	<li>Sven Efftinge (<a href="http://www.itemis.com/">itemis</a>,
	Germany)</li>
</ul>
<h3>Proposed initial committers</h3>
<p>The initial committers for this project are:</p>
<ul>
	<li>Stephan Eberle (<a href="http://www.geensys.com/">Geensys</a>,
	France), lead</li>
	<li>S&eacute;bastien G&eacute;rard (<a
		href="http://www-list.cea.fr/gb/index_gb.htm">CEA LIST</a>, France),
	lead</li>
	<li>Ali Akar (<a href="http://www.geensys.com/">Geensys</a>, France)</li>
	<li>Yannick Didierjean (<a href="http://www.geensys.com/">Geensys</a>,
	France)</li>
	<li>Romain Sezestre (<a href="http://www.geensys.com/">Geensys</a>,
	France)</li>
	<li>Dao Hoang (<a href="http://www.gate-technology.com/">GATe Technolgy</a>,
	subsidiary of Geensys in Vietnam), testing</li>
	<li>Paul Hoser (<a href="http://www.bmw-carit.de/">BMW Car IT</a>,
	Germany)</li>
	<li>Christian Kn&uuml;chel (<a href="http://www.bmw-carit.de/">BMW Car
	IT</a>, Germany), release engineering</li>
	<li>Kenn Hussey (Independent, Canada)</li>
	<li>Yann Tanguy (<a href="http://www-list.cea.fr/gb/index_gb.htm">CEA
	LIST</a>, France)</li>
	<li>C&eacute;dric Dumoulin (<a href="http://www.lifl.fr/">Lifl</a>,
	France)</li>
	<li>Thibault Landre (<a href="http://www.fr.atosorigin.com/">Atos
	Origin</a>, France)</li>
</ul>
<h3>Interested parties</h3>
<p>The following individuals, organizations, companies, and projects
have expressed interest in this project:</p>
<ul>
	<li><a href="http://www.bosch.com/">Bosch</a>, Germany (contact: Harald
	Mackamul)</li>
	<li><a href="http://www.conti-engineering.com/">Continental Engineering
	Services</a>, Germany (contact: Stefan Voget)</li>
	<li><a href="http://www.itemis.com/">itemis</a>, Germany (contact:
	Andreas Graf)</li>
	<li><a href="http://www.mia-software.com/en/">Mia Software</a>, France 
	(contact: Fr&eacute;d&eacute;ric Madiot)
	<li><a href="http://generic-concept.com/blogs/olivier/">Olivier Mo&iuml;ses</a>, 
	France (Generic Concept)</li>
	<li><a href="http://www.opensynergy.com/">OpenSynergy</a>, Germany
	(contact: Mark Br&ouml;rkens)</li>
	<li><a href="http://www.psa-peugeot-citroen.com/">PSA Peugeot
	Citro&euml;n</a>, France (contact: Aldric Loyer)</li>
	<li><a href="http://www.pure-systems.com/">pure-systems</a>, Germany 
	(contact: Holger Papajewski)</li>
	<li><a href="http://www.sap.com/">SAP</a>, Germany (contact: Bernd
	Kolb)</li>
	<li><a href="http://www.zeligsoft.com/">Zeligsoft</a>, Canada (contact:
	Bran Selic)</li>
</ul>
<h3>Code contributions</h3>
<p>The initial code contribution will come from both the <a
	href="http://www.artop.org">Artop</a> and the <a
	href="http://www.eclipse.org/modeling/mdt/?project=papyrus#papyrus">Papyrus</a>
project. Both projects benefit from the research project <a
	href="http://www.edona.fr/scripts/home/publigen/content/templates/show.asp?L=EN&P=55&vTicker=alleza">EDONA</a>
which is funded by the French government and contributes a part of its
results to Artop and Papyrus.</p>
<h4>Artop</h4>
<p>Artop is based on Eclipse and provides common base functionality for
creating modeling tools supporting the <a href="http://www.autosar.org">AUTOSAR</a>
standard. AUTOSAR is a design standard from the automotive industry
focusing on the system architecture of control software for road
vehicles, its deployment to networked ECU (Electronic Control Unit)
devices in vehicles, and the configuration of the basic software in such
ECUs.</p>
<p>Artop essentially encompasses implementations of the different
releases of the AUTOSAR meta-model (alias DSL) plus a rich set of
services and components for managing and processing AUTOSAR models. A
particular characteristic of Artop is the capability of handling
relatively large AUTOSAR models in an efficient way. Artop therefore
corresponds in almost all aspects to what is intended to be done in the
Sphinx project. The only conceptional difference is that Artop is still
dedicated to one specific design standard and DSL (namely AUTOSAR) in
one specific vertical domain (namely automotive). Another limitation is
that Artop could not be made available under <a
	href="http://www.eclipse.org/org/documents/epl-v10.php">Eclipse Public
License</a> (EPL) because it contains material which is protected by
AUTOSAR IP regulations and must be kept accessible to AUTOSAR members
and partners only.</p>
<p>Sphinx can therefore be seen as a generalization of the Artop idea
aiming at a modeling tool platform for arbitrary design standards and
modeling languages in arbitrary vertical domains being available under
EPL. Thanks to the architecture of Artop, this can be achieved quite
quickly: An important portion of the services and components in Artop
has been realized in a generic way and is located in a separate layer
(Eclipse Complementary Layer, ECL). It has no dependencies to the other
AUTOSAR-specific layer of the platform (Artop AUTOSAR Layer, AAL) which
contains the AUTOSAR meta-model implementations plus some related
services and is subject to AUTOSAR IP regulations. The idea therefore is
to move the complete ECL layer from Artop to Sphinx and provide mature
initial implementations for the components proposed above right away.</p>
<h4>Papyrus</h4>
<p>Papyrus is a component of the <a
	href="http://www.eclipse.org/modeling/mdt/">Model Development Tools</a>
(MDT) subproject aiming at providing an integrated, user-consumable
environment for editing any kind of EMF model and particularly
supporting <a href="http://www.uml.org/">UML</a> and related modeling
languages such as <a href="http://www.sysml.org/">SysML</a> and <a
	href="http://www.omgmarte.org/">MARTE</a>. Papyrus provides diagram
editors for EMF-based modeling languages amongst them UML 2 and SysML
and the glue required for integrating these editors (GMF-based or not)
with other MBD and MDSD tools. It also offers a very advanced support of
UML profiles that enables users to define editors for DSLs based on the
UML 2 standard and its extension mechanisms. The main feature of Papyrus
regarding this latter point is a set of very powerful customization
mechanisms which can be leveraged to create user-defined Papyrus
perspectives and give it the same look and feel as a native DSL editor.</p>
<p>Papyrus consists of a set of plug-ins which are based on the Papyrus
core plug-in. This latter allows &quot;nested editors&quot; to be
plugged in which may be based on different technologies (e.g., GEF,
GMF). Papyrus core also supports multi-editor views (called sash
windows) in which nested editors can be arranged side by side in tabs.
The Papyrus core is designed to be open. It easily enables new nested
editors to be added and transversal services to be made available to all
nested editors. All model management facilities (e.g., loading, saving,
synchronizing) are expected to be provided by external services. The
main advantage of this architecture is its flexibility: it is possible
to use the same nested editors in conjunction with different external
services according to the usage context of Papyrus. Finally, another
main point of Papyrus is that all nested editors share the same set of
resources.</p>
<p>The possible main contribution of Papyrus to Sphinx will be its core
plug-in (including its capability of connecting editors to external
services, its sash window support, preferences support, filtering
facilities and its model explorer). This way, Sphinx can benefit from a
set of services that are already used to support UML editors and
graphical editors for any other EMF-based modeling language.</p>
<h3>Tentative Plan</h3>
<table>
	<tr>
		<td valign=top>Feb 2010:</td>
		<td valign=top>Proposal published and announced to Eclipse membership</td>
	</tr>
	<tr>
		<td valign=top>Apr/Mai 2010:</td>
		<td valign=top>Initial code contribution from Artop and Papyrus
		project</td>
	</tr>
	<tr>
		<td valign=top>Mai 2010:</td>
		<td valign=top>Start of architecture reconciliation between Artop ECL
		and Papyrus backbone</td>
	</tr>
	<tr>
		<td valign=top>Sep 2010:</td>
		<td valign=top>First release of Artop and Papyrus based on Sphinx
		initial contribution<br>
		(and retirement of ECL layer at Artop and Backbone in Papyrus)</td>
	</tr>
	<tr>
		<td valign=top>Oct 2010 - Feb 2011:</td>
		<td valign=top>Completion of architecture reconciliation between Artop
		ECL and Papyrus backbone,<br>
		Implementation of architectural and behavioral improvements,<br>
		Migration of Artop and Papyrus to consolidated Sphinx platform</td>
	</tr>
	<tr>
		<td valign=top>Jun 2011:</td>
		<td valign=top>First release of Sphinx (a part of simultaneous release
		train)</td>
	</tr>
</table>

<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>10-Mar-2010</td>
		<td>Text modifications requested by the Papyrus project. Quotation of
		French research project EDONA which Sphinx is benefiting from in terms
		of funding. Updated Tentative plan Added two new interested parties:
		Bosch and OpenSynergy</td>
	</tr>
</table>
</div>
</div>
<?php
$html = ob_get_contents();
ob_end_clean();

# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
