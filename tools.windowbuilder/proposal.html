<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
      "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <title>WindowBuilder</title>
</head>

<body>
<p>The WindowBuilder project is a proposed open source project under the <a
href="http://www.eclipse.org/tools/">Eclipse Tools Project</a>.</p>

<p>This proposal is in the Project Proposal Phase (as defined in the Eclipse
Development Process) and is written to declare its intent and scope. We solicit
additional participation and input from the Eclipse community. Please send all
feedback to the <a
href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<h2>Background</h2>

<p>A GUI builder is a visual programming tool that lets a user build a
graphical user interface by dragging and dropping elements from a palette onto
a design surface. GUI builders typically contain Wizards and templates that
help automate the GUI building process. GUI building tools allow developers to
spend less time and money creating Java GUIs, because they can focus on
creating application-specific functionality rather than coding the low-level
logic required for GUIs to run.</p>

<p>According to Ben Galbraith, in his Successful GUI Building blog, &ldquo;In
my experience, the number one reason why GUI builders hold appeal is that they
often lead to an order of magnitude improvement in productivity over hand
coding, especially amongst average-level Swing developers. I consider myself
above average, and I see improvements in productivity around 2x-10x (perhaps
higher) in my own work.&rdquo;</p>

<p>Created in 2003 by Instantiations, WindowBuilder is widely regarded as the
the best GUI builder in the Java world (winning the award for Best Commercial
Eclipse Tool in 2009). Beyond being an exemplary example of a GUI design tool
for SWT, XWT, Swing and Google&rsquo;s GWT, WindowBuilder is also a highly
extensible and customizable framework (with 50+ existing extension points) for
creating new GUI design tools based on any UI toolkit for any language. Google
re-launched WindowBuilder as a free product available to any member of the
Eclipse community soon after its acquisition of Instantiations in 2010, and now
wishes to contribute the framework (the WindowBuilder Engine) and associated
products (SWT Designer and Swing Designer) to the Eclipse Foundation. Google
will continue to invest heavily in the project, as it will continue to be the
basis for its own in-house GWT Designer tool.</p>

<h2>Scope</h2>

<p>The scope of the project is to provide the definitive Eclipse extensible
framework for creating GUI design tools for any language or UI framework. The
current framework provides solid support for Java and XML based UI frameworks
and exemplary tools for creating Swing and SWT UIs. Extending the framework to
support additional languages (C++, JavaScript, PHP, etc.) and UI toolkits in
the future is highly desirable.</p>

<h2>Description</h2>

<p>The WindowBuilder project goal will be to continue to develop the
WindowBuilder Engine (WBE) framework for tool vendors wishing to support UI
toolkits in a variety of languages. The framework currently has built-in
support for Java-based (Swing, SWT, RCP, etc.) and XML-based (XWT) UI
frameworks and can be extended to support any language for which a parser (and
associated AST model) exists.</p>

<p>The project will also focus on continuing to develop and enhance the
exemplary tool examples for SWT/RCP/XWT and Swing for the benefit of developers
using Eclipse to build their own applications using those UI frameworks.</p>

<p>GUI building tools allow developers to spend less time and money creating
Java GUIs. Here are some key features provided by the WindowBuilder Engine:</p>
<ul>
  <li><strong>WYSIWYG visual designer and editing</strong> &mdash; The WBE
    takes the guesswork out of building and modifying a GUI. It contains both
    design and source views allowing developers to view content identical to
    the end result while the interface is being created.</li>
  <li><strong>Flexibility and consistency across frameworks</strong> &mdash;
    The WBE is easy to use and provides flexibility across different types of
    UI frameworks, meaning that the tool has a flexible parser to parse its own
    code as well as code written by hand or by other GUI building tools. It
    also provides a consistent development experience no matter which UI
    framework is being used.</li>
  <li><strong>Bi-directional code generation (round-trip editing)</strong>
    &mdash; Bi-directional code generation is important so that the visual
    design and source are always synchronized no matter which you edit.</li>
  <li><strong>Drag-and-drop</strong> &mdash; The WBE permits developers to
    implement exactly what they want, where they want it, by simply dragging
    and dropping GUI elements into the design layout view.</li>
  <li><strong>Wizards and templates</strong> &mdash; These features give
    developers a head-start in designing a GUI because they don&rsquo;t have to
    re-create the basics.</li>
  <li><strong>Reverse engineering capabilities</strong> &mdash; The WBE allows
    a developer to read and write code coming from a variety of sources
    &mdash;such as code generated by other GUI builders, code created by hand,
    or code created in another application. This feature means that developers
    do not have to start from scratch on developing the GUI because they can
    use the existing code as a starting basis, especially with a product that
    is already in production.</li>
  <li><strong></strong><strong>Localization and Internationalization</strong>
    &mdash; Localization and internationalization capabilities allow the
    application under development to be easily adapted to various languages and
    regions without engineering changes. The WBE has powerful
    internationalization (i18n) support that helps automate the localization
    process. It can externalize component strings, create and manage resource
    bundles for multiple languages, and edit translated strings in context. The
    WBE allows developers to switch which language is used to render the GUI on
    the fly to see the effect of different length strings. The WBE makes it
    easy to support different languages by adding existing locales or creating
    new locales from existing language and country codes.</li>
  <li><strong>Minimal code changes</strong> &mdash; The WBE makes minimal edits
    to the source code and maintains changes made by developers to customize
    the GUI. The tool knows which lines of code are GUI-related and parses only
    those. It ignores non-GUI code added by the user and preserves that code
    any time it edits the file. The WBE uses a style of micro editing that
    ensures that the smallest possible change is made to the source, again
    preserving all user code and formatting.</li>
  <li><strong>Morphing tools</strong> &mdash; The WBE contains advanced
    features such as a Morphing tool that allows the developer to change one
    widget type into another.</li>
  <li><strong>Visual inheritance</strong> &mdash; The WBE supports visual
    inheritance so that code can be inherited from a parent-child hierarchy.
    Visual Inheritance is the inheritance of visual elements in a window such
    as buttons and panels in derived GUI classes. Inheriting forms is a
    powerful feature that allows standardization of common GUI elements on a
    window that may be changed in one place rather than in countless individual
    windows.</li>
  <li><strong>Factories</strong> &mdash; The WBE supports the creation of
    custom factory classes and methods. This is a convenient way to create
    customized, reusable versions of common components. It allows developers to
    create a static factory method from any widget, morph any widget into a
    factory instance, or add any factory component to the palette. This feature
    saves time and is a good method of building versions of commonly used
    components.</li>
  <li><strong>Data Binding</strong> &mdash; The WBE supports Data binding which
    allows easy linking of UI elements and models so that users can edit or
    view the data in the model. Traditionally, this is what you need to do to
    link, for example, a text widget to a String property of a model
  object.</li>
  <li><strong>Auto-discovery of UI supported frameworks</strong> &mdash; The
    WBE supports the automatic discovery and one-click loading of supported UI
    frameworks. For example, a developer using the WBE and SWT tools can easily
    load the Swing tools if needed and vice versa.</li>
</ul>

<h2>Initial Contribution</h2>

<p>The initial contribution will be composed of three main parts:</p>
<ul>
  <li><strong>WindowBuilder Engine</strong> &mdash; the core extensible
    framework providing support for all of the features listed above.</li>
  <li><strong>SWT Designer</strong> &mdash; an exemplary implementation of an
    GUI design tool that supports SWT, JFace, RCP, eRCP and XWT. This tool also
    provides excellent support for the Nebula widget collection and the Eclipse
    Data Binding framework.</li>
  <li><strong>Swing Designer</strong> &mdash; an exemplary implementation of an
    GUI design tool that supports Swing. This tool also provides excellent
    support for 3rd party layout managers such as JGoodies FormLayout and
    MiGLayout as well as support for the Swing Data Binding framework.</li>
</ul>

<p>The goal is to provide an even more useful framework and associated tools,
so contributions from the community are welcome. Example contributions that we
would hope to see include:</p>
<ul>
  <li>Support for new widgets and widget collections</li>
  <li>Support for new layout managers</li>
  <li>Support for new localization patterns</li>
  <li>Support for new Java and XML based UI frameworks</li>
  <li>Support for new languages and associated frameworks</li>
  <li>Support for additional operating systems beyond Windows, Linux and
  OSX</li>
  <li>Support for additional extension points for further customizing the
  tool</li>
</ul>

<h2>Legal Issues</h2>

<p>No legal issues are anticipated although all 3rd party code used in the
product will go through the standard Eclipse IP process.</p>

<h2>Relationships to other Eclipse projects</h2>

<p>The WindowBuilder project has the following relationships to other Eclipse
projects:</p>
<ul>
  <li>WindowBuilder directly competes with the <strong>Visual Editor
    Project</strong> (VEP).</li>
  <li>WindowBuilder makes heavy use of the <strong>Java Development
    Tools</strong> (JDT). The Java Editor is embedded in the WindowBuilder
    editor to create the Source page. The WBE builds a JDT AST model for
    reverse engineering Java code.</li>
  <li>WindowBuilder uses the <strong>Plug-in Development Environment</strong>
    (PDE) to create plug-in and RCP components like Views and Editors and
    update the plugin.xml and manifest.mf files.</li>
  <li>WindowBuilder embeds the XML editor from the <strong>Web Standard
    Tools</strong> (WST) as the Source page when editing XML-based UIs.</li>
  <li>The SWT tooling uses and generates code to support the following Eclipse
    frameworks: <strong>SWT</strong>, <strong>RCP</strong>,
    <strong>eRCP</strong>, <strong>XWT</strong> and <strong>Nebula</strong>.
    Some support is also available for <strong>RAP</strong> and
    <strong>Riena</strong> based applications.</li>
</ul>

<h2>Committers</h2>

<p>The following individuals are proposed as initial committers to the
project:</p>
<ul>
  <li><strong>Eric Clayberg</strong>, Google<br>
    Eric has been the project manager of the existing WindowBuilder project
    over the last seven years and will continue to be heavily involved in all
    facets of the project going forward as the Project Lead.</li>
  <li><strong>Konstantin Scheglov</strong>, Google<br>
    Konstantin is the chief architect and technical lead for the existing
    WindowBuilder project at Google (formerly at Instantiations) and will
    continue to be heavily involved in all facets of the project going
  forward.</li>
  <li><strong>Alexander Mitin</strong>, Google<br>
    Alexander has been on of the key developers for the existing WindowBuilder
    project over the last seven years and will continue to be heavily involved
    in all facets of the project going forward.</li>
  <li><strong>Andrey Sablin</strong>, Google<br>
    Andrey has been a developer on the existing WindowBuilder project for the
    last two years and will continue to be heavily involved in all facets of
    the project going forward.</li>
  <li><strong>Jaime Wren</strong>, Google<br>
    Jaime has been a developer on the existing WindowBuilder project for the
    last two years and will continue to be heavily involved in all facets of
    the project going forward.</li>
  <li><strong>Dan Rubel</strong>, Google<br>
    Dan has been a contributor on the existing WindowBuilder project for the
    last seven years and will continue to be involved in all facets of the
    project going forward.</li>
  <li><strong>Mark Russell</strong>, Google<br>
    Mark has been a build master for the existing WindowBuilder project for the
    last five years and will continue to be responsible for build-related
    issues going forward.</li>
  <li><strong>Devon Carew</strong>, Google<br>
    Devon is interested in contributing to the project.</li>
  <li><strong>Rajeev Dayal</strong>, Google<br>
    Rajeev is interested in contributing to the project.</li>
  <li><strong>Miguel Mendez</strong>, Google<br>
    Miguel is interested in contributing to the project.</li>
  <li><strong>David Carlson</strong>, Google<br>
    David is interested in contributing to the project.</li>
  <li><strong>Alexander Humesky</strong>, Google<br>
    Alexander is interested in contributing to the project.</li>
  <li><strong>Xavier Ducrohet</strong>, Google<br>
    Xavier is interested in contributing to the project.</li>
  <li><strong>Raphael Moll</strong>, Google<br>
    Raphael is interested in contributing to the project.</li>
  <li><strong>Tor Norbye</strong>, Google<br>
    Tor is interested in contributing to the project.</li>
  <li><strong>Ed Merks</strong>, Cloudsmith<br>
    Ed is interested in Data Binding features.</li>
  <li><strong>Kenn Hussey</strong>, Cloudsmith<br>
    Kenn is interested in Data Binding features.</li>
  <li><strong>Christian Campo</strong>, compeople AG<br>
    Christian is interested in SWT features to support Riena.</li>
  <li><strong>Neeraj Bhusare</strong>, Genuitec<br>
    Neeraj is interested in Swing tools.</li>
  <li><strong>Piotrek Tomiak</strong>, Genuitec<br>
    Piotrek is interested in Swing tools.</li>
  <li><strong>Vadim Ridosh</strong>, Genuitec<br>
    Vadim is interested in tools for mobile apps.</li>
</ul>

<p>We welcome additional committers and contributions.</p>

<h2>Mentors</h2>

<p>The following Architecture Council members will mentor this project:</p>
<ul>
  <li><strong>Ed Merks</strong></li>
  <li><strong>Kenn Hussey</strong></li>
</ul>

<h2>Interested Parties</h2>

<p>The following individuals, organizations, companies and projects have
expressed interest in this project:</p>
<ul>
  <li><strong>Cloudsmith</strong> &mdash; interested in all aspects (esp. Data
    Binding)</li>
  <li><strong>Genuitec</strong> &mdash; interested in taking the lead on Swing
    Designer</li>
  <li><strong>Eclipse Riena Team</strong> &mdash; interested in WBE &amp; SWT
    Designer</li>
  <li><strong>Motorola</strong> &mdash; interested in mobile UI tools</li>
</ul>

<h2>Project Scheduling</h2>

<h2>Changes to this Document</h2>

<table border="1">
  <tbody>
    <tr>
      <td><strong>Date</strong></td>
      <td><strong>Change</strong></td>
    </tr>
    <tr>
      <td>01-Dec-2010</td>
      <td>Document created</td>
    </tr>
    <tr>
      <td>15-Dec-2010</td>
      <td>Document released for public review</td>
    </tr>
  </tbody>
</table>

<p></p>

<p></p>

<p></p>
</body>
</html>
