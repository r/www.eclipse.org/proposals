<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
<h1>BPEL Designer Editor</h1><br>

</p>
<h2>Introduction</h2>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("BEPL-Designer");
?>

<p>The BPEL Designer Editor is a proposed open source
project under the <a href="/technology/">Eclipse
Technology Project</a>.</p>
<p>This proposal is in the Project Proposal Phase (as defined in the
<a href="/projects/dev_process/">Eclipse
Development Process document</a>) and is written to declare its intent
and scope. This proposal is written to solicit additional participation
and
input from the Eclipse community. You are invited to comment on and/or
join the
project. Please send all feedback to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.bpel-designer">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.bpel-designer.</a>
newsgroup. <br>
</p>
<p>The goal of this project is to add comprehensive support to Eclipse
for the definition, editing, deploying, testing, and debugging&nbsp; of
BPEL4WS flows (Business Process Definition Language for Web
Services)&nbsp;
in vendor neutral environments. The implementation will be extensible
to third party vendors so that runtime implementations could integrate
seamlessly to the design/build/deploy/test cycle and integrate
specific domain&nbsp; BPEL4WS constructs and extension<br>
</p>
<h2>Background</h2>
<p>BPEL4WS is developing standard supported by the major software
vendors such as Oracle, IBM,
BEA,Microsoft, SAP, and Siebel
Systems. It is a key component of the SOA (Service Oriented
Architecture) stack and it is finding great
acceptance within the community of users and solution providers. While
each vendor is pursuing their own runtime for BPEL&nbsp; the tool
support is lagging behind and is based on vendor specific tool
solutions which developers are finding increasingly less appealing. <br>
</p>
BPEL4WS depends on WSDL 1.1, XML Schema 1.0, XPath 1.0 and
WS-Addressing. Although WSDL and XML schema are integral to the BPEL
design, this project will not be providing any tools to manage these
resources and will leverage parts of the Eclipse ecosystem for
that.&nbsp;
<h2>Description</h2>
<p>This project will provide broad support for integrating BPEL4WS
design time experience with
the Eclipse platform. BPEL4WS is defined in <a
 href="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnbizspec/html/bpel1-1.asp">here</a>.
<br>
</p>
<p>BPEL4WS is a flow type XML language used to orchestrate business
process flows. The key tasks performed by BPEL processes are
manipulation of&nbsp; XML documents
and interaction with Web Services.&nbsp; BPEL4WS depends on the
following XML-based specifications: WSDL 1.1, XML Schema 1.0, XPath 1.0
and WS-Addressing.<br>
</p>
<p>The specific intent of this project is to provide better,
integrated, and extensible design time experience on the Eclipse
platform. Key to the success will be pairings of vendor runtime
environments and design time extensions (palette extensibility or
element extensibility for example). We intend to use the standard
Eclipse mechanism of extension points to allow 3rd party integration of
various such elements.</p>
<p>The two core components of the
project would be visual and source design of the flow and
interpretation of XML
schema model used as definition for BPEL variables and messages. The
project would focus on the design time aspect of BPEL
which includes construction, definition, inspection, and
documentation. The runtime aspect of BPEL is typically vendor specific
and includes such parts as validation, compilation, deployment,
debugging, versioning, and migration.<br>
</p>
<p>While the runtime aspect is not the focus of the project suitable
extension points will be defined to allow pairing of BPEL runtime to
create complete design time experience. At minimum this will include
validation, compilation, and deployment interfaces for extension by 3rd
party vendors. <br>
</p>
<p>At the time of this writing, BPEL 1.1 has been defined but BPEL 2.0
specification has not been finalized and changes are expected before
the final draft is issued. Accordingly, the scope of this proposal will
track the draft specification. More information on BPEL4WS version 2.0
can be found <a
 href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=wsbpel">here.</a>
<span>In the event that version 2.0 of the BPEL
specification is not complete we anticipate releasing versions of the
designer which may not support the full 2.0 specification. Such
intermediate versions may include <span style="font-style: italic;">no
</span>support or <span style="font-style: italic;">partial </span>support
for the unfrozen sections of the BPEL specification. </span><br>
</p>
<h3>Project Principles</h3>
<b>Leverage the Eclipse Ecosystem</b>:&nbsp; It is our goal to
extend the Eclipse user experience as well as leverage other Eclipse
project functionality where applicable.
<p><b>Vendor Neutrality</b>: We intend to build the tool that so that
it is not biased toward any particular BPEL runtime.&nbsp; <span>Proposed
vendor runtime support
is discussed in <a href="#runtime">Runtime Support</a></span>.<br>
</p>
<p><b>Extensibility</b>:&nbsp; It is our intention to provide a set of
extension points to allow for the binding the runtime time environment
and to allow extensibility of component palettes. The implementation
should have a rich model representation of BPEL with support for
validation. That the model should be extensible so that new activities
and expression languages can be defined.<br>
</p>
<p><b>Incremental Development</b>:&nbsp; As this project will be
subject to an evolving specification, an agile development process will
be employed to manage and respond to changing requirements. We will
employ the milestone approach used by many eclipse projects.<br>
</p>
<h2>Organization</h2>
<p>We propose this project should be undertaken as an incubator within
the Eclipse Technology Project. <br>
</p>
<p>Initially BPEL Expert Group members and other individuals and
organizations that have serious interest will be solicited to
participate.&nbsp; This list would cover a broad base of organizations
interested in BPEL.</p>
<h2 align="left">Proposed project lead and initial committers</h3>
<ul>

  <li>
    <p align="left">Project Lead: Michal Chmielewski &lt;<a
 href="mailto:michal.chmielewski@oracle.com?subject=BPEL%20designer%20proposal">michal.chmielewski@oracle.com</a>&gt;,
Oracle Corporation</p>
  </li>

  <li><p align="left">Co-Lead:&nbsp; Kevin McGuire &lt;<a
 href="mailto:kevin_mcguire@ca.ibm.com?Subject=BPEL%20Designer%20Proposal">kevin_mcguire@ca.ibm.com</a>&gt;,
IBM</li>

  <li>
    <p align="left">Committer:&nbsp; Mahadevan Lakshminarayanan &lt;<a
 href="mailto:mahadevan.lakshminarayanan@oracle.com">mahadevan.lakshminarayanan@oracle.com</a><span
 style="text-decoration: underline;"></span>&gt;, Oracle Corporation </p>
  </li>

  <li>
    <p align="left">Committer: Glenn Mi &lt;<a
 href="mailto:glenn.mi@oracle.com">glenn.mi@oracle.com</a>&gt;, Oracle
Corporation</p>
  </li>

  <li>
    <p align="left">Committer:&nbsp; James Moody &lt;<a
 href="mailto:james_moody@ca.ibm.com?Subject=BPEL%20Designer%20Proposal">james_moody@ca.ibm.com</a>&gt;,
IBM</p>
  </li>

  <li>
    <p align="left">Committer:&nbsp; Rodrigo Peretti &lt;<a
 href="mailto:rodrigo_peretti@ca.ibm.com?Subject=BPEL%20Designer%20Proposal">rodrigo_peretti@ca.ibm.com</a>&gt;,
IBM </p>
  </li>

</ul>

<h2 align="left">Interested parties</h3>
<p align="left">The following parties have expressed interest in this
project. Key contacts listed.</p>

<ul>

  <li>
    <p align="left">Oracle Corporation - Edwin, Khodabakchian &lt;<a
 href="mailto:edwin.khodabakchian@oracle.com">edwin.khodabakchian@oracle.com</a>&gt;</p>
  </li>

  <li>
    <p align="left">IBM - Greg Adams &lt;<a
 href="mailto:greg_adams@ca.ibm.com">greg_adams@ca.ibm.com</a>&gt;</p>
  </li>

  <li>
     <p align="left">FiveSight - Paul Brown &lt;<a href="mailto:paulrbrown@gmail.com">paulrbrown@gmail.com</a>&gt;</p>
  </li>

  <li>
    <p align="left">Tibco - Tom Hillman &lt;<a
 href="mailto:hillman@tibco.com">hillman@tibco.com</a>&gt;</p>
  </li>

</ul>
The submitters of this proposal welcome interested parties to
post
to the <a
 href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.bpel-designer">eclipse.technology.bpel-designer</a>
newsgroup and ask to be added to the list as interested parties or to
suggest changes to this document.<br>
<br>
<h2 align="left">Code Contributions</h3>
<p align="left">None planned at this time.<br>
</p>
<h2>Tentative Plan</h2>
<p>Below are areas of targeted support for the project:</p>
<h3><b>BPEL Designer Specific functionality</b></h3>
<p>This section illustrates type type of functionality the BPEL visual
designer should have<br>
</p>
<ul>
  <li>The
tool must have the ability to construct BPEL
flows visually. The visual representation should have a predefined &#8220;UI
layout&#8221;.
Free form layout design maps tend to be&nbsp; cumbersome and hard to
navigate.&nbsp; The tree like structure of a
BPEL orchestration definition leads itself to predefined UI layout.</li>
  <li>A
palette of elementary building blocks must be
available for drag/drop functionality</li>
  <li>The
palette must be extensible by 3rd party vendors so that contributions
of useful building blocks can be made to the palette. <br>
  </li>
  <li>Skinnable look</li>
  <ul>
    <li>Palette items should be skinnable - this includes both visual
look
and textual description on the palette and process map and in other
views where such items would be visible.</li>
    <li>Process Map visuals - this includes iconic representation on
the process map as well as description format. <br>
    </li>
    <li>Properties drawer (inspector area) for customizing elements and
attributes<br>
    </li>
  </ul>
  <li>Each
node in the flow should be customizable by
a inspector specific to the semantics of the node. This is a fairly
standard
UI paradigm available in a number of development tools.</li>
  <li>The
inspectors must allow for pluggable
customizers that can better help a user with a customization process. </li>
  <li>Zoom
in and out support.</li>
  <li>Support
for Cut Copy Paste (simple text), but
also for Cut/Copy/Paste of whole segments of a BPEL process. Cut/Copy
for node
cloning with automatic rename.</li>
  <li>Drag and drop support for integration with resources such as
WSDLs , XML Schemas, etc.</li>
  <li>Undo
and Redo capabilities need to be present.</li>
  <li>Editor must have the capability to deal with "unknown" activities
(that is names which may be "custom" or vendor specific).</li>
  <li>Ability to view and work within a standard-only world and flag
parts of the process definition which are not portable.<br>
  </li>
  <li>Support
for editing several BPEL process
definitions at once. This can be done by allowing the user to open
several
process definitions each in their own editor instance and independent
of one
another.</li>
  <li>Support for editing large BPEL processes with complex maps -
breaking and managing visual complexity (whitespace, layers). <br>
  </li>
  <li>The
ability to customize Font and Colors. At the
beginning this can be very basic but the code should be written with
the
concept of pluggable resources and colors.</li>
  <li>Ability
to add notes to the process map. These
are help notes that document the process in and are not in any way part
of the
process semantics. Support for rich text (perhaps HTML) should be
considered,
since text notes may contain links etc. <br>
  </li>
  <li>Ability to "open" and "save" processes from a variety of places:</li>
  <ul>
    <li>already deployed processes
for further editing and evolution (via BPEL runtime binding).<br>
    </li>
    <li>from a .bpel file available in an Eclipse project container.</li>
    <li>from a a variety of source repositories (CVS)<br>
    </li>
    <li>WebDAV accessible file systems<br>
    </li>
  </ul>
  <li>Remotely installed web services need to be &#8220;explorable&#8221; from the
designer tool.&nbsp; The WSDL of the web service must be visually
presented to indicate what types of messages and data types are
supported for that service. A good starting point might be a WSIL
document which leads to a WSDL or other WSIL documents.. Just a way to
look at the "structure" of a web service; in/out arguments, type view
of messages, etc <br>
  </li>
  <li>Add Hoc interaction with Web Services - simple client interface
to web services - simple interactions with published services to "try
things out". <br>
  </li>
  <li>UDDI browse capability to discover existing services.</li>
  <li>There should be several views to look at the process:
    <ul>
      <li>Process map - the "diagram" view of the process<br>
      </li>
      <li>Tree structure that mimics the XML BPEL structure <br>
      </li>
      <li>Raw XML view of the process definition.</li>
      <li>Documentation view (generated from BPEL source and annotation
documentation)<br>
      </li>
      <li>Additional views which convey other types of information<br>
      </li>
    </ul>
  </li>
  <li>Support for refactoring support for BPEL process maps. For
example, pushing activities into a scope or sequence container and
placing the result on a palette.<br>
  </li>
  <li>Support for robust searching, bookmarking, and navigating the
process map and the process code.</li>
  <li><br>
    <br>
  </li>
</ul>
<h3><b>Automation/Generation Functionality</b></h3>
<p>Wizards will be provided to assist in prototyping and jump starting
of projects. This does not imply that a special project needs to be
opened for editing a BPEL document. However, a set of templates and
wizards will provide a starting point for a creating a BPEL file. <br>
</p>
<p>In addition, commonly used pieces of BPEL code will be added to the
palette so that they can be easily dragged onto the process map to help
with common "patterns" of&nbsp; BPEL flow (such as scope + assign +
invoke).&nbsp; <br>
</p>
<h3><b>Additional Functionality</b></h3>
<p>Developer productivity will be enhanced with the following features.</p>
<ul>
  <li>Inspection of XML schemas and visual XPath expression building.<br>
  </li>
  <li>Support for BPEL extension functions in the design time
environment (some of which can be vendor specific).<br>
  </li>
  <li>Support for on the fly validation if a runtime has been paired to
the design environment. <br>
  </li>
  <li>Support for schema definition through existing Eclipse frameworks</li>
</ul>
<h3><b>Extension Points</b></h3>
<p>The project should be architected as a set of plug-ins, each of
which provides the ability for other groups or vendors to further
extend and customize the functionality in a well-defined manner. Here
is a list of possible extension points that it may provide:<br>
</p>
<ul>
  <li>Runtime environment pairing <br>
  </li>
  <ul>
    <li>Validation&nbsp; - <br>
    </li>
    <li>Compilation - vendor specific execution map creation and
execution map creation</li>
    <li>Deployment - interaction with "administrative" API for
installing process at runtime</li>
    <li>Exploration -&nbsp; "read" and "save" (deploy) semantics of
deployed processes<br>
    </li>
  </ul>
  <ul>
    <li>Addition of&nbsp; functions callable from the BPEL process.<br>
    </li>
  </ul>
  <li>Providing plugin support for different query languages (XPath
(default), XQuery, JavaScript)<br>
  </li>
  <li>Extensions to BPEL palette definition</li>
  <li>BPEL element extensions and visual tool contributions</li>
  <li>Pluggable "type" system to allow 3rd parties to extend the
designer visually <br>
  </li>
  <ul>
    <li>Modifying existing element definitions such as adding
properties or modifying visuals<br>
    </li>
    <li>Adding new element definitions such as "custom" bpel activities.<br>
    </li>
  </ul>
  <li>A set of extension points around skinabilty</li>
</ul>
<h3><b>Integration Points with Other Eclipse Projects</b></h3>
<p>Given the scope of the project, the BPEL4WS designer will
interact with and leverage the following existing Eclipse
projects:&nbsp; </p>
<ul>
  <li>&nbsp;<a href="/webtools/wst/main.html">WST
(Web Standard
Tools)</a> &#8211; wsdl editor, schema editor, sse components
    <ul>
      <li>WSDL editing<br>
      </li>
      <li>Schema editing<br>
      </li>
      <li>SSE integration<br>
      </li>
      <li>UDDI browsing</li>
    </ul>
  </li>
  <li><a href="http://wwe.eclipse.org/gef/">GEF (Graphical Editing
Framework) (most likely)</a>
    <ul>
      <li>Graph drawing and manipulation<br>
      </li>
      <li>Palette</li>
      <li>Drag and Drop support</li>
    </ul>
  </li>
  <li><a href="/emf/">EMF (Eclipse Modeling
Framework) </a><br>
  </li>
  <ul>
    <li>Models for the editor</li>
    <li>Validation framework<br>
    </li>
  </ul>
  <li> <br>
  </li>
</ul>
<h3><b>High Level Use Cases</b></h3>
<ul>
  <li>Create new BPEL process<br>
  </li>
  <ul>
    <li>New Process Wizard to create new BPEL Process<br>
    </li>
    <li>Drag 'n' Drop activities from component palette to process map</li>
    <li>Drag 'n' Drop useful resources (like WSDLs or Schemas) from the
navigator.<br>
    </li>
    <li>Add partner links, variables</li>
    <li>Create correlation sets for long running interacting processes.<br>
    </li>
    <li>Customize properties of BPEL activities<br>
    </li>
    <li>Process is validated on the fly<br>
    </li>
    <li>Build and deploy to runtime container<br>
    </li>
  </ul>
  <li>Import Existing Project<br>
  </li>
  <ul>
    <li>Import Project into Workspace. Import can be from</li>
    <ul>
      <li>File system, file</li>
      <li>CVS or other source repository</li>
      <li>BPEL runtime (via runtime bindings)<br>
      </li>
    </ul>
    <li>Continue as before</li>
  </ul>
  <li>Browse and edit deployed processes</li>
  <ul>
    <li>Browse deployed processes <br>
    </li>
    <li>Import for editing</li>
    <li>Edit,Validate, Deploy<br>
    </li>
  </ul>
  <li>Import extension to Palette</li>
  <ul>
    <li>Add plugin extending the palette extension points<br>
    </li>
    <li>Added palette appears in the palette menu<br>
    </li>
  </ul>
  <li>Add extension for runtime support</li>
  <ul>
    <li>Add plugin extending runtime support extension points</li>
    <li>Runtime extension point paired</li>
  </ul>
  <li>Add extension for BPEL runtime functions</li>
  <ul>
    <li>Added plugin extending the runtime functions extension points</li>
    <li>Function wizard and expression builder aware of new functions</li>
  </ul>
  <li>Import new activities definitions</li>
  <ul>
    <li>New activities bundle is added to the designer type system</li>
    <li>Palette contributions are reflected</li>
    <li>Property sheets are defined</li>
  </ul>
</ul>
<h3>General</h3>
<ul>
  <li>Runtime support: Win32 and Linux minimally</li>
  <li>The initial release will be for the en locale; we will follow
localization guidelines to make translations and localization simple</li>
  <li>Accessibility issues will not be addressed initially other then
by skinability extension points which could for example add hight
contract or black and white or larger visuals.<br>
  </li>
</ul>
<br>
<h3><a name="runtime"></a>Runtime support</h3>
While the focus of this project will not include runtime components,
runtime interfaces will be provided so that BPEL runtime vendors can
adapt their engines to the design time experience offered by the
designer. This will typically involve steps such as validating,
compiling, and deploying a process. The mechanism for writing such
runtime bindings would typically involve defining a <span
 style="font-style: italic;">plugin </span>(<span
 style="font-style: italic;">extension</span>) which uses<span
 style="font-style: italic;"> extension points</span> of the BPEL
designer to interact with the engine's "administrative" API. <br>
<br>
A given BPEL engine's "administrative" API to perform such tasks may or
may not be public. Hence, there are two paths in which this can be
accomplished. <br>
<ol>
  <li>A given BPEL engine has public "administrative" API.<br>
In this case, any interested party can provide runtime bindings through
eclipse.org or independently. The source code to such plugin <span
 style="font-style: italic;">must be </span>(in case of eclipse.org)
or <span style="font-style: italic;">may not be</span> provided. <br>
  </li>
  <li>A given BPEL engine does not have public "administrative" API.<br>
Only the vendor may provide such runtime bindings and such work would
be most likely outside of eclipse.org. <br>
  </li>
</ol>
<br>
In order to validate the adapter framework/components we anticipate to
provide runtime support for the following BPEL runtime platforms<br>
<ul>
  <li><a
 href="http://www.oracle.com/technology/products/ias/bpel/index.html">Oracle
BPEL</a> Process Manager runtime</li>
  <li>Eclipse Test and Performance Tools Platform (TPTP) 4.0&nbsp; <a
 href="/tptp/platform/documents/design/choreography_html/index.html">Choreography
engine (based on BPEL)</a><br>
  </li>
</ul>
<br>
<br>
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
