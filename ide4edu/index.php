<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Eclipse IDE for Education Project Proposal";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1><?= $pageTitle ?></h1>
</p>
<?php
$project_header = $_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php";
if (file_exists($project_header)) {
	include_once($project_header);
	generate_header("IDE4EDU");
} else {
	$theme = "Phoenix";
}
?> <!-- Proposal content starts here.  -->

<h2>Introduction</h2>

<p>We propose to move the IDE4EDU component from the <a href="/soc">SOC</a>
project into this new project. The Eclipse IDE for Education is a
version of Eclipse streamlined specifically for use by university and
college students. The environment provides support for programming
languages that are commonly used in university courses, including Java,
Scheme, and Prolog.</p>

<p>Our initial efforts are focused on providing an environment with
reduced clutter that allows students to focus on their immediate
requirement of getting classroom work done. To that end, the current
implementation provides a streamlined Java development environment featuring stripped
down versions of wizards for creating common elements. We anticipate
that students will use this environment in their first months of
learning and then progress to a more complete Eclipse configuration.</p>

<p>Our next most immediate focus is to provide support for other
languages commonly used in post-secondary education such as Scheme and
Prolog (Scheme support is expected soon, pending a positive outcome from
our IP due diligence process).</p>

<p>Fundamentally, this project is concerned with providing an IDE specifically
tuned to the needs of students. In that regard, we view the scope of this
project to include such things as streamlining the installation and configuration
of the IDE, assignment workflow, collaboration, and more.</p>

<h2>Scope</h2>
<p>
The objectives of the IDE4EDU project are to:
<ul>
	<li>Develop and maintain a streamlined version of the Java development tools
	that reduces the knowledge and understanding required for students and
	other first-time users to get started.</li>

	<li>Reduce the overall burden of adoption of Eclipse technology
	for students.</li>

	<li>Develop and maintain first-class support for so-called
	academic and research languages such as Scheme and Prolog</li>

	<li>Provide support for other aspects of the overall student
	experience, including such things as assignment workflow management,
	and collaboration tools.</li>
</ul>
</p>

<p>You are invited to comment on and/or join the project. Please send
all feedback to the <a
	href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.ide4edu">eclipse.ide4edu</a>
newsgroup.</p>

<h2>Initial Contributions</h2>

<p>The IDE4EDU component from the SOC project will be moved into this
new project.</p>

<h2>Organization</h2>

<h3>Mentors</h3>
<ul>
	<li>Wayne Beaton (wayne@eclipse.org)</li>
	<li>Boris Bokowski (Boris_Bokowski@ca.ibm.com)</li>
</ul>

<h3>Initial committers</h3>

<p>The initial committers and contributors are:</p>

<ul>
	<li>Wayne Beaton (wayne@eclipse.org) : Project Lead, Committer</li>
	<li>Dwight Deugo (deugo@scs.carleton.ca) : Project Lead, Committer</li>
	<li>Ian Kennedy (ikennedy@connect.carleton.ca) : Committer</li>
</ul>

<h3>Code Contribution</h3>

<p>The IDE4EDU component from the SOC project will be moved into this
new project as a component. This code has already been vetted by the IP
due diligence process and has a mature automated build process.</p>

<h3>Interested parties</h3>

<ul>
	<li>Martin Oberhuber (martin.oberhuber@windriver.com)</li>
	<li>Scott Gray (scott@oreilly.com)</li>
	<li>Andrews Overholt (overholt@redhat.com)</li>
	<li>Charlie Kelly (Charlie@CharlieKelly.com)</li>
	<li>Roy Ganor (roy@zend.com), <a href="http://www.eclipse.org">PDT</a></li>
	<li>Vittorio Scarano (vitsca@dia.unisa.it), <a
		href="http://www.coffee-soft.org/">CoFFEE Project</a></li>
	<li>Ilaria Manno (manno@dia.unisa.it), <a
		href="http://www.coffee-soft.org/">CoFFEE Project</a></li>
	<li>Lee Myers (Lee.Myers@ewc.wy.edu), Computer Science Instructor,
	Easter Wyoming College</li>
	<li>Marlos Guimarães (marlos.jrg@gmail.com)</li>
	<li>Lou Nel (ldnel@scs.carleton.ca), Professor of Computer Science,
	Carleton University</li>
	<li>Jakub Jurkiewicz (jakub.jurkiewicz@pl.ibm.com)</li>
</ul>


<h3>User community</h3>

<p>The intended user community is composed almost entirely of secondary
and post-secondary students. However, the streamlined environment may
have appeal in some segments of industry or for self-directed education.</p>

<h3>Tentative Plan</h3>

<p>The first step is to complete the move of the existing code base and
build process into the new project.</p>

<p>One of the main reasons for wanting IDE4EDU to be a separate project
is to better enable it to schedule releases that can be included in
packages generated by the <a href="/epp">Eclipse Packaging Project</a>.
We therefore intend to schedule a release based on the existing code
base as soon as possible so that we can join with the Packaging Project.</p>

<p>The next step is to include some work that's being done on support
for the Scheme (see <a
	href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=242444">Bug 242444</a>)
and Prolog languages.</p>

<p>This project is intended to attract some amount of research and
exploratory work. We have been in contact with the <a
	href="http://www.coffee-soft.org/home.aspx">CoFFEE</a> project about
possible integration and synergistic work. We have also started
discussions with students about extending the environment to include
functionality for students to obtain assignments, and submit their work
in an automated fashion.</p>


<h3>Changes in this document</h3>
<p>2009-01-28: Modified the wording (but not the meaning) of the Introduction section</p>
<p>2009-01-27: Added Marlos Guimarães, Lou Nel, and Jakub Jurkiewicz as
interested parties</p>
<p>2009-01-23: Added Ian Kennedy as a project committer; added Vittorio
Scarano, Ilaria Manno, and Lee Myers as interested parties</p>
<p>2009-01-07: Added Roy Ganor as an interested party</p>
<p>2009-01-06: Added Charlie Kelly as an interested party</p>
<p>2009-01-05: Added Andrew Overholt as an interested party</p>
<!-- Proposal content ends here.  --></div>
</div>
<?php
$html = ob_get_contents();
ob_end_clean();

# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
