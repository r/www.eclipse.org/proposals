<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
   
 <?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Pollinate");
?>  
     
     <h1>Pollinate Project Proposal</h1><BR>
       

        <H2><B>
          Description</B></H2>
        <blockquote> 
          <P>Pollinate's goal is to build an eclipse-based IDE and toolset that 
            leverages the open source <a href="http://incubator.apache.org/beehive/">Apache 
            Beehive</a> application framework.</P>
          <p>Pollinate provides a full-featured Java development environment that 
            enables developers to visually build and assemble enterprise-scale 
            web applications, JSPs, web services, and leverage the Java controls 
            framework for creating and consuming J2EE components; optimized for 
            a service-oriented architecture. This will be accomplished by creating 
            a broad set of eclipse plugins and user interface components for building 
            projects on top of the Apache Beehive framework. Beehive is an Apache 
            open-source project that uses the innovations in JDK 1.5 to help simplify 
            programming on J2EE and other Web Application containers.</p>
          <P>Note. Pollinate is in proposal phase. Some of the initial design 
            documentation and proposed API specfications can be found <a href="http://www.instantiations.com/pollinate/">here</a> 
            (http://www.instantiations.com/pollinate/). When this project passes 
            the Creation Phase, the development team home page with be <a href="/pollinate/">here</a> 
            (/pollinate/). There is a <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.pollinate">newsgroup</a> 
            created for the discussion of the project. Please visit the newsgroup 
            and get involved. </P>
        </blockquote>
        <H2>People</SPAN></H2>
		<p>The project expects to have commiters from Instantiations (project 
          lead), Genuitec, Soaring Eagle LLC and others. We are looking for others 
          to participate in all aspects of this project. </p>
        <p>The initial set of committers would be. </p>
        <p>Dan Rubel (project lead)<br>
          Instantiations<br>
          dan_rubel@instantiations.com </p>
        <p>Carl McConnell<br>
          Instantiations<br>
          carl_mcconnell@instantiations.com </p>
        <p>Maher Masri<br>
          Genuitec<br>
          maher@genuitec.com </p>
        <p>Scott D. Ryan<br>
          Soaring Eagle LLC.<br>
          scott@theryansplace.com </p>
        <p>Lee Faus<br>
          Fruition Software Inc.<br>
          leefaus@nc.rr.com</p>
        <H2>Status</h2>  
		The project has been formally proposed and is in the Creation Review stage. 
        It will be placed in the Technology PMC as an incubator project to develop 
        these tools. <br>
        <h2>Platforms</SPAN></h2>  
		Pollinate plugins will be built in Java and should be portable to any 
        platform supported by Eclipse. 
        <h2>Development plan</SPAN></h2>
        <p>The plan for the Pollinate project is stll being developed. There is 
          a <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.pollinate">newsgroup</a> 
          created for the discussion of the project. Visit us there to discuss 
          technical issues, commit resources, and help us start this project.</p>
        <p> We plan to follow a milestone based delivery schedule with incremental 
          progress visible in each milestone. Here is a rough outline of the timeline 
          we expect to follow for milestones. </p>
        <p> M1 - Q3 2004<br>
          Deliver prototype with basic editors and builders necessary to produce 
          a <br>
          simple Beehive based web-app </p>
        <p>M2 - Q4 2004<br>
          Deliver a version with enough functionality for the community to produce 
          a <br>
          full scale Beehive based web-app</p>
        <p>M3 - Q1 2005<br>
          Deliver a version of Pollinate that is as close to version 1.0 feature 
          <br>
          complete as possible</p>
        <p>M4 / RC1 / GA - Q2 2005<br>
          Deliver a version that is feature complete (M4) followed closely by 
          release <br>
          candidates (RC1, RC2, ...) as necessary to fix last minute bugs and 
          add <br>
          polish needed reach 1.0 release (GA)</p>
        <p>&nbsp;</p>
  
   
    &nbsp; 
  
   

<P>&nbsp;</P>
<P>&nbsp;</P>
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
