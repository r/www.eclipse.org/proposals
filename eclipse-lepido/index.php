<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
	
	<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Lepido, Tools for Apache Cocoon");
?>
<h1>Lepido, Tools for Apache Cocoon</h1><br>
</p>
<h2>Introduction</h2>

<p>The Lepido project is a
proposed open
source project under the Eclipse Technology Project.</p>
<p>This proposal is in the Project
Proposal Phase (as defined in the <a
 href="/org/documents/Eclipse%20Development%20Pro03_11_09%20FINAL.pdf">Eclipse
Development Process</a> document)
and is written to declare its
intent and scope. This proposal is written to solicit additional
participation and input from the Eclipse community. You are invited
to comment on and/or join the project. Please send all feedback to
the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.lepido">eclipse.technology.lepido</a>
newsgroup.</p>

<h2>Background</h2>
<p><a href="http://cocoon.apache.org/">Apache
Cocoon</a> is a top-level project of
the Apache Software Foundation.
It is a web application framework built around the concepts of XML
processing pipelines and component-based application development. The
project was started in 1999 and has since then grown both in features
and user base, and is now widely used by a lot of people for
mission-critical web sites.</p>
<p>Apache Cocoon uses a lot of
XML-related technologies and XML-based configuration files. Although
very powerful and efficient once mastered, the framework has a steep
learning curve, and having dedicated tools to help newcomers getting
started and experienced users to be more productive would attract
much more people to this innovative product.</p>
<p>The purpose of this project is
therefore to build a set of opensource tools to ease the development
of web applications with Apache Cocoon. These tools will include
wizards to quickly lay out a project structure, dedicated editors for
the various XML files involved in application development, and
runtime tools such as debuggers or profilers. Lepido will provide a
usable feature set along with an extensible framework to build new
editors and features.</p>
<h2>Description</h2>
<p>Lepido will provide various
features
corresponding to all stages of the application development lifecycle.
As a Cocoon application runs in a J2EE/XML environment, a Lepido
project will build on top of the project natures brought by the <a
 href="/webtools/">Web
Tools Platform</a>. Plugins provided
by Lepido will also heavily rely
on WTP features.</p>
<h3>Architecture</h3>
<p>The following diagram outlines
the
global architecture of the Lepido project and its relation with other
Eclipse projects.</p>
<p align="center"><img style="width: 555px; height: 271px;"
 alt="Lepido architecture" title="Lepido architecture"
 src="lepido-architecture.gif"></p>
<h3>Features</h3>
<h4>Wizards</h4>
<p>Apache Cocoon can be used in a
lot of
different situations and comes with a lot of
&ldquo;blocks&rdquo;,
which are modules dedicated either to a particular library (e.g. FOP,
Batik) or a particular functional area (e.g. portal, databases).
Setting up a new project requires to select the appropriate blocks
from an Apache Cocoon distribution and add them to the newly created
project.</p>
<p>To this effect, there will be a
Cocoon project setup wizard, that will allow the user to choose the
blocks that are to be included in the project, also allowing also
further modification of the selected blocks during the project
lifetime.</p>
<p>Other wizards will also be
provided
to quickly set up typical project layouts for common Cocoon project
types such as website publication, web application or mutichannel
publication. These layouts will also promote best practices in Cocoon
applications architecture.</p>
<h4>Editors</h4>
<p>Apache Cocoon provides a lot of
powerful features that are used and configured using XML files. These
files can be classified in two main categories:</p>
<ul>
  <li>Mixed content
files, used
in dynamic page production language such as XSLT, <a
 href="http://cocoon.apache.org/2.1/userdocs/xsp/index.html">XSP</a>, <a
 href="http://cocoon.apache.org/2.1/userdocs/flow/jxtemplate.html">JXTemplate</a>,
and also embedded markup used by transformation components such as <a
 href="http://cocoon.apache.org/2.1/userdocs/transformers/i18n-transformer.html">i18n
directives</a>, <a
 href="http://cocoon.apache.org/2.1/userdocs/transformers/cinclude-transformer.html">include
statements</a> or <a
 href="http://cocoon.apache.org/2.1/userdocs/forms/templatetransformer.html">form
templates</a>. </li>
  <li>Configuration files, which
have a well-defined and regular stucture. This includes for example the
    <a
 href="http://cocoon.apache.org/2.1/userdocs/concepts/sitemap.html">sitemap</a>,
    <a href="http://cocoon.apache.org/2.1/userdocs/forms/">form</a>
definition and binding files, <a
 href="http://cocoon.apache.org/2.1/developing/portal/portal-block.html">portal</a>
definition and layout, etc. </li>
</ul>
<p>Mixed content editors will be
built
on top of WST <a
 href="/webtools/wst/components.html">components</a>
such as XML editor and SSE, providing adaptative syntax checking and
code completion.</p>
<p>Configuration files editors
will be
multi-page (<i>&agrave; la</i>
plugin.xml), providing the classical
source view and form-driven views allowing people to be guided in
their work and also to be more productive by concentrating on the
content without having to care about the underlying XML structure.
The source view will be built on top of WST components similarily to
mixed content editors. Form-driven views will be built using AXE, the
XUL-like system for JFace forms that has been developped by <a
 href="http://www.anyware-tech.com/">Anyware
Technologies</a>.</p>
<p>The use of AXE allows to very
quickly
develop new editors by providing a declarative approach to building
form-oriented editors without requiring an intimate knowledge of
JFace, and by associating form widgets to XPath expressions in the
XML document, thus making it easy to bidirectionally bind forms to
the target source file.</p>
<p>The AXE component has been
proposed
for inclusion as a new WTP component and according the the <a
 href="/webtools/development/pmc_call_notes/2005-03-29.html">latest
WTP PMC report</a> will be
eventually included in WTP 1.1. In the meantime AXE will reside in the
Lepido project before moving to the org.eclipse.wtp namespace.<br>
</p>
<h4>Debugger</h4>
<p>Developping applications with
Apache
Cocoon involves many technologies, most of which are processed on the
fly (XML transformation chains) or interpreted (e.g. sitemap, XSLT,
templates, forms). Even if the interpreted languages are precompiled
into some kind of evaluation trees for faster performance, none of
them except XSP directly translates to Java bytecode, thus preventing
the use of traditional debuggers to step through the application
execution.</p>
<p>Lepido will therefore provide a
debugger for these interpreted languages. As not all libaries used by
Apache Cocoon are hosted within the project and that few of them
provide the necessary hooks to plug in a debugger, the Lepido
debugger will use AOP techniques to instrument interpreters at run
time to provide debugging support.</p>
<p>The Lepido debugger will thus
be
architected into three main parts:</p>
<ul>
  <li>The debugger UI, that will
build on Eclipse's generic debugger infrastructure. </li>
  <li>A debugger runtime core,
managing the communication between the different intepreter debugging
drivers (see below) and the debugger UI </li>
  <li>Interpreter
debugger
drivers, implementing generic debugging contracts managed by the
debugger runtime core. When no debugger hook exist, these drivers will
be injected using AOP tools within the interpreter engine bytecode to
connect it to the debugger runtime. </li>
</ul>
<p>Lepido will provide drivers for
the
most frequently used interpreted languages in typical Cocoon
projects, e.g. the sitemap, XSLT, flowscript (a page flow controller
using the <a href="http://www.mozilla.org/rhino/">Rhino Javascript
engine</a>), JXTempate, form
processing. The pluggable architecture
of the debugger runtime will allow other drivers to be added later as
needed.</p>
<h4>Refactoring</h4>
<p>The separation of concerns,
which is
one of Cocoon's strengths, also comes with a price as many entities
are cross-referenced in several different files, thus making it
sometimes difficult to maintain consistency and refactor an
application. Such cross-referenced entities are for example sitemap
components, input modules, form widgets or even files (e.g. XSLT
files).</p>
<p>A Lepido project will have a
semantic
model of the application being built, tracking places where a single
entity is defined and referenced. This model will be used to build
features such as consistency checkers and refactoring tools. A common
refactoring is renaming and moving, other features being highly
dependent on the entity actually being considered.</p>
<h3>Monitoring and profiling</h3>
<p>Although there are no precise
plans
for it at this time, a possible direction for Lepido is the
integration with <a href="/tptp/">TPTP</a>
to provide features to monitor and profile Cocoon applications.</p>
<h3>Initial Contribution</h3>
<p>The initial contribution
consists in
existing plugins that have been developped by <a
 href="http://www.anyware-tech.com/">Anyware Technologies</a>:</p>
<ul>
  <li>A Cocoon
project setup
wizard that quickly sets up the project structure and files. </li>
  <li>A set of multi-page editors
for the Cocoon sitemap, logkit configuration and Cocoon Forms
definition files. These editors are built on top of AXE, a XUL-like
system which is part of this contribution. </li>
  <li>An XSP editor, based on SSE. </li>
</ul>
<p>Some preliminary developments
have
been made on the debugger, proving the feasability of the proposed
architecture. The AOP engine used is <a
 href="http://aspectwerkz.codehaus.org/">AspectWerkz</a>,
who recently announced its merging with <a
 href="/aspectj/">AspectJ</a>.</p>
<p>The
following picture shows the
form
definition editor in action (click for a larger picture):</p>
<p align="center"><a href="lepido-formeditor.png"><img
 title="Form definition editor (click for a larger picture)"
 style="border: 0px solid ; width: 642px; height: 429px;"
 alt="Form definition editor (click for a larger picture)"
 src="lepido-formeditor.png"></a></p>
<h3>Q&amp;A</h3>
<p
 style="margin-top: 0.42cm; margin-bottom: 0cm; page-break-before: auto; page-break-after: avoid;">
<b>Why is it called
&ldquo;Lepido&rdquo;?</b></p>
<p>Lepido stands for
&ldquo;lepidoptera&rdquo;,
which is the scientific name for butterflies. As you certainly know,
bufferflies come out of cocoons! So Lepido is there to help you
getting the best out of Apache Cocoon!</p>
<p
 style="margin-top: 0.42cm; margin-bottom: 0cm; page-break-before: auto; page-break-after: avoid;">
<b>What does it bring to Apache
Cocoon?</b></p>
<p>Apache Cocoon is a very
powerful but
complex framework, which can frighten some people away. Having a nice
and friendly IDE will make it more appealing for an increased number
of users, and also to managers that will find a complete open source
solution rather that only a powerful runtime component.</p>
<p
 style="margin-top: 0.42cm; margin-bottom: 0cm; page-break-before: auto; page-break-after: avoid;">
<b>What does it bring to the
Eclipse Foundation?</b></p>
<p>Lepido will heavily use WTP
components. Being hosted by Eclipse allows close cooperation between
the teams working on the two projects. Lepido also brings some
features that can be of interest to the Eclipse community at large,
such as the AXE editor infrastructure and the pluggable interpreter
debugger.</p>
<p>At
a
&ldquo;corporate&rdquo; level,
Lepido is also an opportunity to strengthen the relations between the
Eclipse Foundation and the Apache Software Foundation, respective
leaders in IDE and server commercial-friendly opensource software,
and leverage the best of both to provide complete solutions with
which people and companies can build applications and products.</p>
<h2>Organization</h2>
<p>We propose this project should
be
undertaken as an incubator within the Eclipse Technology Project. It
is intended to complement and build on top of WTP, and some parts of
Lepido may be found to be of such general interest that they are
considered for inclusion as components of WTP, similarily to the
current AXE proposal.</p>
<p>The current goals of WTP
don't
allow the inclusion of Lepido as a a WTP subproject, but this can
surely be considered if ever the WTP project extends its scope to a
wider range of web-related technologies.</p>
<h3>Proposed project lead and initial committers</h3>
<ul>
  <li><a href="http://apache.org/%7Esylvain/">Sylvain Wallez</a>
(project lead) &ndash;
Anyware Technologies, Apache Software
Foundation<br>
    <a href="mailto:sylvain@apache.org">sylvain@apache.org</a> </li>
  <li>David
Sciamma &ndash; Anyware
Technologies<br>
    <a href="mailto:david.sciamma@anyware-tech.com">david.sciamma@anyware-tech.com</a></li>
  <li>Thomas
Friol &ndash; Anyware
Technologies<br>
    <a href="mailto:thomas.friol@anyware-tech.com">thomas.friol@anyware-tech.com</a></li>
</ul>
<h3>Interested parties</h3>
<p>The architecture of Lepido,
mainly
the AXE-based editors and the pluggable debugger drivers, is designed
to allow to easily add support for new XML dialects and runtime
interpreters without requiring expert Eclipse skills.</p>
<p>Our hope is that current
members of
the Cocoon development team and advanced users will join the Lepido
project, turning the initial contribution into a real community
project that will enhance it and ensure its features will always be
up to date regarding the evolution of the project at Apache. In
addition, we believe that an IDE team close to the runtime team will
encourage new features of Apache Cocoon to be Lepido-friendly, and
therefore user-friendly.</p>
<p>At this time, the following
people
and companies have expressed their interest in actively participating
to this project:</p>
<ul>
  <li>Matthew Langham &ndash; <a href="http://www.s-und-n.de/">S&amp;N</a>
  </li>
  <li>Gianugo Rabellino
&ndash; <a href="http://www.pro-netics.com/">Pro-Netics</a> </li>
  <li>Antonio Gallardo &ndash; <a href="http://www.agssa.net/">AG
Software</a> </li>
  <li>Stefano Mazzocchi
&ndash; <a href="http://apache.org/foundation/board/">Apache
Software Foundation</a>, community
catalyst. </li>
  <li>Giacomo Pati &ndash; <a href="http://www.otego.com/">Otego</a></li>
  <li><a href="http://vafer.org/blog/tcurdt/">Torsten Curdt</a></li>
  <li>Stavros Kounis
&ndash; <a href="http://www.osmosis.gr/">Osmosis</a></li>
  <li><a href="http://www.jroller.com/page/rgardler/">Ross Gardler</a></li>
</ul>
During a <a
 href="http://marc.theaimsgroup.com/?t=111219933300001&amp;r=1&amp;w=2">poll</a>
on the Cocoon user and developper lists, a number of people also
expressed their
interest in seeing such a project come to life and offered to help as
far as they can by providing feature wishes, bug reports, etc.
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
