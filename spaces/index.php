<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>The Eclipse Spaces Project</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("The Eclipse Spaces Project");
?>

<h2>Introduction</h2>
The Eclipse Spaces Project is a proposed open source project under the Eclipse Technology Project.
This proposal is in the Project Proposal Phase 
and is written to declare its intent and scope. This proposal is written to 
solicit additional participation and input from the Eclipse community. You are 
invited to comment on and/or join the project. Please send all feedback to 
the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.spaces">eclipse.technology.spaces</a> newsgroup.

<h2>Context</h2>
<p>The primary objective of this project would be to help catalyze the 
growth of a new and vibrant segment within the Eclipse ecosystem 
by reducing the gap between "developing" with Eclipse and "sharing" with Eclipse.</p>

<p>The Eclipse technology foundation is of nearly universal interest to 
developers of software products.  However, the development process and 
infrastructure that Eclipse.org provides, although equally successful, 
is applicable only to a relatively small number of Eclipse-centric projects.</p>

<p>Excluded are projects lacking the scope, ambition or licensing model 
required of top-level projects (or subprojects).  Examples include projects 
creating applications or run-time-only (as opposed to "tooling"), open source 
projects involving code licensed under a non-approved license, commercial 
software products including closed-source code and projects for which 
Eclipse management overhead is unsupportable.</p>

<p>Infrastructure outside of Eclipse.org supports collaborative development 
of Eclipse-centric projects.  Open source projects, for example, can 
use SourceForge.  Web services providers (such as AOL, a co-submitter 
of this proposal) provide virtual infrastructure that is available to 
developers without IP constraints (subject to standard commercial terms, etc.).  
And projects can always provision and maintain their own infrastructure, 
where economically feasible.  </p>

<p>However, the cost and effort needed for an "excluded" Eclipse 
project�for example, an individual developer who wants to create 
a personal open source project�is quite high, while the quality of 
integration between the Eclipse environment and sharing infrastructure 
outside of Eclipse.org is unacceptably low.  What is missing is the 
equivalent of "my space" for Eclipse developers.</p>

<h2>Project Overview</h2>
<p>The Spaces project will provide an extensible framework and exemplary 
implementation for an Eclipse feature/plug-in set that 
streamlines the process of publishing, materializing and sharing a 
code base against a specified set of virtual services for source management, 
release staging and downloading, bug-tracking and community collaboration.  
The initial exemplary implementation will include an adapter to connect
the framework to an extended version of AOL's  virtual storage 
infrastructure (XDrive). Other exemplary implementations to capable and available 
virtual infrastructures will be included
based on community demand and project resources (the project welcomes
additional contributors to help with, e.g., an adapter to SourceForge).

<h2>Objectives</h2>
<p>The objectives of the project will be to:<ul>
<li>Make Eclipse the most capable and efficient "front end" for creating and 
sharing collaborative software projects outside of Eclipse.org. 
<li>Encourage a broader range of developers to center shared development initiatives around Eclipse.
<li>Encourage commercial infrastructure providers to create more and 
better open infrastructure services for the Eclipse developer community.
</ul>

<h2>Scope</h2>
<p>We have identified the following functional elements as being within scope of the proposed project:<ul>
<li>An extensible model defining the interaction between the Eclipse IDE and 
a set of open source development services: [SC]  source code repository (e.g., CVS/SVN), 
[DR] distribution repository (e.g., update site URL), [BT] bug/task repository 
(e.g., Bugzilla/JIRA) and [CC] community collaboration (e.g., newsgroup/forum/mailing list/chat/etc).<ul>
<li>Aspects of this model may already addressed by other Eclipse projects, in which case 
this project will reuse or wrap/simplify those existing models, to the greatest extent possible.
<li>A key goal of this extensible model will be to provide a simple minimal set 
of modeled services rather than to attempt to create a most general model.
<li>Another goal of will be to provide a small set of extension points for vendors 
and third-parties to contribute specialized adapters that translate model actions into third-party API calls.
</ul>
<li>One or more exemplary open source implementations of adapters. Specifically, the project 
proposes to implement an AOL  adapter that connects the extensible model to the 
APIs for SC, DR, BT, and CC services.
<li>An extensible Eclipse plug-in user interface for interacting with the model.  
The goal is a set of menu items, context menu items, buttons, and 
wizards that provide the easiest possible user experience for sharing, 
materializing, publishing, and collaborating around small Eclipse plug-in projects:<ul>
<li><b>Sharing</b>:  a simple path to starting a new EclipseSpaces project from a given plug-in within a workspace.
<li><b>Materializing</b>:  from a Spaces project into a workspace�for example by browsing 
to the Spaces page and choosing "materialize".
<li><b>Publishing</b>:  "one-click" creation of a standard Eclipse update 
site for a Spaces plug-in project in the workspace. (This may 
require additional project meta-data, such as a features.xml, but the 
user interface will step the user through the default creation of that file.)  
Publishing should not require the complexity of multiple "feature" and "site" projects.
<li><b>Collaborating</b>:  in the simplest case, a menu item that opens a web page 
for a collaboration medium (e.g., a forum).  More complex cases might 
involve menu items that start up the Mylar Bugzilla view, or Corona 
for a Spaces-mediated chat session.  The exact definition of collaboration will be 
left to the adapter based on the back-end's collaboration services. The 
key user interface feature is that the path to the collaboration will be obviously 
connected with the project itself.
</ul></ul>

<h2>Out of Scope/ Complementary Technologies</h2>
<p>The primary focus of the proposed project will be to address a set of unmet 
community/market requirements.  The proposed project will leverage other 
Eclipse technologies to add functional breadth and depth within 
its target scope.  And wherever possible, the project will attempt to 
extend and re-use complementary technologies developed by other 
Eclipse projects, rather than re-implement them.
<p>Eclipse technologies that have been tentatively identified as applicable include:<ul>
<li>Component metadata management and publishing and component materialization 
capabilities provided by the <a href="/buckminster/">Buckminster Project</a>
<li>Workspace collaboration capabilities provided by the <a href="/corona/">Corona Project</a>
<li>APIs for working with various bug repositories provided by the <a href="/mylar/">Mylar Project</a>
<li>Installer technology provided by the <a href="/epp/">Packaging Project</a>
</ul>
<p>We will seek input and participation from the project teams of these 
complementary technologies, and will proactively suggest and/or contribute enhancements and extensions.
</p>

<h2>Initial committers</h2>
<p>The initial committers will focus on specifying the set of 
target virtual infrastructure services and designing and implementing 
the Spaces plug-in/feature-set and initial exemplary back-end implementation.  
The initial committers are: <ul>
<li>Lucas MacGregor (AOL), Project Co-Lead
<li>Henrik Lindberg  (Cloudsmith), Project Co-Lead
<li>Dennis O�Flynn (Compuware)
<li>Filip Hrbek (Cloudsmith)
<li>Thomas Hallgren (Cloudsmith)
<li>Malcolm Sparks (Congreve)
<li>Bjorn Freeman-Benson (Eclipse Foundation)
<li>Ward Cunningham (Eclipse Foundation)
<li>TBD (AOL)
</ul>
<p>In general, our agile development process will follow Eclipse.org 
standards for openness and transparency. Moreover, we will 
pro-actively seek participation in the project, whether as committers, interested parties or otherwise, from 
individuals or companies that can help the project address the 
requirements from the broadest possible segment of the ecosystem 
or can extend the range of virtual infrastructure services available to EclipseSpaces users.
</p>

<h2>Interested parties</h2>
<p>The following parties have expressed interest extending the platform, 
contributing ideas, guidance and discussion. Key contacts listed:<ul>
<li>Ross David Turk, SourceForge.net
</ul>

<h2>User community</h2>
<p>The proposed project addresses the requirements of a highly disparate 
group of developers, as such, supporting and soliciting feedback from 
a large user community of developers is critical to creating the right 
offering. We plan on achieving this by using the standard Eclipse.org 
mechanisms of supporting an open project and community of early adopters.
</p>









</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
}