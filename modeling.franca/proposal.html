<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Franca</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>

<p>The Franca project is a proposed open source project under the <a
	href="http://projects.eclipse.org/projects/modeling">Modeling
Top-level Project</a>.</p>

<!-- 
	The communication channel must be specified. Typically, this is the
	"Proposals" forum. In general, you don't need to change this.
 -->
<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the 
<a href="http://www.eclipse.org/forums/eclipse.proposals">Eclipse Proposals</a>
Forum.</p>

<h2>Background</h2>

<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->

<p><em>Interfaces</em> are important artifacts in any kind of software architecture.
They represent the contract between components, subsystems or systems and also
between organizational units, like teams, departments, or companies.
Therefore, in many development environments interfaces are modeled as 
first-class entities using an <em>Interface Definition Language</em> (short: IDL).
However, for building large-scale platforms or systems, existing IDLs
have at least two shortcomings:</p>

<ol>
<li>For integration of software systems which are contributed from various sources,
it is necessary to map interfaces defined in one IDL to a software model using
another IDL. E.g., in the international
<a href="http://www.genivi.org">GENIVI consortium</a>
a platform for Automotive/Infotainment systems is being developed by integrating
software components provided by various open-source projects, companies and
institutions as GENIVI itself.
The interfaces of these building blocks are either not modeled at all
(e.g., plain C or C++ headers) or formally modeled in one of several methodologies,
among them UML, D-Bus Introspection XML, annotated Java APIs.
The integration of all contributions on model level can only be done by
establishing mappings between all of these IDLs.</li>

<li>Another common shortcoming in most existing IDL approaches is the lack of
formality in modeling the <em>dynamics</em> of interfaces. Most IDLs provide means
to model static aspects of the interface (e.g., data types, signals,
methods, properties) as opposed to dynamic aspects (e.g., allowed sequences of
events at the interface), which are most often regarded as optional add-on to the
static interface definition.</br>
However, many severe bugs in complex software systems are caused by
mismatches of the dynamic aspects of interfaces. These bugs will occur
especially in late project phases (during system integration or even after
customer delivery), are hard to identify and expensive to fix.
Thus, it is necessary that the dynamic aspects should be part of the original
interface definition, allowing extensive formal validation of the interface's
implementations and usage. This can be done by static analysis or at runtime.</li>
</ol>

<p>In order to address both issues, GENIVI started the 
<a href="http://code.google.com/a/eclipselabs.org/p/franca/">Franca project</a>,
which can serve as a hub for IDL transformations and allows the specification of
dynamic behavior as an integral part of each modeled interface.
Part of Franca is a technology- and platform-neutral IDL.
Franca has been initially contributed by
<a href="http://www.harman.com">Harman</a> and has been published
<a href="http://code.google.com/a/eclipselabs.org/p/franca/">on EclipseLabs</a>
in March 2012 (under Eclipse Public License 1.0).</p>


<h2>Scope</h2>

<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->
 
 <p>The Franca project provides a technology- and platform-neutral IDL including an easy-to-use editor based on the Eclipse IDE. The project also provides:</p>
 
 <ul>
 	<li>support for model-level system integration based on transformations from one IDL to another using Franca IDL as a central hub</li>
	<li>a means to specify for each interface which interactions are allowed (Franca contracts)</li>
	<li>tools supporting validation of implementations against Franca contracts</li>
	<li>implementations of downstream code (and documentation) generators</li>
	<li>for the addition of platform-specific deployment information to interface definitions</li>
	<li>tools for supporting interface designers, e.g. a rapid prototyping tool which automatically generates an executable model out of a single interface definition</li>
 </ul>


<h2>Description</h2>

<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->
 
<p>Franca is a framework for definition and transformation of software interfaces.
It is used for integrating software components from different suppliers, which are
built based on various runtime frameworks, platforms and IPC
(Inter-Process Communication) mechanisms.</p>

<img src="images/franca_features.png" width="400px" />

<p>The diagram above shows the five aspects of the Franca framework.</p>
<ul>
<li><p><b>IDL and feature-rich editor.</b>
The core of it is Franca IDL (Interface Definition Language), which is a textual
language for specification of APIs.</p>
</li>
<li><p><b>Transformations and generation.</b>
	Franca offers a framework for building transformations from/to other IDLs
	and model-based interface descriptions
	(e.g., D-Bus, UML, AUTOSAR, WebIDL) It also supports code generation with some
	open-source code generators already available.
	Support for the D-Bus Introspection format is available as installable
	feature of Franca.<br/>
	The diagram below shows a typical set-up of transformations (yellow) and
	generators (blue) built with Franca.</p>

	<img src="images/franca_typical_infotainment.png" width="100%" />

</li>
<li><p><b>Specification of dynamic behavior.</b>
	With Franca, the dynamic behavior of client/server interactions can be
	specified using protocol state machines. Tools are available to use these
	specifications for validating implementations, e.g., checking runtime
	traces against the expected order of events on the interface.<br/>
	A graphical viewer for protocol state machines is part of Franca.
	The following diagram shows an example screenshot.</p>

	<img src="images/robotarm_contract.png" width="300px" />
</li>
<li><p><b>Flexible deployment models.</b>
	In specific application domains, it may be necessary to extend
	interface specifications by platform- or target-specific information.
	This is supported by Franca's deployment models, which allow these kind
	of extensions in a type-safe way.</p>
</li>
<li><p><b>Rapid interface prototyping.</b>
	An executable test environment for an interface definition can be
	generated instantly, allowing users to see the interface in action.
	This is accomplished by generating eTrice models from Franca interfaces
	and generating executable Java code from these models.
	The generated test environment consists of a client component (acting as
	dynamic test case executor) and a server component (acting as an
	intelligent mock object).</p>
</li>
</ul>

Based on these features, there is a rich set of possible use-cases. 
Here are some examples:
<ul>
<li>Build your own component model based on EMF and use Franca for
    the IDL part.</li>
<li>Generate UML models from Franca interfaces and use those as part
    of a more detailed UML model of your system.</li>
<li>Generate standardized C++ bindings using GENIVI CommonAPI.</li>
<li>Convert Franca interfaces to D-Bus and use the tool-chain as already
    established on Linux desktops.</li>
<li>Use JavaScript-generator (part of Franca delivery) to generate
    JS-code which can be used to build HTML5-applications
    based on WebSockets.</li>
<li>Use Franca to create executable eTrice models and use them 
    as a starting point for eTrice projects.</li>
<li>Check the interaction of runtime components on-the-fly
    based on their interface's dynamic behavior specification.</li>
</ul>


<h2>Current applications of Franca</h2> 

Franca is an open-source project since March 2012. Thus, it is impossible to
provide a complete list of applications. However, we will list some examples
in order to show a part of the Franca community:

<ul>
	<li>Franca is being used as an IDL and integration tool by
	    the <a href="http://www.genivi.org">GENIVI consortium</a>.
	    </li>
	<li>The <a href="http://projects.genivi.org/commonapi/">CommonAPI C++</a>
	    project uses Franca models as a starting point for
	    all code generators.
	    </li>
	<li>The <a href="https://github.com/bmwcarit/joynr">joynr</a>
	    web-based communication framework uses Franca as an IDL
	    and as a basis for its Java and C++ code generators.
	    The open-source joynr framework supports the interaction of applications
	    which are deployed on consumer devices, vehicles, or backend infrastructure.
	    </li>
	<li>The <a href="http://projects.genivi.org/yamaica/">Yamaica</a>
	    project is an Eclipse IDE extension providing a convenient UI for
	    working with Franca models.
	    </li>
	<li>Several companies (including also non-Automotive) are using
	    or evaluating Franca as an IDL for their internal software platform.
	    </li>
</ul>


<h2>Why Eclipse?</h2> 

<!-- 
	Answer these two questions:	What value does this project bring to the Eclipse
	community? What value do you expect to obtain from hosting your project at Eclipse?
	
	What value do you get by having your project at Eclipse over and above the value
	of hosting at Eclipse Labs?
 -->

<h3>Benefits Franca brings for the Eclipse community</h3>

<p>Franca is already quite well-known and used across the Automotive/Infotainment domain.
Therefore, it will provide additional value to the toolset Eclipse has to offer
for the Automotive domain. It will especially attract companies to the Eclipse
ecosystem which develop Infotainment solutions, just because of Franca's impact
in that domain. This is valid not only for Eclipse as a community, but as well for
the Automotive IWG at Eclipse.
</p>

<p>Additionally, Franca might bring additional value to selected Eclipse projects
by enabling some new inter-project functionality. E.g., there is already a feature
to create eTrice models from Franca models. Also for projects tackling other
application domains, Franca could be a valuable addition: E.g., Franca could serve
as a bridge between Automotive applications and the growing M2M ecosystem, motivated by innovative Car2X use cases.
</p>

<h3>Benefits Franca gets by being hosted on Eclipse</h3>

<p>Franca is based on several Eclipse-based technologies,
among them
<a href="http://www.eclipse.org/modeling/emf">EMF</a>,
<a href="http://www.eclipse.org/Xtext">Xtext</a> and
<a href="http://www.eclipse.org/xtend">Xtend</a>.
Moreover, it optionally provides connections to other
Eclipse projects (e.g., eTrice).
Thus, by moving Franca into the Eclipse space, infrastructure
topics can be coupled more tightly. For example, Franca
releases can be aligned with the overall Eclipse release roadmap.
</p>

<p>Moreover, the threshold for initial contributions established
by the Eclipse foundation will help to increase the confidence 
companies will put into Franca. This includes the I.P. check,
committer requirements and also the required continuity for release
trains. This will improve the proliferation of the Franca concepts.
</p>

<p>Finally, the visibility of Franca will be increased by being part
of the Eclipse community. This will help attracting users, committers
and other projects to connect with the Franca network.
</p>



<h2>Relationship with other Eclipse Projects</h2>

<ul>
	<li>Franca's meta-models are based on
	    <a href="http://www.eclipse.org/modeling/emf">EMF</a>.</li>
	<li>Both the Franca IDL and the deployment DSL have been created using
	    <a href="http://www.eclipse.org/Xtext">Xtext</a>.</li>
	<li>For model-to-model transformations and code generation from Franca models,
	    <a href="http://www.eclipse.org/xtend">Xtend</a> is being used.</li>
	<li>Transformations are provided as part of Franca to generate executable models
	    for <a href="http://www.eclipse.org/etrice">eTrice</a>.</li>
</ul>


<h2>Initial Contribution</h2>

<!-- 
	Projects are expected to arrive at Eclipse with existing code.
	
	Describe the existing code that will be contributed to the project. Please provide
	a couple of paragraphs describing the code with modest detail, including important
	information like code ownership (who holds the copyright?), and some consideration
	of community that exists around the code. Include a listing of third-party libraries 
	and associated licenses.
 -->

<p>The initial contribution consists of sources and artifacts found in the current Franca
repository at
<a href="http://code.google.com/a/eclipselabs.org/p/franca/">Franca project</a>. Copyright for the source files is partly held by Harman International, partly by itemis AG.</p>
 
<p>Franca uses the following third-party dependencies (aside from the
Eclipse platform):</p>
<ul>
<li><a href="https://code.google.com/a/eclipselabs.org/p/dbus-emf-model/">dbus-emf-model</a>
project: hosted on EclipseLabs, EPL1, project maintainer is Klaus Birken</li>
<li><a href="http://git.eclipse.org/c/gef/org.eclipse.gef4.git/tree/org.eclipse.gef4.zest.feature">GEF4 Zest Visualization Toolkit</a></li>
<li><a href="https://github.com/kbirken/xDoc">xDoc clone</a>: used only for
creating the documentation</li>
</ul>


<h2>Legal Issues</h2>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->
 
 <p>All contributions will be distributed under the Eclipse Public License.
 The current code planned for initial distribution published at
 EclipseLabs
 (see <a href="http://code.google.com/a/eclipselabs.org/p/franca/">Franca project</a>)
 is distributed under the Eclipse Public License as well.</p>
 
 
<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

<p>The following individuals are proposed as initial committers to the project:</p>

<dl>
	<dt>Dr. Klaus Birken, itemis AG (project lead)</dt>
	<dd>Klaus is initiator, maintainer and lead of the current pre-Eclipse
	    Franca project. He has lead the initial contribution to EclipseLabs.
	    Klaus is working in the <a href="http://www.genivi.org">GENIVI consortium</a>
	    since 2011 and is leading its <em>Common IDL</em> topic, which provided
	    the requirements for the initial development of Franca.</dd>
	<dt>Tamas Szabo, itemis AG</dt>
	<dd>Tamas has provided significant contributions to the existing Franca
	    code base. Tamas is also a committer of the Eclipse project
	    <a href="http://www.eclipse.org/incquery/">EMF-IncQuery</a>.</dd>
	<dt>Steffen Weik, itemis AG</dt>
	<dd>Steffen has provided significant contributions to the existing Franca
	    code base, esp. regarding contract definition and
	    trace-/testcase generation.</dd>
</dl>

<p>We welcome additional committers and contributions.</p>


<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->


<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Ed Merks</li>
	<li>Eike Stepper</li>
</ul>


<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
	<li><a href="http://www.bmw.com/com/en/">BMW</a></li>
	<li><a href="http://www.harman.com/EN-US/Solutions/Car/Pages/INFOTAINMENT.aspx#.UmfXdJGdGiI">Harman</a></li>
	<li><a href = "http://genivi.org">GENIVI</a></li>
	<li><a href ="http://www.bosch.de/en/de/our_company_1/business_sectors_and_divisions_1/car_multimedia_1/car-multimedia.html">Bosch Car Multimedia GmbH</a></li>
	<li><a href = "http://www.protos.de/index.php/en">Protos Software GmbH</a></li>
	<li><a href="http://www.bmw-carit.com">BMW Car IT</a></li>
</ul>


<h2>Project Scheduling</h2>

<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->
 
<ul>
	<li>December 2013: code submission and IP review</li>
	<li>January 2014: first builds on the Eclipse infrastructure</li>
</ul>


<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>05-Nov-2013</td>
		<td>Document created</td>
	</tr>
</table>
</body>
</html>
