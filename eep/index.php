<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1>Eclipse Examples Project</h1>
</p>
<?php
$project_header = $_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php";
if (file_exists($project_header)) {
include_once($project_header);
generate_header("EEP");
} else {
$theme = "Phoenix";
}
?> <!-- Proposal content starts here.  -->

<h2>Introduction</h2>

<p>The Eclipse Examples Project (EEP) provides a location for adopters
of Eclipse technology to find well-documented example applications,
platforms, and features/plug-ins/bundles that demonstrate the use of
Eclipse technology. The idea behind the project is to provide a home for
examples of applications and plug-ins that use Eclipse technology. As a
general rule, examples utilize technologies from multiple Eclipse
projects, though this is not rigorously enforced to avoid excluding
examples that might otherwise be useful and interesting.</p>

<p>We believe that a collection of well-documented and compelling
examples demonstrating the use of Eclipse technology is of great benefit
to the projects and the adopter community. First, these examples provide
increased visibility for the projects. Further, they provide a vehicle
for demonstrating and testing interactions between projects that might
otherwise be difficult or impossible provide in the context of any one
project. Perhaps most important, these examples provide much-needed
documentation for adopters of Eclipse Technology.</p>

You are invited to comment on and/or join the project. Please send all feedback to the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.eep">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.eep</a> newsgroup.</p>

<h2>Values</h2>

<p>The following values guide the development of EEP components.</p>

<p><em>Examples use Eclipse runtime technology.</em> Eclipse <a
	href="/equinox">Equinox</a> is the delivery platform for all examples
except when non-Java technology is required, or it is otherwise
inappropriate. Eclipse technology is also used to to create the
examples. Where Eclipse tools are used in a novel or non-obvious manner
to create the example code, processes employed are documented.</p>

<p><em>Only public APIs are used in the examples.</em> Examples use the
most recent APIs when a choice is available.</p>

<p><em>The most recent releases of Eclipse projects are supported.</em>
As the Eclipse projects the examples are built upon are updated, so are
the examples. Examples support the most recent &quot;general
availability&quot;(GA) release (n) and (possibly) the previous GA
release (n-1) of the projects they use. Examples clearly
indicate&mdash;on both the website and, when possible, within the
example itself&mdash;the projects and releases they are built upon.
Examples that cannot be maintained, or support Eclipse project releases
older than n-1, are archived.</p>

<p><em>All examples are thoroughly documented.</em> More specifically,
all classes, interfaces, and non-trivial fields and
methods&mdash;irrespective of any visibility modifiers&mdash;have
comments that describe their utility. All comments are as descriptive as
they need to be, including sensible cross-referencing links where
appropriate. Comments are not long-winded or unnecessarily long.</p>

<p><em>Examples are as small as possible.</em> Unnecessary or unused
files, types, constructors, fields, methods, etc. are removed.</p>

<p><em>Examples are implemented in as simple a manner as possible</em>
(but no simpler). Implementations are not simplistic. They are
sophisticated enough to demonstrate how the technology is used.</p>

<p><em>Where possible, examples are extensible.</em> Eclipse Rich Client
Platform (<a href="/rcp">RCP</a>) applications are built as extensible
platforms. Examples that manifest as bundles are, where sensible,
extendable by other bundles.</p>

<p><em>Examples are test-infected.</em> That is, they are thoroughly
covered with JUnit tests (using the most recent version of JUnit). In
keeping spirit with the other values, test code is also well-documented,
and are implemented as small and simple as possible.</p>

<p><em>Examples are easily downloaded and buildable.</em> Examples that
require extra artifacts (like bundles from <a href="/orbit">Orbit</a>,
or custom RCP targets) include scripts and/or instructions for building
the custom environment. Where possible, Eclipse technology such as <a
	href="/buckminster">Buckminster</a> is used to define these scripts
(further increasing the utility of the example).</p>

<p><em>Examples are translatable into multiple languages where
appropriate and sensible.</em> All strings are externalized. Examples
leverage the most recent version ICU4J in providing this support.</p>

<p><em>Examples are IP reviewed.</em> All intellectual property (IP)
contributions to the project pass IP review within nine months of
submission. Contributions which cannot be reviewed within that time are
removed from the repository.</p>

<h2>Initial Contributions</h2>

<p>Each of the following applications is a separate component in the
project.</p>

<h3>Sudoku</h3>

<p>The Eclipse Sudoku is well known within the community. It is
implemented as an Eclipse RCP application and has already been enhanced
with multiple-user support by Eclipse Communication Framework (<a
	href="/ecf">ECF</a>) committers. Further, it has been adapted to run on
embedded devices using the Embedded RCP (<a href="/dsdp/ercp">eRCP</a>)
and there is interest in developing a version of it that will run on the
Rich Ajax Platform (<a href="/rap">RAP</a>).</p>

<p>Sudoku is implemented as an extensible platform that can be enhanced
with additional puzzle generators and solvers (which, incidentally, is
the reason that the example was originally created).</p>

<h3>Organizer</h3>

<p>The Eclipse Organizer provides task management and e-mail
functionality. It is implemented as an extensible platform that can be
extended to provide other types of functionality including such things
as contact and calendar management.</p>

<p>The current implementation is very much a proof of concept that will
change architecturally in the coming months. We envision that this
example will make use of technology provided by the <a
	href="/eclipselink">EclipseLink</a> project for data connectivity, and
the <a href="/higgins">Higgins project</a> for identity management. We
believe that there is also potential for the use of technology from the
ECF, <a href="/riena">Riena</a>, and <a href="/swordfish">Swordfish</a>
projects in this example.</p>

<p>We recognize that some of the functionality provided by this example,
specifically task management, is already provided by the <a
	href="/mylyn">Mylyn</a> project. It is not our intention to complete
with that project or any similar products. The task management in this
example functionality is very rudimentary and our plan for moving
forward with this example will include consideration for possible
incorporation of Mylyn technology.</p>

<h3>Image Preview View</h3>

<p>The Image Preview is a feature that includes a small number of
bundles that contribute a view that provides a preview of the image
associated with the current selection in the workbench window. The
example shows how views that are tightly integrated with the workbench
can be loosely coupled from much of the framework using the workbench
selection service, and the adapter framework. The implementation
currently provides a preview for selected files, but can be extended by
providing adapters for other types of objects.</p>

<h2>Organization</h2>

<h3>Mentors</h3>
<ul>
	<li>Wayne Beaton (wayne@eclipse.org)</li>
	<li>Thomas Hallgren (thomas@tada.se)</li>
</ul>

<h3>Initial committers</h3>

<p>The initial committers will focus on getting the aforementioned
examples into the code repository, filling in missing functionality and
ensuring that the project values have been applied. They will further
focus on identifying other examples that are appropriate for inclusion
in the project; we will activity encourage contributions both of code
and development resources. Our agile development process will follow
eclipse.org's standards for openness and transparency.</p>

<p>The initial committers and contributors are:</p>

<ul>
	<li>Wayne Beaton (wayne@eclipse.org) : Project Lead, Committer</li>
	<li>Scott Lewis (slewis@composent.com) : Committer</li>
	<li>Remy Suen (remy.suen@gmail.com) : Committer</li>
	<li>Heiko Seeberger (heiko.seeberger@metafinanz.de) : Committer</li>
	<li>Benjamin Muskalla (bmuskalla@innoopract.com) : Committer</li>
	<li>Ha Li (halizhangyu@sina.com) : Commmitter</li>
</ul>

<h3>Code Contribution</h3>

<p>The Eclipse Foundation offers an initial code contribution for all of
the aforementioned examples.</p>

<h3>Interested parties</h3>

<ul>

	<li>Tom Schindl (tom.schindl@bestsolution.at)</li>

	<li>Doug Clarke (douglas.clarke@oracle.com)</li>

	<li>Peter Krogh (peter.krogh@oracle.com)</li>

	<li>Channing Walton (channing.walton@casualmiracles.com)</li>

	<li>Chris Aniszczyk (zx@ibm.com)</li>

	<li>Thomas Hallgren (thomas@tada.se)</li>

</ul>


<h3>User community</h3>

<p>The intended user community is developers who are building
applications, products, tools, IDEs, plug-ins and more using Eclipse
technology.</p>

<h3>Tentative Plan</h3>

<p>The EEP will retrain incubator status for the foreseeable future. No
components of this project are intended to be ever become official
Eclipse API.</p>

<p>The Eclipse Examples Project will make use of the Eclipse CVS
repository to ensure that the barriers to entry are low. The first step
will be to establish the incubation status of the project and then move
the aforementioned initial contributions through the IP due diligence
process and into the source code repository and establish Eclipse
Bugzilla components for each of them.</p>

<p>Once the project is established, we will start the process of merging
the Sudoku currently present in the DSDP CVS repository into EEP. We
will then engage in revising the architecture of the Sudoku example with
the intent of generalizing it to make a better fit for a RAP-based
version.</p>

<p>The architecture of the Eclipse Organizer application will be
revisited with consideration for inclusion of Higgins to provide
identity management for the e-mail client and EclipseLink for local
persistence of artifacts.</p>

<p>Development of a detailed plan for this development is targeted to
coincide with the Ganymede 'M5' release, with availability of the
completed examples coinciding with the Ganymede 'GA' release. Subsequent
updates will be coordinated with the release schedules of prerequisite
projects. The EEP does not intend to formally join the Ganymede release
train.</p>

<p>The plan will include an automated build process that itself will
serve as an example (ideally building on existing automation provided by
Eclipse projects). The build process builds against the most recent
&quot;I-builds&quot; from prerequisite projects, runs tests, and informs
(via the developer mailing list) project committers of problems. Where
possible, examples that fail to meet with project values will be flagged
appropriately on the project website and marked for archival as part of
this automated process.</p>

<h3>Changes in this document</h3>
<p>2008-01-06: Added Thomas Hallgren as a mentor.</p>
<p>2008-01-05: Added Benjamin Muskalla as a committer.</p>

<!-- Proposal content ends here.  --></div>
</div>
<?php
$html = ob_get_contents();
ob_end_clean();

# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
