<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Project Babel";
$pageKeywords	= "Eclipse, i18n, l10n, internationalization, translation, localization, globalization";
$pageAuthor		= "Jess Garms (jgarms@bea.com)";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Babel</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Babel");
?>

<p>
<h2>Introduction</h2>
The Eclipse Babel Project is a proposed open source project under the Eclipse Technology Project.</p>

<p>
This proposal is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.babel newsgroup.
</p>

<h2>Background</h2>
<p>
Eclipse is a global community. It is in everyone�s interest to ensure that Eclipse is available and translated in as many locales as possible. We are proposing a set of tools to make the job of globalizing Eclipse projects easier. We also want to provide ways for people world wide, who are interested, to contribute translations in their language of choice.
</p>

<h2>Description</h2>
<p>
IBM and BEA are jointly proposing a globalization project at Eclipse.  The project will include tools and activities that are needed to adapt the Eclipse deliverables to properly run in multiple locales and to translate selected Eclipse projects into multiple different languages (French, Japanese, German, etc). The project could include tools to aid in the following areas: Enablement testing (E.g. can the software run in different language environments and handle multiple scripts, etc.), translatability testing (is it ready for translation?), preparation for translation (preparation of resource bundles), Translation testing (Is the translation acceptable?), etc.
</p>

<h2>Scope</h2>
<p>
Software globalization is more than just translating strings!
The following are the potential features for the Eclipse Babel Project:

<h3>Stream line the creation of National Language (NL) fragments</h3>
<ul>
	<li>JDT Editor currently has the function to globalize strings in Java programs. PDE also has the ability to create NL fragment project for any plug-in project. However, the two functions are not very well streamlined together. Eclipse Babel Project will better integrate the functions together to make it easier for programmers to create the NL fragment project for the plug-in project they are working on.</li>
</ul>

<h3>Translation File Preparation Tools</h3>
<ul>
	<li>Validate syntax of resource bundle files
	<ul>
		<li>to list a few common Java resource bundle files syntax problems: missing "=" in Java resource bundle files, missing continuation chars, hard-coded font in html files (hard-coded font may not be available in other locales)</li>
	</ul>
	</li>
</ul>
<ul>
	<li>Spell check resource bundle files
	<ul>
		<li>spell checked resource bundle files will improve software quality and reduce translation questions</li>
	</ul>
	</li>
</ul>
<ul>
	<li>Add translation instructions
	<ul>
		<li>need ability to add translation instructions. For example, instructions telling translators not to translate certain keywords</li>
	</ul>
	</li>
</ul>
<ul>
	<li>Mark non-translatable strings or files
	<ul>
		<li>need ability to mark certain strings or files to be non-translatable</li>
	</ul>
	</li>
</ul>
<ul>
	<li>Indicate single and double quote handling
	<ul>
		<li>depending on whether the strings would be processed by Java MessageFormat class, quotes may need to be duplicated in order to be displayed correctly</li>
	</ul>
	</li>
</ul>

<h3>Globalization Enablement Test Tools</h3>
<ul>
	<li>pseudo translation generation tool for testing layout, encoding, and unexternalized strings</li>
	<li>find the enablement problems that hinder translations</li>
</ul>

<h3>Translation Tools</h3>
<ul>
	<li>host the translation files on server</li>
	<li>allow contributors to translate strings in translation files</li>
	<li>allow contributors to search the translation repository for common translated terms</li>
	<li>protect keys, non-translatable strings and files</li>
	<li>committers review and release translation files back to repository</li>
	<li>build NL fragments</li>
</ul>

<h3>Translation Verification Tools</h3>
<ul>
	<li>display and check if the translation make sense in the context of the use case</li>
	<li>display and check if the translations are consistent from project to project</li>
</ul>


<h2>Organization</h2>
The work of the Eclipse Babel Project can be organized under 2 broad umbrellas:
<ol>
	<li>A community and process framework via which translations to existing components of the platform can be contributed and organized into the production of localized releases.  This infrastructure would allow various interested and capable parties to contribute translations for different parts of different components, and define a process by which those translations are verified, approved merged, and ultimately delivered in a localized release.</li>
	<li>Technology (tools and features both standalone and incorporated into other projects) which ensures that the ongoing development of the Eclipse Platform results in artifacts that are easily localized.</li>
</ol>

<h2>Project Mentors</h2>
<ul>
<li>Richard Gronback, Borland</li>
<li>John Graham, Sybase</li>
</ul>

<h2>Proposed Initial Committers</h2>
The following companies will contribute initial committers to get the project started:
<ul>
	<li>Chris Nguyen <a href="mailto:cnguyen@bea.com">(cnguyen@bea.com)</a> - CoLead</li>
	<li>Kit Lo <a href="mailto:kitlo@us.ibm.com">(kitlo@us.ibm.com</a>) - CoLead</li>
	<li>Paul Colton <a href="mailto:paul@aptana.com">(paul@aptana.com)</a></li>
	<li>Nigel Westbury <a href="mailto:nigel@miegel.org">(nigel@miegel.org)</a></li>
	<li>Jess Garms <a href="mailto:jgarms@bea.com">(jgarms@bea.com)</a></li>
	<li>Werner Keil <a href="mailto:wkeil@bea.com">(wkeil@bea.com)</a></li>
	<li>Daniel McGowan <a href="mailto:dmcgowan@novell.com">(dmcgowan@novell.com)</a></li>
</ul>
<p>We welcome additional committers to the project.</p>

<h2>Interested Parties</h2>
IBM and BEA, as the submitter of this proposal, welcomes interested parties to post to the newsgroup and ask to be added to the list as interested parties or to suggest changes to this document.
A number of companies have expressed interest in the project and/or its components thus far. This list will be updated periodically to reflect the growing interest in this project:
<ul>
	<li>Discovery Machine, Howard Lewis (<a href="mailto:hlewis@discoverymachine.com">hlewis@discoverymachine.com</a>)</li>
	<li>Sybase, John Graham (<a href="mailto:jograham@sybase.com">jograham@sybase.com</a>)</li>
</ul>

<h2>Code Contributions</h2>
Aptana will contribute localization tools to start the project.

<h2>Tentative Plan</h2>
The details of the development/release plan will depend upon the level of participation, as well as the composition of the contributors.
IBM is currently working with BEA on the target features for the Eclipse Babel Project. A development/release plan will be provided when it becomes available. Current plan is to deliver localized versions of the Europa platform release in several major languages in the future.
































</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
