<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>The m2eclipse Project Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("m2e");
?>

  <h2>Background and Overview</h2>
  <p> 
  <p>
The m2e project is a proposed open source project under the Eclipse Technology Project. This 
document describes the content and the scope of the proposed project. This proposal is in the Project Proposal 
Phase (as defined in the Eclipse Development Process document) and is written to declare 
its intent and scope. You are invited to comment on and/or join the project. Please send all feedback to 
the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.m2e">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.m2e</a> newsgroup.</p>
   <p>
   Maven is a widely used tool for build and release engineering which focuses on headless 
   operation to facilitate automation. Maven is language agnostic and has tool chains for 
   working with Java, .NET, and C/++. The Apache Maven project also has sub-projects which 
   deal specifically with binary artifact repositories (Maven Artifact), SCM abstraction 
   (Maven SCM), Testing abstraction (Maven Surefire), and rapid project prototyping 
   (Maven Archetype). Eclipse is the most widely used IDE, users demand parity between 
   the IDE and headless operations, and so it is natural that users are also demanding 
   seamless integration between Maven and Eclipse. The m2eclipse project's primary focus 
   will be creating this seamless integration between Maven and Eclipse, and we would 
   like to bring this effort to the Eclipse Foundation.
   </p>
       
  <p>
    The longest standing effort to unify the Maven and Eclipse worlds has been m2eclipse project at Codehaus. 
    The m2eclipse (m2e) project has rich integration with Maven features, a large community of users, 
    and collaboration with key Maven community members responsible for critical pieces of technology. Examples include the 
    Maven Embedder (Jason van Zyl), Archetype (Jason van Zyl, Raphael Pieroni), and Proximity (Tamas Cservenak). 
    The m2eclipse lead developer, Eugene Kuleshov, is also a committer on projects like Subclipse, and Mylyn which
    affords us the opportunity for great integration with these tools. So from the perspective of diversity, the 
    m2eclipe project is positioned to create solid integration between Maven and Eclipse.
  </p>
   
  <h2>Mission</h2>
  <p>
    <ul>
      <li>
        Extensibility: We recognize both the common need for Maven tooling infrastructure and the desire to extend the 
        tools in new and innovative ways. To support these efforts, our components will be designed for, and make 
        good use of, extensibility mechanisms supported by Eclipse.         
      </li>
      <li>
        Community Involvement: Success for m2e, as with other eclipse.org projects, is as much a factor of community 
        involvement as the technical merit of its components. We strongly believe that m2e will achieve its full potential 
        only as the result of deep and broad cooperation with the Eclipse community. Thus, we will make every 
        effort to accommodate collaboration, reach acceptable compromises, and provide a project management infrastructure 
        that includes all contributors, regardless of their affiliation, location, interests, or level of involvement. 
        Regular meetings covering all aspects of m2e, open communication channels, and equal access to process will 
        be key areas in driving successful community involvement.    
      </li>
      <li>
        Transparency: As with all projects under the eclipse.org umbrella, key information and discussions at every 
        level - such as requirements, design, implementation, and testing - will be easily accessible to the 
        Eclipse community.        
      </li>
    </ul>
  </p>
    
  <h2>Short Term Goals</h2>
  <p>
    <ul>
      <li>Execution of Maven inside Eclipse with an in-process, and out-of-process model</li>
      <li>Integration with Maven repositories</li>
      <li>Rapid project creation using Archetype and extensible project customization for other Eclipse tools</li>
      <li>Headless builds for OSGi-based projects using Maven</li>
      <li>Create services and extension points to allow 3rd parties to access all of Maven's capabilities</li>
      <li>Project materialization using Maven metadata</li>
      <li>Intuitive POM editing and refactoring</ii>
      <li>Dependency visualization and analysis</li>
      <li>PDE, WTP, and Mylyn Integration</li>
    </ul>
  <p>
  <h2>Long Term Goals</h2>
  <p>
    <ul>
        <li>Full bridge from Maven's runtime to Equinox's runtime (Plexus to Equinox bridge)</li>
        <li>Eclipse plugin deployment to Maven repositories</li>
        <li>Eclipse provisioning and updating from Maven repositories</li>
        <li>Natively building OSGi-based projects (look mom, no pom.xml)</li>
      </ul>
    </p>

    <h2>
      Description
    </h2>
    <p>
       The m2eclipse project is currently hosted at Codehaus which you can see <a href="http://m2eclipse.codehaus.org/">here</a>.
       We have also collected a great deal of information in our wiki which you can see
       <a href="http://docs.codehaus.org/display/M2ECLIPSE/Home">here</a>.
    </p>
      
  <h2>Collaboration</h2>

  <h3>Mylyn</h3>
  <p>
    The m2eclipse project currently has rudimentary integration which allows the <i>issueManagement</i> element of Maven's POM to help bootstrap a typical developer's
    Mylyn setup. We have many plans for richer integration and look forward to working with the Mylyn team.
  </p>

  <h3>P2</h3>
  <p>
    The m2eclipse project would like to work with the P2 team to provide integration between Maven repositories and P2. This would allow provisioning 
    runtimes and Eclipse environments from Maven repositories that have been populated with OSGi bundles and Eclipse plug-ins.
  </p>

  <h3>PDE</h3>
  <p>
    The m2eclipse project would like to work with the PDE team to create headless build support for OSGi bundles and Eclipse plug-ins from Maven. We have an initial
    first form on this system in the Tycho sub-project of m2eclipse that was created by Tom Huybrechts. This initial form requires a POM and some magic swizzling of
    Eclipse installations, but we would ultimately like to provide a way to directly build native PDE projects with Maven.
  </p>

  <h3>WTP</h3>
  <p>
    The m2eclipse project being driven by its users needs to provide WTP integration. It is one of the popular requests from users. Raghuraman Ramaswamy has 
    prototyped some parts of WTP integration that we plan to include with m2eclipse. We would really like to work with the WTP team to
    provide first-class integration between Maven and WTP.
  </p>
  
  <h2>
    Organization
  </h2>
  
  <h3>
  Mentors
  </h3>
  <ul>
  <li>Ed Merks (IBM)</li>
  <li>Chris Anisczcyk (IBM)</li>
  </ul>
  
  <h3>
    Initial committers
  </h3>
  <p>
    The initial committers are:
  </p>
  <ul>
    <li>Eugene Kuleshov (Sonatype)</li>
    <li>Wes Isberg (Sonatype)</li>
    <li>Dmitri Platinoff (Sonatype)</li>
    <li>Jason van Zyl (Sonatype, Maven Founder) 
    <li>Oleg Gusakov (EBay)</li>
    <li>Igor Fedorenko</li>
    <li>Herv&eacute; Boutemy (Maven Committer)</li>
    <li>Raghuraman Ramaswamy (Xebia, WTP Integration)</li>
    <li>James Ervin (Eclipse Committer, Groovy Monkey)</li>
    <li>Tom Huybrechts (AGFA)</li>
    <li>Vincent Siveton (Maven Committer)</li>
    <li>Luk&aacute;&#349; K&#345;e&#269;an</li>
    
  </ul>
  <h3>
    Interested parties
  </h3>
  <p>
    The following projects have expressed interest using the tools, contributing ideas, guidance and discussion. Key contacts listed.
  </p>
  <ul>
    <li>AppFuse (Matt Raible)</li>
    <li>Codegear (Ravi Kumar, Karl Ewald)</li>
    <li>E*Trade (Durai Arasan)</li>
    <li>EBay (Oleg Gusakov)</li>
    <li>Intuit (Gill Clark)</li>
    <li>IONA (Oisin Hurley, Bruce Snyder)</li>
    <li>LinkedIn (Arnold Goldberg)</li>  
    <li>MuleSource (Ross Mason, Dan Diephouse)</li>  
    <li>NexB (Philippe Ombr&eacute;danne)</li>
    <li>Qualcomm (Mike McSherry)</li>
    <li>Webtide (Greg Wilkins, Jan Bartel)</li>
    <li>XWiki (Ludovic Dubost, Vincent Massol)</li>
    <li>Polarion (Igor Vinnykov)</li>
    <li>AccuRev</li>
    
  </ul>
  <h3>
    Developer community
  </h3>
  <p>
    The vast majority of the work done on m2eclipse thus far has been sponsored by Sonatype, but we are rapidly gathering a
    following and foresee many of the interested parties getting involved in the development to benefit their organization, 
    or integrate with their commercial offerings. Five of our committers are actually non-Sonatypers so we are already achieving
    some diversity.
  </p>
  
  <h3>
    User community
  </h3>
  <p>
    We have the largest community of any Maven/Eclipse integration project, we get more patches then we can deal with, and we are 
    starting to get substantial contributions from the community. The WTP integration from Raghuraman Ramaswamy, the Maven
    service API developed by Oleg Gusakov and James Ervin, and the Subversive integration for project materialization
    created by Polarion are the first examples of this type of community involvement. We expect a lot more community 
    involvement in the near future.
  </p>

</div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
