<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "EMF Tiger - Component Proposal";
$pageKeywords	= "emf, emft, ecore, model transformations, graph transformations";
$pageAuthor		= "Christian Krause, Karsten Ehrig";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>EMF Tiger - Component Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Modeling Framework Tiger");
?>

<h2>Introduction</h2>

<p>
EMF Tiger is a proposed new open source project under 
<a href="http://www.eclipse.org/modeling/emft/">Eclipse Modeling Framework Technology (EMFT)</a>
to provide a model transformation based
on structured data models and graph transformation concepts.
</p>

<p>
This component is in the Pre-Proposal Phase (as defined in the 
<a href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse Development Process</a>)
and this document is written to declare its intent and scope. This
proposal is written to solicit additional participation and input from
the Eclipse community. You are invited to comment on and/or join in the
development of the component. Please send all feedback to the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">eclipse.technology.emft</a> 
newsgroup.
</p>


<h2>Scope</h2>

<p>
In this document, we propose a new model transformation framework for <a href="http://www.eclipse.org/emf">EMF</a>, called EMF Tiger. 
</p>

<h3>Model Transformation Languages</h3>

<p>
In model driven engineering, the notion of model transformation refers 
to techniques for defining and performing complex operations on structural 
data models. Two different types of model transformations can be distinguished:
<ol>
<li><b>Exogenous / source-target transformations:</b> 
       a model of a source language is translated into a model of a target language.
</li>
<li><b>Endogenous / in-place transformations:</b> 
       a single model is modified directly.
</li>
</ol>
Endogenous transformations are often considered as a special case of 
exogenous transformations where the source and the target metamodel 
are identical. From another perspective, one can also regard the source, 
target and an additional reference metamodel in an exogenous transformation 
as a joint metamodel of an endogenous transformation.
Both viewpoints are valid. To that end, the purpose of a model 
transformation often determines its type, e.g. language translations 
are usually exogenous while refactorings or reconfigurations are endogenous.
</p>

<p>
There are different ways of defining and executing model transformations 
and it is important to note that there is in fact a difference between 
the two types of transformations.
Just as with programming languges, there are also different kinds of model 
transformation languages which usually support both types of transformations 
but in fact are more suited for only one of them. One can basically 
distinguish between declarative (using pattern matching), procedural 
(using control-flow structures) and hybrid languages which combine 
both approaches.
Similar to ordinary programming languages (or e.g. compilers), 
verification of correctness and termination of model transformations 
is a crucial and at the same time non-trivial task. 
</p>

<h3>EMF Tiger</h3>

<p>
EMF Tiger is a tool environment for EMF model transformation.
Transformations are defined and executed 
directly on EMF models, ensuring type safety and efficiency.
Model transformation can be executed either using 
generated code or in an interpreter mode.
</p>

<p>
The rule-based transformation language of EMF Tiger is inspired by graph 
transformation concepts and 
combines both declarative and procedural concepts.
In particular, transformation rules are declarative in the sense 
that a structural pattern is used to define the precondition of 
a rule. Procedural concepts, i.e., control-flow strutures such as
layers and loops can be used to apply a set of transformation rules 
in a controlled manner.
</p>

<p>
Since most EMF transformations performed by 
EMF Tiger can be formalized by the theory of algebraic graph 
transformation (see e.g. <a href="http://www.springerlink.com/content/g215n78q004l310j">[BET08]</a> 
(<a href="http://tfs.cs.tu-berlin.de/publikationen/Papers08/BET08.pdf">PDF</a>)), 
these model transformations can be verified.
In currently ongoing research,
we investigate how existing techniques for
termination and confluence (correctness) 
checks can be adapted to EMF transformations.
As future work, we also plan to incorporate model checking 
techniques for a detailed analysis of model transformations.
</p>

<p>
The EMF Tiger tool environment currently consists of the following constituents:
<ul>
<li>a transformation model</li>
<li>a transformation engine (interpreter approach)</li>
<li>a code generator that produces self-contained transformation code in Java (based on EMF-generated code)</li>
<li>a tree-based editor for transformations rules (generated with EMF)</li>
<li>a graphical editor for transformations rules</li>
<li>export utilities for third-party graph transformation tools (for analysis)</li>
</ul>
</p>

<p>
The frontend is currently under reconstruction. We plan a set of 
GMF-generated editors for instance models and transformation rules. 
Furthermore, a visual debugger for model transformations is planned.
</p>

<p>
Our long-term goal is to make EMF Tiger a comprehensive tool environment for 
endogenous in-place EMF model transformations. It shall comprise 
convenient user-friendly editors for complete model transformation 
systems, including already existing EMF model editors, a visual 
debugger, code generator facilities, as well as validation and 
verification tools for EMF model transformation.
</p>

<h3>Language Concepts</h3>

<p>
EMF Tiger is a rule-based transformation language with both, 
declarative (pattern matching) and procedural aspects (control-flow structures).
A transformation rule consists of two model instances (a set of objects with 
references between them): a left-hand side (LHS) and a right-hand side (RHS). 
The LHS defines the pattern to be matched (precondition) and the RHS the 
in-place modification (postcondition) of the matched model instances. 
Optional negative application conditions (NACs) can be defined for restricting 
the rule application further.
<p>

</p>
Rules can be executed nondeterministally (from a pool) or using 
control-flow structures (e.g. layers and loops). Transformations can further 
include calculations on attributes. The calculations are defined as an 
expression on the attributes that may also involve (primitive-valued) variables.
</p>

<p>
The transformation language is defined through a metamodel. Currently, 
there exists a tree-based editor generated using EMF, and a graphical 
editor shown in the screenshot below.
</p>

<p><img src="proposal_files/editorSC.png"></p>

<p>
This front-end consists of a (read-only) viewer for the domain models and 
a three-pane editor for defining rules.
In this example a model for a simple refactoring for a statechart
variant is shown. This particular rule moves a state (3:State) out of a 
composite state (2:State) up into its parent (1:State).
This is possible, if all adjacent transitions already belong to the
surrounding state. This condition is checked by two similar NACs, one
of which is shown together with the rule.
</p>

<p>
EMF Tiger supports code generation from transformation specifications
as well as an interpreted runtime mode. 
</p>


<h3>Comparison with other Model Transformation Languages</h3>

<p>The most important model transformation approaches for EMF are:</p>
<ul>
<li> <a href="http://www.eclipse.org/m2m/atl/">ATL (ATLAS Transformation Language)</a> </li>
<li> <a href="http://www.eclipse.org/m2m/">Procedural QVT (Operational)</a> </li>
<li> <a href="http://www.eclipse.org/m2m/">Declarative QVT (Core and Relational)</a> </li>
<li> <a href="http://www.eclipse.org/gmt/oaw/">Xpand and Xtend (openArchitectureWare)</a> </li>
<li> <a href="http://www.eclipse.org/gmt/epsilon/">Epsilon</a> </li>
</ul>

<p>
Comparing the EMF Tiger transformation language to these languages, the first obvious commonality is
the rule-based definition of transformations and the use of declarative and procedural concepts. 
At second glance, the approach of EMF Tiger for defining and executing both source-target and 
in-place transformations is quite different
to the other languages. 
</p>

<p>
Particularly in-place transformations are not very well supported by  
the existing languages. For instance, ATL is defined as a language that
<i>'provides ways to produce a set of target models from a set of source models'</i>.
Although it is basically possible to define in-place transformations in ATL, 
this way is not that natural because in-place transformations have to be 
regarded as exogenous transformations where the source and the target 
metamodels are the same. 
This has major consequences on the execution of in-place transformations, i.e.,
instead of performing the transformations directly on a given a model (as it is 
natural for e.g. refactorings or reconfigurations), 
large parts of the source model have to be copied in ATL.
Especially for large models and small changes this is highly
inefficient. It is similar with QVT, since in-place transformations are also defined as 
a special type of source-target transformations.
</p>

<p>
EMF Tiger supports both in-place and source-target transformation.
In-place transformations in particular can be very elegantly described 
and executed efficiently, since the transformations are applied directly 
to an existing instance model.

In contrast to in-place transformations, source-to-target translations require 
an additional reference model that keeps track of newly created elements. Note 
that the transformation engine itself is oblivious to the type of transformation 
performed, i.e., internally source-target and in-place transformations are 
treated in the same way. 
</p>

<p>
Another key difference of EMF Tiger is its formalization by 
graph transformation theory (see <a href="http://www.springer.com/3-540-31187-4">[EEPT06]</a>
or this <a href="http://www.cs.le.ac.uk/people/rh122/papers/2006/Hec06Nutshell.pdf">tutorial</a>),
which enables formal analysis of transformations. Moreover, EMF Tiger
supports the visual development of transformations through its
native graphical notation.
</p>

<h2>Relationship to other Eclipse Projects</h2>

<p>
Apart from the core EMF tools, EMF Tiger uses 
<a href="http://www.eclipse.org/emft/projects/jet">JET</a> for code generation and
<a href="http://www.eclipse.org/gef">GEF</a> for the graphical editor.
We currently adapt the graphical editor to use the 
<a href="http://www.eclipse.org/gmf">GMF</a> notation view model 
and runtime. As a longterm goal, we are planning to partially generate 
the editor using GMF's code generator.
</p>

<p>
As a first case study we are working on a number of refactoring 
rules for EMF itself (defined on its metamodel) and UML-2.
</p>


<h2>Organization</h2>

<h3>Mentors</h3>

<ul>
	<li>Ed Merks (<a href="http://www.macromodeling.com">Macro Modeling</a>, Canada)</li>
	<li>Bernd Kolb (<a href="http://www.sap.com">SAP</a>, Germany)</li>
</ul>

<h3>Initial Committers</h3>

The initial committers for this project are:
<ul>
<li> Christian Krause (<a href="http://www.cwi.nl/">CWI Amsterdam</a>), proposed project lead</li>
<li> Enrico Biermann (<a href="http://tfs.cs.tu-berlin.de/">Technische Universit�t Berlin</a>), committer</li>
<li> Stefan Jurack (<a href="http://www.uni-marburg.de/fb12/swt/">Philipps-Universit�t Marburg</a>), committer</li>
</ul>

<h3>Code Contributions</h3>

The <a href="http://tfs.cs.tu-berlin.de/emftrans/">EMF Tiger Developer Group</a> 
at the TU-Berlin will provide an initial code contribution including the code 
generator and the graphical user interface.

<h3>Interested Parties</h3>
<p>The following parties have expressed their interest in EMF Tiger:</p>
<ul>
<li><a href="http://www.atxtechnologies.co.uk/">ATX Technologies (Carlos Matos)</a> </li>
<li><a href="http://www.eclipse.org/gmf/">Eclipse GMF</a> </li>
<li><a href="http://www.hipes.nl/">HiPeS (Jim van Dam)</a> </li>
<li><a href="http://www.zurich.ibm.com/">IBM Zurich Research Laboratory (Jochen K�ster)</a> </li>
<li><a href="http://www.ct.siemens.com/">Siemens Corporate Technology (Nikolaus Regnat)</a> </li>
</ul>


<h3>Developer Community</h3>
The team of initial committers will explore statements of interest from
additional developers experienced with EMF model transformations or
willing to gain such experience.

<h3>User Community</h3>
It is expected that the user community for this
component will consist of two kinds of users.
On the one hand, there will be researchers and developers who design EMF transformations
and on the other, there will be developers who integrate the generated EMF model
transformation code in their own applications.

<h2>Tentative Plan</h2>
The first community technical preview release is
scheduled for Winter 2009 and the first release is targeted for Summer 2010.

<h2>Further Information</h2>
Further information can be found on the 
<a href="http://tfs.cs.tu-berlin.de/emftrans">EMF Tiger Website</a> 
at the TU-Berlin.

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
