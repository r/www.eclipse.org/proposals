<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">


<h1>Open Healthcare Framework</h1><br>

</p>

<h2>Introduction</h2>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Open Healthcare Framework");
?>
<p>The Eclipse Open Healthcare Framework Project is a proposed open source
project under the <a href="/technology/">Eclipse 
Technology Project</a>.</p>
<p>This proposal is in the Project Proposal Phase (as defined in the&nbsp; <a href="/projects/dev_process/">Eclipse Development Process document</a>)
and is written to declare its intent and scope. This proposal is written to
solicit additional participation and input from the Eclipse community. You are
invited to comment on and/or join the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ohf">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ohf</a>
newsgroup.</p>

<h2>Overview</h2>
The goal of the Open Healthcare Framework (OHF) is to extend the Eclipse
Platform to create an open-source framework for building interoperable,
extensible healthcare systems. We also intend to develop a complementary set of
exemplary tools. OHF will enable software providers and integrators to
cost-effectively create customized offerings for healthcare delivery
organizations that comply with government regulations and industry standards.
<p>The Open Healthcare Framework will develop infrastructure, tools, components,
and services that will support development of both client and server
applications. Client development will be based on RCP and server components will
be based on J2EE together with core Eclipse concepts such as plug-in based
extensibility.</p>
<p>The essential functional elements of OHF V 1.0 are well understood by domain
experts. While there is significant engineering effort required, we feel that
the work plan as proposed here can be implemented given sufficient resources,
and does not involve an unrealistic level of technical risk. Consequently we
propose this project be considered as an incubator within the Technology PMC.</p>
<p>However, OHF does represent a significant element of <i>business innovation</i>.
We are aware of no other efforts to build a <b>platform</b> for constructing EHR
applications. Consequently we expect it will take both time and effort to grow
an OHF community within the healthcare industry. We are optimistic that there
will be considerable growth in the team in the first 6-12 months after
successfully passing the Creation Review and being established as an Eclipse
project. Our development plans are deliberately open-ended so that we can employ
these additional resources effectively as they become available. If we are wrong
in our optimism, our development plan can still succeed, but clearly will take
longer to reach its goals.</p>

<h2>Background</h2>
<p>Healthcare in the United States has been characterized as a $1.7 trillion
cottage industry. It is highly fragmented, labor intensive, barely connected,
extremely competitive, with many different vendors and solutions that are all
proprietary. Against this backdrop, the U.S. Department of Health and Human
Services issued regulations to require that healthcare organizations establish
administrative, physical and technical infrastructure to safeguard sensitive
health information as well as to implement standards to improve efficiency in
healthcare delivery. The US government estimates dramatic cost savings at more
than $250 billion annually if the healthcare industries can be automated.</p>
<p>Other countries around the world suffer from the same problems. The United
Kingdom has committed to several large regional programs such as the NHS
National Programme for Information Technology in England, with a focus on
building core infrastructure. Finland is also pursuing a nationally coordinated
program focusing on infrastructure development for health. These programs, along
with many others, include basic IT and telecommunications infrastructure,
standards, and shared software development.</p>
<p>Healthcare systems throughout the world face the same challenges and
opportunities: the amount and complexity of detailed healthcare information is
overwhelming. The ability to gather the information has outstripped the capacity
to understand and process it. In spite of years of standardization work and
implementation, many commercial and proprietary solutions and systems lack even
useful levels of interoperability. Industry standards and specifications are
broad, complex and in some cases lack implementations. Privacy, security and
accuracy are issues of critical concern.</p>
<p>An Electronic Health Record (EHR) is a collection of documents containing a
patient's health information that are created in compliance with standard
specifications that define document structure, data types,
terminology/vocabulary, semantics and essential processing requirements.
Existing EHR systems and healthcare applications typically grew organically and
often lack a clear architecture. Next generation systems are being designed as
distributed, loosely-coupled systems that comply with emerging standards for EHR
document structure and EHR system functional requirements as defined by <a href="http://www.hl7.org/ehr">HL7</a>, <a href="http://www.astm.org/cgi-bin/SoftCart.exe/COMMIT/COMMITTEE/E31.htm?L+mystore+isym5396+1112774053">ASTM</a>,
<a href="http://www.iso.org/">ISO</a>, <a href="http://www.cenorm.be/">CEN</a> and
other standards bodies. However, while they are better structured than their
predecessors, they are still monolithic, vertically integrated applications that
share little or no code with their sister applications, and indeed offer little
or no interoperability.</p>
<p>We believe there is an opportunity to provide a common, vendor-neutral
framework that includes generic components, integration mechanisms, supporting
tools and workflows, together with a process for staged customization to meet
national standards, allow differentiation of commercial products, and meet the
requirements of healthcare delivery organizations.</p>

<h2>Vision and Scope</h2>
<p>As part of our development effort, we will identify a set of functions that
are useful for EHR and general health application development. We will classify
these functions into one of three categories:</p>
<ul>
  <li>healthcare specific functions.</li>
  <li>cross-industry functions</li>
  <li>cross-industry healthcare-led functions (i.e. there is applicability
    outside the healthcare domain, but specific expertise and requirements in
    the healthcare domain)</li>
</ul>
<p>For those functions in the cross-industry category, we will attempt to reuse
components being developed within other open source efforts, with first
preference for Eclipse projects. For healthcare specific functions, we will
differentiate between components that will be part of the OHF platform and
developed by the open source project, third party code which we will reuse, and
components that will be developed by commercial vendors and integrators. This
differentiation will be based on criteria such as the existence of relevant
accepted standards or existing implementations by potential contributing
members, degree of general use and acceptance in the industry, and the nature of
the business and standards environment relevant to the function</p>
<p>We will develop infrastructure, tools, components, and services to implement
these functions and enable development of healthcare applications using the OHF
framework. Development will follow an agile approach. We will continue to refine
our scope, architecture and development plans on an ongoing basis.</p>
<p>The OHF project will offer a multi-tier customization strategy, in which the
generic OHF elements are customized to meet national standards; vendor specific
differentiation; regional and local variations in delivery; etc. Even if we are
successful, legacy systems will be with us for some time to come. We will
facilitate the development of adaptors and connectors so that legacy systems can
share data with OHF based systems and we will architect OHF so that OHF based
subsystems can be used within reengineered legacy systems</p>

<h2>Functional Architecture</h2>

			<p align="left">The figure below describes the functional
            architectural model underlying the development plan for OHF.
<p><img border="0" src="picture1.png" width="652" height="449"></p>
<p><b>OHF Framework Extensions:</b> Similar to other projects that build on the
RCP and the Eclipse Platform, we will implement extensions and additions to the
RCP. UI frameworks tailored to our user community and security extensions to the
OSGI runtime are examples. In addition, we see a requirement for a server
plug-in framework but have not decided on an implementation.</p>
<p><b>Tools: </b>A number of custom editors and other tools will be developed to
support OHF applications. OHF is willing to collaborate with other organizations
wishing to use Eclipse extensions for their own tooling.</p>
<p><b>Identity Management: </b>Applications must keep track of users, providers,
resources and patients. Since legislation now typically mandates that patients
must have access to at least a subset of their medical records, patients and
providers acquire both active (&quot;user&quot;) and passive
(&quot;resource&quot;) roles at different times.</p>
<p><b>User / Context Management:</b> The authentication of the user and cleanly
maintaining the user's session throughout the application is the foundation of
good security. The user's session is closely associated with the user's context,
which refers to state that is maintained when users interact with healthcare
applications at the point-of-use (e.g., a clinical desktop). OHF will define a
context framework and interoperate with other applications using HL7's Clinical
Context Management Specification (CCOW). Context management additionally
includes user-centric session management required to facilitate user mobility,
where session state is persisted and accessible as the user relocates.</p>
<p><b>Security and Privacy:</b> The usual security concerns are present as well
as some that are specific to health care, usually again driven by legislation.
Support for privacy in OHF goes beyond the standard application of security
methodologies to protect confidential healthcare data, it must be pervasive,
e.g. procedural support for privacy and integrity (including audit facilities)
should be part of the message and document processing chains.</p>
<p>The OHF project hopes to actively collaborate with the Higgins Trust
Framework Project in the Identity Management, User and Context Management, and
Security and Privacy areas.</p>
<p><b>Health Records:</b> The core function of most healthcare systems is to
create, store, search, retrieve and present records of health care events. In
recent years healthcare standards have increasingly focused on this area, and
have enabled a common implementation of these important function points.</p>
<p><b>Interoperability:</b> HL7 has released an updated version of the HL7
Standard, which is the primary general healthcare information standard. Both HL7
V2 - the currently implemented version - and the newly released V3 will be in
use for some time, so we intend to support both in our first release. DICOM
(Digital Imaging and Communications in Medicine) specifies the standards
relevant to medical imaging. As the project proceeds we expect to take account
of other relevant standards, such as HL7's CDA and ASTM's CCR document
standards.</p>
<p>A terminology service is a key component of any Healthcare system.
Essentially, it is a semantic interoperability support service. There are many
different terminologies in use in health care, both small and large (e.g. the
core terminology of the SNOMED database, operated by the College of American
Pathologists, contains over 357,000 health care concepts with unique meanings).
The HL7 Common Terminology Services (CTS) defines an API for accessing
terminological content. The primary initial focus of the OHF project will be to
implement this infrastructure; we are hoping to work with vendors and other
organizations with either expertise, IP, or both, to provide the content.</p>
<p>Archetypes are static models of clinical knowledge that can be used to
describe the health records; they have recently gained acceptance in the
healthcare community as the &quot;best practice&quot; technology. OHF will work
with CEN and other archetype implementers to integrate archetypes with health
records services. Other forms of meta-data representation such as schema, and
OWL (W3C Web Ontology Language) will also be supported.</p>

<h2>Development Plan</h2>

<p><b>Phase 1: Planning&nbsp;<br>
</b>The first step (currently on-going) is to complete the function
identification and classification. The process of refining the development plans
for Phase 2 and Phase 3 is also well underway. In addition, we are seeking early
adopters for Phase 2 and Phase 3 products.</p>
<p><b>Phase 2: Interoperability&nbsp;<br>
</b>We first plan to develop around two main function points for
Interoperability: the HL7 Common Terminology Services, and HL7 based messaging
and communications. Framework extensions and tools will be developed as required
to support these activities. Interoperability has consistently been identified
by vendors and users as potential &quot;low hanging fruit&quot;, hence is our
initial priority area.</p>
<p><b>Phase 3: Basic OHF&nbsp;<br>
</b>The following functions are candidates for inclusion in Phase 3:</p>
<ul>
  <li>Basic Health Records support (Record Location, Storage and Access)</li>
  <li>Basic Identity Management (Person/Provider identification)</li>
  <li>Security Framework - context, login support, trust frameworks and Role
    Based Access Control</li>
  <li>Basic Context Management</li>
  <li>Components for DICOM, CCR, CDA</li>
</ul>
<p>Refinements of existing development phases and a roadmap for additional
development phases may be expected as the project proceeds.</p>
<h3>Proposed Milestones&nbsp;</h3>
<p>We expect that it will take a project of this complexity several months to
get off the ground. Phase 1 is targeted to be complete by the end of the 3rd
quarter of 2005, Phase 2 to be complete by mid-2006 and Phase 3 by end 2007.
With additional resources we could pull these dates in.&nbsp;</p>
<h3>Not Currently in Plan&nbsp;</h3>
<p>Although the OHF is being designed to assist developers who are building
healthcare applications, we will not be developing a complete client or server
application. Our focus is components, tools, and frameworks. We are hoping to
recruit early adopters who will develop applications based on OHF.</p>
<p>A number of potential components are currently not in plan due to resource
restrictions, lack of sufficiently mature standards or uncertainties regarding
commercial interest. These include the following:</p>
<ul>
  <li>Workflow management and scheduling</li>
  <li>Process Automation</li>
  <li>Legacy systems and migration support</li>
  <li>Healthcare financials</li>
</ul>
<p>As the project proceeds we intend to have a clearly articulated roadmap that
outlines areas that we are committed to grow the OHF into; areas that we might
include; and areas that are outside the scope of the project. We have had some
initial discussions along these lines but do not have consensus or a complete
picture to offer at this time.</p>
<h2>Liaison with Industry Standards Groups</h2>

<p>The OHF solutions will rely on many standards, but most particularly those
provided by HL7, OMG, NEMA (DICOM), ASTM (CCR), IHE, CEN and ISO. Wherever
possible, the OHF solutions will provide comprehensive compliant support for the
relevant standards. Of particular interest is the joint OMG-HL7 initiative to
promote interoperable Healthcare IT systems, especially the Healthcare Services
Specification Project.</p>
<p>A number of the individuals and organizations expected to participate in the
project are already members of these standards activities and, in some cases,
are significant contributors to existing and emerging healthcare standards. OHF
intends to collaborate with the standards development organizations, either
formally or through OHF participants who maintain joint involvement.</p>

<h2>Organization</h2>

		<h2 align="left">Proposed project leads</h3>
<p>Mr. Grahame Grieve&nbsp;<br>
Jiva eHealth Solutions&nbsp;<br>
Telephone: +61 3 9450 2230&nbsp;<br>
Facsimile: +61 3 9450 2299&nbsp;<br>
E-Mail: grahameg at jivaehealth.com&nbsp;<br>
Web: www.jivaehealth.com</p>
<p>Mr. Don Jorgenson&nbsp;<br>
Inpriva, Inc.&nbsp;<br>
Telephone: +1 970-472-1441&nbsp;<br>
Facsimile: +1 970-472-5658&nbsp;<br>
E-Mail: djorgenson at inpriva.com&nbsp;</p>
<h3>Initial Committers&nbsp;</h3>
<p>Dmitry Bakuntsev, ITG&nbsp;<br>
Igor Yasno, ITG&nbsp;<br>
Kim Clohessy, IBM&nbsp;<br>
Marc Erickson, Inpriva&nbsp;<br>
Don Jorgenson, Inpriva&nbsp;<br>
Grahame Grieve, Jiva eHealth Solutions&nbsp;<br>
Paul Trevithick, SocialPhysics&nbsp;<br>
Joel West, San Jose State University</p>
		<h2 align="left">Interested parties</h3>
<p align="left">The following organizations have expressed interest in joining
the project:</p>
<ul>
  <li>
    <p align="left">Mayo Clinic</li>
  <li>
    <p align="left">Department of Defense (Health Affairs)</li>
  <li>
    <p align="left">Veterans Health Administration</li>
  <li>
    <p align="left">SerAPI (Finland)</li>
  <li>
    <p align="left">InterMountain Health</li>
  <li>
    <p align="left">Jiva eHealth Solutions</li>
</ul>
<p align="left"><i>Updated June 17, 2005</i></p>
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

