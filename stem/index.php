<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "STEM";
$pageKeywords	= "STEM";
$pageAuthor		= "Daniel Ford";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>STEM, The Spatiotemporal Epidemiological Modeler</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("STEM");
?>




<p> This document is a proposal to create a new Eclipse Project under
the <a href="/technology/">Eclipse Technology Project</a> called the
Spatiotemporal Epidemiological Modeler or <a
href="http://www.eclipse.org/ohf/components/stem/">STEM</a>. STEM is
currently a component of the <a
href="http://www.eclipse.org/ohf/">Open Healthcare Framework (OHF)</a>
project, which is also under the
Eclipse
Technology Project.</p> 

<p>This proposal is presented in accordance with the <a
href="/projects/dev_process/proposal-phase.php">Eclipse Development
Process</a> and is written to declare the project's intent and scope
as well as to solicit additional participation and feedback from the
Eclipse community. You are invited to join the project and to provide
feedback using the <a
href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ohf">
http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ohf
</a>newsgroup.</p>

<h2>Background</h2>

<p> In the past year (2008), the OHF has seen all of its components,
other than STEM, migrate either to other parts of Eclipse 
(e.g., <ahref="http://www.eclipse.org/ohf/components/soda/"> SODA</a>) or to
the external <a href="http://www.openhealthtools.org/"> Open
Healthcare Tools (OHT )</a> organization. This has left STEM as the
only active component in the OHF. . The consensus of the four STEM
committers is that they are very happy with Eclipse and how it is
managed and wish the project to remain with the Foundation. However,
given that the rest of the OHF project has moved, it makes sense to
move STEM out of the OHF and place it directly under the <a
href="/technology/">Eclipse Technology Project</a>. The STEM project
members are looking forward to being a project separate from the OHF
to better establish their "brand" and facilitate communications with
users, contributors and adopters. One of the challenges of being a
component of the OHF is that STEM was mixed in with many different,
completely unrelated, projects; this included sharing a project web
page, newsgroup, developer mailing list and project metadata. This
sharing tended to dilute STEM's effectiveness in communicating with
and developing its community. For instance, the OHF project metadata
is out-of-date and has been for some time; the STEM project has been
unable to correct that issue.
</p>


<p> STEM began life as a platform for the collaborative development of
mathematical models that characterize the spread of infectious
diseases in both time and space. STEM was originally developed by <a
href="http://www.research.ibm.com/">IBM Research</a> at its <a
href="http://www.almaden.ibm.com/">Almaden Research Center</a> to be
part of <a
href="http://www-03.ibm.com/press/us/en/pressrelease/19640.wss"> IBM's
Global Pandemic Initiative (GPI)</a>, a working group of global
healthcare "players" (WHO, UN, etc) that IBM formed to help plan for,
and combat, the threat of global pandemic influenza. As part of its
contribution to the GPI, IBM <a
href="http://www-03.ibm.com/industries/government/us/detail/news/G708487G68482D55.html">
donated</a> the source code for STEM to the Eclipse foundation in May
2007. </p>

<p> STEM enables epidemiologists and other researchers to develop
disease models quickly and collaboratively. STEM includes the basic
data sets that define the political geography, demographic data and
transportation infrastructure for the entire planet, saving the need
for modelers to collect this data on their own. It also includes
configurable "text book" disease models they can use immediately and
extensive editors and wizards that ease model creation. STEM includes
built-in views to visualize the geographic spread of diseases as well
as an interface to <a href="http://earth.google.com/"> Google
Earth</a>. Each disease model is composed of a set of interchangeable
components that supply different aspects of the model, these include
data sets, as well as mathematics. These components can be created by
different researchers and easily shared, thereby fostering cooperation
and collaboration. As a diease modeling system, STEM has an active and
growing community. </p>

<h2>Technical Scope </h2>

<p> At its core, STEM is a framework for composing arbitrary graphs
(nodes, edges, labels) from different "parts" and then managing
computations that use the graph as both a source of data and as place
to record state information. One of the main innovations provided by
STEM is that it allows the graph used during a simulation to be
composed from different parts that represent different aspects of the
eventual simulation. For instance, sets of labeled nodes that
represent geographic locations can be combined with sets of labels
that provide population data for those edges for a particular time
period (e.g., 1918). Similarly, different sets of edges can be added
to the graph to incorporate different kinds of relationships, such as
transportation infrastructure or simple physical relationships such as
sharing a common border. Computation is added to the mix through a
similar well defined interface to the graph. These different parts
can be aggregated and saved for reuse in multiple different models.
They can also be exported and distributed to other users. It is this
aggregation and reuse that promotes collaboration as different
components can be created by different parties and easily shared.
</p>

<p> Having such a general framework enables a variety of other kinds
of applications, not all of which are simulations. It is possible,
for instance, to run STEM in "real-time" where it uses "wall-clock"
time when manipulating the state of the underlying graph and have it
access external data sources as part of that process. Integrating
real-time weather information or other real-time environmental data
into a model in STEM is an example. This ability allows STEM to be
applied to decision support applications that require the integration
of "situational awareness" and analytics; examples would be disaster
planning and response, securities trading and risk management and
logistical planning. The integration of external data sources through
SOA and RSS feeds is a future step being considered for the project.
</p>

<p> The disease modeling framework, built upon the core, has well
established functionality, but is deliberately designed to be
extensible and has an unlimited capacity to absorb new mathematics and
other aspects of disease models. For the project, however, it aims to
provide a refined, but limited, set of built-in "text book" disease
model mathematics as well as another set of advanced experimental
models that result from project member's own research. </p>

<p> The incorporation of real-time data sources into STEM is an area
for future development. The scope and breadth of which is uncertain
and likely dependent on the particular application domains used as
examples. </p>


<p> There are two aspects to STEM, the core for developing simulation
frameworks, and actual simulation frameworks.  The mandates for
developing both of these aspects tend to define and govern their
growth.  The core for simulation frameworks is somewhat organically
constrained as features are only added to it to support the needs of
actual simulation frameworks such as disease modeling.  The base
comprises some nine EMF Ecore models that are used to generate a
significant portion of the core code; no new models are anticipated at
this time.  The remainder of the work on the core is to polish and
refine aspects of the core exposed to users such as model editors,
wizards and other parts of the GUI.  </p>


<p> STEM is more than a disease modeling system, however, the same
attributes that make a good collaborative system for disease modeling
are the same ones that facilitate other kinds of model development.
To this end, STEM was designed and implemented from its very inception
to be a more versatile platform and framework, with disease modeling
being a very complete example "application."  </p>

<h2>Organization</h2>

<h3>Mentors</h3>
<ul>
<li>Ed Merks</li>
<li>Chris Aniszczyk</li>
</ul>


<h3>Committers</h3>
STEM currently has four existing and active Eclipse Committers. 

<ul> 

<li> <p> Daniel Ford (daford att almaden.ibm.com) </p> <p> Daniel
was the initial Eclipse Committer for STEM. His contributions to the
system include the initial concept of a composable graph framework and
the general architecture and organization of STEM.  He also designed
the UML models that underpin STEM's implementation and is responsible
for their implementation using the Eclipse Modeling Framework (EMF).
Daniel wrote the initial versions of most of the components that
constituted the original STEM contribution, and continues to maintain
a significant number of them today.  Daniel created the initial CQ for
STEM's source code and a second one for STEM's data sets.  He worked
closing with Barb Cochrane on the Eclipse IP process to quickly
"clear" the original STEM source code contribution. He also worked on
the initial part of the (much) longer IP processing of the STEM data
sets (later passing that responsibility to James Kaufman).  Daniel
received his Ph.D. in Computer Science from the University of Waterloo
and is now a Research Staff Member (RSM) at the IBM Almaden Research
Center in San Jose, CA. </p> </li>

<li><p> James Kaufman (kaufman att almaden.ibm.com) </p><p> James
founded the STEM project with Daniel Ford and was the project's second
Eclipse committer.  James' has a wide range of contributions to the
OHF and the STEM project under Eclipse. He initiated the formation of
the OHF and as the IBM manager of a number of internal IBM Research
Healthcare related projects, pursued the legal and organizational
challenges that lead to their donation to Eclipse to form the initial
OHF code base.  Later, James followed the same path with STEM and
moved it from an internal IBM Research project to an open source
project under Eclipse.  James is also an active and critical
contributor to the STEM code base with primary responsibility in the
development and implementation of mathematical models for the
characterization of disease propagation and the development and
implementation of mathematical tools and for epidemiological data
analysis in STEM.  James also worked closely with the IBM and Eclipse
legal teams to "clear" the STEM contribution to Eclipse. James
received his Ph.D. in Physics from UCSB and is a Manager and Research
Staff Member at the IBM Almaden Research Center in San Jose, CA.</p>
</li>

<li><p> Stefan Edlund (sedlund att us.ibm.com)</p> <p> Stefan Edlund
is an Eclipse Committer has contributed to STEM since August
2008. Stefan has been working on the logging component in STEM,
dramatically improving its performance. Stefan has also been
contributing to the analytics perspective as well as the mathematics
for STEM disease models. Stefan Edlund is a Senior Software Engineer
at the IBM Almaden Research Center in San Jose, CA.</p> </li>

<li><p> Yossi Mesika (mesika att il.ibm.com) </p><p> Yossi is an
Eclipse Committer who contributed several new features and
improvements to the STEM project. Yossi worked on the graphical
rendering of the geographical maps within STEM and added some useful
features like presenting graph edges and the use of color
providers. Yossi also used performance testing tools for finding
memory leaks and improving the overall performance of STEM. Yossi is
also the release engineer of STEM and responsible for the automatic
process of generating weekly builds and publishing those in the Web
site. Yossi had also made major contributions to the Eclipse Open
Health Framework (OHF). Yossi is a Research Staff Member at the IBM
Haifa Research Labs in Israel.</p> </li>

</ul>


<h3>Collaborations</h3>

<p> The STEM project is working on the development of its community.
The project is doing well in developing a core set of <b>Committers</b> and
<b>Users</b> (listed below).  The project is working hard on
developing relationships with government, industry and academia.  In
academia, the project is nurturing graduate students in both
Epidemiology and Operations Research to create a natural class of
<b>Adopters</b> to join the project.  </p>

<ul>

<li> <p> <a href="http://www.hq.af.mil/"> USAF</a>: STEM currently has
multi-year funding from the United States Air Force for the general
development of the framework as well as for research into specialized
analytics for "reverse engineering" disease model configurations from
incident data. </p></li>

<li><p> <a href="http://www.uvm.edu/">University of Vermont</a>: STEM
has a successful ongoing collaboration with researchers Dr. Charles
Hulse (Charles.Hulse at uvm.edu) and Joanna Conant (Joanna.Conant at
uvm.edu) at the University of Vermont.  This collaboration has
resulted in one paper with more likely to come in the future.  </p>

<p> Kaufman, J., Connance, J., Ford, D.A., Kirhata, W., Jones, B.A.,
Douglas, J.V., "Assessing the Accuracy of Spatiotemporal
Epidemiological Models,"  BioSecure 2008, Raleigh, North Carolina,
Dec. 2, 2008.</p> </li>

<li> <p> <a href="www.jhu.edu">Johns Hopkins</a>: The STEM project
also collaborates with the <a href="http://www.jhsph.edu/">Johns
Hopkins Bloomberg School of Public Health</a>, where we work with
Epidemiology Ph.D., Graduate Student Justin Lesler on the validation
of the mathematics of our disease models. (Full Disclosure: IBM pays
Justin Lessler for this work). </p></li>

<li><p><a href="www.mit.edu>">MIT</a>: STEM has an ongoing
collaboration with Professor Dick Larson, in the <a
href="http://www.mit.edu/~orc/">Operations Research Department</a> at
MIT for the development of advanced disease models.</p></li>
     
<li><p><a href="http://www.mecids.org/index.php"> Middle East
Consortium for Infectious Disease Surveillance</a> (MECIDS): An
organization supported by the <a
href="http://www.nti.org/index.php">Nuclear Threat Initiative</a>
(NTI) and <a href="http://www.sfcg.org/">Search for Common Ground</a>,
which includes the public health departments of <a
href="http://www.health.gov.il/english/"> Israeli<a>, <a
href="http://www.moh.gov.jo/MOH/En/home.php">Jordan</a>, and <a
href="http://www.moh.gov.ps/">Palestinian</a>.  As well as <a
href="http://www.tau.ac.il/index-eng.html"> Tel Aviv University</a>
and <a href="http://www.alquds.edu/index.php"> Al Quds University
</a>.

</p></li>

<li><p> <a href="http://www.publichealth.pitt.edu/"> University of
Pittsburgh Graduate School of Public Health</a> and <a
href="http://www.medschool.pitt.edu/"> School of Medicine</a>,
Prof. Burce Lee</p></li>

</ul>

<h2>Tentative Plan </h2><p> 

2009-06 V0.4: Base release for Disease modeling
2009-12 V0.5: Non-disease modeling example application
2010-06 V0.6: Integrated situational awareness and analytics





      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
