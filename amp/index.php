<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Agent Modeling Platform (AMP)</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("AMP");
?>
<h2>Introduction</h2>
<p>The Agent Modeling Platform (AMP) is a proposed open source project under the Eclipse
Modeling project. This proposal is in the Project Proposal Phase (as defined in the Eclipse
Development Process document) and is written to declare its intent and scope and to solicit
additional input from the Eclipse community. You are invited to comment on and/or join the project.
Please send all feedback to the <a
	href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.amp">eclipse.amp</a> newsgroup.</p>

<h2>Scope</h2>
AMP provides extensible frameworks and exemplary tools for representing, editing, generating,
executing and visualizing agent models.
<p>Agent models share characteristics with object models, but are:
<ul>
	<li>Spatial: Models have explicit environment(s) in which agents interact. (An environment
	need not be a physical landscape; other examples of spatial relationships include social networks
	or positions within a logic system.)</li>
	<li>Temporal: Models change over discrete units of time.</li>
	<li>Autonomous: Agent behaviors are activated independently from other object requests.</li>
	<li>Heterogenous: Agents may share behavior definitions but have apparent and distinct states
	and behaviors.</li>
	<li>Collective: Models contain large communities of agents which exhibit collaborative and
	competitive behaviors.</li>
	<li>Emergent: Agents have collective macro-behaviors that are non-obvious from agent
	micro-specifications.</li>
</ul>
<h2>Intent</h2>

The primary focus of AMP is Agent-Based Modeling (ABM), an innovative technique used to explore
complex phenomenon in many domains, including economics, social sciences, bio-medicine, ecology and
business operations. The Eclipse Platform provides many unique features that make it ideal for an
ABM platform.
<p />But the Agent Modeling Platform is not limited to ABM. Many kinds of objects share
characteristics listed above and software agents are used in many other contexts. Thus, AMP's
meta-modeling support is potentially suitable (or extendible) for a number of approaches outside of
ABM; for example business rules, object interactions, systems dynamics models and traditional
discrete event models. Similarly, AMP execution and graphic support can be used for modeling natural
systems but could also be used to manage other software agents -- for example independent reasoning
tasks or dynamic visualization support.
<p />AMP is also not intended as a monolithic ABM tool. Instead it provides infrastructure and
highly leveraged tools for ABM and other agent technologies. As with the Eclipse project itself,
AMP's vision is of a set of loosely coupled yet strongly collaborating components that integrate and
enable a large and diverse ecosystem of tools and approaches. While AMP is focused on providing
tools for the ABM community, we encourage and support other uses as well as inter-project
collaboration within this broader scope.
<h2>Components</h2>
<p>The relationships between Eclipse, the AMP components and the outside world are summarized in
the architectural diagram below:
<p><img src="AMPArchitecture.png" />
<h3>Agent Modeling Framework (AMF)</h3>
AMF is an Agent-Based Model Meta-Model representation, editor, generator and development
environment. AMF is analogous to EMF but is targeted toward the design and execution of models
composed of agents. The AMF meta-model (currently "MetaABM", but as this name is similar to the
proposing organization's, "Acore" is proposed) is defined in Ecore but provides a more direct and
high-level ABM representation of agents, including spatial, behavioral and functional features
sufficient to generate complete executable models for the target platforms.
<p>AMF's generative capability is designed to be pluggable and modular. While included targets
provide self-documentation, interfaces and so on, they do not target any specific ABM or host system
platform. In keeping with the desire to provide a common ABM infrastructure that facilitates rather
than constrains tools, most ABM execution targets are provided separately. Targets need have no
inherent dependencies on Eclipse or even on a traditional platform. A hierarchical editor is part of
the core offering, but the universe of potential providers is also open; including graphical and
textual editors, as well as automated sources, external model transformations, and guided tools.
<h3>Agent Execution Framework (AXF)</h3>
The execution framework provides core interfaces, services and UI for model life-cycle execution,
agent aggregation, view and control management, and workbench interaction. In keeping with the
overall implementation neutral approach, AXF is not itself intended to provide extensive support for
runtime execution, but instead acts as an abstraction layer between the Eclipse environment and
arbitrary model execution toolkits. For example, implementations would provide model internal
scheduling engines and data collection facilities which are wired up at runtime through adapters.
While there are no dependencies between AMF and AXF, AXF does provide corresponding features where
appropriate. As with AMF, AXF is not limited to Agent-Based Modelers. Anyone who has a need for
executing, managing and visualizing Workbench hosted processes containing collections of interacting
objects should be able to make use of AXF services.
<h3>Agent Graphics and Visualization Framework (AGF)</h3>
The graphics framework extends GEF, Draw2D, Zest and other technologies to support real-time
visualization of and interaction with agent models. Currently, AGF provides support for agents
within 2D space and graph structures. Like the other AMP components, AGF design focus is to provide
an extensible infrastructure, allowing platform adopters to easily create their own view and editor
parts.
<p>AGF will include two sub-components:
<ul>
	<li>AGF-Chart: A dynamic charting facility with end-user oriented interactive tools. An
	envisioned extension would connect to a new AXF Data (ADF) component, which would provide a dynamic
	data service based around DTP.</li>
	<li>AGF-Viz: Support for graph and -- eventually -- other dense information dynamic
	visualization such as nD.</li>
</ul>
<h3>Escape</h3>
Escape is an Open Source ABM environment based on Ascape, a toolkit that has existed for over ten
years. It is offered as an optional implementation for the AXF and AGF frameworks, to provide a
relatively simple non-privileged exemplar for the AMP ecosystem. (Alternatively we can offer this
tool outside of the Eclipse project, but as it does provide a complete set of model execution and
exploration services, as well as a number of important example models, we're proposing its inclusion
here.) One compelling aspect of the AMP Escape implementation -- facilitated by the Eclipse class
loading mechanisms -- is that it allows modelers to code in Java and/or generate models with AMF and
then execute those models within the Agent development environment without needing to spawn a new
environment. This level of interactivity is missing from current Java based ABM tools and was an
important reason for adopting Eclipse.
<h2>Collaboration</h2>
While AMP benefits from a cohesive vision, Eclipse community members are encouraged to review
sub-projects to determine areas for potential collaboration or even combination with other Eclipse
projects within a wider coordinated effort. As many aspects of the project are quite mature, we
would like to offer the existing platform converted to Eclipse foundation specifications prior to
making significant architectural changes.
<p>In addition to the dependencies pictured above, a review of existing or proposed Eclipse
projects reveals a number of complimentary offerings, in one or two cases with minor overlap. Some
of these are quite speculative!
<ul>
	<li>AMF has strong interactions with current and proposed Modeling Project offerings. As a
	single example, AMF may cross-pollinate with MXF. Both approaches aim at the next challenge in
	meta-modeling; providing a high-level representation of executable behavior that can be directly
	instantiated.</li>
	<li>AGF-Chart and the envisioned ADF component could offer BIRT / Chart Engine users tools for
	easily working with dynamic and real-time data sources, as well as advanced tools -- perhaps in
	conjunction with AMF -- for working with arbitrary data-collections.</li>
	<li>BPMN could be used as a provider for AMF, allowing for the automated creation of
	executable business model simulations.</li>
	<li>AGF and eventually ADF services could be used as front-ends for PTP processes. There may
	also be potential for shared features with AXF, such as UI and parameterization / configuration
	tools.</li>
	<li>AMP contributors and supporters have a strong interest in integration with ontological
	systems such OWL/RDF. Unfortunately EODM no longer appears to be an active Eclipse project, but
	some similar service might be contemplated within the Modeling Project.</li>
</ul>
<h2>Organization</h2>
<h3>Initial Committers</h3>
<ul>
	<li><a href="mailto:miles@metascapeabm.com">Miles Parker</a>, Founder, <a
		href="http://metascapeabm.com">Metascape, LLC</a> -- Proposed Lead</li>
</ul>
<h3>Mentors</h3>
<ul>
	<li><a href="mailto:ed.merks@gmail.com">Ed Merks</a></li>
	<li><a href="mailto:sven.efftinge@itemis.de">Sven Efftinge</a></li>
</ul>
<h3>Interested Parties</h3>
<ul>
	<li><a href="mailto:docgca@gmail.com">Gary An</a>, President, <a href="http://www.swarm.org/">Swarm
	Development Group</a>; Assistant Professor of Surgery, Division of Trauma/Critical Care, Northwestern
	University <a href="http://www.medschool.northwestern.edu/">Feinberg School of Medicine</a>
	(Ontologies)</li>

	<li><a href="mailto:lyle.wallis@decisio.com">Lyle Wallis</a>, Partner, <a
		href="http://www.decisio.com/">Decisio Consulting</a> (Dynamic Systems)</li>
</ul>
<h3>Code Contributions</h3>
Metascape, LLC is offering its commitment and the initial codebase to the larger Eclipse community.
The codebase was authored by Miles Parker, influenced by discussions with many other members of the
ABM community, and supported by a number of organizations over a ten year span.
<ul>
	<li>AXF and AGF (unreleased) -- &copy;Metascape, LLC.</li>
	<li>AMF -- MetaABM (http://metascapeabm.org) &copy;Metascape, LLC based on Repast Simphony
	"score" component supported by Argonne National Labs, &copy;ROAD.</li>
	<li>Escape (unreleased) -- &copy;Metascape, LLC, includes Ascape
	(http://ascape.sourceforge.net) &copy; Metascape, LLC, NuTech Solutions, Inc. (formerly Bios,
	inc.), and The Brookings Institution.</li>
</ul>
<h3>Licensing</h3>
<ul>
	<li>All Metascape contributions will be released under the EPL.</li>
	<li>All other contributions have been released under BSD licenses. The proposers will work
	with other copyright holders to release under EPL wherever possible.</li>
</ul>
<h2>References</h2>
<h3>Websites</h3>
<a href="http://metaabm.org">MetaABM</a>
<p /><a href="http://ascape.sourceforge.net">Ascape</a>
<p />MetaABM SVN: http://svn.metascapeabm.com/metaabm/
<p />Ascape SVN: https://ascape.svn.sourceforge.net/svnroot/ascape/
<h3>Papers</h3>
<a href="http://www.pnas.org/content/99/suppl.3/7280.full">Agent-Based Modeling</a>
<p /><a href="http://jasss.soc.surrey.ac.uk/11/4/12.html">"Why Model?"</a>
<p /><a href="http://www.agent2006.anl.gov/2006procpdf/Parker_Agent_2006.pdf">Agent-Based
Meta-Models</a>(An earlier implementation.)
<p /><a href="http://metascapeabm.com/images/news/tr1-metaabminpictures.pdf">MetaABM in Pictures</a>
<p /><a href="http://www.pnas.org/content/99/suppl.3/7304.full">Ascape</a>
<p />
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
