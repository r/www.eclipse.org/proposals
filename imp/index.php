<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Eclipse IDE Meta-tooling Platform ("The Eclipse IMP")</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("IMP");
?>


<h2>Introduction</h2>

<p>The Eclipse IDE Meta-tooling Platform is a proposed project under the
   <a href="http://www.eclipse.org/technology/">Eclipse Technology Project</a>.</p>

<p>This proposal is in the
   <a
   href="http://www.eclipse.org/projects/dev_process/development_process.php#6_2_2_Proposal">Project
   Proposal Phase</a> (as defined in the
   <a href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse
   Development Process</a> document) and is written to declare its intent and scope. This
   proposal is written to solicit additional participation and input from the Eclipse
   community. You are invited to comment on and/or join the project. Please send all
   feedback to the
   <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.imp">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.imp</a>
   newsgroup.</p>

<h2>Background</h2>

<p>Eclipse provides a very high-functionality IDE for Java, as well as for several other
   languages (e.g. <a href="http://www.eclipse.org/cdt/">C/C++</a>, Python, Ruby). At
   this point, however, the cost of developing an IDE for a new language remains
   prohibitively high. This is due largely to the fact that although Eclipse offers a
   rich suite of building blocks for constructing a wide variety of applications, the set
   of APIs intended specifically for building IDEs is largely tied to specific
   languages. This precludes code reuse even where common concepts and language
   processing idioms already exist.</p>

<p>Moreover, building an IDE for Eclipse is an extremely knowledge-intensive task,
   requiring intimate understanding of hundreds or even thousands of interfaces and
   implementation classes. As a result, the learning curve is daunting, and there is not
   nearly enough guidance available to direct the IDE developer to the relevant
   interfaces and reusable implementation classes.</p>

<h2>Description</h2>

<p>The Eclipse IMP's purpose is to develop a set of extensible frameworks and exemplary
   tools built on the Eclipse platform that ease the creation of IDEs for programming
   languages (new or otherwise) that do not yet enjoy full-featured Eclipse support. Our
   ultimate goal is to assist language implementers in creating IDEs with a richness and
   usability on par with the <a href="http://www.eclipse.org/jdt/">Eclipse Java
   Development Toolkit (JDT)</a>. In the near term, however, we intend to provide support
   for a significant portion (but not all) of the JDT's functionality. It is our belief
   that this is a natural next step in the evolution of the Eclipse - doing for
   programming IDEs what <a href="http://www.eclipse.org/home/categories/rcp.php">RCP</a>
   does for rich UI applications.</p>

<p>By accelerating the process of developing new IDEs, we also hope to expand the use of
   Eclipse to new programming domains, further enhancing the appeal of the Eclipse
   platform. In fact, we hope to encourage language design experimentation by making it
   viable for language implementers and their communities to produce full-featured IDEs
   with more reasonable effort than is presently possible.</p>

<h2>Proposed components</h2>

<ul>
<li><b>Service Generators.</b> A set of code-generating wizards and templates that
    generate stub/initial implementations for various language-specific IDE services,
    along with any necessary meta-data (e.g. extensions of IMP-defined extension points).
<li><b>IDE Runtime.</b> A class library that encapsulates behavior and appearance common
    to many IDEs and languages, with hooks (via extension points or abstract methods) for
    providing language-specific customizations. Example sub-components include views such
    as the source editor, the outline view, or the program explorer, a dependency
    tracking mechanism for incremental building, and so on.
<li><b>Language Service Extension Points.</b> A set of extension points that encapsulate
    the various language-specific IDE service implementations (e.g. parsers, syntax
    highlighters, outliners, formatters, etc.).

<li><b>Declarative Languages.</b> A set of domain-specific languages that will allow IDE
    developers to specify such language-specific concepts as language syntax (e.g., for
    syntax highlighting or outline contents), language semantics (used in providing
    various other language services), appearance in the IDE of program elements, program
    transformations (e.g. refactorings), various program analyses, and so on. This will
    include full-featured IDE support for the declarative languages themselves.
<li><b>Parser Generator IDEs.</b> Since much of IDE development activity revolves around
    the language grammar, this component provides IDE support for various existing
    grammar specification languages and parser generators, such as LPG, Antlr, or JavaCC.
<li><b>Compiler/Parser Adapters.</b> IMP will not require any particular scanning,
    parsing, or AST technology. In particular, IMP must be capable of reusing existing
    compiler front-ends. This has the dual benefit that IDE developers can avoid
    gratuitously rewriting existing code, and that the IDE and compiler are in complete
    agreement as to the structure and meaning of every construct. To that end, some
    adapters will be required to permit IMP components to work with existing
    parser/compiler generators, such as LPG, Antlr, or JavaCC.
<li><b>AST-based Tools.</b> A key part of refactoring is transforming existing
    code. Currently, in the JDT this is done by writing verbose Java code to manipulate
    ASTs and build new AST nodes. A language-independent AST pattern/transformation
    language would help speed the development of new transformations and make it easier
    to reason about their correctness. Such a pattern language would also be useful for
    specifying how to populate search indexes, specifying candidate "quick fixes", as
    well as for providing the basis for a user-level AST-based search mechanism. We
    intend to develop a suite of domain-specific languages for accomplishing these and
    other tasks, all making use of a common AST pattern language.
<li><b>Static Analysis.</b> A set of reusable analysis components (type inference,
    pointer analysis, call-graph construction, escape analysis, etc.) to be used in
    constructing refactorings and other tools that rely on deep semantic knowledge of the
    program. While static analysis is by definition language-specific, the analysis
    algorithms themselves can share much code, including data structures, graph
    algorithms, generic solvers, etc. Probably to be built on top of an existing static
    analysis framework such as WALA.
<li><b>Debug Framework.</b> This component would provide, e.g., a specification language
    to define the debug-time presentation of the runtime platform (e.g. backtraces,
    variables, breakpoints, etc), along with a framework to map this specification into
    implementations of the necessary Eclipse Debug APIs.

<li><b>Process Documentation.</b> Cheat sheets and other forms of documentation to guide
    the IDE developer through the process.
<li><b>IDE-building Perspective.</b> A perspective helps focus the user's attention on
    the relevant IDE-building views, actions and commands by making them appear in the
    toolbar, top-level menus, and so on.
</ul>

<h2>Relationship with Other Eclipse Projects</h2>

<p>The Eclipse IMP will be built on top of the Eclipse Platform. Since
   the <a href="http://www.eclipse.org/jdt/">JDT</a> represents the desired level of
   functionality, we intend to work with the JDT team to identify existing components in
   the JDT that can be generalized and made part of the IMP.</p>

<p>The <a href="http://www.eclipse.org/jdt/ui/dev.php#plugins">LTK (Language ToolKit)</a>
   project provides runtime language support components, which is used for example by the
   IMP to provide refactoring support. We will investigate which IMP components could be
   moved to the LTK, which can be used even by IDE developers who do not build their IDE
   using the IMP meta-tooling.</p>

<p>The <a href="http://www.eclipse.org/webtools/main.php">Web Tools Platform's</a>
   Structure Source Editor may also provide components useful for the source editing
   portion of the IMP runtime.</p>

<p>The <a href="http://www.eclipse.org/dltk/">DLTK</a> is a related effort, but requires
   the use of a specific AST class hierarchy to drive much of its functionality, which is
   not suitable for arbitrary languages, nor for situations in which a compiler front-end
   already exists. Nevertheless, we intend to investigate what portions of the DLTK's
   infrastructure can be reused or adapted.</p>

<p>Ed Merks of the <a href="http://www.eclipse.org/modeling/emf/">EMF project</a>
   believes that EMF may benefit from enhancing IMP to permit the design and
   implementation of concrete textual language syntax from existing EMF models. This is
   based on the observation that an EMF model can be viewed as abstract syntax for a
   language. The result is that rather than using generic list- and tree-based EMF editor
   views for both the model and model instances, one can instead use IMP-based
   language-specific (i.e. model-specific) textual source editors, with all the
   affordances that IMP provides for textual languages (e.g. syntax highlighting, content
   assistance, navigation, hover help, etc.).</p>

<h2>Organization</h2>

<p>We propose this project should be undertaken as a Technology project rather than as
   part of the Eclipse Platform. The technology is not yet well defined enough in either
   specification or implementation to be ready for general consumption. As a Technology
   project, we will have the opportunity for eager adopters to use, critique, and improve
   the software in a non-disruptive manner before its use in mainline Eclipse Platform
   development work.</p>

<p>Finally, since Technology sub-projects are not meant to be ongoing, we foresee three
   possible evolutionary paths for the subproject:</p>

<ol>
<li>The project is merged back into the mainline Eclipse Platform top-level project.
<li>The project is moved to a permanent state as a Tools Platform project.
<li>The technology proves to be insufficient for Eclipse and the work is set aside.
</ol>

<p>In all likelihood, some combination of outcomes (1) and (2) will make the most sense
   for sub-portions of the components developed within this project.</p>
   
<h2>Mentors</h2>
<ul>
<li>Rich Gronback, Borland</li>
<li>Ed Merks, IBM</li>   
</ul>

<h2>Proposed initial committers</h2>

<ul>
<li><a href="mailto:rfuhrer@watson.ibm.com">Robert M. Fuhrer</a> - IBM T.J. Watson Research Center
<li><a href="mailto:pcharles@us.ibm.com">Philippe Charles</a> - IBM T.J. Watson Research Center

<li><a href="mailto:suttons@us.ibm.com">Stanley Sutton</a> - IBM T.J. Watson Research Center
<li><a href="mailto:jurgen.vinju@cwi.nl">Jurgen Vinju</a> - CWI, Amsterdam
<li><a href="oege.de.moor@comlab.ox.ac.uk">Oege de Moor</a> - Oxford University, England
</ul>

<h2>Code Contributions</h2>

<p>IBM Research is offering to contribute its Eclipse IMP software (previously released
   as open-source <a href="http://sourceforge.net/projects/eclipse-imp">here</a>) as the
   initial codebase.</p>

<h2>Interested parties</h2>

<ul>
<li>Borland, Inc. - Artem.Tikhomirov@borland.com
<li>Eclipse JDT team - Martin Aeschlimann, Philippe Mulet
<li>Oxford University - Oege.de.Moor@comlab.ox.ac.uk
<li>Centrum voor Wiskunde in Informatica (CWI) - Paul.Klint@cwi.nl, Jurgen.Vinju@cwi.nl
<li>Cornell University - Andrew Myers
<li>EPFL, Switzerland - Martin Odersky
<li>Delft University - Eelco Visser
<li>Eclipse CDT team
<li>IBM Rational - Chris Laffra
</ul>

<h2>Tentative Plan</h2>

<ul>
<li>Initial Eclipse.org presence in Oct 2007
<ul>
<li>website
<li>newsgroup
<li>CVS repository, seeded with source code from current (non-public), if IBM source code
    contributions are accepted
<li>Bugzilla repository, seeded with bug reports from current (non-public) Bugzilla
    repository, if IBM's code contributions are accepted.
</ul>

<li>Initial Eclipse.org release, v1.0: Dec 2007, including:
<ul>
<li>IMP Runtime library including language-independent editor
<li>Initial version of meta-tooling wizards, with support for scanning, parsing, syntax
    highlighting, outlining, source folding, hover help, hyperlinking, content assist,
    incremental building.
<li>Meta-tooling support and IDE support for LPG-based grammar and scanner
    specifications, with APIs that permit integration with other parser/scanner
    generators
<li>Exemplar IMP-based IDEs for X10, the LPG grammar specification language, Java and
    JavaScript
<li>Cheat sheets guiding/describing IDE development process

<li>Initial installation and use documentation, taken from existing (non-public) web
    pages
</ul>

<li>Second Eclipse.org release, v1.1: June 2008, possibly including:
<ul>
<li>Language embedding
<li>Integration with the JAstAdd compiler framework
<li>Integration with other parser generators such as ANTLR and/or JavaCup 
<li>Improved support in meta-tooling and code templates for language inheritance
</ul>
</ul>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
