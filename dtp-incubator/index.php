<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("DTP Incubator");
?>

<h1>Proposal for the Eclipse Data Tools Platform Incubator Project</h1>

<h2>Introduction</h2>

<p>The Eclipse Data Tools Platform Incubator is a proposed open source project
under the Eclipse Data Tools Platform project.</p>

<p>This proposal is in the Project Proposal Phase (as defined in the Eclipse
Development Process document) and is written to declare its intent and scope.
This proposal is written to solicit additional participation and input from the
Eclipse community. You are invited to comment on and/or join the project. Please
send all feedback to the <a
href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.dtp">http://www.eclipse.org/newsportal/thread.php?group=eclipse.dtp</a>
newsgroup.</p>

<h2>Project Overview</h2>

<p>The Eclipse Data Tools Platform Incubator project will host components that
are within the scope of the Eclipse Data Tools Platform project, but not yet
ready for distribution in the main code line. Through targeted mentoring by the
Eclipse Data Tools Platform team, committers working on incubating components
can bring their code and operating processes to maturity.</p>

<h2>Scope</h2>

<p>The Eclipse Data Tools Platform Incubator will focus on new development in
areas that are relevant to the other Eclipse Data Tools Platform projects,
which because of their nature would not be appropriate for direct inclusion in
the effected sub-project. This could be because the work is still experimental,
will have a longer timeline than can be contained within a single release, has
dependencies on external IP that has not yet cleared the Eclipse Foundation IP
process, or is simply potentially too destabilizing in nature.</p>

<h2>Out of Scope</h2>

<p>Work in the Eclipse Data Tools Platform Incubator will be constrained to only those
efforts that we expect to graduate (i.e. the code will eventually become part
of one of the other Eclipse Data Tools Platform sub-projects). It is not a
playground for arbitrary development efforts. In addition, we should ensure
that investment in the incubator never leaves the community with the perception
that it is coming at the cost of important work on the other Eclipse Data Tools
Platform sub-projects.</p>

<h2>Organization</h2>

<p>The Eclipse Data Tools Platform Incubator (<i>org.eclipse.datatools.incubator</i>)
will be a new sub-project of the Eclipse Data Tools Platform project, and will
contain a number of incubating components and their associated committer teams.</p>

<h2>Mentors</h2>

<ul>
<li>Rich Gronback (BEA, Modeling PMC Lead)</li>
<li>Ed Merks (IBM, Modeling PMC Lead)</li>
</ul>

<h2>Initial committers</h2>

<ul>
 <li class=MsoNormal>John Graham (Sybase, DTP PMC)</li>
 <li class=MsoNormal>Linda Chan (Actuate, DTP PMC)</li>
</ul>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
