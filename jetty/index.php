<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Jetty</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Jetty");
?>

<p>
		<h2>Introduction</h2>
		<p>The Jetty Project is a proposed open source project under the Eclipse Runtime 
		Project. This proposal is in the Project Proposal Phase (as defined 
		in the <a href="/projects/dev_process/">Eclipse Development Process 
		document</a>) and is written to declare its intent and scope. This proposal is 
		written to solicit additional participation and input from the Eclipse community. You 
		are invited to comment on and/or join the project. Please send all 
		feedback to 
		the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.jetty">http://www.eclipse.org/newsportal/thread.php?group=eclipse.jetty</a> newsgroup.</p>
		
		<h2>Background</h2>
		<p>Jetty is a Java-based web server and implementation of the Servlet Specification and is currently hosted at The Codehaus under the Apache 2.0 license. It was initially implemented in 1995 as MBServler, and has gone through 6 major releases since then. It is now one of the premier implementations of the Servlet standard as found by the monthly survey found at <a href="http://www.netcraft.com">Netcraft.com</a>. While that survey would not count Jetty instances behind firewalls, Apache HTTPD facades, load balancers, or instances with identifiers turned off, it still finds nearly a quarter million internet domains served directly by versions of the Jetty server. Since the version 3.3 release of the Eclipse IDE, Jetty has been employed to serve its help system. It is a mature project and technology, with numerous committers, both to its core, and to its add-on efforts.  Furthermore, numerous commercial products and other open source projects use Jetty as an integral component. <a href="http://docs.codehaus.org/display/JETTY/Jetty+Powered">The Jetty Powered Page</a> shows many examples of commercial use.</p>
		
		<h2>Scope</h2>
		<p>The objectives of the Jetty project are to:</p>
			<ul>
				<li><p>Maintain an embeddable release of a HTTP server and servlet container.</p></li>
				<li><p>Participate in the formation and implementation of future versions of relevant specifications.</p></li>
				<li><p>Foster better integration with other open source projects, such as Eclipse Equinox, and various tooling efforts.</p></li>
				<li><p>Provide a platform for other projects to create highly scalable solutions in their particular domains.</p></li>
			</ul>

		<h2>Description</h2>
		<p>Jetty is an open-source, standards-based, full-featured web server implemented entirely in Java. It is released under the Apache 2.0 license and is therefore free for commercial use and distribution. Once established as an Eclipse project, it will be dual-licensed, maintaining its Apache 2.0 license, and adding the Eclipse Public License. Beyond the license addition, which would not require any changes from the current large number of users or consuming open source projects or commercial products with use of Jetty, the move is seen as having numerous benefits for the projects and community...</p>
		<p><ul> 
			<li><p>The transition would be a great time to clean up the project's packaging as it moves to an org.eclipse name.</p></li>
			<li><p>The project will be focused on providing components and less on providing a bundled application server stack. This will allow great focus of development energies and further bring out Jetty's embedding strengths.</p></li>
			<li><p>The Eclipse IP policies will supplement Jetty's existing processes.</p></li>
			<li><p>Jetty could discuss handling the maintenance and updating the OSGI HTTP service for Equinox as a module. The project is also interested in being involved in an effort to modernize the standard for OSGI HTTP service. At this time, the project does not envision revising its internal structure to use OSGi, but to stay with POJO that can be configured with IOC/DI frameworks, which will work well for OSGi.</p></li>			
			<li><p>The being a part of Eclipse's release trains would simplify spreading current versions of Jetty for use in relevant projects within the Foundation that may be interested.</p></li>
		</ul></p>
		<p>As history, Jetty was first created in 1995, and has benefited from input from a vast user community and consistent and focused development by a stable core of lead developers. There are many examples of Jetty in action on the  <a href="http://docs.codehaus.org/display/JETTY/Jetty+Powered">Jetty Powered Page</a> however, as Jetty aims to be as unobtrusive as possible, countless websites and products are based around Jetty, but Jetty is invisible!</p>
		<p>Jetty can be used as:</p>
		<p>The objectives of the Jetty project are:</p>
		<p>	<ul>
				<li><p>a stand-alone traditional web server for static and dynamic content</p></li>
				<li><p>a dynamic content server behind a dedicated HTTP server such as Apache using mod_proxy or mod_jk</p></li>
				<li><p>an embedded component within a java application</p></li>
				<li><p>as a component to build an application server or Java EE server.</p></li>
			</ul>
		</p>

		<p>This flexibility means that Jetty can be encountered in a number of different contexts:</p>
		<p>	<ul>
				<li><p>shipped with products to provide out-of-the-box usability eg Tapestry, Grails, JRuby, and Mercury at Apache Maven</p></li>
				<li><p>distributed on CDs with books to make examples and exercises ready-to-run</p></li>
				<li><p>incorporated into applications as a HTTP transport eg JXTA, Cisco SESM</p></li>
				<li><p>integrated as a web container in JavaEE app servers eg Jonas, Geronimo, JBoss, Sybase</p></li>
				<li><p>included as a component of an application eg Eclipse IDE, Hadoop</p></li>
			</ul>
		</p>

		<p>Some of the defining features of Jetty are:</p>
		<p><b>Simplicity</b></p>
		<p>	<ul>
				<li><p>Jetty is built by an assembly of simple components.</p></li>
				<li><p>Where possible features are added by aggregation of components rather than creation of complex deep/optional APIs</p></li>
				<li><p>Assembly and configuration can be done by the java API, the jetty XML configuration file, which is an IOC style mapping of XML to POJO APIs, other IOC/component frameworks such as Spring and Plexus, OSGi activators, and other XML to POJO mappings such as XBeans</p></li>
			</ul>
		</p>
		
		<p><b>Embeddability</b></p>
		<p>Jetty is designed to be a good component. This means that it can easily be embedded in an application without forcing the application to adapt to it's</p>
		<p>	<ul>
				<li><p>Configuration files or formats</p></li>
				<li><p>File system layout</p></li>
				<li><p>Classloading hierarchy</p></li>
				<li><p>Usage of dependency injection and inversion of control patterns</p></li>
			</ul>
		</p>
		
		<p><b>Pluggability</b></p>
		<p>Jetty is architected for pluggability. The API allows different implementations of all of the principal Jetty components to be selected. At least one, but sometimes more, implementations of a component are always provided. However if these do not meet your needs, you are free to code your own using the interfaces and abstract classes as a basis. This means that Jetty can be easily customized to a particular application environment. This is particularly useful when Jetty is acting as the web container in a JavaEE server, as Jetty's pluggability ensures a tight integration with a host container.</p>
		
		<h2>Organization</h2>
		<p><b>Initial Committers</b></p>
		<p>Jetty has a long history, and a diverse base of existing committers.  The initial ones at Eclipse will be:</p>
				<ul>
						<li><p>Greg Wilkins (Webtide): Project Lead</p></li>
						<li><p>Jan Bartel (Webtide)</p></li>
						<li><p>Athena Yao (Webtide)</p></li>
						<li><p>Jesse McConnell (Webtide)</p></li>
						<li><p>David Yu (Webtide)</p></li>
						<li><p>Simone Bordet (Webtide)</p></li>
						<li><p>David Jencks (IBM)</p></li>
				</ul>
		
		<p><b>Interested Parties</b></p>
		<p>The following projects have expressed interest or are already working with Jetty in its current form.</p>
			<ul>
					<li><p>EclipseSource: Jeff McAffer</p></li>
					<li><p>nexB: Philippe Ombredanne</p></li>
					<li><p>IBM: Simon Kaegi</p></li>
					<li><p>Sonatype: Jason van Zyl</p></li>
			</ul>
			
		<p><b>Mentors</b></p>	
		<ul>
		<li><p>Jeff McAffer, EclipseSource</p></li>
		</ul>
			
		
		<h2>Developer Community</h2>
		<p>There is a base of active core committers, as well as people who contribute to ancillary modules and add-ons.  Furthermore numerous systems management, development, and other tools currently support Jetty. Our goal is to continue to work with everyone currently involved, and with greater Eclipse Foundation involvement, expand further.</p>
		<h2>User Community</h2>
		
		<h2>Tentative Plan</h2>
		<p><ul>
				<li><p>February 28, 2009. Code in Eclipse repository</p></li>
				<li><p>March 23-26, 2009. EclipseCon</p></li>
				<li><p>June 2, 2009. JavaOne debut of Jetty v7</p></li>
				<li><p>End of June, 2009 Jetty v7 with implementation of then current Servlet 3.0 specification hopefully in Eclipse Galileo train for release</p></li>
		</ul></p>
		
		</p>"Jetty" is: a breakwater: a protective structure of stone or concrete; extends from shore into the water to prevent a beach from washing away (<a href="wordnet.princeton.edu/perl/webwn">source</a>)</p>

</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
