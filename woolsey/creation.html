<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>
<title>Woolsey Project Creation</title>

</head>

<body>
<h1>Woolsey Project Creation</h1>

<p>March 9, 2010</p>

<p>The Woolsey Project Creation Review will complete on March 17, 2010. Please pose
questions about this project and the creation review on the 
<a href="http://www.eclipse.org/forums/eclipse.woolsey">Woolsey Project</a> Forum.</p>

<h2>Background</h2>

<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->

<p>As part of the Eclipse Development Process, projects are required
to maintain a log that details their intellectual property
contributions. This log must include all third-party code that is either
distributed or leveraged by the project, identities of the committers
who regularly contribute, and the identities of other contributors along
with their contributions. The log should further indicate the licenses
in use by the project and other information relevant to provenance of
the code maintained and distributed by the project. Projects are
required to submit their log for review by the Eclipse IP team at
key points in their life-cycle, including moves, releases, and
graduation.</p>

<p>Much of the content of the logs can be generated using
information that is available through the various databases maintained
by the Eclipse Foundation. The current tool, an automated IP Log 
generator known as the IP Log Tool, uses information from these
databases. This current generation of tool has limited
flexibility.</p>

<h2>Scope</h2>

<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->

<p>It is the purpose of this project to provide tools to assist
committers with the management of intellectual property related to their
project. This includes tools to assist with the generation and
maintenance of IP Logs, the ability to submit logs to the Eclipse
Foundation IP team for review, and the ability to compare logs against
project distributions (to ensure that an IP Log reflects the contents of
a project's downloads, p2 repositories, Maven repositories and other
similar distribution mechanisms). As part of this project, we further
intend to explore enabling the user to specify mappings between
third-party contributions noted in the logs (contribution
questionnaires) and the library files distributed by the project.
Other matters related to IP management by Eclipse projects may be
considered by the project.</p>

<p>The IP Log Tool uses an XML-based file format to store the log
information. This XML format is defined by an XML Schema (XSD)
maintained by the Eclipse Foundation [<a href="#iplog_xsd">1</a>].
Maintenance of this schema is not within the scope of this project (and
is therefore not, strictly speaking, an API of this project). The
project will, however, maintain compatibility with this schema. The
ability to read older versions of the schema (as it matures) will remain
in-scope; the ability to write older versions is out of scope.</p>

<p>Web services used by the project are not strictly owned by the
project and are therefore out of scope.</p>

<h2>Description</h2>

<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->

<p>The new generation of IP Log Tool provides much more flexibility
than the current generation by leveraging the Eclipse platform and
several Eclipse technologies. Specifically, the new IP Log Tool is an
Eclipse-based application that uses EMF technology (including EMF
Compare) that pulls data from Eclipse Foundation databases via RESTful
web services. All editing of the log is done within an Eclipse IDE (or,
potentially, an RCP application) and stored in the file system. The log
maintainer can choose where and how to store the XML-based log file. The
EMF tooling provides a very flexible editor that allows for complete
customisation. The use of EMF Compare allows for easy comparison between
a customised log and the information stored in Eclipse Foundation
databases (it further allows for consolidation of those changes).</p>

<p>The IP Log Tool is primarily a consumer of Eclipse Technology and
as such, we do not currently anticipate that we will be producing many
APIs. Having said that, the project will take advantage of any
opportunities to make the tool extensible.</p>

<p>In addition to being a useful tool for Eclipse committers, this
project also serves as a show case for Eclipse technology.</p>

<h3>More Detail and an Example</h3>

<p>The first thing you see when you use the tool is the wizard. The
wizard can populate a new IP Log with information gathered from the
various Eclipse Foundation databases. As you can see from the
screenshot, the user is presented with a hierarchical list of all
active Eclipse projects.</p>

<p><a
	href="http://dev.eclipse.org/blogs/wayne/files/2009/09/iplog-wizard.png"><img
	src="http://dev.eclipse.org/blogs/wayne/files/2009/09/iplog-wizard-300x245.png"
	alt="The IP Log Tool Wizard" width="300" height="245"
	class="size-medium wp-image-865" /></a></p>

<p>Here, the <a href="http://www.eclipse.org/ercp">eRCP</a> project
from the <a href="http://www.eclipse.org/rt">Eclipse RT</a> top-level
project has been selected. When the user clicks "Finish", the wizard
makes a call out to a web service running at the Eclipse Foundation to
gather all the necessary information. The results, which are saved in a
file in the workspace, are displayed in the editor.</p>

<p><a
	href="http://dev.eclipse.org/blogs/wayne/files/2009/09/iplog-editor.png"><img
	src="http://dev.eclipse.org/blogs/wayne/files/2009/09/iplog-editor-300x249.png"
	alt="IP Log Tool Editor" width="300" height="249"
	class="size-medium wp-image-866" /></a></p>

<p>This editor has been generated using <a
	href="http://www.eclipse.org/emf">EMF</a>. With almost no work, the
editor is completely functional (with help from the <em>Properties</em>
view). We envision eventually having a page for each part of the IP Log
(committers, third-party contributions, contributors and their
contributions, etc.).</p>

<p>One of the bigger challenges of maintaining an IP Log is keeping
it up to date. EMF Compare provides the ability to compare what is
captured in the log against the information found in the Eclipse
databases, and selectively update the log with those changes (we are
able to just reuse the same web service that was used to do the initial
population of the log). Here's what the compare editor looks like after
a few changes are made to the initial log:</p>

<p><a
	href="http://dev.eclipse.org/blogs/wayne/files/2009/09/iplog-compare.png"><img
	src="http://dev.eclipse.org/blogs/wayne/files/2009/09/iplog-compare-300x204.png"
	alt="IP Log Tool Compare" width="300" height="204"
	class="size-medium wp-image-867" /></a></p>

<p>Right now the compare and update works directly out of the
editor. Eventually, you'll be able to do the compare without even
opening the log. Note the "Copy current change from right to left" icon,
<a
	href="http://dev.eclipse.org/blogs/wayne/files/2009/09/right-to-left.png"><img
	src="http://dev.eclipse.org/blogs/wayne/files/2009/09/right-to-left.png"
	alt="" width="19" height="20" class="alignnone size-full wp-image-873" /></a>,
above the "Eclipse Foundation Databases" panel that users can employ to
make updates to their log. The functionality that permits movement from
left to right has been disabled: at least for now, Eclipse Foundation
Databases cannot be updated using this tool.</p>

<h2>Target Audience</h2>
<p>The target audience for this project is Eclipse Committers. While there is
likely some value in the project as an example of the use of Eclipse Technology, 
it is primarily intended as a tool to assist Eclipse Committers. As such, we do
not anticipate much interest from user or adopter communities. Further, while we
will openly welcome contributions, we do not anticipate any great flood of diversity
into the project. </p>

<h2>Initial Contribution</h2>

<!-- 
	Describe any existing code that will be contributed to the project.
 -->

<p>The current implementation of the IP Log Tool, which will serve
as the initial contribution, supports one-way retrieval of information
from Eclipse Foundation Servers. Specifically, it does not send any data
to the Eclipse Foundation. Since the current implementation only pulls
data, there is currently no authentication and authorisation strategy in
place. As the project develops, such strategy may be required.</p>

<p>The initial contribution was completed entirely by Wayne Beaton,
an employee of the Eclipse Foundation. Copyright is assigned to the
Eclipse Foundation, and the work is all distributed under the Eclipse
Public License. Some parts of the code have been generated using EMF.</p>

<h2>Legal Issues</h2>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->

<p>We are unaware of any legal issues at this time.</p>

<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

<p>The following individuals are proposed as initial committers to
the project:</p>

<ul>
	<li>Wayne Beaton, The Eclipse Foundation (Project Lead)</li>
	<li>Gabe O'Brien, The Eclipse Foundation</li>
</ul>

<p>Wayne and Gabe are responsible for having created the current code base and 
intend to stay on to develop the project. Both Wayne and Gabe are familiar with
the Eclipse Development Process and have excellent credentials as committers
on current Eclipse projects including the Eclipse Examples Project, Eclipse Packaging
Project, and Dash.</p>

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
	<li>Wayne Beaton, The Eclipse Foundation</li>
	<li>Chris Aniszczyk, EclipseSource</li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects
have expressed interest in this project:</p>

<ul>
	<li>C&eacute;dric Brun, Obeo</li>
	<li>Shawn Pearce, Google</li>
</ul>

<h2>Project Scheduling</h2>

<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->

<table>
	<tr>
		<td>April&nbsp;2010</td>
		<td>Project provisioned. Initial contribution sent to IP team for review. Code
		contributed. Regular Build script created.</td>
	</tr>
	<tr>
		<td>August&nbsp;2010</td>
		<td>Initial release (0.1) featuring EMF-generated editors and
		preview feature that renders XML-based log in HTML (via XSLT) for easy
		viewing.</td>
	</tr>
	<tr>
		<td>December&nbsp;2010</td>
		<td>Second release (0.2) including specialised editors and
		ability to submit log for IP review directly from workspace.</td>
	</tr>
</table>

<hr />
<p>[<a name="iplog_xsd"></a>1] <a
	href="http://eclipse.org/projects/xml/iplog.xsd">http://eclipse.org/projects/xml/iplog.xsd</a>.</p>
<hr />
<p>Copyright &copy; 2010 The Eclipse Foundation. All Rights Reserved</p>
</body>
</html>