<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Proposal for Rich Server Platform - User Interface";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
      <div id="midcolumn">
        <h1>
        Rich Server Platform - User Interface
        </h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Rich Server Platform - User Interface");
?>

<H1>Proposal for 
</H1>
<H1>Rich Server Platform &ndash; User Interface
Framework (RSP-UI)</H1>
<P>
</P>
<P>This document declares the intent,
benefits and scope of a proposed project called &quot;Rich Server
Platform &ndash; User Interface Framework&quot; (RSP-UI). RSP-UI is
in the Project Proposal Phase as defined in the <a 
href="http://www.eclipse.org/projects/dev_process/">Eclipse Development
Process document</a>. We propose that this project be established within
the Technology Project PMC.</P>
<P>
Current status: We are currently working on the creation review slides. 
Development is ongoing. New committers have recently joined the project 
for a PDE Launcher and a "web.xml" extension point.
</P>
<P>In particular, we are soliciting
participation and input from the Eclipse community beyond those who
already support this project proposal (see Organization). The more
input RSP-UI receives from you, the more cross-integration it can
provide. You are invited to comment on the project and strongly
encouraged to join. Please send all feedback to the 
<A HREF="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology</A> 
 newsgroup, "RSP-UI Proposal Discussion Thread" 
 (web access at <A HREF="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology">
 http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology</A>).</P>
<P>
</P>
<H2>Introduction</H2>
<P>Eclipse Rich Client Platform (Eclipse
RCP) has been successfully established as an application platform for
the client-side. With the recent acceptance of large parts of the
OSGi specification as a JSR (291), widespread application of OSGi for
server-side applications has become natural.</P>
<P>Recently, the Equinox Incubator has
begun applying the Eclipse/OSGi concepts to the server-side. This
opens tremendous new possibilities for Eclipse as an application
platform for the server-side (refer to
<A href="http://www.infonoia.com/en/content.jsp?d=inf.05.07">
http://www.infonoia.com/en/content.jsp?d=inf.05.07</a>) for a discussion
of possibilities.</P>
<P>
</P>

<P>The Eclipse/OSGi development model,
applied to the server-side, allows the creation of truly pluggable,
componentized, server-side applications. Plug-ins can communicate at
a logic layer with each other using established and tested methods,
such as extension points and OSGi services.</P>
<P>What is needed for the UI layer is a UI
composition framework which we propose in the form of RSP-UI.</P>
<P>
</P>
<H2>Executive Summary</H2>
<TABLE WIDTH=100% BORDER=1 CELLPADDING=4 CELLSPACING=2>
	<COL WIDTH=256*>
	<THEAD>
		<TR>
			<TD WIDTH=100% VALIGN=TOP>

				<P>1. What: UI framework to compose integrated server-side
				applications Eclipse/OSGi-style, thereby enhancing developer
				productivity</P>
			</TD>
		</TR>
	</THEAD>
	<TBODY>
		<TR>
			<TD WIDTH=100% VALIGN=TOP>
				<P>2. Allow sets of rendering technologies to be combined for
				versatile applications</P>

			</TD>
		</TR>
		<TR>
			<TD WIDTH=100% VALIGN=TOP>
				<P>3. Augment the Eclipse community by bridging client- and
				server-side assets</P>
			</TD>
		</TR>
	</TBODY>

</TABLE>
<P>
</P>
<P>This document proposes a Rich Server
Platform &ndash; User Interface Framework (RSP-UI) that will allow
you to compose integrated server-side applications
Eclipse/OSGi-style.</P>
<P>RSP-UI follows a highly integrating
approach:</P>
<P>
</P>
<P>First, RSP-UI will allow you to freely
combine the most suitable rendering technologies for versatile web
applications. Ideally, a web application based on RSP-UI should be
able to serve the user with a page that combines content from, for
example, a plug-in with XML/XSL transformation to display an RSS news
feed, a plug-in that uses PHP to render content, another plug-in that
uses JSF for rich interactive web components, and a plug-in that
renders LZX/Laszlo for interactive components with highest layout
requirements.</P>
<P>
</P>
<P>Second, RSP-UI will lower your entry
barrier to the respective advantages of the client- and the
server-side world, thereby augmenting the Eclipse community.</P>

<P>
</P>
<P>Furthermore, using RSP-UI, you will
increase code reuse, enjoy a flexible and lean assembly of just the
components and services you need, and enhance your productivity in
composing a versatile web application. RSP-UI adresses core needs in
the area of &quot;service-oriented clients&quot;, where this
architecture allows developers to build better applications that are
more targeted to user needs, tasks, roles and workflows.</P>
<P>
</P>
<P>You find a demo walkthrough with sample
code of a basic server-side application at
http://www.infonoia.com/en/content.jsp?d=inf.05.09. It uses
jsp:include tags as the lowest degree of UI composition.</P>
<P>
</P>
<H2>Issues, solution and benefits</H2>
<TABLE WIDTH=100% BORDER=1 CELLPADDING=4 CELLSPACING=2>
	<COL WIDTH=85*>

	<COL WIDTH=85*>
	<COL WIDTH=85*>
	<THEAD>
		<TR VALIGN=TOP>
			<TD WIDTH=33%>
				<P><B>Current Issues</B></P>
			</TD>
			<TD WIDTH=33%>

				<P><B>Our solution</B></P>
			</TD>
			<TD WIDTH=33%>
				<P><B>Your benefits</B></P>
			</TD>
		</TR>
	</THEAD>
	<TBODY>

		<TR VALIGN=TOP>
			<TD WIDTH=33%>
				<P>1. Code reuse between client-side and server-side is limited</P>
			</TD>
			<TD WIDTH=33%>
				<P>Server-side UI framework that renders &quot;dual&quot;
				components</P>

			</TD>
			<TD WIDTH=33%>
				<P><B>You will increase code reuse</B></P>
			</TD>
		</TR>
		<TR VALIGN=TOP>
			<TD WIDTH=33%>
				<P>2. Modularizing server-side applications is difficult</P>

			</TD>
			<TD WIDTH=33%>
				<P>Apply Eclipse/OSGi bundle paradigm to the server-side</P>
			</TD>
			<TD WIDTH=33%>
				<P><B>You will enjoy a flexible and lean assembly of just the
				components and services you need</B></P>
			</TD>
		</TR>

		<TR VALIGN=TOP>
			<TD WIDTH=33%>
				<P>3. Integrating UI from sets of rendering technologies is
				cumbersome</P>
			</TD>
			<TD WIDTH=33%>
				<P>Define (best practices) for UI interactions and composition</P>
			</TD>
			<TD WIDTH=33%>

				<P><B>You will enhance developer productivity</B></P>
			</TD>
		</TR>
		<TR VALIGN=TOP>
			<TD WIDTH=33%>
				<P>4. Server-side world offers a large choice of rendering
				technologies, but no single best answer as to which set to use
				for web UI</P>
			</TD>
			<TD WIDTH=33%>

				<P>Offer a large choice of rendering technologies to pick and
				choose from</P>
			</TD>
			<TD WIDTH=33%>
				<P><B>You will meet the diverse and special requirements of your
				application efficiently</B></P>
			</TD>
		</TR>
		<TR VALIGN=TOP>
			<TD WIDTH=33%>

				<P>5. Client-side and server-side have developed in separate
				worlds</P>
			</TD>
			<TD WIDTH=33%>
				<P>Leverage familiar RCP paradigms and established web
				technologies</P>
			</TD>
			<TD WIDTH=33%>
				<P><B>You will enjoy facilitated entry to the advantages of &quot;the
				other&quot; world</B></P>

			</TD>
		</TR>
	</TBODY>
</TABLE>
<P>
</P>
<P><B>1. Code reuse between client-side
and server-side is limited</B> &ndash; Many business-critical
applications have both a heavy data-entry element for internal use
(rich client) and a general-public web interface. Usually, the UI of
these will not be the same, however, good parts of the business logic
will be. By applying OSGi to the server-side, RSP-UI will provide you
with a web UI framework that allows you to reuse a lot of the
business logic (non-UI plug-ins) for both parts of an application.
With RSP-UI, you will be able to reuse &quot;dual&quot; components
that can be rendered both in a browser and in a rich client.</P>

<OL>
	<P></P>
</OL>
<P><B>2. Modularizing server-side
applications is difficult</B> &ndash; Many web applications have
numerous functional modules, such as shopping cart, news feed, or
user management. This compares to RCP applications where the Eclipse
IDE is also composed of numerous plug-ins. However, the WAR (Web
Archive) as the smallest deployment unit of aggregation in J2EE web
applications is too coarse grained to allow practical reuse of
application &quot;modules&quot;. It is not practical to package each
module into its own WAR for numerous reasons, including the fact that
inter-WAR dependencies are not defined. However, maintaining multiple
modules inside a single WAR, with code, and URL resources usually
residing in different places, does not provide the degree of
isolation that makes such modules easily transferable and reusable in
other projects. RSP-UI will apply the OSGi bundle paradigm to the
server-side. So you will enjoy a flexible and lean assembly of just
the components and services you need to compose your application.</P>
<OL START=2>
	<P></P>
</OL>
<P><B>3. Integrating UI from sets of
rendering technologies is cumbersome</B> &ndash; Eclipse/OSGi allows
inter-plug-in communication at the logic level via extension points
and OSGi services. Beyond the OSGi http-service as transport for http
resources, however, interactions at web UI level are not defined
today, making it cumbersome to &quot;glue components together&quot;

at the UI level. RSP-UI will focus on the definition of UI
interactions and UI composition in order to create a framework for
easily integrating UI from sets of rendering technologies and
frameworks already established in the server-side world. With RSP-UI,
you will enhance developer productivity</P>
<P>
</P>
<P><B>4. The server-side world offers a
large choice of rendering technologies, while there is no single best
answer as to which set to use for web UI</B> &ndash; The choice will
vary with project requirements and developer skills, and may well
incorporate multiple approaches. Ideally, a truly componentized web
application is able to integrate and display content from plug-ins
that use classic web technologies such as, but not limited to,
JSP/JSTL, framework Struts/WebWork framework, Wicket, JSF/Facelets
and other applicable widget libraries, Spring and PHP. It should also
be able to integrate components that use rich rendering technologies
such as LZX/Laszlo.</P>
<OL START=4>
	<P></P>
</OL>
<P>A truly componentized web application
should be able to serve the user with a page that combines content
from, for example, a plug-in with XML/XSL transformation to display
an RSS news feed, a plug-in that uses PHP to render content, another
plug-in that uses JSF for rich interactive web components, and a
plug-in that renders LZX/Laszlo for interactive components with
highest layout requirements. RSP-UI will help you to meet the diverse
and special requirements of your application efficiently (and on a
macro level serve the &quot;long tail&quot;, Carl Zetie, Forrester
Research).</P>

<P>
</P>
<P><B>5. Client-side and server-side have
developed in separate worlds</B> &ndash; RSP-UI lowers entry barriers
for RCP developers to the advantages of server-side programming by
using familiar paradigms such as extension points, services, and
workbench API. RCP developers can thus benefit from the advantages of
server-side programming but don't need to learn the complete J2EE
toolsets. On the other hand, as RSP-UI offers integration facilities
with established web frameworks and rendering technologies, web
developers can leverage their experience with server-side
applications and thus efficiently develop rich server-side
applications. RSP-UI will facilitate your entry to the advantages of
&quot;the other world&quot; and thereby augment the Eclipse
community.</P>
<P>
</P>
<H2>Scope</H2>
<TABLE WIDTH=100% BORDER=1 CELLPADDING=4 CELLSPACING=2>
	<COL WIDTH=256*>

	<THEAD>
		<TR>
			<TD WIDTH=100% VALIGN=TOP>
				<P>1. Define composition framework with API and sample
				implementations</P>
			</TD>
		</TR>
	</THEAD>
	<TBODY>

		<TR>
			<TD WIDTH=100% VALIGN=TOP>
				<P>2. Provide services commonly needed for web applications</P>
			</TD>
		</TR>
		<TR>
			<TD WIDTH=100% VALIGN=TOP>
				<P>3. Address needs ranging from dynamic websites to &quot;single-url&quot;

				web applications</P>
			</TD>
		</TR>
	</TBODY>
</TABLE>
<P>
</P>
<P>
</P>
<P><B>1. Define UI composition framework
with API and sample implementations</B></P>
<OL>

	<P></P>
</OL>
<P>Making plug-ins run on a server
involves the whole application stack; RSP-UI makes no attempt to
cover the whole territory. Instead, it narrowly focuses on defining
interfaces and creating sample implementations for UI integration and
associated components.</P>
<P>
</P>
<P>The RCP workbench API will be analyzed
and used as the most structured, Java-based API for composing web UI,
and leveraged by templating and decoration. We also envisage
markup-based composition using a tag library that can be used from
JSPs.</P>
<P>
</P>
<P>Essentially, the same basic
abstractions of Eclipse UI will be used. Therefore, PageLayouts will
be defined by a combination of Parts. Different part types are
EditorPart and ViewPart. Parts will have their trim window built by
the workbench framework. This workbench will also provide features to
manipulate (maximize, minimize, restore, close, move and dock) views
and editors. 
</P>
<P>
</P>
<P>A given Part will be associated to
ContentProvider. Providers will be the link to different rendering
technologies, ranging from script languages such as PHP to
fine-grained widgets libraries.</P>

<P>Perspectives are a way to group views
for a particular purpose. Plug-ins can define new perspectives or
change perspectives defined by other plug-ins. The API used for a
plug-in to define a new perspective will be essentially the same as
Eclipse&rsquo;s API. The developer will define an extension to the
perspectives extension point and implement the createInitialLayout()
method of a new IPerspectiveFactory implementation.</P>
<P>
</P>
<P>Likewise, the API for defining new
views are the same as Eclipse&rsquo;s (through views extension
point).</P>
<P>
</P>
<P>The RSP-UI composition framework will
aim to ensure that individual rendering technologies can access a
shared context (such as request, session and application scope for
servlets).</P>
<P>
</P>
<P>The Apache Tiles framework is a very
popular server-side UI composition framework. Given its age of about
5 years, Tiles has limitations in its out-of the box capacities to
render non-JSP/servlet content. Some limitations are those of the
content to be integrated. For example, integration of JSF is
difficult given that JSF currently does not support page scope. Tiles
does not use the notion of workbench contributions, perspectives,
views and the like. We aim to resolve these issues in a &quot;from-the
ground-up&quot; construction of an extensible and configurable UI
composition framework which leverages the Eclipse/OSGi approaches.</P>

<P>
</P>
<P>We will design interfaces and sample
implementations that allow integrating classic web technologies and
frameworks in combined web UI. This will usually require an intricate
understanding of the workings of the particular technology and
framework implementation. Class-loading approaches of such frameworks
that differ from the OSGi model will be investigated and resolved to
meet OSGi requirements.</P>
<P>
</P>
<P>Taking PHP as an example, we plan to
take a uniform integration approach for scripting languages, with the
aim to allow the use of various scripting languages supported by JSR
223 (Scripting for the Java Platform Specification 1.1), such as, but
not limited to, Groovy. Scripting  integrations will pass context
information through namespaces, as proposed in JSR-223.</P>
<P>
</P>
<P><B>2. Provide services commonly needed
for web applications</B></P>
<OL START=2>
	<P></P>
</OL>
<P>With project members coming from a
strong web applications background, part of the project effort will
be spent on features and services that developers have been able to
rely on in classic J2EE/J2SE web applications, and are useful to be
leveraged when writing Eclipse-OSGi server-side applications. These
features include, but are not limited to, runtime JSP compilation,
user authentication, servlet filters and listeners.</P>

<P>
</P>
<P>We will also address multiple
web-client specific issues, such as client state vs. server state,
user session state, &quot;bookmarkable URLs&quot;, double-submit,
redirect after post, client-side validation, windowing, and
converting events from such web UI components into events/commands
that can be understood by RSP-UI and relayed to the applicable
integrated framework/component. Further integrations with WebWork
(Struts 2) and Tapestry are sought.</P>
<P>
</P>
<P><B>3. Address needs ranging from
dynamic websites to &quot;single-url&quot; web applications</B></P>
<OL START=3>
	<P></P>
</OL>

<P>We consider leveraging time-tested
principles in established web module &ldquo;composition&rdquo;
frameworks such as Apache Tiles (Standalone) and SiteMesh to maximize
layout design and composition flexibility, and thus facilitate the
use of RSP-UI for any kind of dynamic web application, beyond a
specific workbench-style application. Note: actual use and
integration of Sitemesh would be subject to approval of modified
Apache license.</P>
<P>
<img src="./EcoSystem_606_456.gif" border=0 width="606px" height="456px"/>
</P>
<P>
</P>
<H2>Deliverables</H2>
<UL>
	<LI><P>Plug-ins that allow composing UI
	programmatically from multiple plug-ins that use any of the
	following: JSP/JSTL, Apache Struts, Wicket, JSF/Facelets and other
	applicable widget libraries, LZX/Laszlo and PHP. A
	choice of APIs will be proposed, to include workbench-type web
	applications, the most structured alternative will be closely
	aligned with the RCP workbench API (perspectives, views, etc.)</P>
	<LI><P>A tag library plug-in that allows
	doing the same declaratively</P>

	<LI><P>Sample &quot;OSGi-fied&quot;
	implementations of the interfaces (where not delivered by the
	original creator of the respective framework)</P>
	<LI><P>UI composition samples covering
	the technologies/frameworks above 
	</P>
	<LI><P>Basic build, run and deployment
	support similar to and based on PDE and PDE Build</P>
	<LI><P>Basic remote bundle management
	tool</P>
	<LI><P>Supporting server OSGi services as
	needed</P>

	<LI><P>Documentation of the above</P>
</UL>
<P>
</P>
<P>9 committers contribute to these
deliverables (see Organization for details).</P>
<P>
</P>
<H2>Relationships with other Eclipse Projects</H2>
<P><B>Ajax Tools Framework</B> &ndash; The
Eclipse Ajax Tools Framework project focuses on tools to write Ajax
applications. Ajax Tools can be used to build components for RSP-UI.</P>

<P>
</P>
<P><B>Corona (Tools Services Framework)</B>
&ndash; The Corona project initiated by Compuware addresses needs for
remote components in a Service-Oriented Architecture (SOA) context.
It has no server-side UI elements. We have discussed touching points
with the Corona architects, and agree that its focus and concerns are
orthogonal. However, we have agreed a close collaboration so that
both projects may benefit by sharing server-side infrastructure
components. Should RSP-UI require remoting capabilities (not seen
today), we will attempt to avoid implementations that would conflict
with those of Corona.</P>
<P>
</P>
<P><B>ECF (Eclipse Communication
Framework)</B> &ndash; Scott Lewis, ECF project lead, has expressed
interest to be a supporter of and possible committer to RSP-UI,
through RSP-UI usage and feedback by ECF. ECF is thus a potential
consumer of RSP-UI work.</P>
<P>
</P>
<P><B>Eclipse PHP-IDE</B> &ndash; The
Eclipse PHP-IDE project focuses on tools to write PHP applications.
PHP-IDE can be used to build PHP components for RSP-UI.</P>

<P>
</P>
<P><B>Eclipse Platform</B> &ndash; We have
engaged in discussions (with D. Pollock and C. Daly) with the intent
to use and integrate with the new and evolving Eclipse 3.2 command
framework to structure interactions and declaration of interactions
with the server, as is being introduced also to trigger commands from
the browser-based Eclipse help.</P>
<P>
</P>
<P><B>ECP (Enterprise Component Platform)
Proposal</B> &ndash; ECP has been initiated by Exadel and focuses on
a business component framework that uses a JSF implementation for UI,
including markup. RSP-UI sees ECP as fully complementary as it nicely
fits with its integrating approach. Exadel and RSP-UI have agreed to
collaborate to maintain the complementary alignment of both projects.
RSP-UI and ECP see each other as integration points.</P>
<P>
</P>
<P><B>Equinox Incubator</B> &ndash; There
is a very close collaboration established with Simon Kaegi, who is a
committer on the Equinox server-side incubator. Members of the RSP-UI
project consortium have previously tested, validated and provided
feedback to Equinox incubator work, notably the servlet bridge. If we
happen to create components that the Equinox incubator wishes to use
and feels are applicable beyond the scope of the RSP-UI project, we
will be most pleased to contribute them to their project.</P>

<P>
</P>
<P><B>IDE for Laszlo</B> - We have made
contact with the IDE for Laszlo project architect aiming to ensure
that LZX/Laszlo components can be rendered within RSP-UI
applications. RSP-UI should support logic integration via Laszlo
extension points.</P>
<P>
</P>
<P><B>OHF (Open Healthcare Framework)</B>
&ndash; We have discussed collaboration with OHF. OHF has server-side
requirements and wishes to be a consumer of RSP-UI work.</P>
<P>
</P>
<P><B>RAP (Rich Ajax Platform) Proposal</B>

&ndash; RAP has been initiated by Innoopract and focuses on a non-JSF
widget implementation for UI that emulates SWT/JFace, in a Java-only
approach. First, RSP-UI aims to match the Java and web worlds by
contributing Java web expertise to the Eclipse community, and second
considers RAP to be one of multiple rendering alternatives which
would fit with the integrating approach of RSP-UI. RSP-UI can be used
for any kind of dynamic web application, beyond RCP workbench-type
applications. We had invited RAP to cooperate with RSP-UI, but while
discussing cooperation, they chose to independently submit their own
proposal. We respect their wish for an independent activity. We have
agreed to revisit merging activities in 6 to 9 months.</P>
<P>
</P>
<H2>FAQ</H2>
<P><B>Does RSP-UI depend on JSP or other
non-Java technologies?</B></P>
<P>No. A tag library is part of the
deliverables, but UI can be composed through Java code only, in an
API that will essentially be the same as the Workbench API. In web
applications, some URL resources, such as CSS and JavaScript always
use non-Java technologies.</P>
<P>
</P>
<P><B>Will my web application that uses
RSP-UI have dependencies on
JSP-JSTL/Struts/WebWork/Tapestry/Wicket/JSF/Facelets/Spring/PHP?</B></P>
<P>Not unless you choose so. The RSP-UI
project aims to allow you to use the rendering framework(s) of your
choice; though you will want to use at least one of them to render
content in Views and Editors if you want to run more than simple
servlets in your web application. 
</P>

<P>
</P>
<P><B>How does RSP-UI relate to Tiles and
SiteMesh?</B></P>
<P>Apache Tiles (Standalone) allows UI
composition of modules via XML templates, while OpenSymphony SiteMesh
is strong in combining and decorating arbitrary HTML streams. Both
functionalities are very useful in the context of all types of web
applications. Therefore, RSP-UI will build on their composition
concepts for Eclipse/OSGi-based web applications. Project committer
Cedric Dumoulin is the original creator of Tiles. 
</P>
<P>
</P>
<P><B>Will RSP-UI work with Spring?</B></P>
<P>RSP-UI invites contributions by the
Spring community to enable the use of Spring within RSP-UI
applications. Work is ongoing in the Spring community to provide use
of Spring with OSGi by Spring Release version 3.</P>
<P>
</P>
<P><B>Does RSP-UI enable my classic web
applications to run in RCP without any changes?</B></P>
<P>Running of arbitrary web applications
within RCP is already supported by the Tomcat plug-in, which is also
used by the Eclipse help system. RSP-UI may optionally explore
embedding RSP-UI-integrated components as individual UI contributions
within RCP applications. This might be done via the RCP Browser
widget and a pass-through bridge in an augmented version of the
Tomcat plug-in, which accesses the RCP application extension points.
WSRP (Web Services for Remote Portlets) concepts would be studied for
this. This item is optional since it may be too ambitious for this
project.</P>

<P>
</P>
<P><B>Will RSP-UI require a J2EE-certified
server or a Servlet Engine to run?</B></P>
<P>No. An initial implementation will use
the Equinox ServletBridge which runs in a servlet container as a
WAR-style application. This provides for maximum compatibility, as
this approach allows adding Eclipse-/OSGi components to existing web
applications, without having to migrate the entire web application to
Eclipse/OSGi. However, RSP-UI aims to be &quot;server-agnostic&quot;;
it should be able to run in any OSGi-compliant environment. Given
that an OSGi-&quot;server&quot; can itself be composed with bundles,
and OSGi class-loading isolation features ensure that different
implementations of services can co-exist in the same OSGi
environment, as a general rule, RSP-UI does not depend on particular
OSGi container features beyond those specified by applicable OSGi
release levels. 
</P>
<P>
</P>
<P><B>Is the mission of RSP-UI to bridge
the gap between OSGi and all of JavaEE?</B></P>
<P>No. However, RSP certainly has the
potential to go beyond web applications, and include other parts of
JavaEE, such as Enterprise JavaBeans, applying the same benefits to
EARs as WARs. RSP-UI is aware of interest in running relevant JavaEE
features under OSGi, and time will tell in what form and by whom
those interests may be addressed. As more and more of the Java
development spectrum (JavaME-to-JavaEE) is brought into the modular,
extensible, RCP-style of definition and architecture, the longer
Java's lifespan will be. Should any questions of project scope arise
during the project, RSP-UI will act in accordance with the Eclipse
Development Process.</P>

<P>
</P>
<P><B>Will RSP-UI make web development
simpler?</B></P>
<P>We believe so. There is pent-up demand
for solutions that are simpler to implement than by playing the full
JavaEE orchestra. Wicket is one rendering technology approach that
helps for simplicity in UI development. The key theme of RSP-UI is
easy composition of smart technologies.</P>
<P>
</P>
<P><B>Is RSP-UI too ambitious? It sounds
ambitious.</B></P>
<P>Putting everything imaginable under the
heading of RSP into one project would be too ambitious. This is why
the RSP-UI project narrowly focuses on UI integrations, and provides
a framework, with sample implementations only, whereby
ContentProviders can use their technology of choice. It deals with
the complexity of the server-side world by providing uniform
integration approaches, in hindsight of and knowledge of existing
technologies. All project deliverables are assigned to definitive
committers.</P>
<P>
</P>
<P>RSP-UI does not intend to create a
server-side component framework beyond what Eclipse/OSGi already
provides. Further, RSP-UI applies existing concepts such as workbench
API as well as established server-side approaches. The RSP-UI project
aims to support a major strengh of the Eclipse platform: its capacity
for leveraging cross-technology integration. 
</P>
<P>

</P>
<P>Creating a UI composition framework
that integrates different URL resources is feasible; RSP-UI committer
Cedric Dumoulin has previously done this (Tiles). Running established
web frameworks under OSGi is feasible; RSP-UI committers from OPS4J
and Infonoia have done this. Composing web UI using Eclipse workbench
API paradigms is feasible; RSP-UI committers from Vetta Technologies
are doing this. Providing PDE build, run and deployment support is
feasible; RSP-UI committer Gunnar Wagenknecht has PDE experience.
RSP-UI brings these kinds of expertise together in a
multidisciplinary team for a successful project.</P>
<P>
</P>
<P>To avoid scope creep, we will be very
happy to spin off new, independent projects for scoped items that may
develop their own dimension, such as, but not limited to, scripting.
For example, it may be useful to extend scripting integration to
JavaScript, Groovy or Rails runtimes, within another project.</P>
<P>
</P>
<P><B>Does RSP-UI dilute Eclipse API?</B></P>
<P>No. By providing uniform integration
approaches, we actually help avoid API dilution. Uniform integration
approaches are very important when ContentProviders can use their
technology of choice. At the same time, RSP-UI follows the 2006
Eclipse Roadmap, which states that &quot;Support for compiled
languages, scripting languages and &quot;Java like&quot; languages
such as JSP, SQLJ and the like are all related areas where work needs
to be done&quot;. 

</P>
<P>
</P>
<H2>Organization</H2>
<P><B>Project lead</B></P>
<P>
</P>
<P>Wolfgang Gehner (Infonoia, Switzerland,
wgehner@infonoia.com)</P>
<P>
</P>
<P><B>Initial committers</B> 
</P>
<P>

</P>
<UL>
	<LI><P>Cedric Dumoulin (Universit&eacute;
	des Sciences et Technologies de Lille, France, original creator of
	Tiles, co-author of &quot;Struts in Action&quot;): Apache Tiles
	concepts</P>
	<LI><P>John R. Fallows (TXE Systems, USA,
	co-author of &quot;Pro JSF and Ajax&quot;): JSP  integrations</P>
	<LI><P>Wilson Freitas (Vetta
	Technologies, Brazil): Web workbench and scripting languages (such
	as PHP) enablement</P>

	<LI><P>Wolfgang Gehner (Infonoia,
	Switzerland, co-author of &quot;Struts Best Practices&quot;): Java
	web applications and frameworks, Struts enablement</P>
	<LI><P>Ricardo Giacomin (Vetta
	Technologies, Brazil): Web workbench and PHP enablement</P>
	<LI><P>Niclas Hedhman (OPS4J, Malaysia,
	JSR 291 Expert Group member): Wicket enablement, Flash client
	support &amp; various OSGi support services</P>
	<LI><P>Makas Lau (OPS4J, Malaysia):
	Wicket enablement</P>
	<LI><P>Peter Neubauer (OPS4J, Sweden):
	Wicket enablement, Remote OSGi management console</P>

	<LI><P>Gunnar Wagenknecht (Truition,
	Canada): Basic build, run and deployment support similar to and
	based on PDE and PDE Build<BR><BR>
	</P>
</UL>
<P><B>Code contributions</B></P>
<P>
</P>
<P>Project participants will make initial
code contributions that will encompass some of the core functionality
for RSP-UI:</P>
<P>
</P>
<P>Infonoia will make an initial code
contribution as follows:</P>
<P>

</P>
<UL>
	<LI><P>Integration facilities with the
	Apache Struts Framework</P>
	<LI><P>Command-driven program control</P>
	<LI><P>A mechanism for maintaining
	browser state across requests, allowing &ldquo;bookmarkable URLs&rdquo;
	and browser back-button functionality, required for common web
	applications and portals</P>
</UL>
<P>

</P>
<P>OPS4J will make an initial code
contribution as follows:</P>
<UL>
	<LI><P>Wicket support</P>
	<LI><P>Unified Jakarta Commons Logging
	and Log4J support for external libraries/frameworks that will be
	wrapped as OSGi services</P>
	<LI><P>Remote OSGi management console</P>
</UL>
<P>
</P>
<P>TXE Systems will make an initial code
contribution as follows:</P>

<UL>
	<LI><P>Integration facilities with JSP
	and a Servlet 2.3-compatible OSGi service and web application bridge</P>
</UL>
<P>
</P>
<P>Vetta Technologies will make an initial
code contribution as follows:</P>
<UL>
	<LI><P>Web workbench UI composition
	modules</P>
	<LI><P>Integration facilities with
	scripting languages (PHP as an example)</P>
</UL>

<P>
</P>
<P><B>Further support has been expressed
by:</B></P>
<P>
</P>
<UL>
	<LI><P>Alex Blewitt</P>
	<LI><P>Max Carlson (IDE for Laszlo)</P>
	<LI><P>Grahame Grieve (OHF) and Eishay
	Smith (OHF), OHF is potential consumer</P>
	<LI><P>Russell Hamm (Mayo Clinic via OHF
	subproject), potential consumer</P>

	<LI><P>Martin Lippert (it-agile),
	potential consumer</P>
	<LI><P>Simon Kaegi (Cognos)</P>
	<LI><P>Scott Lewis (ECF), potential
	consumer</P>
	<LI><P>Ed Warnicke (Cisco)<BR><BR>
	</P>
</UL>
<P>The more input the project receives,
the more cross-integration it can provide. We strongly encourage you
to contribute your ideas, vision, know-how and skills in an area that
may fall within the scope of this project.</P>

<P>
</P>
<H2>Timing</H2>
<UL>
	<LI><P>June 2006: Eclipse 3.2/Callisto
	release slated to already support deploying plug-ins to a servlet
	container</P>
	<LI><P>July 2006: Demo application
	integrating JSP/JSTL, Struts and Wicket</P>
	<LI><P>October 2006: Initial API draft</P>
	<LI><P>May 2007: Version 1.0 API and
	sample implementations</P>

</UL>
<P>
</P>

      </div>
    </div>
   
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
