<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1>Enterprise Modules Project (Gemini) Proposal</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Enterprise Modules (Gemini)");
?>

<p>The Enterprise Modules Project is a proposed open source project
under the <a
	href="http://www.eclipse.org/projects/project_summary.php?projectid=rt">Eclipse
Runtime Project</a>.</p>

<p>This proposal is in the Project Proposal Phase (as defined in the
Eclipse Development Process) and is written to declare its intent and
scope. We solicit additional participation and input from the Eclipse
community. Please send all feedback to the <a
	href="http://www.eclipse.org/forums/eclipse.gemini">Gemini</a> Eclipse Forum or <a
	href="news://news.eclipse.org/eclipse.gemini">eclipse.gemini</a> newsgroup.</p>

<h2>Introduction</h2>

<p>This proposal recommends the creation of a new project called
&quot;Enterprise Modules&quot;, nicknamed Gemini, to provide a home for
subprojects that integrate existing Java enterprise technologies into
module-based platforms, and/or that implement enterprise specifications
on module-based platforms. Gemini will be a parent ("container project"
as defined by the Eclipse Development Process) for several subprojects
("operating projects" as defined by the Eclipse Development Process)
that provide the specific implementations/integrations. Gemini will
itself be a subproject of the Eclipse Runtime Project and will strive to
leverage functionality of existing projects. We encourage and request
additional participation and input from any and all interested parties
in the Eclipse community.</p>

<h2>Background</h2>

<p>Enterprise applications are often complex, involving multiple layers
and many actors. Even though every application has a different purpose
and usage, there are a number of features and behavioral requirements in
the underlying infrastructure that are common among many of those
seemingly dissimilar applications. For a long time, not only did
developers need to create the vertical business logic of the
application, but they also had to create the platform plumbing on which
the application could run. Enterprise Java standards made great strides
in defining specifications describing how these layers could be
implemented, exported and used. They have since become the assumed and
ubiquitous underpinnings of the majority of enterprise Java systems.</p>

<p>OSGi started as a technology to enable embedded devices and
appliances to operate and communicate through services in dynamic
environments. The devices could come online and offline, were decoupled
from each other, and had independent life cycles. The framework that
emerged to host and support these features turned out to be beneficial
to other applications and software layers as well. Recently, OSGi and
the module-based design principles that it espouses and promotes, have
begun gaining popularity amongst enterprise developers as well. The
natural evolution was to start creating standards for integrating
popular enterprise technologies in module-based systems, and then
provide implementations for consumption by the general population.</p>

<h2>Scope</h2>

<p>The scope of the Gemini project is two-fold:</p>
<ul>
	<li>Integration of existing Java enterprise technologies into
	module-based platforms; and</li>
	<li>Implementation of enterprise specifications for module-based
	platforms</li>
</ul>

<p>The project focuses on standards developed by the OSGi
Enterprise Expert Group. It may later extend to include technology and
standards for other module-based systems. Modularity within a
standardised component framework is the key binding principle of Gemini
projects.</p>

<p>The project is not concerned with creating new enterprise standards,
nor with creating a new variety of full-featured enterprise container.</p>

<h2>Description</h2>

<p>The primary goal of the Gemini project is to provide access to
standard enterprise technology implementations within a modular
framework. The OSGi Alliance has developed specifications for the
application and usage of many of the enterprise technologies within
OSGi. These specifications describe how vendors should implement and
interoperate with existing services, and how the OSGi modularity, life
cycle, and service models should be applied with respect to those
technologies. Gemini will provide implementations of many of these
specifications, including:</p>

<ul>
	<li>RFC 66 &mdash; Web Container</li>
	<li>RFC 98 &mdash; Transactions</li>
	<li>RFC 122 &mdash; Database Access</li>
	<li>RFC 124 &mdash; Blueprint Services</li>
	<li>RFC 139 &mdash; JMX Integration</li>
	<li>RFC 142 &mdash; JNDI Integration</li>
	<li>RFC 143 &mdash; JPA Integration</li>
	<li>RFC 146 &mdash; JCA Connector Integration</li>
</ul>

<p>Each of the specifications will be hosted as a separate operating
project within the Gemini parent project. The scope for each of these
projects is limited to providing an implementation of the corresponding
specification and integration with related technology. Each operating
project will have its own separate leadership, committers, build
process, release schedule, developer mailing list, and community
newsgroups/forums. Subprojects may individually opt to participate in
the annual release train. They may be released separately or together,
and will for the most part be executable individually or as part of a
group.</p>

<p>Each subproject may evolve or be developed as the community, and
those involved with the project, see fit. However, each will continue to
share the Gemini prime directive -- the provision of existing
enterprise-level standards on a modular framework.</p>

<h2>Requirements</h2>

<p>The project will deliver standalone implementations of the RFCs where
possible. In most cases the implementation will be the actual OSGi
Reference Implementation for the specification it implements.</p>

<p>The implementations will be consumable as &quot;modules&quot; (or
OSGi bundles), installable on one or more frameworks. They will be
largely independent of each other, or they may be combined to provide a
suite of services for use in an existing enterprise platform.

<p>Project teams will strive to openly and transparently collaborate
with other Eclipse projects doing related work. Where overlap between
projects occur, Gemini subproject committers will work to either (a)
resolve the overlap to mutual benefit, or (b) make clear for the
community the distinctions between the corresponding projects. Gemini
subprojects will leverage existing Eclipse technology as appropriate.</p>

<h2>Committers</h2>


<p>Multiple companies have agreed to be cooperative committers on this
project at the outset. These companies are agreeing to initially
contribute IP and continue to add to and maintain this IP as the project
develops. The companies that have signed up to commit are:</p>

<ul>
	<li>Oracle</li>
	<li>SpringSource (VMware)</li>
</ul>

<p>The following individuals are committers to the project:</p>

<ul>
	<li>Adrian Colyer &mdash; SpringSource</li>

	<li>Andy Piper &mdash; Oracle</li>

	<li>Andy Wilkinson &mdash; SpringSource</li>

	<li>Ben Hale &mdash; SpringSource</li>

	<li>Bob Nettleton &mdash; Oracle</li>

	<li>Bryan Atsatt &mdash; Oracle</li>

	<li>Christopher Frost &mdash; SpringSource</li>

	<li>Costin Leau &mdash; SpringSource</li>

	<li>Doug Clarke &mdash; Oracle</li>

	<li>Glyn Normington &mdash; SpringSource</li>

	<li>Gordon Yorke &mdash; Oracle</li>

	<li>Hal Hildebrand &mdash; Oracle</li>

	<li>J.J. Snyder &mdash; Oracle</li>

	<li>Jeff Trent &mdash; Oracle</li>

	<li>Mike Keith &mdash; Oracle (Project lead)</li>

	<li>Peter Krogh &mdash; Oracle</li>

	<li>Rob Harrop &mdash; SpringSource</li>

	<li>Shaun Smith &mdash; Oracle</li>

	<li>Steve Powell &mdash; SpringSource</li>

	<li>Tom Ware &mdash; Oracle</li>
</ul>

<h2>Initial Contributions</h2>

<p>The subprojects will be created with the following initial IP
components and donating companies:</p>

<ul>
	<li>Blueprint Service Implementation &mdash; SpringSource</li>
	<li>Web Container Integration code &mdash; SpringSource</li>
	<li>JPA Integration code for EclipseLink &mdash; Oracle</li>
	<li>Implementation of JMX Mbeans and composite data types &mdash;
	Oracle</li>
	<li>Derby JDBC Service Implementation &mdash; Oracle</li>
	<li>JNDI Service Integration code &mdash; Oracle</li>
</ul>

<h2>Mentors</h2>

<p>The following Architecture Council members will mentor this project.</p>

<ul>
	<li>Wayne Beaton</li>
	<li>Adrian Colyer</li>
	<li>Doug Clarke</li>
</ul>

<h2>Interested Parties</h2>

<p>The following projects or companies have expressed an interest in
this project:</p>

<ul>
	<li>EclipseLink</li>

	<li>EclipseSource</li>
	
	<li>Eclipse Communication Framework (ECF)</li>

	<li>Equinox</li>
	
	<li>Swordfish</li>

	<li>SAP</li>
	
	<li>Red Hat</li>
	
	<li>SOPERA</li>
	
	<li>Infor</li>
	
	<li>Werner Keil</li>
</ul>

<p>We also believe that there will be some opportunities to collaborate
with the Jetty project.</p>

<h2>Project Scheduling</h2>

<p>The first milestone will be to get the IP code contributions accepted
and approved and the project provisioned. Once it is active, we expect
the project to progress and evolve according to the expectations of its
developers and users. The release schedule will align with the
development and availability of the coincident OSGi specifications.</p>

<h2>Changes to this document</h2>

<p>23-Nov-2009: Changed wording (but not meaning) in the scope section.</p>

</div>
</div>
<?php
$html = ob_get_contents();
ob_end_clean();

# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
