﻿<!-- 
	This document is provided as a template along with some guidance for creating
	your project proposal. This is just a template. Feel free to change it as
	you see fit (add sections, remove section). We feel, however, that the
	suggestions represented in this document represent the reasonable minimum
	amount of information to move forward.
	
	Please keep the formatting in this document simple. Please do not edit 
	this document in Microsoft Word as it adds huge piles of markup that make
	it difficult to restyle.
	
	More information is available here:
	
	http://wiki.eclipse.org/Development_Resources/HOWTO/Pre-Proposal_Phase
	
	Direct any questions about this template to emo@eclipse.org
 -->

<html>
<head>

<!-- 
	Include the title here. We will parse it out of here and include it on the
	rendered webpage. Do not duplicate the title within the text of your page.
 -->

<title>Eclipse Process Manager (Stardust)</title>
</head>

<!-- 
	We make use of the 'classic' HTML Definition List (dl) tag to specify
	committers. I know... you haven't seen this tag in a long while...
 -->
 
<style>
dt {
display: list-item;
list-style-position:outside;
list-style-image:url(/eclipse.org-common/themes/Phoenix/images/arrow.gif);
margin-left:16px;
}
dd {
margin-left:25px;
margin-bottom:5px;
}
</style>

<body>
<p>The Eclipse Process Manager (Stardust) project is a proposed open source 
project under the
<a href="http://www.eclipse.org/projects/project_summary.php?projectid=soa">
Service Oriented Architecture Top-Level Project</a>.<sup><a href="#ftn1">1</a></sup></p>
<p>This proposal is in the Project Proposal Phase (as defined in the Eclipse 
Development Process) and is written to declare its intent and scope. We solicit 
additional participation and input from the Eclipse community. Please send all 
feedback to the <a href="http://www.eclipse.org/forums/eclipse.proposals">
Eclipse Proposals</a> Forum. </p>

<h2>Background</h2>
<!-- 
	Optionally provide the background that has lead you to creating this project.
 -->

<p>Business Process Management (BPM) has its roots in: </p>
<ul>
				<li>Enterprise Application Integration,</li>
				<li>Human-centric workflow,</li>
				<li>Document Management and</li>
				<li>Business Intelligence. </li>
</ul>
<p>BPM combines these concepts and has an important role – driven by the need 
for optimization of business processes and the demands to increase enterprise 
agility. Originally, many products had offered just parts of the required 
functionality e.g. business process modeling. Today, Business Process Management 
Suites (BPMS) support the whole process lifecycle of process modeling, 
simulation, execution and monitoring. </p>
<p>Some products run standalone - especially those focused on smaller and medium 
enterprises (SME) – but the majority are running interwoven with other 
enterprise middleware products such as Application Servers. Several software 
vendors have tied their BPM offerings closely with their entire portfolio of 
middleware products, thus forcing the customer to use the whole technology stack 
from this vendor. </p>
<p>In the last couple of years, the BPM market has been consolidating, mainly 
through acquisitions of smaller BPMS providers by larger companies and smaller 
companies vanishing. However, no open source product has been really established 
in the market.</p>
<p>We think the time has come for a BPMS project under the stewardship of the 
Eclipse Foundation to do what its IDE already has achieved for software 
development: a common platform accepted by multiple vendors on which a large 
variety of more specialized products and solutions can be based upon.</p>
<p>SunGard’s Infinity Process Platform (IPP), which has evolved from the 
acquisition of the German BPMS provider CARNOT AG by SunGard in 2006, is a 
mature BPMS which has been developed for more than 10 years. It has a proven 
track record in the industry with more than 1,500 customers on all 5 continents 
and is embedded in more than 25 SunGard solutions in mission critical, partially 
high throughput environments. IPP was ranked in Gartner&#39;s BPMS Magic Quadrant in 
the 2007 as #2 in Vision.</p>
<p>IPP has been relying on Eclipse technology for many years and is integrated 
with the Eclipse environment wherever possible. Hence, Eclipse is the natural 
home for the product and SunGard is willing to contribute the current version of 
IPP into an Eclipse project and to help maintaining it further in the Eclipse 
community.</p>
<p>As IPP provides not just a variety of tools but a fully integrated platform 
for the whole lifecycle of managing business processes, we propose to name it 
Eclipse Process Manager, nickname Stardust and acronym EPM. As BPM is an 
essential part of every SOA the EPM project should be a subproject under the 
umbrella of the SOA top-level project.</p>

<h2>Scope</h2>
<!-- 
	All projects must have a well-defined scope. Describe, concisely, what
	is in-scope and (optionally) what is out-of-scope. An Eclipse project
	cannot have an open-ended scope.
 -->

<p>This project will provide a comprehensive Business Process Management
Suite (BPMS) which covers all phases of the Business Process Lifecycle
including modeling, simulation, execution and monitoring.</p>

<p>The contribution
scope is: </p>

<ul>
 <li>The business process modeling environment
     including human workflow, system integration, organizational and data
     modeling entirely based on Eclipse, facilitated by the <b>Process
     Workbench</b></li>
 <li>The Runtime Environment including the Process
     Engine and the browser-based End User Process Portal entirely based on Web
     standards (J2EE, JSF) as well as planned adoption for OSGi services</li>
 <li>The rapid application development environment
     embedded in Eclipse</li>
 <li>The simulation feature</li>
 <li>The interfaces to external systems (e.g.
     via Web Services or messaging), Document Repositories, Scanning Engines
     and Business Rules Management Systems</li>
</ul>

<p>Not in scope are:</p>

<ul>
 <li>The Reporting Engine, which is handled by
     the Eclipse BIRT project</li>
 <li>Document Repositories, Scanning Engines
     and Business Rules Management Systems</li>
 <li>Application Servers and Database Management
     Systems being Runtime Environments for the Process Engine and the Process
     Portal and especially not</li>
 <li>Any business process models for an
     arbitrary industry</li>
</ul>

<p>These contributions are either
complementary components to a BPMS or value add by industry providers in their
respective application domains.</p>
<!-- 
	Describe the project here. Be concise, but provide enough information that
	somebody who doesn't already know very much about your project idea or domain
	has at least a fighting chance of understanding its purpose.
 -->
 
<h2>Description</h2>

<h3>Process Workbench</h3>

<p>The contribution includes
the <b>Process Workbench</b>, an integrated process modeling and development
environment offering: </p>

<ul>
 <li>Modeling of processes in the <b>Business
     Process Modeling Notation</b> (BPMN),</li>
 <li>Perspectives for process modeling
     including an <b>Analyst and a Development Perspective</b>,</li>
 <li>Importing and exporting functionality of
     process models in the <b>XML Process Definition Language</b> (XPDL) and</li>
 <li>A <b>Rapid Application Development Environment</b>
     within the Eclipse Workbench, which allows to execute process models on an
     embedded <b>Application Server</b> (Tomcat) using an embedded <b>DBMS</b>
     (Apache Derby).</li>
</ul>

<p>The Process Workbench is based on the
Eclipse Modeling Project/Graphical Modeling Project and displays the genuine <b>Eclipse
Look &amp; Feel</b> and leverages various other Eclipse components and concepts
such as:</p>

<ul>
				<li>Project setup and management,</li>
				<li>Java classpath management and class resolution,</li>
				<li>Validation,</li>
				<li>Cross-referencing,</li>
				<li>Debugging and</li>
				<li>WTP integration for Rapid Application Development.</li>
</ul>

<p>Individuals proficient with Eclipse will know how to use
the Stardust
Process Workbench immediately. Comprehensive documentation for the whole suite
is available via the <b>Eclipse Help System</b>.</p>

<p>As the whole Stardust Process Workbench is implemented
using the Eclipse Features/Plug-ins, the ability to install and update the Process
Workbench via the <b>Eclipse Update Mechanism</b>. </p>

<p>The modeler (as well as the Runtime
Environment) the can be extended by customers and partners via numerous Eclipse<b>
Extension Points </b>e.g. to </p>
<ul>
				<li>support new application or data integration technologies,</li>
				<li>add new message formats (e.g. SWIFT, FIX) and corresponding 
				processing functionality,</li>
				<li>bind additional, use-case specific information to a process 
				model,</li>
				<li>add external process model data sources (e.g.SOA Registries)</li>
				<li>and others. </li>
</ul>
<p>Support for <b>version control and
concurrent modeling</b> via SVN is also provided.</p>

<p>The Process Workbench, consisting of a set
of Eclipse Plugins/Features, is fully <b>OSGi</b> enabled by design.</p>

<h3>Monitoring and
Reporting</h3>

<p>Advanced functionality for monitoring and analysis
of process and related data is based on the Eclipse <b>Business Intelligence
and Reporting Tool</b> (BIRT). It uses BPM specific data sources from the BPM
model and BPM Audit Trail Database (Process Instances, Activity Instances, Process
Data) and can be joined with arbitrary BIRT Data Sources for rich, use case
specific reports.</p>


<h3>Simulation
Feature</h3>

<p>The Stardust Process Workbench also
contains a <b>Simulation Feature</b> which allows to simulate modeled processes
without deploying them to a Runtime Environment by running a fast, in-memory
algorithm performing thousands of processes for a simulation period of possibly
years in seconds. This way critical paths or resource bottlenecks can be
identified and general behavior of processes (e.g. overall duration) can be
predicted. Simulation results can be displayed graphically and copied into
spreadsheets or written to a database.</p>


<h3>Process Runtime
Environment</h3>

<p>The contribution furthermore comprises a <b>Runtime
Environment</b> that:</p>

<ul>
 <li>supports EJB and Spring for a managed JEE
     environment,</li>
 <li>runs on all common application servers,</li>
 <li>can use most public DBMS for the audit
     trail,</li>
 <li>has an interface to many DMS via JCR and</li>
 <li>provides support for other widespread
     enterprise technologies like JTA, JMS, JDBC, JCA and EJB</li>
</ul>

<p>as well as a comprehensive end user portal
supporting:</p>

<ul>
 <li>web-based portals with support for modern
     Web technologies like AJAX as well as portals based on Java Swing with</li>
 <li>three perspectives</li>
 <ul>
  <li>the Workflow Execution Perspective for
      end users,</li>
  <li>the Business Control Center Perspective providing
      business monitoring and management capabilities and </li>
  <li>the Administration Perspective for administration
      tasks</li>
 </ul>
</ul>

<p>and finally a client for document scanning.</p>

<p>The Process Runtime Environment is designed
with a modular architecture, heavily using various Java Interface based
extension points. Given this foundation it can be expected that the Process
Runtime Environment can be adapted to native use of OSGi services in a
straightforward way.</p>

<h3>Architectural Overview</h3>

<p>The following diagram provides an overview of the Stardust
architecture as of today:</p>
<p><img src="EPM-architecture-1.png" width="100%"></p>
<p>The architecture of the Simulation Feature is shown in the next diagram: </p>
<p><img src="EPM-architecture-2.png" width="100%"></p>
<p>The next diagram shows the integration of Stardust and BIRT:</p>
<p><img src="EPM-architecture-3.png" width="100%"></p>

<h2>Why Eclipse?</h2>
<!-- 
	Answer these two questions:	What value does this project bring to the Eclipse
	community? What value do you expect to obtain from hosting your project at Eclipse?
	
	What value do you get by having your project at Eclipse over and above the value
	of hosting at Eclipse Labs?
 -->

<p>Infinity Process Platform has leveraged Eclipse technology since
inception (10 years) and seamlessly integrated with Eclipse technology and
concepts. It is a BPMS built on Eclipse technologies and concepts.</p>

<p>Although Eclipse is adding more and more SOA and Runtime
capabilities to its portfolio, a comprehensive BPMS is not yet available from
Eclipse – nor does an established BPMS exist in the open source market in
general. The Eclipse Process Manager (Stardust) project can close this gap.</p>

<h2>Initial Contribution</h2>
<!-- 
	Projects are expected to arrive at Eclipse with existing code.
	
	Describe the existing code that will be contributed to the project. Please provide
	a couple of paragraphs describing the code with modest detail, including important
	information like code ownership (who holds the copyright?), and some consideration
	of community that exists around the code. Include a listing of third-party libraries 
	and associated licenses.
 -->
 

<p>The initial
contribution by SunGard will be based on the existing Infinity Process Platform
product as of today into Stardust. It consists of code for:</p>

<ul>
 <li>the Process Engine Runtime Environment,</li>
 <li>the Process Workbench,</li>
 <li>the Reporting Facility,</li>
 <li>the Simulation Feature and</li>
 <li>the Process Portal</li>
</ul>

<p>as well as comprehensive documentation of
the whole suite.</p>

<p>More than a decade of product development, a proven track
record and the knowledge base of the SunGard internal community<sup><a href="#ftn2"
name="_ftnref2" title="">2</a></sup> can be combined with the
mature and established community processes and the market footprint of Eclipse.
SunGard is dedicated to help transition the project into successful community
work.</p>

<p>All IPP code – except used open source libraries – currently
is owned by SunGard and licensed under a proprietary license. The complete code
for the <em>Stardust</em>
project will be relicensed under the EPL as part of the contribution to
Eclipse.</p>

<h2>Legal Issues</h2>

<p>IPP as of today requires libraries for Hibernate and XOM, distributed
under the LGPL. Some other libraries under LGPL are needed for non-core
functionality.</p>

<p>Hibernate will be required by users of Stardust if users
intend to use Hibernate to persist process data. For XOM other libraries accepted
by the Eclipse Foundation will be considered.</p>

<p>There are no dependencies on code under GPL.</p>

<!-- 
	Please describe any potential legal issues in this section. Does somebody else
	own the trademark to the project name? Is there some issue that prevents you
	from licensing the project under the Eclipse Public License? Are parts of the 
	code available under some other license? Are there any LGPL/GPL bits that you
	absolutely require?
 -->
 
<h2>Relationship to other projects</h2>
<h3>Service Oriented Architecture (Container)</h3>
<p>The goal of this top-level project is to deliver a packaged open source SOA 
platform solution. This includes a comprehensive SOA runtime environment and a 
suite of complementary tools and tool frameworks designed to address the needs 
of architects, developers and deployers of SOA solutions. It is the aim of the 
project to be the natural and happy home of all future SOA-related projects 
within Eclipse. Thus the Stardust project fits very nice in the Service Oriented 
Architecture container.</p>
<p>Within the SOA top level project, the Stardust project has&nbsp;links to most of 
the other projects. </p>
<ul>
				<li><strong>eBPM</strong> offers a significantly smaller subset 
				of the Stardust functionality. Its sponsor, Spagi has already 
				declared support for the Stardust project. <strong>eBAM</strong> 
				from the same sponsor could be adapted to work with the Stardust 
				project.</li>
				<li><strong>BPMN Modeler</strong> is a submission by Intalio 
				providing a modeler for the Business Process Management Notation 
				(BPMN) which Stardust is using as well. The BPMN modeler seems 
				to be almost inactive.</li>
				<li>The <strong>SCA Tools</strong> project and the <strong>
				Mangrove</strong> project complement Stardust. <strong>Swordfish</strong> 
				is an OSGi-based SOA framework that builds on Apache ServiceMix 
				and Apache CXF. It is designed to be complemented by additional 
				open source components such as a service registry, a messaging 
				system, a process engine, etc. to form a comprehensive open 
				source SOA runtime environment based on both established and 
				emerging standards. Stardust would significantly contribute to this 
				goal.</li>
				<li><strong>Java Workflow Tooling</strong> targets the same 
				domain as Stardust. In some aspects (the Workflow Editor) it is 
				very similar; in others it is more abstract, e.g. it only 
				supports a generic workflow engine administration API, but does 
				not deliver a concrete implementation of a process engine. By 
				contrast Stardust is a complete BPMS with components for the 
				whole process lifecycle in commercial operation for more than a 
				decade. Details on how the two projects would coexist need to be 
				evaluated, but Java Workflow Tooling could profit from many 
				components that Stardust delivers.</li>
				<li><strong>Business Intelligence and Reporting Tools (BIRT)</strong> 
				The Stardust Process Workbench uses the Business Intelligence 
				and Reporting Tools as base for its analysis and reporting 
				functionality. There is a close collaboration with the project 
				and the main committer, Actuate. Actuate is listed as an 
				Interested Party in the present proposal.</li>
				<li><strong>Eclipse Modeling Project</strong> The Stardust 
				Process Workbench makes intensive use of several components of 
				the Eclipse Modeling Project. Thus improvements of these 
				underlying modules will have a straight impact on the Process 
				Workbench.</li>
</ul>
<h2>Committers</h2>

<!-- 
	List any initial committers that should be provisioned along with the
	new project. Include affiliation, but do not include email addresses at
	this point.
 -->

	<p>The following individuals are proposed as initial committers to
		the project; these are all developers and enabling team for the
		current SunGard product IPP.</p>
	<ul>
		<li>Stephan Born, SunGard</li>
		<li>Monika Funke, SunGard</li>
		<li>Shrikant Gangal, SunGard</li>
		<li>Marc Gille, SunGard, Lead</li>
		<li>Subodh Godbole, SunGard</li>
		<li>Barry Grotjahn, SunGard</li>
		<li>Florin Herinean, SunGard</li>
		<li>Yogesh Manware, SunGard</li>
		<li>Vikas Mishra, SunGard</li>
		<li>Andreas Müller, SunGard</li>
		<li>Anoop Nair, SunGard</li>
		<li>Herbert Neureiter, SunGard</li>
		<li>Vikash Pandey, SunGard</li>
		<li>Ankita Patel, SunGard</li>
		<li>Rainer Pielmann, SunGard</li>
		<li>Holger Prause, SunGard</li>
		<li>Sven Rottstock, SunGard</li>
		<li>Robert Sauer, SunGard</li>
		<li>Christian Schenzel, SunGard</li>
		<li>Roland Stamm, SunGard</li>
		<li>Nicolas Werlein, SunGard</li>
	</ul>

	<!-- 
	Describe any initial contributions of code that will be brought to the 
	project. If there is no existing code, just remove this section.
 -->

<h2>Mentors</h2>

<!-- 
	New Eclipse projects require a minimum of two mentors from the Architecture
	Council. You need to identify two mentors before the project is created. The
	proposal can be posted before this section is filled in (it's a little easier
	to find a mentor when the proposal itself is public).
 -->

<p>The following Architecture Council members will mentor this
project:</p>

<ul>
 <li>Chris Aniszczyk, <i>Red Hat</i></li>
 <li>Adrian Mos, <i>Xerox</i></li>
</ul>

<h2>Interested Parties</h2>

<!-- 
	Provide a list of individuals, organisations, companies, and other Eclipse
	projects that are interested in this project. This list will provide some
	insight into who your project's community will ultimately include. Where
	possible, include affiliations. Do not include email addresses.
 -->

<p>The following individuals, organisations, companies and projects have 
expressed interest in this project:</p>

<ul>
 <li>SAP - Patrick Schmidt, Development
     Manager, Netweaver BPM</li>
 <li>BIRT/Actuate - Mark Coggins, SVP Engineering</li>
 <li>Spagi - Antonio Majori, Senior Technical
     Manager</li>
 <li>Nuxeo – Eric Barroca, Founder and CEO</li>
 <li>Adrian Mos, SOA PMC</li>
</ul>

<h2>Project Scheduling</h2>
<!-- 
	Describe, in rough terms, what the basic scheduling of the project will
	be. You might, for example, include an indication of when an initial contribution
	should be expected, when your first build will be ready, etc. Exact
	dates are not required.
 -->

<ul>
 <li>April 2011 - proposal</li>
 <li>May 2011 - creation review</li>
 <li>June 2011 - initial contribution based on
     version 6.0 of the donor project. </li>
 <li>Beginning of Q3 2011 - approval of the
     code and other IP contributions and provisioning of the projects on
     Eclipse.org infrastructure.</li>
 <li>December 2011 - synchronization with the
     milestone release train of the successors to Eclipse 3.7/4.1</li>
</ul>
<h3>Footnotes</h3>
<p><a name="ftn1">1</a>The
goal of the Eclipse SOA Platform Project is to deliver a packaged 
Open-Source SOA Platform solution. This includes a comprehensive SOA runtime 
environment and a suite of complementary tools and tool frameworks, designed to 
address the needs of architects, developers and deployers of SOA solutions. It 
is the aim of the project to be the natural and happy home of all future 
SOA-related projects within Eclipse. </p>
<p><a name="ftn2">2</a> SunGard’s adoption of IPP has followed an internal open source model, 
resulting in a community spread across geographies, product lines and customers </p>


<h2>Changes to this Document</h2>

<!-- 
	List any changes that have occurred in the document here.
	You only need to document changes that have occurred after the document
	has been posted live for the community to view and comment.
 -->

<table>
	<tr>
		<th>Date</th>
		<th>Change</th>
	</tr>
	<tr>
		<td>04-Mar-2011</td>
		<td>Document created</td>
	</tr>
	<tr>
  <td>
  <p>05-Apr-2011</p>
  </td>
  <td>
  <p>Version to be proposed</p>
  </td>
	<tr>
		<td>08-Apr-2011</td>
		<td>First Architecture Council Mentor added.</td>
	</tr>
	<tr>
		<td>13-May-2011</td>
		<td>Second Architecture Council Mentor added.</td>
	</tr>
</table>
</body>
</html>