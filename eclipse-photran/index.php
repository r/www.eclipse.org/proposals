<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn"><P STYLE="margin-bottom: 0in">&nbsp; 
</P>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Photran");
?>	

            <h1>Photran -- A Fortran Development Tool</h1>
	
</P>
<P>
We are 
<A HREF="/org/documents/Eclipse Development Process 2003_11_09 FINAL.pdf">proposing</A>
that the Eclipse <a href="/technology/">Technology Project Management Committee</a> charter a 
<i>Fortran Development Tool</i> subproject, <a href="http://www.photran.org">Photran</a>, under its aegis.
We are inviting comments and assistance in this prospective enterprise from one and all. 
Please direct any feedback you may be inclined to offer to the
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.photran">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.photran</a> 
newsgroup.
</P>

	
		<h2>Description</h2>
	
	
		
<p>
The Photran subproject's goal will be to cultivate a fully integrated development facility
for the Fortran programming language, with full support for Fortran editing, refactoring,
and debugging, for the Eclipse platform.
</p>
<p>
We have produced two releases. Our first, <i>Hyperion</i>, was based upon CDT 1.2.0 and Eclipse 2.1.3.
Our current release, <i>Eos</i>, was adapted from CDT 2.1.0, and runs atop Eclipse 3.0.1.
We propose that this current release serve as the foundation for our proposed subproject.
This release can currently be found at our interim Photran 
website: <a href="http://www.photran.org">http://www.photran.org</a>.
</p>
<p>
This release supports, to varying degrees, a variety of 
compilers (e.g. Lahey Fortran, Intel Fortran, g77, g95, PGI, Solaris, F), 
under Linux, Windows, Mac OS X 10.3, and Solaris. 
Other platforms supported by CDT 2.1.0 will likely work as well, but have yet to be 
tested.
</p>
<p>
A major focus of this project will be to bring to bear the full force of the broader community
of tool developers, application specialists, testers, and day-to-day Fortran programmers
upon the development and refinement of this tool.
</p>
<p>
Our current release includes:
<ul>
<li>Support for Fortran Development under Linux, Windows, Solaris and Mac OS X
<li>Fortran programming editing support, including Fortran 9x/2003 keyword highlighting
<li>Code browsing and navigation
<li>CVS repository management, courtesy of Eclipse's team support plug-ins
<li>Error parsing support for several different compilers
<li>Support for debugging for compilers that are compatable with <i>gdb</i>
<li>Build support for any Fortran compiler or preprocessor that uses a <i>make</i> tool
</ul>
</p>




	
	
		<h2>
            Status</h2>
	


<p>
Our current plug-ins are the partial culmination of over three years of
research into Fortran development and refactoring tools.
A gallery of screenshots, as well as the latest status information, can be 
found at: <a href="http://www.photran.org">http://www.photran.org</a>.<br>
</p>




<p>
</p>





	
	
		<h2>
            Development Plan</h2>
	


<p>
Our goals up to the point of this submission have been to 
develop a functional feature and plug-in set, under Eclipse 2.1.3, 
that supports Fortran editing, code management and 
debugging, and to demonstrate that 
our parsing infrastructure can 
parse all the Fortran code 
for 
<a href="http://www.ibeam.org">IBEAM</a>, 
a large, massively parallel, high-performance framework. 
These have now been met.
</p>
<p>
We are in the process of formulating a formal development plan. Our plans include:
<ul>
<li>Support of the current level of Photran functionality for  Eclipse 3.x at the earliest possible date (Done 2/4/05!)
<li>Full error parsing support for a wider range of Fortran compilers and tools
<li>Managed build support for Fortran tools
<li>Full support for program outlining and pretty-printing
<li>Extensive support for automated refactoring in Fortran 9x
<li>Fortran 2003 Support 
<li>Collaboration with the CDT Subproject to make the CDT code-base more language neutral (i.e. progress towards a UDT or XDT)
</ul>
</p>


	
	
	
		
	



	
		<h2>
			<p ALIGN="LEFT">
            Interested Parties</h2>
	
	
		
                <p><b>Prospective Interim Leads</b><ul>
                  <li>Brian Foote, UIUC (foote at cs.uiuc.com)</li>
                  <li>Ralph E. Johnson, UIUC (johnson at cs.uiuc.com)</li>
                  </ul>
                <b>Interested Organizations</b><ul>
              <li><a href="http://www.cs.uiuc.edu">Dept. of Computer Science, University of Illinois at Urbana-Champaign</a></li>
              <li><a href="http://www.astro.uiuc.edu">Dept. of Astronomy, University of Illinois at Urbana-Champaign</a></li>
              <li><a href="http://www.ncsa.uiuc.edu">NCSA, University of Illinois at Urbana-Champaign</a></li>
              <li>ASC Flash Center, University of Chicago</li>
	      <LI>Dept. of Astronomy, SUNY Stonybrook</li>
		<LI>Vanderbilt University</li>
		<LI>Northrop-Grumman Corporation</li>
		<li>Flensburger Shiffbau Gesellschaft</li>
		<li>Los Alamos National Laboratory</li>
		<li>The Fortran Company</li>
            </ul>
	<p><b>Potential Committers / Contributors</b></p>
            <ul>
              <li>Brian Foote, UIUC</li>
              <li>Ralph E. Johnson, UIUC</li>
              <li>Spiros Xanthos, UIUC</li>
              <li>Jeffrrey Overbey, UIUC</li>
              <li>Danny Dig, UIUC</li>
              <li>Munawar Hafiz, UIUC</li>
              <li>Dirk Rossow, FSG</li>
              <li>Walt Brainerd, The Fortran Company</li>
              <li>Greg Watson, LANL</li>
              <li>Nathan Debardeleben, LANL</li>
              <li>Craig Rasmussen, LANL</li>
	      <li>Tianchao Li, Technical University of Munich</li>
            </ul>
		
	

<P><BR><BR>
</P>
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
