<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Maya: An incubator project";
$pageKeywords	= "Maya Eclipse Provisioning Packaging Deployment Installation Distribution";
$pageAuthor		= "Timothy Webb";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Proposal for Maya: An incubator project</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Maya");
?>

<h2>Introduction</h2>

<p>The goal of the Maya incubator project is to provide an exemplary application to enable
automated deployment of Eclipse installations by building on services within Equinox and
content exposed via update sites.  The Maya project will take a first step in providing the
provisioning services required to enable organizations to leverage the Eclipse platform for
both internal and external tooling built on top of Eclipse.  Many users within an
organization are unfamiliar with Eclipse; those users leveraging Eclipse software should not
be required to learn the update manager or associated technology.  Maya will promote a
centralized deployment model for Eclipse in contrast to the standard Eclipse update mechanism
which is designed to empower each individual client and by association user.  The platform
will be extensible allowing it to be leveraged in multiple deployment modes beyond those
directly supplied by the project.</p>

<p>Please post comments on the Maya project proposal to <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.maya">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.maya</a>.

<h2>Scope</h2>

<p>The following are the target features for the Maya project.</p>

<ul>
<li><p><b>Lightweight Installation and Deployment</b><br>
Instead of deploying the full Eclipse application or just the RCP, a small native client
will be installed on user systems.  This client will connect to back-end servers hosting
Meta information on Eclipse-based software enabled for use in the organization.  Maya will
automatically setup an Eclipse instance with the correct set of tooling for their profile.
The client also allows any Eclipse component to be upgraded as needed including the base
Eclipse and JRE versions.</p></li>

<li><p><b>Profile-Based Software Sets</b><br>
Within organizations, sets of software need to be configured at a profile-level instead of
relying on each user to perform maintenance of their own setup.  Within Maya, administrators
configure the default software associated with each profile and users are empowered to
tailor additional software in their own user settings.</p></li>

<li><p><b>Software Discovery from Update Sites</b><br>
Instead of requiring Maya to have a separate mechanism for registering software to be exposed
within a deployment, Maya will allow registration of internal or external update sites that
can be scanned for feature information.  Maya will then use this information when presenting
administrators with options for defining or updating profiles.  Extensibility will be
provided to allow other mechanisms of supplying installable software to be distributed within
Maya.</p></li>

<li><p><b>Software Dependency Automation</b><br>
Administrators defining profiles will only need to specify the top-level features desired.
Maya will handle automated dependency resolution ensuring that all required dependencies are
automatically installed along side the explicitly listed dependencies.  Since Maya will have
a list of update sites registered, the automatic dependency inclusion should work for all
software listed within any of the update sites -- avoiding the complexity of the user needing
to locate the source for a given feature or missing dependency.</p></li>

<li><p><b>User and Profile Selection Extensibility</b><br>
Organizations will be able to integrate existing authentication systems to allow users to be
automatically associated with available profiles.  In addition, the selection of profiles
applicable to a user will be extensible allowing deployments to choose what software a given
user is delivered.  In the case where a user has access to multiple profiles, Maya will
allow the user to choose the most applicable profile for their specific needs.</p></li>

<li><p><b>Statistics Collection Extensibility</b><br>
Within many organizations, statistics need to be collected on the software being used.
Maya will be designed to tie in with an organizational statistics monitoring engines allowing
numerous statistics from Maya to be collected including the number of uses of different
software components, the average profile and team membership, and which profiles are being
most heavily used.</p></li>

<li><p><b>Deployment Architecture Extensibility</b><br>
As Maya will serve as a base for more complex offerings, certain extensibility will need to be
built into the platform to allow for alternate deployment architectures.  These architectural hooks
will include services to enable activities such as filtering of software visible to a user, or being
able to hook in client-side to enable additional software to be installed.  Hooks may also be added
to enable creation of services on top of Maya that are technically out of Maya's own scope.
</p></li>

</ul>

<h2>Proposed Components</h2>

<p>It is expected that there will be five deliverables of the Maya project.</p>

<ul>
<li><p><b>Bootstrap Client</b><br>
The first is the bootstrap client that will connect to the backend services and setup the
environment given the user's profile.  By downloading OSGi bundles, the client will be
extremely lightweight and will actually have very little knowledge of how to process a user's
profile until the appropriate bundle is retrieved.  As the bootstrap will be written in
native code, everything from the JRE to the Equinox runtime will be able to be dynamically
setup.</p></li>

<li><p><b>Profile Selection Launcher RCP</b><br>
The launcher is an RCP-based UI responsible for walking the user through authentication and
profile selection.  Upon profile selection, the launcher then constructs the Eclipse
environment.  To optimize client-side performance, the backend service will conduct the
dependency validation allowing the user to choose the desired software and if desired defer
the selection of dependent software to the service.  In certain cases, the launcher stage
can be skipped allowing the bootstrap to directly instantiate the full Eclipse
environment.</p></li>

<li><p><b>Backend Web Service</b><br>
The third deliverable is the web service / backend service that will support the Maya
deployment.  This will include knowledge of profiles, as well as having the dependency
graph allowing the service to quickly determine which sets and versions of software should
be made available.</p></li>

<li><p><b>Limited Administrative User Interface</b><br>
The fourth deliverable is a small set of Eclipse perspectives and views to support
administrators setting up profiles for users in their organization and allowing users to
override and control their own use of selected profiles.</p></li>

<li><p><b>Native Installers</b><br>
The last component is a base installer which will drop the bootstrap client onto a system.
The installer will not be required to install a JRE or other related software as the
bootstrap is responsible for provisioning the actual Eclipse environment.</p></li>
</ul>

<h2>Background</h2>

<p>Organizations managing many Eclipse installations can incur a high cost in maintaining
and deploying those systems. With the desire to build out organization-specific RCP
applications and deliver to end-users, the knowledge of Eclipse becomes significantly
less.  Metrics within organizations can show that many users are either not aware of RCP
tooling applicable to their work or are not running the appropriate version for their
needs.  In addition, some end-users are not interested in worrying about what software or
versions are installed.  They desire the right set of tooling to be available on their
desktop and expect to have what they need available to them automatically.</p>

<p>A secondary goal of the platform is to enable tooling vendors to more easily deploy
their software in an enterprise.  Being able to effectively release Eclipse-based
software to engineers has been hampered by the proliferation of different techniques for
delivering software, whether a vendor requires a custom installer or uses a separate
update site.  By making Maya part of the standard Eclipse offering, companies will be able
to leverage Maya instead of requiring each vendor to deliver a custom provisioning
solution.  Ultimately, Maya should allow vendors to enable their products to be
distributed within the enterprise with the appropriate licensing enablement and necessary
configuration.</p>

<h2>Out of Scope</h2>

<p>The following areas are explicitly out of scope for the Maya project.  While these capabilities
will not be supported directly by Maya, in certain instances extensibility may be provided to
allow these services to be supplied by building on top of Maya.</p>

<ul>
<li><p><b>High Availability for Web Service</b><br>
Maya will not provide a high availability solution for the web service, nor will it be
optimized for server clustering as may be required to meet high scalability requirements.
While Maya will not purposefully block the use of publicly available solutions, it will
be an exercise for the consumer to deploy Maya in such an environment.</p></li>

<li><p><b>Software Licensing and Controls</b><br>
Maya will not perform any license enforcement or tracking.  The architecture of Maya may
be leveraged to help provide these services; however, the complexity of licensing and the
various different systems involved add significant challenges beyond the scope of this
project.</p></li>

<li><p><b>Package Definition and Validation</b><br>
Maya will not provide any vetting of packages.  Maya will attempt to provide a standard
set of profiles that can be consumed.  It is hoped that package definitions from the
Eclipse Packaging Project (EPP) may be able to be imported as profiles into Maya.</p></li>

<li><p><b>Billing and Up-Selling of Software</b><br>
While Maya can provide a profile selection service which could be extended to up-sell
software offerings, any such extensions of Maya will be explicitly out of scope.  Maya
is targeted for provisioning software readily available from update sites.</p></li>

<li><p><b>Generic Artifact Distribution and Installation</b><br>
Maya will only provide distribution of Eclipse software and the JRE.  Maya will
explicitly not provide generic software artifact installation though the architecture may be
extended to allow other components to be installed.</p></li>

<li><p><b>Workspace Settings and Configuration</b><br>
Maya will not profile workspace configuration and setting synchronization.  Maya is
positioned for provisioning Eclipse software.  Enterprise management of Eclipse development
environments is explicitly out of scope for Maya.</p></li>
</ul>

<h2>Complimentary Works</h2>

<p>Due to the intrinsic nature of provisioning to Eclipse, Maya will have touch points with
multiple projects.  The following two have high likelihood requiring collaboration for joint
success in large managed deployments for Eclipse.</p>

<ul>
<li><p><b>Equinox Provisioning Project</b><br>
<a href="http://www.eclipse.org/equinox/incubator/provisioning/proposal.php"><i>
http://www.eclipse.org/equinox/incubator/provisioning/proposal.php</i></a><br>
The Equinox provisioning project hosted in the Equinox incubator area is starting in
parallel and is planned to provide client-side support to enable richer / more complete
provisioning at a base layer.  As the two projects evolve, it is highly expected that Maya
will be able to leverage the richer services exposed by the Equinox project.  In addition,
certain components of Maya especially those related to the client-side may end up migrating
or being rewritten as part of the Equinox project to better align the two projects as both
mature.</p></li>

<li><p><b>Eclipse Packaging Project</b><br>
<a href="http://www.eclipse.org/epp/"><i>
http://www.eclipse.org/epp/</i></a><br>
The Eclipse Packaging Project (EPP) will have at least one interesting touch point with Maya
in that the packages being defined in EPP will be perfect sources for installing a base set
of profiles within a Maya installation.  In addition, since the EPP will be providing a
client-side installer, it may be necessary to ensure consistency between the end-user
managed installer of EPP and the Maya managed client installer of Maya.</p></li>
</ul>

<p>A potential derivative work of the Maya project could be for an entity such as the
Eclipse Foundation to host a publicly available installation of Maya for delivering general
Eclipse software.  Maya could be used to configure a series of questions such as "Are you a
Java Developer?"  "Do you work with Web technologies?"  "Would you like integrated testing
services?"  The Maya launcher would provide the responses to the web service -- which could
then choose the appropriate pre-configured profile based on the responses.  This service
would then be able to deliver a full Eclipse installation with the right set of software
for the user's needs.  Deploying this service is out of scope for the Maya project but is a
potential opportunity to help with the first-time user experience dove-tailing with EPP.</p>

<h2>Code Contributions</h2>

<p>Cisco Systems, Inc. is offering an internally developed Eclipse deployment infrastructure
as an initial codebase.  The internal infrastructure is being used inside Cisco to deliver
Eclipse-based solutions to engineers and contains at least partial functionality for a
number of the project objectives.  As the Equinox provisioning project moves forward, it may
be appropriate to refactor certain components of the contribution into the Equinox
client-side provisioning project instead of maintaining them in Maya.</p>

<h2>Organization</h2>

<p><b>Proposed Initial Committers</b></p>

<p>The initial contributors will focus on preparing the code contribution by Cisco for
consumption by the Eclipse community.  The focus will then shift to building out a complete
feature set in the Maya project.  Our agile development process will follow eclipse.org's
standards for openness and transparency.  We will actively encourage contributions to the
project and plan on contributing patches to other Eclipse projects if necessary.  As the
Equinox provisioning project moves forward, Maya committers may also need to become actively
involved in Equinox given the multiple touch points regarding provisioning in general.
The initial planned committers are:</p>

<ul>
<li>Timothy Webb (Cisco): Project Lead</li>
<li>Sharanya Doddapaneni (Cisco)</li>
<li>Jennifer Li (Cisco)</li>
<li>Dennis Vaughn (Cisco)</li>
<li>2 engineers (Genuitec)</li>
<li>1+ engineers (Innoopract)</li>
<li>1 engineer (CloudSmith)</li>
</ul>

<p><b>Interested Parties</b></p>

<p>The following parties have expressed interest in the project:</p>

<ul>
<li>Xored (Andrey Platov)</li>
<li>Equinox Team</li>
<li>Genuitec (MyEclipse)</li>
<li>Innoopract</li>
<li>CloudSmith (Buckminster)</li>
<li>Oracle (EclipseLink)</li>
</ul>

<p><b>User Community</b></p>

<p>The Maya project is targeting a disparate group of developers and consumers, as such,
supporting and soliciting feedback from a large community of developers is critical to
creating the right offering.  We plan on doing this by using the standard eclipse.org
mechanisms of supporting an open project and a community of early adopters. </p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
