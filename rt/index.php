<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>The Eclipse Runtime Project (RT)</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("RT");
?>

<h2>Introduction</h2>
<p>
The Eclipse Runtime (RT) project is proposed as a top-level Eclipse open source project. This proposal is
 in the Project Proposal Phase (as defined in the Eclipse Development Process document) and is 
 written to declare its intent and scope. This proposal is written to solicit additional 
 participation and input from the Eclipse community. You are invited to comment on and/or join 
 the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.rt newsgroup">
 http://www.eclipse.org/newsportal/thread.php?group=eclipse.rt</a> newsgroup.
</p>

<h2>Description</h2>
<p>
Eclipse RT is designed to foster, promote and house runtime efforts in the Eclipse community. These efforts strive towards the common goal of providing a uniform component model across a wide variety of computing environments. The Equinox framework and OSGi form the basis of this infrastructure.
</p>
<p>
Eclipse RT projects target "clients" and "servers" across embedded devices, desktops, and enterprise systems, and provide those intermediate software services, which enable applications to be more easily and concisely constructed across these environments. This supports and extends the Equinox vision of a consistent programming and component model where developers create application domain code that runs on a variety of platforms.
</p>
<p>
By providing a consistent symmetric architecture, Eclipse RT technology enables developers to focus on the business problem at hand and still have many system architecture options available at deployment time. 
</p>

<h2>Background</h2>
<p>
Since the creation of the Eclipse Rich Client Platform (RCP) and the adoption of Equinox and OSGi in 2004, interest in and use of Eclipse technologies in runtime scenarios has been steadily increasing. Initially, the focus of these runtime efforts was on desktop or client technologies. The community has steadily found new and innovative uses for Equinox and the Eclipse platform in areas such as devices, rich internet applications and servers.
</p>
<p>
Eclipse RT is an open source collaborative software development project dedicated to supporting and enhancing these efforts and providing a generic, extensible, standards-based runtime platform.
</p>

<h2>Charter</h2>
<p>
The charter for RT is in a separate document found <a href="http://wiki.eclipse.org/Runtime_Top-Level_Project_Draft_Charter">here</a>.

<h2>Organization</h2>

<h3>Initial PMC</h3>
<ul>
<li>Jeff McAffer (Code 9) co-lead</li>
<li>Jochen Krause (Innoopract) co-lead</li>
<li>Ricco Deutscher (Sopera)</li>
<li>Douglas Clarke (Oracle)</li>
<li>Thomas Watson (IBM)</li>
</ul>
</p>

<h3>Contributions/Projects</h3>
<p>
Despite its name and scope, RT is not intended to encompass all runtime-related efforts at Eclipse in the same way as the Tools project does not contain all tooling efforts.  This new top-level project has however attracted a number of new and existing sub-projects.  Currently the following projects have indicated their desire to move to RT.
<ul>
<li><a href="http://www.eclipse.org/equinox/">Equinox</a></li>
<li><a href="http://www.eclipse.org/rap/">RAP</a></li>
<li><a href="http://www.eclipse.org/swordfish/">Swordfish</a></li>
<li><a href="http://www.eclipse.org/eclipselink/">EclipseLink</a></li>
<li><a href="http://www.eclipse.org/ecf/">ECF</a></li>
<li><a href="http://www.eclipse.org/riena/">Riena</a></li>
</ul>
</p>

<h3>Developer Community</h3>
<p>
RT expects to attract developers interested in runtime technologies from embedded to clients/desktops to servers.  The range of initial projects and the background of the leadership team sets a broad scope for participation.  RT intends to provide a fertile atmosphere for collaboration amongst its sub-projects and other runtime-related projects at Eclipse.
</p>

<h3>User Community</h3>
<p>
RT offers the user community a coordinated source for their runtime needs.  It is expected that RT will produce something akin to the tooling release train easing adoption and consumption of Eclipse runtime technology.
</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
