<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "EMF Refactor - Component Proposal";
$pageKeywords	= "emf, emft, ecore, refactoring, model refactoring, compare, uml, uml2, emf tiger, ltk, ewl";
$pageAuthor		= "Thorsten Arendt";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>EMF Refactor - Component Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Modeling Framework Refactor");
?>

<h2>Introduction</h2>

<p>EMF Refactor is a proposed new open source component under 
<a href="http://www.eclipse.org/modeling/emft/">Eclipse EMFT</a> to provide tool support for 
generating and applying refactorings for models based on EMF Ecore, e.g. UML EMF models.</p>

<p>This component is in the Pre-Proposal Phase (as defined in the 
<a href="http://www.eclipse.org/projects/dev_process/">Eclipse Development Process</a>) and 
this document is written to declare its intent and scope. This proposal is written to solicit 
additional participation and input from the Eclipse community. You are invited to comment on 
and/or join in the development of the component. Please send all feedback to the 
<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">emft newsgroup</a>.

</p>

<h2>Background</h2>

<p> Model driven development has become a promising trend in software development which makes
models to the primary artifacts. However, code generators can produce high quality code only 
for input models being of high quality. On the other side, code refactoring is a well-known and
investigated technique to improve software structures without changing the observable behaviour.
So an essential technique for model quality assurance is the refactoring of models which is
in focus of the current research.</p> 

<p>Although several refactorings for models have been suggested, there is not yet a corresponding 
tool support in Eclipse for creating custom refactorings and applying them. Moreover, some model 
refactorings were adopted from code refactorings, especially for models that describe structural 
features like UML class diagrams. In addition, completely new approaches have been developed, 
especially for further diagram types such as state machines and activity diagrams or just other 
kinds of EMF models.</p>

<h2>Scope</h2>

<p>Providing similarly comfortable refactoring support for models as for code refactoring in 
Eclipse would be worthwhile. Since EMF has become a key reference in model driven software
development and a basic technology in Eclipse modeling, the intention is to provide an extensible
tool support for handling EMF model refactorings.</p>

<p>A basic intention of the proposed component is to provide a framework for developing 
refactorings for models that are based on EMF. Model refactorings can be seen as endogenous model 
transformations preserving the semantics of a model yet improving the internal structure. The idea 
is to provide an easy to use generator which relies on model transformations to build model 
refactorings. So in the proposed framework the development of individual model refactorings will be 
based on model transformations, whereas the specifications of these model refactorings should be 
done by means of several different model transformation approaches. This will lead to a framework 
which will be extensible in two ways: on the one hand it should be possible to develop new EMF model 
refactorings which will, on the other hand, be specified and executed by exchangeable model transformation 
engines.</p>

<p>Like in tool support for code refactoring all generated model refactorings shall be applied 
in an intuitive and convenient way. Users should be able to select one or more model elements of
interest - either in the standard EMF instance editor or in a generated graphical editor - and 
choose the appropriate model refactoring out of the context menu of the selection. Needed parameters 
should be entered like in Eclipse code refactoring wizards. If the selected refactoring can not be
executed the user should be informed by appropriate error messages. Furthermore, tool support for 
EMF model refactoring should provide a preview of the performed changes as well as undo and redo 
functionality. Here, the proposed framework should aspire to an integration or adjustment of the 
Language Toolkit API (<a href="http://www.eclipse.org/articles/Article-LTK/ltk.html">LTK</a>).</p>

<p>An advanced tool support for EMF model refactoring could additionally combine refactorings
of different models or, what is especially of interest in the field of MDD, combine model 
refactorings and code refactorings to synchronize the approriate artifacts of the software
development process. A further improvement would be to provide supplementary tool support for
specifying and detecting model smells which could be hints for possible model refactorings. 
Here, model refactorings could be executed in a quick-fix manner.</p> 

<p>Because of the increasing usage of UML2 models refactoring support especially for UML2 models
edited in <a href="http://www.eclipse.org/modeling/mdt/?project=uml2tools">UML2 Tools</a> is desirable.</p>

<h2>Initial Contribution</h2>

<p>An initial contribution will provide the primary functionality for model refactoring.</p>
 
<p><img src="images/ModulesArchitecture.png" /></p>
 
<p>As described above EMF Refactor consists of two main modules with basic functionalities. 
The Refactoring Generation Module integrates the functionality of each model refactoring, that 
has been defined by an EMF model transformation, into a special Eclipse plugin. Here, the actual 
distribution supports the model transformation tool EMF Tiger that allows defining model 
transformations graphically. The advanced plugin will extend EMF Refactor and provide a new 
item in the context menu of the standard tree-based EMF instance model editor by which the 
refactoring can be applied. Moreover, the Refactoring Application Module allows to select a model 
refactoring, to set all parameters needed, to preview the result of a refactoring, and to actually 
perform a refactoring.</p>

<p>The actual distribution of EMF Refactor provides implementations of a number of basic UML2 
model refactorings:</p>

<h4>Available Refactorings for UML2 class diagrams</h4>
<ul>
	<li>Change Attribute To Association End - 
	<i>transforms an attribute to an association end.</i></li> 
	<li>Change Association End To Attribute - 
	<i>transforms an association end to an attribute and is the opposite of refactoring
    [Change Association End To Attribute]</i>.</li>
    <li>Create Associated Class -
    <i>creates an empty class and connects it with a new association to the source class
    from where it is extracted.</i></li>
    <li>Create Intermediate Superclass -
    <i>creates an empty class in between a selected class and one of its superclasses.</i></li>

    <li>Create Subclass -
    <i>creates an empty subclass of a selected class.</i></li>
    <li>Create Subinterface -
    <i>creates an empty subinterface of a selected interface.</i></li>
	<li>Create Superclass - 
	<i>creates a new super-class of a class.</i></li>
	<li>Hide Property - 
	<i>makes a non-private attribute/property of a given class private and creates getter and 
	setter operations for it. In the literature, this refactoring is called 
	[Attribute/Field/Property Privatization] and [Encapsulate Attribute/Field/Property].</i></li>
	<li>Pull Up Operation - 
	<i>moves an operation of a class to one of its superclasses.</i></li>

	<li>Pull Up Operation To Interface - 
	<i>moves an operation of a class to one of its implemented interfaces.</i></li>
	<li>Pull Up Operation To Superinterface - 
	<i>moves an operation of an interface to one of its superinterfaces.</i></li>	
	<li>Pull Up Property - 
	<i>removes a property from a class and inserts it into one of its super-classes.</i></li>
	<li>Push Down Operation - 
	<i>pushes an operation down to all its sub-classes. Sometimes it is also called [Push Down
    Method] and it is the opposite of refactoring [Pull Up Operation].</i></li>
	<li>Push Down Property - 
	<i>moves a property of a class to all of its sub-classes. It is the opposite of refactoring 
	[Pull Up Property].</i></li>

	<li>Remove Empty Intermediate Superclass - 
	<i>removes a completely empty class which is a superclass of a set of subclasses but also
	generalizes other classes or implements interfaces. It is the opposite of refactoring 
	[Create Intermediate Superclass].</i></li>
	<li>Remove Empty Subclass - 
	<i>removes a completely empty subclass of a classes and is the opposite of refactoring 
	[Create Subclass].</i></li>
	<li>Remove Empty Subinterface - 
	<i>removes a completely empty subinterface of an interface and is the opposite of refactoring 
	[Create Subinterface].</i></li>	
	<li>Remove Empty Superclass - 
	<i>removes a completely empty superclass of a set of classes.</i></li>
	<li>Rename Class - 
	<i>changes a class name to a new one.</i></li>

	<li>Show Property - 
	<i>makes a non-visible property public and removes its getter and setter operations.
	It is the opposite of refactoring [Hide Property].</i></li>
</ul> 
 
<h4>Available Refactorings for UML2 state machines</h4>
<ul>
	<li>Fold Transition Incoming to Composite State - 
	<i>folds transitions with same event incoming to a composite state from a state 
	outside that composite state.</i></li>
	<li>Fold Transition Outgoing From Composite State - 
	<i>folds transitions with same event outgoing from a composite state to a state 
	outside that composite state.</i></li>
	<li>Unfold Transition Incoming to Composite State - 
	<i>unfolds a transition incoming to a composite state to transitions incoming to 
	each sub-state of the composite state. It is the opposite of refactoring [Fold 
	Transition Incoming to Composite State].</i></li>

	<li>Unfold Transition Outgoing From Composite State - 
	<i>unfolds a transition outgoing from a composite state to transitions outgoing from each
    sub-state of the composite state. It is the opposite of refactoring [Fold Transition 
    Outgoing From Composite State].</i></li>
</ul>

<h2>Relationships to other Eclipse Projects/Components</h2>

<p>EMF Refactor builds upon the functionality provided by the 
<a href="http://www.eclipse.org/modeling/emf/">EMF</a> project itself.</p>

<p><img src="images/UsesArchitecture.png" /></p>

<p>Special relationships to other projects/components are:</p>

<ul>
	<li><a href="http://www.eclipse.org/modeling/m2t/?project=jet">JET</a>, for Java code 
	generation by the Refactoring Generation Module. </li>
	<li>EMF model transformation tools for designing and generating individual model refactorings. 
	Here we plan a close collaboration with <a href="http://tfs.cs.tu-berlin.de/emftrans">EMF Tiger</a>, 
	also in the Pre-Proposal Phase as EMFT component. But other transformation tools should be
	supported by EMF Refactor, too.</li>

	<li>An EMF model comparison tool for providing a preview before applying individual model 
	refactorings. Here we plan to integrate the existing EMFT component 
	<a href="http://www.eclipse.org/modeling/emft/?project=compare#compare">EMF Compare</a>. 
	</li>
</ul>

<p>Planned integrations:</p>

<ul>
	<li>An integration into the graphical editors of 
	<a href="http://www.eclipse.org/modeling/mdt/?project=uml2tools">UML2 Tools</a> should be 
	involved.</li>
	<li>EMF Refactor should aspire to an integration or adjustment of the Language Toolkit API 
	(<a href="http://www.eclipse.org/articles/Article-LTK/ltk.html">LTK</a>).</li>

</ul>

<p>Related Work:</p>

<ul>
	<li>EMF Refactor will not compete with the <a href="http://www.eclipse.org/gmt/epsilon/">Epsilon 
	Wizard Language (EWL)</a>, a language that supports update transformations in the small for
	diverse modeling languages. Instead, a combinated use of EMF Refactor and EWL could be possible.</li>
	<li>EMF Refactor could be used in combination with <a href="http://www.eclipse.org/proposals/edapt/">Edapt</a>,
	a framework for Ecore model adaptation and instance migration which is also proposed as project
	under <a href="http://www.eclipse.org/modeling/emft/">Eclipse EMFT</a>.</li>
</ul>

<h2>Organization</h2>

<h3>Mentors</h3>

<ul>
	<li>Ed Merks (<a href="http://www.macromodeling.com">Macro Modeling</a>, Canada)</li>
	<li>Bernd Kolb (<a href="http://www.sap.com">SAP</a>, Germany)</li>
</ul>

<h3>Initial Committers</h3>

<p>The initial committers for this component would be:</p>

<ul>
	<li>Thorsten Arendt (<a href="http://www.uni-marburg.de/fb12">Philipps-Universitaet Marburg</a>, 
	Germany), proposed project lead</li>
	<li>Florian Mantz (<a href="http://www.uni-marburg.de/fb12">Philipps-Universitaet Marburg</a>, 
	Germany), committer</li>

	<li>Lars Schneider (<a href="http://www.uni-marburg.de/fb12">Philipps-Universitaet Marburg</a>, 
	<!-- (<a href="http://www.de.capgemini-sdm.com/deutsch/index.html">Capgemini sd&m AG</a>, Munich, --> 
	Germany), committer</li>
</ul>

<h3>Interested Parties</h3>

<p>So far, interest in this component has been expressed by:</p>
<ul>
	<li>Nikolaus Regnat 
	(<a href="http://w1.siemens.com/innovation/en/about_fande/corp_technology/index.htm">Siemens 
	Corporate Technology</a>, Munich, Germany)</li>
	<li>Philip Langer 
	(<a href="http://www.tuwien.ac.at/">Vienna University of Technology</a>, Vienna, Austria)</li>

</ul>

<p>Interested parties are welcome to add themselves to the list above as interested parties or to 
suggest changes to this document.</p>

<h3>Developer Community</h3>

<p>The team of initial committers will explore statements of interest from additional developers 
experienced with model refactoring or willing to gain such experience.</p>

<h3>User Community</h3>

<p>It is expected that the user community for EMF Refactor will consist of two kinds of users. 
On the one hand, there will be researchers and developers who design model refactorings. On the 
other hand, there will be modelers who apply the generated model refactorings to their EMF based 
models - which can surely belong to the first category, too.<p/>

<h3>Tentative Plan</h3>

<p>The first community technical preview release is scheduled at the beginning of 2010 and the first 
release is targeted in autumn 2010.<p/>
 
<p>Please visit our <a href="https://www.mathematik.uni-marburg.de/~swt/modref/">Homepage</a> for 
further information.</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
