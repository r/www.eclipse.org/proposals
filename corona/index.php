<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Proposal for Tools Services Framework (Corona)";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
      <div id="midcolumn">
        <h1>
          Proposal for Tools Services Framework (Corona)
        </h1>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Tools Services Framework (Corona)");
?>
        <p>
          This document declares the intent and scope of a
          proposed project called the Tools Services Framework
          project, or Corona. The Corona project is in the Project
          Proposal Phase as defined in the <a 
href="http://www.eclipse.org/projects/dev_process/">Eclipse Development
          Process</a> document. We propose that this project be
          established within the Technology Project PMC. In
          addition, we are soliciting additional participation
          and input from the Eclipse community. You are invited
          to comment on the project and encouraged to join.
          Please send all feedback to the <a 
href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.corona">
          http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.corona</a>
          newsgroup.
        </p>
        <h2>
          Background
        </h2>
        <p>
          The success of Eclipse has led to a number of requirements for functionality
          that enables integrations across multiple 
          clients. Eclipse plug-in tools and applications
          are frequently used by development teams and other
          organizational units who could benefit from additional
          collaboration capabilities among Eclipse client
          environments enabled by a central shared context. As
          the Eclipse ecosystem grows to support a broader set of
          tools, it would be advantageous to have a framework for
          hosting plug-ins that supports tool integrations in a
          shared context.
        </p>
        <p>
          A number of existing and proposed Eclipse projects are
          already demonstrating a need for a shared framework:
        </p>
        <ul>
          <li><p>The Application Lifecycle Framework (ALF) project
          specifies a hosted BPEL environment, along with a set
          of services to support event routing and additional
          utility functions such as single sign-on. Hosting these
          services in an extensible Eclipse framework would
          enhance their scope and flexibility.</p>
          </li>
          <li><p>The Eclipse Communications Framework (ECF) already
          contains an exemplary collaboration server, based on
          the Resource layer of Eclipse. The ability to access
          the ECF's collaboration abilities, within a shared
          environment populated by additional life cycle tools,
          would allow vendors to implement dynamic distributed
          troubleshooting scenarios.</p>
          </li>
          <li><p>Higgins, the Trust Framework could be deployed
          within this framework to provide both the social
          contexts required for collaboration and the needed
          security framework.</p>
          </li>
          <li><p>The Eclipse Test and Performance Tools Platform
          (TPTP) could simplify its agent control infrastructure
          by building a unified Agent Controller that can operate
          on the developer's system or remote machines.</p>
          </li>
          <li><p>The Business Intelligence and Reporting Tools
          (BIRT) project could enhance the capabilities of their
          report server using this plug-in framework.</p>
          </li>
        </ul>
        <p>
          Thus far, no Eclipse framework has been defined to
          create and manage the plug-ins that would support these
          collaborative cases.
        </p>
        <p>
          The SOA Tools Project (STP) has identified a set of
          Enterprise Service Bus (ESB) implementations as its
          deployment targets. The emerging popularity of an ESB
          as an environment for hosting services suggests that
          using an ESB environment to host a shared Eclipse
          framework would provide a number of benefits. A few of
          these benefits would be a SOA front end for Eclipse
          tools, synergies with the SOA Tools Project, and
          deployment flexibility with multiple protocol bindings.
        </p>
        <h2>
          Description
        </h2>
        <p>
        Corona will provide the frameworks required for
        collaboration among instances of Eclipse based clients as well as
        clients that exist outside the Workbench environment. The Corona project
        will include:</p>
        <ul>
          <li><p>A service plug-in model based on OSGi and hosted
          within a generic ESB container. The initial deployment
          targets are the ones supported by the SOA Tools
          Project.</p>
          </li>
          <li><p>Extension points that facilitate the creation a
          wide variety of plug-ins.</p>
          </li>
          <li><p>An implementation of extensions for collaboration
          concepts that are analogous to applicable Workbench concepts
          such as Projects and Natures. For example, a shared
          Project would implement a container for associated
          Project repositories and would host ad hoc
          collaboration via ECF between project members as
          defined by Higgins.</p>
          </li>
        </ul>
        <h3>
          Goals
        </h3>
        <p>
          The goals of the project are:
        </p>
        <p>
          <i><u>An Eclipse OSGi loader that is deployable
          within an ESB container</u></i> - We will provide an
          Eclipse environment, complete with plug-in support and
          extension points, running within the ESB containers
          supported by the Eclipse SOA Tools Project.
        </p>
        <p>
          <i><u>Collaboration-oriented analogues to existing
          Workbench constructs such as Projects and
          Natures</u></i> - In addition to the vertical
          extensibility provided by the plug-in and extension
          point mechanisms, we will provide a set of horizontal
          integration constructs that are analogous to the
          Project and Nature concepts within the Workbench. This 
          will include a shared version of Workbench Projects, 
          support for transient activities analogous to Builders 
          within the Workbench, and support for ad-hoc collaboration spaces.
        </p>
        <p>
          <i><u>Web Service Endpoint accessibility to
          Eclipse-hosted services</u></i> - We will provide a closed access loop
          between our OSGi-hosted plug-ins and services that are
          hosted by the ESB container. We will do this by
          supporting the ability to access plug-in functionality
          through Web Services endpoints and allowing plug-ins to
          participate in ESB-hosted BPEL activities.
        </p>
        <p>
          <i><u>Provision for shared classes between ESB
          tooling and Eclipse plug-ins</u></i> - We will
          integrate the OSGi container with the ESB host in such
          a way that users of the platform may designate portions
          of their plug-in implementation to be accessible from
          outside the plug-in framework by implementing an option
          for deployment of plug-in classes to a class path whose
          scope extends beyond the OSGi plug-in layer.
        </p>
        <p>
          <i><u>PDE and STP extensions for the development and
          deployment of components hosted in Corona</u></i> - We
          will work with the Plug-in Development Environment
          (PDE) project and the SOA Tools Project (STP) to
          support the creation and deployment of plug-in
          services.
        </p>
        <h3>
          Deliverables
        </h3>
        <p>
          We will deliver:
        </p>
        <p>
          <i><u>Corona Core</u></i> - A set of OSGi-based
          plug-ins that implement the framework. These plug-ins
          define the extension points for shared Repositories,
          Projects, Tasks and Collaboration spaces. Additionally,
          we will deliver an implementation of a Link concept and
          the existing Marker concept. Finally, we will provide a
          Manageability abstraction within the Core that can be
          mapped onto a Web Services Distributed Management
          (WSDM) interface.
        </p>
        <p>
          <i><u>Exemplary deployments</u></i> - The base
          framework will be deployable within the ESB
          implementations provided by the SOA Tools Project. We
          will implement the interface between the ESB container
          and this framework in a pluggable fashion using the
          existing Eclipse extension point mechanism. In addition
          to an ESB deployment, we will also provide a
          stand-alone deployment, leveraging the OSGi HTTP
          Service and the Apache Muse Project.
        </p>
        <p>
          <i><u>Exemplary plug-ins</u></i> - We will provide a
          set of OSGi-based plug-ins for the framework, to
          demonstrate some possible usage patterns. The initial
          exemplary plug-ins will provide support for an
          instantiation of some of the collaborations delineated
          in the Background section, using existing Eclipse and
          popular Open Source functionality. We will deliver the
          following:
        </p>
        <ul>
          <li><p>A Higgins plug-in for Corona that is accessible by
          other Corona hosted plug-ins and that mediates access to
          both data and services.</p>
          </li>
          <li><p>An ECF plug-in that will duplicate the
          functionality of the existing ECF server component and
          extend it to be 'socially aware' with respect to
          Higgins, and context aware with respect to Projects,
          etc. The ECF plug-in will demonstrate the Corona
          Collaboration Space extension points.</p>
          </li>
          <li><p>An implementation of the WSDM MUWS specification
          for managing the Corona plug-ins and artifacts. This
          implementation will externalize the Corona Container
          concepts (Projects, etc.) as Manageable Resources, and
          externalize the Corona Link concept as Relationships.</p>
          </li>
          <li><p>An Eclipse Plug-in for browsing and modifying Corona
          instances using the WSDM interface.</p>
          </li>
          <li><p>A deployment framework for the ALF event manager
          and BPEL engine.</p>
          </li>
        </ul>
        <h2>
          Organization
        </h2>
        <p>
          We propose that this project be undertaken as a
          Technology project. Once this work matures, the project
          could move from the Technology Project PMC to the
          Eclipse Platform PMC to provide a common open source
          framework for building plug-ins for either RCP or ESB
          applications. Alternatively, it could continue as a
          sub-project of the Technology Project or be
          incorporated into a future top-level project such as
          the SOA Tools Project.
        </p>
        <h2>
          Suggested Project Lead and Committers
        </h2>
        <p>
          This section lists organizations that have expressed
          interest in the project or its components, and will be
          updated periodically to reflect the growing interest in
          this project.
        </p>
        <h3>
          Initial Set of Committers
        </h3>
        <p>
          Dennis O&rsquo;Flynn, (Project Lead) Compuware Corp.<br />
          Glenn Everitt, Compuware Corp.<br />
          Joel Hawkins, Compuware Corp.<br />
          Jim Wright, Compuware Corp.<br />
          Neil Faigenbaum, Compuware Corp.<br />
        </p>
        <h3>
          Interested Parties
        </h3>
        <p>
          Compuware Corp. <a href="http://www.compuware.com">- www.compuware.com</a><br />
          Serena Software, Inc. - <a href="http://www.serena.com">www.serena.com</a><br />
					IONA Technologies - <a href="http://www.iona.com">www.iona.com</a><br />
        </p>
      </div>
    </div>
   
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
