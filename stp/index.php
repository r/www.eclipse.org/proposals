<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">


<h1>SOA TOOLS PROJECT</h1><br>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("SOA Tools Top-Level");
?>

<h2>Project Organization</h2>
<p>The SOA (Service Oriented Architecture) Tools Platform Project (STP) - a top-level project proposal - is an open source project of  <a href="/">eclipse.org</a>, overseen by a Project Management Committee (PMC) and project leaders. The PMC Planning Council coordinates identified resources of the project against a Project Development Plan. All the work will be done in subprojects of SOA Tools Platform Project.
</p>
<p>This proposal is in the Project Proposal Phase (as defined in the <a href="/projects/dev_process/">
Eclipse Development Process document</a>) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.stp">
http://www.eclipse.org/newsportal/thread.php?group=eclipse.stp.</a>

<h2>Overview</h2>
<p align="left">�SOA Tools� is a vast domain, however there are a set of key requirements that run across the domain. The goal of this proposed project is to put the fundamentals in-place so that an extensible tool set made of components and exemplary tools for constructing SOA applications is created. STP will leverage the existing work of the other projects like Data Tools and Web Tools Platform projects.</p>

<p align="left">A developer using the proposed SOA Tooling project is interested in an environment that is easy to use -- one in which the challenges of application development are due to the problem domain, not the complexity of the tools employed. To this end, the project will strive to create a highly usable and consistent environment that works well with associated technologies, whether being used by a developer working/creating a service, an administrator maintaining or monitoring a production system, or constructing a larger SOA Network.</p>

<p align="left">Such an environment starts with key frameworks designed both for use and extensibility. Examples include the location or creation of a service consumer or provider, the consumption of these services, the configuration of the physical attributes (transport, message format) and the policies required to access or consume the like (security policy, access control, transactional, availability). Further the ability to locate and add services to the SOA interactions like transformation, routing, for process orchestration to a broker or end-point needs to be addressed.</p>

<p align="left">Finally the creation of Artifacts that can be used to deploy, enforce or manage said SOA participants� needs to be created from the Eclipse tooling in an extensible way.</p>

<p align="left">The proposed project will not try to attempt to define every type of service in a SOA but to define the contracts to unify them into a SOA through an extensible framework and model the policies and interactions with an abstraction so that multiple specific vendor implementations can be supported from the vendor independent models of <a href="http://www.w3.org/TR/wsdl" target="_blank">WSDL</a> and other web service standards.</p>


<h2>Mission</h2>
<p align="left">
The SOA Tools Platform (STP) project will include extensible frameworks and exemplary tools, enabling a diverse set of plug-in offerings specific to particular SOA participant technologies and supported by the STP ecosystem. In the spirit of Eclipse, the project will be guided by the following values:

<ul>
    <li><p align="left"><i>Vendor neutrality</i>: We intend to provide SOA service construction, policy and service interaction frameworks and tools not biased toward any vendor. Our intention is that STP be leveraged to provide the Eclipse community with the widest range of choices possible. To this end, we seek <a href="/projects/dev_processes/validation-phase.php">community involvement</a> in formulating key framework interfaces, so that the largest possible constituency is represented.</p></li>
    <li><p align="left"><i>Extensibility</i>: We recognize both the common need for SOA tooling infrastructure and the desire to extend the offerings in new and innovative ways. To support these efforts, our components will be designed for, and make good use of, extensibility mechanisms supported by Eclipse. 
  
		<br><br>
<i>It should be noted that this project would define a number of extension points using the Eclipse plugin mechanism. The structure of the SOA tools will need to be abstracted so that out of the box we will provide functionality that is common across all toolkits (e.g. basic WSDL creation and editing) but will rely on underlying runtime toolkits extending platform to offer their services into the toolkit. For example, the ability to create complex routing components would be made available as an Eclipse plugin that would extend the basic STP to support open source projects like <a href="http://celtix.objectweb.org" target = "_blank">Celtix</a>, <a href="http://www.servicemix.org/" target="_blank">ServiceMix</a> and <a href="http://mule.codehaus.org/" target="_blank">Mule</a>. </i> </p></li>

<li><p align="left"><i>Standards-based innovation</i>: This proposed project will deliver an extensible, standards-based tooling foundation on which the widest possible range of vendors can create value-added development products for their customers and end-users. Where standards exist, we will adhere to them. At least, at first, where standards are emerging, we will wait for them to emerge; this can be re-evaluated later according to user needs and contributor availability. Where multiple technologies are widely used for a given functional need, we will attempt to support each, subject only to technical feasibility and our goal of providing the most capable and extensible foundation long term.</p></li>

<li><p align="left"><i>Community Involvement</i>: Success for STP, as with other eclipse.org projects, is as much a factor of community involvement as the technical merit of its components. We strongly believe that STP will achieve its full potential only as the result of deep and broad cooperation with the Eclipse membership-at-large. Thus, we will make every effort to accommodate collaboration, reach acceptable compromises, and provide a project management infrastructure that includes all contributors, regardless of their affiliation, location, interests, or level of involvement. Regular meetings covering all aspects of STP, open communication channels, and equal access to process will be key areas in driving successful <a href="/projects/dev_processes/validation-phase.php">community</a> involvement.</p></li>

<li><p align="left"><i>Transparency</i>: As with all projects under the eclipse.org banner, key information and discussions at every level � such as requirements, design, implementation, and testing � will be easily accessible to the Eclipse membership-at-large.</p></li>

<li><p align="left"><i>Agile development</i>: We will strive to incorporate into our planning process innovations that arise once a project is underway, and the feedback from our user community on our achievements to date. We think an agile planning and development process, in which progress is incremental, near-term deliverables are focused, and long-term planning is flexible, will be the best way to achieve this.</p></li></ul>
		
		
		
		</p>

<h2>Scope</h2>

			<p align="left">SOA applications, services and deployments (we will call this the SOAN, the SOA Network) are ones that are created across two axes, the principles of creating a service and the methodology for deploying these services in a network, the SOAN.</p>

<p align="left"><i>Service Creation subproject</i></p>

<p align="left">Ideally when a SOA service is created, the contract (STP will use WSDL), the implementation and the physical attributes should be independent of each other. STP will re-enforce this architecture for all service creation tooling provided in this proposed project.</p> 
<p align="left">Explicitly,</p> 

  

<ol type="a">
    <li>WSDL editing capabilities: Extending the capabilities above Web Tools Project (WTP) to provide the extensibility for multiple language and technology support, extending it to support Java (<a href="http://java.sun.com/xml/jaxb/" target="_blank">JAX-B</a>, <a href="http://java.sun.com/xml/jaxrpc/index.jsp" target="_blank">JAX1.1</a>), <a href="http://www.omg.org/schedule/" target="_blank">C++</a>, PL/I and COBOL mapping</li>
    <li>The WSDL editing capabilities will be extended to provide the ability to support multiple physical bindings but not limited to JMS, MQ, Tibco, CORBA, and the ability for vendors to easily provide extensions for specific transports, for example Celtix, WSIF, or Synapse.</li>
    <li>The creation of and configuration of bindings (WSDL) for additional message formats for legacy integration in the SOAN, for example tooling to represent Cobol copy books one the wire or G2++, or CSV records</li>
    <li>The association of runtime policy with said service participants, for example the ability to mark a service provider as secure, load balanced, state-full/stateless, or transactional. This information is then used to create deployment artifacts and model interaction between service consumers and providers. (See SOAN Deployment subproject for list of policies targeted)</li>
    <li>Exemplary implementation support for Normalized Message (<a href="http://www.jcp.org/en/jsr/detail?id=208" target="_blank">JBI � JSR208</a>) bindings and transports (WSDL) for collocated or JBI/JVM interactions. The collocated interactions will not be limited to JBI, but JBI will be used as one of the exemplary tool implementations. Thus the editors required to create a JBI plugin, consume JBI services and model interactions with JBI components will be integrated into the relevant editors</li>
    <li>The creation of an extensible framework for plugging in additional extensions for configuring bindings, transports and policies. For examples of these see (b,c, and d).</li>
    <li>The WTP validation tools used and extended to validate said contacts/policies</li>
    <li>The development tools will be able to support additional extensions providing extension points into the editor as well as the schema validator to allow developers to validate their WSDL against the schema which defines that extension.</li>
    <li>The import/derivation of contracts (WSDL) from existing artifacts like, XSD, existing CSV, COBOL, copybooks, IDL or other defined message formats.</li>
</ol>

<p align="left"><i>SOAN Deployment subproject</i></p>
  
<p align="left">
Ideally when a SOAN is created, a separation of Resources, Components, Business Services, and Presentation is desired. Most people don�t agree on the exact terms, but all agree on the patterns for creating a re-useable network of both application and infrastructure participants. The STP platform will focus on providing the tooling required to tie these entities together based on their contracts and policies.</p>
 

Explicitly,
  

<ol type="a">
    <li>An abstract model (EMF) will be created to describe service policies. The initial policies targeted include, message reliability, addressing, contract retrieval, location services, transactional, load balancing, high availability, state-full/stateless, security, SLAs(max response time), governance (how it is audited), message attributes (compression)</li>
    <li>Readers and writers will be provided for <a href="http://www.xml.com/" target="_blank">XML</a> & <a href="http://specs.xmlsoap.org/ws/2004/09/policy/ws-policy.pdf" target="_blank">WS-policy</a> description of these policies. Extensions can be added for additional readers and writers</li>
    <li>Validation and matching of services will be provided (for example, are the consumer and provider policies compatible)</li>
    <li>Specific policy patterns will be implemented for <a href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=wss" target="_blank">WS-Security</a>, role based access control, transaction (<a href="http://www.oasis-open.org/committees/%20tc_home.php?wg_abbrev=ws-caf" target="_blank">WS-C and WS-AT</a> implementation), location services (repository policy for contacts and references).</li>
    <li>Policies extensions may also be used to describe policies like message compression, encryption, and Service usage and response time contacts. Samples of some of these will be provided as implementation examples. A base model for policies will be provided together with the models of the specific policies. A default editor will be dynamically created based on the model. However specific editors can be substituted for a specific policy model by providing an extension to edit that policy. All policies will have a default dynamic XML generator, and it is expected that implementations will be provided for specific runtimes. An exemplary implementation will be provided for Celtix, under the Celtix Tools Project subproject.</li>
    <li>The definition of the deployment requirements for a service policies, i.e. the service can be collocated, requires JEE, JSE etc, so that deployable configurations can be modeled. This definition is then used by the extension to create the relevant deployment artifacts for that support the specific runtime. For example create a WAR file with security enabled and deploy as a proxy server.</li>
</ol>

  
<p align="left">
This section is NOT attempting to handle service orchestration with <a href="http://www.oasis-open.org/committees/%20tc_home.php?wg_abbrev=wsbpel" target="_blank">BPEL</a>. That is viewed as the scope of a services participant, and will be an internal detail on one of the services in SOAN.</p>

<p align="left"><i>Expected Runtime specific extensions subprojects</i></p>
<p align="left"><b>Exemplary implementations can be provided for specific tooling of runtimes, provided they are built on the extensions and are pluggable for the specific vendor or open source implementations so that the extensible framework is not compromised.</b> Thus extensions to support ESB�s, Repositories, Brokers, Containers and Data Access services, etc should be built as Runtime extension subprojects.</p>
<p align="left">Over and above the participants there are some key enabling technologies in a SOAN. These include ESB�s, Repositories, Brokers, Containers and Data Access, which are present in every SOA deployment. These specific participants will be also tackled in the initial scope. Additional services can be tackled with the addition of members to this project. These subprojects build on the concepts and deliverables from the above two subproject in addition to the DTP (Data Tools Platform) project. </p>


<p align="left"><i>Celtix Tools Project </i></p>

<p align="left">This proposed project will explicitly tackle the tooling of specific implementations of ESB�s, Repositories, Brokers, Containers and Data Access. Thus the abstraction layers created by the above projects will be used to create extensions to tool the Celtix, ServiceMix, or used with any other JBI container. This forms both an example and exemplary tool implementation for the end-end usage of the Tools provided include</p>

  

<ol type="a">
    <li>The tooling for creating ESB services to be deployed to a router or broker: This will include the tooling for the following services:

<ol type="a">
    <li>Routing, extensions will be provided to do WSDL operation, context (properties of a message, transport, or service) or message content. The editors to make the routing decision for content will be pluggable so that different editors for XPath and XQuery can be used in conjunction the routing semantics.</li>
<li>Transformation based of standard processors like <a href="http://www.w3.org/TR/xslt" target="_blank">XSLT</a>.</li>
<li>Data integration utilizing the <a href="/datatools/">DTP</a>, to define the access of Data for use tasks like routing and transformation.</li></ol></li>

<li>JBI Tooling

<ol type="a">
    <li>Editors for creating JBI bindings, and service engines will be provided.</li>
<li>Editors to orchestrate JBI service flows will be provided.</li>
<li>A browser to view the contents of a JBI container will be provided.</li>
<li>JBI deployment extensions will be created to the creation of deployment bundles.</li></ol></li>

<li>Retrieval / posting of artifacts to repositories will be done in a pluggable way so that <a href="http://www.uddi.org/specification.html" target="_blank">UDDI</a>, LDAP or any other such repository can be used. These facilities are to be used for Policy and contract management.</li></ol>
 
<p align="left">
Note, JBI Tooling might be deemed enough scope to subdivide this sub-project.</p>

<p align="left"><i>Related concepts </i></p>

  
<p align="left">
In a SOA network the Quality of Service is one of the key characteristics, this includes High Availability, security, response times, transactional, management and logging, for example ARM. These dynamics define the �value� of the network to deliver on a service contract SLA (service level agreements). The modeling and tooling of each of these are out of scope of the initial proposal, but the visualization of these attributes and design based on them is in scope, together with interaction with them in the SOAN to provide or enforce the SLA�s.</p>

  
<p align="left">
Although the scope of STP includes exemplary tool implementations for open source and commercial ESB participants and services, these are not necessarily intended to be the definitive extensibility generators provided. Instead, they are intended to serve two purposes. First, they are intended to enable users to immediately use these artifacts (for example using a WTP generated application in a SOA deployment), although possibly without exploiting all their features. Second, they are intended to serve as examples to both commercial and open source developers who want to integrate consumption/interaction with SOA participants into Eclipse STP framework. It is consistent with the goals of this project that the exemplary extensions become superseded by more complete implementations provided by third parties, both commercial and open source.</p>
<p align="left">
The key behind this proposal is that the entire Eclipse membership, not just for those intimately involved with SOA will benefit from the tooling.</p>

<h2>How it fits together</h2>
		
		
		<p align="left"><i>Logical Model Architecture</i></p>
		
		
		<img src="/proposals/stp/STP-pr1.gif" width="556" height="289" alt="Logical Model Architecture" border=0 align="">

<p align="left">The diagram describes the key components that will be delivered as part of the SOA Tools Project. At its core is a suite of contract design and development tools that allow users to create services with additional validation. From a design time perspective, it will provide a number of extension points to extend the basic functionality. Extensions provided but not limited to include:

<ol type="a">
    <li>Importers, the means by which users are able to start with a technical artifact that is not WSDL (e.g. Java class, IDL XML Schema) and converts this into WSDL that can be edited within the design time environment</li>
    <li>Validators will allow the developer to validate the WSDL against relevant schemas</li>
    <li>Editors that allow different types of views to be used to edit the WSDL (e.g. useful in the case of new binding types, different types of transports)</li>
</ol>

Typically service contract get bound to an implementation. An extensible layer will be provided to plug-in support for various runtimes, to create executable entities, either Service Consumers or Service Providers. Attributes of the runtime component will need to be exposed to provide the editor the ability to generate/create artifacts for specific implementations, as an Eclipse plugin. The services that extension for runtime support element will bring are:

<ol type="a">
    <li>Generators to create starting point application code that the developer can complete out. Test / Debug environments to be able to properly test their development activities</li>
    <li>Management Screens to look at the deployed solution, start / stop / pause services etc.</li>
</ol>

The very top of the layer is the SOAN level, a view of the policies and interrelations of the services and Quality of Service (QoS) provided but the SOAN. This layer will be used to view inter service policies, connect �compatible� services together, etc. For example polices can be set prior to creating of contracts, settings security levels and even identify key repositories that will be used by the developer. The SOAN layer provides the �business� view of the SOA Network.</p>

<h2>Projects</h2>

Initially STP will contain the following three subprojects: service creation/development, SOAN Deployment and Runtime specific extensions.

<h2>Organization</h2>

The proposed PMC will be initially made up of 3 members.

<ul>
    <li>Christophe Ney, ObjectWeb </li>
    <li>Carl Trieloff, IONA Technologies (Lead) </li>
    <li>Karl Reti, Sybase</li>
</ul>


Support or interest to contribute has been expressed by,

<ul>
    <li>Adrian Skehill, IONA (Subproject lead)</li>
    <li>Adrien Louis, EBM WebSourcing</li>
    <li>Alain Boulze, ObjectWeb</li>
    <li>Alex Chen, IONA</li>
    <li>Ciaran O Cleirigh, IONA</li>
    <li>David Beaurpere, IONA</li>
    <li>Erica Mitchell, IONA</li>
    <li>Fiona Kennedy, IONA</li>
    <li>Freeman Fang, IONA</li>
    <li>Ga�l Blondelle, EBM WebSourcing</li>
    <li>Howard Gao, IONA</li>
    <li>James Strachan, LogicBlaze</li>
    <li>Jean-Pierre Laisne, ObjectWeb / Bull</li>
    <li>Joan Rohn, IONA</li>
    <li>Johnson Ma, IONA</li>
    <li>Mairead Melia, IONA</li>
    <li>Members of ServiceMix Project</li>
    <li>Members of ObjectWeb Celtix</li>
    <li>Miko Matsumura, Infravio</li>
    <li>Rafael Marins, Fossil E-Commerce</li>
    <li>Ross Mason, SymphonySoft Ltd</li>
    <li>Interest from the SI community</li>
</ul>


		
</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
