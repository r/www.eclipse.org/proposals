<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">
  
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Model Driven Development Integration");
?> 
    
      <h1>Model Driven Development Integration</h1>
  
      <p>
	  This Eclipse project proposal (refer to the <a href="/projects/dev_process/">Eclipse
	  Development Process Document</a>) is posted to declare the intent and scope of a Technology PMC Project called the Model Driven Development integration
	  project (MDDi). In addition, this proposal is written to solicit additional participation and input from the Eclipse community. You are invited to
	  comment on and join the project. Please send all feedback to <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.mddi">
	  http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.mddi</a>
      </p>
      <p align="center">
    
  

<br>

  
  
      <h2>Introduction</h2>
  
  
    
      <p>
	  Model Driven Development (MDD) is increasingly gaining the attention of both industry and research communities. MDD stresses the use of models in the
	  software development life cycle and argues automation via model execution, model transformation and code generation techniques. The
	  <a href="http://www.omg.org/">OMG</a> is promoting a model-driven approach for software development through its Model Driven Architecture
	  (<a href="http://www.omg.org/docs/omg/03-06-01.pdf">MDA</a>&#8482;) initiative and its supporting standards, such as UML, MOF and QVT. Model Driven
	  Development is the specific application of MDA to software development.		
      </p>
      <p>
	  The Eclipse MDDi project is dedicated to the realization of a platform offering the integration facilities needed for applying a MDD approach. This
	  project will produce extensible frameworks and exemplary tools, designed to support various modeling languages (UML, Domain-Specific Languages) and
	  methodologies. The MDDi platform will provide the ability to integrate modeling tools, as well as tools supporting other technological spaces, to
	  create a fully customizable MDD environment.      	
      </p>
    
  

<br>

  
  
      <h2>Project outline</h2>
  
  
    
      <p>
	  <p><b>Scope</b></p>
	  The Eclipse platform is an excellent basis for building and integrating development tools. The MDDi project aims at extending it to enable an
	  additional level of integration for Model Driven Development. It will make available an extensible MDD platform, based on frameworks dedicated to
	  integration of modeling technologies and their supporting tools. The platform will provide a foundation that can be leveraged and extended for
	  the development of future modeling environments, whether they are open-source or commercial.
      </p>
      <p>
	  The MDDi project will ensure integration of services designed for the platform as well as external tools. Eclipse Modeling Framework (EMF)-based
	  tools are good candidates, although other modeling tools, including those supporting other technological spaces, can be considered as well. Other
	  technological spaces such as XML or graph transformation provide solution to MDD issues. For example, the space of formal technologies provides a
	  number of good practices and tools to validate models. The problem is that these technological spaces rely on heterogeneous support tools that usually
	  need to be manually or programmatically linked using specific bridges. The MDDi project will address this issue in the context of MDD.		
      </p>
      <p align="center">
      	<img src="main_data/image004.gif" border="0">
      </p>
      <p align="center">
	  	Figure 1: An instance of a MDD environment based on MDDi.
      </p>
      <p>
        Integration of modeling technologies is not trivial to achieve, although standard languages and exchange formats (such as XMI) already exist. As
	  illustrated on Figure 1, one has to deal with different languages (standard or proprietary, general-purpose or domain-specific), as well as different
	  tools and their related limitations (interoperability constraints, compliance to standards, deployment�).
	</p>
	<p>
	  In that context, three different levels of integration can be considered:
      </p>
	<ul>
        <li><p>
	    A technological integration level &#8211; ensures the interoperability of tools providing model-oriented services, but also provide connectivity
	    capabilities to other non-modeling tools (e.g. requirement tool, formal validation), provided they can be adapted to be driven by meta-models. It
	    allows tools supporting a common execution model to be compatible.
	  </p></li>
	  <li><p>
	    A semantics integration level &#8211; ensures the complete description of languages, as well as interchange mechanisms to ease comparison and
	    translation of these semantics between tools. Explicitly mapping the semantics used by tools is a requirement in order to support fully executable
	    models.
        </p></li>
        <li><p>
	    A methodological integration level &#8211; provides extension and customization capabilities to support an unbound number of development practices
	    based on modeling languages and model-driven methodologies.
	  <p></li>
      </ul>
      <p>
	  The end goal is to allow methodology experts to design modeling languages and development processes, and then to integrate existing tools (or build
	  new ones) to provide a dedicated supporting environment. Application developers can then use the platform tailored according to the expert�s
	  specifications for performing modeling tasks. The platform developed within MDDi will be therefore an integrated environment with modeling and 
	  meta-modeling capabilities, where MDD principles apply at both levels.
	</p>
	<p><b>Example user scenario</b></p>
	<p>
	  Why would a developer want to use the facilities provided by the MDDi project? Because different tools used in the same development cycle can rely on
	  different meta-models and models of execution. Moreover, some services (validation, transformation�) may be needed but not available in the modeling
	  tool currently used. Consider this scenario to be performed by a developer:
	</p>
	<ol>
        <li><p>
	     Model the structure of a system. The developer uses a UML modeler, and in particular class diagrams.
	  </p></li>
        <li><p>
	     Model the behavior of the same system. The developer uses a particular kind of Petri nets within a dedicated tool. (A Petri net tool will have a
	     meta-model that is clearly different from that of UML.)
	  </p></li>
        <li><p>
	     Check consistency between the two models built at Step 1 and Step 2.
	  </p></li>
        <li><p>
	     Generate code for the whole system from both modeled parts.
	  </p></li>
	</ol>
	<p>
	  This scenario uses different tools with different underlying conceptual and semantic models.  Putting them together is the essence of model integration.
	  This is just the simplest of scenarios.  Step 4 could be replaced by a transformation into another model, for example, and there are many elaborations
	  of this.
	</p>
	<p>
	  At the next level of abstraction up, we need a way to define precisely what is in a given modeling language easily. If tool support is required for domain
	  specific language, a solution is to use an existing UML modeler and to map this language onto the UML meta model. This is normally performed by a profile,
	  and there needs to be tools to define them easily. With those profiles, we can also enable user-defined concrete languages. This allows us to integrate
	  existing tools and tools with specialized languages.  
	</p>
	<p>
	  Specification of a set of tools and how they interact implies a development process.  We also need the ability to examine existing tools and decide how
	  well they fit, and make explicit the process. Orchestrating an automated development process is important, but we do not plan to build support for this
	  as a part of the core proposal.
	</p>
    
  
  

<br>


  
  	<h2><b>
  	Components</b></h2>
  
  
    
      <p>
	  The MDDi project makes available the core frameworks and services providing modeling integration facilities on top of Eclipse. The project will be
	  initially broken down into two main components enabling technological and semantics integration.
      </p>
	<p><b>ModelBus</b></p>
	<p>
	  <a href="./main_data/ModelBusWhitePaper_MDDI.pdf">ModelBus</a> will enable transparent interaction between model-aware tools. It will allow end users to
	  easily assemble heterogeneous tools, and let them interoperate without having any direct knowledge of the specifics of other tools. ModelBus will take
	  advantage of XMI, EMF, and protocols such as SOAP from the W3C.
	</p>
	<p>
	  ModelBus can be compared to a middleware as it offers facilities for managing modeling services interoperability. The particularity of ModelBus is
	  that it is dedicated to models. It mainly offers facilities at the model layer (M1 in the four layers architecture). It has the charge of model
	  representation (including translation of XMI versions), and model transmission (based on local method invocation or SOAP messages). The ModelBus
	  architecture is composed of three high-level components: Adapter, NotificationService and Registry. Those components offer added value to tools that
	  want to consume or provide modeling services. They fully rely on the Eclipse platform and extend it for managing modeling service interoperability.
	</p>
	<p>
	  ModelBus provides a generic infrastructure that needs to be extended by tool-specific adapters. When a tool that wants to consume or provide modeling
	  services, it only has to interact with its corresponding adapter plugged in the bus. A tool provider (or an integrator) who wants a tool to be
	  connectable to ModelBus will therefore create a related adapter using the ModelBus toolkit, which offers added value for extending the basic Adapter
	  component. Adapters are deployed then manually configured and linked to the tool. The end user is able to use them thanks to the Adapter component API.
	</p>
	<p><b>Semantic binding</p></b>
	<p>
	  In order to support model execution interoperability requirements, it is necessary to address the question of interchange of the semantic aspects of
	  meta-model artifacts. That is, the exchange of execution semantics data. The MDDi project will meet this requirement by supporting the negotiation of
	  a common meta-model to be used when exchanging semantic data between co-operating tools (e.g. a UML modeler and a Petri nets tool). This negotiation
	  is based on services registered by tools on the ModelBus and can be easily automated.
	</p>
	<p>
	  In the case where no matching meta-model can be found then the user should be able to define his own mapping from/to a default meta-model used as a
	  pivot. The OMG QVT standard will be used to describe such a mapping. The default meta-model is still to be defined but the UML 2.0 standard appears
	  to be a good candidate for the exchange of execution semantics data, as it is extensible (thanks to profiles) and comes with an action language, which
	  can be used to express the semantic part of the underlying modeling languages used by tools.
	</p>
	<p>
	  The Semantic binding component implemented on top of ModelBus will therefore provide:
	</p>
	<ul>
	  <li>a common interchange meta-model enabling a complete description of modeling languages semantics,</li>
	  <li>a dedicated meta-model negotiation service,</li>
	  <li>a tool to ease the description of user-defined "QVT mappings" between meta-models.</li>
	</ul>
	<p>
	  The Profile Design tool and the Action Semantics Modeling tool will be provided as initial contributions addressing the issue of describing precisely
	  modeling languages.
	</p>
	<p><b>Exemplary tools</b></p>
	<p>
	  The exemplary tools listed below will be also developed as part of the project to validate the platform components. The project especially needs a
	  sufficient number of adapters to ensure a successful adoption. We will provide an initial set of adapters for several modeling tools and we will also
	  propose additional ones depending on our resources and what the Eclipse community needs. A generic client for the ModelBus will be made available as well;
	  it will provide an Eclipse Menu for consuming modeling  service connected to the ModelBus.
	</p>
	<ul>
	<li>ModelBus adapters (ATL, EMF, MOF, UML2�),</li>
	<li>ModelBus generic client,</li>
	<li>Others�</li>
	</ul>
	<br>	
    
  




  
  	<h2><b>
  	Relationships with other Eclipse projects</b></h2>
  
  
  	
  	  <p>
	    The MDDi project aims at providing, in collaboration with GMF, GMT and other future projects, a comprehensive approach to MDD in the Eclipse Technology
	    project. MDDi will make available integration capabilities for the tools developed as part of these projects and will provide complementary technologies
	    (e.g. action semantics modeling). MDDi is also related to other Eclipse Tool projects such as EMF. Detailed relationships are presented below:
  	  </p>
	  <ul>
  	      <li><p>
			<b><a href="/proposals/eclipse-almiff/">Application Lifecycle Integration and Interoperability Framework (ALMIIF)
			</a></b>is a project that addresses the integration and interoperability of existing tools covering the application lifecycle. Overlaps with the
			ALMIIF proposal are currently being investigated.
		</li></p>
		<li><p>
			<b><a href="/emf">Eclipse Modeling Framework (EMF)</a></b> is the reference framework in the Eclipse environment for
			defining and generating model repositories based on meta-models. The MDDi platform will fully support EMF-based editors and allow model
			repositories to be plugged in ModelBus.
  	      </p></li>
  	      <li><p>
			<b><a href="/gmf">Graphical Modeling Framework (GMF)</a></b>
			aims at enhancing the editors generated by EMF to support graphical editing. We will support any GMF-based editors. The "tool generation" feature
			of GMF provides an exciting opportunity to automatically include the MDDi-provided integration capability as part of what gets generated. MDDi
			will provide those tools with a) the ability to integrate with other tools through services and events, and b) the ability to be customized
			(e.g. using wizards) to support specific development tasks/processes. 
  	      </p></li>
  	      <li><p>
			<b><a href="/gmt">Generative Model Transformer (GMT)</a></b> consists of a set of research prototypes dedicated to 
			model-driven software development. It provides, among others, model weavers and transformation engines that will be plugged onto the MDDi platform
			to provide modeling services. It is felt that there is no overlap between the MDDi and GMT project, as MDDi focuses on technology integration
			aspects. An adapter to connect the ATL transformation engine to the MDDi platform will be especially provided.
  	      </p></li>
  	      <li><p>
			<b><a href="/omelet">OMELET</a></b> provides a framework for integrating models and model transformations. This project has
			demonstrated the ability to build upon a set of transformations and the technological integration aspects of MDDi may have some overlap with it.
			A possible integration of the OMELET project�s work within MDDi will be evaluated to benefit from the lessons learned. 
  	      </p></li>
  	    </ul>
  	  <p></p>
  	  <p>
	    Tools from other Eclipse projects such as UML2, Data Tools, Web Tools and XSD are good candidates for integration in the MDDi platform as well. We plan to
	    evaluate existing components and reuse them every time it is possible and it meets our needs.
  	  </p>
  	
  

<br>





  
  	<h2><b>
  	Development plan</b></h2>
  
  
    
      <p>
	  A roadmap will be defined as soon as the project gets started. We plan to focus on technological and semantics integration during this first stage of the
	  project; future work will address methodological integration aspects, to support multiple development processes. The development of the MDDi platform will
	  be an interactive process involving contributors to the project, as well as actual tool developers willing to help defining the requirements and experiment
	  the developed technologies. 
      </p>
      <p>
	  The MDDi project has an initial goal, which is to build an extensible integration platform, dedicated to MDD, inside the Eclipse Technology project. A longer
	  perspective for the project shall be to drive, in collaboration with GMF, GMT and others, the creation of a top-level Eclipse project dedicated to modeling
	  tools.
      </p>
    
  


<br>


  
  	<h2><b>
  	Organization</b></h2>
  
  
    
      <p>
	  The MDDi proposal is endorsed by tool vendors, industrial companies, researchers and academia, interested in the realization of an open MDD platform. The
	  project will have initial committers among them. We encourage others to participate in all the aspects of the project. If you are interested, please take
	  part in the newsgroup discussions or ask to be added to the list of interested parties.
      </p>
      <p>
      	<b>Board members</b>
      	</p><ul>
      	  <li>Mariano Belaunde (France Telecom)</li>
      	  <li>Jean B�zivin / Freddy Allilaire (INRIA Nantes)</li>
      	  <li>Xavier Blanc (Laboratoire d'Informatique de Paris 6)</li>
      	  <li>Andy Evans / James Willans (Xactium)</li>
      	  <li>Pete Rivett / Nick Dowler (Adaptive)</li>
      	  <li>Madeleine Faug�re / S�bastien Demathieu (Thales)</li>
      	  <li>Pierre Gaufillet (Airbus France)</li>
      	  <li>Jean-Marc Jezequel (INRIA Rennes)</li>
      	  <li>Keith Mantell (IBM UK Limited)</li>
		  <li>Stephen Mellor / Campbell McCausland (Mentor Graphics)</li>
      	  <li>Miguel A. de Miguel (Universidad Polit�cnica de Madrid)</li>
      	  <li>Jon Oldevik (SINTEF)</li>
      	  <li>Yann Tanguy (CEA)</li>
      	  <li>R�gis Vogel (Enabler Inform�tica, S.A.)</li>
      	  <li>Wolfgang Wieser (Imbus AG)</li>
      	</ul>
      <p></p>
      <p>
	  	<b>Initial Committers</b>
	  	</p><ul>
      	  <li>Freddy Allilaire</li>
		  <li>Mariano Belaunde</li>
      	  <li>Xavier Blanc</li>
      	  <li>Nick Dowler</li>
      	  <li>Madeleine Faug�re</li>
		  <li>Stephen Mellor</li>
		  <li>Miguel A. de Miguel</li>
      	  <li>Jon Oldevik</li>
      	  <li>Yann Tanguy</li>
      	</ul>
      <p></p>
      <p>
	    <b>Initial Contributions</b>
	    </p><ul>
      	  <li>ModelBus and ModelBus toolkit (MODELWARE - LIP6)</li>
      	  <li>ATL adapter for ModelBus (MODELWARE - SINTEF, INRIA Nantes)</li>
      	  <li>Front-End QVT adapter for ModelBus (MODELWARE - France Telecom)</li>
      	  <li>Model repository adapter for ModelBus (MODELWARE - Adaptive)</li>
      	  <li>ModelBus Generic Client (LIP6)</li>
      	  <li>Action Semantic Modeling tool (CEA)</li>
      	  <li>Profile Design tool (CEA)</li>
		  <li>QoS and Safety Analysis tools (UPM)</li>
      	</ul>
      <p></p>
      <p>
	  	<b>Interested Parties</b>
	  	
	  	  
	  	  
	  	  	
	  	  	  
	  	  	  
	  	  	  
	  	  	    
	  	  	      EU FP6-IP MODELWARE Project (<a href="http://www.modelware-ist.org/">http://www.modelware-ist.org</a>):<br>
	  	  	    
	  	  	    
	  	  	  
	  	  	  
	  	  	    
	  	  	      <i>Thales, Adaptive Limited, Enabler Inform�tica, S.A., France Telecom, IBM UK Limited, Imbus AG,
	  	  	      Institut National de Recherche en Informatique et en Automatique (INRIA Nantes), Laboratoire
	  	  	      d&#8217;Informatique de Paris 6 (LIP6), Stiftelsen for industriell og teknisk forskning ved NTH (SINTEF),
	  	  	      Universidad Polit�cnica de Madrid (UPM)</i><br/><br/>
	  	  	    
	  	  	  
	  	  	  
	  	  	  
	  	  	
	  	  
	  	  
		  	
		  	  CARROLL Program (<a href="http://www.carroll-research.org/">http://www.carroll-research.org</a>):<br>
		  	  <i>CEA, INRIA, Thales</i><br/><br/>
		  	
	  	  
	  	  
		  	Other interested parties:<br><i>Airbus France, Xactium, Versata, Communications and Systems, MetaMatrix, inStream, Laboratoire d'Informatique
			Fondamentale de Lille (LIFL), Philips Medical Systems, Financial Toolsmiths AB, Politecnico di Milano, Mentor Graphics</i><br/>
		  	
	  	  
	  	  
	  	
      </p>
    
  


</div>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
