<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>EMF Feature Model</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("Eclipse Modeling Framework Feature Model");
?>

<h2>Introduction</h2>
<p>EMF Feature Model is a proposed open source project under the Eclipse Modeling Framework Technology Project (EMFT).
This proposal is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft</a> newsgroup.</p>
<h2>Background</h2>
<p>During the last years Feature Modeling has become the "standard" for
variability management in the field of Software Product Lines. Feature
Models are easy to understand and provide a generic way to represent
variability information, independent of a specific application domain.
Several independent projects using the Eclipse platform / EMF have each
defined their own meta model for feature models. Although these meta
models have considerable structural differences, their core semantics
are similar. A brief description of feature models can be found at <a href="http://en.wikipedia.org/wiki/Feature_model">Wikipedia</a> and this
<a href="http://www.pure-systems.com/fileadmin/downloads/pure-variants/tutorials/SPLWithFeatureModelling.pdf">article</a> describes a small feature modeling example.
The EMF Feature Model project will define a standard representation of
Feature Models inside the Eclipse platform. The intent is to provide a
uniform representation for variability information for tools based on
the Eclipse Modeling Framework. This will allow easy and consistent
access to variability-related information, such as variation points and
variant decisions, in DSLs, M2M transformations, and other contexts
where variability information is produced or consumed.</p>
<h2>Scope</h2>
<p>The objectives of the EMF Feature Model project are the following:
<ul> 
        <li>Define Feature Meta Model</li>
        <li>Define an extensible evaluation engine framework and provide an exemplary engine implementation</li>
        <li>Provide extensible editors and visualizations for the EMF Feature Models</li>
</ul>
</p>
<h2>Description</h2>
<h3>Relations to other Eclipse Projects</h3>
<p><ul>
        <li>EMF Feature Model will be built on top of EMF</li>
        <li>GMF and ZEST will be used to implement the editors and visualizations</li>
        <li>M2T (Xpand) and TMF (Xtext) eclipse projects already have (simple) interfaces for feature model information integrated. These should be based on the proposed EMF Feature Model.</li>
        <li>Other eclipse projects with need for variability modelling (e.g. UML2, BPEL) could use the meta model and evaluation API provided by the project to create model variants.</li>
</ul>
</p>

<h3>Organization</h3>
<h4>Mentors</h4>
<p><ul>
        <li>Ed Merks</li>
        <li>C&eacute;dric Brun</li>
</ul>
</p>
<h4>Proposed initial committers</h4>
<p>
<ul>
        <li>Holger Papajewski (pure-systems GmbH, Germany)</li>
        <li>Markus V�lter</li>
	</ul>
</p>
<h4>Interested parties </h4>
<p>Parties indicating interest were
	<ul>
        <li>pure-systems GmbH</li>
        <li>itemis AG</li>
        <li>Geensys</li>
        <li>General Motors Corp. [pending management approval]</li>
        <li>Open Canarias S.L.</li>
        <li>IKV++ AG</li>
        <li>Generative Software Development Lab, University of Waterloo</li>
        <li>Andreas Rummler, SAP Research</li>
	</ul>
</p>
<h3>Code contributions</h3>
<p>The initial contribution will be from the pure::variants � Connector for Ecore. pure::variants is a commercial variant management tool, which already provides a mapping for its internal feature models to Ecore models. The Connector for Ecore source contains the Ecore meta model and related APIs. Xpand and Xtext already have interfaces to this meta model. The pure::variants feature meta model is mature and has been used for modeling in commercial and research applications for several years now and thus should provide a solid basis for the EMF Feature Model.</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
