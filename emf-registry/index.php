<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>EMF Registry Proposal</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("EMF Registry");
?>
		
<h2>Introduction</h2>
<p>EMF Registry is a proposed open source project under the <a HREF="/modeling/emft/">Eclipse
Modeling Framework Technology Project</a> (EMFT).</p>
 
<p>This proposal is in the Project Proposal Phase (as defined in the
<a HREF="/projects/dev_process/">Eclipse Development Process
document</a>) and is written to declare its intent and scope. This
proposal is written to solicit additional participation and input
from the Eclipse community. You are invited to comment on and/or join
the project. Please send all feedback to the
<a HREF="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.emft</a>
newsgroup.</p>

<h2>Background</h2>

<p>EMF provides a well-established and widely used mechanism to allow developers
to publish model and meta-model as plugin registrations for use by Eclipse users.</p>

<p>There is no accepted mechanism to allow users to publish models and meta-models
for use by themselves or other users. Consequently a modeling user developing
for instance both a model transformation and a
meta-model has no convenient way to enable tools processing the model transformation
to locate the meta-model. Various modeling projects have worked around this
problem with inconsistent and not very satisfactory solutions.</p>

<p>This problem was addressed in a modeling project independent fashion by
the GMT UMLX Model Registry which migrated to the M2M QVT Declarative Model Registry.
In order to support the migration of an OCL editor from M2M QVT Declarative
to MDT OCL, a further migration of the Model Registry is necessary.</p>

<p>This proposal advocates making the Model Registry available as an independent
project, facilitating its availability and usage in a modeling project independent fashion.
An EMFT positioning makes this simple but critical modeling functionality available
to all EMF users eliminating dependency problems. It avoids the need for
a Model Registry migration to MDT OCL.</p>

<h2>Description</h2>

<p>A more detailed write-up may be found at
<a HREF="http://www.eclipse.org/gmt/umlx/doc/EclipseAndOMG08/ModelRegistry.pdf">
http://www.eclipse.org/gmt/umlx/doc/EclipseAndOMG08/ModelRegistry.pdf</a>

<p>The model registry supports a two level user definable mapping. Firstly
from an arbitrary name (such as UML_4_5) to a nominal URI (such as "http://one/day/uml4.5")~
and then from a nominal URI to an exact URI
(such as platform://projects/MyUML/model/uml4_5.uml). Tool support
for languages that define models using URIs can use the model registry
as a URI resolver. Tool support for languages that define models using
names may use both levels of mapping. 

<p>EMF Registry will provide the following functionality: 
</p>
<dl>
	<dt>Persistent Registry</dt><dd>
	The Model Registrations are saved in a per-project EMF model.
	<dt>Registry API</dt><dd>
	The API enables tools to browse the registry and locate and load
	a required (meta-)model.
	<dt>UI - Registrations</dt><dd>
	Property pages support maintenance of the registry. 
	</dd>
	<dt>UI - Browser</dt><dd>
	A browser supports viewing registered models in a 'Sample Ecore Editor'. 
	</dd>
	<dt>Extensibility</dt><dd>
	Registration mechanisms will be extensible to support registration of a
	a variety of different model-related artefacts, such as transformations,
	execution engines, ...
	</dd>
</dl>

<p>EMF Registry will not provide the following functionality: 
</p>
<dl>
	<dt>Persistent Models</dt><dd>
	EMF and other Eclipse Content Type related storage facilities are used.
	<dt>Format conversion</dt><dd>
	The API will exploit existing Ecore/EMOF/UML conversions where available.
	<dt>Transformation Compilation or Execution</dt><dd>
	Transformation language-dependent tools are enabled.
	<dt>Transformation Sequencing</dt><dd>
	A problem too far for a 'simple' project..
</dl>

<h2>Relationship with other Eclipse Projects</h2>
<ul>
	<li>EMF Registry will be built on top of EMF.</li>
	
	<li>As EMF Registry will be generic, it will be usable by any 
	modeling project, not depending on the specific modeling language.
	</li>

	<li>EMF/Index is expected to use EMF Registry to support location of the models
	and meta-models for which element indexing servicves are provided. 
	</li>

	<li>MDT/OCL will use EMF Registry to support location of meta-models for
	semantic validations and completions within, for instance, the OCL editor. 
	</li>

	<li>M2M/QVT Declarative will use EMF Registry to support location of
	meta-models for semantic validations and completions within, for instance, the 
	QVT Relations and QVT Core editors and compilers. 
	</li>

	<li>M2M/QVT Operational is expected to use EMF Registry to support location of
	meta-models for semantic validations and completions within, for instance, the 
	QVT Operational editors and compilers. 
	</li>

	<li>It is expected that the functionality of the M2M/QVT Operational model
	browser will be provided by the EMF Registry UI. 
	</li>

	<li>It is expected that the transformation registration functionality of the
	GMT/AM3 ATLAS MegaModel Management will be supported by the Model Registry allowing
	transformation composition tools to exploit accurate type signatures and
	transformation execution tools to invoke transformations in a variety
	of technologies.
	</li>

	<li>GMT/UMLX will use EMF Registry to support location of
	meta-models for semantic validations and completions within, for instance, the 
	UMLX editors. 
	</li>

	<li>It is expected that a variety of other modeling projects will benefit
	from this shared functionality. 
	</li>
</ul>

<h2>Organization</h2>
<h3>Mentors</h3>
<ul>
<li>Sven Efftinge (itemis AG, Germany)</li>
<li>C&eacute;dric Brun (Obeo)</li>
</ul>

<h3>Proposed initial committers</h3>
<ul>
	<li>Ed Willink (Thales, UK), lead </li>
</ul>

<h3>Interested parties</h3>
<p>The M2M/QVT Declarative Model Registry and M2M/QVT Operational Model Browser
were discussed at the Modeling BoF at the EclipseCon 2008. 
</p>
<p>Parties indicating interest are 
</p>
<ul>
	<li>AMMA, Jean B&eacute;zivin</li>
	<li>Epsilon, Richard Paige</li>
	<li>Topcased, Marc Pantel</li>
	<li>Viatra, Istv&aacute;n R&aacute;th</li>
</ul>
<h3>Code contributions</h3>
<p>The initial code contribution will be a set of plug-ins from the
M2M/QVT Declarative Eclipse project. 
</p>




      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
