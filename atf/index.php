<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>The AJAX Toolkit Framework (ATF) Project</h1>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("AJAX Toolkit Framework (ATF)");
?>

<h2>An Eclipse Incubation Project Proposal<br />
January 2006</h2>


<b>1	Introduction</b>
<p align=justify>
The AJAX Toolkit Framework is a proposed open-source project to be incubated within the Eclipse Web Tools Platform ("WTP") Project.  This document describes the content and the scope of the proposed project.
</p>
<p align=justify>
This proposal is in the Project Proposal Phase (as defined in the Eclipse Development Process document) and is written to declare its intent and scope. This proposal is written to solicit additional participation and input from the Eclipse community. You are invited to comment on and/or join the project. Please send all feedback to the http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools newsgroup.
</p>

<b>2	Background</b>
<p align=justify>
Asynchronous JavaScript And XML, or AJAX, is a web development technique for creating interactive web applications using a combination of:
</p>
<ul>
<li>	XHTML (or HTML) and CSS for marking up and styling information. (XML is commonly used, although any format will work, including preformatted HTML, plain text, JSON and even EBML).</li>
<li>	The Document Object Model manipulated through JavaScript to dynamically display and interact with the information presented</li>
<li>	The XMLHttpRequest object to exchange data asynchronously with the web server. In some Ajax frameworks and in some situations, an IFrame object is used instead of the XMLHttpRequest object to exchange data with the web server.</li>
</ul>
<p align=justify>
Like DHTML, LAMP, or SPA, AJAX is not a technology in itself, but a term that refers to the use of a group of technologies together.
</p>
 
<b>3	Project Goal</b>
<p align=justify>
AJAX Toolkit Framework (ATF) will provide extensible frameworks and exemplary tools for building IDEs for the many different AJAX runtime offerings (Dojo, Zimbra, etc) in the market. These frameworks will contain features for developing, deploying, debugging and testing AJAX applications. Tools built upon these frameworks will initially include: enhanced JavaScript editing features such as edit-time syntax checking; an embedded Mozilla web browser; an embedded DOM browser; and an embedded JavaScript debugger. Because it is a framework, ATF will provide for the development and incorporation of additional AJAX development tooling. ATF will use existing Eclipse extensions for web application deployment so that the environment will be "server agnostic" - that is, a developer may easily extend the framework to deploy their AJAX application to an arbitrary new class of server. Initial adapters will include a J2EE / JSP adapter and an Apache / PHP adapter. An additional and unique aspect of the framework is the Personality Builder function, which assists in the construction of arbitrary AJAX runtime frameworks, thus allowing those runtimes to be used with ATF tools.
</p>

<b>4	Project Description</b>
<p align=justify>
The AJAX Toolkit Framework will include the following components:

<ul>
<li>	<b><i>ATF Tools</i></b>
<p align=justify>
Note that the initial contribution focuses heavily on the use of Mozilla because the Mozilla system offers cross-platform interfaces to enable such things as JavaScript Debugging. However, as a framework, ATF exposes a prototype API that would allow for the incorporation of arbitrary browsers with comparable function as they become available.
</p>
<ul>
<li>	Enhanced JavaScript Editing Features
<ul>
<li>	Batch and as-you-type syntax validation</li>
</ul>
</li>

<li>	JavaScript Debugger
<ul>
<li>	Tight integration with Eclipse debug UI to provide flow control in browser runtime and the ability to examine JavaScript code and variables</li>
</ul>
</li>

<li>	Embedded Browser
<ul>
<li>	Access to browser's DOM, e.g., Mozilla XPCOM </li>
</ul>
</li>

<li>	DOM Inspector / JavaScript Console
<ul>
<li>	Mozilla tools integration for DHTML developers as Eclipse Views.</li>
</ul>
</li>
</ul>
<br />
</li>
<li>	<b><i>ATF Personality Framework</i></b>
<br /><br />
<ul>

<li>	Personality
<p align=justify>
A Personality is a collection of IDE features that are specifically targeted to a certain AJAX Runtime Library. This is a core concept of ATF: providing an extensible framework to support AJAX development in arbitrary AJAX runtimes. 
</p>
</li>

<li>	Browser Embedding Framework
<br /><br />
<ul>
<li>	A prototype API that allows for the embedding of high-function web browsers within the ATF framework to support JavaScript debugging, DOM inspection, and other interesting browser-centric tooling.<br /></li>
</ul>
<br />
</li>

<li>	Personality Builder Framework
<p align=justify>
The Personality Builder Framework is primarily accessed via the Personality Builder Wizard, which generates the basic Eclipse assets for a new personality. They include:
</p>
<ul>
<li>	Plugin projects - A set of plugin projects that constitute a personality</li>
<li>	Eclipse Extensions - Extensions to existing Eclipse extension points (Wizards, Snippets, etc)</li>
<li>	New Extensions - Extensions to newly defined Extension points:

<ul>
<li>	Wizard Models: for contributing new AJAX application wizards.</li>
<li>	Snippet Models: for contributing new code snippets.</li>
<li>	Nature Artifacts: to define the source of AJAX runtime artifacts and control where runtime artifacts are placed within the project when a particular AJAX nature is injected. </li>
<li>	Variable Factory: for contributing new variable types to be used in the wizardModel and snippetModel extensions.</li>
<li>	Variable Renderer Factory: for contributing new variable renderers which control how a variable type input is rendered in a wizard or snippet dialog window. </li>
<li>	Operation Factory: for contributing new wizard operations such as file creations.</li>
</ul>
</li>

<li>	A special Plugin project that wraps all the required assets of the Ajax Runtime Library (JavaScript Files, Java Files, HTML, JSP, etc)</li>
<br />
A developer seeking to contribute a new Personality will typically begin with the Personality Builder Wizard and then fill in runtime-specific code as necessary. 
<br /><br />
</li>
</ul>
<li>	Integrated Deployment
<p align=justify>
<ul>
<li>	Using Eclipse APIs ATF will support J2EE / JSP and Apache / PHP - in practice ATF will be 'server agnostic' to the largest extent possible.
</ul>
</ul>
</p>

</ul>
</p>

<b>5	Dependencies</b>
<p>
<ul>
<li>	Rhino, JSLint
<ul>
<li>	Used for JavaScript syntax validation</li>
</ul>
</li>
<li>	XULRunner with JavaConnect
<ul>
<li>	Mozilla-specific code to enable the embedding of the Mozilla browser within a Java container (Eclipse)
</ul>
</li>
<ul>
<li>AJAX Runtime
<br />
ATF is tooling for arbitrary AJAX runtimes. The initial distribution will include tooling support for
<ul>
<li>	Zimbra</li>
<li>	OpenRico</li>
<li>	Dojo</li>
</ul>
Note: one or more of these initial AJAX runtimes may move to Apache.org, but whether the runtimes are part of Apache.org is irrelevent to this proposal.
</li>

<ul>
<li>	Eclipse Platform 3.2 and WTP 1.0</li>
</ul>
</ul>
</ul>
</p>

 
<b>6	Design</b>

<p align=justify> 
<a href="ATF2.gif"><img src="ATF2.gif" border="0" /></a>
<center><i>AJAX Toolkit Framework System Architecture</i></center>
<br />
The AJAX Toolkit Framework will be built using several Eclipse Web Tools Platform (WTP) project components. The WTP provide generic, extensible and standards-based tools for Web-centric application development. It provides editors for various Web-centric languages such as: HTML, CSS, JavaScript, WSDL, etc. In addition, WTP provides facilities for configuring and connecting to Web Servers. The AJAX Toolkit Framework will use the WTP Server facilities to manage and configure Web Servers.  AJAX Toolkit Framework will benefit from the client web development support, already available in the WTP (HTML, CSS, etc) and when necessary will take advantage of server side web deployment. The integration between the AJAX Toolkit Framework and WTP will provide AJAX developers users with a comprehensive solution for Web development.
<br /><br />
Main plug-ins:

<ul>
<li>	Enhanced JavaScript Editing features - providing edit-time and batch syntax checking of JavaScript code.</li>

<li>	Embedded Mozilla Browser - provides cross-platform preview of AJAX application, plus DOM inspector function via XPCOM bridge.</li>

<li>	Embedded JavaScript Debugger - takes advantage of the Mozilla / XPCOM bridge to provide an Eclipse-style debugging experience for JavaScript code.</li>

<li>	Personality Builder - Wizard-driven functions that allow for the easy adaptation of arbitrary AJAX runtimes into the Eclipse / AJAX Toolkit Framework.</li>
</ul>
</p>

<b>7	Code contributions</b>

<p align=justify>
IBM will make an initial code contribution that will encompass the core functionality for the AJAX Toolkit Framework project including:
<ul>
<li>	Enhanced JavaScript editing features including edit-time syntax checking and validation</li>
<li>	Integrated Mozilla environment including browser, DOM inspector, Error console, and debugging</li>
<li>	A Personality Builder that will facilitate the construction of additional personalities to allow arbitrary AJAX runtimes to be used within ATF</li>
<li>	ATF personalities for Zimbra, OpenRico, and Dojo</li>
<li>	Adapters for J2EE / JSP and Apache / PHP</li>
</ul>
</p>

<b>8	Development Plan</b>
<p align=justify>
<ul>
<li>	1Q2006: Initial Prototype version available, includes stabilization and bug-fixes.</li>

<li>	2Q2006: Process input from developing community to enhance functionality. Current considered enhancements include:</li>
<ul>
<li>	Support for additional AJAX runtimes.</li>
<li>	Support for additional server adapters.</li>
<li>	Refinement of prototype browser embedding API.</li>
<li>	Modeling of Widget sets and other components for AJAX runtimes.</li>
<li>	WYSIWYG editing framework for arbitrary AJAX runtimes.</li>
<li>	Enhancing Personality Builder extensions and utility.</li>
</ul>
</ul>
</p>

<b>9	Organization</b>
<p>
9.1	Project Lead
</p>
<p>
The project lead will be initially the following member:

<ul>
<li>	Craig Becker, IBM 	(jlpicard@us.ibm.com)</li>
</ul>
</p>

<p>
9.2	Interested Parties
<br /><br />
Support has been expressed by:
<ul>
<li>	BEA</li>
<li>	IBM</li>
<li>	Laszlo</li>
<li>	Oracle</li>
<li>	RedHat</li>
<li>	Genuitec</li>
<li>	Yahoo</li>
<li>	Zend</li>
<li>	Zimbra</li>
</ul>
</p>

<p>
9.3	Potential Committers  	
<ul>
<li>	Craig Becker,		IBM 	(jlpicard@us.ibm.com)</li>
<li>	Leugim Bustelo, 	IBM	(lbustelo@us.ibm.com)</li>
<li>	Javier Pedemonte, 	IBM	(pedemont@us.ibm.com)</li>
<li>	Adam Peller,	 	IBM 	(apeller@us.ibm.com)</li>
<li>	Donald Sedota, 	 	IBM  	(sedota@us.ibm.com)</li>
</ul>
<br />
Additional contributors from outside of IBM are being sought.
</p>

      </div>
  </div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
