<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "";
$pageKeywords	= "";
$pageAuthor		= "";

ob_start();
?>
    <div id="maincontent">
	<div id="midcolumn">

<h1>Swordfish</h1>
</p>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/projects/fragments/proposal-page-header.php");
generate_header("SOA Runtime");
?>

<p>

<h2>Introduction</h2>


<p>"Swordfish" is the nickname for the SOA Runtime Framework project.
It is is a proposed open source project. This proposal is in the
Project Proposal Phase (as defined in the Eclipse Development Process
document) and is written to declare its intent and scope. This proposal
is written to solicit additional participation and input from the
Eclipse community. You are invited to comment on and/or join the
project.
Please send all feedback to <a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.soa">http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.soa</a>.</p>


<h2>Background</h2>


<p>The goal of a service-oriented business architecture (SOA) is the
loose coupling of business services. In order to enable such a
service-oriented business architecture users require a SOA runtime
platform. As of today, enterprise users face a number of challenges in
operating these SOA runtime platforms, e.g.</p>


<ol>

  <li>SOA Platforms consist of a set of internal technical services
(e.g. registry, messaging, and security). Today, those technical
services are
strongly coupled within most of these platforms. A loose coupling of
those services would allow a best of breed approach and would give the
user the option to leverage existing software assets</li>

  <li>The interoperability between different SOA runtime platforms is
often limited to the basic technical services (e.g. messaging). High
level
interoperability (e.g. business process orchestration) across SOA
domains operated on different platforms is often not possible.</li>

  <li>The enablement of embedded systems collaborating in a SOA runtime environment.</li>

</ol>


<p>The SOA platforms available today fall short to these expectations
and limit the value of SOA on the user side. This project intends to
address these three challenges:</p>

<ul>

  <li>Lack of common interfaces between the technical components of a SOA runtime platform</li>

  <li>Lack of interoperability between SOA environments</li>

  <li>Lack of SOA runtime deployment on embedded systems, e.g., on medical devices in the healthcare industry</li>

</ul>


<p><b>Lack of common interfaces</b>: The different SOA runtime
platforms consist of very similar technical components (e.g. service
registry, messaging). The lack of common APIs between these technical
components means
that these components cannot be exchanged. This project intends to
develop a common framework/API which treats these components as
exchangeable
plug-ins.</p>


<p><b>Lack of interoperability</b>: Although there are ongoing
standardization efforts, the interoperability of heterogeneous SOA
environments
is very limited today. As an example, a BPEL code can usually not be
executed across different SOA environments. Rather than introducing yet
another
new SOA platform, the SOA Runtime Framework introduces a new
abstraction layer and allows developers to create adapters to existing
SOA platforms.</p>


<p><b>Lack of SOA deployment on embedded systems</b>: The vast majority
of the currently existing SOA environments is exclusively targeted at
enterprise
environments and demands a considerable amount of memory and processing
resources. The SOA Runtime Framework takes a different approach in that
all components could potentially be replaced by alternatives with a
smaller footprint which renders it equally well suited for both
enterprise environments and
resource-restricted embedded and mobile devices.</p>


<h2>Description</h2>


<p>The goal of the Swordfish project is to provide an extensible SOA
framework that can be complemented by additional open source components
such as a service registry, a messaging system, a BPEL engine etc. to
form a comprehensive open source SOA runtime environment based on both
established and emerging open standards.</p>


<p>The following figure illustrates Swordfish's component architecture:</p>



<div style="text-align: center;"><img style="width: 397px; height: 356px;" src="swordfish%20architecture.png" alt=""></div>



<h3>(1) Functional components and plug-ins</h3>


<p>Swordfish defines a number of components and plug-ins:</p>


<ul>

  <li><i>Business Application Adaptor</i> (plug-in)
    <p>Business application adaptors connect business application logic
(the actual service implementation) to the other components within the
framework. A Business Application Adaptor's reponsibilities include the
mapping of implementation-specific data types to normalized XML
messages and the correlation of incoming and outgoing messages. A
sample implementation of an Business Application Adapter delivered
along with the framework provides an easy-to-use Java API to facilitate
the integration of business logic.<br>
In addition to that, an open source BPEL engine will be integrated into
the framework as another example of a Business Application Adaptor.</p>
  </li>


  <li><i>Service Resolver</i> (plug-in)
    <p>The Service Resolver is reponsible for resolving logical service
references into physically addressable communication endpoints. The
revolving process can take additional metadata (e.g. policies) into
account, depending on the desired strategy. A sample implementation
delivered as part of the
framework uses a file system-based data store to retrieve service
information. Another possible implementation resolves service
references based on information stored in a UDDI registry.</p>
  </li>


  <li><i>Security Manager</i> (core component &amp; plug-in)
    <p>The Security Manager is responsible for coordinating and
supervising security-related tasks such as authentication,
authorization, data confidnetiality and integrity protection within the
framework.</p>
  </li>


  <li><i>Message Dispatcher</i> (core component)
    <p>The Message Dispatcher is responsible for decoupling message
processing being performed inside the Message Processor from the
Transport Bindings. The interfaces exposed by the Message Dispatcher
are defined by the JBI standard.</p>
  </li>


  <li><i>Transport Bindings</i>&nbsp;(plug-in)
    <p>Transport Bindings are reponsible for a) transforming messages
from the canonical representation used
within the framework into transport and protocol specific formats and
b) communicating with remote source and target systems. The interface
provided by the framework for integrating Transport Bindings is defined
by the JBI standard. Sample implementations delivered along with the
framework support SOAP over HTTP and SOAP over JMS.</p>
  </li>


  <li><i>Message Processor</i> (core component)
    <p>The Message Processor is responsible for coordinating message
processing between Business Application Adapters and Transport
Bindings. The actual
processing is performed within a chain of pluggable Interceptor
components. The assembly of the processing chain and the order of
execution is controlled by an interchangeable Planner component. A
sample implementation of a Planner that is provided as part of the
framework evaluates policies in WS-Policy format.</p>
  </li>


  <li><i>Message Interceptors</i> (plug-in)
    <p>A Message Interceptor is responsible for processing a message
regarding (ideally exactly) one
functional aspect such as transformation, validation or compression.
Message Interceptors can be assembled into a chain to jointly
perform more complex tasks. The interface that Message Interceptors
have to implement will be defined within this project and serves as a
public extension point of the framework.</p>
  </li>


  <li><i>Management/Monitoring</i> (core component &amp; plug-in)
    <p>The Management/Monitoring component is responsible for recording
and processing relevant events within the framework and provides
management interfaces for monitoring and controlling the framework's
components remotely. A sample implementation provided along with the
framework is based on the JMX standard.</p>
  </li>


  <li><i>Configuration</i> (core component &amp; plug-in)
    <p>The Configuration component provides configuration data to all
other components of the framework in a unified way. A sample
implementation that is delivered as part of the framework takes
configuration data from filesystem-based storage. Another possible
implementation retrieves configuration data
from a remote respository.</p>
  </li>

</ul>


<p>In addition to that, the SOA Runtime Framework provides a deployment mechanism that supports run-time deployment of components
based on artefacts generated by SOA design tools (e.g. by the Eclipse SOA Tools Platform).</p>


<h3>(2) Foundation on OSGi</h3>


<p>The foundation on OSGi is a prerequisite of the SOA Runtime
Framework due to the following reasons. The OSGi Framework
provides a mature and proven component lifecycle model with an
underlying security concept and a fine-grained isolated class loading
scheme which makes it
an excellent choice for the technical foundation of the SOA Runtime
Framework. In addition to that, OSGi&rsquo;s notion of a dynamically
deployable
bundle as a higher level unit of modularization allows for the
flexibility that is necessary to support the SOA Runtime
Framework&rsquo;s deployment model. Apart from these technical aspects,
adopting OSGi as the SOA Runtime Framework&rsquo;s future technical
base architecture has a number of additional advantages, including</p>

<ul>

  <li>Seamless integration into the Eclipse ecosystem,</li>

  <li>Potentially more straight-forward integration into future enterprise runtime environments such as applications servers, and</li>

  <li>The opportunity to extend the reach of the SOA Runtime Framework into the embedded and mobile devices market.</li>

</ul>


<h3>(3) Support existing SOA standards</h3>


<p>It is the intention that the SOA Runtime Framework will support all
available major existing SOA standards and be flexible enough
to react to future standard work. As of today, the Service Component
Architecture (SCA) and Java Business Integration (JBI) are planned to
be supported.</p>

<ul>

  <li><i>Service Component Architecture (SCA)</i>:
SCA describes a model for building applications and systems using a
service-oriented and component-based architecture. The SOA Runtime
Framework will be designed to act as an SCA runtime implementation that
is able to execute SCA applications built, for example, with the
development tools created by the Eclipse SOA Tools Project. The SOA
Runtime Framework intends to provide a framework for the consumption of
existing SCA runtime implementations, for example Tuscany and Fabric3.</li>


  <li><i>Java Business Integration (JBI)</i>:
JBI describes Java standard interfaces for normalized message mediation
between service components within a Java VM, along with a corresponding
component lifecycle model and management facilities. Out of these, the
normalized messaging concept is especially valuable to allow for
protocol and transport abstraction. Based on OSGi as the underlying
component model, the intention of the SOA Runtime Framework
project is to provide a JBI 2.0-compliant implementation.</li>

</ul>



<h2>Organization</h2>

<h3>Project Management</h3>

The SOA Runtime Framework project is overseen by a project lead.<br>

<ul>

  <li>Oliver Wolf (Sopera): Project lead</li>

</ul>

<h3>Initial committers and contributors</h3>

<p>The initial committers will focus on (a) making the contributed code
OSGi based, (b) providing
framework APIs, and (c) submitting patches and further code
contributions. Our agile
development process will follow eclipse.org's standards for openness
and transparency. As such we will actively encourage contributions to
this
project. The initial committers and contributors are:</p>


<ul>

  <li>Klaus Kiehne (DPWN): Lead Architect &amp; Committer</li>

  <li>James Strachan (IONA): Architect &amp; Committer</li>

  <li>Guillaume Nodet (IONA): Committer</li>

  <li>Zsolt Beothy-Elo (Sopera): Contributor</li>

  <li>Gerald Preissler (Sopera): Contributor</li>

  <li>Dietmar Wolz (Sopera): Contributor</li>

  <li>Anubhav Sharma (Sopera): Contributor</li>

  <li>Andreas Mattes (Sopera): Contributor</li>

  <li>Ramtin Mesbahipour (Individual): Contributor</li>

  <li>Adrian Co (Individual): Contributor</li>

  <li>Jonas Lim (Individual): Contributor</li>

  <li>Torsten Reimers (SoftQuadrat): Contributor</li>

  <li>J&ouml;rg Drescher (Sopera): QA</li>

  <li>Georg Voutsinos (Sopera): QA</li>

  <li>Carsten Biermann (Sopera): Build</li>

</ul>


<h3>Mentors</h3>

<ul>

  <li>Ois&iacute;n Hurley (IONA): STP PMC Member and Member of the Architecture Council</li>

  <li>Jochen Krause (innoopract): WTP PMC Member</li>

</ul>


<h3>Potential Consumers</h3>

<p>Potential consumers of the SOA Runtime Framework within the Eclipse
Foundation include the ALF, SOA Tools, EclipseLink and the OSEE
projects. Potential non-Eclipse consumers include Deutsche Post World
Net (DPWN), Senacor, SoftQuadrat GmbH.</p>


<h3>Code Contribution</h3>

<p>Deutsche Post World Net (DPWN) will contribute its custom-developed SOA
platform into this project. The contributed code contains the initial
code representing the framework, a messaging plug-in, a service
discovery plug-in and a management plug-in.</p>

<p>The contributed code of Deutsche Post World Net is not based on OSGi.
One of the first project tasks will be an adoption of the code to
deliver an OSGi-based runtime component which enables the
implementation of OSGi-based service providers and consumers. The DPWN
contact person for the Eclipse Community will be Klaus Kiehne (Deutsche
Post World Net).</p>

<h3>Related projects</h3>

<p>The following related projects which require future alignment:</p>


<ul>

  <li><i>SOA Tool Platform (STP):</i> The major phase of the SOA
Lifecycle Management are &ldquo;Planning&rdquo;, &ldquo;Design&rdquo;,
&ldquo;Implementation&rdquo;,
&ldquo;Deployment&rdquo;, &ldquo;Operations&rdquo; and
&ldquo;Support&rdquo;, The STP aims the earlier phases of the
SOA lifecycle &ndash; &ldquo;Planning&rdquo;, &ldquo;Design&rdquo; and
&ldquo;Implementation&rdquo;. In contrast the SOA Runtime Framework
project aims the later phases &ldquo;Operations&rdquo; and
&ldquo;Support&rdquo;. &nbsp;It is general consensus that STP and
Swordfish complement each other. More
specifially, the deployment framework &nbsp;developed as part of the
STP effort &nbsp;will be crucial for successfully using Swordfish
in productive environments.</li>


  <li><i>Equinox:</i> Swordfish relies heavily on OSGi as the
underlying component architecture.
Advancements in OSGi standardization and implementation -- especially
regarding results of the OSGi Enterprise Expert Group -- are expected
to require frequent alignment with Equinox</li>


  <li><i>Corona:</i> The Corona project also aims at defining a Service
Runtime Framework, but concentrates on tool integration rather that
general SOA
applications. It is anticipated that Corona could benefit from adopting
Swordfish as the underlying SOA runtime framework in the future.</li>


  <li><i>Higgins &ndash; The Trust Framework:</i> The Higgins project
is developing an extensible, platform-independent,
identity protocol-independent, software framework to support existing
and new applications that give users more convenience, privacy and
control over their identity information. Higgins should be considered
as a preferred candidate for a potential security and identity
framework for Swordfish.</li>

</ul>


<h2>Tentative Plan</h2>


<p>Assuming a start of the incubation phase in August 2007 the following milestones are planned:</p>

<table style="text-align: left; width: 100%; height: 116px; background-color: rgb(255, 255, 255);" border="1" cellpadding="2" cellspacing="2">

  <tbody>

    <tr>

      <td><i>Milestone</i></td>

      <td><i>Version</i></td>

      <td><i>Deliverable/Scope</i></td>

    </tr>


    <tr>

      <td>October 2007</td>

      <td>v0.5</td>

      <td>Basic framework</td>

    </tr>


    <tr>

      <td>February 2008</td>

      <td>v0.7</td>

      <td>All examply plug-ins</td>

    </tr>


    <tr>

      <td>July 2008</td>

      <td>v0.9</td>

      <td>BPEL plug-in</td>

    </tr>


    <tr>

      <td>October 2008</td>

      <td>v1.0</td>

      <td>SCA compliant deployment mechanism</td>

    </tr>

  
  </tbody>
</table>



<h2>Outlook</h2>

<p>The future server-side extension of OSGi (e.g. enterprise deployment and distributed
execution container) should be leveraged after v1.0 as soon as a first open source implementation is available.</p>




</p>

      </div>
  </div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
